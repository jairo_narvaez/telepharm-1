# Configure dev environment

## Install composer & install dev dependencies

```cd www```

```curl -s https://getcomposer.org/installer | php```

```php composer.phar install --dev```

After first installation composer should update dependencies

```php composer.phar update --dev```

## Modify configuration files

```cd www/application```

```cp -R default-config config```

```cd  www-old/application/```

```cp -R default-config config```

```cd  www-old/patient/```

```cp default-config.php config.php```

## Configure db for old apps

```vim www-old/application/config/database.php```

```vim www-old/patient/config.php```

# Create cache & logs in new and old apps

```mkdir -p www/application/cache```

```mkdir -p www/application/logs```

```mkdir -p www-old/application/cache```

```mkdir -p www-old/application/logs```

Create the folders for images

```mkdir -p private/images/{source,account}```

```cd private/images/source```
```php ../../../bin/rmkdir.php 2```

```cd private/images/account```
```php ../../../bin/rmkdir.php 2```

## Create, Configure and Import DBs

Create ```telepharm``` & ```telepharm_testing``` for the new app

Configure ```www/application/config/database.php``` to point to your created DB instance(s)

Run migrations

```php www/index.php --uri=dbmigrate/diff``` to see diff of migrations to be run

```php www/index.php --uri=dbmigrate/applydiff``` to run migrations previously not run

Create ```robymi5_dev``` for the old app

Import robymi5_dev

```mysql robymi5_dev < etc/database/robymi5_dev.sql```

# Configure and fire up local dev environment

Configure ```www/application/config/application``` to work locally.  Use 0.0.0.0/800X urls if planning to run via PHP server

```mkdir www/application/cache``` to create Kohana cache dir (assure that it is writable by your web server)

If running via PHP server: ```php bin/start-server.php```

If running via other web server: configure web server and start web server as required

## Visit URLs

### If using PHP server:

http://0.0.0.0:8000/ new app

http://0.0.0.0:8001/ new app assets

http://0.0.0.0:8002/ old app

http://0.0.0.0:8002/admin old admin

http://0.0.0.0:8003/ old patient app

### If using other web server:

Whatever URLs you set in www/application/config/application.php

# JS files

Please check style before commit

INSTALL:

[gjslint](https://developers.google.com/closure/utilities/docs/linter_howto)

```bin/check-js-style.sh```

# CSS files

master.css is generated from master.less

Compile css with less app and copy to master.css

# TESTING

Use the phpunit.sh from bin

```bin/phpunit.sh```

# REQUIRE JS

This project use [requirejs](http://requirejs.org) with jquery integrated
in environments different to production we need use all the files defined
in main.

We require to have [node.js](http://nodejs.org) installed.

For production we need to optimize first:

```bin/optimize-js.sh```

This will generate one js/built/main.js with all the dependencies
that is going to be served when the app is in Production mode
we should do this before pushing to production, adding all files
to repo before deploying.

