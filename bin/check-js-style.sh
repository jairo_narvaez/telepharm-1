#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
DIR="$( dirname "$SOURCE" )"
JS_APP="$DIR/../static/js/app"

gjslint -r $JS_APP --strict --nojsdoc   
