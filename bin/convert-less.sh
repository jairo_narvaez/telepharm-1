#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
DIR="$( dirname "$SOURCE" )"

watch-lessc -i $DIR/../static/css/master.less -o $DIR/../static/css/master.css
