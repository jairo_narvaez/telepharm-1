#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
DIR="$( dirname "$SOURCE" )"

cd $DIR/../www/
php index.php --uri=dbmigrate/$1
