#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
DIR="$( dirname "$SOURCE" )"

cd $DIR/../www/

php -dzend.enable_gc=1 -dmemory_limit=-1 index.php --uri=cli/medispan/import_image_files
