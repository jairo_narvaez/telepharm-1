#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
DIR="$( dirname "$SOURCE" )"

cd $DIR/../www/

DUMP_FILE=/tmp/dump_telepharm.sql

# create the db structure file (from default db)
php index.php --uri=cli/database/dump_structure > $DUMP_FILE
# load the db structure file (to testing db)
echo "$DUMP_FILE" | php index.php --uri=cli/database/load_structure
rm $DUMP_FILE
