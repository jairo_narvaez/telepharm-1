<?php
# as provided by matthew

// Update Release Information
$release = @include __DIR__.'/../www/application/config/release.php';
$release['build']++;
$release['dt'] = gmdate('Y-m-d H:i:s');

$fp = fopen(__DIR__.'/../www/application/config/release.php', 'w');
fwrite($fp, "<?php return ");
fwrite($fp, var_export($release, true));
fwrite($fp, "; ");
fclose($fp);
