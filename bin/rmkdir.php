<?php
	$dirs = array_merge(range('0','9'), range('a','f'));

   if ($argc < 2)
   {
     echo "The depth of directories to create must be specified as the first argument.\n\n";
     exit;
   }

	rmkdir((int) $argv[1]);

	function rmkdir($depth)
	{
		global $dirs;

		if (!$depth--) return;

		foreach ($dirs as $c)
		{
			mkdir($c);
			chdir($c);
			rmkdir($depth);
			chdir('..');
		}
	}
?>
