<?php
if (version_compare(PHP_VERSION, '5.4.0') < 0)
  die("Requires PHP 5.4\n");

# Use --bind=host to override the specified host from the config
#     e.g., --bind=0.0.0.0 to listen on all interfaces
$opts = getopt('', ['bind:']);

$path = realpath(__DIR__.'/../www/application/config/application.php');
if (!$path)
	die("The application/config/application.php file is missing. Have you copied default-config as config yet?\n");
$config = include $path;

if (!isset($config['urls']['www']) || !isset($config['urls']['assets']) || !isset($config['urls']['www-old']))
	die("urls.www, urls.www-old and urls.assets must both be configured\n");

function get_host_and_port($url)
{
	global $opts;

	$url = array_merge(['scheme' => '', 'path' => '', 'host' => '', 'port' => 80], parse_url($url));
	if ($url['scheme'] != 'http')
		die("Invalid scheme for $url. Only http is supported.\n");

	if ($url['path'] != '/')
		die("Invalid path for $url. Only / is supported.\n");

	return [isset($opts['bind']) ? $opts['bind'] : $url['host'], $url['port']];
}

list($www_host, $www_port) = get_host_and_port($config['urls']['www']);
list($old_host, $old_port) = get_host_and_port($config['urls']['www-old']);
list($pat_host, $pat_port) = get_host_and_port($config['urls']['www-old-patient']);
list($assets_host, $assets_port) = get_host_and_port($config['urls']['assets']);

echo "Running www on $www_host:$www_port\n";
$www_pipe = popen(PHP_BINARY." -S $www_host:$www_port ".realpath(__DIR__.'/../www/index.php'), 'r');

echo "Running assets on $assets_host:$assets_port\n";
$assets_pipe = popen(PHP_BINARY." -S $assets_host:$assets_port -t ".realpath(__DIR__.'/../static'), 'r');

# OLD Apps

echo "Running old www on $old_host:$old_port\n";
$old_pipe = popen(PHP_BINARY." -S $old_host:$old_port -t ".realpath(__DIR__.'/../www-old')."/", 'r');

echo "Running old patient on $pat_host:$pat_port\n";
$pat_pipe = popen(PHP_BINARY." -S $pat_host:$pat_port -t ".realpath(__DIR__.'/../www-old/patient')."/", 'r');


