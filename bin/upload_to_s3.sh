#!/bin/bash

# First install s3cmd from http://s3tools.org/download
# tar xvfz s3cmd-1.5.0-alpha1.tar.gz
# cd s3cmd-1.5.0-alpha1
# sudo python setup.py install

# check the new names of the image directories

cd private/images

s3cmd sync original/ s3://telepharm-prescriptions/original/

s3cmd sync account/ s3://telepharm-prescriptions/account/

s3cmd sync prescription/ s3://telepharm-prescriptions/prescription/

s3cmd sync medispan/ s3://telepharm-prescriptions/medispan/

s3cmd sync signature/ s3://telepharm-prescriptions/signature/
