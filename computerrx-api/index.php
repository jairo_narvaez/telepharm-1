<?php
// Require the libraries installed using composer
require 'vendor/autoload.php';
// Configuration variables
require 'config.php';

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);

$conns = new \SplObjectStorage();
$messages = new \SplObjectStorage(); # To store incomplete messages

$socket->on('connection', function ($conn) use ($conns, $messages) {
	echo 'CONNECTION INITIALIZED' . "\n";
	$conns->attach($conn);

	$conn->on('data', function ($data) use ($conns, $messages, $conn) {
		foreach ($conns as $current) {
			if ($conn != $current) continue;
			try
			{
				$newMessage = $messages[$conn];
			}
			catch ( UnexpectedValueException $e)
			{
				$newMessage = '';
			}
			echo 'DATA RECEIVED: ' . $data . "\n";
			if (substr($data, 0, 4) == '|**|')
			{
				$newMessage = trim($data);
			}
			else
			{
				// Not start, concatenate the message
				$newMessage .= trim($data);
			}
			if (substr(trim($newMessage), -4) == '|##|')
			{
				try
				{
					$parsed = Telepharm\CRX\Parser::parse($newMessage);
					$sent = Telepharm\CRX\Send::toServer($parsed, $current->getRemoteAddress());
					$current->write('OK RECEIVED');
					$newMessage = '';
					unset($messages[$conn]);
				} catch (Exception $e)
				{
					echo 'There was a problem trying to send to Telepharm ' . $e->getMessage(). "\n";
					$current->write('RECEIVED BUT FAILED TO PROCESS');
				}
			}
			else
			{
				echo 'Waiting for the rest of the message';
			}
			$messages[$conn] = $newMessage;
		}
	});

	$conn->on('end', function () use ($conns, $conn) {
		echo 'CONNECTION FINISHED' . "\n";
		$conns->detach($conn);
	});
});

echo "Socket server listening on port ". PORT. ".\n";
echo "You can connect to it by running: telnet " . HOST . " ". PORT. "\n";

$socket->listen(PORT, HOST);
$loop->run();
