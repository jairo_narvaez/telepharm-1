<?php

namespace Telepharm\CRX;

class Parser
{
	/**
	 * @param string $data the data to be parsed
	 */
	public static function parse($data)
	{
		$fieldsInLines = preg_replace("/</", "\n<", $data);
		$result = [];
		foreach(explode("\n", $fieldsInLines) as $line)
		{
			$rawField = preg_replace("/([|][*]{2}[|])|([|]##[||])/", "", $line);
			if ($rawField == '') continue;
			$fieldParts = explode(">", $rawField);
			$fieldName = preg_replace("/</", "", $fieldParts[0]);
			$data = $fieldParts[1];
			$result[$fieldName] = $data;
		}
		return $result;
	}
}
