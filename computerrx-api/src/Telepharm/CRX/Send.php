<?php

namespace Telepharm\CRX;

use Guzzle\Http\Client;

class Send
{
	/**
	 * @param Array $data the data to be parsed
	 * @param string $localIp, the ip from where this data comes from
	 */
	public static function toServer($parsed, $localIp)
	{
		$client = new Client(TELEPHARM_SERVER);
		// Start session
		$request = $client->post('api/crx/start_session');
		if (TELEPHARM_SERVER_USER != '')
		{
			$request->setAuth(TELEPHARM_SERVER_USER, TELEPHARM_SERVER_PASS);
		}
		$request->addPostFields(['local_ip' => $localIp]);
		$session = $request->send()->json();

		// Create the prescription
		$request = $client->post('api/crx/create');
		if (TELEPHARM_SERVER_USER != '')
		{
			$request->setAuth(TELEPHARM_SERVER_USER, TELEPHARM_SERVER_PASS);
		}
		$request->addPostFields($parsed);
		$request->addPostFields(['session_id' => $session['session_id']]);
		$prescriptionResponse = $request->send();

		// End session
		$request = $client->post('api/crx/end_session');
		if (TELEPHARM_SERVER_USER != '')
		{
			$request->setAuth(TELEPHARM_SERVER_USER, TELEPHARM_SERVER_PASS);
		}
		$request->addPostFields(['session_id' => $session['session_id']]);
		$session = $request->send()->json();
		
		return $prescriptionResponse->json();
	}
}
