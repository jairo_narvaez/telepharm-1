<?php 

require __DIR__ . '/../vendor/autoload.php';

define('FIXTURES_DIR', __DIR__ . '/fixtures');

class TelepharmCRXParserTest extends PHPUnit_Framework_TestCase
{

	public function commands()
	{
		return [
			[
				file_get_contents(FIXTURES_DIR . '/crx-fill-1.txt'),
				[
				'COMMAND' => 'FILL',
				'COM_ID' => '022621542048',
				'SCRIPT_NUM' => '600008',
				'CANCEL_FILL' => 'NO',
				'ORDER_CONTROL' => 'FILL',
				'FILL_DATE' => '02/26/2013',
				'FILL_NUM' => '00',
				'BARCODE' => '060000800',
				'ORIGINAL_DATE' => '02/26/2013',
				'NDC' => '57394-0270-52',
				'QUANTITY' => '10.00',
				'DRUG_NAME' => 'ACETOMINOPHEN 500MCAPLET',
				'UOM' => 'EA',
				'MFG' => '',
				'GENERIC_NAME' => '',
				'DRUG_EXP' => 'Use By:02/26/2014',
				'DAYS_SUPPLY' => 'DS:3',
				'TOTAL_QTY_REMAIN' => '10.00',
				'DAW' => '0',
				'PHYSICIAN' => 'HERNDON, MIKE',
				'PAT_ID' => '2',
				'PATIENT' => 'GREENFIELD, ALICE',
				'PAT_STREET' => '123 NORTH STREET',
				'PAT_CITY' => 'OKLAHOMA CITY',
				'PAT_ST' => 'OK',
				'PAT_ZIP' => '73142',
				'PAT_PHONE' => '(555) 555-5555',
				'PAT_PHONE2' => '(  )   -    ',
				'PAT_DOB' => '01/15/1964',
				'NHOME' => 'WINDWARD TERRAC',
				'SIG' => 'FOUR TIMES DAILY ',
				'LANG' => 'E',
				'RPH' => 'MA',
				'RPH_NAME' => 'Demo Pharmacist',
				'NUM_REFILLS' => '1',
				'EXP_DATE' => '02/26/2014',
				'PAYMENT_METHOD' => 'CASH',
				'RETAIL_PRICE' => '$    5.59',
				'REFILL_INFO' => '1.0 Refill(s) by 02/26/2014',
				'SAFTY_CAP' => 'CRC',
				'PRIORITY_GROUP' => '1',
				'TIME_STAMP' => '20130226215420',
				'CHK_ID' => '022621542048',
				]
			],
			[
				file_get_contents(FIXTURES_DIR . '/crx-fill-2.txt'),
				[
				'COMMAND' => 'FILL',
				'COM_ID' => '022622082646',
				'SCRIPT_NUM' => '600006',
				'CANCEL_FILL' => 'NO',
				'ORDER_CONTROL' => 'FILL',
				'FILL_DATE' => '02/26/2013',
				'FILL_NUM' => '00',
				'BARCODE' => '060000600',
				'ORIGINAL_DATE' => '02/26/2013',
				'NDC' => '51927-1888-00',
				'QUANTITY' => '120.00',
				'DRUG_NAME' => 'ACETONE NF',
				'UOM' => 'ML',
				'MFG' => 'PCCA',
				'GENERIC_NAME' => 'Acetone Soln                ',
				'DRUG_EXP' => '03/26/2013',
				'DAYS_SUPPLY' => 'DS:12',
				'TOTAL_QTY_REMAIN' => '0.00',
				'DAW' => '3',
				'PHYSICIAN' => 'JONES, FRED',
				'PAT_ID' => '1',
				'PATIENT' => 'SMITH, BOB',
				'PAT_STREET' => '11825 S PORTLAND',
				'PAT_CITY' => 'OKLAHOMA CITY',
				'PAT_ST' => 'OK',
				'PAT_ZIP' => '73170',
				'PAT_PHONE' => '(405) 799-5282',
				'PAT_PHONE2' => '(405) 799-7118',
				'PAT_DOB' => '06/01/1955',
				'SIG' => 'FOUR TIMES DAILY ',
				'LANG' => 'E',
				'RPH' => 'MA',
				'RPH_NAME' => 'Demo Pharmacist',
				'NUM_REFILLS' => '0',
				'EXP_DATE' => '02/26/2014',
				'PAYMENT_METHOD' => 'CASH',
				'RETAIL_PRICE' => '$   15.99',
				'REFILL_INFO' => '  ',
				'SAFTY_CAP' => 'CRC',
				'PRIORITY_GROUP' => '1',
				'TIME_STAMP' => '20130226220826',
				'CHK_ID' => '022622082646',
				]
				
			],
			[
				file_get_contents(FIXTURES_DIR . '/crx-fill-3.txt'),
				[
				'COMMAND' => 'FILL',
				'COM_ID' => '022622085807',
				'SCRIPT_NUM' => '600007',
				'CANCEL_FILL' => 'NO',
				'ORDER_CONTROL' => 'FILL',
				'FILL_DATE' => '02/26/2013',
				'FILL_NUM' => '00',
				'BARCODE' => '060000700',
				'ORIGINAL_DATE' => '02/26/2013',
				'NDC' => '57394-0270-52',
				'QUANTITY' => '10.00',
				'DRUG_NAME' => 'ACETOMINOPHEN 500MCAPLET',
				'UOM' => 'EA',
				'MFG' => '',
				'GENERIC_NAME' => '',
				'DRUG_EXP' => 'USE BY:02/26/2014',
				'DAYS_SUPPLY' => 'DS:3',
				'TOTAL_QTY_REMAIN' => '20.00',
				'DAW' => '1',
				'PHYSICIAN' => 'HERNDON, MIKE',
				'PAT_ID' => '2',
				'PATIENT' => 'GREENFIELD, ALICE',
				'PAT_STREET' => '123 NORTH STREET',
				'PAT_CITY' => 'OKLAHOMA CITY',
				'PAT_ST' => 'OK',
				'PAT_ZIP' => '73142',
				'PAT_PHONE' => '(555) 555-5555',
				'PAT_PHONE2' => '(  )   -    ',
				'PAT_DOB' => '01/15/1964',
				'NHOME' => 'WINDWARD TERRAC',
				'SIG' => 'THREE TIMES DAILY ',
				'LANG' => 'E',
				'RPH' => 'MA',
				'RPH_NAME' => 'Demo Pharmacist',
				'NUM_REFILLS' => '2',
				'EXP_DATE' => '02/26/2014',
				'PAYMENT_METHOD' => 'CASH',
				'RETAIL_PRICE' => '$    5.59',
				'AUX_MSG' => 'SeePharmacist',
				'REFILL_INFO' => '2.0 Refill(s) by 02/26/2014',
				'SAFTY_CAP' => 'CRC',
				'PRIORITY_GROUP' => '1',
				'TIME_STAMP' => '20130226220858',
				'CHK_ID' => '022622085807',
				]
			],
		];
	}

	/**
	 * @dataProvider commands
	 */
	public function testParseCommand($command, $result)
	{
		$parsed = Telepharm\CRX\Parser::parse($command);
		$this->assertEquals($result, $parsed);
	}
}
