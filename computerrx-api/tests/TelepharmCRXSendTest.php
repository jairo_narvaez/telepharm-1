<?php 

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config.php';

define('FIXTURES_DIR', __DIR__ . '/fixtures');

class TelepharmCRXSendTest extends PHPUnit_Framework_TestCase
{
	public function testSendParsedDataToServer()
	{
		$data = file_get_contents(FIXTURES_DIR . '/crx-fill-1.txt');
		$parsedData = Telepharm\CRX\Parser::parse($data);
		$sent = Telepharm\CRX\Send::toServer($parsedData, '127.0.0.1');
		$this->assertNotNull($sent['prescription_id']);
	}
}
