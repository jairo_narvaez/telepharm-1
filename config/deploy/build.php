<?php
/**
 * Manipulates the release.php file 
 * 
 * Increase version number and update datetime
 * every time is called
 *
 */
class Build {
	/**
	 * main function of the class that do
	 * all the work
	 * 
	 * @return boolean
	 */
	public static function increase($file)
	{
		$bn = new self();
		$realFile = $bn->checkConfigFile($file);
		$data = $bn->load($realFile);
		$data['build'] = intVal($data['build']) + 1;
		$data['dt'] = gmdate('Y-m-d H:i:s');
		return $bn->save($realFile, $data);
	}

	/**
	 * load the contents of the config file
	 * 
	 * @param string $file the path of the file to read
	 * 
	 * @return the evaluation of the file
	 */
	protected function load($file)
	{
		return include $file;
	}

	/**
	 * check if file exists if not create it with default values
	 * 
	 * @param string $file the path of the file
	 * 
	 * @return string the real file path already created
	 */
	protected function checkConfigFile($file)
	{
		$filePath = realpath($file);
		if (! $filePath)
		{
			// create the file with defaults
			touch($file);
			if (! is_writable($file))
			{
				throw new Exception("File is not writeable");
			}

			$defaults = array(
				'build' => 0,
				'dt' => gmdate('Y-m-d H:i:s'),
			);

			$this->save($file, $defaults);

			return realpath($file);
		}
		return $filePath;
	}

	/**
	 * Save a php file with the content of an array
	 * 
	 * @param string $file path of the file
	 * @param array $data the data to be saved as php code
	 * 
	 * @return boolean
	 */
	protected function save($file, $data)
	{
		$fHandle = fopen($file, 'w');
		$written = fwrite($fHandle, "<?php \nreturn ".var_export($data, true). ";" );
		fclose($fHandle);
		return $written !== FALSE;
	}
}
