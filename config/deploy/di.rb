namespace :di do
  desc "Clear js/cache directory on kohana-di projects"
  task :cleanup_cache do
    run "rm -f #{current_path}/static/js/cache/* "
  end

  desc "Update build number on shared/config/release.php"
  task :increase_build_number do
    run "php #{current_path}/config/deploy/increase.php #{shared_path}/config/release.php"
  end
end

# after "deploy:update_code", "di:cleanup_cache"
after "deploy:update_code", "di:increase_build_number"

