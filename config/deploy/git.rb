namespace :deploy do
  desc "Deploy the app with git style"
  task :default do
    update
    restart
    cleanup
  end

  desc "Setup your git-based deployment app"
  task :setup, :except => { :no_release => true } do
    run "mkdir -p #{current_path}"
    run "git clone #{repository} #{current_path}"
    run "cd #{current_path}; git fetch origin; git reset --hard #{branch}"
#    run "cd #{current_path}; git submodule init ; git submodule update"
#    Telepharm is not using submodules
  end

  task :cold do
    update
  end

  task :update do
    transaction do
      update_code
    end
  end

  desc "Update the deployed code."
  task :update_code, :except => { :no_release => true } do
    run "cd #{current_path}; git fetch origin; git reset --hard #{branch}"
#    run "cd #{current_path}; git submodule init ; git submodule update"
#    Telepharm is not using submodules
  end
  
  desc  "Cleanup"
  task :cleanup, :except => {:no_release => true} do
    # don nothing
  end

  desc "Rollback a single commit."
  namespace :rollback do
    task :default, :except => { :no_release => true } do
       set :branch, "HEAD^"
       deploy.default
    end
  end

  task :finalize_update, :except => { :no_release => true } do
    sudo "chmod -R g+w #{latest_release}; chown -R #{user}:#{group} #{latest_release}" if fetch(:group_writable, true)
    run "cd #{current_path}; git clean -df"
  end

  namespace :git do
    desc "Ask for a tag, if exists confirm to overwrite, if not confirmed or don't exist create a new tag"
    task :create_deploy_tag do
      if tag_repo
        while true do
          set(:deploy_tag) do
            Capistrano::CLI.ui.ask("What is the name of the tag for this deploy, default(YYYYMMDD) ?") do |q|
              q.default = "#{Date.today.strftime('%Y%m%d')}"
            end.gsub(/\s/, '-')
          end
          # all this git commands are run locally
          existing_tag = `git tag -l #{deploy_tag}`.chomp
          unless existing_tag.empty?
            confirmed = Capistrano::CLI.ui.ask("Are you sure you want to overwrite the existing tag '#{deploy_tag}' (y/n) ? ") do |q|
              q.default = 'n'
            end
            if confirmed.downcase == 'y'
              # remove the existing
              puts `git tag -d #{deploy_tag}`
              break
            end
          else
            # the tag doesn't exist
            break
          end
        end
        # just create the tag and push
        puts `git tag #{deploy_tag}`
        puts `git push --tags`
      end
    end
  end
end

after "deploy:update_code", "deploy:git:create_deploy_tag"
