<?php

require_once __DIR__. "/build.php";

if ($argc == 2 && Build::increase($argv[1]))
{
	echo "Build number increased\n";
}
else
{
	echo "ERROR trying to increase the build number\n";
}
