set :branch, "origin/master"
set :server_name, "telepharm.com"
set :port, 22
set :deploy_to, "/srv/webapps/#{application}"
set :kohana_environment, "production"
set :tag_repo, true

role :web, "#{server_name}"  # Your HTTP server, Apache/etc
role :app, "#{server_name}"  # This may be the same as your `Web` server
role :db,  "#{server_name}", :primary => true # This is where migrations will run
