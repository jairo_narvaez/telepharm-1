set :branch, "origin/master"
set :server_name, "184.106.181.31" #d5
set :deploy_to, "/srv/webapps/#{application}"
set :port, 2202
set :tag_repo, false

role :web, "#{server_name}"  # Your HTTP server, Apache/etc
role :app, "#{server_name}"  # This may be the same as your `Web` server
role :db,  "#{server_name}", :primary => true # This is where migrations will run
