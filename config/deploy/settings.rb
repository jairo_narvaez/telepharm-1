set :application, 'app.telepharm.com'

set :scm, :git
set :repository,  "git@bitbucket.org:digintent/telepharm.git"
set :deploy_to, "/srv/webapps/#{application}"

set :ssh_options, {:forward_agent => true }
set :default_run_options, {:pty => true, :shell => 'bash' }
set :scm_verbose, true
set :deploy_via, :remote_cache

set :user, "deployer"
set :group, "www-data"
set :use_sudo, false

set(:latest_release)  { fetch(:current_path) }
set(:release_path)    { fetch(:current_path) }
set(:current_release) { fetch(:current_path) }

set(:current_revision)  { capture("cd #{current_path}; git rev-parse --short HEAD").strip }
set(:latest_revision)   { capture("cd #{current_path}; git rev-parse --short HEAD").strip }
set(:previous_revision) { capture("cd #{current_path}; git rev-parse --short HEAD@{1}").strip }
