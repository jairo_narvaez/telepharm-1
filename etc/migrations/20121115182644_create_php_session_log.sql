CREATE TABLE `php_session_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `php_session_id` varchar(255) NOT NULL DEFAULT '',
  `before_data` text,
  `after_data` text,
  `created_dt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
