CREATE TABLE `session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `php_session_id` varchar(255) NOT NULL,
  `ip_address` int(10) unsigned NOT NULL,
  `user_agent` text,
  `referrer` text,
  `created_dt` datetime NOT NULL,
  `updated_dt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
