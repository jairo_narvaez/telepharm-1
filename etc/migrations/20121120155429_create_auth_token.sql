-- Please add the migration SQL --
CREATE TABLE `auth_token` (
  `id` char(22) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` varchar(50) NOT NULL,
  `account_id` int(10) unsigned DEFAULT NULL,
  `details` text NOT NULL,
  `created_dt` datetime NOT NULL,
  `expires_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
