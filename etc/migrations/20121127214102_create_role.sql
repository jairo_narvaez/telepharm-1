CREATE  TABLE `role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `type` ENUM('admin', 'company', 'store') NOT NULL ,
  `name` VARCHAR(25) NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `role` (`type`, `name`, `created_dt`)
VALUES
('admin', 'Site Administrator', NOW()),
('company', 'Company Administrator', NOW()),
('store', 'Pharmacist', NOW()),
('store', 'Technician', NOW());
