CREATE  TABLE `image` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL COMMENT 'file name' ,
  `sha1` CHAR(40) NULL ,
  `type` VARCHAR(4) NULL COMMENT 'jpg/png' ,
  `size` INT(10) UNSIGNED NULL ,
  `width` SMALLINT(5) UNSIGNED NULL ,
  `height` SMALLINT(5) UNSIGNED NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB DEFAULT CHARSET=utf8;
