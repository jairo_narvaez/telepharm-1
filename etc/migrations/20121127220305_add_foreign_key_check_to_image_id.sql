ALTER TABLE `account` ADD INDEX `fk_image_idx` (`image_id` ASC);

ALTER TABLE `account` ADD CONSTRAINT `fk_image_account`
    FOREIGN KEY (`image_id` )
    REFERENCES `image` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION; 
