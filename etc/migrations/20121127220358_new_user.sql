INSERT INTO `account` (`username`, `password`, `email`, `first_name`, `last_name`, `image_id`, `timezone`, `login_dt`, `created_dt`, `updated_dt`)
VALUES
	('admin', '$2a$11$0LBL2DmAlDnwYBCu1ga5Ieh8BR0omxAjb2FMK3.7MjwAKTc5e07PO', 'admin@telepharm.com', 'Super', 'Admin', NULL, 'America/Chicago', NULL, NOW(), NOW());

INSERT INTO `account_role` (`account_id`, `role_id`, `created_dt`)
VALUES
	(1, 1, NOW());

