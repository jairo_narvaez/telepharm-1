CREATE  TABLE `location` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NULL ,
  `address1` VARCHAR(255) NOT NULL ,
  `address2` VARCHAR(255) NULL ,
  `city` VARCHAR(50) NOT NULL ,
  `state` CHAR(2) NOT NULL ,
  `zip` VARCHAR(10) NOT NULL ,
  `created_dt` DATETIME NULL ,
  `updated_dt` DATETIME NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB DEFAULT CHARSET=utf8;
