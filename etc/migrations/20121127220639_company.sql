CREATE TABLE `company` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  `location_id` INT(10) UNSIGNED NULL ,
  `phone` VARCHAR(20) NULL ,
  `size` SMALLINT(5) UNSIGNED NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_location_idx` (`location_id` ASC) ,
  CONSTRAINT `fk_location_company`
    FOREIGN KEY (`location_id` )
    REFERENCES `location` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
