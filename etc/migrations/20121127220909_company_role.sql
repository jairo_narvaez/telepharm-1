CREATE  TABLE `company_role` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `company_id` INT(10) UNSIGNED NOT NULL ,
  `account_id` INT(10) UNSIGNED NOT NULL ,
  `role_id` INT(10) UNSIGNED NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_company_idx` (`company_id` ASC) ,
  INDEX `fk_account_idx` (`account_id` ASC) ,
  INDEX `fk_role_idx` (`role_id` ASC) ,
  CONSTRAINT `fk_company_role_company`
    FOREIGN KEY (`company_id` )
    REFERENCES `company` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_role_account`
    FOREIGN KEY (`account_id` )
    REFERENCES `account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_role_role`
    FOREIGN KEY (`role_id` )
    REFERENCES `role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
