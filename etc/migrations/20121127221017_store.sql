CREATE  TABLE `store` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `company_id` INT(10) UNSIGNED NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  `contact` VARCHAR(255) NULL ,
  `location_id` INT(10) UNSIGNED NULL ,
  `phone` VARCHAR(20) NULL ,
  `url` VARCHAR(255) NULL ,
  `code` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_company_idx` (`company_id` ASC) ,
  INDEX `fk_location_idx` (`location_id` ASC) ,
  CONSTRAINT `fk_store_company`
    FOREIGN KEY (`company_id` )
    REFERENCES `company` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_location`
    FOREIGN KEY (`location_id` )
    REFERENCES `location` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
