CREATE  TABLE `store_role` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `store_id` INT(10) UNSIGNED NOT NULL ,
  `account_id` INT(10) UNSIGNED NOT NULL ,
  `role_id` INT(10) UNSIGNED NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_company_idx` (`store_id` ASC) ,
  INDEX `fk_account_idx` (`account_id` ASC) ,
  INDEX `fk_role_idx` (`role_id` ASC) ,
  CONSTRAINT `fk_store_role_store`
    FOREIGN KEY (`store_id` )
    REFERENCES `store` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_role_account`
    FOREIGN KEY (`account_id` )
    REFERENCES `account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_role_role`
    FOREIGN KEY (`role_id` )
    REFERENCES `role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
