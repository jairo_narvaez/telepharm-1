INSERT INTO `account` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `image_id`, `timezone`, `login_dt`, `created_dt`, `updated_dt`)
VALUES
	(2, 'pharmacist', '$2a$11$azb0kmcyZBZEecyLs830C.kb5BFTElcW8T8fnVPx8ZairAX036qrm', 'pharmacist@telepharm.com', 'Pharmacist', '.', NULL, 'America/Chicago', '2012-11-27 16:23:37', '2012-11-27 22:21:43', '2012-11-27 22:23:37'),
	(3, 'technician', '$2a$11$FiHMEpAPXRIXV1SV7x1GY.w6eC7DVIBInMiFidPJiUaTUoHGgKO3m', 'technician@telepharm.com', 'Technician', '.', NULL, 'America/Chicago', '2012-11-27 16:23:50', '2012-11-27 22:22:00', '2012-11-27 22:23:50'),
	(4, 'company', '$2a$11$/IBhzeeaOTbrcNUBL9rLnO8H/PEfMYhkrGPjk2.UlMu4mXTGtU4pK', 'company@telephar.com', 'Company', 'Admin', NULL, 'America/Chicago', '2012-11-27 16:24:02', '2012-11-27 22:23:09', '2012-11-27 22:24:02');

INSERT INTO `location` (`id`, `name`, `address1`, `address2`, `city`, `state`, `zip`, `created_dt`, `updated_dt`)
VALUES
	(1, 'Office', 'Peoria 100 St.', '', 'Chicago', 'IL', '60108', '2012-11-27 22:20:33', '2012-11-27 22:20:33'),
	(2, 'Offic', 'Peoria 100 St.', '', 'Chicago', 'IL', '60018', '2012-11-27 22:21:12', '2012-11-27 22:21:12');

INSERT INTO `company` (`id`, `name`, `description`, `location_id`, `phone`, `size`)
VALUES
	(1, 'Digital Intent', 'Digital Intent', 1, '809-019-129', 20);
	
INSERT INTO `store` (`id`, `company_id`, `name`, `description`, `contact`, `location_id`, `phone`, `url`, `code`)
VALUES
	(1, 1, 'Chicago 1', 'Chicago 1 store', 'Matt Anarde', 2, '809-019-129', 'http://www.digintent.com/store.php', 'DI');
	
INSERT INTO `company_role` (`id`, `company_id`, `account_id`, `role_id`, `created_dt`)
VALUES
	(1, 1, 4, 2, '2012-11-27 22:23:25');
	
INSERT INTO `store_role` (`id`, `store_id`, `account_id`, `role_id`, `created_dt`)
VALUES
	(1, 1, 2, 3, '2012-11-27 22:22:34'),
	(2, 1, 3, 4, '2012-11-27 22:22:42');
	
