CREATE  TABLE `prescription` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` INT(10) UNSIGNED NULL COMMENT 'for resubmit, is set to the previous prescription' ,
  `account_id` INT(10) UNSIGNED NOT NULL COMMENT 'creator id' ,
  `created_by` VARCHAR(255) NOT NULL ,
  `patient_fname` VARCHAR(255) NOT NULL ,
  `patient_lname` VARCHAR(255) NOT NULL ,
  `patient_dob` DATE NULL ,
  `number` VARCHAR(32) NOT NULL ,
  `is_refill` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 ,
  `on_hold` TINYINT(1) NOT NULL DEFAULT 0 ,
  `is_draft` TINYINT(1) NOT NULL DEFAULT 1 COMMENT 'is_draft = 1 only technician can see\\n\\ntechnician can submit, so is_draft = 0, it show up on the pharmacist queue if is_completed = 0\\n\\n' ,
  `completed` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'start in 0, not completed the process\\n\\nbecomes to 1 and the users can\\\'t edit this prescription any more' ,
  `approved_for_counsel` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'the pharmacist approved the prescription' ,
  `required_counsel` TINYINT(1) NOT NULL DEFAULT 0 ,
  `refused_counsel` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'a patient can refuse the counsel this bit is set to 1, and completed to 1' ,
  `rejected` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'something is off with the prescription, it makes rejected and completed as 1' ,
  `canceled` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'the pharmacist/tech can cancel a prescription is just set to 1 and completed to 1' ,
  `archived` TINYINT(1) NOT NULL DEFAULT 0 ,
  `priority` SMALLINT(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'just an integer to sort the queue' ,
  `created_dt` DATETIME NOT NULL ,
  `updated_dt` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_account_idx` (`account_id` ASC) ,
  INDEX `fk_prescription_idx` (`parent_id` ASC) ,
  CONSTRAINT `fk_prescription_account`
    FOREIGN KEY (`account_id` )
    REFERENCES `account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prescription_prescription`
    FOREIGN KEY (`parent_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
