CREATE  TABLE `prescription_store` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_id` INT(10) UNSIGNED NOT NULL ,
  `store_id` INT(10) UNSIGNED NOT NULL ,
  `company_name` VARCHAR(255) NOT NULL ,
  `store_name` VARCHAR(255) NOT NULL ,
  `store_description` TEXT NULL ,
  `store_contact` VARCHAR(255) NULL ,
  `store_phone` VARCHAR(20) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_store_prescription_idx` (`store_id` ASC) ,
  INDEX `fk_pre_store_pre_idx` (`prescription_id` ASC) ,
  CONSTRAINT `fk_prescription_store_store`
    FOREIGN KEY (`store_id` )
    REFERENCES `store` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prescription_store_prescription`
    FOREIGN KEY (`prescription_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
