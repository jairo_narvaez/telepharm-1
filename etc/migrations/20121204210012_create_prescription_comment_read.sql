CREATE  TABLE `prescription_comment_read` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_comment_id` INT(10) UNSIGNED NOT NULL ,
  `account_id` INT(10) UNSIGNED NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_prescription_comment_read_prescription_comment_idx` (`prescription_comment_id` ASC) ,
  INDEX `fk_prescription_comment_read_account_idx` (`account_id` ASC) ,
  CONSTRAINT `fk_prescriptin_comment_read_prescription_comment`
    FOREIGN KEY (`prescription_comment_id` )
    REFERENCES `prescription_comment` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prescriptin_comment_read_account`
    FOREIGN KEY (`account_id` )
    REFERENCES `account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
