CREATE  TABLE `prescription_log` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_id` INT(10) UNSIGNED NOT NULL ,
  `account_id` INT(10) UNSIGNED NOT NULL ,
  `created_by` VARCHAR(255) NOT NULL COMMENT 'account.first_name + account.last_name for the record' ,
  `action` VARCHAR(255) NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_prescription_log_prescription_idx` (`prescription_id` ASC) ,
  INDEX `fk_prescription_log_account_idx` (`account_id` ASC) ,
  CONSTRAINT `fk_prescription_log_prescription`
    FOREIGN KEY (`prescription_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prescription_log_account`
    FOREIGN KEY (`account_id` )
    REFERENCES `account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
