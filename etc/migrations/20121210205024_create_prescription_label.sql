CREATE  TABLE `prescription_label` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_id` INT(10) UNSIGNED NOT NULL ,
  `account_id` INT(10) UNSIGNED NOT NULL ,
  `label` VARCHAR(64) NOT NULL ,
  `color` VARCHAR(6) NOT NULL DEFAULT 'white',
  `display_order` INT(5) UNSIGNED NOT NULL DEFAULT 0,
  `created_by` VARCHAR(45) NULL ,
  `created_dt` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `prescription_label_prescription_idx` (`prescription_id` ASC) ,
  INDEX `prescription_label_account_idx` (`account_id` ASC) ,
  CONSTRAINT `prescription_label_prescription`
    FOREIGN KEY (`prescription_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `prescription_label_account`
    FOREIGN KEY (`account_id` )
    REFERENCES `account` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB 
DEFAULT CHARACTER SET = utf8;
