CREATE  TABLE IF NOT EXISTS `prescription_drug` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_id` INT(10) UNSIGNED NOT NULL ,
  `new_ndc` VARCHAR(155) NULL ,
  `old_ndc` VARCHAR(155) NULL ,
  `manf` TEXT NULL ,
  `description` TEXT NULL ,
  `package_size` VARCHAR(155) NULL ,
  `strength` VARCHAR(155) NULL ,
  `created_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_prescription_drug_idx` (`prescription_id` ASC) ,
  CONSTRAINT `fk_prescription_drug`
    FOREIGN KEY (`prescription_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;