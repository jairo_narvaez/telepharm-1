CREATE  TABLE IF NOT EXISTS `prescription_dispense` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_id` INT(10) UNSIGNED NOT NULL ,
  `qty_written` VARCHAR(45) NULL ,
  `qty_dispensed` VARCHAR(45) NULL ,
  `sig` VARCHAR(45) NULL ,
  `days_supply` VARCHAR(45) NULL ,
  `fills_owed` VARCHAR(45) NULL ,
  `refilll_auth` VARCHAR(45) NULL ,
  `created_dt` DATETIME NOT NULL ,
  `sig_coded` VARCHAR(255) NULL ,
  `sig_expanded` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_prescription_dispense_idx` (`prescription_id` ASC) ,
  CONSTRAINT `fk_prescription_dispense`
    FOREIGN KEY (`prescription_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB