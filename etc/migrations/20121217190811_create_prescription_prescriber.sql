CREATE  TABLE `prescription_prescriber` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `prescription_id` INT(10) UNSIGNED NOT NULL ,
  `prescriber_id` INT(10) UNSIGNED NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  `first_name` VARCHAR(255) NOT NULL ,
  `last_name` VARCHAR(255) NOT NULL ,
  `phone` VARCHAR(45) NULL ,
  `npi` VARCHAR(100) NULL ,
  `identifier` VARCHAR(100) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_prescription_prescriber_prescripion_idx` (`prescription_id` ASC) ,
  INDEX `fk_prescription_prescriber_prescriber_idx` (`prescriber_id` ASC) ,
  CONSTRAINT `fk_prescription_prescriber_prescripion`
    FOREIGN KEY (`prescription_id` )
    REFERENCES `prescription` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prescription_prescriber_prescriber`
    FOREIGN KEY (`prescriber_id` )
    REFERENCES `prescriber` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;