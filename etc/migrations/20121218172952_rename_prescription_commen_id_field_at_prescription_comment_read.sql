ALTER TABLE `prescription_comment_read` DROP FOREIGN KEY `fk_prescriptin_comment_read_prescription_comment`;

ALTER TABLE `prescription_comment_read` DROP INDEX `fk_prescription_comment_read_prescription_comment_idx`;

ALTER TABLE `prescription_comment_read` CHANGE `prescription_comment_id` `prescription_comment_id` INT(10)  UNSIGNED  NOT NULL;

ALTER TABLE `prescription_comment_read` ADD INDEX `fk_prescription_comment_read_prescription_comment_idx` (`prescription_comment_id`);

ALTER TABLE `prescription_comment_read` ADD CONSTRAINT `fx_prescription_comment_read_prescription_comment` FOREIGN KEY (`prescription_comment_id`) REFERENCES `prescription_comment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
