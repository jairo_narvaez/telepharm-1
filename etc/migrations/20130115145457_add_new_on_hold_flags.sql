ALTER TABLE `prescription` MODIFY COLUMN `completed` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'start in 0, not completed the process\\n\\nbecomes to 1 and the users can\\\'t edit this prescription any more' AFTER `rejected`;

ALTER TABLE `prescription` ADD `approved_on_hold` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `on_hold`;

ALTER TABLE `prescription` ADD `show` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `archived`;

UPDATE `prescription` SET `show` = 1;