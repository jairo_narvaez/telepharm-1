ALTER TABLE `prescription` ADD `is_new` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0' AFTER `number`;
ALTER TABLE `prescription` ADD `is_partial` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `is_new`;

