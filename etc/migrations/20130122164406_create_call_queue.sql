CREATE TABLE `call_queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_account_id` int(10) unsigned NOT NULL,
  `from_as_patient` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `to_account_id` int(10) unsigned DEFAULT NULL,
  `to_store_id` int(10) unsigned DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `is_pending` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `call_status` enum('accepted','canceled') DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `cancel_reason` enum('from','to','error') DEFAULT NULL,
  `complete_status` enum('incomplete','complete','error') NOT NULL DEFAULT 'incomplete',
  `created_dt` datetime NOT NULL,
  `updated_dt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
