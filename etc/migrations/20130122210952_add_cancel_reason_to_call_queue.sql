ALTER TABLE `call_queue` CHANGE `cancel_reason` `cancel_reason` ENUM('from','to','error', 'no answer')  CHARACTER SET utf8  NULL  DEFAULT NULL;
ALTER TABLE `call_queue` ADD `possible_candidates` TEXT  CHARACTER SET utf8  NULL AFTER `complete_status`;
