CREATE TABLE `notification` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `message` TEXT CHARACTER SET utf8 NOT NULL DEFAULT '',
	  `type` varchar(100) NOT NULL DEFAULT '',
	  `account_id` int(10) unsigned NOT NULL,
	  `details` text NOT NULL,
	  `created_dt` datetime NOT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
