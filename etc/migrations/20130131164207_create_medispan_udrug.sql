CREATE TABLE `medispan_udrug` (
  `unique_drug_id` char(10) NOT NULL DEFAULT '',
  `dosage_form_id` char(5) NOT NULL DEFAULT '',
  `external_drug_id` char(20) NOT NULL DEFAULT '',
  `manufacturer_id` char(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`unique_drug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
