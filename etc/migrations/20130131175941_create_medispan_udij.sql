CREATE TABLE `medispan_udij` (
  `unique_drug_id` char(10) NOT NULL DEFAULT '',
  `start_date` date NOT NULL,
  `stop_date` date DEFAULT NULL,
  `image_id` char(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`unique_drug_id`,`start_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
