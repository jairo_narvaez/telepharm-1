CREATE TABLE `medispan_dmfg` (
  `manufacturer_id` char(10) NOT NULL DEFAULT '',
  `manufacturer_name` char(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
