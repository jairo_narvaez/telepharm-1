CREATE TABLE `medispan_lb_lbl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label_id` char(6) NOT NULL DEFAULT '',
  `label` text,
  PRIMARY KEY (`id`),
  KEY `label_index` (`label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `medispan_lb_lbl_gpi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label_id` char(6) DEFAULT NULL,
  `generic_product_id` char(14) NOT NULL DEFAULT '',
  `priority` tinyint(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `label_generic_index` (`label_id`,`generic_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `medispan_mf_drug` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mf_drug_id` char(6) DEFAULT '',
  `generic_product_id` char(14) DEFAULT NULL,
  `name` char(30) DEFAULT NULL,
  `dosage_form` char(4) DEFAULT NULL,
  `strength` char(15) DEFAULT NULL,
  `strength_unit_of_measure` char(10) DEFAULT NULL,
  `bioequivalence_code` char(1) DEFAULT NULL,
  `controlled_substance_code` char(1) DEFAULT NULL,
  `efficacy_code` char(1) DEFAULT NULL,
  `legend_indicator_code` char(1) DEFAULT NULL,
  `knowledge_base_code` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drug_product_index` (`mf_drug_id`,`generic_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `medispan_mf_ndc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_drug_id` char(20) NOT NULL DEFAULT '',
  `mf_drug_id` char(6) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `drug_external_index` (`external_drug_id`,`mf_drug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;