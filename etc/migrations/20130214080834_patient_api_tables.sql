CREATE TABLE `patient_api_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(100) DEFAULT NULL,
  `store_id` int(10) unsigned DEFAULT '0',
  `is_active` tinyint(1) unsigned DEFAULT '0',
  `created_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `patient_api_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` char(100) DEFAULT NULL,
  `patient_api_account_id` int(10) unsigned DEFAULT '0',
  `prescription_id` int(10) unsigned DEFAULT '0',
  `created_dt` datetime DEFAULT NULL,
  `end_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;