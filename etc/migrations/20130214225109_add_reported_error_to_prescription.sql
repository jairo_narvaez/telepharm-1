ALTER TABLE `prescription` ADD `reported_error` TINYINT(1)  UNSIGNED  NOT NULL  DEFAULT '0'  AFTER `show`;
ALTER TABLE `prescription` ADD `error_message` TEXT  NULL  AFTER `priority`;
