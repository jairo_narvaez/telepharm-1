ALTER TABLE `prescription` ADD `date_filled` DATE  NULL  AFTER `error_message`;
ALTER TABLE `prescription` ADD `date_expires` DATE  NULL  AFTER `date_filled`;
ALTER TABLE `prescription` ADD `date_written` DATE  NULL  AFTER `date_expires`;
