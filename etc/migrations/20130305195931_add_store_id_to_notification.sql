ALTER TABLE `notification` DROP FOREIGN KEY `notification_ibfk_1`;
ALTER TABLE `notification` ADD `store_id` INT(10)  UNSIGNED  NULL  DEFAULT NULL  AFTER `account_id`;
ALTER TABLE `notification` CHANGE `account_id` `account_id` INT(10)  UNSIGNED  NULL  DEFAULT NULL;
