-- update account images
update image set kind = 'account' where id in (
		select image_id from account where image_id is not null
	);

-- update prescription images
update image set kind = 'prescription' where id in (
		select image_id from prescription_image where image_id is not null
	);

-- update signature images
update image set kind = 'signature' where id in (
		select signature_image_id from prescription where signature_image_id is not null
	);

-- update old images
update image set kind = 'prescription' where kind is null;
