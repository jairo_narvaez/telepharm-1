ALTER TABLE `patient_api_session` ADD COLUMN `call_queue_id` INT(10) NOT NULL DEFAULT '0' AFTER prescription_id;

ALTER TABLE `patient_api_session` ADD COLUMN `patient_info` TEXT AFTER `call_queue_id`;

ALTER TABLE `patient_api_session` ADD COLUMN `pharmacist_id` INT(10) NOT NULL DEFAULT '0' AFTER prescription_id;

ALTER TABLE `patient_api_session` ADD COLUMN `technician_id` INT(10) NOT NULL DEFAULT '0' AFTER prescription_id;