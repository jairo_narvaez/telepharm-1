UPDATE account
JOIN company_role ON company_role.account_id = account.id
SET account.company_id = company_role.company_id
WHERE account.company_id = 0;

UPDATE account
JOIN store_role ON store_role.account_id = account.id
JOIN store ON store.id = store_role.store_id
SET account.company_id = store.company_id
WHERE account.company_id = 0;