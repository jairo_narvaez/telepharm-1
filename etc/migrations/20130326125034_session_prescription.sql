CREATE TABLE `patient_api_session_prescription` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_api_session_id` int(10) unsigned NOT NULL DEFAULT '0',
  `prescription_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_prescription_index` (`patient_api_session_id`,`prescription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `patient_api_session_prescription`
(`patient_api_session_id`, `prescription_id`)
SELECT `id`, `prescription_id` FROM `patient_api_session`
WHERE `prescription_id` > 0;