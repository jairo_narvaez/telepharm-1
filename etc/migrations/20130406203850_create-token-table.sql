CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `created` datetime NOT NULL,
  `issued` datetime NOT NULL,
  `expires` datetime NOT NULL,
  `ipaddress` varchar(15) NOT NULL,
  `slide_minutes` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;