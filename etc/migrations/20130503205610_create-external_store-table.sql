CREATE TABLE `external_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address_id` int(11) NOT NULL,
  `created_dt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `external_store_name_index` (`name`)
) ENGINE=InnoDB;