CREATE TABLE `internal_store_external_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internal_store_id` int(11) NOT NULL,
  `external_store_id` int(11) NOT NULL,
  `created_dt` datetime NOT NULL,
  `updated_dt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `internal_store_id_index` (`internal_store_id`)
) ENGINE=InnoDB;