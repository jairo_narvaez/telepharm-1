CREATE  TABLE `internal_store_external_pharmacist` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `internal_store_id` INT UNSIGNED NOT NULL ,
  `external_pharmacist_id` INT UNSIGNED NOT NULL ,
  `created_dt` DATETIME NOT NULL ,
  `updated_dt` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `internal_store_id_index` (`internal_store_id`)
  ) ENGINE=InnoDB;