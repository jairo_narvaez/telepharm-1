CREATE TABLE `prescription_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prescription_id` int(11) NOT NULL,
  `external_pharmacist_id` int(11) NOT NULL,
  `external_store_id` int(11) NOT NULL,
  `called_in_by_person_id` int(11) NOT NULL,
  `created_dt` datetime NOT NULL,
  `updated_dt` datetime NULL,
  `initiated_by_account_id` int(11) NOT NULL,
  `completed_by_account_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `prescription_id_index` (`prescription_id`),
  KEY `external_pharmacist_id_index` (`external_pharmacist_id`)
) ENGINE = InnoDB;