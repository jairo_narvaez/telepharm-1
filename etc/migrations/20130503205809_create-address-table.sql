CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line1` varchar(128) NOT NULL,
  `line2` varchar(128) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `state_id` int(11) NOT NULL,
  `postal_code` varchar(16) NOT NULL,
  `created_dt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;