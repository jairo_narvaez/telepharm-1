<?php
// Require the libraries installed using composer
require 'vendor/autoload.php';
// Configuration variables
require 'config.php';
// Require the custom class
require 'telepharm/rxkey/query.php';


$secureApiCall = function() {
	$app = \Slim\Slim::getInstance();
	$req = $app->request();
	$res = $app->response();
	if (function_exists('getallheaders'))
	{
		$headers = getallheaders();
		if ( isset($headers['Authorization']) )
			$authorization = $headers['Authorization'];
		else
			$authorization = $headers['authorization'];
	}
	else
	{
		$authorization = $req->headers('Authorization');
	}
	// Check if the Authorization header came in the request
	// to process the query
	$token = explode(': ', $authorization);
	if (count($token) != 2)
		$app->halt(403, 'Authorization Required');
	if ($token[1] != SECURE_TOKEN)
		$app->halt(403, 'Authorization Required');
};

$jsonContentType = function() {
	// Check if request is using application/json
	$app = \Slim\Slim::getInstance();
	$req = $app->request();
	$res = $app->response();
	if ('application/json' != $req->getMediaType())
		$app->halt(405, 'Require application/json content type');
	// if it does, set the response to application/json
	$res['Content-Type'] = 'application/json';
};

$app = new \Slim\Slim([
	'log.enabled' => true,
	'debug' => false,
]);
$app->setName('RxKey API');
$log = $app->getLog();
$log->setLevel(\Slim\Log::DEBUG);

$rxKeyObj = new \Telepharm\RxKey\Query();

$app->error(function (\Exception $e) use ($app) {
	echo json_encode([
		'error' => true,
		'message' => $e->getMessage()
		]);
	die();
});

$app->get('/prescription/:number', $secureApiCall, $jsonContentType,
	function ($number) use ($rxKeyObj) {
		if ($number == '')
			throw new Exception('Parameter number is required');
		echo json_encode(['error' => false, 'result' => $rxKeyObj->getPrescription($number)]);
	});

$app->get('/patient-history/:patient_id', $secureApiCall, $jsonContentType, 
	function ($patient_id) use ($rxKeyObj)  {
		if ($patient_id == '')
			throw new Exception('Parameter patient_id is required');
		echo json_encode(['error' => false, 'result' => $rxKeyObj->getPatientHistory($patient_id)]);
	});

$app->run();
