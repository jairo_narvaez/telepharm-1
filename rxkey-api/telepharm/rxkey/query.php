<?php

namespace Telepharm\RxKey;

/**
 * Class to manage the connection to rxkey db
 * and create the queries for telepharm
 */
class Query {

	/**
	 * Get a prescription from the Rx Key database
	 * 
	 * @param string $number 
	 * 
	 * @return array
	 */
	public function getPrescription($number)
	{
		$result = $this->queryDB(
			$this->getSQLForPrescription(),
		  	[ $number ]
		);
			
		// Get the first result
		return count($result) > 0 ? $result[0] : false;
	}

	/**
	 * Get the patient history of prescriptions
	 * from the rx key database
	 * 
	 * @param string $patientId 
	 * 
	 * @return array
	 */
	public function getPatientHistory($patientId)
	{
		$result = $this->queryDB(
			$this->getSQLForPatientHistory(),
			[ $patientId ]
		);
			
		// Get all the result
		return $result;
	}

	/**
	 * Make the query to the db and return an array
	 * of associative arrays
	 * 
	 * @param string $sql 
	 * 
	 * @return array or false
	 */
	public function queryDB($sql, $params)
	{
		$connectionInfo = [
			'UID' => UID,
			'PWD' => PWD,
			'Database' => DB,
			];
		$conn = sqlsrv_connect(SERVER_NAME, $connectionInfo);
		if ( $conn === false )
			return false;

		$stmt = sqlsrv_query($conn, $sql, $params);
		if ( $stmt === false)
			return false;
		
		$results = [];

		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
		{
			$results[] = $row;
		}

		sqlsrv_free_stmt($stmt);
		sqlsrv_close($conn);

		return $results;
	}

	/**
	 * SQL for get a prescription from rxkey
	 * 
	 * @return string
	 */
	private function getSQLForPrescription()
	{
		return <<<EOF
			select 
			Customer.Identifier PatientID,
			Customer.FirstName,
			Customer.LastName,
			Customer.DateOfBirth,
			CustomerRx.DateFilled,
			CustomerRx.DateExpires,
			CustomerRX.DateFirstFill,
			CustomerRx.RefillStatus,
			CustomerRx.rxNumber,
			CustomerRx.SIGCoded,
			CustomerRx.SIGExpanded,
			CustomerRx.DaysSupply,
			CustomerRx.OriginalQuantity as QuantityOwed,
			CustomerRx.DispensedQuantity,
			CustomerRx.FillsOwed,
			Prescriber.FirstName as pfname,
			Prescriber.LastName as plname,
			Prescriber.Identifier,
			Prescriber.CellPhoneNbr,
			Prescriber.NpiNbr,
			PrescriberAddress.PhoneNumber,
			Drug.Description,
			Drug.Strength,
			Drug.packageSize,
			Drug.PackageSizeUnitCode,
			Drug.ManufacturerName,
			DrugIdentifier.Identifier as ndc
			FROM  Customer
			Inner join CustomerRx
			ON customer.CustomerId=customerRx.CustomerId
			inner join Drug
			on CustomerRx.DrugId=Drug.DrugId
			inner join Prescriber
			on CustomerRx.PrescriberId=Prescriber.PrescriberId
			inner join DrugIdentifier
			on Drug.DrugId=DrugIdentifier.DrugId
			inner join PrescriberAddress
			on Prescriber.PrescriberId=PrescriberAddress.PrescriberID
			where PrescriberAddress.IsPrimary = 1 
			AND RxNumber = ?
EOF;
	}

	/**
	 * SQL for get a patient history
	 * 
	 * @return string
	 */
	private function getSQLForPatientHistory()
	{
		return <<<EOF
			select 
			Customer.Identifier PatientID,
			Customer.FirstName,
			Customer.LastName,
			Customer.DateOfBirth,
			CustomerRx.DateFilled,
			CustomerRx.DateExpires,
			CustomerRX.DateFirstFill,
			CustomerRx.RefillStatus,
			CustomerRx.rxNumber,
			CustomerRx.SIGCoded,
			CustomerRx.SIGExpanded,
			CustomerRx.DaysSupply,
			CustomerRx.OriginalQuantity as QuantityOwed,
			CustomerRx.DispensedQuantity,
			CustomerRx.FillsOwed,
			Prescriber.FirstName as pfname,
			Prescriber.LastName as plname,
			Prescriber.Identifier,
			Prescriber.CellPhoneNbr,
			Prescriber.NpiNbr,
			PrescriberAddress.PhoneNumber,
			Drug.Description,
			Drug.Strength,
			Drug.packageSize,
			Drug.PackageSizeUnitCode,
			Drug.ManufacturerName,
			DrugIdentifier.Identifier as ndc
			FROM  Customer
			Inner join CustomerRx
			ON customer.CustomerId=customerRx.CustomerId
			inner join Drug
			on CustomerRx.DrugId=Drug.DrugId
			inner join Prescriber
			on CustomerRx.PrescriberId=Prescriber.PrescriberId
			inner join DrugIdentifier
			on Drug.DrugId=DrugIdentifier.DrugId
			inner join PrescriberAddress
			on Prescriber.PrescriberId=PrescriberAddress.PrescriberID
			where PrescriberAddress.IsPrimary = 1 
			AND Customer.Identifier = ?
			AND DATEDIFF ( dd , CustomerRx.DateFilled , GETDATE() ) < 180
EOF;
	}
}
