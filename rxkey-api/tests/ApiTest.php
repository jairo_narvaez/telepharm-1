<?php
require '../vendor/autoload.php';

DEFINE('CLIENT_SECURE_TOKEN', 'd1c2c20a2029dfedb429e3a588479706');

use Guzzle\Http\Client;

class ApiTest extends PHPUnit_Framework_TestCase
{

	function testAuthorization()
	{
		$client = new Client('http://0.0.0.0:8090/');
		$request = $client->get('prescription/700900');
		$request->setHeader('Content-Type', 'application/json');
		try {
			$response = $request->send();
			$this->assertTrue(false, 'Request should raise an exception');
		} catch (Exception $e)
		{
			$this->assertContains('403', $e->getMessage());
		}
	}

	function testInvalidTokenAuthorization()
	{
		$client = new Client('http://0.0.0.0:8090/');
		$request = $client->get('prescription/700900');
		$request->setHeader('Content-Type', 'application/json');
		$request->setHeader('Authorization', 'Token: INVALID-' . CLIENT_SECURE_TOKEN);
		try {
			$response = $request->send();
			$this->assertTrue(false, 'Request should raise an exception');
		} catch (Exception $e)
		{
			$this->assertContains('403', $e->getMessage());
		}
	}


	function testContentType()
	{
		$client = new Client('http://0.0.0.0:8090/');
		$request = $client->get('prescription/700900');
		$request->setHeader('Authorization', 'Token: ' . CLIENT_SECURE_TOKEN);
		try {
			$response = $request->send();
			$this->assertTrue(false, 'Request should raise an exception');
		} catch (Exception $e)
		{
			$this->assertContains('405', $e->getMessage());
		}
	}

	function testGetPrescription()
	{
		$this->markTestSkipped('need sql server to run this tests');
		$client = new Client('http://0.0.0.0:8090/');
		$request = $client->get('prescription/700900');
		$request->setHeader('Authorization', 'Token: ' . CLIENT_SECURE_TOKEN);
		$request->setHeader('Content-Type', 'application/json');
		$response = $request->send();
		$this->assertEquals(200, $response->getStatusCode());
		echo(var_export($response->getBody(),1));
		$data = $response->json();
		echo(var_export($data,1));
	}

	function testGetPatientHistory()
	{
		$this->markTestSkipped('need sql server to run this tests');
		$client = new Client('http://0.0.0.0:8090/');
		$request = $client->get('patient-history/12345');
		$request->setHeader('Authorization', 'Token: ' . CLIENT_SECURE_TOKEN);
		$request->setHeader('Content-Type', 'application/json');
		$response = $request->send();
		$this->assertEquals(200, $response->getStatusCode());
		echo(var_export($response->getBody(),1));
		$data = $response->json();
		echo(var_export($data,1));
	}
}
