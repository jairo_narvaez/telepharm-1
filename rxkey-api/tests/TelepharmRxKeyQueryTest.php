<?php
require '../vendor/autoload.php';
require '../telepharm/rxkey/query.php';

use \Telepharm\RxKey\Query;

class TelepharmRxKeyQueryTest extends PHPUnit_Framework_TestCase
{
	protected $q = null;

	public function setUp()
	{
		parent::setUp();
		$this->q = new Query();
	}

	public function testGetPrescription()
	{
		$r = $this->q->getPrescription(1);
		$this->assertNotNull($r);
		$this->assertArrayHasKey('Firstname', $r);
		$this->assertArrayHasKey('lastname', $r);
		$this->assertArrayHasKey('dob', $r);
		$this->assertArrayHasKey('rx', $r);
		$this->assertArrayHasKey('SigCoded', $r);
		$this->assertArrayHasKey('Sigexpanded', $r);
		$this->assertArrayHasKey('refill', $r);
		$this->assertArrayHasKey('DaysSupply', $r);
		$this->assertArrayHasKey('qtywritten', $r);
		$this->assertArrayHasKey('DispensedQuantity', $r);
		$this->assertArrayHasKey('FillsOwed', $r);
		$this->assertArrayHasKey('identifier', $r);
		$this->assertArrayHasKey('plname', $r);
		$this->assertArrayHasKey('pfname', $r);
		$this->assertArrayHasKey('npi', $r);
		$this->assertArrayHasKey('phone', $r);
		$this->assertArrayHasKey('newndc', $r);
		$this->assertArrayHasKey('manf', $r);
		$this->assertArrayHasKey('description', $r);
		$this->assertArrayHasKey('packagesize', $r);
		$this->assertArrayHasKey('Strength', $r);
		$this->assertArrayHasKey('PackageSizeUnitCode', $r);
	}

	public function testGetPatientHistory()
	{
		$r = $this->q->getPatientHistory(1);
		$this->assertNotNull($r);
	}
}
