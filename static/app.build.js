({
    appDir   : "js",
    baseUrl  : "lib",
    paths: {
        "jquery": "empty:",
        "app" : "../app"
    },
    dir      : "js-built",
    optimize : "uglify",
    //If using UglifyJS for script optimization, these config options can be
    //used to pass configuration values to UglifyJS.
    //See https://github.com/mishoo/UglifyJS for the possible values.
    uglify: {
      toplevel        : true,
      ascii_only      : true,
      beautify        : false,
      max_line_length : 1000,
      no_mangle       : false
    },

    modules: [
        //Now set up a build layer for each page, but exclude
        //the common one. "exclude" will exclude nested
        //the nested, built dependencies from "common". Any
        //"exclude" that includes built modules should be
        //listed before the build layer that wants to exclude it.
        //"include" the appropriate "app/main*" module since by default
        //it will not get added to the build since it is loaded by a nested
        //require in the page*.js files.
        {
            //module names are relative to baseUrl/paths config
            name: "../welcome_index",
            include : ['app/page_welcome_index']
        }
        ,
        {
            name: "../scaffold_index"
        }
        ,
        {
            name: "../pharmacist_index"
        }
        ,
        {
            name: "../prescription_view",
            include : ['app/page_prescription_view']
        }
        ,
        {
            name: "../prescription_create",
            include : ['app/page_prescription_create']
        }
        ,
        {
            name : '../account_update',
            include : ['app/page_account_update']
        },
        {
            name : '../account_index',
            include : ['app/page_account_index']
        }
    ]
})
