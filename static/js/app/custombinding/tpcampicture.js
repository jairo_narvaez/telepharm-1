define(function(require) {
  var ko = require('knockout');

  ko.bindingHandlers.tpCamPicture = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel,
        bindingContext) {

      viewModel = bindingContext.$parent;
      var parent = $(element).parent();
      var onItemClick = function() {
        page.wizard.selectedImage(valueAccessor().displayOrder());
        page.wizard.show();
        event.preventDefault();
      };
      $(element).on('click', onItemClick);
    }
  };
});
