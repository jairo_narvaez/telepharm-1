define(function(require) {
  var ko = require('knockout');
  ko.bindingHandlers.tpCheckbox = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel,
        bindingContext) {
      var box = $(element).parent();
      var value = valueAccessor();
      var label = $('<label>', {'for': value.id}).html(value.label);
      var onCheckBoxClick = function(event) {
        var el = $(event.target);
        if (el.hasClass('checked')) {
          el.removeClass('checked');
          newVal = false;
        } else {
          el.addClass('checked');
          newVal = true;
        }
        if (typeof value.checked != 'undefined' &&
            typeof value.checked.subscribe == 'function') {
          value.checked(newVal);
        }
        else {
          if (newVal)
            $(element).attr('checked', 'checked');
          else
            $(element).removeAttr('checked');
        }
      };
      box.prepend(label);
      var clickableSpan = $('<span>', {'class': value.className});

      if (! value.disabled)
      {
        clickableSpan.on('click',
            onCheckBoxClick);
        label.on('click',
            onCheckBoxClick);
      }

      box.prepend(clickableSpan);

      if (typeof value.checked != 'undefined' &&
          typeof value.checked.subscribe == 'function') {
        var updateCheckbox = function(newVal) {
          if (newVal) {
            clickableSpan.addClass('checked');
            label.addClass('checked');
          }
          else {
            clickableSpan.removeClass('checked');
            label.removeClass('checked');
          }
        };
        value.checked.subscribe(updateCheckbox);
        updateCheckbox(value.checked());
      }

    }
  };
});
