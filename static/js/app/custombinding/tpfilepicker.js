define(function(require) {
  var ko = require('knockout');
  var PrescriptionImg = require('app/model/prescriptionimage');

  ko.bindingHandlers.tpFilePicker = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel,
        bindingContext) {
      var parent = $(element).parent();
      var action = $.browser.msie ? $(element).attr('data-action-ie') : $(element).attr('data-action');
      var form = $('<form>', {'enctype': 'multipart/form-data',
                   'action': action, 'method': 'post', 'style': 'display:none;'
                 });
      var inputFile = $('<input>', {'type': 'file', 'name': 'picture', 'style':'display:none'});
      form.append(inputFile);
      var addInput = function(name, value) {
        form.append($('<input>', {'type': 'hidden', 'name': name,
          'value': value}));
      };

      addInput('id', valueAccessor().id());
      addInput('picture_type', valueAccessor().type());
      addInput('image_id', valueAccessor().image_id());
      addInput('display_order', valueAccessor().displayOrder());
      addInput('prescription_id', valueAccessor().prescription_id());
      form.append($('<input>', {'type':'submit', 'value':'Submit', 'class' : 'btnSubmit', 'name':'btnSubmit'}));

      var parentModel = bindingContext.$parent;

      parent.append(form);
      $(element).on('click', function(event) {
        event.preventDefault();
        parentModel.wizard.selectImage(viewModel);
        inputFile.click();
        return false;
      });
      inputFile.on('change', function() {
        var file_name = inputFile.val();
        if (inputFile.val()) {
          if( !$.browser.msie){
            parent.addClass('uploading');
            form.ajaxSubmit({
              dataType: 'json',
              success: function(data, status, response) {
                if (parentModel.hasOwnProperty('onSuccessImageUploaded')) {
                  var prescriptionImage =
                      new PrescriptionImg(data.result.prescription_image);
                    parentModel.onSuccessImageUploaded(prescriptionImage,
                                                   parent.find('img'));
                }
              },
              error: function(data, status, response) {
                alert('Image was not able to be uploaded');
              },
              complete: function() {
                console.log("COMPLETED");
                parent.removeClass('uploading');
              }
            });
        }
        else
        {
          console.log("TEST");
          form.css({'display' : 'block'});
        }
        }
      });
    }
  };
});
