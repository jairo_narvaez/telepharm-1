define(function(require) {
  var ko = require('knockout');

  function DragDropUploader(element, onLoadEnd) {
    var self = this;

    self.uploader_place = element;
    self.reader = null;
    self.preview = null;

    self.loadEnd = function() {
      onLoadEnd(self.reader);
    };
    self.loadError = function() {
      var msg = '';
      switch (event.target.error.code) {
        case event.target.error.NOT_FOUND_ERR:
          msg = 'File not found!';
          break;
        case event.target.error.NOT_READABLE_ERR:
          msg = 'File not readable!';
          break;
        case event.target.error.ABORT_ERR:
          break;
        default:
          msg = 'Read error.';
          alert(msg);
      }
    };
    self.loadProgress = function() {};
    self.previewNow = function() {};

    self.encodeFile = function(file) {
      // Firefox 3.6, Chrome 6, WebKit
      if (window.FileReader) {
        self.reader = new FileReader();
        // Firefox 3.6, WebKit
        if (self.reader.addEventListener) {
          self.reader.addEventListener('loadend', self.loadEnd, false);
          if (status !== null)
          {
            self.reader.addEventListener('error',
                self.loadError, false);
            self.reader.addEventListener('progress',
                self.loadProgress, false);
          }
        // Chrome 7
        } else {
          self.reader.onloadend = self.loadEnd;
          if (status !== null)
          {
            self.reader.onerror = self.loadError;
            self.reader.onprogress = self.loadProgress;
          }
        }
        self.preview = new FileReader();
        // Firefox 3.6, WebKit
        if (self.preview.addEventListener) {
          self.preview.addEventListener('loadend', self.previewNow, false);
        // Chrome 7
        } else {
          self.preview.onloadend = self.previewNow;
        }

        // The function that starts reading the file as a binary string
        self.reader.readAsDataURL(file);
      // self.preview.readAsDataURL(file);
      }
    //Safari 5 does not support FileReader
      else {
        var msg = 'This feature is not supported by this browser, ' +
            'please try a different method';
        alert(msg);
      }
    };

    self.drop = function() {
      event.preventDefault();
      var dt = event.dataTransfer;
      var files = dt.files;
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        self.encodeFile(file);
      }
    };

    if( self.uploader_place.addEventListener){
      self.uploader_place.addEventListener('drop', self.drop, false);
      self.uploader_place.addEventListener('dragover', function(event) {
        event.stopPropagation();
        event.preventDefault();
      }, true);

    }

  }

  ko.bindingHandlers.tpFileUploader = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel,
                       bindingContext) {

      var parent = $(element).parent();
      viewModel = bindingContext.$parent;
      var onFileReaded = function(fileReader) {
        $(element).attr('src', fileReader.result);
        if (viewModel.hasOwnProperty('onImageSelected')) {
          viewModel.onImageSelected(valueAccessor(), fileReader.result, parent);
        }
      };
      new DragDropUploader(element, onFileReaded);
    }
  };

});
