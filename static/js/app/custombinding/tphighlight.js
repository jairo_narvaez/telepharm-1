define(function(require) {
  var ko = require('knockout');

  ko.bindingHandlers.tpHighlight = {
    update:  function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext){
      if (valueAccessor())
        $(element).effect('highlight', {}, 2000);
    }
  };
});
