define(function(require) {
  var ko = require('knockout'),
      $ = require('jquery');

  ko.bindingHandlers.tpMedispanImage = {
    init: function(element, valueAccessor, allBindingsAccessor,
      viewModel, bindingContext) {
      var pImage = valueAccessor();
      if (pImage.type() == 'drug_dispensed')
      {
        var medispanImg = $('img[data-medispan]')[0];
        if (medispanImg != undefined)
        {
          $(medispanImg).clone().insertAfter(element);
          var newImage = $(element).next();
          $(newImage).removeAttr('data-medispan').addClass('medispan');
          $(newImage).on('click', function() {
            $('#medispan-carousel').carousel();
            $('#medispan-full').modal().draggable(
            { handle: ".modal-header" }
            );
          });
        }
      }
    }
  }
});

