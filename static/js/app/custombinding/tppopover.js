define(function(require) {
  var ko = require('knockout');

  ko.bindingHandlers.tpPopOver = {
    init:  function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext){
      var container = $('#' + valueAccessor().container);
      var leftOffset = valueAccessor().leftOffset || -149;
      var topOffset = valueAccessor().topOffset || 4;
      var elementPosition = $(element).offset();
      var setButtonPosition = function(element, container){
            container.css({
            display:'block',
            left:elementPosition.left - element.width() + leftOffset,
            top: elementPosition.top + element.height() + topOffset
          });
      };

      $(element).on('click', function(event){
        if($(element).attr('data-status') === 'open'){
          $(element).attr('data-status', 'close');
          container.removeAttr('style');
        }else{
          $(element).attr('data-status', 'open');
          setButtonPosition($(element), container);
        }
        event.stopPropagation();
        return false;
      });

      $(window.document).on('click', function(event){
        if($(element).attr('data-status') === 'open'){
          $(element).attr('data-status', 'close');
          container.removeAttr('style');
        }
      });

    }
  };
});
