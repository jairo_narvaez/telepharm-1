define(function(require) {
  var ko = require('knockout');

  ko.bindingHandlers.tpTip = {
    init: function(element, valueAccessor, allBindingsAccessor,
      viewModel, bindingContext) {
        var timeout;
      $(element).mouseenter(function(){
         var self = $(this);
         self.popover({
              trigger: 'manual',
              placement: 'bottom',
              html: true,
              content: valueAccessor()
            });
         timeout = setTimeout(function(){
                self.popover('show');
            }, 1000);
      });
      $(element).mouseleave(function(){
          if (timeout){
              clearTimeout(timeout);
          }
        $(this).popover('destroy');
      });
    }
  };
});
