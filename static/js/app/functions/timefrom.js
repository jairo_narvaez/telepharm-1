define(function(require, exports, module) {
  var moment = require('moment');

  moment.fn.adjustedToServer = function(now) {
    // Correct browser date, based on server date
    var serverDate = moment(TP.serverDate, 'YYYY-MM-DD HH:mm:ss Z');
    var clientDate = moment(TP.clientDate);
    var differenceServerClient = clientDate.diff(serverDate);
    if (differenceServerClient < 0)
      this.add('ms', Math.abs(differenceServerClient));
    else
      this.subtract('ms', Math.abs(differenceServerClient));
    return this;
  };

  moment.fn.timeFrom = function(now) {
    var ms = now.diff(this);
    var hours = Math.floor(ms / (1000 * 60 * 60));
    ms = ms % (1000 * 60 * 60);
    var minutes = Math.floor(ms / (1000 * 60));
    ms = ms % (1000 * 60);
    var seconds = Math.floor(ms / 1000);
    return (hours > 0 ? ((hours < 10 ? '0' + hours : hours) + ':') : '') +
           (minutes < 10 ? '0' + minutes : minutes) + ':' +
           (seconds < 10 ? '0' + seconds : seconds);
  };

  module.exports = moment;
});
