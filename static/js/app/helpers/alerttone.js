define(function(require, exports, module) {
    var urls = require('app/model/urls');

    return function(data){
        if (typeof Audio == 'undefined') return;
        var self = this;
        self.name = data.name;
        self.fileName = data.fileName;
        self.audio = new Audio();
        self.audio.src =  urls.assets('audio/alerts/' + self.fileName);
        self.audio.type = "audio/mpeg";
        self.preload = true;
        self.audio.load();
        self.play = function(){
            if (self.audio.currentTime)
            {
                // this resets the audio timer
                self.audio.src = self.audio.src;
            }
            self.audio.play();
        };
    }
});