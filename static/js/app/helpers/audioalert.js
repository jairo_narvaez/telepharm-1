define(function(require, exports, module) {
  var $ = require('jquery'),
      ko = require('knockout'),
      urls = require('app/model/urls'),
      AlertTone = require('app/helpers/alerttone');

  var AlertTone = function(data){
    if (typeof Audio == 'undefined') return;
    var self = this;
    self.name = data.name;
    self.fileName = data.fileName;
    self.audio = new Audio();
    self.audio.addEventListener("ended", function() {
      // this resets the audio timer
      self.audio.src = self.audio.src;
      if (self.name == 'incoming-call')
        self.audio.play();
    }, false);
    self.play = function(){
      if (self.audio.currentTime)
      {
        // this resets the audio timer
        self.audio.src = self.audio.src;
      }
      self.audio.play();
    };

    self.audio.src =  urls.assets('audio/alerts/' + self.fileName);
    self.audio.type = "audio/mpeg";
    self.preload = true;
    self.audio.load();
  };

  return function(account){
    var self = this;
    self.accountId = account.id;
    self.isEnable = ko.observable(account.is_audio_enabled == '1');
    self.currentAlerTone = null;

    self.audioFiles  = [
      new AlertTone({name:'notification' ,  fileName: 'Bloom.mp3'}),
      new AlertTone({name:'new-prescription' ,  fileName: 'Concern.mp3'}),
      new AlertTone({name:'incoming-call', fileName: 'phone-ring.mp3'})
    ];

    //assume name is unique
    self.playAlert = function(name){
      if(!self.isEnable()) return;

      ko.utils.arrayForEach(self.audioFiles|| [], function(alertTone) {
        if( alertTone.name == name){
          //cancel current tone
          if ( self.currentAlerTone != null){
            self.currentAlerTone.audio.pause();
          }
          self.currentAlerTone = alertTone;
          self.currentAlerTone.play();
        }
      });
    };

    self.stop = function() {
      if (self.currentAlerTone != null) {
        self.currentAlerTone.audio.pause();
        self.currentAlerTone = null;
      }
    };

    self.toggleEnable = function () {
      $.post(urls.www('ajax/notifications/set_audio_enabled'),
      { enabled: !self.isEnable() },
      function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        if (response.result)
          self.isEnable(!self.isEnable());
      });

    }
  };
});
