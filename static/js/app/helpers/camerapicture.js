define(function(require, exports, module) {
    var ko = require('knockout');

    return function(videoSel,
                    coverSel, canvasSel,
                    photoSel, startSel, selectPicSel) {
        var self = this;
        self.previewToUpdate = ko.observable(null);
        var streaming = false,
            video = document.querySelector(videoSel),
            cover = document.querySelector(coverSel),
            canvas = document.querySelector(canvasSel),
            photo = document.querySelector(photoSel),
            startbutton = document.querySelector(startSel),
            selectpicture = document.querySelector(selectPicSel);
        var scale = 2,
            width = (896),
            height = (876);
        var context = canvas.getContext('2d');


        navigator.getMedia = (navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);
        navigator.getMedia(
            {
                video: true,
                audio: false
            },
            function(stream) {
                self.localMediaStream = stream;
                if (navigator.mozGetUserMedia) {
                    video.mozSrcObject = stream;
                } else {
                    var vendorURL = window.URL || window.webkitURL;
                    video.src = vendorURL ? vendorURL.createObjectURL(stream) : stream;
                }
                video.play();
            },
            function(err) {
                alert('An error occurred! Please verify you camera connection and ' +
                    'ensure your browser allowed the use of the camera');
            }
        );

        self.isReady = ko.observable(false);
        self.isReady.subscribe(function(newVal){
            if (newVal && self.previewToUpdate()){
                self.previewToUpdate().fadeIn();
            }
        });
        video.addEventListener('canplay', function(ev) {
            if (!streaming) {
                video.setAttribute('width', width * scale);
                video.setAttribute('height', height * scale);
                canvas.setAttribute('width', width * scale);
                canvas.setAttribute('height', height * scale);
                context.scale(scale, scale);
                streaming = true;
                self.isReady(true);
            }
        }, false);


        self.takePicture = function takepicture() {
            if (!streaming) {
                alert('Please verify your browser has been granted permission to' +
                    ' use the camera');
                return false;
            }

            context.drawImage(video, 0, 0, width, height);
            var data = canvas.toDataURL('image/jpeg', 0.6);
            photo.setAttribute('src', data);
        };
    };
});
