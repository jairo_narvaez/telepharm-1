define(function(){
    return function(){
        //this will go away at some point.
        this.build_error_ul = function(messages) {
            if (typeof messages == "string"){
                return messages;
            }
            var result = '<ul>';
            for (var i = 0; i< messages.length; i++){
                var message = messages[i];
                if (message.severity < 2){
                    result += '<li>'+ message.message +'</li>';
                }
            }
            result += '</ul>'
            return result;
        }
    }
});
