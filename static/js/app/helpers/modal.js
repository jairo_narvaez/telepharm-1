define(function(require, exports, module) {
  var $ = require('jquery'),
  ko = require('knockout');

  return function() {
    var self = this,
        body = $('body');

    $(function() {
      self.$el = $('#modal-template').modal({
        show: false,
        keyboard: false
      });
    });

    self.title = ko.observable('Header');
    self.content = ko.observable('<p>Content</p>');
    self.closeTitle = ko.observable('Close');
    self.okTitle = ko.observable('OK');
    self.callAfterClose = ko.observable(null);
    self.callAfterOk = ko.observable(null);
    self.pressed = 'close';

    self.$el.on('hidden', function() {
      if (self.pressed == 'close')
      {
        if (typeof self.callAfterClose() == 'function')
          self.callAfterClose()();
      }
      if (self.pressed == 'ok')
      {
        if (typeof self.callAfterOk() == 'function')
          self.callAfterOk()();
      }
    });

    self.reset = function() {
      self.pressed = 'close';
      self.title('');
      self.content('');
      self.closeTitle('Close');
      self.okTitle('Ok');
      self.callAfterClose(null);
      self.callAfterOk(null);
      return self;
    };

    self.confirm = function(message) {
      self.reset().title('Confirm').
      content('<b>' + message + '</b>').
      closeTitle('No').
      okTitle('Yes');
      return self;
    };

    self.error = function(message) {
      self.reset().title('Information').
      content(message).
      closeTitle('').
      okTitle('Ok');
      return self;
    };

    self.show = function() {
      self.$el.modal('show');
    };

    self.close = function() {
      self.pressed = 'close';
      self.$el.modal('hide');
    };

      self.getScrollBarWidth = function  () {
          var inner = document.createElement('p');
          inner.style.width = "100%";
          inner.style.height = "200px";

          var outer = document.createElement('div');
          outer.style.position = "absolute";
          outer.style.top = "0px";
          outer.style.left = "0px";
          outer.style.visibility = "hidden";
          outer.style.width = "200px";
          outer.style.height = "150px";
          outer.style.overflow = "hidden";
          outer.appendChild (inner);

          document.body.appendChild (outer);
          var w1 = inner.offsetWidth;
          outer.style.overflow = 'scroll';
          var w2 = inner.offsetWidth;
          if (w1 == w2) w2 = outer.clientWidth;

          document.body.removeChild (outer);

          return (w1 - w2);
      };

      self.scrollLock = function(element){
          element.on('mouseenter', function(){
              var lockedElement = $(this);
              var left = lockedElement.position().left;// + (Math.floor(getScrollBarWidth() / 2));
              if (body.css('overflow-y') != 'hidden'){
                  body.css('overflow-y', 'hidden');
                  lockedElement.css('left', left);
                  if (body.width() > 1020){
                      body.css('margin-left', 0 - self.getScrollBarWidth());
                  }
              }
          });

          element.on('mouseleave', function(){
              var lockedElement = $(this);
              var left = lockedElement.position().left;// - (Math.floor(getScrollBarWidth() / 2));
              if (body.css('overflow-y') != 'scroll'){
                  body.css('overflow-y', 'scroll');
                  lockedElement.css('left', left);
                  if (body.width() > 1020){
                      body.css('margin-left', 0);
                  }
              }
          });
      }

    self.ok = function() {
      self.pressed = 'ok';
      self.$el.modal('hide');
    };
  };
});
