define(function(require, module, exports) {
  'use strict';
  var ko = require('knockout'),
  urls = require('app/model/urls');
  return function(videoConfVM, subscribeVideoDiv, localVideoDiv) {
    var self = this;
    self.video = videoConfVM;
    self.name = '';
    self.publishContainer = localVideoDiv || 'publish-container';
    self.subscribeContainer = subscribeVideoDiv || 'subscribe-container';
    self.autoOpen = self.subscribeContainer == 'subscribe-container';
    self.session = null;
    self.listeners = [];

    self.addListener = function(kind, callback) {
      self.listeners.push({
        kind: kind,
        callback: callback});
    };

    self.$el = $('#modal-video').modal({
        show: false,
        keyboard: false,
        backdrop : 'static'
    });

    self.$body = self.$el.find('.modal-body');

    self.$el.on('hidden', function() {
      self.session.disconnect();
        self.$body.html($(
            '<div id="tokbox-container">' +
                '	<div id="publish-container"></div>' +
                '	<div id="subscribe-container"></div>' +
                '</div>'
        ));
    });

    self.hangUp = function() {
      self.video.hangUpCall();
      self.$el.modal('hide');
    };

    self.noModalHangUp = function() {
      self.video.hangUpCall(function() {
        self.session.disconnect();
      });
    };

    function exceptionHandler(event) {
      // Retry session connect
      if (event.code === 1006 || event.code === 1008 || event.code === 1014) {
        alert('There was an error connecting. Trying again.');
        self.session.connect(apiKey, token);
      }
    }

    if (typeof TB != 'undefined')
    {
      // TB.setLogLevel(TB.DEBUG);
      TB.addEventListener('exception', exceptionHandler);
    }

    function sessionConnectedHandler(event) {
      subscribeToStreams(event.streams);
      self.session.publish(self.publishContainer, {
        height: 176,
        width: 234,
        name: self.name
      });
      if (self.autoOpen)
        self.$el.modal('show');
    }

    function streamCreatedHandler(event) {
      subscribeToStreams(event.streams);
    }

    function streamDestroyedHandler(event) {
      if (self.autoOpen)
        self.$el.modal('hide');
      else
        self.noModalHangUp();
    }

    function subscribeToStreams(streams) {
      for (var i = 0; i < streams.length; i++) {
        var stream = streams[i];
        if (stream.connection.connectionId !=
            self.session.connection.connectionId) {
          self.session.subscribe(stream, self.subscribeContainer, {
            height: 176,
            width: 234
          });
        }
      }
    }

    self.connect = function(session_id, token, name) {
        self.session = TB.initSession(session_id);
        self.session.addEventListener('sessionConnected',
            sessionConnectedHandler);
        self.session.addEventListener('streamCreated', streamCreatedHandler);
        self.session.addEventListener('streamDestroyed', streamDestroyedHandler);
        ko.utils.arrayForEach(self.listeners, function(l) {
            self.session.addEventListener(l.kind, l.callback);
        });
        self.session.connect(urls.TB_api_key(), token);
        self.name = name;
    };
  };
});
