define(function(require, exports, module) {
  var ko = require('knockout'),
      moment = require('moment');

  return function(data) {
    var self = this;
    self.id = ko.observable(data.id);
    self.username = ko.observable(data.username);
    self.email = ko.observable(data.email);
    self.firstName = ko.observable(data.first_name);
    self.lastName = ko.observable(data.last_name);
    self.store = ko.observable(data.store || '');
    self.isAudioEnabled = ko.observable(data.is_audio_enabled == '1');
    self.name = ko.computed(function() {
      return self.firstName() + ' ' + self.lastName();
    });
    self.createdDt = ko.observable(moment(data.created_dt + ' +0000',
        'YYYY-MM-DD HH:mm:ss Z').utc());
  }
});

