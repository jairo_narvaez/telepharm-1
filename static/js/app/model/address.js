define(function(require, exports, module) {
    var ko = require('knockout');
    var StateModel = require('app/model/state');
    return function(){
        var self = this;
        self.line1 = ko.observable();
        self.line2 = ko.observable();
        self.city = ko.observable();
        self.postalCode = ko.observable();
        self.state = new StateModel();

        self.load = function(data){
            self.line1(data.line1);
            self.line2(data.line2);
            self.city(data.city);
            self.state.load(data.state);
            self.postalCode(data.postal_code);
        };
        self.unload = function(){
            self.line1(null);
            self.line2(null);
            self.city(null);
            self.state.unload();
            self.postalCode(null);
        }
    }
});