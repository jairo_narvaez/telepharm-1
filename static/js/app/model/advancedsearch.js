define(function(require, exports, module) {
  var ko = require('knockout'),
      moment = require('moment');

  return function(data) {
    var self = this;
    self.store = ko.observable('all');
    self.status = ko.observable('all');
    self.name = ko.observable('');
    self.rxNumber = ko.observable('');
    self.fromDate = ko.observable(moment().subtract('days', 1).format('MM/DD/YYYY'));
    self.toDate = ko.observable(moment().format('MM/DD/YYYY'));
  }
});
