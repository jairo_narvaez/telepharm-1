define(function(require, exports, module) {

  var $ = require('jquery');

  return function() {
    var self = this;

    self.intervals = $.parseJSON($('#intervals-data').val());

    self.getInterval = function(type, defaultInterval) {
      if (self.intervals == null) return defaultInterval;
      if (typeof self.intervals[type] != 'undefined')
        return parseInt(self.intervals[type], 10);
      return defaultInterval;
    };
  };
});
