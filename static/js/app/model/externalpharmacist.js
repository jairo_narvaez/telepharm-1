define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        urls = require('app/model/urls');

    var PersonModel = require('app/model/person'),
        ExternalStoreModel = require('app/model/externalstore');

    return function(){
        var self = this;
        self.id = ko.observable();
        self.person = new PersonModel();
        self.externalStore = new ExternalStoreModel();
        self.load = function(data){
            self.id(data.id);
            self.person.load(data.person);
            self.externalStore.load(data.store);
        }
        self.unload = function(){
            self.id(null);
            self.person.unload();
            self.externalStore.unload();
        }
    }
});