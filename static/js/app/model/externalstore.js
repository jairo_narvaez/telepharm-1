define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        urls = require('app/model/urls');
    var AddressModel = require('app/model/address');

    return function(data){
        var self = this;
        self.id = ko.observable();
        self.name = ko.observable();
        self.address = new AddressModel();
        self.load = function(data){
            self.id(data.id);
            self.name(data.name);
            self.address.load(data.address);
        }
        self.unload = function(){
            self.id(null);
            self.name(null);
            self.address.unload();
        }
    }
});
