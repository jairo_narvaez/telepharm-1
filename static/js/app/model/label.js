define(function(require, exports, module) {
  'use strict';
  var $ = require('jquery'),
      ko = require('knockout'),
      urls = require('app/model/urls');

  return function(data, prescription) {
    var self = this;

    self.prescription = prescription;
    self.id = ko.observable(data.id || 0);
    self.label = ko.observable(data.label || '');
    self.color = ko.observable(data.color || 'white');
    self.displayOrder = ko.observable(data.display_order || 0);
    self.checked = ko.observable(data.checked || false);

    self.interactWithTheServer = ko.observable(data.interact || false);

    self.checked.subscribe(function(newVal) {
      if (!self.interactWithTheServer()) return;
      if (newVal) {
        // Adding a new label
        // post to add the label and update the new id
        $.post(urls.www('ajax/prescription/add_label/' + self.prescription.prescriptionId()),
            {label: ko.toJSON(self)},
            function(response) {
              if (response.error)
                alert('Something went wrong removing the label');
              self.id(response.result.id);
              self.label(response.result.label);
              self.color(response.result.color);
              self.displayOrder(response.result.display_order);
            });
      }
      else {
        // Removing a label
        // if has a id only
        if (self.id() == 0) return;
        $.post(urls.www('ajax/prescription/remove_label/' +
            self.prescription.prescriptionId()),
            {label: ko.toJSON(self)},
            function(response) {
              if (response.error || !response.result)
                alert('Something went wrong removing the label');
            });
      }
    });
  };
});
