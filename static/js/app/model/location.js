define(function(require, exports, module) {
  var ko = require('knockout'),
      moment = require('moment');
//this will all be going away when I get the address table fully implemented
  return function() {
    var self = this;
    self.id = ko.observable();
    self.name = ko.observable();
    self.address1 = ko.observable();
    self.address2 = ko.observable();
    self.city = ko.observable();
    self.state = ko.observable();
    self.zip = ko.observable();
    self.createdDt = ko.observable();

    self.load = function(data){
        self.id(data.id);
        self.name(data.name);
        self.address1(data.address1);
        self.address2(data.address2);
        self.city(data.city);
        self.state(data.state);
        self.zip(data.zip);
        self.createdDt(moment(data.created_dt + ' +0000',
            'YYYY-MM-DD HH:mm:ss Z').utc());
    }
  }
});

