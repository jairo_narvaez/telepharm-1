define(function (require, exports, module) {
    var ko = require('knockout'),
        moment = require('moment'),
        timeFrom = require('app/functions/timefrom'),
        urls = require('app/model/urls');

    return function (data) {
        var self = this;

        data = $.extend({
            id: '',
            created_dt: '',
            message: '',
            processed: '0',
            type: 'generic',
            details: {},
            actionMessage: '',
            actionFunction: null,
            dismissFunction: null,
            autohideFunction: null,
            timeAutoHide: 0,
            imageUrl: ''
        }, data);

        self.id = ko.observable(data.id);
        self.createTime = moment().adjustedToServer();
        self.time = ko.observable(moment(data.created_dt + ' +0000',
            'YYYY-MM-DD HH:mm:ss Z').utc());
        self.message = ko.observable(data.message);
        self.processed = ko.observable(data.processed == '1');
        self.actionMessage = ko.observable(data.actionMessage);
        self.actionFunction = ko.observable(data.actionFunction);
        self.dismissFunction = ko.observable(data.dismissFunction);
        self.autohideFunction = ko.observable(data.autohideFunction);
        self.timeAutoHide = ko.observable(data.timeAutoHide || 0); // in seconds
        self.now = ko.observable(moment().adjustedToServer());
        self.collection = null;
        self.show = ko.observable(true);
        self.details = data.details;
        self.imageUrl = ko.observable(data.imageUrl);
        self.processed.subscribe(function (newVal) {
            if (!newVal) return;
            $.post(urls.www('ajax/notifications/set_processed'),
                { notification_id: self.id() },
                function (response) {
                    if (response.error) {
                        self.processed(false);
                        page.modal.error(response.message).show();
                        return;
                    }
                });
        });

        self.type = ko.observable(data.type);
        self.typeName = ko.computed(function () {
            return self.type() == 'comment' ? 'Comment' : 'Notification';
        });
        self.className = ko.computed(function () {
            //prescription-comment-box, prescription-notification-box
            return 'prescription-' + self.typeName().toLowerCase() + '-box';
        });

        self.customMessage = ko.computed(function () {
            if (self.type() !== 'comment' && self.type() !== 'generic'){
                if (self.message().split(' is ').length==2 ){
                    return ' is '+self.message().split(' is ')[1];
                } else if (self.message().split(' has ').length==2){
                    return ' has '+self.message().split(' has ')[1];
                }
            }
            return self.message();
        });

        self.prescriptionName = ko.computed(function () {
            if (self.type() !== 'comment' && self.type() !== 'generic'){
                if (self.message().split(' is ').length==2 ){
                    return self.message().split(' is ')[0];
                } else if (self.message().split(' has ').length==2){
                    return self.message().split(' has ')[0];
                }
            }
            return '';
        });

        self.timeFromNow = ko.computed(function () {
            return self.time().timeFrom(self.now());
        });

        self.tick = function () {
            self.now(moment().adjustedToServer());
            self.autohide();
        };

        self.execute = function () {
            if (typeof self.actionFunction() == 'function') {
                self.actionFunction()(self);
            }
        };

        self.dismiss = function () {
            if (typeof self.dismissFunction() == 'function') {
                self.dismissFunction()(self);
            }
        };

        self.autohide = function () {
            if (self.processed()) return; // not auto hide processed
            if (self.timeAutoHide() == 0) return; // unlimited
            var secondsElapsed = self.now().diff(self.createTime, 'seconds');
            if (secondsElapsed > self.timeAutoHide()) {
                if (typeof self.autohideFunction() == 'function') {
                    self.autohideFunction()(self);
                    self.timeAutoHide(0);
                }
            }
        };
    }
});
