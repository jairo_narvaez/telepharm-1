define(function(require, exports, module) {
  var ko = require('knockout'),
      moment = require('moment');

  return function(session) {
    var self = this;
    self.id = ko.observable(session.id);
    self.token = ko.observable(session.token);
    self.patientApiAccountId = ko.observable(session.patient_api_account_id);
    self.prescriptionId = ko.observable(session.prescription_id);
    self.humanName = ko.observable(session.human_name);
    self.createdDt = ko.observable(moment(session.created_dt + ' +0000',
        'YYYY-MM-DD HH:mm:ss Z').utc());
    self.endDt = ko.observable(moment(session.end_dt + ' +0000',
        'YYYY-MM-DD HH:mm:ss Z').utc());
  };
});
