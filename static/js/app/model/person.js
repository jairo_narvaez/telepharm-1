define(function(require, exports, module) {
    var ko = require('knockout');

    return function(){
        var self = this;
        self.firstName = ko.observable();
        self.lastName = ko.observable();
        self.middleName = ko.observable();
        self.dateOfBirth = ko.observable();
        self.load = function(data){
            self.firstName(data.first_name);
            self.lastName(data.last_name);
            self.middleName(data.middle_name);
            self.dateOfBirth(data.date_of_birth);
        }
        self.unload = function(){
            self.firstName(null);
            self.lastName(null);
            self.middleName(null);
            self.dateOfBirth(null);
        }
    }
});