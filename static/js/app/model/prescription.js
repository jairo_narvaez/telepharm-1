define(function(require, exports, module) {
  var ko = require('knockout'),
      $ = require('jquery'),
      moment = require('moment'),
      urls = require('app/model/urls'),
      timeFrom = require('app/functions/timefrom'),
      PrescriptionImage = require('app/model/prescriptionimage'),
      PrescriptionTransferModel = require('app/model/prescriptiontransfer'),
      PrescriptionDispenseModel = require('app/model/prescriptiondispense'),
      PrescriptionPrescriberModel = require('app/model/prescriptionprescriber'),
      PrescriptionDrugModel = require('app/model/prescriptiondrug'),
      StoreModel = require('app/model/store');

  return function(data) {
    var self = this;

    self.prescriptionId = ko.observable(data.id);
    self.patientId = ko.observable(data.patient_id || '');
    self.patientFirstName = ko.observable(data.patient_fname || '');
    self.patientLastName = ko.observable(data.patient_lname || '');
    self.patientDOB = ko.observable(moment(data.patient_dob));
    self.patientPhone = ko.observable(data.patient_phone || '');
    self.patientEmail = ko.observable(data.patient_email || '');
    self.markLast = ko.observable(false);
    self.patientName = ko.computed(function() {
      return self.patientFirstName() + ' ' + self.patientLastName();
    });
    self.accountId = ko.observable(data.account_id);
    self.createdBy = ko.observable(data.created_by);
    self.getClass = function(){
        if (self.preDraft()){
            return 'btn btn-mini btn-primary';
        }
        if (self.isTransfer()){
            return 'btn btn-mini btn-warning';
        }
        if (self.inFinalStatus()){
            return 'btn btn-mini btn-success';
        }
        return 'btn btn-mini btn-default';
    }
    self.number = ko.observable(data.number);
    self.refillNumber = ko.observable(data.refill_number||'1');
    self.fullNumber = ko.computed(function() {
      var fNumber = self.number();
      fNumber += '-' + self.refillNumber();
      return fNumber;
    });
      self.store = new StoreModel();
      if (data.associated_store && data.associated_store.id){
          self.store.load(data.associated_store);
      }

    self.storeName = ko.observable(typeof data.store != 'undefined' ? data.store.store_name : '');
    self.storePhone = ko.observable(typeof data.store != 'undefined' ? data.store.store_phone: '');
    self.isHighPriority = ko.observable(data.priority);
    self.approvedByPharmacist = ko.observable(data.approved_by_pharmacist||'');
    self.transfer = new PrescriptionTransferModel();
      self.firstLoadItem = ko.observable();
      self.lastLoadItem = ko.observable();

      if (data.transfer && data.transfer.id){
          self.transfer.load(data.transfer);
      }

      if (data.drug && data.drug.id){
          self.drug = new PrescriptionDrugModel(data.drug);
      }

      if (data.dispense && data.dispense.id){
          self.dispense = new PrescriptionDispenseModel(data.dispense);
      }

      if (data.prescriber && data.prescriber.id){
          self.prescriber = new PrescriptionPrescriberModel(data.prescriber);
      }
    self.technicianInitials = ko.computed(function() {
      var createdNames = self.createdBy(),
          techNames = [];
        if (createdNames && createdNames.indexOf(' ') != -1){
            techNames = createdNames.split(' ');
        }else if (createdNames){
            techNames.push(createdNames);
        }
      var initials = '';
      ko.utils.arrayForEach(techNames, function(name){
        initials += name.substr(0,1).toUpperCase();
      });
      return initials;
    });

    self.pharmacistInitials = ko.computed(function() {
      var pharmNames = self.approvedByPharmacist().split(' ');
      var initials = '';
      ko.utils.arrayForEach(pharmNames, function(name){
        initials += name.substr(0,1).toUpperCase();
      });
      return initials;
    });

    // This is only to highlight the row being prioritized
    self.highlight = ko.observable(false);
    self.highlight.subscribe(function(newVal){
      if (newVal) {
        setTimeout(function() { self.highlight(false); }, 1000);
      }
    });
      self.isRefill = ko.observable(false);
      self.onHold = ko.observable(false);
      self.preDraft = ko.observable(false);
      self.isDraft = ko.observable(false);
      self.completed = ko.observable(false);
      self.isTransfer = ko.observable(false);
      self.completedTransfer = ko.observable(false);
      self.approvedForCounsel = ko.observable(false);
      self.approvedOnHold = ko.observable(false);
      self.requiredCounsel = ko.observable(false);
      self.refusedCounsel = ko.observable(false);
      self.rejected = ko.observable(false);
      self.canceled = ko.observable(false);
      self.archived = ko.observable(false);
      self.show = ko.observable(false);
      self.isNew = ko.observable(false);
      self.isPartial = ko.observable(false);
      self.priority = ko.observable(0);
      self.reportedError = ko.observable(false);
      self.errorMessage = ko.observable();
      self.signatureImageUrl = ko.observable();
      self.counselDate = ko.observable();
      self.counselingPharmacistFullName = ko.observable(data.counseling_pharmacist_full_name);
      self.hasSignature = ko.observable(false);
      self.signatureDt = ko.observable(data.signature_dt);

      self.loadSignatureInfo = function(data){
      self.signatureImageUrl(urls.www('image/get/') + data.signature_image_id);
      self.counselDate(data.counsel_dt);
      self.counselingPharmacistFullName(data.counseling_pharmacist_full_name);
      self.hasSignature(true);
  }

      if (data.signature_image_id){
          self.loadSignatureInfo(data);
      }

    self.loadStatuses = function(statuses, callback) {
      self.isNew = ko.observable(statuses.is_new == 1);
      self.isPartial = ko.observable(statuses.is_partial == 1);
      self.isRefill(statuses.is_refill == 1);
      self.onHold(statuses.on_hold == 1);
      self.preDraft(statuses.pre_draft == 1);
      self.isDraft(statuses.is_draft == 1);
      self.completed(statuses.completed == 1);
      self.isTransfer(statuses.transfer_rx == 1);
      self.completedTransfer(statuses.completed_transfer == 1);
      self.approvedForCounsel(statuses.approved_for_counsel == 1);
      self.approvedOnHold(statuses.approved_on_hold == 1);
      self.requiredCounsel(statuses.required_counsel == 1);
      self.refusedCounsel(statuses.refused_counsel == 1);
      self.rejected(statuses.rejected == 1);
      self.canceled(statuses.canceled == 1);
      self.archived(statuses.archived == 1);
      self.show(statuses.show == 1);
      self.isHighPriority(statuses.priority == 1);
      self.priority(parseInt(statuses.priority, 10));
      self.reportedError(statuses.reported_error == 1);
        if (callback){
            callback();
        }
    };

   self.loadStatuses(data);
    self.priority.subscribe(function(newVal) {
      $.post(urls.www('ajax/prescription/update_priority'), {
        prescription_id: self.prescriptionId(),
        priority: self.priority()
      }, function(response) {
        if (response.error) {
          page.modal.reset().title('Error').
          content(response.message).
          closeTitle('').
          okTitle('Ok').
          show();
          return;
        }
        self.highlight(true);
      });
    });

    self.dateFilled  = ko.observable(moment(data.date_filled,
        'YYYY-MM-DD'));
    self.dateWritten = ko.observable(moment(data.date_written,
        'YYYY-MM-DD'));
    self.dateExpires = ko.observable(moment(data.date_expires,
        'YYYY-MM-DD'));

    self.createdDt = ko.observable(moment(data.created_dt + ' +0000',
        'YYYY-MM-DD HH:mm:ss Z').utc());
    self.updatedDt = ko.observable(moment(data.updated_dt + ' +0000',
        'YYYY-MM-DD HH:mm:ss Z').utc());

    self.filled = ko.computed(function(){
      if (self.dateFilled() == null) return self.createdDt().format('MM/DD/YYYY');
      return self.dateFilled().format('MM/DD/YYYY');
    });
    self.written = ko.computed(function(){
      if (self.dateWritten() == null) return '';
      return self.dateWritten().format('MM/DD/YYYY');
    });
    self.expires = ko.computed(function(){
      if (self.dateExpires() == null) return '';
      return self.dateExpires().format('MM/DD/YYYY');
    });

    self.now = ko.observable(moment().adjustedToServer()); // To keep track of time

    self.images = ko.observableArray();
    self.sortedImages = ko.computed(function() {
      self.images.sort(function(a, b) {
        return a.displayOrder() == b.displayOrder() ? 0 :
        a.displayOrder() > b.displayOrder();
      });
      return self.images();
    });

    // Make list of images paginated
    self.currentImagePage = ko.observable(0);
    self.pageSize = 3;

    self.paginatedImages = ko.computed(function() {
      var startIndex = self.pageSize * self.currentImagePage();
      return self.sortedImages().slice(startIndex, startIndex + self.pageSize);
    });

    self.maxPageIndex = ko.computed(function() {
      return Math.ceil(self.images().length / self.pageSize) - 1;
    });

    self.canGoPrevious = ko.computed(function() {
      return self.currentImagePage() > 0;
    });

    self.goPrevious = function() {
      self.currentImagePage(self.currentImagePage() - 1);
    };

    self.canGoNext = ko.computed(function() {
      return self.currentImagePage() < self.maxPageIndex();
    });

    self.goNext = function() {
      self.currentImagePage(self.currentImagePage() + 1);
    };

    self.uploadedImages = ko.computed(function() {
      var result = [];
      ko.utils.arrayForEach(self.images(), function(i) {
        if (i.id() != null)
          result.push(i);
      });
      return result;
    });

    self.loadImages = function(rxInfo) {
      if (typeof rxInfo.images === 'object') {
        ko.utils.arrayForEach(rxInfo.images, function(i) {
          var img = new PrescriptionImage(i);
          self.images.push(img);
        });
        // If no images found create empty ones
        var size = 3 - self.images().length;
        if (size > 0)
        {
          var pi = new PrescriptionImage({});
          for (var i = 3 - size; i < 3; i++) {
            self.images.push(new PrescriptionImage({
              id: null,
              display_order: i,
              type: pi.getTypePerOrder(i),
              prescription_id: self.prescriptionId(),
            image_id: null}));
          }
        }
      }
    };

    self.loadImages(data);

    /** Stages **/
    self.isAwaitingCounsel = ko.computed(function() {
      return self.approvedForCounsel() &&
             !self.completed() &&
             !self.canceled();
    });
    self.isAwaitingApproval = ko.computed(function() {
      return !self.isDraft() &&
             !self.canceled() &&
             !self.completed() &&
             !self.rejected() &&
             !self.isAwaitingCounsel() &&
             !self.isTransfer() &&
             self.show();
    });

    self.isRejected = ko.computed(function() {
      return self.rejected();
    });
    self.inFinalStatus = ko.computed(function() {
      return self.completed() ||
             self.canceled() ||
             self.archived() ||
             self.completedTransfer();
    });
    self.isOnHold = ko.computed(function() {
      return self.onHold() || self.approvedOnHold();
    });
    self.considerHighPriority = ko.computed(function() {
      return self.isAwaitingApproval() &&
            self.isHighPriority();
    });

    self.increasePriority = function() {
      if (page.currentUser.role() != 'Technician') {
        page.modal.error('Sorry only technicians can change priority').show();
        return;
      }
      self.priority(self.priority() + 1);
    };

    self.decreasePriority = function() {
      if (page.currentUser.role() != 'Technician') {
        page.modal.error('Sorry only technicians can change priority').show();
        return;
      }
      self.priority(self.priority() - 1);
    };

    self.showPrescription = function() {
      if (self.isDraft() && !self.canceled())
        window.location = urls.www('prescription/create/' +
            self.prescriptionId());
      else
        window.location = urls.www('prescription/view/' +
            self.prescriptionId());
    };

    self.tick = function() {
      self.now(moment().adjustedToServer());
    };

    self.updateFromServer = function()
    {
      $.get(urls.www('ajax/prescription/get/' + self.prescriptionId()), function(data) {
        self.processFromServer(data);
      });
    };

    self.processFromServer = function(response, callback) {

      if (response.error) {
        page.modal.error(response.message).show();
        return;
      }

      self.loadStatuses(response.result);
        if (response.result.signature_image_id){
            self.loadSignatureInfo(response.result);
        }
        if (response.result.transfer && response.result.transfer.id){
            self.loadTransfer(response.result.transfer);
        }
        if (callback){
            callback(self);
        }
    };

    self.loadTransfer = function(transfer) {
        self.transfer.load(transfer);
    };

    self.waitingTime = ko.computed(function() {
      return self.createdDt().timeFrom(self.now());
    });

    var submitAction = function(callback) {
      var submitUrl = urls.www('ajax/prescription/submit');
      if (self.approvedOnHold())
      {
        submitUrl = urls.www('ajax/prescription/re_submit');
      }
      $.post(submitUrl, {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        if (typeof callback == 'function') callback();
      }, 'json');
    };

    self.canSubmit = function() {
      if (self.inFinalStatus() && !self.isPartial()) return false;
      if (self.isDraft() ||
        self.rejected() ||
        self.approvedOnHold() ||
        self.isPartial()) return true;
      return false;
    };

    var createAnotherAfterSubmitModal = function() {
        window.location = urls.www('technician');
    };

    self.submit = function(goToView) {
      if (!self.canSubmit()) return false;
        var required_image_number = parseInt($('#current_store_rx_required_image_number').val());
      if (self.uploadedImages().length < required_image_number && !self.onHold()) {
        page.modal.error('You must upload at least '+ required_image_number +' photos to submit an Rx.').show();
        return false;
      }

      if (!self.isRefill() && !self.isNew() && !self.isOnHold() && !self.isPartial() && !self.rejected()) {
        page.modal.error('You must choose at least one option from "Prescriptions Options"').show();
        return false;
      }

      page.modal.confirm('Are you sure you want to submit this prescription?').
      callAfterOk(function() {
        submitAction(createAnotherAfterSubmitModal);
      }).
      show();
      return true;
    };

    self.saveDraft = function() {
      window.location.href = '/';
    };

    var cancelAction = function(callback) {
      $.post(urls.www('ajax/prescription/cancel'), {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        if (typeof callback == 'function') callback();
        window.location = urls.www('/');
      }, 'json');
    };

    self.canCancel = function() {
      if (self.inFinalStatus()) return false;
      if (self.isDraft() ||
        self.isAwaitingCounsel() ||
        self.rejected() ||
        self.approvedOnHold()) {
        return true;
      }
      return false;
    };

    self.cancel = function(callback) {
      if (! self.canCancel()) return false;
      page.modal.confirm('Are you sure you want to cancel this prescription?').
      callAfterOk(function() {
        cancelAction(callback);
      }).
      show();
      return true;
    };

    self.canUpdateOption = function() {
      if (self.inFinalStatus()) return false;
      if (self.isDraft()) return true;
      return false;
    };

    self.updateOption = function(prescriptionOption, isActive) {
      if (! self.canUpdateOption()) return false;
      $.post(urls.www('ajax/prescription/update_option'), {
        prescription_id: self.prescriptionId(),
        option_name: prescriptionOption.name,
        value: isActive ? '1' : '0'
      }, function(response) {
        if (response.error)
        {
          page.modal.error(response.message).show();
          return;
        }
        self.loadStatuses(response.result);
      });
      return true;
    };

    var approveAction = function( dashboard ) {
      var requireCouncel = ($('#require_counsel:checked').length==1)? true : false ;
      var approveUrl = urls.www('ajax/prescription/approve');
      if (self.onHold())
      {
        approveUrl = urls.www('ajax/prescription/approve_on_hold');
      }

      if (requireCouncel){
          approveUrl = urls.www('ajax/prescription/required_counsel');
      }

      $.post(approveUrl, {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          setTimeout('page.modal.error("' + response.message + '").show();', 1000);
          
          return;
        }
        self.loadStatuses(response.result.statuses);
        if (/view/.test(response.result.next_prescription) && !dashboard) {
            page.approver.processNext(response.result.next_prescription);
        }
        else {
            page.approver.processReturn();
        }
      });
    };

    self.canApprove = function() {
      if (self.inFinalStatus() ||
          self.isAwaitingCounsel() ||
          self.isDraft() ||
          self.rejected() ||
          self.approvedOnHold() ||
          !self.show()) return false;
      return true;
    };

    self.approveMessage = function() {
      if (! self.canApprove()) return false;
      page.approver.show();
      return true;
    };

      self.approve = function() {
          var dashboard = $(event.currentTarget).hasClass('dashboard');
          approveAction(dashboard);
          return true;
      };



    var requestCounselAction = function() {
      $.post(urls.www('ajax/prescription/request_counsel'), {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        self.loadStatuses(response.result.statuses);
        if (/view/.test(response.result.next_prescription)) {
          page.chooser.show(response.result.next_prescription);
        }
        else {
          page.chooser.processReturn();
        }
      });
    };

    self.canRequestCounsel = function() {
      if (self.inFinalStatus() ||
        self.isAwaitingCounsel() ||
        self.isDraft() ||
        self.rejected()) return false;
      return true;
    };

    self.requestCounsel = function() {
      if (! self.canRequestCounsel()) return false;
      page.modal.confirm('Are you sure you want to approve and ' +
      'request counsel for this prescription?').
      callAfterOk(requestCounselAction).
      show();
      return true;
    };

    var completeCounselAction = function(callback) {
      $.post(urls.www('ajax/prescription/complete_counsel'), {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        self.loadStatuses(response.result);
        if (typeof callback == 'function') callback();
      });
    };

    self.canCompleteCounsel = function() {
      if (self.inFinalStatus() || self.isDraft()) return false;
      return true;
    };

    self.canTechCompleteCounsel = function()
    {
      if (!self.canCompleteCounsel()) return false;

      if (!self.isAwaitingCounsel()) return false;

      if (self.refusedCounsel()) return false;
        
      return true;
    }
      self.completeCounselPharm = function(callback){
          completeCounselAction(callback);
      }

    self.completeCounsel = function(callback) {
      if (! self.canCompleteCounsel()) return false;
      page.modal.confirm('Are you sure you want to complete the ' +
      'counseling session for this prescription?').
      callAfterOk(function() {
        completeCounselAction(callback)
      }).
      show();
      return true;
    };

    self.canInitiateTablet = function() {
      if (self.inFinalStatus() ||
        self.isDraft() ||
        !self.isAwaitingCounsel()) return false;
      return true;
    };

    var reportErrorAction = function() {
      page.reportError.show();
    };

    self.canReportError = function() {
      return self.completed() && !self.reportedError();
    };

    self.reportError = function() {
      if (! self.canReportError() ) return false;
      page.modal.confirm('Are you sure you want to report an error ' +
      'on this prescription?').
      callAfterOk(reportErrorAction).
      show();
      return true;
    };

    self.viewError = function() {
      if (! self.reportedError() ) return false;
      page.reportError.errorText(self.errorMessage());
      page.reportError.showDisabled();
      return true;
    };
    var refuseCounselAction = function() {
      $.post(urls.www('ajax/prescription/decline'), {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        self.loadStatuses(response.result);
      });
    };

    self.canRefuseCounsel = function() {
      if (self.inFinalStatus() ||
        self.isDraft() ||
        !self.isAwaitingCounsel()) return false;
      return true;
    };

    self.refuseCounsel = function() {
      if (! self.canRefuseCounsel()) return false;
      page.modal.confirm('Are you sure you want to refuse the ' +
      'counsel for this prescription?').
      callAfterOk(refuseCounselAction).
      show();
      return true;
    };


    var rejectAction = function() {
      $.post(urls.www('ajax/prescription/reject'), {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        self.loadStatuses(response.result.statuses);
        if (/view/.test(response.result.next_prescription) && !dashboard ) {
          page.rejecter.processNext(response.result.next_prescription);
        }
        else {
          page.rejecter.processReturn();
        }
      });
    };

    self.canReject = function() {
      if (self.onHold() && self.approvedOnHold()) return false;
      if (self.inFinalStatus() ||
           self.isDraft() ||
           self.isAwaitingCounsel() ||
           self.rejected()) return false;
      return true;
    };

    self.rejectMessage = function() {
      if (! self.canReject()) return false;
      page.rejecter.show();
      return true;
    };

      self.reject = function() {
          dashboard = $(event.currentTarget).hasClass('dashboard');
          var rejectWithComment = function() {
              page.rejectComment.setCallback(rejectAction);
              page.rejecter.hide();
              page.rejectComment.show();
          };
          rejectWithComment();
          return true;
      };

    var editAction = function() {
      $.post(urls.www('ajax/prescription/update'),
      { prescription_id: self.prescriptionId() },
      function(response) {
        window.location = urls.www('prescription/create/' +
          self.prescriptionId());
      });
    };

    self.canEdit = function() {
      return true;
    };

    self.edit = function() {
      page.modal.confirm('Are you sure you want to re-open this Prescription? ' +
      '<br /><br /><span style="font-weight: normal;">Make sure that the information has been re-entered and sent from your PMS.</span>').
      callAfterOk(editAction).
      show();
      return true;
    };

    self.toggleHighPriority = function() {
//      if (page.currentUser.role() != 'Technician') {
//        return;
//      }
      $.post(urls.www('ajax/prescription/set_high_priority'), {
        prescription_id: self.prescriptionId(),
        value: self.isHighPriority() ? '0' : '1'
      }, function(response) {
        if (response.error)
        {
          page.modal.error(response.message).show();
          return;
        }
        self.isHighPriority(!self.isHighPriority());
        self.highlight(true);
      });
    };

    self.initiatePatientTabletDialog = function(element, position, clickEvent) {

      if (!position) position = 'right';

      $.post(urls.www('ajax/patienttablet/check_rx'), {
        prescription_id: self.prescriptionId()
      }, function(response) {
        if (response.error)
        {
          page.modal.error(response.message).show();
          return;
        }

        var prescriptions = response.result.prescriptions;

        if (!prescriptions.length)
        {
          self.initiatePatientTablet(self.prescriptionId());
        }
        else
        {
          var is_plural = prescriptions.length > 1;

          var message = '<strong>' + prescriptions.length + '</strong> other active prescription' + (is_plural ? 's were' : ' was') + ' found for this patient.  Do you want to include ' + (is_plural ? 'these prescriptions' : 'this prescription') + ' in the tablet session as well?  The other prescription' + (is_plural ? 's are' : ' is') + ':';
          message += '<div style="margin-top: 10px; background-color: white; padding: 5px;" class="well">';
          $.each(prescriptions, function(k, v){
            message += '<div class="form form-inline" style="padding-left: 10px;"><input style="margin-bottom: 6px;" id="multiple-' + v.id + '" type="checkbox" class="multiple-prescriptions" value="' + v.id + '" /><label style="margin-left: 5px;" for="multiple-' + v.id + '"><strong>' + v.number + '-' + v.refill_number + '</strong> ' + v.description + '</label></div>';
          });
          message += '</div>';

          message += '<div><a id="initiate-multiple-button" href="#" class="btn btn-primary">Start Session</a></div>';

          //var element = $(b.srcElement);

          element.popover({
            trigger: 'manual',
            placement: position,
            html: true,
            title: '<div id="initiate-tablet-popover-header" style="min-height: 20px;"><div style="float: left;" id="initiate-tablet-popover-title"></div><div style="float: right; width: 20px; text-align: right;"><a href="#" class="close-initiate-tablet-popover badge">X</a></div></div>',
            content: '<div id="initiate-tablet-popover-body" style="text-align: left;"></div>'
          });

          element.popover('show');

          $('#initiate-tablet-popover-title').html('Similar Prescriptions');
          $('#initiate-tablet-popover-body').html(message);

          var popover_width = 400;
          var styles = {width: popover_width + 'px'};

          if (clickEvent)
          {
            var xpos = clickEvent.pageX;
            if (position == 'left') xpos -= popover_width;
            var ypos = clickEvent.pageY;
            var doc_height = $(window).height();
            if ((doc_height - ypos) < 100) ypos -= 100;

            styles.left = xpos;
            styles.top = ypos;
          }

          $('.popover:visible').css(styles);
          $('.popover:visible .arrow').remove();

          $('.close-initiate-tablet-popover').click(function(){
            element.popover('hide');
            return false;
          });

          $('#initiate-multiple-button').click(function(){
            var rx_ids = [self.prescriptionId()];
            $('.multiple-prescriptions:checked').each(function()
            {
              rx_ids.push($(this).val());
            })

            self.initiatePatientTablet(self.prescriptionId(), rx_ids);
            element.popover('hide');
            return false;
          });
        }
      });
    };

    self.initiatePatientTablet = function(prescription_id, prescriptions) {

      $.post(urls.www('ajax/patienttablet/initiate'), {
        prescription_id: prescription_id,
        prescriptions: prescriptions ? prescriptions.join(',') : prescription_id
      }, function(response) {
        if (response.error)
        {
          page.modal.error(response.message).show();
          return;
        }
        else
        {
          page.modal.error(response.result.message).show();
        }
      });

      page.tabletSync.runSync();
    };


    self.rxState = ko.computed(function() {
      if (self.preDraft()) return 'ready to create';
      if (self.isDraft()) return 'draft';
      if (self.approvedOnHold()) return 'approved - on hold';
      if (self.isTransfer()) return 'transfer initiated';
      if (self.completedTransfer()) return 'transfer complete';
      if (self.isAwaitingApproval()) return 'awaiting approval';
      if (self.isAwaitingCounsel()) return 'awaiting counsel';
      if (self.inFinalStatus()) return 'completed';
    });
  };
});
