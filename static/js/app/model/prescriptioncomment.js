define(function(require, exports, module) {
  var ko = require('knockout'),
    moment = require('moment'),
    urls = require('app/model/urls');

  return function(data) {
    var self = this;
    self.id = ko.observable(data.id);
    self.accountId = ko.observable(data.account_id);
    self.createdBy = ko.observable(data.created_by);
    self.prescriptionId = ko.observable(data.prescription_id);
    self.comment = ko.observable(data.comment);
    self.createdDt = ko.observable(moment(data.created_dt +
         ' +0000', 'YYYY-MM-DD HH:mm:ss Z'));
    self.isRead = ko.observable( data.is_read || false );
    self.authorPicture = urls.www('account/profile_image/' + self.accountId());
    self.setAsRead = function(item, event){
        if ( item.isRead()) return;
        var url = urls.www('ajax/comments/set_as_read');
        $.post(url, {prescription_comment_id: self.id},
            function(data, status, response){
                 item.isRead(true);
            },'json');
    };
  };
});

