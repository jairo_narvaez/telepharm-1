define(function(require, exports, module) {
  var ko = require('knockout');

  return function(data) {
    var self = this;

    self.id = ko.observable(data.id || '');
    self.prescriptionId = ko.observable(data.prescription_id || '');
    self.qtyWritten = ko.observable(data.qty_written || '');
    self.qtyDispensed = ko.observable(data.qty_dispensed || '');
    self.sig = ko.observable(data.sig || '');
    self.daysSupply = ko.observable(data.days_supply || '');
    self.fillsOwed = ko.observable(data.fills_owed || '');
    self.refillAuth = ko.observable(data.refill_auth || '');
    self.numRefills = ko.observable(data.num_refills || 0);
    self.sigCoded = ko.observable(data.sig_coded || '');
    self.sigExpanded = ko.observable(data.sig_expanded || '');
  }
});

