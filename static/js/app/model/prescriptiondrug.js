define(function(require, exports, module) {
  var ko = require('knockout');

  return function(data) {
    var self = this;

    self.id = ko.observable(data.id || '');
    self.prescriptionId = ko.observable(data.prescription_id || '');
    self.newNDC = ko.observable(data.new_ndc || '');
    self.oldNDC = ko.observable(data.old_ndc || '');
    self.manf = ko.observable(data.manf || '');
    self.packageSize = ko.observable(data.package_size || '');
    self.strength = ko.observable(data.strength || '');
    self.description = ko.observable(data.description);

    self.formattedNDC = ko.computed(function() {
      var ndc = self.newNDC();

      if (ndc.length == 11)
      {
        return ndc.substr(0,5) + '-' + ndc.substr(5,4) + '-' + ndc.substr(9,2);
      }
      if (ndc.length == 10)
      {
        return ndc.substr(0,4) + '-' + ndc.substr(4,4) + '-' + ndc.substr(8,2);
      }
      return ndc;
    });
  };
});

