define(function(require, exports, module) {
  var ko = require('knockout'),
      urls = require('app/model/urls');

  return function(data) {
    var self = this;
    self.id = ko.observable(data.id);
    self.type = ko.observable(data.type);
    self.displayOrder = ko.observable(data.display_order);
    self.image_id = ko.observable(data.image_id);
    self.prescription_id = ko.observable(data.prescription_id);
    self.exists = ko.observable(false);
    self.checkImageInterval = null;

    self.imageType = ko.computed(function() {
      switch (self.type())
      {
        case 'other':
          return 'Other';
        case 'prescription':
          return 'Hardcopy Prescription';
        case 'drug_label':
          return 'Prescription Label';
        case 'drug_dispensed':
          return 'Prescribed Physical Drug';
      }
    });

    var checkExistanceOfUrl = function(url) {
        var exist = false;
        $.ajax({
          type: "HEAD",
          async: false,
          url: url,
          success: function(message,text,response) {
            exist = true;
          },
          error: function() {
            exist = false;
          }
        });
        return exist;
    }

    self.image_id.subscribe(function(newVal) {
        // check if image exists (b/c of the gearman processing that could take place
        // in a differen server
        self.exists(false);
        if (self.image_id() == null) return;
        self.checkImageInterval = setInterval(function(){
          var exist = checkExistanceOfUrl(urls.www('image/get/' + self.image_id()));
          if (exist) {
            self.exists(true);
            clearInterval(self.checkImageInterval);
            self.checkImageInterval = null;
          }
        }, 1000); // Every 2 seconds check the image
    });
    self.image_id(null);
    self.image_id(data.image_id);

    self.imageUrl = ko.computed(function() {
      var image_id = self.image_id(); // This is just to trigger the computed
      // after changing to the new file
      if (self.id() != null && self.exists()) {
        return urls.www('image/get/' + self.image_id());
      }
      else
        return urls.assets('img/prescription_placeholder.png');
    });

    self.getTypePerOrder = function(order)
    {
      switch (order)
      {
        case 0: return 'prescription';
        case 1: return 'drug_label';
        case 2: return 'drug_dispensed';
        default: return 'other';
      }
    };

   self.delete = function(callback){
       $.post(urls.www('ajax/prescription/delete_image'), {
           id : self.id()
       }, function(response) {
           if (response.error)
           {
               page.modal.error(response.message).show();
               return;
           }
           if (response.result.success){
               callback();
               self.image_id(null);
               self.id(null);
           }
       });
   }
      //this will go away at some point.
      var _build_error_ul = function(messages) {
          var result = '<ul>';
          for (var i = 0; i< messages.length; i++){
              var message = messages[i];
              if (message.severity < 2){
                  result += '<li>'+ message.message +'</li>';
              }
          }
          result += '</ul>'
          return result;
      }
    self.rotate = function(direction, callback){
        $.post(urls.www('ajax/prescriptionimage/rotate'), {
            prescription_image_id : self.id(),
            rotate_direction : direction
        }, function(response) {
            if (response.error)
            {
                page.modal.error(_build_error_ul(response.message)).show();
                return;
            }
            if (response.result){
                if (callback){
                    callback(self.image_id());
                }
            }
        });
    }
  }
});

