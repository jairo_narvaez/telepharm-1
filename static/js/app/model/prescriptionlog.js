define(function(require, exports, module) {
  var ko = require('knockout'),
  moment = require('moment'),
  urls = require('app/model/urls');

  return function(data) {
    var self = this;
    self.id = ko.observable(data.id);
    self.accountId = ko.observable(data.account_id);
    self.createdBy = ko.observable(data.created_by);
    self.prescriptionId = ko.observable(data.prescription_id);
    self.createdDt = ko.observable(moment(data.created_dt +
      ' +0000', 'YYYY-MM-DD HH:mm:ss Z').utc());
    self.action = ko.observable(data.action);
    self.prescription = ko.observable(data.prescription);
      self.message = data.message;

    self.title = ko.computed(function() {
      switch(self.action())
      {
        case 'submit': return 'Filled';
        case 'reject': return 'Rejected';
        case 'approve': return 'Approved';
        case 'approve-on-hold': return 'Approved On Hold';
        case 'cancel': return 'Canceled';
        case 'create-comment': return 'Created a Comment on';
        case 'complete-counsel': return 'Completed';
          case 'complete-counsel-technician' : return 'Manually Completed'
        case 'create': return 'New';
        case 'store-image': return 'Added an Image';
        case 'update-priority': return 'Updated Priority';
        case 'transfer': return 'Transfer RX';
        case 'toggle-priority': return 'Toggled High Priority';
        case 're-submit': return 'Re-Submited On Hold';
        case 'update-from-pms': return 'Updated from PMS';
          case 'transfer_complete' : return 'Transfer Completed';
          case 'edit-prescription' : return 'Prescription was edited and returned to draft status';
          case 'video-call-ended' : return 'Video counsel for prescription'
        default: return null;
      }
    });
  };
});
