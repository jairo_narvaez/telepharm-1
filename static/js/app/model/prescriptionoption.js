define(function(require, exports, module) {
  var ko = require('knockout'),
      urls = require('app/model/urls');

  return function(data) {
    var self = this;
    self.prescription = data.prescription;
    self.label = data.label;
    self.name = data.name;
    self.checked = ko.observable(data.checked || false);
    self.checked.subscribe(function(isActive) {
      self.prescription.updateOption(self, isActive);
    });

  };
});
