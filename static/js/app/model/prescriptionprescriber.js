define(function(require, exports, module) {
  var ko = require('knockout');

  return function(data) {
    var self = this;

    self.id = ko.observable(data.id || '');
    self.prescriptionId = ko.observable(data.prescription_id || '');
    self.prescriberId = ko.observable(data.prescriber_id || '');
    self.firstName = ko.observable(data.first_name || '');
    self.lastName = ko.observable(data.last_name || '');
    self.name = ko.computed(function() {
      return self.firstName() + ' ' + self.lastName();
    });
    self.phone = ko.observable(data.phone || '');
    self.npi = ko.observable(data.npi || '');
    self.identifier = ko.observable(data.identifier || '');
  }
});

