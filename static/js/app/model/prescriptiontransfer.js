define(function(require, exports, module) {
    var ko = require('knockout'),
        urls = require('app/model/urls');

    var StateCollectionModel = require('app/model/statecollection'),
        ExternalPharmacistModel = require('app/model/externalpharmacist'),
        ExternalStoreModel = require('app/model/externalstore'),
        PersonModel = require('app/model/person');

    return function() {
        var self = this;

        self.id = ko.observable();
        self.stateCollection = new StateCollectionModel();
        self.errorMessages = ko.observableArray();
        self.externalPharmacist = new ExternalPharmacistModel();
        self.externalStore = new ExternalStoreModel();
        self.calledInPerson = new PersonModel();
        self.loaded = ko.observable(false);
        self.pharmacists = ko.observableArray();
        self.stores = ko.observableArray([]);
        self.useNewPharmacy = ko.observable();
        self.useNewPharmacist = ko.observable();
        self.initiatedBy = ko.observable();
        self.completedBy = ko.observable();
        self.initiatedDate = '';
        self.completedDate = '';

        self.fetchStores = function(currentState, callback){
            $.post(urls.www('ajax/externalstore/fetch_all'), {}, function(response){
                self.stores(response.result);
                if (response.result.length > 0){
                    self.externalStoreId(response.result[0].id);
                }
                self.useNewPharmacy(response.result.length == 0);
                callback(currentState);
            });
        }

        self.fetchPharmacists = function(callback){
            $.post(urls.www('ajax/externalpharmacist/fetch_all'),
                { },
                function(response){
                    self.pharmacists(response.result.pharmacists);
                    self.useNewPharmacist(response.result.pharmacists.length == 0);
                    if (response.result.pharmacists.length > 0){
                        self.externalPharmacistId(response.result.pharmacists[0].id);
                        self.useNewPharmacist(false);
                    }
                    callback(response.result.current_state);
                });
        }

        self.prepare = function(callback){
            self.stateCollection.fetchAll();
            self.fetchPharmacists(function(currentState){
                self.fetchStores(currentState, callback);
            });
        }

        self.load = function(data){
            if (self.loaded()) return;
            self.id(data.id);
            self.calledInPerson.load(data.called_in_by);
            self.externalStore.load(data.store);
            self.externalPharmacist.load(data.pharmacist);
            self.initiatedBy(data.initiated_by);
            self.initiatedDate = data.created_dt;
            self.completedBy(data.completed_by);
            self.completedDate = data.updated_dt;
            self.loaded(true);
        }

        self.reloadExternalPharmacist = function(externalPharmacistId){
            var pharmacists = self.pharmacists()
            for (var i = 0; i < pharmacists.length; i++){
                var pharmacist = pharmacists[i];
                if (pharmacist.id == externalPharmacistId){
                    self.externalPharmacist.load(pharmacist);
                    break;
                }
            }
        }

        self.reloadExternalStore = function(externalStoreId){
            var stores = self.stores()
            for (var i = 0; i < stores.length; i++){
                var store = stores[i];
                if (store.id == externalStoreId){
                    self.externalPharmacist.externalStore.load(store);
                    break;
                }
            }
        }

        self.initiate = function(prescription, callback) {
            self.errorMessages([]);
            var data = {
                pharmacist : {
                    id : self.externalPharmacistId(),
                    first_name : self.externalPharmacist.person.firstName(),
                    last_name : self.externalPharmacist.person.lastName(),
                    store: {
                        id : self.externalStoreId(),
                        name : self.externalPharmacist.externalStore.name(),
                        address : {
                            line1 : self.externalPharmacist.externalStore.address.line1(),
                            line2 : self.externalPharmacist.externalStore.address.line2(),
                            city : self.externalPharmacist.externalStore.address.city(),
                            state: { id : self.externalPharmacist.externalStore.address.state.id() },
                            postal_code : self.externalPharmacist.externalStore.address.postalCode()
                        }
                    }
                },
                prescription : {
                    id : prescription.prescriptionId()
                },
                called_in_person : {
                    first_name : self.calledInPerson.firstName(),
                    last_name : self.calledInPerson.lastName()
                }
            };

            $.post(urls.www('ajax/prescriptiontransfer/initiate'), data, function(response) {
                if (response.error) {
                    self.errorMessages(response.message);
                }else{
                    self.load(response.result.transfer);
                    callback(response.result.statuses);
                }
            });
        };

        self.complete = function(callback) {
            var completeUrl = urls.www('ajax/prescriptiontransfer/complete');
            $.post(completeUrl, {
                transfer_id : self.id()
            }, function(response) {
                if (response.error) {
                    page.modal.error(_build_error_ul(response.message)).show();
                }else{
                    self.load(response.result.transfer);
                    callback(response.result.statuses);
                }
            });
        };

        //this will go away at some point.
        var _build_error_ul = function(messages) {
            var result = '<ul>';
            for (var i = 0; i< messages.length; i++){
                var message = messages[i];
                if (message.severity < 2){
                    result += '<li>'+ message.message +'</li>';
                }
            }
            result += '</ul>'
            return result;
        }

        self.destroy = function(){
            self.errorMessages([]);
            self.useNewPharmacist(false);
            self.useNewPharmacy(false);
        }

        self.setExistingPharmacy = function(){
            var stores = self.stores();
            if (stores.length > 0){
                self.externalStoreId(stores[0].id);
            }
            self.useNewPharmacy(false);
        }

        self.setNewPharmacy = function(){
            self.externalStoreId(0);
            self.useNewPharmacy(true);
        }

        self.setExistingPharmacist = function(){
            var stores = self.stores();
            if (stores.length > 0){
                self.externalStoreId(stores[0].id);
            }
            self.externalPharmacistId(0);
            self.useNewPharmacist(false);
        }

        self.setNewPharmacist = function(){
            self.externalPharmacistId(0);
            var stores = self.stores();
            self.useNewPharmacy(stores.length == 0);
            self.externalStoreId(0);
            if (stores.length > 0){
                self.externalStoreId(stores[0].id);
            }
            self.useNewPharmacist(true);
        }

        self.externalStoreId = ko.observable();

        self.externalStoreId.subscribe(function(newValue){
            if (newValue){
                self.reloadExternalStore(newValue);
            }else{
                self.externalPharmacist.externalStore.unload();
            }
        });

        self.pharmacists.subscribe(function(newValue){
            self.useNewPharmacist(newValue.length == 0);
        });

        self.externalPharmacistId = ko.observable(0);

        self.externalPharmacistId.subscribe(function(newValue){
            if (newValue){
                self.reloadExternalPharmacist(newValue);
            }else{
                self.externalPharmacist.unload();
            }
        });
    }
});
