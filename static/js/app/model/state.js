define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        urls = require('app/model/urls');

    return function(){
        var self = this;
        self.id = ko.observable();
        self.name = ko.observable();
        self.abbreviation = ko.observable();

        self.load = function (data){
                self.id(data.id);
                self.name(data.name);
                self.abbreviation(data.abbreviation);
        }
        self.unload = function(){
            self.id(null);
            self.name(null);
            self.abbreviation(null)
        }
    }
});