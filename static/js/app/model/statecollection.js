define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        urls = require('app/model/urls');

    var StateModel = require('app/model/state');

    return function(){
        var self = this;

        self.stateCollection = ko.observableArray();
        //when the callback is invoked, the stateCollection should be available
        self.fetchAll = function(){
            var _rawStateCollection = $.jStorage.get('stateCollection') || [];
            if (_rawStateCollection.length == 0){
                $.post(urls.www('ajax/states/fetch_all'), {
                }, function(response) {
                    if (response.error){
                        //if we got an error for whatever reason, try again in 10 minutes.
                        setTimeout(self.fetchAll, 10 * 60 * 1000);
                    }else{
                        //cache the state collection in the browser because that data doesn't change often.
                        $.jStorage.set('stateCollection', response.result);
                        //expire data after one week to force a hard pull (we will need to change this at some point)
                        $.jStorage.setTTL('stateCollection', 7 * 24 * 60 * 1000);

                        self.stateCollection(response.result);
                    }
                });
            }else{
                self.stateCollection(_rawStateCollection);
            }
        }
    }
});