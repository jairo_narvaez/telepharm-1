define(function(require, exports, module) {
  var ko = require('knockout'),
      moment = require('moment'),
      LocationModel = require('app/model/location');

  return function(data) {
    var self = this;
    self.id = ko.observable();
    self.companyId = ko.observable();
    self.contact = ko.observable();
    self.description = ko.observable();
    self.name = ko.observable();
    self.phone = ko.observable();
      //this will be going away when I move everything over to the address table
    self.location = new LocationModel();
    self.createdDt = ko.observable();

      self.load = function(data){
          self.id(data.id);
          self.companyId(data.company_id);
          self.contact(data.contact);
          self.description(data.description);
          self.name(data.name);
          self.phone(data.phone);
          //this will be going away when I move everything over to the address table
          if (data.location){
              self.location.load(data.location);
          }
          self.createdDt(moment(data.created_dt + ' +0000',
              'YYYY-MM-DD HH:mm:ss Z').utc());
      }
  }
});


