define(function(require, exports, module) {
  return {
    www: function(uri) {
      return TP.urls.www + uri;
    },
    assets: function(uri) {
      return TP.urls.assets + uri;
    },
    TB_api_key: function() {
      return TP.TB.api_key;
    }
  };
});
