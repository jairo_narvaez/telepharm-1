define(function(require) {
    'use strict';
    var $ = require('jquery'),
        urls = require('app/model/urls'),
        ErrorHelper = require('app/helpers/errors');
    var $success = $('#store-assign-success'),
        $error = $('#store-assign-error');
    var errorHelper = new ErrorHelper();
    var technicianCurrentValue = $('#technician-store-assignment-select').val();

    $('.pharmacist-store-assignment-checkbox').on('click', function(){
        var self = $(this),
            add = self.is(':checked'),
            storeId = self.attr('data-store-id');
        $success.hide();
        $error.hide();
        $.post(urls.www('ajax/account/pharmacist_modify_store_association'),
            {
                store_id : storeId,
                add : add
            },
        function(response){
            if (response.error){
                $error.html(errorHelper.build_error_ul(response.message)).show();
                if (add){
                    self.removeAttr('checked');
                }else{
                    self.attr('checked', 'checked');
                }
            }else{
                $success.show();
            }
        });
    });

    $('#technician-store-assignment-select').on('change', function(){
        var self = $(this),
            storeId = self.val();
        $success.hide();
        $error.hide();
        $.post(urls.www('ajax/account/technician_modify_store_association'),
            {
                store_id : storeId
            },
            function(response){
                if (response.error){
                    $error.html(errorHelper.build_error_ul(response.message)).show();
                    self.val(technicianCurrentValue);
                }else{
                    $success.show();
                }
            });
    });
});
