define(function(require) {
  'use strict';
  var $ = require('jquery'),
      ko = require('knockout'),
      showPage = require('app/functions/showpage'),
      PatientPrescriptionVM = require('app/viewmodel/patient_prescription'),
      Modal = require('app/helpers/modal');

  var patientPrescriptionViewModel = new PatientPrescriptionVM();
  patientPrescriptionViewModel.modal = new Modal();

  ko.applyBindings(patientPrescriptionViewModel);
  window.page = patientPrescriptionViewModel;

  $('#prescription-form').validate();
  showPage();
});


