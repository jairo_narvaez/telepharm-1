define(function(require) {
  'use strict';
  var $ = require('jquery'),
      ko = require('knockout'),
      tpHighlight = require('app/custombinding/tphighlight'),
      tpTip = require('app/custombinding/tptip'),
      tpPopOver = require('app/custombinding/tppopover'),
      showPage = require('app/functions/showpage'),
      DashboardVM = require('app/viewmodel/dashboard'),
      Modal = require('app/helpers/modal');

  var dashboardViewModel = new DashboardVM();
  dashboardViewModel.modal = new Modal();

  ko.applyBindings(dashboardViewModel);
  window.page = dashboardViewModel;

  showPage();
  var timeout;
  $('#btn_available_state').mouseenter(function(){
      var self = $(this);
      var is_unavailable = self.hasClass('unavailable');
      timeout = setTimeout(function(){
          self.popover({
              trigger: 'manual',
              title: 'What does this do?',
              placement: 'bottom',
              html: true,
              content: is_unavailable
                  ? "Click this button to indicate that you are <strong>available</strong> for a video conference"
                  : "Click this button to indicate that you are <strong>unavailable</strong> for a video conference"
          });
          self.popover('show');
      }, 1000);
  });

  $('#btn_available_state').mouseleave(function(){
      if (timeout){
          clearTimeout(timeout);
      }
    $(this).popover('destroy');
  });
    var timeout;
  $('th p[data-bind]').mouseenter(function(){
    var label = $(this).text().split(' ')[0];

    var html = $(this).html();
    var toggle = false;
    var current = false;

    if (html.indexOf('▽') >= 0)
    {
      current = 'descending';
      toggle = 'ascending';
    }

    if (html.indexOf('△') >= 0)
    {
      current = 'ascending';
      toggle = 'descending';
    }

    var additional = '';
    if (current)
    {
      additional = 'You are currently sorting by ' + label + ' in ' + current + ' order. Click here to sort the list in ' + toggle + ' order.';
    }
    var self = $(this);
    self.popover({
        trigger: 'manual',
        title: '',
        placement: 'top',
        html: true,
        content: '<p style="color: black;">' + (additional ? additional : 'Sort by ' + label + '. ' + additional) + '</p>'
      });

      timeout = setTimeout(function(){
          self.popover('show');
      }, 1000);
  });

  $('th p[data-bind]').mouseleave(function(){
      if (timeout){
          clearTimeout(timeout);
      }
      $(this).popover('destroy');
  });

    $('#search-text-box').focus();

});
