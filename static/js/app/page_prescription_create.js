define(function(require) {
  'use strict';
  var ko = require('knockout'),
      tpcheckbox = require('app/custombinding/tpcheckbox'),
      tpPopOver = require('app/custombinding/tppopover'),
      tpTip = require('app/custombinding/tptip'),
      tpMedispanImage = require('app/custombinding/tpmedispanimage'),
      showPage = require('app/functions/showpage'),
      PrescriptionCreate = require('app/viewmodel/prescription_create'),
      Modal = require('app/helpers/modal');

  var prescriontCreateViewModel = new PrescriptionCreate();
  prescriontCreateViewModel.modal = new Modal();

  ko.applyBindings(prescriontCreateViewModel);
  window.page = prescriontCreateViewModel;

  showPage();
});
