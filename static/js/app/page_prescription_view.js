define(function(require) {
  'use strict';
  var $ = require('jquery'),
      ko = require('knockout'),
      tpcheckbox = require('app/custombinding/tpcheckbox'),
      tpPopOver = require('app/custombinding/tppopover'),
      tpMedispanImage = require('app/custombinding/tpmedispanimage'),
      showPage = require('app/functions/showpage'),
      PrescriptionViewVM = require('app/viewmodel/prescription_view'),
      Modal = require('app/helpers/modal');

  var prescriptionViewModel = new PrescriptionViewVM();
  prescriptionViewModel.modal = new Modal();
  ko.applyBindings(prescriptionViewModel);
    window.page = prescriptionViewModel
    showPage();
    prescriptionViewModel.loadPrescriptionImages();
    prescriptionViewModel.createAdditionalPrescriptionHandlers();
});
