define(function(require) {
  'use strict';
  var $ = require('jquery'),
      ko = require('knockout'),
      showPage = require('app/functions/showpage'),
      HomeVM = require('app/viewmodel/home'),
      Modal = require('app/helpers/modal');

  var homeViewModel = new HomeVM();
  homeViewModel.modal = new Modal();

  ko.applyBindings(homeViewModel);
  window.page = homeViewModel;

  showPage();
});

