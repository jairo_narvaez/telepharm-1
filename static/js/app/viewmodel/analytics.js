define(function(require, exports, module) {
  var highcharts = require('highcharts'),
      $ = require('jquery'),
      urls = require('app/model/urls');
  var Highcharts = window.Highcharts;

  return function() {
    $.getJSON(urls.www('ajax/analytics/get_for_store'), function(response) {
      if (response.error)
        alert('There was an error getting the data for the chart');

      // pending define js chart library
      chart = new Highcharts.Chart({
        chart: {
          renderTo: 'chart-container',
          plotBackgroundColor: null,
          plotBorderWidth: 0,
          plotShadow: false,
          shadow: false
        },
        legend: {
          verticalAlign: 'top',
          borderWidth: 0
        },
        title: {
          text: null
        },
        tooltip: {
          pointFormat: '<b>{point.percentage}%</b>',
          percentageDecimals: 1
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              distance: 5,
              connectorWidth: 0,
              formatter: function() {
                return '<b>' + Math.round(this.percentage * 10) / 10 + '%</b>';
              }
            },
            showInLegend: true
          }
        },
        series: [{
          type: 'pie',
          data: [
            {
              name: 'Refills',
              color: '#8BB157',
              y: response.result.refills
            },
            {
              name: 'New Prescriptions',
              color: '#649CC9',
              y: response.result.news
            },
            {
              name: 'Rejected Counsels',
              color: '#CA7741',
              y: response.result.rejected
            },
            {
              name: 'Accepted Counsels',
              color: '#6877F8',
              y: response.result.accepted
            },
            {
              name: 'Reported With Error',
              color: '#982823',
              y: response.result.with_error
            }

          ]
        }]
      });

    });
  };

});
