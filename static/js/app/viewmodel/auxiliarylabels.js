define(function (require, exports, module) {
    var $ = require('jquery'),
        ko = require('knockout'),
        Label = require('app/model/label');

    return function (prescription) {
        var self = this;
        self.prescription = prescription;

        var defLabels = [];
        $.each(JSON.parse($('#default-labels-data').val()) || [],
            function (index, item) {
                defLabels.push(new Label(item, self.prescription));
            });
        self.defaultLabels = ko.observableArray(defLabels);

        var currLabels = [], udLabels = [];
        console.log(self.prescription.prescriptionId());
        pool.add(DI.ajaxPolling.request('ajax/prescriptionlabels/fetch', {post: {prescription_id : self.prescription.prescriptionId()}}, function (response) {
            var labels = self.userDefinedLabels().slice();
            $.each(response.result || [],
                function (index, item) {
                    var foundOnDefault = false;
                    ko.utils.arrayForEach(self.defaultLabels(), function (l) {
                        if (l.label() == item.label) {
                            l.id(item.id);
                            l.checked(true);
                            foundOnDefault = true;
                        }
                    });
                    if (!foundOnDefault)
                        labels.push(new Label($.extend(item, {checked: true}),
                            self.prescription));
                });
            self.userDefinedLabels(labels);
        }));
        self.userDefinedLabels = ko.observableArray(udLabels);

        self.allLabels = ko.computed(function () {
            var result = [];
            ko.utils.arrayPushAll(result, self.defaultLabels());
            ko.utils.arrayPushAll(result, self.userDefinedLabels());
            return result;
        });

        // at this point we can interact with the server
        ko.utils.arrayForEach(self.allLabels(), function (l) {
            l.interactWithTheServer(true);
        });

        self.newLabelText = ko.observable('');
        self.showNewLabel = ko.observable(false);

        self.showNewLabel.subscribe(function (newVal) {
            if (newVal) {
                $('#create-label-popover').css({top: 0, left: 0, display: 'block'});
                var pos = $.extend({}, $('#add-label-link').offset(), {
                    width: $('#add-label-link')[0].offsetWidth,
                    height: $('#add-label-link')[0].offsetHeight
                });
                var actualWidth = $('#create-label-popover')[0].offsetWidth;
                var tp = { top: pos.top + pos.height + 10,
                    left: pos.left + pos.width / 2 - actualWidth / 2 };

                $('#create-label-popover').offset(tp);
                $('#new-label-text').focus();
            }
        });

        self.listenEnterForAddLabel = function (data, event) {
            if (event.keyCode == 13) {
                self.addLabel();
            }
        };

        self.addLabel = function () {
            if ($.trim(self.newLabelText()) === '') {
                alert('Please enter the text of the label');
                return;
            }
            var label = new Label({label: self.newLabelText()}, self.prescription);
            self.userDefinedLabels.push(label);
            self.newLabelText('');
            self.showNewLabel(false);
            label.interactWithTheServer(true);
            label.checked(true);
        };

    };
});
