define(function(require, exports, module) {
  var ko = require('knockout'),
      $ = require('jquery'),
      urls = require('app/model/urls'),
      Account = require('app/model/account'),
      Store = require('app/model/store');


  return function(page) {
    var self = this;
    self.users = ko.observableArray();
    self.stores = ko.observableArray();

    var updateAvailableUsers = function() {
      $.getJSON(urls.www('ajax/videocall/get_available_users'), function(data) {
        processAvailableUsers(data);
      });
    };

    var processAvailableUsers = function(data) {
     if (data.error)
        return;

      self.users([]);
      ko.utils.arrayForEach(data.result, function(act) {
        self.users.push(new Account(act));
      });
    };

    if (page.currentUser.role() == 'Technician')
    {
      updateAvailableUsers(); // call first then wait for queue interval
      pool.add(new DI.ajaxPolling.request("ajax/videocall/get_available_users", {}, function(response) {
        processAvailableUsers(response);
      }, AjaxIntervalManager.getInterval('available_users', DI.ajaxPolling.priority.tiny)));
    }

    var updateAvailableStores = function() {
      $.getJSON(urls.www('ajax/videocall/get_available_stores'), function(data) {
        processAvailableStores(data);
      });
    };

    var processAvailableStores = function(data) {
      if (data.error)
        return;

      var newStores = [];
      ko.utils.arrayForEach(data.result, function(sto) {
          var store = new Store();
          store.load(sto)
          newStores.push(store);
      });
        self.stores(newStores);
    };

    if (page.currentUser.role() == 'Pharmacist')
    {
      updateAvailableStores();
      pool.add(new DI.ajaxPolling.request("ajax/videocall/get_available_stores", {}, function(response) {
          processAvailableStores(response);
      }, AjaxIntervalManager.getInterval('available_users', DI.ajaxPolling.priority.tiny)));
    }
  }
});


