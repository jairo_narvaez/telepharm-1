define(function (require, exports, module) {
    var $ = require('jquery'),
        ko = require('knockout'),
        Prescription = require('app/model/prescription'),
        urls = require('app/model/urls');

    return function () {
        var self = this;
        self.resetWarning = ko.observable(false);
        self.prescriptionNumber = ko.observable('');
        self.showNewPrescription = ko.observable(false);
        self.found = ko.observableArray([]);
        self.resetDashSpecial = ko.observable(false);
        var container = $('#create-prescription-container'),
            link = container.find('#create-prescription-link'),
            popover = container.find('#create-prescription-popover'),
            button = popover.find('#create-refill-button'),
            input = popover.find('#new-prescription-text'),
            cancel = popover.find('#cancel-rx-button');
        var defaultDashIndex = parseInt($('#current_store_rx_num_length').val()),
             defaultRxNumberLength = 2;
        self.dashIndex = ko.observable(defaultDashIndex);
        var increasingRxNumber = false;
        $('body > :not(.modal)').click(function () {
            popover.fadeOut(300, function () {
                self.found([]);
                input.val('');
            });
        });
        link.on('click', function(){
            if (popover.is(':visible')) {
                popover.fadeOut(300, function () {
                    self.found([]);
                    input.val('');
                });
            }else{
                popover.css({
                    top: 0,
                    left: 0,
                    display: 'none'
                });
                var pos = $.extend({}, link.offset(), {
                    width: link[0].offsetWidth,
                    height: link[0].offsetHeight
                });
                var actualWidth = popover.width();
                var tp = { top: pos.top + pos.height + 10,
                    left: pos.left + pos.width / 2 - actualWidth / 2 };
                popover.offset(tp);
                popover.fadeIn(300, function(){
                    input.focus();
                });
                return false;
            }
        });
        input.on('keyup',function (event) {
            var element = $(this),
                newValue = element.val(),
                increasingRxNumber = newValue.length > self.previousNumber().length,
                copyPasted = Math.abs((newValue.length - self.previousNumber().length)) > 1;
            if (event.which == 189 || event.which == 109 || copyPasted){
                var existTest = newValue.replace(/[^-+]/g, ""),
                    numDashes = existTest.length;
                if (numDashes > 1){
                    newValue = newValue.slice(0, newValue.indexOf('-')) + '-' + newValue.slice(newValue.indexOf('-') + 1, newValue.indexOf('-') + 1 + defaultRxNumberLength);
                    self.dashIndex(newValue.indexOf('-'));
                }else if (numDashes != 0){
                    self.dashIndex(newValue.indexOf('-'));
                }
            }

            //parse two numbers
            var first = 0, second = 0;
            if (!increasingRxNumber) {
                if (newValue.length == (self.dashIndex() - 1)) {
                    self.found([]);
                    self.nextRefillNumber(-1);
                    button.text('Create').removeClass('btn-default btn-danger').addClass('btn-primary').removeAttr('disabled');
                    self.buttonChanged(false);
                    self.dashIndex(defaultDashIndex);
                    first = parseInt(newValue.substr(0, newValue.length));
                }else if (self.dashIndex() < newValue.length) {
                    first = parseInt(newValue.substr(0, self.dashIndex()));
                    second = Math.abs(parseInt(newValue.substr(self.dashIndex(), newValue.length)));
                } else {
                    first = parseInt(newValue.substr(0, newValue.length));
                }
            }else{
                first = parseInt(newValue.substr(0, self.dashIndex()));
                if (newValue.length > self.dashIndex()){
                    second = Math.abs(parseInt(newValue.substr(self.dashIndex(), newValue.length)));
                }
            }

            if (isNaN(first)){
                element.val('');
                return false;
            }
            //handle two numbers
            if (first > 0 && second > 0) {
                self.buttonChanged(true);
                if (self.nextRefillNumber() != -1 && second > self.nextRefillNumber()) {
                    button.text('Not Available').removeClass('btn-default btn-primary').addClass('btn-danger').attr('disabled', 'disabled');
                } else if (second == self.nextRefillNumber()) {
                    if(self.hasPreDraft()){
                        button.text('Create Refill').removeClass('btn-default btn-danger').addClass('btn-primary').removeAttr('disabled');
                    }else{
                        button.text('Use this').removeClass('btn-primary btn-danger').addClass('btn-default').removeAttr('disabled');
                    }
                } else if (second < self.nextRefillNumber()) {
                    button.text('Use this').removeClass('btn-primary btn-danger').addClass('btn-default').removeAttr('disabled');
                } else {
                    button.text('Create').removeClass('btn-default btn-danger').addClass('btn-primary').removeAttr('disabled');
                }
                element.val(first + '-' + (second.toString().substr(0, defaultRxNumberLength)));
                return false;
            } else if (first > 0 && !second) {
                button.text('Create').removeClass('btn-default btn-danger').addClass('btn-primary').removeAttr('disabled');
                self.buttonChanged(false);
                element.val(first);
                return false;
            }
        }).on('keydown', function (event) {
                var element = $(this);
                var prev = element.val();
                self.previousNumber(prev);
                if (event.which == 13) {
                    self.createPrescription(element.val());
                    return false;
                }
            });
        button.on('click', function () {
            self.createPrescription(input.val());
            return false;
        });
        cancel.on('click', function () {
            popover.fadeOut(300, function () {
                self.found([]);
                input.val('');
            });
            return false;
        });
        container.on('click', function () {
            return false;
        });

        self.previousNumber = ko.observable('');

        var parseRxNum = function(rxNum){
            if (rxNum.indexOf('-') == -1){
                return !isNaN(parseInt(rxNum));
            }else{
                rxNum = rxNum.split('-');
                if (rxNum.length == 2){
                    return !isNaN(parseInt(rxNum[0])) && !isNaN(parseInt(rxNum[1]));
              }
            }
            return false;
        }

        self.createPrescription = function (prescriptionNumber) {
            if (!parseRxNum(prescriptionNumber)) {
                page.modal.title('Error').content('<p>Please enter a valid prescription number</p>').
                    closeTitle('').okTitle('Ok').
                    callAfterClose(null).
                    callAfterOk(function () {
                        input.focus();
                    }).show();

                return;
            }

            $.post(urls.www('ajax/prescription/create'), {
                prescription_number: prescriptionNumber
            }, function (response) {
                if (response.error) {
                    page.modal.title('Error').content(response.message).
                        closeTitle('').okTitle('Ok').
                        callAfterClose(null).
                        callAfterOk(function () {
                            $('#new-prescription-text').focus();
                        }).show();
                    return;
                }
                self.dashIndex(prescriptionNumber.indexOf('-'));
                if (response.result.created) {
                    window.location = urls.www('prescription/create/' + response.result.id);
                } else if (response.result.multiple) {
                    if (response.result.multiple.length == 1) {
                        var prescription = response.result.multiple.pop();
                        if (prescription.is_draft == 1) {
                            window.location = urls.www('prescription/create/' + prescription.id);
                        } else {
                            window.location = urls.www('prescription/view/' + prescription.id);
                        }
                    } else {
                        var results = [];
                        ko.utils.arrayForEach(response.result.multiple || [], function (pr) {
                            results.push(new Prescription(pr));
                        });
                        self.found(results);
                    }
                } else {
                    self.found([]);
                    input.val('');
                }
            }, 'json');
        };
        self.hasPreDraft = ko.observable(false);
        self.found.subscribe(function (newValue) {
            self.nextRefillNumber(-1);
            self.hasPreDraft(false);
            var refillNumber;
            for (var i = newValue.length - 1; i >= 0; i--) {
                if (newValue[i].preDraft() == 1) {
                    refillNumber = parseInt(newValue[i].refillNumber());
                    break;
                }
            }
            if (refillNumber){
                self.hasPreDraft(true);
                self.nextRefillNumber(refillNumber);
            }
            var element = $('#create-prescription-found').find('.wrapper'),
                results = element.find('.results');

            if (newValue.length == 0) {
                if (results.is(':visible')) {
                    results.fadeOut(300, function () {
                        element.slideUp(300, function () {
                            element.height(0);
                        });
                    });
                } else {
                    element.height(0);
                }
                if (self.buttonChanged()) {
                    button.text('Create').removeClass('btn-default btn-danger').addClass('btn-primary').removeAttr('disabled');
                    self.buttonChanged(false);
                }
                self.nextRefillNumber(-1);
            } else {
                element.height((34 * newValue.length) + 10);
                element.slideDown(300, function () {
                    results.fadeIn();
                    if (!self.hasPreDraft()) {
                        self.nextRefillNumber(newValue[newValue.length - 1].refillNumber());
                    }
                });
            }
        });
        self.buttonChanged = ko.observable(false);
        self.nextRefillNumber = ko.observable(-1);
        self.nextRefillNumber.subscribe(function (newValue) {
            if (newValue > 0) {
                if (self.hasPreDraft()) {
                    button.text('Create Refill').removeClass('btn-default btn-danger').addClass('btn-primary').removeAttr('disabled');
                } else {
                    button.text('Use this').removeClass('btn-primary btn-danger').addClass('btn-default').removeAttr('disabled');
                }
                var rxNum = input.val();
                var appended = rxNum += "-" + newValue;
                input.val(appended);
                self.dashIndex(appended.indexOf('-'));
                self.previousNumber(appended);
                self.buttonChanged(true);
            }
        });
        self.useThis = function (prescription) {
            if (prescription.preDraft()) {
                self.createPrescription(prescription.fullNumber());
                return;
            }
            if (prescription.isDraft()) {
                window.location = urls.www('prescription/create/' + prescription.prescriptionId());
            } else {
                window.location = urls.www('prescription/view/' + prescription.prescriptionId());
            }
        };
    };
});
