define(function(require, exports, module) {
  var ko = require('knockout'),
      Account = require('app/model/account'),
      urls = require('app/model/urls');

  return function(data) {
    var self = this;
    self.account = new Account(data);
    self.role = ko.observable(data.role || 'Guest');
  }
});
