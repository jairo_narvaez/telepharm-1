define(function(require, exports, module) {
  var ko = require('knockout'),
      VideoVM = require('app/viewmodel/videoconf'),
      CurrentUser = require('app/viewmodel/currentuser'),
      AvailableUsersVM = require('app/viewmodel/availableusers'),
      QueueVM = require('app/viewmodel/queue'),
      SearchVM = require('app/viewmodel/search'),
      NotificationsVM = require('app/viewmodel/notifications'),
      CreatePrescriptionVM = require('app/viewmodel/createprescription'),
      AnalyticsVM = require('app/viewmodel/analytics'),
      TabletSyncVM = require('app/viewmodel/tablet_sync'),
      AudioAlertHelper = require('app/helpers/audioalert');

  return function() {
    var self = this;

    var account = $.parseJSON($('#account-data').val());
    var call = $.parseJSON($('#start-call').val());
    self.currentUser = new CurrentUser(account);
    self.videoConf = new VideoVM(account.is_available == '1');
      if (call.call_id != null) {
          self.videoConf.currentCall(call.call_id);
          $('.counsel-comments').modal({
              keyboard: false,
              backdrop: 'static'
          });
          self.videoConf.checkForCallStatus();
      }
    self.availableUsers = new AvailableUsersVM(self);
    self.queue = new QueueVM();
    self.search = new SearchVM(self.queue);
    self.audioAlertHelper = new AudioAlertHelper(account);
    self.notifications = new NotificationsVM(self);
    self.analytics = new AnalyticsVM();
    self.createPrescription = new CreatePrescriptionVM();
    self.tabletSync = new TabletSyncVM();
    window.page = self;
  };
});
