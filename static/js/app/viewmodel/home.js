define(function(require, exports, module) {
  var ko = require('knockout');

  return function() {
    var self = this;
    self.name = ko.observable('');
  }
});
