define(function(require, exports, module) {
  var ko = require('knockout'),
      $ = require('jquery'),
      urls = require('app/model/urls');

  return function(prescription, videoConf) {
    var self = this,
        call = videoConf;

    self.prescription = prescription;
    self.text = ko.observable('');
      self.confSave = function() {
          if ($.trim(self.text()) == '') {
              page.modal.error('Please enter the text of the comment').show();
              return;
          }
          $.post(urls.www('ajax/comments/create_multi'), {
              call_id : call.acceptedVideoCall().id,
              text: self.text()
          }, function(response) {
              if (response.error) {
                  page.modal.error('Sorry, the comment could not be saved').show();
                  return;
              };
              self.text('');
          });
      };
    self.save = function() {
      if ($.trim(self.text()) == '') {
        page.modal.error('Please enter the text of the comment').show();
        return;
      }
      $.post(urls.www('ajax/comments/create'), {
        prescription_id: self.prescription.prescriptionId(),
        text: self.text()
      }, function(response) {
        if (response.error) {
          page.modal.error('Sorry, the comment could not be saved').show();
          return;
        };
        self.text('');
      });
    };
  }
});

