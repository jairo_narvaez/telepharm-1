define(function(require, exports, module) {
    var $ = require('jquery'),
        ko = require('knockout'),
        Notification = require('app/model/notification'),
        urls = require('app/model/urls');

    return function(page) {
        var self = this;
        self.notifications = ko.observableArray();
        self.lastNotificationId = ko.observable(null);

        self.$el = $('.all-notifications').modal({
            show: false,
            keyboard: false
        });

        self.notProcessedNotifications = ko.computed(function() {
            return self.notifications().filter(function(n) {
                return !n.processed();
            });
        });

        self.recentNotProcessed = ko.computed(function() {
            return self.notProcessedNotifications().slice(0, 5);
        });

        self.countNotificationsNotProcessed = ko.computed(function() {
            return self.notProcessedNotifications().length;
        });

        self.growlNotifications = ko.computed(function() {
            // only show growl notifications that occurred in the past 1 minute
            var lower_limit = moment().subtract('seconds', 10).format('YYYY-MM-DD HH:mm:ss');

            return self.recentNotProcessed().filter(function(n) {
                return (n.type() == 'incoming-call' ||
                    n.type() == 'approved' ||
                    n.type() == 'rejected' ||
                    n.type() == 'transfer' ||
                    n.type() == 'transfer_completed' ||
                    n.type() == 'completed' ||
                    n.type() == 'canceled' ||
                    n.type() == 'generic' ||
                    n.type() == 'submitted')
                    && (n.time().format("YYYY-MM-DD HH:mm:ss") > lower_limit);
            });
        });

        self.dismissNotification = function(notification, callback) {
            page.audioAlertHelper.stop();
            notification.show(false);
            notification.processed(true);
            if (typeof callback == 'function') callback();
        };

        self.addNotification = function(n) {
            if (n == null) return;
            var found = false;
            ko.utils.arrayForEach(self.notifications(), function(notif) {
                if (notif.id() == n.id()) found = true;
            });
            if (found) return; // avoid duplicate notifications
            n.collection = self;
            self.notifications.push(n);
            self.lastNotificationId(n.id());
            self.notifications.sort(function(left, right) {
                return right.time().diff(left.time());
            });
        };

        updateNotificationTime = function() {
            ko.utils.arrayForEach(self.notifications(), function(notification) {
                notification.tick();
            });
        };

        self.createCommentNotification = function(rawNotification) {
            if (rawNotification.processed=='0')
                page.audioAlertHelper.playAlert('notification');
            return new Notification($.extend(rawNotification, {
                actionMessage: 'View the prescription',
                actionFunction: function(n) {
                    var goToPrescription = function() {
                        var url = urls.www('prescription/view/' +
                            n.details.prescription_id);
                        window.location = url;
                    };
                    if (n.processed()) goToPrescription();
                    else self.dismissNotification(n, goToPrescription);
                },
                timeAutoHide: 15, // seconds
                autohideFunction: function(n) {
                    n.show(false);
                    n.processed(true);
                }
            }));
        };

        function inDashboard() {
            return window.location.pathname == '/technician' ||
                window.location.pathname == '/pharmacist';
        }

        self.createCallNotification = function(rawNotification) {
            // set up timing interval
            var time_to_ring = 60; // default to one minute
            var possible_candidates = rawNotification.details.possible_candidates;

            if (possible_candidates)
            {
                possible_candidates = $.parseJSON(possible_candidates.replace(/&quot;/ig,'"'));

               if (possible_candidates.length <= 3)
                {
                    time_to_ring = 20;
                }
                else
                {
                    time_to_ring = 15;
                }
            }

            if (rawNotification.processed=='0')
                page.audioAlertHelper.playAlert('incoming-call');
            return new Notification($.extend(rawNotification, {
                actionMessage: 'Answer the call',
                actionFunction: function(n) {
                    if (n.processed()) {
                        page.modal.error('This call was already answered').show();
                        return;
                    }
                    page.videoConf.acceptIncomingCall(n.details, function() {
                        var call = n.details;
                        self.dismissNotification(n, function() {
                            var is_patient = parseInt(call.from_as_patient) == 1,
                                from_prescription_id = parseInt(call.from_prescription_id);
                            if (is_patient && from_prescription_id > 0) {
                                window.location = urls.www('prescription/view/' +
                                    call.from_prescription_id + '?start_call=' + call.id);
                                return true;
                            } else if (!inDashboard()) {
                                window.location = urls.www('/' +
                                    '?start_call=' + call.id);
                                return true;
                            }
                            return false;
                        });
                    });
                },
                dismissFunction: function(n) {
                    var call = n.details;
                    page.videoConf.dismissIncomingCall(call);
                },
                timeAutoHide: time_to_ring, // seconds
                autohideFunction: function(n) {
                    var call = n.details;
                    page.videoConf.dismissIncomingCall(call, true);
                    self.dismissNotification(n)
                }
            }));
        };

        self.createPrescriptiontNotification = function(rawNotification) {
            if (rawNotification.processed=='0')
                page.audioAlertHelper.playAlert('new-prescription');
            return new Notification($.extend(rawNotification, {
                actionMessage: 'View the prescription',
                actionFunction: function(n) {
                    var goToPrescription = function() {
                        var url = urls.www('prescription/view/' +
                            n.details.prescription_id);
                        window.location = url;
                    };
                    if (n.processed()) goToPrescription();
                    else self.dismissNotification(n, goToPrescription);
                },
                timeAutoHide: 15, // seconds
                autohideFunction: function(n) {
                    n.show(false);
                    if (n.type() != 'rejected')
                        n.processed(true);
                }
            }));
        };

        self.createGenericNotification = function(rawNotification) {
            if (rawNotification.processed=='0')
                page.audioAlertHelper.playAlert('notification');
            return new Notification($.extend(rawNotification, {
                actionMessage: 'ok',
                actionFunction: function(n) {
                    self.dismissNotification(n);
                },
                timeAutoHide: 15, // seconds
                autohideFunction: function(n) {
                    n.show(false);
                    n.processed(true);
                }
            }));
        };

        self.prepareNewNotification = function(rawNotification) {
            rawNotification.details = $.parseJSON(rawNotification.details) || {};
            switch (rawNotification.type)
            {
                case 'comment':
                    return self.createCommentNotification(rawNotification);
                case 'incoming-call':
                    return self.createCallNotification(rawNotification);
                case 'approved':
                case 'rejected':
                case 'canceled':
                case 'transfer':
                case 'transfer_completed':
                case 'completed':
                case 'submitted':
                    return self.createPrescriptiontNotification(rawNotification);
                case 'generic':
                    return self.createGenericNotification(rawNotification);
                default:
                    return null;
            }
        };

        self.loadFromUrl = function(callback)
        {
            $.get(urls.www('ajax/notifications/get?last_notification_id=' + self.lastNotificationId()), function(data) {
                self.processUrl(data, callback);
            });
        };

        self.processUrl = function(response, callback)
        {
            if (response && response.result.length > 0) {
                ko.utils.arrayForEach(response.result || [], function(notification) {
                    self.addNotification(self.prepareNewNotification(notification));
                });
            }

            if (typeof callback == 'function') callback();
        };

        self.updateIntervalID = setInterval(updateNotificationTime, 1000);

        var currentEnabled = page.audioAlertHelper.isEnable();
        page.audioAlertHelper.isEnable(false);
        self.loadFromUrl(function() {
            page.audioAlertHelper.isEnable(currentEnabled);
        });

        self.dynRequest = new DI.ajaxPolling.request("ajax/notifications/get", {query: {last_notification_id: self.lastNotificationId()}}, function(response) {
            self.processUrl(response, function() {
                page.audioAlertHelper.isEnable(currentEnabled);
                self.dynRequest.data = {query: {last_notification_id: self.lastNotificationId()}}; // refresh the notification id
            });
        }, AjaxIntervalManager.getInterval('get_notifications', DI.ajaxPolling.priority.high));

        pool.add(self.dynRequest);

        //// Modal methods
        self.allNotifications = ko.observableArray();

        self.allNotificationsOfTypeComments = ko.computed(function(){
            return self.allNotifications().filter(function(a) {
                return a.type() == 'comment';
            });
        });

        self.allNotificationsOfTypeNotifications = ko.computed(function(){
            return self.allNotifications().filter(function(a) {
                return a.type() != 'comment';
            });
        });

        function loadAllNotifications (callback) {
            $.get(urls.www('ajax/notifications/get_all'), {
                page: 1
            },function(response) {
                if (response.error) {
                    page.modal.error(response.message).show();
                    return;
                }
                self.allNotifications([]);
                ko.utils.arrayForEach(response.result || [], function(notification) {
                    var n = self.prepareNewNotification(notification);
                    if (['rejected', 'incoming-call'].indexOf(n.type()) == -1) {
                        ko.utils.arrayForEach(self.notifications(), function(recent) {
                            if (recent.id() == n.id())
                                recent.processed(true);
                        });
                        n.processed(true);
                    }
                    self.allNotifications.push(n);
                });
                self.allNotifications.sort(function(left, right) {
                    return right.time().diff(left.time());
                });
                if (typeof callback == 'function') callback();
            });
        };

        self.showNotificationsModal = function() {
            var currentEnabled = page.audioAlertHelper.isEnable();
            page.audioAlertHelper.isEnable(false);
            loadAllNotifications( function() {
                page.audioAlertHelper.isEnable(currentEnabled);
                $('#comments-popup-box').removeAttr('style');
                self.$el.modal('show');
            });
        };
    }
});
