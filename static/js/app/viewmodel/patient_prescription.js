define(function(require, exports, module) {
  'use strict';
  var ko = require('knockout'),
    urls = require('app/model/urls'),
    Prescription = require('app/model/prescription'),
    PrescriptionDrug = require('app/model/prescriptiondrug'),
    Label = require('app/model/label'),
    Location = require('app/model/location'),
    VideoVM = require('app/viewmodel/videoconf'),
    AudioAlertHelper = require('app/helpers/audioalert');

  return function() {
    var self = this;
    var account = $.parseJSON($('#account-data').val());
    var prescriptionData = $.parseJSON($('#prescription-data').val());
    var prescriptionLabesData = $.parseJSON($('#prescription-labels-data').val());
    self.videoConf = new VideoVM(account.is_available == '1', 'pharmacist-video', 'patient-video');
    clearInterval(self.videoConf.incomingIntervalID); // no need to check incoming call in patient app
    self.videoConf.tokbox.addListener('streamDestroyed', function() {
      self.prescription.updateFromServer();
    });
    self.audioAlertHelper = new AudioAlertHelper(account);

    self.prescription = new Prescription(prescriptionData);
    self.prescriptionLabels = ko.observableArray();
    self.prescriptionDrug = new PrescriptionDrug(prescriptionData.prescription_drug);
    self.patientLocation = new Location();
      self.load = function(data){
            self.patientLocation.load(data.patient_location);
      }

    ko.utils.arrayForEach(prescriptionLabesData, function(label) {
      self.prescriptionLabels.push(new Label(label));
    });

    self.talkToPharmacist = function(item) {
      self.videoConf.requestPatientVideoConf(self.prescription);
    };
    self.declineCounsel = function(item, event) {
      self.prescription.refuseCounsel();
    };
    self.finishCounsel = function(item) {
      self.prescription.completeCounsel(function() {
        self.videoConf.tokbox.noModalHangUp();
      });
    };
  };
});
