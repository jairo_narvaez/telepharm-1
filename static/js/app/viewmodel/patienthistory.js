define(function(require, exports, module) {
  var $ = require('jquery'),
  ko = require('knockout'),
  urls = require('app/model/urls');

var Prescription = require('app/model/prescription'),
    ModalHelper = require('app/helpers/modal');
  return function(patientId, prescriptionId) {
    var self = this,
        body = $('body'),
        modalHelper = new ModalHelper();
    self.patientId = patientId;
    self.prescriptionId = prescriptionId;
      self.showNoMoreResults = ko.observable(false);
      self.element = $('#patient-history-modal');
    self.$el = self.element.modal({
        show: false,
        keyboard: false
    });

      self.$el.on('shown', function(){
          var nav = self.element.find('.body-scroller tr:not(.extra, :first)');
          var containerHeight = ((nav.length * nav.height()) / (nav.length / 10)) + 21;

          self.stickyNavPoints.push(containerHeight);
          $('.patient-history-append-results-break:eq(0)').addClass('sticky');
          self.isShown(true);
      });
      self.$el.on('hidden', function(){
          if (body.css('overflow-y') != 'scroll'){
              body.css('overflow-y', 'scroll');
              if (body.width() > 1020){
                  body.css('margin-left', 0);
              }
          }
          self.element.find('.sticky').removeClass('sticky');
          self.isShown(false);
          self.stickyNavPoints = [0];
      });
      self.$el.on('hide', function(){
          self.element.find('.body-scroller').scrollTop(self.element.find('.body-scroller').position().top);
      });

      modalHelper.scrollLock(self.element);

      self.stickyNavPoints = [0];

      self.element.find('.body-scroller').on('scroll', function(){
          var tbody = $(this),
              modalTop = tbody.offset().top - body.scrollTop();
          var scroll = tbody.scrollTop(),
              totalHeight = tbody[0].scrollHeight,
              visibleHeight = tbody.height();
          if (scroll > self.lastScrollValue()){
              if ((scroll) >= self.stickyNavPoints.slice(-1).pop()){
                  var currentNav = $('.patient-history-append-results-break:eq('+ (self.stickyNavPoints.length - 2) + ')');
                  currentNav.removeClass('sticky');
                  var currentTop = currentNav.position().top;
                  var newNav = currentNav.nextAll('.patient-history-append-results-break').first(),
                      newTop = newNav.position().top;
                  newNav.css('top', modalTop).addClass('sticky');
                  var currentDiff = Math.abs(currentTop - newTop);
                  if (self.stickyNavPoints.length > 2){
                      self.stickyNavPoints.push(self.stickyNavPoints.slice(-1).pop() + currentDiff + 1);
                  }else{
                      self.stickyNavPoints.push(self.stickyNavPoints.slice(-1).pop() + currentDiff - 20);
                  }
              }
          }else{
              if ((scroll) <= self.stickyNavPoints.slice(-2, -1).pop()){
                  var currentNav = $('.patient-history-append-results-break:eq('+ (self.stickyNavPoints.length - 2) + ')'),
                      currentTop = currentNav.position().top;
                  currentNav.removeClass('sticky');
                  var previousNav = currentNav.prevAll('.patient-history-append-results-break').first();
                  previousNav.css('top', modalTop).addClass('sticky');
                  if (self.stickyNavPoints.length > 2){
                      self.stickyNavPoints.pop();
                  }else{
                      $('.patient-history-append-results-break:eq(0)').addClass('sticky');
                  }
              }
          }
          var ratio = (self.additionalPrescriptionCurrentIndex() / (self.additionalPrescriptionCurrentIndex() + 1));
          if ((totalHeight * ratio) < ((scroll * ratio) + visibleHeight)){
              self.loadAdditionalPrescriptions();
          }
          if (!self.moreToLoad()){
              var lastSet = tbody.find('.patient-history-last-set');
              if (lastSet.length){
                  var top = lastSet.offset().top,
                      distanceToBottom = (top - visibleHeight);
                  self.showNoMoreResults((scroll + visibleHeight) > (totalHeight - 50));
              }
          }
          self.lastScrollValue(scroll);
      });

      self.lastScrollValue = ko.observable(0);

    self.prescriptions = ko.observableArray([]);
    self.isShown = ko.observable(false);
    self.show = function() {
      if (self.prescriptions().length == 0) {
        self.loadAdditionalPrescriptions(function(){
            self.$el.modal('show');
        });
      }
      else {
        self.$el.modal('show');
      }
    };
    self.hide = function() {
      self.$el.modal('hide');
    };
      self.additionalPrescriptionCurrentIndex = ko.observable(0);
      self.totalLoadCount = ko.observable(0);
      self.moreToLoad = ko.observable(true);

      self.patient = ko.observable();
      self.lastRx = ko.observable();
      self.patientName = ko.computed(function() {
          var patient = self.patient();
          if (patient) return patient.patientFirstName() + ' ' + patient.patientLastName();

      });

      self.dob = ko.computed(function() {
          var patient = self.patient();
          if (patient) return moment(patient.patientDOB()).format('M/D/YY');
      });

      self.prescriber = ko.computed(function() {
          var patient = self.patient();
          if (patient) return patient.prescriber.firstName() + ' ' + patient.prescriber.lastName();
      });

      self.prescriberPhone = ko.computed(function() {
          var patient = self.patient();
          if (patient) return patient.prescriber.phone();
      });

      self.prescriptions.subscribe(function(newValue){
          if (newValue.length == self.totalLoadCount()){
              self.moreToLoad(false);
              var prescription = newValue.splice(-1, 1).shift();
              self.lastRx(prescription);
          }
      });
      self.isLoading = ko.observable(false);

      self.loadAdditionalPrescriptions = function(openCallback){
          if (!self.moreToLoad() || self.isLoading()) return;
          self.isLoading(true);
          $.post(urls.www('ajax/prescription/get_patient_history'),
              {
                  current_index : self.additionalPrescriptionCurrentIndex(),
                  prescription_id : self.prescriptionId
              }, function (response){
                  var prescriptions = response.result.results,
                      total = prescriptions.length;
                  if (self.additionalPrescriptionCurrentIndex() == 0){
                      self.totalLoadCount(parseInt(response.result.total_count));
                  }

                  function loadResults(prescription, callback){
                      var last = prescriptions.length == 0,
                          first = (total - 1) == (prescriptions.length);

                      var prescriptionModel = new Prescription(prescription);
                      prescriptionModel.firstLoadItem(first);
                      prescriptionModel.lastLoadItem(last);
                      if(self.moreToLoad()){
                          self.prescriptions.push(prescriptionModel);
                      }else{
                          return;
                      }
                      if (prescriptionModel.firstLoadItem()){
                          if (self.additionalPrescriptionCurrentIndex() == 0 && !self.patient()){
                              self.patient(prescriptionModel);
                          }
                          var placeHolder = $('#patient-history-load-first-prescription'),
                              timesTen =  parseInt(self.additionalPrescriptionCurrentIndex() * 10);

                          placeHolder.find('td').text('Results '+ (timesTen + 1) +' through '+ (timesTen + total) + ' of ' + self.totalLoadCount());
                          placeHolder.addClass('patient-history-append-results-break').removeClass('hide');
                          placeHolder.removeAttr('id');
                      }
                      if(prescriptionModel.lastLoadItem()){
                          callback(openCallback);
                      }else{
                          loadResults(prescriptions.shift(), callback);
                      }
                  }

                  loadResults(prescriptions.shift(), function(openCallback){
                      self.additionalPrescriptionCurrentIndex(self.additionalPrescriptionCurrentIndex() + 1);
                      self.isLoading(false);
                      if (openCallback){
                          openCallback();
                      }
                  });
              });
      }
  };
});
