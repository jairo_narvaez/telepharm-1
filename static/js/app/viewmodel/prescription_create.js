define(function(require, exports, module) {
  'use strict';
  /*global alert, page*/
  var $ = require('jquery'),
  ko = require('knockout'),
  VideoVM = require('app/viewmodel/videoconf'),
  PrescriptionOptionsVM = require('app/viewmodel/prescriptionoptions'),
  PrescriptionImg = require('app/model/prescriptionimage'),
  Urls = require('app/model/urls'),
  Prescription = require('app/model/prescription'),
  PrescriptionDrug = require('app/model/prescriptiondrug'),
  PrescriptionDispense = require('app/model/prescriptiondispense'),
  NewCommentVM = require('app/viewmodel/newcomment'),
  PrescriptionCommentsVM = require('app/viewmodel/prescriptioncomments'),
  PatientHistoryVM = require('app/viewmodel/patienthistory'),
  NotificationsVM = require('app/viewmodel/notifications'),
  UploadWizardVM = require('app/viewmodel/uploadwizard'),
  TabletSyncVM = require('app/viewmodel/tablet_sync'),
  //CreateChooserVM = require('app/viewmodel/createchooser'),
  Mousetrap = require('mousetrap'),
  PrescriptionTransferVM = require('app/viewmodel/prescriptiontransfer'),
  PrescriptionPrescriber = require('app/model/prescriptionprescriber'),
  AudioAlertHelper = require('app/helpers/audioalert');

  return function() {
    var self = this;

    var account = $.parseJSON($('#account-data').val());
    var prescription = $.parseJSON($('#prescription-data').val());

    self.prescription = new Prescription(prescription);
    self.videoConf = new VideoVM(account.is_available == '1');
    self.prescriptionOptions = new PrescriptionOptionsVM(self.prescription);

    self.patientHistory = new PatientHistoryVM(self.prescription.patientId(), self.prescription.prescriptionId());

    self.prescriptionDrug =
    new PrescriptionDrug(prescription.prescription_drug);
    self.prescriptionDispense =
    new PrescriptionDispense(prescription.prescription_dispense);
    self.prescriptionPrescriber =
        new PrescriptionPrescriber(prescription.prescription_prescriber);

    self.prescriptionTransfer = new PrescriptionTransferVM(self.prescription);
    self.wizard = new UploadWizardVM(self.prescription, self);
    self.newComment = new NewCommentVM(self.prescription);
    self.commentsVM = new PrescriptionCommentsVM(self.prescription);

    self.audioAlertHelper = new AudioAlertHelper(account);
    self.notifications = new NotificationsVM(self);
    //self.createChooser = new CreateChooserVM();

    self.tabletSync = new TabletSyncVM();

    self.numberOfBackgroundUploads = 0;

    $(window).bind('beforeunload', function(e) {
      if (self.numberOfBackgroundUploads>0)
      {
        return 'There are some upload jobs pending to process.';
      }
    });

//    Mousetrap.bind('left', function() {
//      if (self.createChooser.isShown()) {
//        self.createChooser.processReturn();
//        return;
//      }
//    });
//    Mousetrap.bind('right', function() {
//      if (self.createChooser.isShown()) {
//        self.createChooser.processNext();
//        return;
//      }
//    });

    self.onImageSelected = function(model, data, element_box, callback) {
      var url = Urls.www('ajax/prescription/store_image');

      element_box.addClass('uploading');

      self.numberOfBackgroundUploads+=1;
      $.post(url, {
        picture: data,
        picture_type: model.type(),
        prescription_id: model.prescription_id(),
        display_order: model.displayOrder(),
        id: model.id()
      }, function(data, status, response) {
        self.numberOfBackgroundUploads-=1;
        ee.emitEvent('finish-picture-upload');
        if (data.error) {
          alert('The file you are trying to upload is invalid, please ' +
            'verify the file is an image');
        }
        else if (data.result.hasOwnProperty('prescription_image')) {
          var prescriptionImage =
          new PrescriptionImg(data.result.prescription_image);
          self.onSuccessImageUploaded(prescriptionImage,
            element_box.find('img'), callback);
        }
      }, 'json').error(function() {
        alert('The file you are trying to upload is invalid, please verify ' +
          ' the file is an image');
      })
      .complete(function() {
              element_box.removeClass('uploading');
          });
    };

    self.onSuccessImageUploaded = function(model, element, callback) {
      ko.utils.arrayForEach(self.prescription.images(), function(image) {
        if (parseInt(model.displayOrder(), 10) ==
            parseInt(image.displayOrder(), 10)) {
          self.prescription.images.remove(image);
          self.prescription.images.push(model);
            if (callback){
                callback();
            }
        }
      });
    };
  };
});
