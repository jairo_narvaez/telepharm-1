define(function(require, exports, module) {
  var ko = require('knockout'),
      VideoVM = require('app/viewmodel/videoconf'),
      CurrentUser = require('app/viewmodel/currentuser'),
      NewCommentVM = require('app/viewmodel/newcomment'),
      RejectCommentVM = require('app/viewmodel/rejectcomment'),
      AuxiliaryLabelsVM = require('app/viewmodel/auxiliarylabels'),
      Prescription = require('app/model/prescription'),
      PrescriptionDrug = require('app/model/prescriptiondrug'),
      PrescriptionDispense = require('app/model/prescriptiondispense'),
      PrescriptionPrescriber = require('app/model/prescriptionprescriber'),
      PrescriptionImg = require('app/model/prescriptionimage'),
      PrescriptionCommentsVM = require('app/viewmodel/prescriptioncomments'),
      PrescriptionTransferVM = require('app/viewmodel/prescriptiontransfer'),
      PatientHistoryVM = require('app/viewmodel/patienthistory'),
      NotificationsVM = require('app/viewmodel/notifications'),
      AudioAlertHelper = require('app/helpers/audioalert'),
      ChooserVM = require('app/viewmodel/chooser'),
      ApproverVM = require('app/viewmodel/approver'),
      RejecterVM = require('app/viewmodel/rejecter'),
      ReportErrorVM = require('app/viewmodel/reporterror'),
      QueueVM = require('app/viewmodel/queue'),
      TabletSyncVM = require('app/viewmodel/tablet_sync'),
      Mousetrap = require('mousetrap'),
      urls = require('app/model/urls'),
      ModalHelper = require('app/helpers/modal'),
      TechnicianCompleteVM = require('app/viewmodel/technician_complete'),
      EditPrescriptionVM = require('app/viewmodel/prescription_edit'),
      PrescriptionImageLightBoxModalVM = require('app/viewmodel/prescriptionimagemodal_lightbox');

  return function() {
    var self = this;

    var account = $.parseJSON($('#account-data').val());
    var prescription = $.parseJSON($('#prescription-data').val());
    var call = $.parseJSON($('#start-call').val());
    self.currentUser = new CurrentUser(account);
    self.videoConf = new VideoVM(account.is_available == '1',
    'patient-video',
    'local-video');
    clearInterval(self.videoConf.incomingIntervalID);
    if (call.call_id != null) {
      self.videoConf.currentCall(call.call_id);
      $('.counsel-comments').modal({
        keyboard: false
      });
        self.videoConf.checkForCallStatus();
    }
      self.videoConf.tokbox.addListener('streamDestroyed', function() {
          $('.counsel-comments').modal('hide');
          self.prescription.updateFromServer();
      });
      self.prescriptionTransfer = new PrescriptionTransferVM();
    self.prescription = new Prescription(prescription);
      if (self.prescription.transfer){
          self.prescriptionTransfer.load(self.prescription);
      }
      self.technicianComplete = new TechnicianCompleteVM(self.prescription);
      self.prescriptionEdit = new EditPrescriptionVM(self.prescription);

      self.prescriptionDrug =
        new PrescriptionDrug(prescription.prescription_drug);
    self.prescriptionDispense =
        new PrescriptionDispense(prescription.prescription_dispense);
    self.prescriptionPrescriber =
        new PrescriptionPrescriber(prescription.prescription_prescriber);

    self.newComment = new NewCommentVM(self.prescription, self.videoConf);
    self.auxiliaryLabels = new AuxiliaryLabelsVM(self.prescription);
    self.commentsVM = new PrescriptionCommentsVM(self.prescription);
    self.rejectComment = new RejectCommentVM(self.prescription);
    self.additionalPrescriptions = ko.observableArray([]);
      self.additionalPrescriptionCurrentIndex = ko.observable(1);
      self.totalLoadCount = ko.observable(parseInt($('#total_load_count').val()));
      self.currentLoadCount = ko.observable(parseInt($('#current_load_count').val()));
      self.moreToLoad = ko.observable(self.currentLoadCount() < self.totalLoadCount());

      self.loadPrescriptionImages = function(){
          var element = $('#patient-info-print');

          var images = self.prescription.sortedImages().slice(0);

          var placeHolderClone = $('#image-placeholder').clone();

          placeHolderClone.find('img.fake').removeClass('hide');
          element.find('.prescription-image-li').append(placeHolderClone.clone().html());

          function showImages(realImages, fakeImages){
              fakeImages.remove();

              realImages.on('click', function(){
                  var image_id = $(this).attr('data-image-id');
                  $('#lightbox-image-' + image_id).lightbox();
              });
              realImages.show();
          }
          var ts = new Date();
          function loadImages(image, callback, realImages, fakeImages){
              realImages = realImages || [];
              fakeImages = fakeImages || [];
              if(image){
                  self.prescriptionImageLightBoxModalCollection.push(new PrescriptionImageLightBoxModalVM(image, ts));
                  var liElement = element.find('#prescription-image-li-' + image.id());
                  liElement.find('img.real').on('load', function(){
                      var realImage = $(this),
                          fakeImage = realImage.parent().find('img.fake');
                      realImages = $.merge(realImage, realImages);
                      fakeImages = $.merge(fakeImage, fakeImages);
                      loadImages(images.shift(), callback, realImages, fakeImages);
                  }).on('error', function(){
                          loadImages(images.shift(), callback, realImages, fakeImages);
                      }).attr({ alt : image.imageType(), src : urls.www('image/get/' + image.image_id()) + '?original=true&ts=' + ts.getTime(), id : 'prescription-image-' + image.image_id(), 'data-image-id' : image.image_id() });
              }else{
                 callback(realImages, fakeImages);
              }
          }
          loadImages(images.shift(), showImages);
     }

      self.prescriptionImageLightBoxModalCollection = ko.observableArray([]);

      self.createAdditionalPrescriptionHandlers = function(){
          var modalHelper = new ModalHelper(),
              element = $('#additional-rx');
          modalHelper.scrollLock(element);
      }

      self.loadAdditionalPrescriptions = function(){
          $.post(urls.www('ajax/prescription/get_patient_history'),
              {
                 current_index : self.additionalPrescriptionCurrentIndex(),
                 prescription_id : self.prescription.prescriptionId()
              }, function (response){
                  var prescriptions = response.result.results,
                      total = prescriptions.length;
                  self.currentLoadCount(self.currentLoadCount() + total);
                  self.moreToLoad(self.currentLoadCount() < self.totalLoadCount());
                  function loadResults(prescription, callback){
                      var last = prescriptions.length == 1,
                          first = total == (prescriptions.length + 1);

                      var prescriptionModel = new Prescription(prescription);
                      prescriptionModel.firstLoadItem(first);
                      prescriptionModel.lastLoadItem(last);
                      self.additionalPrescriptions.push(prescriptionModel);
                      if (prescriptionModel.firstLoadItem()){
                          var placeHolder = $('#load-first-prescription');
                          placeHolder.find('p').text('Results '+ (parseInt(self.additionalPrescriptionCurrentIndex() * 10) +1) +' through '+ parseInt((self.additionalPrescriptionCurrentIndex() * 10) + total) + ' of ' + self.totalLoadCount());
                          placeHolder.addClass('append-results-break').removeClass('hide');
                          placeHolder.removeAttr('id');
                      }
                      if(prescriptionModel.lastLoadItem()){
                          callback();
                      }else{
                          loadResults(prescriptions.shift(), callback);
                      }
                  }

                  loadResults(prescriptions.shift(), function(){
                      self.additionalPrescriptionCurrentIndex(self.additionalPrescriptionCurrentIndex() + 1);
                  });
          });
      }

    self.audioAlertHelper = new AudioAlertHelper(account);
    self.notifications = new NotificationsVM(self);
    self.chooser = new ChooserVM();
    self.approver = new ApproverVM();
    self.rejecter = new RejecterVM();
    self.tabletSync = new TabletSyncVM();
    self.completeTransfer = function(){
        self.prescription.transfer.complete(function(statuses){
            self.prescription.loadStatuses(statuses, function(){
                window.print();
            });
        });
    }
  self.print = function(){
      window.print();
  }

    // Bind keyboard left/right arrows to actions
    self.queue = new QueueVM();
    clearInterval(self.queue.updateQueueIntervalID);
    self.queue.updatePatientQueue(); // Get the patient queue
    Mousetrap.bind('left', function() {
      if (self.chooser.isShown()) {
        self.chooser.processReturn();
        return;
      }
      // go to previous rx on queue
      var current = null;
      ko.utils.arrayForEach(self.queue.prescriptions(), function(item) {
        if (item.prescriptionId() == self.prescription.prescriptionId())
          current = self.queue.prescriptions.indexOf(item);
      });
      if (current == 0) {
        // no more previous rx
        self.chooser.processReturn();
      }
      else if (current > 0)
      {
        var rx = self.queue.prescriptions()[current - 1];
        window.location = urls.www('prescription/view/' + rx.prescriptionId());
      }
    });
    Mousetrap.bind('right', function() {
      if (self.chooser.isShown()) {
        self.chooser.processNext();
        return;
      }
      // go to next rx on queue
      var current = null;
      ko.utils.arrayForEach(self.queue.prescriptions(), function(item) {
        if (item.prescriptionId() == self.prescription.prescriptionId())
          current = self.queue.prescriptions.indexOf(item);
      });
      if (current == (self.queue.prescriptions().length - 1)) {
        // no more next rx
        self.chooser.processReturn();
      }
      else if (current < self.queue.prescriptions().length)
      {
        var rx = self.queue.prescriptions()[current + 1];
        window.location = urls.www('prescription/view/' + rx.prescriptionId());
      }
    });

    self.reportError = new ReportErrorVM(self.prescription);

    self.patientHistory = new PatientHistoryVM(self.prescription.patientId(),
    self.prescription.prescriptionId());

      updateWaitingTime = function() {
      self.prescription.tick();
    };

    setInterval(updateWaitingTime, 1000);
          pool.add(new DI.ajaxPolling.request('ajax/prescription/get/' + self.prescription.prescriptionId(), {}, function(response) {
        self.prescription.processFromServer(response, function(prescription){
            if ((prescription.approvedOnHold() || prescription.completed()) && !(prescription.isTransfer() || prescription.completedTransfer())){
                if (!self.prescriptionTransfer.isLoaded()){
                    self.prescriptionTransfer.load(prescription);
                }
            }
        });
    }, AjaxIntervalManager.getInterval('update_prescription', DI.ajaxPolling.priority.low)));

    self.drugInformationDisplay = ko.observable(
        !self.prescription.isAwaitingApproval() ||
        !self.prescription.isAwaitingCounsel());
    self.dispenseInformationDisplay = ko.observable(
        !self.prescription.isAwaitingApproval() ||
        !self.prescription.isAwaitingCounsel());

    window.page = self;
  };
});
