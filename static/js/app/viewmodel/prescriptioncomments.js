define(function(require, exports, module) {
  var ko = require('knockout'),
    PrescriptionComment = require('app/model/prescriptioncomment'),
    PrescriptionLog = require('app/model/prescriptionlog'),
    urls = require('app/model/urls');

  return function(prescription) {
    var self = this;
//    var loadedComments = $.parseJSON($('#prescription-comments-data').val());
//    var loadedHistory = $.parseJSON($('#prescription-log-data').val());

    self.allComments = ko.observableArray();
    self.allHistory = ko.observableArray();

//    ko.utils.arrayForEach(loadedComments || [], function(comment) {
//      self.allComments.push(new PrescriptionComment(comment));
//    });
//
//    ko.utils.arrayForEach(loadedHistory || [], function(log) {
//      self.allHistory.push(new PrescriptionLog(log));
//    });

    self.findLastCommentId = function() {
        var id = 0;
        ko.utils.arrayForEach(self.allComments() || [], function(comment) {
            var comment_id = parseInt(comment.id());
            if (comment_id > id)
                id = comment_id;
        });
        return id;
    };

    self.lastCommentId = self.findLastCommentId();

    var fetchNewComments = function()
    {
        $.get(urls.www("ajax/comments/for_prescription?prescription_id=" + prescription.prescriptionId() +"&last_comment_id=" + self.findLastCommentId()), function(data) {
            self.processNewComments(data);
        });
    };

    var processNewComments = function(response)
    {
        if (response.result.length > 0) {
             ko.utils.arrayForEach(response.result || [], function(comment) {
                self.allComments.push(new PrescriptionComment(comment));
            });
            self.lastCommentId = self.findLastCommentId();
        }
    };

    self.totalUnread = ko.computed(function() {
        var total = 0;
        ko.utils.arrayForEach(self.allComments() , function(comment) {
            if (!comment.isRead()) total++;
        });
        return total;
    });

    self.dynRequest = new DI.ajaxPolling.request("ajax/comments/for_prescription", {query: {prescription_id: prescription.prescriptionId(), last_comment_id: self.findLastCommentId()}}, function(response) {
        processNewComments(response);
        self.dynRequest.data = {query: {prescription_id: prescription.prescriptionId(), last_comment_id: self.findLastCommentId()}}; // refresh the last comment id
    }, AjaxIntervalManager.getInterval('prescription_comments', DI.ajaxPolling.priority.high));

    pool.add(self.dynRequest);
    
    self.findLastLogId = function() {
        var id = 0;
        ko.utils.arrayForEach(self.allHistory() || [], function(log) {
            var log_id = parseInt(log.id());
            if (log_id > id)
                id = log_id;
        });
        return id;
    };

    self.lastLogId = self.findLastLogId();

    var fetchNewHistory = function()
    {  
        $.get(urls.www('ajax/logs/for_prescription?prescription_id=' + prescription.prescriptionId() + '&last_log_id=' + self.findLastLogId()), function(data) {
            self.processNewHistory(data);
        });
    };

    var processNewHistory = function(response) {
       
        if (response.result.length > 0) {
             ko.utils.arrayForEach(response.result || [], function(log) {
                self.allHistory.push(new PrescriptionLog(log));
            });
            self.lastLogId = self.findLastLogId();
        }
    };

    self.dynRequest2 = new DI.ajaxPolling.request("ajax/logs/for_prescription", {query: {prescription_id: prescription.prescriptionId(), last_log_id: self.findLastLogId()}}, function(response) {
        processNewHistory(response);
        self.dynRequest2.data = {query: {prescription_id: prescription.prescriptionId(), last_log_id: self.findLastLogId()}};
    }, AjaxIntervalManager.getInterval('update_history', DI.ajaxPolling.priority.tiny));

    pool.add(self.dynRequest2);
  };
});
