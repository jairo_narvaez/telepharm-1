define(function(require){
   var ko = require('knockout'),
       urls = require('app/model/urls');

    return function(prescriptionimage, ts){
        var self = this,
            callback = function(image_id){
                var d = new Date(),
                    updatedSrc = urls.www('image/get/') + image_id + '?original=true&ts=' + d.getTime();
                $('#lightbox-image-' + image_id).find('.full-image').attr('src', updatedSrc);
                $('#prescription-image-' + image_id).attr('src', updatedSrc);
            };
        self.timeStamp = ts;
        self.prescriptionImage = prescriptionimage;

        self.rotateClockwise = function(){
            self.prescriptionImage.rotate(1, callback);
        }
        self.rotateCounterClockwise = function(){
            self.prescriptionImage.rotate(2, callback);
        }
    }
});