define(function(require, exports, module) {
  var ko = require('knockout'),
      PrescriptionOption = require('app/model/prescriptionoption');

  return function(prescription) {
    var self = this;
    self.prescription = prescription;
    self.allOptions = ko.observableArray();
    self.allOptions.push(new PrescriptionOption({
      label: 'New',
      prescription: prescription,
      name: 'is_new',
      checked: prescription.isNew()
    }));
    self.allOptions.push(new PrescriptionOption({
      label: 'Partial',
      prescription: prescription,
      name: 'is_partial',
      checked: prescription.isPartial()
    }));
    self.allOptions.push(new PrescriptionOption({
      label: 'On Hold',
      prescription: prescription,
      name: 'on_hold',
      checked: prescription.approvedOnHold() ? false : prescription.onHold()
    }));
    self.allOptions.push(new PrescriptionOption({
      label: 'Re fill',
      prescription: prescription,
      name: 'is_refill',
      checked: prescription.isRefill()
    }));
  };
});
