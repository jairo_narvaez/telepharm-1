define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery');
    var ModalHelper = require('app/helpers/modal');
    return function(prescription) {
        var self = this,
            isShown = false,
            element = $('#prescription-transfer-modal'),
            modal = element.modal({
                show: false
            });
        var body = $('body'),
            modalHelper = new ModalHelper();
        self.isLoaded = ko.observable(false);

        self.prescription = null;

        modal.on('hidden', function(){
            if (body.css('overflow-y') != 'scroll'){
                body.css('overflow-y', 'scroll');
                if (body.width() > 1020){
                    body.css('margin-left', 0);
                }
            }

            isShown = false;
        });

        modal.on('shown', function(){
           isShown = true;
        });

        self.show = function() {
            modal.modal('show');
        };

        //prep modal for display
        var prepare = function(callback){
            var rxClone = $('#patient-info-print .patient-info').clone(),
                modalRx = $('#transfer-rx-info');

            modalRx.children().remove();
            modalRx.append(rxClone);

            modalHelper.scrollLock(element);
            if (callback){
                callback();
            }
        }



        self.ok = function(){
            element.find('#transfer-errors .fader').fadeOut({duration : 300, queue : true, complete :function(){
                $(this).parent().slideUp({duration : 300, queue : true, complete : function(){
                    self.prescription.transfer.initiate(self.prescription, function(statuses) {
                        self.prescription.loadStatuses(statuses);
                        modal.modal('hide');
                    });
                }});
            }});
        }

        self.close = function(){
            element.find('#transfer-errors').slideUp({duration : 300, queue : true });
            modal.modal('hide');
        }

        self.load = function(prescription) {
            //if view modal is already loaded, don't try to load it again.
            if (self.isLoaded()) return;

            self.prescription = prescription;

            var displayControls = function(newValue, selector){
                var hideSelector, showSelector, root = element.find(selector);

                if(newValue){
                    hideSelector = '.select-existing';
                    showSelector = '.create-new';
                }else{
                    hideSelector = '.create-new';
                    showSelector = '.select-existing';
                }

                var selectorToHide = getHeight(root.find(hideSelector)),
                    selectorToShow = getHeight(root.find(showSelector));

                var scroll = function(showSelectorCallback){
                    root.parent().scrollTop(root.position().top);
                    if (showSelectorCallback){
                        showSelectorCallback();
                    }
                }

                selectorToHide.removeClass('hide').addClass('hide');
                selectorToHide.fadeOut({duration : 300, queue : true, complete : function(){
                    root.find('.fader').height(selectorToShow.attr('data-height'));
                    scroll(function(){
                        selectorToShow.delay(100).removeClass('hide').fadeIn({queue : true, complete : function(){

                        }});
                    })
                }});
            }

            var getHeight = function(element){
                if (element && element.length && !element.attr('data-height')){
                    var displayComingIn = element.css('display');
                    element.css({
                        display : 'block'
                    });
                   var height = element.height();
                    element.css({
                        display : displayComingIn
                    });
                }
                if (height){
                    element.attr('data-height', height);
                }
                return element;
            }

            self.prescription.transfer.useNewPharmacist.subscribe(function(newValue){
                displayControls(newValue, '#transfer-pharmacist');
                return true;
            });

            self.prescription.transfer.useNewPharmacy.subscribe(function(newValue){
                displayControls(newValue, '#transfer-pharmacy');
                return true;
            });

            self.prescription.transfer.errorMessages.subscribe(function(newValue){
                if (newValue && newValue.length > 0) {
                    element.find('#transfer-errors')
                        .removeClass('hide')
                        .slideDown({duration : 300, queue : true, complete : function(){
                            $(this).find('.fader').fadeIn({duration : 300, queue : true, complete : function(){
                                var controls = element.find('.controls');
                                controls.animate({ scrollTop : 0, queue : true}, 500);
                            }});
                        }});
                }
                return true;
            });
            self.isLoaded(true);
            prepare(function(){
                self.prescription.transfer.prepare(function(currentState){
                    //this is a hack until I can get internal stores to look up address by state_id.
                    var stateSelect = $('#transfer-state'),
                        currentStateId = stateSelect.find('option[data-state-abbr="'+currentState +'"]').val();
                    stateSelect.val(currentStateId);
                });
            });
        }
    };
});
