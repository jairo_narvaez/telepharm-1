define(function(require, exports, module) {
  var ko = require('knockout'),
      Prescription = require('app/model/prescription'),
      Urls = require('app/model/urls'),
      AlertTone = require('app/helpers/alerttone');

  return function() {
    var self = this;
    self.prescriptions = ko.observableArray();
    self.searchResult = ko.observableArray();
    self.highPriorityAudioAlert = new AlertTone({name: 'High Priority Alert', fileName : 'Looking_Up.mp3'});
    var loadedPrescripions = $.parseJSON($('#prescriptions-data').val());

    ko.utils.arrayForEach(loadedPrescripions || [], function(rx) {
      self.prescriptions.push(new Prescription(rx));
    });

    self.allCount = ko.computed(function() {
      return self.prescriptions().length;
    });

    self.rejectedCount = ko.computed(function() {
      var count = 0;
      ko.utils.arrayForEach(self.prescriptions(), function(prescription) {
        if (prescription.isRejected()) count++;
      });
      return count;
    });

    self.awaitingCounselCount = ko.computed(function() {
      var count = 0;
      ko.utils.arrayForEach(self.prescriptions(), function(prescription) {
        if (prescription.isAwaitingCounsel()) count++;
      });
      return count;
    });

    self.transferCount = ko.computed(function() {
      var count = 0;
      ko.utils.arrayForEach(self.prescriptions(), function(prescription) {
        if (prescription.isTransfer()) count++;
      });
      return count;
    });

    self.awaitingApprovalCount = ko.computed(function() {
      var count = 0;
      ko.utils.arrayForEach(self.prescriptions(), function(prescription) {
        if (prescription.isAwaitingApproval() && !prescription.isTransfer()) count++;
      });
      return count;
    });

    self.draftCount = ko.computed(function() {
      var count = 0;
      ko.utils.arrayForEach(self.prescriptions(), function(prescription) {
        if (prescription.isDraft()) count++;
      });
      return count;
    });

    self.changeSort = function(newSort) {
      if (self.sortBy() == newSort)
        self.sortByOrder(!self.sortByOrder());
      else
        self.sortByOrder(true);
      self.sortBy(newSort);
    };

    self.getSort = function(columnSort) {
      var directionChar = self.sortByOrder() ? 'asc' : 'desc';
      if (self.sortBy() == columnSort)
        return directionChar;
      return '';
    };

    self.filterPrescriptionBy = ko.observable('all');
    self.sortBy = ko.observable('default');
    self.sortByOrder = ko.observable(false);

    self.sortQueue = function() {
      var direction = self.sortByOrder() ? 1 : -1;
      var patientName = function(a, b) {
        return a.patientName() == b.patientName() ? 0 :
               (a.patientName() > b.patientName() ? -direction : direction);
      };
      var number = function(a, b) {
        return a.fullNumber() == b.fullNumber() ? 0 :
               (a.fullNumber() > b.fullNumber() ? -direction : direction);
      };
      var date = function(a, b) {
        return a.createdDt() == b.createdDt() ? 0 :
               (a.createdDt() > b.createdDt() ? -direction : direction);
      };
      var priority = function(a, b) {
        return a.priority() == b.priority() ? 0 :
               (a.priority() > b.priority() ? -direction : direction);
      };
      var defaultSort = function(a, b) {
        var aSortField = '';
        var bSortField = '';
        if (window.location.pathname == '/technician'){
          if (a.isRejected()) aSortField = '2';
          if (a.isAwaitingApproval()) aSortField = '4';
          if (a.isAwaitingCounsel()) aSortField = '3';
          if (a.isDraft()) aSortField = '1';

          if (b.isRejected()) bSortField = '2';
          if (b.isAwaitingApproval()) bSortField = '4';
          if (b.isAwaitingCounsel()) bSortField = '3';
          if (b.isDraft()) bSortField = '1';
        } else {
          if (a.isAwaitingApproval()) aSortField = '1';
          if (a.isRejected()) aSortField = '2';
          if (a.isAwaitingCounsel()) aSortField = '3';

          if (b.isAwaitingApproval()) bSortField = '1';
          if (b.isRejected()) bSortField = '2';
          if (b.isAwaitingCounsel()) bSortField = '3';
        }

        if (aSortField == bSortField) 
        {
           return a.considerHighPriority() == b.considerHighPriority() ? 0 :
             (a.considerHighPriority() > b.considerHighPriority() ? -1 : 1);
        } else {
           return aSortField > bSortField ? -direction : direction;
        }
      };

      var functionSelected = function(a, b) {
        if (a.considerHighPriority() == b.considerHighPriority()) return 0;
        return (a.considerHighPriority() < b.considerHighPriority() ? 1 : -1);
      };

      switch (self.sortBy())
      {
        case 'patient-name':
          functionSelected = patientName;
          break;
        case 'number':
          functionSelected = number;
          break;
        case 'date':
          functionSelected = date;
          break;
        case 'priority':
          functionSelected = priority;
        case 'default':
          functionSelected = defaultSort;
          break;
      }

      self.prescriptions.sort(functionSelected);
    };

    self.prescriptionsToShow = ko.computed(function() {
      if (self.filterPrescriptionBy() == 'search')
      {
        return self.searchResult();
      }
      else
      {
        var result = [];
        self.sortQueue();
        ko.utils.arrayForEach(self.prescriptions(), function(prescription) {
          switch (self.filterPrescriptionBy()) {
            case 'all':
              result.push(prescription);
              break;
            case 'rejected':
              if (prescription.isRejected()) {
                result.push(prescription);
              }
              break;
            case 'approved':
              if (prescription.isAwaitingApproval() && !prescription.isTransfer()) {
                result.push(prescription);
              }
              break;
            case 'transfer':
              if (prescription.isTransfer()) {
                result.push(prescription);
              }
              break;
            case 'counsel':
              if (prescription.isAwaitingCounsel()) {
                result.push(prescription);
              }
              break;
            case 'draft':
              if (prescription.isDraft()) {
                result.push(prescription);
              }
              break;
            default:
              alert('error on type of filter');
              break;
          }
        });
        return result;
      }
    });

    self.updatePatientQueue = function()
    {
      $.getJSON(Urls.www('ajax/prescription/get_queue'), function(data) {
        self.processPatientQueue(data);
      });
    };
      var has_high_priority = true;
    self.processPatientQueue = function(response) {
      
      if (response.error) {
        page.modal.reset().title('Error').content('Error trying to get the patient queue').show();
        return;
      }
        has_high_priority = true;
      self.prescriptions([]);
      ko.utils.arrayForEach(response.result, function(rx) {
          var model = new Prescription(rx);
          if (model.isHighPriority() && !model.inFinalStatus() && !model.rejected() && !model.isDraft() && !model.approvedForCounsel() && window.location.pathname == '/pharmacist'){
            if (has_high_priority){
                self.highPriorityAudioAlert.play();
                has_high_priority = false;
            }
              model.highlight(true);
          }
        self.prescriptions.push(model);
      });

        self.filterPrescriptionBy(self.filterPrescriptionBy());
    };

    pool.add(new DI.ajaxPolling.request("ajax/prescription/get_queue", {}, function(response) {
        self.processPatientQueue(response);
    }, AjaxIntervalManager.getInterval('get_queue', DI.ajaxPolling.priority.low)));

  };
});


