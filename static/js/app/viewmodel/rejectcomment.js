/*jslint browser: true, indent: 2, maxlen: 80 */
define(function(require, exports, module) {
  'use strict';
  var ko = require('knockout'),
    $ = require('jquery'),
    urls = require('app/model/urls');

  return function(prescription) {
    var self = this;
    self.$el = $('#reject-comment-modal').modal({
      show: false,
      keyboard: false,
      backdrop: false
    });
    self.prescription = prescription;
    self.text = ko.observable('');
    self.callback = null;
    self.setCallback = function(callback) {
      self.callback = callback;
    };
    self.save = function() {
      if ($.trim(self.text()) == '') {
        page.modal.error('Please enter the text of the comment').show();
        return;
      }
      $.post(urls.www('ajax/comments/create'), {
        prescription_id: self.prescription.prescriptionId(),
        text: self.text()
      }, function(response) {
        if (response.error) {
          page.modal.error('Sorry, the comment could not be saved').show();
          return;
        }
        self.text('');
        self.$el.modal('hide');
        if (typeof self.callback == 'function') self.callback();
      });
    };
    self.show = function() {
      self.$el.modal('show');
    };
  }
});

