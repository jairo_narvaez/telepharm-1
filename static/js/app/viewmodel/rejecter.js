define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        urls = require('app/model/urls');

    return function() {
        var self = this;

        self.url = null;

        self.$el = $('#reject-modal').modal({
            show: false
        });

        self.isShown = function() {
            return self.$el.css('display') != 'none';
        };

        self.show = function(url) {
            self.url = url;
            self.$el.modal('show');
        };

        self.hide = function(url) {
            self.url = url;
            self.$el.modal('hide');
        };

        self.processReturn = function() {
            self.$el.modal('hide');
            window.location = urls.www('/');
        };

        self.processNext = function(url) {
            self.url = url;
            if (self.url == null) return;
            self.$el.modal('hide');
            window.location = self.url;
        };
    };
});
