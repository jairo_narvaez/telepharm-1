define(function(require, exports, module) {
  var ko = require('knockout'),
  $ = require('jquery'),
  Urls = require('app/model/urls');

  return function(prescription) {
    var self = this;
    self.prescription = prescription;
    self.$el = $('#report-error-modal').modal({
      show: false,
      keyboard: false
    });

    self.errorText = ko.observable('');
    self.pharmacist = ko.observable('');
    self.technician = ko.observable('');
    self.disabled = ko.observable(false);
 
    if (self.prescription.canReportError() || self.prescription.reportedError()) {
      $.get(Urls.www('ajax/prescription/get_participants'), {
        prescription_id: self.prescription.prescriptionId()},
        function(response) {
          if (response.error) return;
          self.pharmacist(response.result.pharmacist);
          self.technician(response.result.technician);
        });
    }


    self.save = function() {
      $.post(Urls.www('ajax/prescription/report_error'), {
      prescription_id: self.prescription.prescriptionId(),
      error_text: self.errorText()},
      function(response) {
        if (response.error) {
          page.modal.error('An error occurred when trying ' +
          'to report the error').show();
          return;
        }
        self.prescription.loadStatuses(response.result);
      });
      self.$el.modal('hide');
    };

    self.showDisabled = function() {
      self.disabled(true);
      self.$el.modal('show');
    };

    self.show = function() {
      self.disabled(false);
      self.$el.modal('show');
    };

    self.dismiss = function() {
      self.$el.modal('hide');
    };
  };
});
