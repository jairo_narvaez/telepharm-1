define(function(require, exports, module) {
  var ko = require('knockout'),
      $ = require('jquery'),
      urls = require('app/model/urls'),
      moment = require('moment'),
      AdvancedSearch = require('app/model/advancedsearch'),
      Prescription = require('app/model/prescription');

  return function(queue) {
    var self = this;
    self.$elAdvSearch = $('#advanced-search').modal({
      keyboard: false,
      show: false
    });

    $('#fromDate').inputmask('mm/dd/yyyy', { 'autounmask': true });
    $('#toDate').inputmask('mm/dd/yyyy', { 'autounmask': true });

    self.queue = queue;
    self.query = ko.observable('');
    self.listenEnterForSearch = function(data, event) {
      if (event.keyCode == 13)
      {
        self.search();
      }
    };
    self.search = function() {
      $.getJSON(urls.www('ajax/prescription/search'),
      {q: self.query()},
      function(data) {
        self.queue.searchResult([]);
        ko.utils.arrayForEach(data.result || [], function(rx) {
          self.queue.searchResult.push(new Prescription(rx));
        });
        window.location.hash = 'q=' + self.query();
        self.queue.filterPrescriptionBy('search');
      });
    };

    self.advancedSearch = new AdvancedSearch;
    self.showAdvancedSearch = function() {
      self.$elAdvSearch.modal('show');
    };
    self.performAdvancedSearch = function() {
      self.$elAdvSearch.modal('hide');
      $.getJSON(urls.www('ajax/prescription/full_search'),
      $.parseJSON(ko.toJSON(self.advancedSearch)),
      function(data) {
        self.queue.searchResult([]);
        ko.utils.arrayForEach(data.result || [], function(rx) {
          self.queue.searchResult.push(new Prescription(rx));
        });
        self.queue.filterPrescriptionBy('search');
      });
    };
    $(function(){
      if (window.location.hash !== '')
      {
        self.query(window.location.hash.substring(3));
        self.search();
      }
    });
  };
});
