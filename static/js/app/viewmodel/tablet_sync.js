define(function(require, exports, module) {
  var ko = require('knockout'),
  $ = require('jquery'),
  PatientApiSession = require('app/model/patientapisession');
  urls = require('app/model/urls');

  return function() {
    var self = this;

    self.runSync = function()
    {
      currentSyncResponse();
    };

    self.currentSync = ko.observableArray();

    var currentSyncResponse = function()
    {
      $.get(urls.www('ajax/patienttablet/current'), function(data) {
        processSyncResponse(data);
      });
    };

    var processSyncResponse = function(response)
    {
      if (response.error) {
        page.modal.error(response.message).show();
      }

      self.currentSync([]);
      ko.utils.arrayForEach(response.result || [], function(session) {
        self.currentSync.push(new PatientApiSession(session));
      });
    };

    currentSyncResponse();
    pool.add(new DI.ajaxPolling.request("ajax/patienttablet/current", {}, function(response) {
      processSyncResponse(response);
    }, AjaxIntervalManager.getInterval('tablet_check', DI.ajaxPolling.priority.medium)));

    self.unSync = function(session) {
      $.post(urls.www('ajax/patienttablet/unsync'),
      { session_id: session.id() },
      function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        page.modal.error(session.humanName() +
        ' was deactivated').show();
      });

      currentSyncResponse();
    };
  };
});
