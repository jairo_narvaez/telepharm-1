define(function(require, exports, module) {
    var ko = require('knockout'),
        $ = require('jquery'),
        urls = require('app/model/urls');

    return function(prescription) {
        var self = this;
        var $el = $('#technician-complete-modal'),
            modal = $el.modal({show: false});

        self.open = function(){
            modal.modal('show');
        }

        self.close = function(){
            modal.modal('hide');
        }
        self.technicianCompleteTypeId = ko.observable(1);
        self.requiresInput = ko.observable(false);
        self.technicianCompleteTypeId.subscribe(function(newValue){
           if (newValue == 4){
                self.requiresInput(true);
           } else{
               self.requiresInput(false);
               self.message('');
           }
        });
        self.error = ko.observable();
        self.message = ko.observable('');
        self.complete = function (){
            self.error(null);
            if (self.requiresInput() && self.message().length == 0){
                self.error('You must provide a reason for selecting other.');
                return;
            }
            $.post(urls.www('ajax/prescription/complete_counsel'),
                {prescription_id : prescription.prescriptionId(),
                    technician_completion_type : self.technicianCompleteTypeId(),
                message : self.message()},
                function(response){
                    if (response.error){
                        self.error(response.message);
                    }else{
                        modal.modal('hide');
                        prescription.loadStatuses(response.result);
                    }
            });
        }
    }
});