define(function(require, exports, module) {
  var $ = require('jquery'),
      ko = require('knockout'),
      urls = require('app/model/urls'),
      PrescriptionImage = require('app/model/prescriptionimage');
  var CameraPictureHelper = require('app/helpers/camerapicture');

  return function(prescription, prescriptionCreateVM) {
    var self = this;

    self.camHelper = ko.observable(null);
    self.prescription = prescription;
    self.prescriptionCreateVM = prescriptionCreateVM;
    var uploadedImages = self.prescription.uploadedImages();
    var uploadedImagesLength = uploadedImages.length;

    self.selectedImage = ko.observable(uploadedImagesLength);

    var isNew = uploadedImages[0] || true;
    // event bus functions
    ee.addListener('take-picture', function() {
      // Hide take picture button and show loading
      $('#wizard-startbutton').hide();
      $('#wizard-loading').show();
    });
    ee.addListener('finish-picture-upload', function() {
      // Hide loading and show take picture button
      $('#wizard-loading').hide();
      $('#wizard-startbutton').show();
    });

    self.title = ko.computed(function() {
      switch (parseInt(self.selectedImage(), 10))
      {
        case 0:
          return 'Step 1 of 4: Capture Hardcopy Prescription';
        case 1:
          return 'Step 2 of 4: Capture Prescription Label';
        case 2:
          return 'Step 3 of 4: Capture Prescribed Physical Drug';
        default:
          return 'Step 4 of 4: Capture Any Additional Photos';
      }
    });

    self.addImage = function() {
      var i = self.prescription.images().length;
      var pi = new PrescriptionImage({});
      self.prescription.images.push(new PrescriptionImage({
            id: null,
            display_order: i,
            type: pi.getTypePerOrder(i),
            prescription_id: self.prescription.prescriptionId(),
            image_id: null}));
      self.selectedImage(i);
    };

    self.selectImage = function(image) {
        isNew = !image.exists();
        self.selectedImage(image.displayOrder());
    };

  self.expandImage = function(image) {
      self.selectedImage(image.displayOrder());
      
      page.modal.closeTitle('');
      page.modal.content(buildImageModal(image));
      page.modal.title(self.title());
      page.modal.okTitle('Ok');
      page.modal.callAfterOk(null);
      page.modal.show();
  };

      var buildImageModal = function(image){
          var clone = $('#uploaded-image-' + image.id()).clone();
          clone.attr('src', clone.attr('src') + '?original=true')
          var body = $('#image-expand-modal');
          body.children().remove();
          body.append(clone.attr('width', 500).attr('height', 500));
          var modal = $('<div>').append(body.clone().show());
          return modal.html();
      }

      self.deleteImage = function(image){
          page.modal.content(buildImageModal(image));
          page.modal.closeTitle('Cancel');
          page.modal.title('Delete Image For: ' + self.title());
          page.modal.okTitle('Delete Image');
          page.modal.callAfterOk(function(){
              image.delete(function(){
                  if (image.displayOrder() > 2){
                      self.prescription.images.remove(image);
                      self.selectedImage(parseInt(image.displayOrder()) - 1);
                  }else{
                      self.selectedImage(parseInt(image.displayOrder()));
                  }
              });
          });
          page.modal.show();
      }

    self.startCamera = function() {
      if (self.camHelper() === null) {
        self.camHelper(new CameraPictureHelper('#wizard-video',
        '#wizard-cover', '#wizard-canvas',
        '#wizard-photo', '#wizard-startbutton',
        '#wizard-selectpicture'));
      }
    };

    self.isCameraOn = ko.computed(function() {
      return self.camHelper() != null;
    });

    self.isReady = ko.computed(function() {
      return self.isCameraOn() && self.camHelper().isReady();
    });

    self.startCameraText = ko.computed(function() {
      return !self.isCameraOn() ? 'Click to Start Live Camera' :
      'Live Camera is Started';
    });

    self.takeShot = function() {
      if (self.camHelper() === null) {
        page.modal.error('Please Start the Live Camera').show();
        return;
      }
      ee.emitEvent('take-picture');
      self.camHelper().takePicture();
      if (self.selectedImage() == -1) {
        page.modal.error('Please select the image you want to replace').show();
        return;
      }
      var data = $('#wizard-photo').attr('src');
      var imageToUpload = null;
      ko.utils.arrayForEach(self.prescription.images(), function(image) {
        if (image.displayOrder() == self.selectedImage())
          imageToUpload = image;
      });
      if (imageToUpload !== null) {
        self.prescriptionCreateVM.onImageSelected(
          imageToUpload,
            data,
            $('#image-upload-wizard').find('.stream .body'),
            function(){
                if (self.prescription.uploadedImages().length > 2) {
                    self.addImage();
                }else{
                    self.selectedImage(self.prescription.uploadedImages().length)
                }
            }
          );
      }
    };

    if (self.prescription.uploadedImages().length > 2) {
      self.addImage();
    }
    self.startCamera();
  };
});

