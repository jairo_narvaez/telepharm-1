define(function(require, exports, module) {
  var ko = require('knockout'),
      urls = require('app/model/urls'),
      Notification = require('app/model/notification'),
      TokBox = require('app/helpers/tokbox');

  return function(userIsAvailable, subscribeVideoDiv, localVideoDiv) {
    var self = this;
    self.tokbox = new TokBox(self, subscribeVideoDiv, localVideoDiv);
    self.currentCall = ko.observable(null);
    self.callee = ko.observable('');
    self.currentCall.subscribe(function(val) {
      if (val == null)
        self.callee('');
    });
    self.acceptedVideoCall = ko.observable(null);
    self.isAvailable = ko.observable(userIsAvailable);

    self.isAvailable.subscribe(function(newVal) {
      $.post(urls.www('ajax/videocall/toggle_availability'),
          {availability: newVal},
          function(response) {
            if (response.error)
              page.modal.error('An error occurred during availability toggling').show();
          });
    });

    self.changeAvailabilityForVideoConf = function() {
      self.isAvailable(!self.isAvailable());
    };

    self.checkForCallStatus = function() {
      if (self.currentCall() == null) return;
      $.get(urls.www('ajax/videocall/check_call'),
      {call_id: self.currentCall()},
      function(response) {
        if (response.error) {
          self.currentCall(null);
          page.modal.error('An error occurred while checking call status').show();
          return;
        }

        if (response.result.answered) {
          self.currentCall(null);
          var callInfo = response.result.call;
          if (callInfo.call_status == 'accepted') {
            self.acceptedVideoCall(callInfo);
            self.tokbox.connect(callInfo.session_id,
            callInfo.token,
            callInfo.to_name);
          }
        }else if (response.result.dismissed) {
            self.currentCall(null);
            clearInterval(self.callStatusIntervalID);
            page.modal.error('The call was not answered').show();
        }
      });
    };



    /*
    pool.add(new DI.ajaxPolling.request("ajax/videocall/check_call", {}, function(response) {
        checkForCallStatus(JSON.parse(response));
    }, AjaxIntervalManager.getInterval('call_status_check', DI.ajaxPolling.priority.high)));
    */

    self.acceptIncomingCall = function(call, callback) {
      $.post(urls.www('ajax/videocall/accept'), {
      call_id: call.id }, function(response) {
        if (response.error) {
          page.modal.error('An error occurred accepting the call').show();
          return;
        }
        if (response.result.accepted) {
          var call = response.result.call;
          if (typeof callback == 'function') {
            if (callback())
              return;
          }
          self.acceptedVideoCall(call);
          self.tokbox.connect(call.session_id, call.token, call.from_name);
        } else {
          page.modal.error('An error occurred during the call').show();
        }
      });
    };

    self.dismissIncomingCall = function(call, ignored) {
      var action = 'dismiss';
      if (ignored === true)
        action = 'cancel';
      $.post(urls.www('ajax/videocall/dismiss'), {
      call_id: call.id,
      action: action}, function(response) {
        if (response.error) {
          page.modal.error('An error occurred when the call was dismissed').show();
          return;
        }
        if (!response.result.dismissed) {
          page.modal.error('An error occurred when the call was dismissed').show();
        }
      });
    };

    self.hangUpCall = function(callback) {
      if (self.acceptedVideoCall() == null) {
        page.modal.error('Call not defined').show();
      }
      $.get(urls.www('ajax/videocall/check_call'),
      {call_id: self.acceptedVideoCall().id},
      function(response) {
        if (response.error) {
          page.modal.error('Error trying to get info about the call, ' +
          'hanging up!').show();
          return;
        }

        if (response.result.answered) {
          var callInfo = response.result.call;
          if (callInfo.complete_status == 'incomplete') {
            // Need to be hanged up
            $.post(urls.www('ajax/videocall/hangup'), {
            call_id: self.acceptedVideoCall().id }, function(response) {
              if (response.error) {
                page.modal.error(response.message).show();
                return;
              }
              if (!response.result.hanged) {
                page.modal.error('Error trying to hang up the call').show();
              }
              self.acceptedVideoCall(null);
              if (typeof callback == 'function')
                callback();
            });
          } else {
              self.acceptedVideoCall(null);
              if (typeof callback == 'function')
                callback();
          }
        }
      });
    };

    self.checkIncomingCall = function()
    {
      $.get(urls.www('ajax/videocall/check_incoming'), function(data) {
        self.processIncomingCall(data);
      });
    };

    self.processIncomingCall = function(response) {
     
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }

    };

    pool.add(new DI.ajaxPolling.request("ajax/videocall/check_incoming", {}, function(response) {
        self.processIncomingCall(response);
    }, AjaxIntervalManager.getInterval('incoming_call_check', DI.ajaxPolling.priority.medium)));

    self.showVideoConfOptions = function(element, object, next_available) {

      var inner_html = '<div style="margin-bottom: 5px;"><a class="technician-call-pharmacist-button btn btn-primary" href="#" style="width: 100%;">Technician&nbsp;-&nbsp;call&nbsp;pharmacist</a></div><div><a style="width: 100%;" class="patient-call-pharmacist-button btn btn-info">Patient&nbsp;tablet&nbsp;-&nbsp;call&nbsp;pharmacist</a></div>';

      var pharmacist_name = next_available ? 'the next available pharmacist' : object.firstName() + ' ' + object.lastName()

      element.popover({
        trigger: 'manual',
        placement: 'right',
        html: true,
        title: '<div id="call-pharmacist-popover-header" style="min-height: 20px;"><div style="float: left;" id="call-pharmacist-popover-title"></div><div style="float: right; width: 20px; text-align: right;"><a href="#" class="close-call-pharmacist-popover badge">X</a></div></div>',
        content: '<div id="call-pharmacist-popover-body" style="width: 345px;"></div>'
      });

      element.popover('show');
      var timer = null;
      $('#main-container .sidebar .popover')
      .css({width: '400px'})
      .on('mouseleave', function(){
             timer = setTimeout(function(){
                  element.popover('hide');
              }, 2000);
       }).on('mouseenter', function(){
              if (timer){
                  clearTimeout(timer);
                  timer = null;
              }
          });

      $('#call-pharmacist-popover-title').html('Call ' + pharmacist_name);
      $('#call-pharmacist-popover-body').html(inner_html);

      $('.close-call-pharmacist-popover').click(function(){
        element.popover('hide');
        return false;
      });

      $('.technician-call-pharmacist-button').click(function(){
        self.requestVideoConf(object);
        element.popover('hide');
        return false;
      });

      $('.patient-call-pharmacist-button').click(function(){
        self.showPatientCallOptions(element, object);
        return false;
      });
    };

    self.showPatientCallOptions = function(element, object) {

      var inner_html = '<div id="call-pharmacist-initiate-tablet-body">Patient Name:&nbsp;&nbsp;<input id="call-pharmacist-patient-name" style="width: 125px;" />&nbsp;&nbsp;<a id="call-pharmacist-initiate-tablet-button" href="#" class="btn btn-info">Initiate tablet</a></div>';

      $('#call-pharmacist-popover-body').html(inner_html);
      $('#call-pharmacist-patient-name').focus();

      $('#call-pharmacist-initiate-tablet-button').click(function(){

        if (!$('#call-pharmacist-patient-name').val())
        {
          $('#call-pharmacist-initiate-tablet-body').css({color: 'red'}).animate({color: '#333'}, 3000);
          $('#call-pharmacist-patient-name').css({borderWidth: '1px', borderColor: 'red'}).animate({borderColor: '#ccc'}, 3000);

          return;
        }

        self.requestPatientVideoConf(null, object);

        element.popover('hide');

      });
    };

    self.requestVideoConf = function(object) {
      if (self.currentCall() != null) {
        page.modal.error('Can not start another call,' +
        ' because there is one in progress');
        return;
      }
      
      type = '';
      to = 0;

      if (!object) {
        type = 'next-available';
      }
      else if (object.hasOwnProperty('companyId')) {
        type = 'store';
        to = object.id();
      }
      else
      {
        type = 'user';
        to = object.id();
      }

      $.post(urls.www('ajax/videocall/place'), {
      to: to, type: type}, function(response) {
        if (response.error) {
          page.modal.error(response.message).show();
          return;
        }
        if (response.result.placed) {
          self.currentCall(response.result.call_id);
          self.callee(object ? object.name() : '');
            self.callStatusIntervalID = setInterval(
                self.checkForCallStatus,
                AjaxIntervalManager.getInterval('call_status_check', 2000));
        } else {
          page.modal.error('Error trying to place the call').show();
        }
      });
    };

    self.requestPatientVideoConf = function(prescription, pharmacist) {

      if (self.currentCall() != null) {
        page.modal.error('Can not start another call;' +
        ' there is one in progress');
        return;
      }

      var prescription_id = prescription === null ? '' : prescription.prescriptionId();
      var pharmacist_id = pharmacist ? pharmacist.id() : '';

      $.post(urls.www('ajax/patienttablet/initiate_direct'), {
        prescription_id: prescription_id,
        pharmacist_id: pharmacist_id,
        patient_name: $('#call-pharmacist-patient-name').val()
      }, function(response) {
        if (response.error)
        {
          page.modal.error(response.message).show();
          return;
        }
        if (response.result.success)
        {
          page.modal.error(response.result.message).show();
        }
      });

      /*

      $.post(
        urls.www('ajax/videocall/patient_place'),
        {
          prescription_id: prescription_id,
          pharmacist: pharmacist_id
        },
        function(response)
        {
          if (response.error) {
            page.modal.error(response.message).show();
            return;
          }
          if (response.result.placed) {
            self.currentCall(response.result.call_id);
          } else {
            page.modal.error('Error trying to place the call').show();
          }
        }
      );

      */
    };

  }
});

