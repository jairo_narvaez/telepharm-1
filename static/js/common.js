//The build will inline common dependencies into this file.

//For any third party dependencies, like jQuery,
//place them in the lib folder.

//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.
require([
  'jquery',
  'knockout',
  'EventEmitter',
  'app/model/ajaxinterval',
  'pollingrequest',
  'bootstrap',
  'jquery.printElement.min',
  'bootstrap-lightbox',
  'jquery-ui-1.10.2.custom',
  'jstorage.min'],
  function(
    $,
    ko,
    EventEmitter,
    AjaxIntervalManager
  ) {
    var documentHeight = $(document).height();
    var footerHeight = $('footer').height();
    var headerHeight = $('header').height();
    var footerMarginTopHeight = parseInt($('footer').css('margin-top'), 50);
    var reduceHeader = 0;
    if ($('header').hasClass('admin') && $('ul.breadcrumb').length > 0)
      reduceHeader = 114;
    $('#main-container').height(documentHeight -
      headerHeight -
      footerHeight -
      footerMarginTopHeight -
      reduceHeader);
    window.ee = new EventEmitter();
    window.AjaxIntervalManager = new AjaxIntervalManager();

    $(window).bind(
      "load",
      function(event) {
        $.get('/ajax/videocall/browser_open');
      }
    );

    $(window).bind(
      "unload",
      function(event) {
        $.ajax({
          url: '/ajax/videocall/browser_close',
          async: false
        });
      }
    );

  }
);
