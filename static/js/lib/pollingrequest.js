var DI = DI || {};
DI.ajaxPolling = (function () {
    return {
        priority: {tiny: 60000, low: 10000, medium: 5000, high: 2000},

        request: function (url, data, callback, priority) {
            if ($.isFunction(data)) {
                priority = callback;
                callback = data;
                data = {};
            }

            return {
                url: url,
                data: data,
                callback: callback,
                priority: priority || DI.ajaxPolling.priority.medium,
                lastRunDate: null
            };
        },

        pool: function (options) {
            var self = this;
            var timer;
            var requests = [];
            var pendingRequests = null;

            var tick = function () {
                if (pendingRequests == null && requests.length > 0) {
                    pendingRequests = [];

                    var now = new Date();
                    var data = [];
                    for (var i = 0; i < requests.length; ++i) {
                        if (now - requests[i].lastRunDate + 10 > requests[i].priority) {
                            pendingRequests.push(requests[i]);

                            data.push({
                                url: requests[i].url,
                                data: requests[i].data
                            });

                            requests[i].lastRunDate = now;
                        }
                    }

                    if (pendingRequests.length == 0) {
                        pendingRequests = null;
                    }
                    else {
                        var data = {requests: data}

                        $.ajax("/ajax/pollingrequest", {
                            type: 'POST',
                            data: data,
                            statusCode: {
                                200: function (response) {
                                    for (var i = 0; i < pendingRequests.length; ++i) {
                                        if (response[i] && pendingRequests[i].callback) {
                                            if (response[i].kill_session) {
                                            }
                                            pendingRequests[i].callback(response[i]);
                                        }

                                    }
                                    pendingRequests = null;
                                },
                                403: function () {
                                    window.location = 'login'
                                },
                                500 : function(){
                                    pendingRequests = null;
                                }
                            }
                        });
                    }
                }
            };

            self.run = function () {
                if (!timer) {
                    timer = window.setInterval(tick, 1000);
                }
            };

            self.stop = function () {
                window.clearInterval(timer);
                timer = null;
            }

            self.add = function (request) {
                request.lastRunDate = new Date();

                requests.push(request);

                return self;
            };

            self.remove = function (request) {
                requests = $.grep(requests, function (r) {
                    return request != r;
                });

                return self;
            };
        }
    };
})();


var pool = new DI.ajaxPolling.pool({defaultPriority: 1});

pool.run();