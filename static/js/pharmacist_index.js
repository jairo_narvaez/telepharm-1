require(['./common', 'jquery.inputmask', 'jquery.ba-resize.min', 'jquery.inputmask.date.extensions', 'app/page_pharmacist_index'], function(common) {
  var fixElementsPosition = function()
  {
    $('#main-container').removeAttr('style');
    $(".available-persons").removeAttr('style');

    if ($('body').height() <= $(window).height())
      $('#main-container').height($(window).height() - 228); // 228 value of header plus footer and paddings

    var available_persons_height = $(".available-persons").height($(window).height() - $('.sidebar .profile').height() - $('header').height() - $('.sidebar .store-box').height() - $('footer').height() - parseInt($('.available-persons').css('padding-bottom')) - parseInt($('.available-persons').css('padding-top')));
    if (available_persons_height > 35 ) //minimum available pharmacist height
      $(".available-persons").height(available_persons_height);

  };
  $(window).resize(fixElementsPosition);
  $('.status-details').resize(fixElementsPosition);
});
