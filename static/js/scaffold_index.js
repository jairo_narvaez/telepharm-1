require(['jquery', './common', 'jquery.dataTables', 'bootstrap-paging', 'app/functions/showpage'], function($, common, dt, bp, showPage) {
  $.extend( $.fn.dataTableExt.oStdClasses, {
    "sWrapper": "dataTables_wrapper form-inline"
  } );
  $('#records').dataTable({
    "sDom": "<'row'<'span3 button-space'><'span5'l><'span5'f>r>t<'row'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
      return iStart +" - "+ iEnd + " of " + iTotal + " entries";
    }
  });
  var buttons = $("#left-button").html();
  $("#records_wrapper").find(".button-space").html(buttons);
  $("#left-button").detach();
  showPage();
});
