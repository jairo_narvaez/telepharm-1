// To run go to http://0.0.0.0:8001/js-spec-runner/SpecRunner.html

define(['knockout',
  'app/model/prescription',
  'app/model/prescriptionoption',
  'app/helpers/modal'],
function( ko,
  Prescription,
  PrescriptionOption,
  Modal ) {
  describe("Prescription Model", function() {
    var rx;
    var page = {
      modal: new Modal()
    };
    var TP = {};
    TP.urls = {
      www : 'http://0.0.0.0:8000/',
      assets : 'http://0.0.0.0:8001/'
    };
    window.TP = TP;
    page.modal.callAfterOk.subscribe(function(newVal){
      page.modal.callAfterOk(null);
    });
    window.page = page;

    beforeEach(function() {
      var data = $.parseJSON($('#prescription-data').val());
      rx = new Prescription(data);
    });

    describe("Life Cycle", function() {
      it("should be created as draft", function() {
        expect(rx.isDraft()).toBe(true);
        expect(rx.show()).toBe(true);

        expect(rx.onHold()).toBe(false);
        expect(rx.approvedOnHold()).toBe(false);
        expect(rx.approvedForCounsel()).toBe(false);
        expect(rx.requiredCounsel()).toBe(false);
        expect(rx.refusedCounsel()).toBe(false);
        expect(rx.rejected()).toBe(false);
        expect(rx.completed()).toBe(false);
        expect(rx.canceled()).toBe(false);
        expect(rx.archived()).toBe(false);
      });

      describe("A new prescription (in Draft status)", function() {
        describe("Should Allow the following actions", function() {
          it("submit", function () {
            expect(rx.canSubmit()).toBe(true);
          });
          it("cancel", function () {
            expect(rx.cancel()).toBe(true);
          });
          it("update option", function () {
            expect(rx.updateOption(new PrescriptionOption({
            prescription: rx,
            label: 'On Hold',
            name: 'on_hold',
            checked: false}), '1')).toBe(true);
          });
        });
        describe("Should NOT allow the following actions", function() {
          it("approve", function() {
            expect(rx.approve()).toBe(false);
          });
          it("request counsel", function() {
            expect(rx.requestCounsel()).toBe(false);
          });
          it("complete counsel", function() {
            expect(rx.completeCounsel()).toBe(false);
          });
          it("refuse counsel", function() {
            expect(rx.refuseCounsel()).toBe(false);
          });
          it("reject", function() {
            expect(rx.reject()).toBe(false);
          });
        });
        describe("Stages", function() {
          it('should not be in Awaiting Approval', function() {
            expect(rx.isAwaitingApproval()).toBe(false);
          });
          it('should not be in Awaiting Counsel', function() {
            expect(rx.isAwaitingCounsel()).toBe(false);
          });
          it('should not be in Final Status', function() {
            expect(rx.inFinalStatus()).toBe(false);
          });
          it('should not be in On Hold', function() {
            expect(rx.isOnHold()).toBe(false);
          });
        });
      });


      describe('After a normal submit', function(){
        beforeEach(function() {
          // a normal submit will do:
          // $prescription->is_draft = false;
          // $prescription->rejected = false;
          // $prescription->show = true;
          rx.isDraft(false);
          rx.rejected(false);
          rx.show(true);
        });
        describe("Should Allow the following actions", function() {
          it('approve', function() {
            expect(rx.approve()).toBe(true);
          });
          it('approve & request counsel', function() {
            expect(rx.requestCounsel()).toBe(true);
          });
          it('reject', function() {
            expect(rx.reject()).toBe(true);
          });
        });
        describe("Should NOT allow the following actions", function() {
          it("should not allow refuseCounsel", function() {
            expect(rx.refuseCounsel()).toBe(false);
          });
        });
        describe("Stages", function() {
          it('should be in Awaiting Approval', function() {
            expect(rx.isAwaitingApproval()).toBe(true);
          });
          it('should not be in Awaiting Counsel', function() {
            expect(rx.isAwaitingCounsel()).toBe(false);
          });
          it('should not be in Final Status', function() {
            expect(rx.inFinalStatus()).toBe(false);
          });
          it('should not be in On Hold', function() {
            expect(rx.isOnHold()).toBe(false);
          });
        });

        describe('After approving the prescription', function() {
          beforeEach(function(){
              // Approved on server
              rx.approvedForCounsel(true);
          });
          describe("Should Allow the following actions", function() {
            it('complete counsel', function() {
              expect(rx.completeCounsel()).toBe(true);
            });
            it('cancel', function() {
              expect(rx.cancel()).toBe(true);
            });
          });
          describe("Should NOT allow the following actions", function() {
            it('reject', function() {
              expect(rx.reject()).toBe(false);
            });
          });
          describe("Stages", function() {
            it('should not be in Awaiting Approval', function() {
              expect(rx.isAwaitingApproval()).toBe(false);
            });
            it('should be in Awaiting Counsel', function() {
              expect(rx.isAwaitingCounsel()).toBe(true);
            });
            it('should not be in Final Status', function() {
              expect(rx.inFinalStatus()).toBe(false);
            });
          });

          describe('without request counsel', function(){
            beforeEach(function(){
              rx.requiredCounsel(false);
            });
            it('should not require counsel', function() {
              expect(rx.requiredCounsel()).toBe(false);
            });
          });

          describe('requiring counsel', function(){
            beforeEach(function(){
              rx.requiredCounsel(true);
            });
            it('should require counsel', function() {
              expect(rx.requiredCounsel()).toBe(true);
            });

          });

          describe("Cancel a prescription", function() {
            beforeEach(function(){
              rx.canceled(true);
            });
            describe("Should NOT allow the following actions", function() {
              it("complete counsel", function() {
                expect(rx.completeCounsel()).toBe(false);
              });
              it("refuse counsel", function() {
                expect(rx.refuseCounsel()).toBe(false);
              });
              it("reject", function() {
                expect(rx.reject()).toBe(false);
              });
            });
            describe("Stages", function() {
              it('should not be in Awaiting Approval', function() {
                expect(rx.isAwaitingApproval()).toBe(false);
              });
              it('should not be in Awaiting Counsel', function() {
                expect(rx.isAwaitingCounsel()).toBe(false);
              });
              it("Should be in Final Status", function() {
                expect(rx.inFinalStatus()).toBe(true);
              });
            });
          });

          describe("Pharmacist set as complete", function() {
            beforeEach(function(){
              rx.completed(true);
            });
            describe("Should allow the following actions", function() {
              it('report error', function() {
                expect(rx.canReportError()).toBe(true);
              });
            });
            describe("Should NOT allow the following actions", function() {
              it("complete counsel", function() {
                expect(rx.completeCounsel()).toBe(false);
              });
              it("refuse counsel", function() {
                expect(rx.refuseCounsel()).toBe(false);
              });
              it("reject", function() {
                expect(rx.reject()).toBe(false);
              });
            });
            describe("Stages", function() {
              it('should not be in Awaiting Approval', function() {
                expect(rx.isAwaitingApproval()).toBe(false);
              });
              it('should not be in Awaiting Counsel', function() {
                expect(rx.isAwaitingCounsel()).toBe(false);
              });
              it("Should be in Final Status", function() {
                expect(rx.inFinalStatus()).toBe(true);
              });
            });
          });

        });

        describe("After Rejecting the prescription", function() {
          beforeEach(function() {
            rx.rejected(true);
          });
          describe("Should Allow the following actions", function() {
            it("edit", function() {
              expect(rx.canEdit()).toBe(true);
            });
            it("submit", function() {
              expect(rx.canSubmit()).toBe(true);
            });
            it("cancel", function() {
              expect(rx.canCancel()).toBe(true);
            });
          });
          describe("Should NOT Allow the following actions", function() {
            it("approve", function() {
              expect(rx.approve()).toBe(false);
            });
            it("request counsel", function() {
              expect(rx.requestCounsel()).toBe(false);
            });
            it("complete counsel", function() {
              expect(rx.completeCounsel()).toBe(false);
            });
            it("refuse counsel", function() {
              expect(rx.refuseCounsel()).toBe(false);
            });
            it("reject", function() {
              expect(rx.reject()).toBe(false);
            });
          });
          describe("Stages", function() {
            it('should be in Rejected', function() {
              expect(rx.isRejected()).toBe(true);
            });
            it('should not be in Awaiting Approval', function() {
              expect(rx.isAwaitingApproval()).toBe(false);
            });
            it('should not be in Awaiting Counsel', function() {
              expect(rx.isAwaitingCounsel()).toBe(false);
            });
            it("Should not be in Final Status", function() {
              expect(rx.inFinalStatus()).toBe(false);
            });
          });
        });
      });

      describe('After a submit on hold', function(){
        beforeEach(function() {
          // update the on hold option
          rx.onHold(true);
          // a normal submit will do:
          // $prescription->is_draft = false;
          // $prescription->rejected = false;
          // $prescription->show = true;
          rx.isDraft(false);
          rx.rejected(false);
          rx.show(true);
        });

        describe("Should Allow the following actions", function() {
            it("approve on hold", function() {
              expect(rx.approve()).toBe(true);
            });
            it('reject', function() {
              expect(rx.reject()).toBe(true);
            });
          });
          describe("Should NOT allow the following actions", function() {
            it("submit", function() {
              expect(rx.submit()).toBe(false);
            });
            it('complete counsel', function() {
              expect(rx.completeCounsel()).toBe(false);
            });
            it('refuse counsel', function() {
              expect(rx.refuseCounsel()).toBe(false);
            });
            it('cancel', function() {
              expect(rx.cancel()).toBe(false);
            });
          });
          describe("Stages", function() {
            it('should be in Awaiting Approval', function() {
              expect(rx.isAwaitingApproval()).toBe(true);
            });
            it('should not be in Awaiting Counsel', function() {
              expect(rx.isAwaitingCounsel()).toBe(false);
            });
            it('should not be in Final Status', function() {
              expect(rx.inFinalStatus()).toBe(false);
            });
            it('should be in On Hold', function() {
              expect(rx.isOnHold()).toBe(true);
            });
          });

        describe('After approving the prescription on Hold', function() {
          beforeEach(function(){
              // Approved on server
              rx.approvedOnHold(true);
              rx.show(false);
            });
          describe("Should Allow the following actions", function() {
            it("submit (re-submit)", function() {
              expect(rx.submit()).toBe(true);
            });
            it("cancel", function() {
              expect(rx.cancel()).toBe(true);
            });
          });
          describe("Should NOT allow the following actions", function() {
            it('approve', function() {
              expect(rx.approve()).toBe(false);
            });
            it('complete counsel', function() {
              expect(rx.completeCounsel()).toBe(false);
            });
            it('refuse counsel', function() {
              expect(rx.refuseCounsel()).toBe(false);
            });
            it('reject', function() {
              expect(rx.reject()).toBe(false);
            });
          });
          describe("Stages", function() {
            it('should not be in Awaiting Approval', function() {
              expect(rx.isAwaitingApproval()).toBe(false);
            });
            it('should not be in Awaiting Counsel', function() {
              expect(rx.isAwaitingCounsel()).toBe(false);
            });
            it('should not be in Final Status', function() {
              expect(rx.inFinalStatus()).toBe(false);
            });
            it('should be in On Hold', function() {
              expect(rx.isOnHold()).toBe(true);
            });
          });

          describe('After a re-submit (technician search by number and submit again)', function() {
            beforeEach(function() {
              // a normal submit will do:
              // $prescription->is_draft = false;
              // $prescription->show = true;
              // $prescription->on_hold = false;
              // $prescription->rejected = false;
              rx.isDraft(false);
              rx.show(true);
              rx.onHold(false);
              rx.rejected(false);
            });
            describe("Should Allow the following actions", function() {
              it('approve', function() {
                expect(rx.approve()).toBe(true);
              });
              it('approve & request counsel', function() {
                expect(rx.requestCounsel()).toBe(true);
              });
              it('reject', function() {
                expect(rx.reject()).toBe(true);
              });
            });
            describe("Should NOT allow the following actions", function() {
              it("refuse counsel", function() {
                expect(rx.refuseCounsel()).toBe(false);
              });
              it("complete counsel", function() {
                expect(rx.completeCounsel()).toBe(false);
              });
            });
            describe("Stages", function() {
              it('should be in Awaiting Approval', function() {
                expect(rx.isAwaitingApproval()).toBe(true);
              });
              it('should not be in Awaiting Counsel', function() {
                expect(rx.isAwaitingCounsel()).toBe(false);
              });
              it('should not be in Final Status', function() {
                expect(rx.inFinalStatus()).toBe(false);
              });
              it('should be in On Hold', function() {
                expect(rx.isOnHold()).toBe(true);
              });
            });
         });

          describe("Cancel a prescription", function() {
            beforeEach(function(){
              rx.canceled(true);
            });
            describe("Should NOT allow the following actions", function() {
              it("complete counsel", function() {
                expect(rx.completeCounsel()).toBe(false);
              });
              it("refuse counsel", function() {
                expect(rx.refuseCounsel()).toBe(false);
              });
              it("reject", function() {
                expect(rx.reject()).toBe(false);
              });
            });
            describe("Stages", function() {
              it('should not be in Awaiting Approval', function() {
                expect(rx.isAwaitingApproval()).toBe(false);
              });
              it('should not be in Awaiting Counsel', function() {
                expect(rx.isAwaitingCounsel()).toBe(false);
              });
              it("Should be in Final Status", function() {
                expect(rx.inFinalStatus()).toBe(true);
              });
              it("Should be in On Hold", function() {
                expect(rx.isOnHold()).toBe(true);
              });
            });
          });

        });


      describe("After Rejecting the prescription on Hold", function() {
          beforeEach(function() {
            rx.rejected(true);
          });
          describe("Should Allow the following actions", function() {
            it("edit", function() {
              expect(rx.canEdit()).toBe(true);
            });
            it("submit", function() {
              expect(rx.canSubmit()).toBe(true);
            });
            it("cancel", function() {
              expect(rx.canCancel()).toBe(true);
            });
          });
          describe("Should NOT Allow the following actions", function() {
            it("approve", function() {
              expect(rx.approvedForCounsel()).toBe(false);
            });
            it("complete counsel", function() {
              expect(rx.completeCounsel()).toBe(false);
            });
            it("refuse counsel", function() {
              expect(rx.refuseCounsel()).toBe(false);
            });
          });
          describe("Stages", function() {
            it('should be in Rejected', function() {
              expect(rx.isRejected()).toBe(true);
            });
            it('should not be in Awaiting Approval', function() {
              expect(rx.isAwaitingApproval()).toBe(false);
            });
            it('should not be in Awaiting Counsel', function() {
              expect(rx.isAwaitingCounsel()).toBe(false);
            });
            it("Should not be in Final Status", function() {
              expect(rx.inFinalStatus()).toBe(false);
            });
          });
        });
      });
    });
  });
});
