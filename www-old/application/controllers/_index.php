<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Index extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('members');
		$this->load->model('rx_pharmacist');
		$this->load->model('rx_model');
		$this->data['stores'] = $this->rx_pharmacist->getAllStores();
		//CRON to make RX as Archive 
		$completedRx=$this->rx_model->getCompletedRx();
		$currentTime=strtotime(date('Y-m-d H:i:s'));
		if(!empty($completedRx))
		{
			foreach($completedRx as $completeRx)
			{
				$setExpire=(strtotime($completeRx->updateDate))+(8*60*60);
				if($setExpire<=$currentTime)
				{
					$this->rx_model->setArchivedRx($completeRx->rxId);
				}
			}
		}
		$userType=$this->session->userdata('userType');
		if($userType=='Pharmacist')
		{
			$storeId=$this->session->userdata('storeIds');
			$this->data['countApproval'] = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
			$this->data['countCounsel'] = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId);
			$this->data['countArchive'] = $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
			$this->data['countCompleted'] = $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
			$this->data['countCancelled'] = $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId);
			$this->session->unset_userdata('storeIds');
		}
		elseif($userType=='Technician')
		{
			$storeId = $this->session->userdata('storeId');
			$this->data['countApproval'] = $this->rx_model->countAllPrescriptions('Approval',$storeId);
			$this->data['countCounsel'] = $this->rx_model->countAllPrescriptions('Counsel',$storeId);
			$this->data['countArchive'] = $this->rx_model->countAllPrescriptions('Archive',$storeId);
			$this->data['countCompleted'] = $this->rx_model->countAllPrescriptions('Completed',$storeId);
			$this->data['countCancelled'] = $this->rx_model->countAllPrescriptions('Cancelled',$storeId);
			$this->data['countDraft'] = $this->rx_model->countAllPrescriptions('Draft',$storeId);
		}
	}	
	public function index()
	{		
		$this->load->view('login');
	}
	public function login()
	{
		if ($_POST['userName']=='' || $_POST['password']=='') 
		{
			if($_POST['userName']=='')
			{
				$this -> session -> set_flashdata('message', 'Please enter username');
				redirect('index');
			}
			if($_POST['password']=='')
			{
				$this -> session -> set_flashdata('message', 'Please enter password');
				redirect('index');
			}
		}
		else
		{
			$password=md5($this->input->post('password'));
			$customers=$this->users->checkUser($this->input->post('userName'),$password);			
			if(!empty($customers))
			{
				if(!empty($customers->status) && $customers->status=='1')
				{
					$name=trim($customers->firstName);
					if(!empty($customers->lastName))
						$name.=" , ".strtoupper (substr($customers->lastName, 0, 1));
					$sessiondata = array('storeId'=>($customers->storeId) ,'userId'=>trim($customers->id) , 'email'=>trim($customers->email), 'userType'=>trim($customers->userType),'userName'=>$name );
					$this->session->set_userdata($sessiondata);
					if(trim($customers->userType)=='Technician')
						redirect("technician/rx/index");
					elseif(trim($customers->userType)=='Pharmacist')
						redirect("pharmacist/rx/index");
					elseif(trim($customers->userType)=='Administrator')
						redirect("index/logout");	
				}
				else
				{
					$this -> session -> set_flashdata("message", "Sorry, You're unable to access. Please contact to site manager");
					redirect('index');
				}	
			}
			else
			{
				$this -> session -> set_flashdata('message', 'Please enter valid username and password');
				redirect('index');
			}		
		}
	}
	function profile()
	{		
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			redirect("index");		
		$id = $this->session->userdata('userId');	
		$this->data['userData'] = $this->members->getUserData($id);
		$this->load->view('profile', $this->data);
	}	
	function changepassword()
	{		
		$this->load->view('changepassword', $this->data);
	}	
	public function resetpassword()
    {
    	$userId = $this->session->userdata('userId');
		if(empty($userId))
			redirect("index");	
        $data['error'] = "";
        $this->load->library('form_validation');
        $loginId = $this->session->userdata('userId');           
        $this->form_validation->set_rules('currentpass', 'Current Password', 'required');
		$this->form_validation->set_message('required', '%s required');		
        $this->form_validation->set_rules('newpass', 'New Password', 'required');
		$this->form_validation->set_message('required', '%s required');		
        $this->form_validation->set_rules('confirmpass', 'Confirm Pasword', 'required|matches[newpass]');
		$this->form_validation->set_message('required', '%s required');			
        if ($this->form_validation->run() != FALSE)
        { 
			$_POST['currentpass']=md5($_POST['currentpass']);
			$email=	$_POST['emailaddress'];
			$password=$_POST['currentpass'];				
			$userId = $this->users->checkPassword($email,$password);
            if(!empty($userId->id))
            {
            	$_POST['newpass']=md5($_POST['newpass']);
                $this->users->updatePassword($_POST['newpass'],$loginId);
              	$message ="Password changed successfully";
				$this->session->set_flashdata('message', $message);
				redirect('index/resetpassword');
            }
            else
     		{
				$message = "Current password does not match. Please try again";				
				$this->session->set_flashdata('message', $message);
				redirect('index/changepassword');                  
            }
        }
		else
		{			 
			$this->load->view('changepassword', $this->data);
		}
    }
	function update()
	{
		$userId = $this->session->userdata('userId');				 
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname', 'First Name ', 'required');	
		$this->form_validation->set_rules('lastname', 'Last Name', 'required');			
		if($this->form_validation->run() != FALSE)
		{
			if(!empty($_FILES['photo']["name"]))
			{
				$file= $_FILES['photo']["name"];
				$files=explode('.' , $file);
				$fileextension=$files[1];				
				$filename=uniqid().'.'.$fileextension;
				$target_path="./uploads/pharma/".$filename;
				move_uploaded_file($_FILES['photo']['tmp_name'], $target_path);
				$_POST['photo'] = $filename;
			}
			$message=$this->users->updateUser($this->input->post());	
			if(!empty($message))
				$message="Account information updated";
			else 
				$message="Account information could not be updated";
			$this->session->set_flashdata('message',$message);
			redirect('index/profile');		
		}
		else
		{
			$errors=validation_errors();
			$this->data['errors']=$errors;	
			$this->data['post']=$this->input->post(); 		
			$this->load->view('profile',$this->data);		
		}
	}	
	function logout()
	{
 		$this->session->sess_destroy();
		redirect(base_url()."index");
	}

	function reportproblem()
	{
		$this->rx_model->reportproblem();
	}	
}
