<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('users');
		$this->load->model('stores');
		$this->load->model('members');
		$this->load->model('prescriptions');
		$this->load->model('rx_model');
		//CRON to make RX as Archive 
		$completedRx=$this->rx_model->getCompletedRx();
		$currentTime=strtotime(date('Y-m-d H:i:s'));
		if(!empty($completedRx))
		{
			foreach($completedRx as $completeRx)
			{
				$setExpire=(strtotime($completeRx->updateDate))+(8*60*60);
				if($setExpire<=$currentTime)
				{
					$this->rx_model->setArchivedRx($completeRx->rxId);
				}
			}
		}
	}
	public function search() 
	{		
		if((!empty($_POST['search']) && isset($_POST['search'])) )
		{
			$this->session->set_userdata('search', $_POST['search']);		
		}		
		$search = $this->session->userdata('search');	
		if(!empty($search)){
				$search=$search;}	
		$search=trim($search);		
		$this->data=''; 	
		$base_url= base_url().'admin/admin/search/'.$search;	
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '5'; 
		if ($this->uri->segment(5)=="")$startCount=0;
		else $startCount=$this->uri->segment(5);		
		$this->data['allPrescription']=$this->prescriptions->getAllPresSearch($search,$config['per_page'],$startCount);	
		$count=$this->prescriptions->countAllPresSearch($search);			
		$config['total_rows']=$count;
		$this->data['total']=$count;		
		$this->pagination->initialize($config); 
		$this->load->view('admin/searchresults',$this->data);		
	}	
	public function home()
	{
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}	
		$this->data['allStatus']=$this->prescriptions->countAllPresDash('');
		$this->data['approvalStatus']=$this->prescriptions->countAllPresDash('Approval');
		$this->data['draftStatus']=$this->prescriptions->countAllPresDash('Draft');
		$this->data['completedStatus']=$this->prescriptions->countAllPresDash('Completed');
		$this->data['counselStatus']=$this->prescriptions->countAllPresDash('Counsel');
		$this->data['cancelledStatus']=$this->prescriptions->countAllPresDash('Cancelled');
		$this->data['rejectedStatus']=$this->prescriptions->countAllPresDash('Rejected');		
		$this->load->view('admin/home');
	}
	public function index()
	{
		 	$this->load->view('admin/login');
	}
	public function login()
	{	
	 	$password=md5($this->input->post('password'));
		$admins=$this->users->checkUserForAdm($this->input->post('username'),$password);	
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username ', 'required');	
		$this->form_validation->set_rules('password', 'Password', 'required');			
		if($this->form_validation->run() != FALSE)
		{		
			if(!empty($admins))
			{		
				$newdata = array( 'userId'=>trim($admins->id) , 'email'=>trim($admins->email), 'userType'=>trim($admins->userType) );			
				$this->session->set_userdata($newdata); 		
				redirect("admin/admin/home");
			}
			else
			{
				$message = "Please enter valid username and password";
				$this->session->set_flashdata('message', $message);
				redirect("admin/admin/index",$this->session ->flashdata('message'));
			}
		}
		else
		{
			$errors=validation_errors();		
			$this->data['errors']=$errors;			
			$this->load->view('admin/login',$this->data);		
		}
	}
	function profile()
	{
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}		
		$id = $this->session->userdata('userId');	
		$this->data['userData'] = $this->members->getUserData($id);	
		$this->load->view('admin/profile/addprofile',$this->data);
	}
	function update()
	{
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}	 
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname', 'First Name ', 'required');	
		$this->form_validation->set_rules('password', 'Password', 'required');			
		if($this->form_validation->run() != FALSE)
		{
			$_POST['password']=md5($_POST['password']);	 
			$message=$this->users->updateUser($this->input->post());	
			if(!empty($message))
				$message="Account information updated";
			else 
				$message="Account information could not be updated";
			$this->session->set_flashdata('message',$message);
			redirect('admin/member/index');		
		}
		else
		{
			$errors=validation_errors();
			$this->data['errors']=$errors;	
			$this->data['post']=$this->input->post(); 		
			$this->load->view('admin/profile/addprofile',$this->data);		
		}
	}
	function logout()
	{		
			$newdata = array( 'userId'=>'', 'email'=>'', 'userType'=>'');
			$this->session->unset_userdata($newdata);		
			redirect(base_url()."admin/admin/index");
	}	
	function permissions()
	{
		if(!isset($_POST['submit']))
			$this->load->view('admin/permissions');
		else
		{
			var_dump($_POST); 
		}		
	}
}
