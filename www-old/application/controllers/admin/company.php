<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Company extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('companies');
		$this->load->model('stores');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}	
	}
	public function index() 
	{
		$base_url= base_url().'admin/company/index/' ; 
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);		
		$this->data['companies'] = $this->companies->getAllCompany($config['per_page'],$startCount,''); 		
		$count=$this->companies->countAllCompany();  		
		$config['total_rows'] = $count;
		$this->data['total'] = $count;		
		$this->pagination->initialize($config);
		$this->load->view('admin/company/company', $this->data);
	}
	public function add() 
	{
		$this->load->view('admin/company/add');
	}
	function save()
	{
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Store Name ', 'required'); 
		$this->form_validation->set_rules('storename', 'Store Name ', 'required'); 		
		$this->form_validation->set_rules('accessCode', 'Access Code ', 'required|is_unique[stores.accessCode]'); 
			if($this->form_validation->run() != FALSE) 
			{
				$message= $this->companies->addCompany($this->input->post());
				if(!empty($message))				
					$message ="Company added successfully";
				else
					$message = "Company could not be added";				
				$this->session->set_flashdata('message', $message);
				redirect('admin/company/index');
			}
			else
			{
				$errors = validation_errors();
				$this->data['errors'] = $errors; 
				$this->load->view('admin/company/add', $this->data);
			}
	}
	//To call this function for editing the store
	public function edit($id='')
	{
		if(!empty($id))
		$this->data['storeData'] = $this->companies->getCompanyData($id);
		$this->data['getStoreData'] = $this->stores->getStoreDataComp($id);	
		$this->load->view('admin/company/edit', $this->data);
	}
	//This function is used for updating store
	public function update() 
	{			
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Company Name ', 'required');
		if($_POST['originalAccessCode'] != $_POST['accessCode'])
			$this->form_validation->set_rules('accessCode', 'Access Code ', 'required|is_unique[stores.accessCode]'); 		
				
		if($this->form_validation->run() != FALSE)
		{
			$message= $this->companies->updateCompany($this->input->post());		
			if(!empty($message))
				$message ="Company updated successfully";
			else 
				$message = "Company could not be updated";
			$this->session->set_flashdata('message', $message);
			redirect('admin/company/index');
		}
		else
		{
			$errors=validation_errors();
			$this->data['errors'] =$errors; 
			$this->load->view('admin/company/edit', $this->data);
		}
	}
	//To call this function for deleting the store
	public function delete($id)
	{ 
		$result=$this->companies->deleteCompany($id);
		if(!empty($result))
		{
			$message = "Company deleted successfully" ;
			$this->session->set_flashdata('message', $message);
		}
		else 
		{
			$message = "Company can not be deleted . Because it is associated with some Store";
			$this->session->set_flashdata('message1', $message);
		}		
		redirect('admin/company/index');
	}	
}