<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('members');
		$this->load->model('stores');		
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}	
	}
	public function index() 
	{		
		$usertype='';$status='';
		if(isset($_POST) && !empty($_POST) && $_POST['submit']=='Submit')
		{
			$this->session->unset_userdata('usertype');
			$this->session->unset_userdata('status');			
			$usertype=$_POST['usertype'];
			if($_POST['status']!='')
			{
				$status=$_POST['status'];
				$this->session->set_userdata('status', $_POST['status']);
			}
			$this->session->set_userdata('usertype', $_POST['usertype']);
		}
		elseif($this->uri->segment(4) != '')
		{
			$usertype = $this->session->userdata('usertype');
			$status = $this->session->userdata('status');
			$this->data['usertype']=$usertype;
			if($status!='')
			$this->data['status']=$status;
		}
		else
		{
			$this->session->unset_userdata('usertype');
			$this->session->unset_userdata('status');	
		}		
		$base_url= base_url().'admin/member/index/' ; 	
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);		
		$this->data['allUser']=$this->members->getAllUser($config['per_page'],$startCount,$usertype,$status);		
		$count=$this->members->countAllUser($usertype,$status);		
		$config['total_rows']=$count;
		$this->data['total']=$count;
		$this->pagination->initialize($config); 
		$this->load->view('admin/user/users',$this->data);		
	}
	public function add() 
	{
		$this->data['allcompanies'] = $this->stores->getCompanies();				
		$this->data['allStore'] = $this->stores->getAllStore('','','');			
 		$this->load->view('admin/user/addusers',$this->data);
	}	
	public function displaystore($selectBox) 
	{		
		$this->data['allcompanies'] = $this->stores->getCompanies();
		$allStore = $this->stores->getStore($selectBox);	
?>		
						<div id="categories">
						<?php
							if(!empty($allStore))
							{
						?>
						<ul class="caegories reset fl">
							<?php							
							foreach($allStore as $store)
							{
							?>
									<li style="margin-top: -5px;">							
										<input type="checkbox"  name="storeId[]"  style="margin-right: 10px;" value="<?=$store->storeId?>"  /><?=$store->storeName?>
									</li>
									<?php
							}
									?>
						</ul>
						<?php
							}
							else{ echo "<h6 class='nofound'>No store found</h6>";}
						?>
						</div>	
						<?php						
	}
	function check_default($post_string)
	{
	  return $post_string == '0' ? FALSE : TRUE;
	}		
	function verify_checkbox($str) 
	{
        if($str === '')
		{
            $this->validation->set_message('verify_checkbox', 'zomg error!');
            return false;
        }
		else
	    {
            return true;
        }
    }	
	//call this function for add a user
	function save()
	{		
		$this->data['errors'] = '';
		$this->load->library('form_validation');	
		$this->form_validation->set_rules('company', 'Company ', 'required'); 
		$this->form_validation->set_rules('usertype','User Type','required|callback_check_default');
		$this->form_validation->set_message('check_default', 'You need to select User Type other than the default');
		$this->form_validation->set_rules('storeId','Store','required|callback_verify_checkbox');	
		$this->form_validation->set_rules('firstname', 'First Name ', 'required'); 
		$this->form_validation->set_rules('useremail', 'Email Address', 'required|valid_email');	
		$this->form_validation->set_rules('email', 'Username', 'required|is_unique[users.email]');			
		$this->form_validation->set_rules('password', 'Password', 'required');		
		if($this->form_validation->run() != FALSE) 
		{
			$_POST['pwdoriginal']=$_POST['password'];				
			$_POST['password']=md5($_POST['password']);				
			if(empty($_POST['primarycontact'])){ $_POST['primarycontact']=0;}	
			if(empty($_POST['companyadmin'])){ $_POST['companyadmin']=0;}			
			$message=$this->members->addUser($this->input->post());					
			if(!empty($message))
					$message="User added successfully";
			else 
					$message="User could not be added";
			$this->session->set_flashdata('message',$message);
			redirect('admin/member/index');
		}
		else
		{
			$errors=validation_errors();
			$this->data['errors']=$errors;
			$this->data['allcompanies'] = $this->stores->getCompanies();			
			$this->data['allStore'] = $this->stores->getAllStore('','',''); 
			$this->load->view('admin/user/addusers',$this->data);		
		}
	}
	//To call this function for editing the user
	public function edit($id='')
	{  
		if(!empty($id))
		{
			$this->data['allcompanies'] = $this->stores->getCompanies();
			$this->data['userData'] = $this->members->getUserData($id);	
			$this->data['storeData'] = $this->members->getStoreData($id);	
			$this->data['allStore'] = $this->stores->getStore($this->data['userData']->companyId);
		}	
		$this->load->view('admin/user/editusers', $this->data);
	}
	//This function is used for updating User
	public function update() 
	{		
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstname', 'First Name ', 'required');			
		$this->form_validation->set_rules('company', 'Company ', 'required'); 
		$this->form_validation->set_rules('usertype','User Type','required|callback_check_default');
 		$this->form_validation->set_message('check_default', 'You need to select User Type other than the default');	
		if($this->form_validation->run() != FALSE)
		{
			$message=$this->members->updateUser($this->input->post());
			if(!empty($message))
				$message="Account information updated";
			else 
				$message="Account information could not be updated";
			$this->session->set_flashdata('message',$message);	
			redirect('admin/member/index');		
		}
		else
		{
			$errors=validation_errors();
			$id=isset($_POST['userId'])?$_POST['userId']:'';
			$this->data['errors']=$errors;	
			$this->data['post']=$this->input->post();			
			$this->data['allcompanies'] = $this->stores->getCompanies();
			$this->data['storeData'] = $this->members->getStoreData($id);	
			$this->data['allStore'] = $this->stores->getStore($this->input->post('company'));	
			$this->load->view('admin/user/editusers',$this->data);		
		}
	}
	//To call this function for deleting the User
	public function delete($id)
	{	
		$result=$this->members->deleteUser($id);
		if(!empty($result))
		{
			$message="User deleted successfully";
			$this->session->set_flashdata('message',$message);
		}
		else 
		{	
			$message="User can not be deleted. Because it is associated with some Rx";
			$this->session->set_flashdata('message1',$message);
		}		
		redirect('admin/member/index');
	}	
	//To call this function for editing the user password
	public function respassword($id='')
	{  
		if(!empty($id))
		{		
			$this->data['userData'] = $this->members->getUserData($id);		
		}
		$this->load->view('admin/user/resetpassword', $this->data);
	}
	//This function is used for updating User password
	public function updatepassword() 
	{	
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('newpassword', 'New Password ', 'required');			
		if($this->form_validation->run() != FALSE)
		{
			$_POST['modifiedpwd']=$_POST['newpassword'];
			$_POST['newpassword']=md5($_POST['newpassword']);				
			$message=$this->members->updatePassword($this->input->post());
			if(!empty($message))
				$message="Password changed successfully";
			else 
				$message="Password could not be changed";
			$this->session->set_flashdata('message',$message);
			redirect('admin/member/index');		
		}
		else
		{
			$errors=validation_errors();
			$this->data['errors']=$errors;	
			$this->data['post']=$this->input->post();			
			$this->load->view('admin/user/resetpassword',$this->data);		
		}
	}
	//function for gettting all check box values
	public function getcheckbox()
	{		
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		die();
		$this->load->view('admin/user/addusers');
	}
}