<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Prescription extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('prescriptions');
		$this->load->model('stores');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}		
	}
	public function index() 
	{		
		if((!empty($_POST['storeId']) && isset($_POST['storeId'])) && ($_POST['storeId']=='temp' ))
		{
			 $this->session->set_userdata('storeId', '');
		}
		if(!empty($_POST['storeId']) && isset($_POST['storeId']) && $_POST['storeId']!='temp')
		{			
			$this->session->set_userdata('storeId', $_POST['storeId']);			
		}
		$storeId=$this->session->userdata('storeId');
		if($this->uri->segment(4) == 'all')
		{
			 $this->session->unset_userdata('storeId');
			 $this->session->unset_userdata('status');		
		}
		if(isset($_POST['status']))
		{ 
			$this->session->set_userdata('status', $_POST['status']);
		}
		$status = $this->session->userdata('status');
		$base_url= base_url().'admin/prescription/index/';	
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);		
		$this->data['allPrescription']=$this->prescriptions->getAllPrescription($storeId,$config['per_page'],$startCount,$status);	
		$count=$this->prescriptions->countAllPrescription($storeId,$status);	
		$this->data['allStore']=$this->stores->getAllStore('','','');			
		$config['total_rows']=$count;
		$this->data['total']=$count;		
		$this->pagination->initialize($config); 
		$this->load->view('admin/prescription/prescriptions',$this->data);		
	}	
	function DupPres($id)
	{
		if(!empty($id))
		{
			$base_url= base_url().'admin/prescription/DupPres/'.$id.'/' ; 	
			$config['base_url'] = $base_url;
			$config['per_page'] =10;
			$config['uri_segment'] = '5'; 
			if ($this->uri->segment(5)=="")$startCount=0;
			else $startCount=$this->uri->segment(5);	
			$this->data['allDupPrescription']=$this->prescriptions->getAllDupPrescription($config['per_page'],$startCount,'',$id);					
			$count=$this->prescriptions->countAllDupPrescription($id);		
			$this->data['parentPrescription']=$this->prescriptions->getPrescriptionData($id);		
			$this->data['storeName']=$this->stores->getStoreData($this->data['parentPrescription']->storeId);		
			$config['total_rows']=$count;
			$this->data['total']=$count;		
			$this->pagination->initialize($config); 
			$this->load->view('admin/prescription/dupprescriptions',$this->data);
		}
	}
	//To call this function for editing the user
	public function view($id='')
	{		
		if(!empty($id))
		{
			$this->data['allPrescription']=$this->prescriptions->getPrescription($id);				
		}
		$this->load->view('admin/prescription/viewprescriptions',$this->data);
	}	
}