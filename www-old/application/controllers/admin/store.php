<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Store extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('stores');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			{redirect("admin/admin/index");}	
	}
	public function index() 
	{
		$base_url= base_url().'admin/store/index/' ; 
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);		
		$this->data['allStore'] = $this->stores->getAllStore($config['per_page'],$startCount,''); 		
		$count=$this->stores->countAllStore();  		
		$config['total_rows'] = $count;
		$this->data['total'] = $count;		
		$this->pagination->initialize($config);
		$this->load->view('admin/store/stores', $this->data);
	}
	public function add() 
	{
		$this->data['allcompanies'] = $this->stores->getCompanies();		
		$this->load->view('admin/store/addstores',$this->data);
	}
	function save()
	{		
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Store Name ', 'required'); 
		$this->form_validation->set_rules('accessCode', 'Access Code ', 'required|is_unique[stores.accessCode]'); 
		$this->form_validation->set_rules('serviceURL', 'RxKey service url', 'required'); 
		if($this->form_validation->run() != FALSE) 
		{
			$message= $this->stores->addStore($this->input->post());
			if(!empty($message))				
				$message ="Store added successfully";
			else
				$message = "Store could not be added";				
			$this->session->set_flashdata('message', $message);
			redirect('admin/store/index');
		}
		else
		{
			$errors = validation_errors();
			$this->data['errors'] = $errors; 
			$this->data['allcompanies'] = $this->stores->getCompanies();		
			$this->load->view('admin/store/addstores', $this->data);
		}
	}
	//To call this function for editing the store
	public function edit($id='')
	{
		if(!empty($id))
			$this->data['storeData'] = $this->stores->getStoreData($id);	
		$this->data['allcompanies'] = $this->stores->getCompanies();				
		$this->load->view('admin/store/editstores', $this->data);
	}
	//This function is used for updating store
	public function update() 
	{		
		$this->data['errors'] = '';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Store Name ', 'required');
		if($_POST['originalAccessCode'] != $_POST['accessCode'])
			$this->form_validation->set_rules('accessCode', 'Access Code ', 'required|is_unique[stores.accessCode]'); 
		$this->form_validation->set_rules('serviceURL', 'RxKey service url', 'required'); 
		if($this->form_validation->run() != FALSE)
		{
			$message= $this->stores->updateStore($this->input->post());		
			if(!empty($message))
			$message ="Store updated successfully";
			else 
			$message = "Store could not be updated";
			$this->session->set_flashdata('message', $message);
			redirect('admin/store/index');
		}
		else
		{
			$errors=validation_errors();
			$this->data['errors'] =$errors; 
			$this->data['allcompanies'] = $this->stores->getCompanies();
			$this->load->view('admin/store/editstores', $this->data);
		}
	}
	//To call this function for deleting the store
	public function delete($id)
	{ 
		$result=$this->stores->deleteStore($id);
		if(!empty($result))
		{
			$message = "Store deleted successfully" ;
			$this->session->set_flashdata('message', $message);
		}
		else 
		{
			$message = "Store can not be deleted . Because it is associated with some User";
			$this->session->set_flashdata('message1', $message);
		}		
		redirect('admin/store/index');
	}	
}