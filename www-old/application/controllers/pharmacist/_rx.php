<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rx extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('rx_pharmacist');
		$this->load->model('rx_model');
		$this->load->model('users');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			redirect("index");		
		$this->data['stores'] = $this->rx_pharmacist->getAllStores();		
	 	$storeId=$this->session->userdata('storeIds');
		$this->data['countApproval'] = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
		$this->data['countCounsel'] = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId);
		$this->data['countArchive'] = $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
		$this->data['countCompleted'] = $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
		$this->data['countCancelled'] = $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId);
		$this->data['countOnHold']= $this->rx_pharmacist->countAllPrescriptions('onHold',$storeId); 
		$nnotification=$this->rx_model->countUnreadComments();	 
		$result['countNotification']= ($nnotification > 0 )?$nnotification:'';

		$this->session->unset_userdata('storeIds');
	}	
	function setNotificationsCount()
	{
		$storeId=$this->session->userdata('storeIds');
		$this->data['countApproval'] = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
		$this->data['countCounsel'] = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId);
		$this->data['countArchive'] = $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
		$this->data['countCompleted'] = $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
		$this->data['countCancelled'] = $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId);
		$this->data['countOnHold']= $this->rx_pharmacist->countAllPrescriptions('onHold',$storeId); 
		$nnotification=$this->rx_model->countUnreadComments();	 
		$result['countNotification']= ($nnotification > 0 )?$nnotification:'';
		return $this->data;
	}
	function requiredconusel($rxid,$chkState)
	{
		$this->rx_model->setrequiredconusel($rxid,$chkState);
	}
	function getnotification()
	{
		$result= array();
		$storeId=$this->session->userdata('storeId');		
		$napproval = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
		$nconsuel = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId);
		$ncompleted= $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
		$ncancelled= $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId); 
		$nonhold= $this->rx_pharmacist->countAllPrescriptions('onHold',$storeId);
		$nnotification=$this->rx_model->countUnreadComments();	 
		$result['countNotification']= ($nnotification > 0 )?$nnotification:'';

		$result['countApproval'] =($napproval > 0 )?$napproval:'';
		$result['countCounsel']= ($nconsuel > 0 )?$nconsuel:'';
		$result['countCompleted']= ($ncompleted > 0 )?$ncompleted:'';
		$result['countCancelled']= ($ncancelled > 0 )?$ncancelled:'';
		$result['countOnHold']= ($nonhold > 0 )?$nonhold:'';
 		echo json_encode($result);
		//	$result['countArchive']= $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
	}
	//To call this function for getting all prescription related to a specific Pharmacist
	public function index() 
	{		
		/*
		$this->data['approvalPrescription'] = $this->rx_pharmacist->getAllPrescriptions('Approval','1','10',$storeId);
		$limit=10-count($this->data['approvalPrescription']);
		$this->data['lowApproval'] = $this->rx_pharmacist->getAllPrescriptions('Approval','0',$limit,$storeId);
		$this->data['countApproval'] = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
			
		$this->data['counselPrescription'] = $this->rx_pharmacist->getAllPrescriptions('Counsel','1','10',$storeId);
		$limit=10-count($this->data['counselPrescription']);
		$this->data['lowCounsel'] = $this->rx_pharmacist->getAllPrescriptions('Counsel','0',$limit,$storeId);
		$this->data['countCounsel'] = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId); 
		
		$this->data['archivePrescription'] = $this->rx_pharmacist->getAllPrescriptions('Archive','','10',$storeId);
		$this->data['countArchive'] = $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
		
		$this->data['completedPrescription'] = $this->rx_pharmacist->getAllPrescriptions('Completed','','10',$storeId);
		$this->data['countCompleted'] = $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
		
		$this->data['cancelledPrescription'] = $this->rx_pharmacist->getAllPrescriptions('Cancelled','','10',$storeId);
		$this->data['countCancelled'] = $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId);
		
		$this->data['activities'] = $this->rx_pharmacist->getAllActivity('','10',$storeId);
		$this->data['activitiesMore'] = $this->rx_pharmacist->getAllActivity('','',$storeId);
		
		
		$this->load->view('pharmacist/showrx', $this->data); 
		 */		
		redirect('pharmacist/viewmore/index/Approval');
	}
	//To call this function for getting the data of one prescrition by passing it's Id
	function view($rxId,$childId='')
	{		
		if(isset($_POST["comments"]))
		{
			$commenttxt =trim($_POST["comments"]);
			if(!empty($commenttxt)) $this->addcomment();
		}				
		$this->data['childRx']=$this->rx_model->getChildRx($rxId);
		if(!empty($childId)) $rxId= $childId;
		$this->data['patientData']= $this->rx_model->getRxRecord($rxId);
		$this->data['capturedImage']= $this->rx_model->getcapturedImage($rxId);
		$this->data['drugImage']= $this->rx_model->getDrugImage($rxId);
		$this->data['NDCdrugImage']= $this->rx_model->getNDCDrugImage($this->data['patientData']->ndc_number);

		$this->data['filledBy']=$this->rx_model->getUserName($this->data['patientData']->technicianId);
		//$this->data['filledBy']=$this->rx_model->getUserName($this->data['patientData']->technicianId);
		if($this->data['patientData']->changedStatusBy)
		{
			$this->data['statusChangedBy']=$this->rx_model->getUserName($this->data['patientData']->changedStatusBy);
		}
		$this->data['comments']=$this->rx_model->getComments($rxId,'5');
		$storeId = $this->session->userdata('storeId');
		$this->data['activities'] = $this->rx_model->getAllActivity($storeId,$rxId,'1');		
		$this->data['activitiesMore'] = $this->rx_model->getAllActivity($storeId,$rxId );
		$this->data['videoConf'] = $this->rx_model->getvideoConfiguration($storeId,$rxId );		

		$this->data['otherdata']= $this->rx_model->getDrugPresciber($rxId);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();	

		$this->load->view('pharmacist/view',$this->data);
	}
	//To call this function for adding new comment
	function addcomment()
	{
		if(isset($_POST['commentRxId']))
		{
			$data=array("comments"=>$this->input->post("comments"),
						'rxId'=>$_POST['commentRxId'],
						"userId"=>$this->session->userdata["userId"]
						);
			$this->rx_model->addComment($data);
		}
	}
		//To call this function for for changing status
	function change($rxId,$status,$pageName='', $checked='')
	{
		$flag='';
		//$data['councel']=$_POST['councel'];			
		$data['rxId']=$rxId;
		$data['status']=$status;
		$data['requiredCounsel']=$checked;
		
		
		//$updateData=array('requiredCounsel'=>$data['requiredCounsel']);
		if($status=='Archive')
		$data['priority']='0';	
								
		if($status=='Counsel'){$flag='accepted';}
		if($status=='Rejected'){$flag='rejected';}
		if($status=='Cancelled'){$flag='cancelled';}
		if($status=='Approval'){$flag='re-submitted';}
		if($status=='Completed'){$flag='completed';}
		if($status=='Archive'){$flag='archived';}
		if($status=='OnHold'){$flag='On Hold';}
		if($status=='Refused')
		{
			$data['status']='Completed';
			$data['refuse']='1';
			$flag='refused';
		}		
		$result=$this->rx_model->changeStatus($data);
		if(!empty($result))
		{
			$message = "Rx record has been ".$flag;
		}
		else
		{
			$message = "Rx record couldn't be ".$flag;
		}		
		$this->session->set_flashdata('message', $message);
		if(!empty($pageName) && $pageName!='')
			redirect('pharmacist/rx/'.$pageName);
		else
			redirect('pharmacist/rx/view/'.$rxId);
	}			
	//To call this function for getting the searched data
	function search()
	{
		if(isset($_POST) && !empty($_POST))
		{
			$this->session->unset_userdata('searchRxNumber');
			$data['search']=$_POST['search'];
			$this->session->set_userdata('searchRxNumber',$_POST['search']);
		}
		else
		{
			$data['search']=$this->session->userdata('searchRxNumber');
		}		
		$base_url= base_url() .'pharmacist/rx/search/'; 
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();		
		$this->data['prescription'] = $this->rx_model->getSearchedData($data , $config['per_page'],$startCount);
		$count = $this->rx_model->countSearchedData($data);
		if(!empty($data['search']))
			$this->data['message']='Result of '.$data['search'];
		else
			$this->data['message']='Result of all';
		$config['total_rows'] = $count;
		$this->data['count'] = $count;
		$this->pagination->initialize($config);		
		$this->load->view('pharmacist/result',$this->data);
	}			
	//To call this function for getting advanced searched data
	function advancesearch()
	{
		$postData=array();
		$newData=array();
		$flag=0;
		$segment4=$this->uri->segment(4);
		if($segment4!='')
			$flag=1;
		elseif(isset($segment4))
			$flag=1;
		if(isset($_POST) && !empty($_POST))
		{
			$this->session->unset_userdata('store');
			$this->session->unset_userdata('Date');
			$this->session->unset_userdata('rxstatus');
			$this->session->unset_userdata('patient');
			$this->session->unset_userdata('rx');
			if(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='1')
			{
				$postData['matchDate']=date('Y-m-d');
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='2')
			{
				$number=date('w');
				$postData['matchDate']=date('Y-m-d',strtotime("-".$number." days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='3')
			{
				$number=date('w');
				$number+=7;
				$postData['matchDate']=date('Y-m-d',strtotime("-".$number." days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='4')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-30 days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='5')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-60 days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='6')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-182 days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='7')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-365 days"));
			}
			if(!empty($postData['matchDate'])){
				$newData['Date']=$postData['matchDate'];
			}
			if(!empty($_POST['status'])){
				$postData['status']=$_POST['status'];
				$newData['rxstatus']=$_POST['status'];
			}
			if(!empty($_POST['patientName'])){
				$postData['patientName']=$_POST['patientName'];
				$newData['patient']=$_POST['patientName'];
			}
			if(!empty($_POST['rxNumber'])){
				$postData['rxNumber']=$_POST['rxNumber'];
				$newData['rx']=$_POST['rxNumber'];
			}
			if(!empty($_POST['storeName'])){
				$postData['storeName']=$_POST['storeName'];
				$newData['store']=$_POST['storeName'];
			}
			$this->session->set_userdata($newData);
			
		}
		else 
		{			
			$postData['matchDate']=$this->session->userdata('Date');
			$postData['status']=$this->session->userdata('rxstatus');
			$postData['patientName']=$this->session->userdata('patient');
			$postData['rxNumber']=$this->session->userdata('rx');
			$postData['storeName']=$this->session->userdata('store');			
		}
		$base_url= base_url() .'pharmacist/rx/advancesearch/'; 
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")
			$startCount=0;
		else 
			$startCount=$this->uri->segment(4);		
		$this->data['prescription'] = $this->rx_model->getAdvancedSearchData($postData , $config['per_page'],$startCount);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();
		$count = $this->rx_model->countAdvancedSearchData($postData);
		$message="Your search result ";
		if(!empty($postData))
		{
			if(!empty($postData['status']))
			{				
				if($postData['status']=='Approval')
					$msgstatus='Awaiting Approval';
				elseif($postData['status']=='Counsel')
					$msgstatus= 'Awaiting Counsel';
				elseif($postData['status']=='Completed')
					$msgstatus= 'Completed';
				elseif($postData['status']=='Cancelled')
					$msgstatus= 'Cancelled';
				elseif($postData['status']=='Draft')
					$msgstatus= 'Draft';
				elseif($postData['status']=='Archive')
					$msgstatus= 'Archived';
				elseif($postData['status']=='OnHold')
					$msgstatus= 'OnHold';
				elseif($postData['status']=='All')
					$msgstatus= 'Awaiting Approval, Awaiting Counsel, Completed,OnHold,Cancelled, Draft and Archived';				
				$message.=$msgstatus." ";
			}
			if(!empty($postData['patientName']))
				$message.=$postData['patientName']." patient ";
			if(!empty($postData['rxNumber']))
				$message.="Rx# ".$postData['rxNumber']." ";
			if(!empty($postData['matchDate']))
			{
				$showDate=date('m/d/Y',strtotime($postData['matchDate']));
				$message.="date from ".$showDate." to ".date('m/d/Y');	
			}
		}
		else
		{
			$message.="all";
		}
		if(isset($_POST) && !empty($_POST))
		{
			$this->session->unset_userdata('showMessage');
			$this->session->set_userdata('showMessage',$message);
		}
		elseif($flag!=0)
		{
			$message=$this->session->userdata('showMessage');
		}
		else
		{
			$this->session->unset_userdata('showMessage');
		}	
		$this->data['message']=$message;
		$config['total_rows'] = $count;
		$this->data['count'] = $count;
		$this->pagination->initialize($config);
		$this->load->view('pharmacist/advanceresult',$this->data);
	}		
	function checkVideoConferencing()
	{
		$conf = $this->rx_model->checkliveConference();
		if(empty($conf))
			echo "error";
		else 
			echo $conf[0]->rxId."-".$conf[0]->id."-".$conf[0]->storeName;
	}		
	function changePharmaqueue($confId)
	{
		$conf = $this->rx_model->changePharmaqueue($confId);
	}	
	function closeVideoConferencing($rxId,$confId)
	{
		$this->rx_model->closeliveConference($rxId,$confId);
		redirect('pharmacist/rx/view/'.$rxId);
	}	
	
	function closeVideoPatientCall($confId)
	{

		$this->rx_model->closePharmaConfCall($confId);
		redirect('pharmacist/viewmore/');
	}	
	
	function deletepharmaqueue($confId)
	{
		$conf = $this->rx_model->deletepharmaqueue($confId);
	}	
	function setcall($call)
	{
		$this->session->set_userdata('videoprop', '1');
		if(!empty($call))
		{
			$this->session->set_userdata('videoCall', '1');
			$this->users->pharmaActiveSet();
		}
		else
		{
			$this->session->set_userdata('videoCall', '0');
			$this->users->logoutUser();
			$this->rx_pharmacist->logoutPharma();
		}
	}
	
	function patientCall(){
		$storeId=$this->session->userdata('storeIds');
		$this->data['countApproval'] = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
		$this->data['countCounsel'] = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId);
		$this->data['countArchive'] = $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
		$this->data['countCompleted'] = $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
		$this->data['countCancelled'] = $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId);
		$this->data['countOnHold']= $this->rx_pharmacist->countAllPrescriptions('onHold',$storeId); 
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();

		$this->data['videoConf'] = $this->rx_model->getvideoConfiguration($this->session->userdata('storeId'),"patient" );	
		$this->load->view('pharmacist/patientCall',$this->data);
	}
}