<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Viewmore extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('rx_model');	
		$this->load->model('rx_pharmacist');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			redirect("index");
		$this->data['stores'] = $this->rx_pharmacist->getAllStores();
	}
 
		//to call this function displaying all rx with related to status 
	function index($number='')
	{
		$storeId=$this->session->userdata('storeIds');			
		if(isset($_POST['storeName']) )
		{
			$storeId=$_POST['storeName'];
			$this->session->set_userdata('storeIds', $storeId);
		}
		$this->session->unset_userdata('number');
		if($number=='Approval')	$status='Approval'; 
		elseif($number=='Counsel')	$status='Counsel';
		elseif($number=='Completed') $status='Completed';			
		elseif($number=='Archive')    $status='Archive';
		elseif($number=='Cancelled')   $status='Cancelled';
		elseif($number=='OnHold')   $status='OnHold';
		if(!empty($status))
		{	
			$this->session->set_userdata('status', $status);
			$this->session->set_userdata('number', $status);
		}		
		$status=$this->session->userdata('status');
		$base_url=base_url().'pharmacist/viewmore/index/'.$number.'/';
		$config['base_url']=$base_url;
		$config['per_page']=10;
		$config['uri_segment']='5';
		if($this->uri->segment(5)=="")$startcount=0;
		else $startcount=$this->uri->segment(5);		
		$this->data['allPrescription']=$this->rx_pharmacist->getMorePrescription($config['per_page'] , $startcount , $status , $storeId);		
		$count=$this->rx_pharmacist->countMorePrescription($status , $storeId);	
		$config['total_rows']=$count;
		$this->data['total']=$count;		 
		$this->pagination->initialize($config);
		$this->data['activities'] = $this->rx_pharmacist->getAllActivity('','1',$storeId);
		$this->data['activitiesMore'] = $this->rx_pharmacist->getAllActivity('','',$storeId);			
		$this->data['countApproval'] = $this->rx_pharmacist->countAllPrescriptions('Approval',$storeId);
		$this->data['countCounsel'] = $this->rx_pharmacist->countAllPrescriptions('Counsel',$storeId);
		$this->data['countArchive'] = $this->rx_pharmacist->countAllPrescriptions('Archive',$storeId);
		$this->data['countCompleted'] = $this->rx_pharmacist->countAllPrescriptions('Completed',$storeId);
		$this->data['countCancelled'] = $this->rx_pharmacist->countAllPrescriptions('Cancelled',$storeId);						
		$this->data['countOnHold'] = $this->rx_pharmacist->countAllPrescriptions('OnHold',$storeId);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();								
		$this->load->view('pharmacist/viewmores',$this->data);	
	}	
		//This function is used for updating the priority
		function updatepriority($id,$priority,$seg,$storeId) 
		{
			if(!empty($id))
			{
				$message=$this->rx_pharmacist->updatePriority($id,$priority);		
				$this->data['rxId']=$id;
				$this->data['status']='Edit';
				$this->data['userId']=$this->session->userdata('userId');
				$this->data['storeId']=$storeId;		
				if(!empty($message))
				{
					$this->rx_model->addStatusViewmore($this->data); 
					$message ="High priority set successfully";
				}
				else 
					$message = "Priority could not be set successfully";
				$this->session->set_flashdata('message', $message);		    
				redirect('pharmacist/viewmore/index/'.$seg);
			}
			else		
				$this->load->view('pharmacist/viewmores');	
		}
		//This function is used for updating the approve to counsel
		function updateapprove() 
		{ 
			$id=$_POST['rxId'];
			$comment=$_POST['comment'];
			$seg=$_POST['segId'];
			$storeId=$_POST['storeId'];			
				if(!empty($id))
				{
					$message=$this->rx_pharmacist->updateApprove($id,$comment,$storeId);			
					if(!empty($message))		
						$message ="Rx approved successfully";
					else 
						$message = "Rx could not be approved successfully";
					$this->session->set_flashdata('message', $message);		    
					redirect('pharmacist/viewmore/index/'.$seg);
				}
				else		
					$this->load->view('pharmacist/viewmores');	
		}	
		//This function is used for updating the approve to Reject
		function updatereject()
		{		
			$id=$_POST['rxId']; 
			$comment=$_POST['comment'];
			$seg=$_POST['segId'];		
			$storeId=$_POST['storeId'];				
				if(!empty($id))
				{ 
					$message=$this->rx_pharmacist->updateReject($id,$comment,$storeId);			
					if(!empty($message))
						$message ="Rx rejected successfully";
					else 
						$message = "Rx could not be rejected";
					$this->session->set_flashdata('message', $message);		    
					redirect('pharmacist/viewmore/index/'.$seg);
				}
				else		
					$this->load->view('pharmacist/viewmores');	
		}
		//This function is used for updating the counsel to complete
		function updatecomplete()
		{		
			$id=$_POST['rxId']; 
			$comment=$_POST['comment'];
			$storeId=$_POST['storeId'];	
			$seg=$_POST['segId'];		
			$usertype= $this->session->userdata('userType');					
				if(!empty($id))
				{ 
					$message=$this->rx_pharmacist->updateCompleted($id,$comment,$storeId);			
					if(!empty($message))
						$message ="Rx completed successfully";
					else 
						$message = "Rx could not be completed";
					$this->session->set_flashdata('message', $message);
					if($usertype=='Technician')
						$flag='technician';
					elseif($usertype=='Pharmacist')
						$flag='pharmacist';
					redirect($flag.'/viewmore/index/'.$seg);
				}
				else		
					$this->load->view('pharmacist/viewmores');	
		}		
		//This function is used for updating the counsel to complete
		function setComplete()
		{		
			$id=$_POST['rxId']; 
			$comment=$_POST['comment'];
			$storeId=$_POST['storeId'];
			$message=$this->rx_pharmacist->updateCompleted($id,$comment,$storeId);
			$usertype= $this->session->userdata('userType');			
			if($usertype=='Technician')
				$flag='technician';
			elseif($usertype=='Pharmacist')
				$flag='pharmacist';			
			redirect($flag.'/rx/'.$_POST['pageName']);	
		}			
		//This function is used for updating the counsel to Cancelled
		function updatecancel()
		{	
			$id=$_POST['rxId']; 
			$comment=$_POST['comment'];
			$storeId=$_POST['storeId'];	
			$seg=$_POST['segId'];
			$usertype= $this->session->userdata('userType');			
				if(!empty($id))
				{ 
					$message=$this->rx_pharmacist->updateCancelled($id,$comment,$storeId);			
					if(!empty($message))
						$message ="Rx cancelled successfully";
					else 
						$message = "Rx could not be cancelled";
					$this->session->set_flashdata('message', $message);		    
					//redirect('pharmacist/viewmore/index/'.$seg);
					if($usertype=='Technician')
						$flag='technician';
					elseif($usertype=='Pharmacist')
						$flag='pharmacist';
					redirect($flag.'/viewmore/index/'.$seg);
				}
				else		
					$this->load->view('pharmacist/viewmores');	
		}
		//This function is used for updating the archieve
		function updatearchive() 
		{  
			$id=$_POST['rxId'];
			$comment=$_POST['comment'];
			$seg=$_POST['segId'];
			$storeId=$_POST['storeId'];
			$usertype= $this->session->userdata('userType');
				if(!empty($id))
				{
					$message=$this->rx_pharmacist->updateArchive($id,$comment,$storeId);			
					if(!empty($message))		
						$message ="Rx record archived";
					else 
						$message = "Rx could not be archive";
						$this->session->set_flashdata('message', $message);		    
					//redirect('pharmacist/viewmore/index/'.$seg);
					if($usertype=='Technician')
						$flag='technician';
					elseif($usertype=='Pharmacist')
						$flag='pharmacist';
					redirect($flag.'/viewmore/index/'.$seg);
				}
				else		
				$this->load->view('pharmacist/viewmores');	
		}
		//This function is used for setting to archieve
		function setArchive() 
		{  
			$rxId=$_POST['rxId'];
			$comment=$_POST['comment'];
			$storeId=$_POST['storeId'];
			$usertype= $this->session->userdata('userType');		
			$this->rx_pharmacist->updateArchive($rxId,$comment,$storeId);
			$usertype= $this->session->userdata('userType');			
			if($usertype=='Technician')
				$flag='technician';
			elseif($usertype=='Pharmacist')
				$flag='pharmacist';
			redirect($flag.'/rx/'.$_POST['pageName']);				
			//redirect('technician/rx/'.$_POST['pageName']);
		}
		//This function is used for changing cancelled to approval
		function updateresubmit() 
		{
			$id=$_POST['rxId'];
			$comment=$_POST['comment'];
			$seg=$_POST['segId'];
			$storeId=$_POST['storeId'];
			$usertype= $this->session->userdata('userType');
			if(!empty($id))
			{
				$message=$this->rx_pharmacist->updateResubmit($id,$comment,$storeId);			
				if(!empty($message))		
					$message ="Rx re-submited successfully";
				else 
					$message = "Rx could not be re-submited ";
				$this->session->set_flashdata('message', $message);		    
				//redirect('pharmacist/viewmore/index/'.$seg);
				if($usertype=='Technician')
					$flag='technician';
				elseif($usertype=='Pharmacist')
					$flag='pharmacist';
				redirect($flag.'/viewmore/index/'.$seg);
			}
			else		
				$this->load->view('pharmacist/viewmores');	
		}
		function getNewRX(){
			$message=$this->rx_pharmacist->getNewRX();			
		}
		function getNotification(){
			$this->rx_model->getnotification('10');
		}
		function moreNotification()
		{
			$this->load->library('pagination');
			$storeId=$this->session->userdata('storeId');		
			$this->data['countApproval'] = $this->rx_model->countStatusCount('Approval',$storeId);
			$this->data['countRejected'] = $this->rx_model->countStatusCount('Rejected',$storeId);
			$this->data['countCounsel'] = $this->rx_model->countAllPrescriptions('Counsel',$storeId);
			$this->data['countCompleted'] = $this->rx_model->countAllPrescriptions('Completed',$storeId);
			$this->data['countCancelled'] = $this->rx_model->countAllPrescriptions('Cancelled',$storeId);
			$this->data['countDraft'] = $this->rx_model->countAllPrescriptions('Draft',$storeId);		
			$this->data['countOnHold'] = $this->rx_model->countAllPrescriptions('OnHold',$storeId);	
			$this->data['countNotification'] = $this->rx_model->countUnreadComments();	
			$config=array();
			$base_url= base_url().'pharmacist/viewmore/moreNotification';	
			$config['base_url'] = $base_url;
			$config['per_page'] =10;
			$config['uri_segment'] = '4';   
			$count=$this->rx_model->countMoreNotification();
			$config['total_rows']=$count;
			$this->data['total']=$count;
			$page=($this->uri->segment(4))?$this->uri->segment(4):0;
			$this->data['notification']=$this->rx_model->getMoreNotification($config['per_page'], $page);
			$this->pagination->initialize($config);	
			$this->load->view('pharmacist/viewnotification',$this->data);		
		}
		function addComment()
		{
			$this->rx_model->insertComment($_POST['commentid']);
		}
		function clearNotification()
		{
			$this->rx_model->clearComment();
		}
}