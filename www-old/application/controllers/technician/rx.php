<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rx extends CI_Controller 
{
	function capturepicture()
	{
		$str = file_get_contents("php://input");
		file_put_contents("../uploads/rajkumar.jpg", pack("H*", $str));
	}
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('rx_model');
		$this->load->library('image_lib');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			redirect("index");
			$storeId = $this->session->userdata('storeId');
 			$this->data['countApproval'] = $this->rx_model->countStatusCount('Approval',$storeId);
			$this->data['countRejected'] = $this->rx_model->countStatusCount('Rejected',$storeId);
			$this->data['countCounsel'] = $this->rx_model->countAllPrescriptions('Counsel',$storeId);
 			$this->data['countCompleted'] = $this->rx_model->countAllPrescriptions('Completed',$storeId);
			$this->data['countCancelled'] = $this->rx_model->countAllPrescriptions('Cancelled',$storeId);
			$this->data['countDraft'] = $this->rx_model->countAllPrescriptions('Draft',$storeId);
			$this->data['countOnHold'] = $this->rx_model->countAllPrescriptions('OnHold',$storeId);
	}
	function getnotification()
	{
 			$result= array();
			$storeId=$this->session->userdata('storeId');		
			$napproval = $this->rx_model->countStatusCount('Approval',$storeId);
			$nrejected = $this->rx_model->countStatusCount('Rejected',$storeId);			
			$nconsuel = $this->rx_model->countAllPrescriptions('Counsel',$storeId);
			$ncompleted= $this->rx_model->countAllPrescriptions('Completed',$storeId);
			$ncancelled= $this->rx_model->countAllPrescriptions('Cancelled',$storeId);
			$ndraft  = $this->rx_model->countAllPrescriptions('Draft',$storeId);
			$nnotification=$this->rx_model->countUnreadComments();	
			$result['countApproval'] =($napproval > 0 )?$napproval:'';
			$result['countRejected'] =($nrejected > 0 )?$nrejected:'';
			$result['countCounsel']= ($nconsuel > 0 )?$nconsuel:'';
			$result['countCompleted']= ($ncompleted > 0 )?$ncompleted:'';
			$result['countCancelled']= ($ncancelled > 0 )?$ncancelled:'';
			$result['countDraft']= ($ndraft > 0 )?$ndraft:'';
			$result['countNotification']= ($nnotification > 0 )?$nnotification:'';
			echo json_encode($result);
 	}
	//To call this function for getting all prescription related to a specific Technician
	public function index() 
	{
		redirect('technician/viewmore/index/Approval');
	}
	//To call this function for creating RX
	function createrx($rxId='')
	{
		$this->data['highest'] = $this->rx_model->getHighestPriority();
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();	
		if(!empty($rxId))
		{
			$this->data['patientData'] = $this->rx_model->getPatientInfo($rxId);
		}
		$this->load->view('createrx' , $this->data);
	}	

	/**
	 * Looks up drug images, given an NDC number. Returns a list of image filenames separated
	 * by semi-colons.
	 */
	function ajaxImgLookup() {
		if ( empty($_POST['ndc']) )
			exit();
		
		$files = $this->rx_model->getNDCDrugImage($_POST['ndc']);
		
		if ( !empty($files) ) {
			// append site base url
			foreach ( $files as $f => $file )
				$files[$f] = base_url().$file;
			// return file paths
			echo implode(';', $files);
		}
		exit();
	}
	
	//To call this function for checking and getting the data related to that rx number
	function getRxData()
	{
		$rxNumber='';
		if(isset($_POST['rxNumber']) && !empty($_POST['rxNumber']))
			$rxNumber =$_POST['rxNumber'];		

		 $storeId=$this->session->userdata('storeId');		

	$serviceData= $this->rx_model->getServiceURL($storeId);
 
		if(!empty($serviceData->storeUrl))
		{
			$url=$serviceData->storeUrl."?rxNumber=".$rxNumber."";
		//	$process = curl_init("http://216.248.98.151:90/test.php?rxNumber=".$rxNumber."");
			$process = curl_init($url);
			$data="rxNumber=".$rxNumber."";
			curl_setopt($process, CURLOPT_POSTFIELDS, $data);
			curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($process, CURLOPT_POST, 1);
			$returnData = curl_exec($process);
			curl_close($process);
			if(!empty($returnData))
			{
				echo $returnData;
			}
		}

	}	
	//To call this function for saving new prescription
	public function save($add='')
	{					
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstName', 'First Name', 'required');
		$this->form_validation->set_rules('rxNumber', 'RX Number', 'required');
		if ($this->form_validation->run() != FALSE) 
		{
			if(!empty($_POST['uploadImage']))
			{
				$counter=0;
				foreach($_POST['uploadImage'] as $uploadImage)
				{
					//copy('foo/test.php', 'bar/test.php');
					$sourceDir='./tmpimages/'.$uploadImage;
					$destinationDir='./uploads/technician/uploadimage/'.$uploadImage;
					if(file_exists($sourceDir))
						rename($sourceDir, $destinationDir);
					$_POST['drugImage'][$counter]=$uploadImage;
					$counter++;
				}
			}
			if(!empty($_POST['capture_image']))
			{
				$counter=0;
				foreach($_POST['capture_image'] as $capture_imagee)
				{
					$sourceDir='./tmpimages/'.$capture_imagee;
					$destinationDir='./uploads/technician/captureimage/'.$capture_imagee;
					if(file_exists($sourceDir))
						rename($sourceDir, $destinationDir);
					$_POST['captureImage'][$counter]=$capture_imagee;
					$counter++;
				}
			}
			$onhold='';
			if(isset($_POST['onhold']))
					$onhold=$_POST['onhold'];

			if(!empty($add) && $add=='Draft')
				$_POST['status']='Draft';
			elseif(!empty($onhold) && $onhold=='1')				
				$_POST['status']='OnHold';			
			else
				$_POST['status']='Approval';
								
			$rxId= $this->rx_model->saveRx($this->input->post());
			if(!empty($rxId))
			{
				$message = "Rx added successfully"; 
			}
			else
			{
				$message = "Rx could not be added";
			}		
			$this->session->set_flashdata('message', $message);
			if(!empty($add) && $add=='add')
				redirect('technician/rx/createrx/'.$rxId);			
			else
				redirect('technician/rx/index');
		}
		else
		{
			$this->load->view('createrx');
		}				
	}
	//To call this function for creating thumbnail
	function _createThumbnail($fileName ,  $width ,$height , $path)
	{		
		$config['image_library'] = 'gd2';
		$config['source_image'] = 'uploads/technician/'.$fileName;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = FALSE;
		$config['width'] = $width;
		$config['height'] = $height;
		$config['new_image']=  './uploads/technician/'.$path.'/' ;
		$this->load->library('image_lib', $config);
		$this->image_lib->initialize($config);		
		if($this->image_lib->resize())
		{
			//echo "Success";                                
		}
		else
		{
			//echo "Failed." .$i . $this->image_lib->display_errors();
		}	
	}
	function getrejectedalert($status,$techId=0,$pharm=0)
	{
	//	$this->->rx_model->getTechincanAlerts($status,$techId=0,$pharm=0);
	}
	//To call this function for getting the data of one prescrition by passing it's Id
	function view($rxId,$childId='')
	{	
		if(isset($_POST["comments"]))
		{
			$commenttxt =trim($_POST["comments"]);
			if(!empty($commenttxt)) $this->addcomment();
		}				
		$this->data['childRx']=$this->rx_model->getChildRx($rxId);
		if(!empty($childId)) $rxId= $childId;
		$this->data['patientData']= $this->rx_model->getRxRecord($rxId);
 		$this->data['capturedImage']= $this->rx_model->getCapturedImage($rxId);
		$this->data['drugImage']= $this->rx_model->getDrugImage($rxId);
		$this->data['NDCdrugImage']= $this->rx_model->getNDCDrugImage($this->data['patientData']->ndc_number);
		$this->data['filledBy']=$this->rx_model->getUserName($this->data['patientData']->technicianId);
		$this->data['statusChangedBy']=$this->rx_model->getUserName($this->data['patientData']->changedStatusBy);
		$this->data['comments']=$this->rx_model->getComments($rxId,'5');
		$this->data['commentsMore']=$this->rx_model->getComments($rxId);
		$storeId = $this->session->userdata('storeId');
		$this->data['activities'] = $this->rx_model->getAllActivity($storeId,$rxId,'1');		
		$this->data['activitiesMore'] = $this->rx_model->getAllActivity($storeId,$rxId );		
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();	
		$this->data['otherdata']= $this->rx_model->getDrugPresciber($rxId);

		$this->load->view('view',$this->data);
	}	
	//To call this function for adding new comment
	function addcomment()
	{
		if(isset($_POST['commentRxId']))
		{
			$data=array("comments"=>$this->input->post("comments"),
						'rxId'=>$_POST['commentRxId'],
						"userId"=>$this->session->userdata["userId"]
						);
			$this->rx_model->addComment($data);
		}
	}	
	//To call this function for setting High
	function sethigh($parentRxId , $currentRxId='')
	{
		$url=$parentRxId;
		if(!empty($currentRxId))
		{
			$url.="/".$currentRxId;
			$RxId=$currentRxId;
		}
		else
		{
			$RxId=$parentRxId;
		}
		$this->rx_model->setHigh($RxId);
		$message = "High priority set Successfully";
		$this->session->set_flashdata('message', $message);
		$usertype=$this->session->userdata('userType');
		if($usertype=='Technician')
			$flag='technician';
		elseif($usertype=='Pharmacist')
			$flag='pharmacist';
		redirect($flag.'/rx/view/'.$url);
	}
	//To call this function for editing RX
	function edit($currentRxId)
	{
		$this->data['highest'] = $this->rx_model->getHighestPriority();
		$this->data['patientData'] = $this->rx_model->getPatientInfo($currentRxId);		
		$this->data['capturedImage']= $this->rx_model->getCapturedImage($currentRxId);
		$this->data['drugImage']= $this->rx_model->getDrugImage($currentRxId);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();				
		$this->load->view('editrx' , $this->data);
	}	
	//To call this function for saving new prescription
	public function update($add='')
	{
		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		die();*/
		$this->load->library('form_validation');
		$this->form_validation->set_rules('firstName', 'First Name', 'required');
		$this->form_validation->set_rules('rxNumber', 'RX Number', 'required');
		if ($this->form_validation->run() != FALSE) 
		{			
			if(!empty($_POST['uploadImage']))
			{
				$counter=0;
				foreach($_POST['uploadImage'] as $uploadImage)
				{
					//copy('foo/test.php', 'bar/test.php');
					$sourceDir='./tmpimages/'.$uploadImage;
					$destinationDir='./uploads/technician/uploadimage/'.$uploadImage;
					if(file_exists($sourceDir))
						rename($sourceDir, $destinationDir);
					$_POST['drugImage'][$counter]=$uploadImage;
					$counter++;
				}
			}
			if(!empty($_POST['capture_image']))
			{
				$counter=0;
				foreach($_POST['capture_image'] as $capture_imagee){
					$sourceDir='./tmpimages/'.$capture_imagee;
					$destinationDir='./uploads/technician/captureimage/'.$capture_imagee;
					if(file_exists($sourceDir))
						rename($sourceDir, $destinationDir);
					$_POST['captureImage'][$counter]=$capture_imagee;
					$counter++;
				}
			}
			if(!empty($add) && $add=='Draft')
			{
				$_POST['status']='Draft';
				$_POST['priority']='0';
			}
			else
				$_POST['status']=$add;
			//$_POST['refill'];	
			$onhold=$_POST['onhold'];
			//echo $onhold;
			//die();
			if(!empty($onhold) && $onhold=='1')
				$_POST['status']='OnHold';
			else
				$_POST['status']=$add;
				//$_POST['priority']='0';
			$result=$this->rx_model->updateRx($this->input->post());
			$rxId=$_POST['rxId'];
			if(!empty($result))
			{
				$message = "Rx updated successfully"; 
			}
			else
			{
				$message = "Rx could not be updated";
			}		
			$this->session->set_flashdata('message', $message);
			if(!empty($_POST['status']) && $_POST['status']=='add')
				redirect('technician/rx/createrx/'.$rxId);
			else
				redirect('technician/rx/index');
		}
		else
		{
			$this->load->view('editrx');
		}				
	}	
	//To call this function for setting counsel to complete
	function completecounsel($rxId)
	{
		$data['status']='Completed';
		$data['refuse']='1';
		$data['rxId']=$rxId;
		$segment5=$this->uri->segment(5);
		$result=$this->rx_model->changeStatus($data);
		if(!empty($result))
		{
			$message = "Status completed Successfully";
		}
		else
		{
			$message = "Status couldn't be completed";
		}		
		$this->session->set_flashdata('message', $message);
		if(!empty($segment5))
			redirect('technician/viewmore/index/'.$segment5);
		else
			redirect('technician/rx/view/'.$rxId);
	}	
	//To call this function for setting Draft to Approval 
	function setDraftToApproval()
	{	
		$_POST['status']='Approval';
		$_POST['priority']='0';	
		if(!empty($_POST['uploadImage']))
		{		
			$counter=0;
			foreach($_POST['uploadImage'] as $uploadImage)
			{
				$sourceDir='./tmpimages/'.$uploadImage;
				$destinationDir='./uploads/technician/uploadimage/'.$uploadImage;
				if(file_exists($sourceDir))
					rename($sourceDir, $destinationDir);
				$_POST['drugImage'][$counter]=$uploadImage;
				$counter++;
			}
		}
		if(!empty($_POST['capture_image']))
		{
			$counter=0;
			foreach($_POST['capture_image'] as $capture_imagee)
			{
				$sourceDir='./tmpimages/'.$capture_imagee;
				$destinationDir='./uploads/technician/captureimage/'.$capture_imagee;
				if(file_exists($sourceDir))
					rename($sourceDir, $destinationDir);
				$_POST['captureImage'][$counter]=$capture_imagee;
				$counter++;
			}
		}			
		$result=$this->rx_model->changeStatus($this->input->post());
		if(!empty($result))
		{
			$message = "Rx record has been re-submitted";
		}
		else 
		{
			$message = "Rx record couldn't be re-submitted";
		}		
		$this->session->set_flashdata('message', $message);
		redirect('technician/rx/index');
	}
	//To call this function for getting the searched data
	function search()
	{
		if(isset($_POST) && !empty($_POST))
		{
			$this->session->unset_userdata('searchRxNumber');
			$data['search']=$_POST['search'];
			$this->session->set_userdata('searchRxNumber',$_POST['search']);
		}
		else
		{
			$data['search']=$this->session->userdata('searchRxNumber');
		}		
		$base_url= base_url() .'technician/rx/search/'; 
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);
		$this->data['prescription'] = $this->rx_model->getSearchedData($data , $config['per_page'],$startCount);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();
		$count = $this->rx_model->countSearchedData($data);
		if(!empty($data['search']))
			$this->data['message']='Result of '.$data['search'];
		else
			$this->data['message']='Result of all';
		$config['total_rows'] = $count;
		$this->data['count'] = $count;
		$this->pagination->initialize($config);
		$this->load->view('result',$this->data);
	}
	//To call this function for getting advanced searched data
	function advancesearch()
	{
		$postData=array();
		$newData=array();
		$flag=0;
		$segment4=$this->uri->segment(4);
		if($segment4!='')
			$flag=1;
		elseif(isset($segment4))
			$flag=1;
		if(isset($_POST) && !empty($_POST))
		{						
			$this->session->unset_userdata('Date');
			$this->session->unset_userdata('rxstatus');
			$this->session->unset_userdata('patient');
			$this->session->unset_userdata('rx');
			if(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='1')
			{
				$postData['matchDate']=date('Y-m-d');
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='2')
			{
				$number=date('w');
				$postData['matchDate']=date('Y-m-d',strtotime("-".$number." days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='3')
			{
				$number=date('w');
				$number+=7;
				$postData['matchDate']=date('Y-m-d',strtotime("-".$number." days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='4')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-30 days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='5')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-60 days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='6')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-182 days"));
			}
			elseif(!empty($_POST['dateWithin']) && $_POST['dateWithin']=='7')
			{
				$postData['matchDate']=date('Y-m-d',strtotime("-365 days"));
			}
			if(!empty($postData['matchDate'])){
				$newData['Date']=$postData['matchDate'];
			}
			if(!empty($_POST['status'])){
				$postData['status']=$_POST['status'];
				$newData['rxstatus']=$_POST['status'];
			}
			if(!empty($_POST['patientName'])){
				$postData['patientName']=$_POST['patientName'];
				$newData['patient']=$_POST['patientName'];
			}
			if(!empty($_POST['rxNumber'])){
				$postData['rxNumber']=$_POST['rxNumber'];
				$newData['rx']=$_POST['rxNumber'];
			}
			$this->session->set_userdata($newData);		
		}
		else 
		{		
			$postData['matchDate']=$this->session->userdata('Date');
			$postData['status']=$this->session->userdata('rxstatus');
			$postData['patientName']=$this->session->userdata('patient');
			$postData['rxNumber']=$this->session->userdata('rx');			
		}
		$base_url= base_url() .'technician/rx/advancesearch/'; 
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '4'; 
		if ($this->uri->segment(4)=="")$startCount=0;
		else $startCount=$this->uri->segment(4);		
		$this->data['prescription'] = $this->rx_model->getAdvancedSearchData($postData , $config['per_page'],$startCount);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();
		$count = $this->rx_model->countAdvancedSearchData($postData);
		$message="Your search result ";
		if(!empty($postData))
		{
			if(!empty($postData['status']))
			{			
				if($postData['status']=='Approval')
				$msgstatus='Awaiting Approval';
				elseif($postData['status']=='Counsel')
				$msgstatus= 'Awaiting Counsel';
				elseif($postData['status']=='Completed')
				$msgstatus= 'Completed';
				elseif($postData['status']=='Cancelled')
				$msgstatus= 'Cancelled';
				elseif($postData['status']=='Draft')
				$msgstatus= 'Draft';
				elseif($postData['status']=='OnHold')
				$msgstatus= 'OnHold';
				elseif($postData['status']=='Archive')
				$msgstatus= 'Archived';
				elseif($postData['status']=='All')
				$msgstatus= 'Awaiting Approval, Awaiting Counsel, Completed, Cancelled,OnHold, Draft and Archived';			
				$message.=$msgstatus." ";
			}				
			if(!empty($postData['patientName']))
				$message.=$postData['patientName']." patient ";
			if(!empty($postData['rxNumber']))
				$message.="Rx# ".$postData['rxNumber']." ";
			if(!empty($postData['matchDate']))
			{
				$showDate=date('m/d/Y',strtotime($postData['matchDate']));
				$message.="date from ".$showDate." to ".date('m/d/Y');	
			}				
		}
		else
		{
			$message.="all";
		}
		if(isset($_POST) && !empty($_POST))
		{
			$this->session->unset_userdata('showMessage');
			$this->session->set_userdata('showMessage',$message);
		}
		elseif($flag!=0)
		{
			$message=$this->session->userdata('showMessage');
		}
		else
		{
			$this->session->unset_userdata('showMessage');
		}
		$this->data['message']=$message;
		$config['total_rows'] = $count;
		$this->data['count'] = $count;
		$this->pagination->initialize($config);
		$this->load->view('advanceresult',$this->data);
	}
	//To call this function for uploading temp file
	function uploadFile()
	{
		$filearray=array();
		if(!empty($_FILES['drugImage']["name"]))
		{
			$filename = $_FILES['drugImage']["name"];
			$files=explode('.' , $filename);
			$fileextension=$files[1];
			$randname = uniqid().'.'.$fileextension;
			$config['upload_path'] = './uploadjs/';			
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '40000';
			$config['file_name']  = $randname ; 
			$this->load->library('upload', $config); 
			$filearray= $config['file_name'];
			$this->upload->initialize($config);			
			if ( ! $this->upload->do_upload('drugImage'))
			{
				 $error= array('error' => $this->upload->display_errors());
				 $message="Error";
			}
			else 
			{
				$filearray = $this->upload->data();
				$_POST['drugImage']=$filearray['file_name'];
				$this->_createThumbnail($_POST['drugImage'] , '400' , '260' , 'thumbnail');
				$this->_createThumbnail($_POST['drugImage'] , '430' , '260' , 'largeimage');
				$message="Success";
			}
			$this->image_lib->clear();
		}		
		echo "<font color=red>".$message."</font>";
	}	
}