<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Viewmore extends CI_Controller
{
	 function __construct() {
        parent::__construct();
		$this->load->model('rx_model');
		$this->load->model('rx_pharmacist');
		$userId = $this->session->userdata('userId');
		if(empty($userId))
			redirect("index");
	}
	//to call this function displaying all rx with related to status and storeId
	function index($number='')
	{
		if($number=='Approval')		$status='Approval'; 
		elseif($number=='Counsel')	$status='Counsel';
		elseif($number=='Completed')    $status='Completed';
		elseif($number=='Draft')	$status='Draft';		
		elseif($number=='Cancelled')    $status='Cancelled';		
		elseif($number=='OnHold')    $status='OnHold';
		elseif($number=='Rejected') $status='Rejected';
		if(!empty($status))		
			$this->session->set_userdata('status', $status);
		$storeId=$this->session->userdata('storeId');	
		$status=$this->session->userdata('status');			
		$base_url= base_url().'technician/viewmore/index/'.$number.'/';	
		$config['base_url'] = $base_url;
		$config['per_page'] =10;
		$config['uri_segment'] = '5';   
		if($this->uri->segment(5)=="")$startCount=0;
		else $startCount=$this->uri->segment(5);				
		$this->data['status']=	$status;			
		$this->data['allPrescription']=$this->rx_pharmacist->getMorePrescription($config['per_page'],$startCount,$status,$storeId);		
		$count=$this->rx_pharmacist->countMorePrescription($status,$storeId);	
		$this->data['activities'] = $this->rx_model->getAllActivity($storeId,'','1');
		$this->data['activitiesMore'] = $this->rx_model->getAllActivity($storeId);		
		$this->data['countApproval'] = $this->rx_model->countStatusCount('Approval',$storeId);
		$this->data['countRejected'] = $this->rx_model->countStatusCount('Rejected',$storeId);
		$this->data['countCounsel'] = $this->rx_model->countAllPrescriptions('Counsel',$storeId);
		$this->data['countCompleted'] = $this->rx_model->countAllPrescriptions('Completed',$storeId);
		$this->data['countCancelled'] = $this->rx_model->countAllPrescriptions('Cancelled',$storeId);
		$this->data['countDraft'] = $this->rx_model->countAllPrescriptions('Draft',$storeId);	
		$this->data['countOnHold'] = $this->rx_model->countAllPrescriptions('OnHold',$storeId);
		$this->data['countNotification'] = $this->rx_model->countUnreadComments();	
		$config['total_rows']=$count;
		$this->data['total']=$count;		 
		$this->pagination->initialize($config);		
		$this->load->view('viewmores',$this->data);	
	}	
	//This function is used for updating the status
	 function update($id,$status='') 
	 {  
		$seg=$this->uri->segment(6);	
		if(!empty($id))
		{		
			$message=$this->rx_model->updateStatus($id,$status);			
			$this->data['rxId']=$id;
			$this->data['status']='Archive';
			$this->data['userId']=$this->session->userdata('userId');
			$this->data['storeId']=$this->session->userdata('storeId');
			if(!empty($message))
			{
				$this->rx_model->addStatusViewmore($this->data);
				$message ="Rx record archived";
			}
			else 
				$message = "Rx could not be archive";
			$this->session->set_flashdata('message', $message);	
			if(!empty($seg))
			{	    
				redirect('technician/viewmore/index/'.$seg);
			}
			else 
			{
				redirect('technician/rx/view/'.$id);
			}
		}
		else	
			redirect('technician/viewmore/index/'.$seg);		
	}
	//This function is used for updating the priority
	 function updatePrio($id,$priority) 
		{
			$usertype=$this->session->userdata('userType');
			if($usertype=='Technician')
				$flag='technician';
			if($usertype=='Pharmacist')
				$flag='pharmacist';
			$seg=$this->uri->segment(6);	
			if(!empty($id))
			{
				$segmenst7=$this->uri->segment(7);
				if(!empty($segmenst7))
				{
					$prior=$this->rx_model->getPriority($segmenst7);
					$this->rx_model->updatePriority($segmenst7,$priority);
					$message=$this->rx_model->updatePriority($id,$prior);
				}
				elseif($segmenst7=='0' && $segmenst7!='')
				{
					redirect($flag.'/viewmore/index/'.$seg);
				}
				else
				{
					$highest = $this->rx_model->getHighestPriority();
					$priority+=$highest;
					$message=$this->rx_model->updatePriority($id,$priority);
				}
				$this->data['rxId']=$id;
				$this->data['status']='High';
				$this->data['userId']=$this->session->userdata('userId');
				$this->data['storeId']=$this->session->userdata('storeId');		
				if(!empty($message))
				{
					$this->rx_model->addStatusViewmore($this->data);
					$message ="High priority set successfully";
				}
				else 
				$message = "Priority could not be set";
				$this->session->set_flashdata('message', $message);		    
				redirect($flag.'/viewmore/index/'.$seg);
			}
			else		
				$this->load->view('viewmores');	
		}
		//To call this function for setting counsel to complete
		function completecounsel($rxId, $segment)
		{
			$data['status']='Completed';
			$data['rxId']=$rxId;
			$result=$this->rx_model->changeStatus($data);
			if(!empty($result)){
				$message = "Status completed Successfully";
			}
			else {
				$message = "Status couldn't be completed";
			}			
			$this->session->set_flashdata('message', $message);
			redirect('technician/viewmore/index/'.$segment);			
		}
		function addVideoSession()
		{
			$this->rx_model->addVideoSession();
		}
		function closeVideoConferencing($rxId)
		{
			$this->rx_model->closeliveConference($rxId);
		}
		function getNotification(){
			
			$this->rx_model->getnotification('10');
		}
		function moreNotification()
		{
			$this->load->library('pagination');
			$storeId=$this->session->userdata('storeId');		
			$this->data['countApproval'] = $this->rx_model->countStatusCount('Approval',$storeId);
			$this->data['countRejected'] = $this->rx_model->countStatusCount('Rejected',$storeId);
			$this->data['countCounsel'] = $this->rx_model->countAllPrescriptions('Counsel',$storeId);
			$this->data['countCompleted'] = $this->rx_model->countAllPrescriptions('Completed',$storeId);
			$this->data['countCancelled'] = $this->rx_model->countAllPrescriptions('Cancelled',$storeId);
			$this->data['countDraft'] = $this->rx_model->countAllPrescriptions('Draft',$storeId);		
			$this->data['countOnHold'] = $this->rx_model->countAllPrescriptions('OnHold',$storeId);	
			$this->data['countNotification'] = $this->rx_model->countUnreadComments();	
			$config=array();
			$base_url= base_url().'technician/viewmore/moreNotification';	
			$config['base_url'] = $base_url;
			$config['per_page'] =10;
			$config['uri_segment'] = '4';   
			$count=$this->rx_model->countMoreNotification();
			$config['total_rows']=$count;
			$this->data['total']=$count;
			$page=($this->uri->segment(4))?$this->uri->segment(4):0;
			$this->data['notification']=$this->rx_model->getMoreNotification($config['per_page'], $page);
			$this->pagination->initialize($config);
			$this->load->view('viewnotification',$this->data);	
		}
		function addComment()
		{
			$this->rx_model->insertComment($_POST['commentid']);
		}
		function clearNotification()
		{
			$this->rx_model->clearComment();
		}
}
