<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Companies extends CI_Model{		
		function __construct()
		{		
			parent::__construct();
		}
	//This function is used for getting all stores
	public function getAllCompany($perpage , $startCount , $status) 
	{
		$this->db->select('*');
		if ($perpage!='')
			$this->db->limit($perpage,$startCount);
		$this->db->order_by("companyName", "asc"); 
		$query =$this->db->get('companies'); 		
		$row = $query->num_rows();
		if($row > 0) 
		{
			$result= $query->result();
			return $result;		
		} 
		else
		{
			return false;
		}
	}
	//This function is used for counting all store.
	function countAllCompany()
	{
		$this->db->select('id'); 		
		$query =$this->db->get('companies'); 
		return $query->num_rows();
	}
	
	//This function is used for adding new store.
	function addCompany($data) 
	{		
		$storeObject=array('companyName'=>$data['name'],'description'=>$data['description'],'address'=>$data['address'],'city'=>$data['city'],'state'=>$data['state'],'zipcode'=>$data['zipcode'],'phone'=>$data['phone']);		
		$query=$this->db->insert('companies' , $storeObject); 		
		$companyId=$this->db->insert_id();			
		if(!empty($query))
			{
				$storeObj=array('storeName'=>$data['storename'],'accessCode'=>$data['accessCode'],'description'=>$data['content'],'address'=>$data['addressstore'],'city'=>$data['citystore'],'state'=>$data['statestore'],'zip'=>$data['zipstore'],'phoneNumber'=>$data['phonestore'],'companyId'=>$companyId);		
				$this->db->insert('stores' , $storeObj);
				return $query;
			}
		else
		{
			return false;
		}
	}
	//This function is calling for getting one particular store by passing its ID
	function getCompanyData($id) 
	{
		$this->db->select('*');
		$query = $this->db->get_where('companies', array('id'=>$id)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$getStoreData = $query->row();		
			return $getStoreData;
		} 
		else 
		return false;
	}
	//To call this function for updating the store	
	public function updateCompany($data)
	{	
		$newData=array('companyName'=>$data['name'],'description'=>$data['description'],'address'=>$data['address'],'city'=>$data['city'],'state'=>$data['state'],'zipcode'=>$data['zipcode'],'phone'=>$data['phone']);		
		$this->db->where('id',$data['id']);
		$result=$this->db->update('companies',$newData);		
		if($result)
		{
			$storeObj=array('storeName'=>$data['storename'],'accessCode'=>$data['accessCode'],'description'=>$data['content'],'address'=>$data['addressstore'],'city'=>$data['citystore'],'state'=>$data['statestore'],'zip'=>$data['zipstore'],'phoneNumber'=>$data['phonestore'],'companyId'=>$data['id']);		
			$this->db->where('companyId',$data['id']);
			return $this->db->update('stores',$storeObj);
		}
		else
		{
			return false;
		}
	}
	//To call this function for deleting store
	public function deleteCompany($id='')
	{
		if(!empty($id))
		{
			$query = $this->db->select("storeId")->where("companyId", $id)->get("stores");
			if($query->num_rows() > 0)
				return FALSE;
			else
			{
				$this->db->where('id',$id);
				return $this->db->delete('companies');
			}				
		}		
		else 	
			return false;
	}	
}
	