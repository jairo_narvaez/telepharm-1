<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Members extends CI_Model{		
		function __construct(){		
		parent::__construct();
		}
	//This function is used for getting all user
	public function getAllUser($perpage , $startCount , $usertype,$status) 
	{	
		$this->db->select('users.id,users.firstName,users.lastName,users.phoneNumber,users.userType,users.status,companies.companyName,users.email');		 
		$this->db->from('users');
		$this->db->join('companies', 'users.companyId = companies.id');	
		if ($perpage!='')
		$this->db->limit($perpage,$startCount);		
		$this->db->where('users.userType !=','Administrator');		
		if(!empty($usertype) && $usertype!='')
			$this->db->where('users.userType',$usertype);		
		if($status!=''){ $this->db->where('users.status',$status);}		
			$this->db->order_by("users.firstName", "asc");
		
		$query = $this->db->get();		
	
			$row = $query->num_rows();
		if($row > 0) 
		{
			$allUser = array();
			$i=0; 
			foreach ($query->result() as $result) 
			{
				$allUser[$i]->id =$result->id; 			
				$allUser[$i]->firstname =$result->firstName; 				
				$allUser[$i]->lastname=$result->lastName;
				$allUser[$i]->phonenumber=$result->phoneNumber;
				$allUser[$i]->usertype=$result->userType;
				$allUser[$i]->status=$result->status;
				$allUser[$i]->companyname=$result->companyName;			
				$allUser[$i]->email=$result->email;			
				$i++;
			}	
		return $allUser;		
		} 
		else{
			return false;
			}
	}
	//This function is used for counting all user.
	function countAllUser($usertype,$status)
	{
		$this->db->select('users.id'); 
		if(!empty($usertype))
					$this->db->where('users.userType',$usertype);
		if($status!=""){ $this->db->where('users.status',$status);}		
		$this->db->where('users.userType !=','Administrator');	
		$this->db->from('users');	
			$this->db->join('companies', 'users.companyId = companies.id');	
		$query = $this->db->get();			
		return $query->num_rows();	
	}
	//This function is used for adding new user.
	function addUser($data) 
	{
		$userObject=array('firstName'=>$data['firstname'],'lastName'=>$data['lastname'],'email'=>$data['email'],'password'=>$data['password'],'storeId'=>$data['storeId'][0],'notes'=>$data['notes'],'phoneNumber'=>$data['phonenumber'],'companyId'=>$data['company'],'userType'=>$data['usertype'],'primaryContact'=>$data['primarycontact'],'companyAdmin'=>$data['companyadmin'],'useremail'=>$data['useremail']);		
		$query= $this->db->insert('users' , $userObject);
		$userId=$this->db->insert_id();		
			if(!empty($query))
				{
					if(!empty($data['storeId']))
					{
						for($i=0;$i<count($data['storeId']);$i++)
						{
									$storeObject=array('userId'=>$userId,'storeId'=>$data['storeId'][$i]);		
									$this->db->insert('store_access_users' , $storeObject); 
						}
						
					}
				/*$username =$data['email'] ;
				$password =$data['pwdoriginal'] ;
					
				$to = $data['email'];			
				$subject = "Regisration successful on Telepharm Technologies";				
				$message = "
				<html>
				<body>
				<p>Thank you for registraring on Telepharm Technologies. Use following details for further communication.</p>
				<table>	
				<tr>
					<td>User Name</td><td>".$username."</td>
				</tr>
				<tr>
					<td>Password</td><td>".$password."</td>
				</tr>
				</table>
				
				<p><a href='".base_url()."'>Click on the link to login on Telepharm Technologies</a></p>
				<p>Thank you </p>
				<p>Support Team</p>
				</body>
				</html>
				";
				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";		
				$headers .= 'From: <admin@telepharmtechnologies.com>' . "\r\n";			
				@mail($to,$subject,$message,$headers);*/
				return $query;
				}
			else
			{
				return false;
			}
		
	}
	//This function is calling for getting one particular user by passing its ID
	function getUserData($id) 
	{
		$this->db->select('*');
		$query = $this->db->get_where('users', array('id'=>$id)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$getUserData = $query->row();
			return $getUserData;
		} 
		else 
		return false;
	}
	
	//This function is calling for getting one particular store by passing its ID
	function getStoreData($id) 
	{
		$this->db->select('*');
		$query = $this->db->get_where('store_access_users', array('userId'=>$id)); 		
		$row = $query->num_rows();		
		if(!empty($row) && $row > 0) 
		{
			$getStoreData = $query->result();
			return $getStoreData;
		} 
		else 
		return false;
	}
	
	
	
	
	//To call this function for updating the user	
	public function updateUser($data)
	{
		if(!isset($data['primarycontact'])) $data['primarycontact'] = "0";	
		if(!isset($data['companyadmin'])) $data['companyadmin'] = "0";		
		$newData=array('userType'=>$data['usertype'],
									'companyId'=>$data['company'],
									'firstName'=>$data['firstname'],
									'lastName'=>$data['lastname'],
									'email'=>$data['email'],
									'phoneNumber'=>$data['phonenumber'],
									'storeId'=>$data['storeId'][0],
									'notes'=>$data['notes'],
									'primaryContact'=>$data['primarycontact'],
									'companyAdmin'=>$data['companyadmin'],
									'useremail'=>$data['useremail']
									);
		$this->db->where('id',$data['userId']);
		 $query=$this->db->update('users',$newData);		
		
				if(!empty($query))
				{
						$this->db->where('userId',$data['userId']);
						$this->db->delete('store_access_users');
							
					
					if(!empty($data['storeId']))
					{
						for($i=0;$i<count($data['storeId']);$i++)
						{
						
									$storeObject=array('userId'=>$data['userId'],'storeId'=>$data['storeId'][$i]);		
									$this->db->insert('store_access_users' , $storeObject); 
						}
						
					}
				}
		
			return $query;
			
	}
	//To call this function for updating the user Password
	public function updatePassword($data)
	{		
		$newData=array('password'=>$data['newpassword']);
		$this->db->where('id',$data['userId']);
		$query= $this->db->update('users',$newData);	
		/*
			if(!empty($query))
			{
				$username =$data['email'] ;
				$password =$data['modifiedpwd'] ;
					
				$to = $data['email'];			
				$subject = "Regisration successful on ConnectRx";				
				$message = "
				<html>
				<body>
				<p>Your Password has been reset by ConnectRx Support Team.</p>
				<table>	
				<tr>
					<td>User Name</td><td>".$username."</td>
				</tr>
				<tr>
					<td> New Password</td><td>".$password."</td>
				</tr>
				</table>
				
				<p><a href='".base_url()."'>Click on the link to login on ConnectRx</a></p>
				<p>Thank you </p>
				<p>Support Team</p>
				</body>
				</html>
				";
 				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";		
				$headers .= 'From: <admin@crx.com>' . "\r\n";			
				//mail($to,$subject,$message,$headers);
				return $query;
				}
			else
			{
				return false;
			}*/
	}
	//To call this function for deleting user
	public function deleteUser($id='')
	{	
		if(!empty($id))
		{
			$this->db->select('*');
			$query = $this->db->get_where('activities', array('userId'=>$id)); 
			$row = $query->num_rows();
			if(!empty($row) && $row > 0)		
				return false;		
			else
			{
				
				$this->db->where('userId',$id);
				$result=$this->db->delete('store_access_users');	
				if($result)
				{
				$this->db->where('id',$id);
				return $this->db->delete('users');
				}
			}			
			
		}		
		else 		
		return false;
	}	
}
	