<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Prescriptions extends CI_Model{		
		function __construct(){		
		parent::__construct();
		}
	//This function is used for getting all prescription
	public function getAllPrescription($storeId,$perpage , $startCount,$status) 
	{	
		$this->db->select('prescription.rxId,prescription.parentId,prescription.firstName,prescription.lastName,prescription.rxNumber,prescription.priority,prescription.status,stores.storeName');
		$this->db->from('prescription');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');	
		if ($perpage!='')
					$this->db->limit($perpage,$startCount);
		$this->db->where('prescription.parentId','0');
		$this->db->order_by("prescription.updateDate", "desc");
		if(!empty($storeId) && $storeId!='all')
		$this->db->where('prescription.storeId' ,$storeId);	
		if($status!='')
		$this->db->where('prescription.status',$status);	
		$query = $this->db->get();		
		$row = $query->num_rows();	
		if($row > 0) 
		{
			$allPrescription = array();
			$i=0;
			foreach ($query->result() as $result) 
			{
				if(!empty($result->rxId))
				{
					$count=self::countDuplicate($result->rxId);
				}				
				$allPrescription[$i]->id =$result->rxId; 			
				$allPrescription[$i]->firstname =$result->firstName; 				
				$allPrescription[$i]->lastname=$result->lastName;
				$allPrescription[$i]->rxNumber=$result->rxNumber;
				$allPrescription[$i]->priority=$result->priority;
				$allPrescription[$i]->status=$result->status;
				$allPrescription[$i]->storeName=$result->storeName;
				$allPrescription[$i]->countDuplicate=$count;
				$i++;
			}	
		return $allPrescription;		
		} 
		else
			return false;			
	}

	//This function is used for counting all prescription.
	function countAllPrescription($storeId,$status)
	{ 
		$this->db->select('prescription.rxId'); 
		$this->db->from('prescription');		
		if(!empty($storeId) && $storeId!='all')
		$this->db->where('prescription.storeId' ,$storeId);
		if(!empty($status))
		$this->db->where('prescription.status',$status);		
		$this->db->where('prescription.parentId','0');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');
		$query = $this->db->get();		
		return $query->num_rows();
	}
	//
	//This function is used for counting all prescription per status for DASHBOARD.
	function countAllPresDash($status)
	{ 
		$this->db->select('prescription.rxId'); 
		$this->db->from('prescription');
		if(!empty($status))
		$this->db->where('prescription.status' ,$status);		
		//$this->db->where('prescription.parentId','0');
		
		$query = $this->db->get();		
		return $query->num_rows();
	}
	
	//To call this function for getting the count of duplicate records
	function countDuplicate($rxId)
	{		
		$this->db->select('rxId');
		$this->db->where('parentId',$rxId);		
		$this->db->from('prescription');
		$query = $this->db->get();		
		return $query->num_rows();
	}
	
	
	
	//this function is  use for getting all duplicate prescription of that Id
	public function getAllDupPrescription($perpage , $startCount , $status,$id) 
	{ 
		$this->db->select('prescription.rxId,prescription.parentId,prescription.firstName,prescription.lastName,prescription.rxNumber,prescription.priority,prescription.status,stores.storeName');
		
		if ($perpage!='')
					$this->db->limit($perpage,$startCount);
					$this->db->where('prescription.parentId',$id);
					$this->db->order_by("prescription.updateDate", "desc");					
		$this->db->from('prescription');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');	
		$this->db->or_where('prescription.rxId',$id);	
		$query = $this->db->get();		
		$row = $query->num_rows();	
		if($row > 0) 
		{
			$dupPrescription=array();
				$i=0;
			foreach ($query->result() as $result) 
			{		
				$dupPrescription[$i]->id =$result->rxId; 			
				$dupPrescription[$i]->firstname =$result->firstName; 				
				$dupPrescription[$i]->lastname=$result->lastName;
				$dupPrescription[$i]->rxNumber=$result->rxNumber;
				$dupPrescription[$i]->priority=$result->priority;
				$dupPrescription[$i]->status=$result->status;
				$dupPrescription[$i]->storeName=$result->storeName;				
				$i++;
			}			
		return $dupPrescription;		
		} 
		else
			return false;
			
	}
	//This function is used for counting all  duplicate  prescription of that id.
	function countAllDupPrescription($id)
	{ 
		$this->db->select('prescription.rxId'); 
		$this->db->where('prescription.parentId',$id);		
		$this->db->from('prescription');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');
		$this->db->or_where('prescription.rxId',$id);	
		$query = $this->db->get();		
		return $query->num_rows();
	}	
	//This function is calling for getting one particular prescription by passing its ID
	function getPrescriptionData($id) 
	{
		$this->db->select('*');
		$query = $this->db->get_where('prescription', array('rxId'=>$id)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$parentPrescription = $query->row();		
			return $parentPrescription;
		} 
		else 
		return false;
	}
	
	//This function is used for getting all prescription for Search RX Number
	public function getAllPresSearch($search,$perpage , $startCount) 
	{	
		$this->db->select('prescription.rxId,prescription.parentId,prescription.firstName,prescription.lastName,prescription.rxNumber,prescription.priority,prescription.status,stores.storeName');
		$this->db->from('prescription');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');	
		if ($perpage!='')
					$this->db->limit($perpage,$startCount);
		$this->db->where('prescription.parentId','0');
		$this->db->order_by("prescription.updateDate", "desc");
		if(!empty($search) && $search!='all')		
			$this->db->like('prescription.rxNumber', $search);		
		$query = $this->db->get();			
		$row = $query->num_rows();	
		if($row > 0) 
		{
			$allPrescription = array();
			$i=0; 
			foreach ($query->result() as $result) 
			{
				if(!empty($result->rxId))
				{
					$count=self::countDuplicate($result->rxId);
				}				
				$allPrescription[$i]->id =$result->rxId; 			
				$allPrescription[$i]->firstname =$result->firstName; 				
				$allPrescription[$i]->lastname=$result->lastName;
				$allPrescription[$i]->rxNumber=$result->rxNumber;
				$allPrescription[$i]->priority=$result->priority;
				$allPrescription[$i]->status=$result->status;
				$allPrescription[$i]->storeName=$result->storeName;
				$allPrescription[$i]->countDuplicate=$count;
				$i++;
			}	
		return $allPrescription;		
		} 
		else
			return false;			
	}
	//This function is used for counting all prescription related with search for RX number.
	function countAllPresSearch($search)
	{ 
		$this->db->select('prescription.rxId'); 
		$this->db->from('prescription');		
		if(!empty($search) && $search!='all')		
			$this->db->like('prescription.rxNumber', $search);	
		$this->db->where('prescription.parentId','0');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');
		$query = $this->db->get();		
		return $query->num_rows();
	}
	//this function is  use for getting  prescription of that Id
	public function getPrescription($id) 
	{ 
		$this->db->select('prescription.rxId,prescription.technicianId,prescription.drugDispensed,prescription.parentId,prescription.firstName,prescription.lastName,prescription.rxNumber,prescription.drugDispensed,prescription.drugImage,prescription.captureImage,prescription.priority,prescription.refill,
		prescription.status,prescription.changedStatusBy,stores.storeName');
		
					$this->db->where('prescription.rxId',$id);
					$this->db->order_by("prescription.updateDate", "desc");					
		$this->db->from('prescription');
		$this->db->join('stores', 'prescription.storeId = stores.storeId');		
		$query = $this->db->get();	
		
		$row = $query->num_rows();	
		if(!empty($row) && $row > 0) 
		{
			$getPrescriptionData = $query->row();		
			return $getPrescriptionData;
		} 
		else 
		return false;
	}
	
	
	
}
	