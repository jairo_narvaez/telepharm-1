<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rx_model extends CI_Model {
	function __construct() {
		parent::__construct();
		//date_default_timezone_set('America/Chicago');
		date_default_timezone_set('Asia/Calcutta');
	}
	function getCurrentTime(){
		$query = $this->db->query('SELECT NOW() as currenttime');
		$result = $query->row();
		return $result->currenttime;
	}
function getServiceURL($storeId)	{
		$this->db->select('storeUrl');
		$query = $this->db->get_where('stores', array('storeId'=>$storeId)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$getStoreData = $query->row();		
			return $getStoreData;
		} 
		else 
		return false;
}
	function getDrugPresciber($rxid){
			$sql="SELECT * FROM drugs
			INNER JOIN prescibers ON drugs.RxId=prescibers.RxId 
			INNER JOIN dispense ON drugs.RxId=dispense.rxId
			WHERE prescibers.RxId=".$rxid;
		$query=$this->db->query($sql);
		return $query->result();
	}
	function getTechincanAlerts($status,$storeId, $techId=0,$pharm=0)
	{
			$sql="SELECT rxId ,firstName , lastName , rxNumber , updateDate , status , priority,birthDate FROM prescription ";
			$sql.= " WHERE   status='".$status."'";
 			$sql.=" and storeId='".$storeId."' AND technicianId=".$techId;
			$sql.=" ORDER BY priority ASC , updateDate DESC ";
	}
	//To call this function for getting all prescription related to a specific Technician
	function getAllPrescriptions($status , $storeId='',$priority='',$limit)
	{
		$sql="SELECT rxId ,firstName , lastName , rxNumber , updateDate , status , priority,birthDate FROM prescription ";
		if($status=='Approval')
			$sql.= " WHERE ( status='".$status."' or status='Rejected' ) ";
		else
			$sql.= " WHERE ( status='".$status."') ";
		if(!empty($storeId)){$sql.=" and storeId='".$storeId."' ";}
		if($priority=='0'){$sql.= " AND ( priority='0') ";}
		else{$sql.= " AND ( priority!='0') ";}
		$sql.=" ORDER BY priority ASC , updateDate DESC ";
		//if($limit!='' )
		$sql.=" LIMIT ".$limit;
		$query=$this->db->query($sql);
		//echo $this->db->last_query();
		$rows=$query->num_rows();
		if($rows>0)
		{
			foreach($query->result() as $result)
			{
			  	$prescription[]=$result ;
			}
			return $prescription ;
		}
		else {
			return false ;
		}
	}
	function countStatusCount($status , $storeId='')
	{
		$sql="SELECT rxId  FROM prescription ";
 			$sql.= " WHERE ( status='".$status."') ";
		if(!empty($storeId)){$sql.=" and storeId='".$storeId."' ";}
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	//To call this function for getting all prescription related to a specific Technician
	function countAllPrescriptions($status , $storeId='')
	{
 		$sql="SELECT rxId  FROM prescription ";
		if($status=='Approval')
			$sql.= " WHERE ( status='".$status."' or status='Rejected' ) ";
		else
			$sql.= " WHERE ( status='".$status."') ";

		if($status=="Draft")
		{
			$sql.= " AND  ( technicianId=".$this->session->userdata('userId')." ) ";
				
		}
		if(!empty($storeId)){$sql.=" and storeId='".$storeId."' ";}
 		$query=$this->db->query($sql);
		return $query->num_rows();
	}	
	//To call this for adding new Rx
	public function saveRx($data)
	{ 		 	
		$newData=array( 'technicianId'=>$data['technicianId'],
						'firstName'=>$data['firstName'],
						'lastName'=>$data['lastName'],
						'rxNumber'=>$data['rxNumber'],
						'drugDispensed'=>$data['drugDispensed'],
						'ndc_number'=>$data['ndc_number'],
						'status'=>$data['status'],
						'storeId'=>$data['storeId']
						);	
		if(isset($data['refill']))
				$newData['refill']=$data['refill'];
		if(isset($data['priority']))
				$newData['priority']=$data['priority'];

		if(isset($data['parentRxId']) && !empty($data['parentRxId']))
				$newData['parentId']=$data['parentRxId'];
		if(isset($data['dob']) && !empty($data['dob']))
				$newData['birthDate']=date('Y/m/d',strtotime($data['dob']));
		$this->db->insert('prescription', $newData);
		$rxId=$this->db->insert_id();
		if(!empty($rxId))	
		{
			if(!empty($data['identifer']))
			{
				$presciberData=array(
						"firstName"=>$data['pfname'], 
						"lastName"=>$data['plname'], 
						"phone"=>$data['pphone'], 
						"Npi"=>$data['npi'], 
						"identifier"=>$data['identifer'], 
						"SIGcoded"=>$data['sigcoded'], 
						"SigExpanded"=>$data['sigexpanded'],
						"RxId"=>$rxId
				);
				$this->db->insert('prescibers', $presciberData);
			}
			
			//dispense information	
			if(!empty($data['sigexpanded']))
			{
					$druginfo=array(
							"qtyWritten"=>$data['qtywritten'], 
							"qtyDispensed"=>$data['DispensedQuantity'], 
							"sig"=>$data['sigexpanded'], 
							"daysSupply"=>$data['DaysSupply'], 
							"fillsOwed"=>$data['FillsOwed'], 
							"refillsAuth"=>$data['refillsAuth'], 
							"rxId"=>$rxId
					);
					$this->db->insert('dispense', $druginfo);
			}
			
			//drug information	
			if(!empty($data['manf']))
			{
					$druginfo=array(
							"newndc"=>$data['newndc'], 
							"oldndc"=>$data['oldndc'], 
							"manf"=>$data['manf'], 
							"description"=>$data['description'], 
							"packagesize"=>$data['packagesize']." ".$data['packagesizeunbitcode'], 
							"strength"=>$data['Strength'], 
							"RxId"=>$rxId
					);
					$this->db->insert('drugs', $druginfo);
			}

			if(!empty($data['captureImage'])){
				foreach($data['captureImage'] as $captureImagee){
					$files=array('imageName'=>$captureImagee , 'rxId'=>$rxId);
					$this->db->insert('capturedimage', $files);
				}
			}			
			if(!empty($data['drugImage'])){
				foreach($data['drugImage'] as $uploadImage){
					$files=array('imageName'=>$uploadImage , 'rxId'=>$rxId);
					$this->db->insert('drugimage', $files);
				}
			}
			$this->db->select('createdDate');
			$query=$this->db->get_where('prescription', array('rxId'=>$rxId));
			$result=$query->row();
			$updateDate['updateDate']=$result->createdDate;
			$this->db->where('rxId',$rxId);
			$this->db->update('prescription',$updateDate);
			if(!empty($data['comments'])){
				$comments=array('comments'=>$data['comments'],'rxId'=>$rxId ,'userId'=>$data['technicianId']);
				$this->db->insert('comments', $comments);
			}
			$activity=array('userId'=>$data['technicianId'],'rxId'=>$rxId ,'storeId'=>$data['storeId'],'status'=>$data['status']);			
			$this->db->insert('activities', $activity);
		}
		return $rxId ;
	}
	function getRxRecord($id){
		$this->db->select('prescription.*, stores.storeName');
		$this->db->join('stores','stores.storeId=prescription.storeId');
		$this->db->where('rxId',$id);
		$query=$this->db->get("prescription");
		return $query->row();
	}
	//To call this function for getting patient data
	public function getPatientInfo($rxId)
	{
		$query=$this->db->get_where('prescription', array('rxId'=>$rxId));
		//$onhold=$_POST['onhold'];	
		$rows=$query->num_rows();
		
		if($rows>0)
		{
			//if(!empty($onhold) && $onhold=='1')				
				//$_POST['status']='OnHold';
			$result=$query->row();
			$patientData->rxId=$result->rxId;
			$patientData->parentId=$result->parentId;
			$patientData->firstName=$result->firstName;
			$patientData->lastName=$result->lastName;
			$patientData->rxNumber=$result->rxNumber;
			$patientData->drugDispensed=$result->drugDispensed;
			$patientData->drugImage=$result->drugImage;
			$patientData->birthDate=$result->birthDate;
			$patientData->refill=$result->refill;
			$patientData->priority=$result->priority;
			$patientData->status=$result->status;
			$patientData->ndc_number=$result->ndc_number;
			return $patientData ;
		}
		else {
			return false ;
		}
	}
	//To call this function for getting child of RX number
	function getChildRx($rxId)
	{
		$this->db->select('prescription.rxNumber , prescription.rxId ');
		$query=$this->db->where('parentId',$rxId);
		$query=$this->db->or_where('rxId',$rxId);
 		$query=$this->db->get('prescription');
		$rows=$query->num_rows();
		if($rows>0)
		{
			$i=0;
			foreach($query->result() as $result)
			{
				$activities[$i]->childRxNumber=$result->rxNumber;
				$activities[$i]->childRxId=$result->rxId;
				$i++;
			}
			return $activities ;
		}
		else {
			return false ;
		}
	}
	//To call this function for getting child of RX number
	function getComments($rxId,$limit='')
	{
		$this->db->select("comments,commentDate,firstName,lastName");
		$this->db->order_by('commentDate','desc');
		$this->db->join('users','comments.userId=users.id','left');
		if($limit!='')
			$this->db->limit($limit);
		$query=$this->db->get_where('comments', array('comments.rxId'=>$rxId));
		$rows=$query->num_rows();
		if($rows>0)
		{
			$i=0;
			foreach($query->result() as $result)
			{
				$comments[$i]->comments=$result->comments;
				$comments[$i]->date=date('m/d/Y h:ia',strtotime($result->commentDate));
				$comments[$i]->fullname=$result->lastName." ".$result->firstName;
				$i++;
			}
			return $comments ;
		}
		else {
			return false ;
		}
	}
	function getUserName($userId)
	{
		$this->db->select('users.firstName , users.lastName , users.userType');
		$query=$this->db->get_where('users', array('users.id'=>$userId));
		$rows=$query->num_rows();
		if($rows>0)
		{
			$result=$query->row();
			$user->name=$result->firstName." ".$result->lastName;
			$user->userType=$result->userType;
			return $user ;
		}
		else {
			return false ;
		}
	}
	//To call this function for getting all activities by passing storeId
	function getAllActivity($storeId,$rxId='',$limit='')
	{
		$activities=array();
		$this->db->select('activities.activityId , activities.status , activities.activityDate as addDate, users.firstName , prescription.rxNumber');
		$this->db->join('users','users.id=activities.userId','left');
		$this->db->join('prescription','prescription.rxId=activities.rxId','left');
		$this->db->where('activities.storeId',$storeId);
		if (!empty($rxId))$this->db->where('activities.rxId',$rxId);
		$this->db->order_by('activities.activityDate','desc');
		if($limit!='')
		$this->db->limit($limit);
		$query=$this->db->get('activities');
		$rows=$query->num_rows();
		if($rows>0)
		{
			$i=0;
			foreach($query->result() as $result)
			{
				
			 	$date=date('m/d/Y h:ia',strtotime($result->addDate));
				if(!empty($result->status) && $result->status=='Approval')
					$activities[$i]->string="<b><i>".$result->firstName."</b></i> awaiting for ".$result->rxNumber." approval on ".$date ;
				elseif(!empty($result->status) && $result->status=='Draft')
					$activities[$i]->string="<b><i>".$result->firstName."</b></i> saved ".$result->rxNumber." on ".$date;
				elseif(!empty($result->status) && $result->status=='Edit')
					$activities[$i]->string=$date." ".$result->rxNumber." edited by ".$result->firstName ;
				elseif(!empty($result->status) && $result->status=='Archive')
					$activities[$i]->string="<b><i>".$result->firstName."</b></i> deleted ".$result->rxNumber." on ".$date ;
				elseif(!empty($result->status) && $result->status=='Rejected')
					$activities[$i]->string=$date." ".$result->rxNumber." has been rejected by ".$result->firstName ;	
				elseif(!empty($result->status) && $result->status=='Completed')
					$activities[$i]->string="<b><i>".$result->firstName."</b></i> marked completed ".$result->rxNumber." record on ".$date ;
				elseif(!empty($result->status) && $result->status=='Cancelled')
					$activities[$i]->string=$date." ".$result->rxNumber." has been cancelled by ".$result->firstName ;
				elseif(!empty($result->status) && $result->status=='Counsel')
					$activities[$i]->string=$date." ".$result->rxNumber." has been accepted by ".$result->firstName ;
				elseif(!empty($result->status) && $result->status=='High')
					$activities[$i]->string="<b><i>".$result->firstName."</b></i> set high priority for ".$result->rxNumber." on ".$date ;
				$i++;
			}
			return $activities ;
		}
		else {
			return false ;
		}
	}
	//To call this function for adding new comment
	function addComment($data)
	{
		$comments=array('comments'=>$data['comments'],'rxId'=>$data['rxId'] ,'userId'=>$data['userId']);
		$this->db->insert('comments', $comments);
//		$this->session->set_userdata($currenttime);
		//$this->session->userdata('currenttime');
		return TRUE;
	}
	//To call this function for setting High
	function setHigh($RxId)
	{
		$highest =self::getHighestPriority();
		$updateDate=date('Y-m-d H:i:s');
		$setHigh=array('priority'=>$highest+1,'updateDate'=>$updateDate);
		$this->db->where('rxId', $RxId);
		$result=$this->db->update('prescription', $setHigh);
		if(!empty($result))
		{
			$storeId = $this->session->userdata('storeId');
			$userId = $this->session->userdata('userId');
			$activity=array('userId'=>$userId ,'rxId'=>$RxId , 'status'=>'High' , 'storeId'=>$storeId);
			return $this->db->insert('activities', $activity);
		}
	}
	//To call this for updating Rx
	public function updateRx($data)
	{
		
		$updateDate=date('Y-m-d H:i:s');
		$updateData=array('firstName'=>$data['firstName'],
						'lastName'=>$data['lastName'],
						'drugDispensed'=>$data['drugDispensed'],
						'ndc_number'=>$data['ndc_number'],
						'updateDate'=>$updateDate,						
						);					
		if(isset($data['status']) && !empty($data['status']))
				$updateData['status']=$data['status'];				
		if(isset($data['refill']))
				$updateData['refill']=$data['refill'];
		else 
				$updateData['refill']='0';
		
		if(isset($data['priority']))
				$updateData['priority']=$data['priority'];
		else 
				$updateData['priority']='0';		
		
		if(isset($data['dob']) && !empty($data['dob']))
				$updateData['birthDate']=$data['dob'];
		if(isset($data['status']) && !empty($data['status']) && $data['status']=='Draft')
				$updateData['changedStatusBy']='0';	
		
		$this->db->where('rxId', $data['rxId']);
		$result=$this->db->update('prescription', $updateData);
		if(!empty($data['captureImage'])){
			foreach($data['captureImage'] as $captureImagee){
				$files=array('imageName'=>$captureImagee , 'rxId'=>$data['rxId']);
				$this->db->insert('capturedimage', $files);
			}
		}
		if(!empty($data['drugImage'])){
			foreach($data['drugImage'] as $uploadImage){
				$files=array('imageName'=>$uploadImage , 'rxId'=>$data['rxId']);
				$this->db->insert('drugimage', $files);
			}
		}
		if($data['comments'])	
		{
			$comments=array('comments'=>$data['comments'],'rxId'=>$data['rxId'] ,'userId'=>$data['technicianId']);
			$this->db->insert('comments', $comments);
		}
		if(isset($data['status']) && !empty($data['status']) && $data['status']=='Draft')
		$statusAct='Draft';
		else
		$statusAct='Edit';
		$activity=array('userId'=>$data['technicianId'],'rxId'=>$data['rxId'] ,'status'=>$statusAct , 'storeId'=>$data['storeId']);
		$this->db->insert('activities', $activity);
		return $result ;
	}

	//This function is used for getting all prescription
	public function getMorePrescription($storeId,$perpage , $startCount,$status,$priority='') 
	{
		$this->db->select('prescription.changedStatusBy,users.firstName as fname,users.lastName as lname,prescription.rxId ,prescription.firstName, prescription.lastName , prescription.rxNumber , prescription.updateDate , prescription.status , prescription.priority,prescription.storeId,birthDate');	
		$this->db->from('prescription');
		$this->db->join('users', 'prescription.changedStatusBy=users.id','left');	
		if($perpage!='')
			$this->db->limit($perpage,$startCount);
			if($status=='Approval')
			{
				$sql="(prescription.status='".$status."' OR prescription.status='Rejected')";
				$this->db->where($sql);
			}		
			else
			$this->db->where('prescription.status',$status);
		if(!empty($storeId)) $this->db->where('prescription.storeId',$storeId);
		if($priority=='0'){
			$sql1="(prescription.priority='0')";
			$this->db->where($sql1);
		}
		else{
			$sql1="(prescription.priority!='0')";
			$this->db->where($sql1);
		}
		$this->db->order_by('prescription.priority ASC , prescription.updateDate DESC');		
		$query=$this->db->get();
		//echo $this->db->last_query()."<br/>";	
		$row = $query->num_rows();	
		if($row > 0) 
		{
			$allPrescription = array();
			$i=0; 
			foreach ($query->result() as $result) 
			{
				$allPrescription[$i]->id =$result->rxId; 			
				$allPrescription[$i]->firstname =$result->firstName; 				
				$allPrescription[$i]->lastname=$result->lastName;
				$allPrescription[$i]->fname =$result->fname; 				
				$allPrescription[$i]->lname=$result->lname;
				$allPrescription[$i]->rxNumber=$result->rxNumber;
				$allPrescription[$i]->priority=$result->priority;
				$allPrescription[$i]->status=$result->status;
				$allPrescription[$i]->storeId=$result->storeId;
				$allPrescription[$i]->birthDate=$result->birthDate;
				$allPrescription[$i]->updateDate=$result->updateDate;
				if(!empty($result->rxId)){
					$allPrescription[$i]->videoConf=self::getVideoRecord($result->rxId);
				}
				$i++;
			}	
		return $allPrescription;		
		} 
		else
			return false;			
	}
	//This function is used for counting all prescription.
	function countMorePrescription($storeId,$status)
	{
		$this->db->select('prescription.rxId');
		$this->db->from('prescription');
		$this->db->join('users', 'prescription.changedStatusBy=users.id','left');		
		if($status=='Approval')
			{
				$this->db->where('prescription.status',$status);
				$this->db->or_where('prescription.status','Rejected');
			}		
			else
			$this->db->where('prescription.status',$status);	
		if(!empty($storeId)) $this->db->where('prescription.storeId',$storeId);	
		$query=$this->db->get();	
		return $query->num_rows();
	}
	//to call this function for update status for viewmore 
	public function updateStatus($id,$status)
	{
		$userId= $this->session->userdata('userId');
		$updateDate=date('Y-m-d H:i:s');
		$newData=array('status'=>'Archive','priority'=>'0','updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
		$this->db->where('rxId',$id);
		return $this->db->update('prescription',$newData);	
	}	
	//to call this function for adding the status and Rx 
	function  addStatusViewmore($data)
	{    
		$activity=array('status'=>$data['status'],'rxId'=>$data['rxId'] ,'userId'=>$data['userId'],'storeId'=>$data['storeId']);
		return $this->db->insert('activities', $activity);
	}
	//to call this function for update status for viewmore 
	public function updatePriority($id,$priority)
	{
		$updateDate=date('Y-m-d H:i:s');
		$newData=array('priority'=>$priority,'updateDate'=>$updateDate);
		$this->db->where('rxId',$id);
		return $this->db->update('prescription',$newData);		
	}	
	function setrequiredconusel($rxid,$reqState){
		//end of counseling
		$data=array("requiredCounsel"=>$reqState);
		$this->db->where('rxId',$rxid);
		$result=$this->db->update('prescription',$data);
		echo "success";
	}
	//To call this function for setting status as complete counsel
	function changeStatus($data)
	{
		$updateDate=date('Y-m-d H:i:s');
		if(isset($data['refuse']) && !empty($data['refuse']))
			$newdata['refusedCounsel']=$data['refuse'];
		/*
		if(isset($data['drugImage']) && !empty($data['drugImage']))
			$newdata['drugImage']=$data['drugImage'];
		*/
		if(isset($data['refill']) && !empty($data['refill']))
			$newdata['refill']=$data['refill'];			
		
		if(isset($data['priority']) && !empty($data['priority']))
			$newdata['priority']=$data['priority'];			
			
		$newdata['status']=$data['status'];		
		if(isset($data['status']) && $data['status']=='Draft')
			$newdata['changedStatusBy']='0';
		else
			$newdata['changedStatusBy'] = $this->session->userdata('userId');
		$newdata['updateDate'] = $updateDate;
		if(isset($data['priority']))
			$newdata['priority']=$data['priority'];
		else 
			$newdata['priority']='0';	
		if(isset($data['firstName']) && !empty($data['firstName']))
			$newdata['firstName']=$data['firstName'];
		if(isset($data['lastName']) && !empty($data['lastName']))
			$newdata['lastName']=$data['lastName'];
		if(isset($data['drugDispensed']) && !empty($data['drugDispensed']))
			$newdata['drugDispensed']=$data['drugDispensed'];
		//end of counseling
		$this->db->where('rxId',$data['rxId']);
		$result=$this->db->update('prescription',$newdata);
		if(!empty($result)){
			if(!empty($data['captureImage'])){
				foreach($data['captureImage'] as $captureImagee){
					$files=array('imageName'=>$captureImagee , 'rxId'=>$data['rxId']);
					$this->db->insert('capturedimage', $files);
				}
			}
			if(!empty($data['drugImage'])){
				foreach($data['drugImage'] as $uploadImage){
					$files=array('imageName'=>$uploadImage , 'rxId'=>$data['rxId']);
					$this->db->insert('drugimage', $files);
				}
			}
			$storeId = $this->session->userdata('storeId');
			$userId = $this->session->userdata('userId');
			if(!empty($data['comments']))
			{
				$comments=array('comments'=>$data['comments'],'rxId'=>$data['rxId'] ,'userId'=>$userId);
				$this->db->insert('comments', $comments);
			}
			$activity=array('status'=>$data['status'],'rxId'=>$data['rxId'] ,'userId'=>$userId,'storeId'=>$storeId);
			$this->db->insert('activities', $activity);
		}
		return $result;
	}
	//To call this functionn for getting the record of video conferencing of a particular prescription
	function getVideoRecord($rxId){
		$query=$this->db->query("SELECT id FROM videoconf WHERE rxId=".$rxId." AND status='active'");
		return $query->num_rows();
	}
	//This function is used  for getting the searched data
	function getSearchedData( $data,$perpage,$startCount)
	{
		$usertype= $this->session->userdata('userType');
		$this->db->select('prescription.*, stores.storeName');
		$this->db->join('stores','prescription.storeId=stores.storeId','left');
		$this->db->like('prescription.rxNumber',$data['search'],'none');
		if($usertype=='Pharmacist'){
			$sql="(prescription.status!='Draft')";
			$this->db->where($sql);
		}
		else {
			$this->db->where('prescription.storeId',$this->session->userdata('storeId'));
		}
		if ($perpage!='')
			$this->db->limit($perpage,$startCount);
		$this->db->order_by('prescription.updateDate DESC');
		$query=$this->db->get_where('prescription');
		//echo $this->db->last_query();
		$rows=$query->num_rows();
		if($rows>0)
		{
			$i=0; 
			foreach($query->result() as $result)
			{
			  	//$prescription[]=$result ;
	  			$allPrescription[$i]->rxId =$result->rxId; 	
				$allPrescription[$i]->storeId =$result->storeId; 			
				$allPrescription[$i]->firstName =$result->firstName; 				
				$allPrescription[$i]->lastName=$result->lastName;
				$allPrescription[$i]->rxNumber=$result->rxNumber;
				$allPrescription[$i]->priority=$result->priority;
				$allPrescription[$i]->status=$result->status;
				$allPrescription[$i]->birthDate=$result->birthDate;
				$allPrescription[$i]->updateDate=$result->updateDate;
				$allPrescription[$i]->storeName=$result->storeName;
				if(!empty($result->rxId)){
					$allPrescription[$i]->videoConf=self::getVideoRecord($result->rxId);
				}
			  	$i++; 
			}
			return $allPrescription ;
		}
		else {
			return false ;
		}
	}
	//This function is used  for counting the searched data
	function countSearchedData($data)
	{
		$usertype= $this->session->userdata('userType');
		$this->db->select('prescription.*, stores.storeName');
		$this->db->join('stores','prescription.storeId=stores.storeId','left');
		$this->db->like('prescription.rxNumber',$data['search'],'none');
		if($usertype=='Pharmacist'){
			$sql="(prescription.status!='Draft')";
			$this->db->where($sql);
		}
		else {
			$this->db->where('prescription.storeId',$this->session->userdata('storeId'));
		}
		$query=$this->db->get_where('prescription');
		return $query->num_rows();
		
	}
	//This function is used  for getting advanced searched data
	function getAdvancedSearchData($data, $perpage , $startCount)
	{
		$usertype= $this->session->userdata('userType');
		$this->db->select('prescription.*,stores.storeName');
		$this->db->from('prescription');
		$this->db->join('stores','prescription.storeId=stores.storeId','left');
			if(isset($data['status']) && !empty($data['status']))
			{
				if($data['status']=='All')
					$statusSql="( status in ('Approval','Counsel','Completed','Cancelled','Draft','Archive') )";
				else
					$statusSql="( status='".$data['status']."' )";
				$this->db->where($statusSql);
			}
			if(isset($data['patientName']) && !empty($data['patientName']))
			{
				$patientSql="( firstName like '".$data['patientName']."%' or lastName like '".$data['patientName']."%' ) ";
				$this->db->where($patientSql);
			}
			if(isset($data['rxNumber']) && !empty($data['rxNumber']))
			{
				$rxSql="( rxNumber like '".$data['rxNumber']."' ) ";
				$this->db->where($rxSql);
			}
			if(isset($data['matchDate']) && !empty($data['matchDate']))
			{
				$dateSql="( DATE(updateDate)>= '".$data['matchDate']."' ) ";
				$this->db->where($dateSql);
			}
			if(isset($data['storeName']) && !empty($data['storeName']))
			{
				$storeSql="( prescription.storeId= '".$data['storeName']."' ) ";
				$this->db->where($storeSql);
			}
			if($usertype=='Pharmacist'){
				$sql="(prescription.status!='Draft')";
				$this->db->where($sql);
			}
			else {
				$this->db->where('prescription.storeId',$this->session->userdata('storeId'));
			}
		if ($perpage!='')
			$this->db->limit($perpage,$startCount);
		$this->db->order_by('updateDate DESC');
		$query=$this->db->get();
		$rows=$query->num_rows();
		if($rows>0)
		{
			$i=0;
			foreach($query->result() as $result)
			{
			  	//$prescription[]=$result ;
			  	$allPrescription[$i]->rxId =$result->rxId; 	
				$allPrescription[$i]->storeId =$result->storeId; 			
				$allPrescription[$i]->firstName =$result->firstName; 				
				$allPrescription[$i]->lastName=$result->lastName;
				$allPrescription[$i]->rxNumber=$result->rxNumber;
				$allPrescription[$i]->priority=$result->priority;
				$allPrescription[$i]->status=$result->status;
				$allPrescription[$i]->birthDate=$result->birthDate;
				$allPrescription[$i]->updateDate=$result->updateDate;
				$allPrescription[$i]->storeName=$result->storeName;
				if(!empty($result->rxId)){
					$allPrescription[$i]->videoConf=self::getVideoRecord($result->rxId);
				}
			  	$i++; 
			}
			return $allPrescription ;
		}
		else {
			return false ;
		}
	}
	//This function is used  for counting advanced searched data
	function countAdvancedSearchData($data)
	{
		$usertype= $this->session->userdata('userType');
		$this->db->select('prescription.*,stores.storeName');
		$this->db->from('prescription');
		$this->db->join('stores','prescription.storeId=stores.storeId','left');
			if(isset($data['status']) && !empty($data['status']))
			{
				if($data['status']=='All')
					$statusSql="( status in ('Approval','Counsel','Completed','Cancelled','Draft','Archive') )";
				else
					$statusSql="( status='".$data['status']."' )";
				$this->db->where($statusSql);
			}
			if(isset($data['patientName']) && !empty($data['patientName']))
			{
				$patientSql="( firstName like '".$data['patientName']."%' or lastName like '".$data['patientName']."%' ) ";
				$this->db->where($patientSql);
			}
			if(isset($data['rxNumber']) && !empty($data['rxNumber']))
			{
				$rxSql="( rxNumber like '".$data['rxNumber']."' ) ";
				$this->db->where($rxSql);
			}
			if(isset($data['matchDate']) && !empty($data['matchDate']))
			{
				$dateSql="( DATE(updateDate)>= '".$data['matchDate']."' ) ";
				$this->db->where($dateSql);
			}
			if(isset($data['storeName']) && !empty($data['storeName']))
			{
				$storeSql="( prescription.storeId= '".$data['storeName']."' ) ";
				$this->db->where($storeSql);
			}
			if($usertype=='Pharmacist'){
				$sql="(prescription.status!='Draft')";
				$this->db->where($sql);
			}
			else {
				$this->db->where('prescription.storeId',$this->session->userdata('storeId'));
			}
		$this->db->order_by('updateDate DESC');
		$query=$this->db->get();
		return $query->num_rows();
	}
	//To call this function for getting highest priority
	function getHighestPriority()
	{
		$query=$this->db->query("SELECT MAX(priority) as highest FROM prescription");
		$rows=$query->num_rows();
		if($rows>0)
		{
			$result=$query->row();
			return $result->highest ;
		}
		else {
			return false ;
		}
	}
	//To call this function for getting priority by passing it's Id
	function getPriority($rxId)
	{
		$query=$this->db->query("SELECT priority FROM prescription where rxId=".$rxId."");
		$rows=$query->num_rows();
		if($rows>0)
		{
			$result=$query->row();
			return $result->priority ;
		}
		else {
			return false ;
		}
	}
	//To call this function for getting the completed RX .Purpose to set Archive after 8 hours on completing the RX.
	function getCompletedRx()
	{
		$this->db->select('rxId,updateDate');
		$query=$this->db->get_where('prescription',array('status'=>'Completed'));
		$rows=$query->num_rows();
		if($rows>0)
		{
			foreach($query->result() as $result)
			{
			  	$completedRx[]=$result ;
			}
			return $completedRx ;
		}
		else {
			return false ;
		}
	}
	//To call this function for setting archive status after 8 hours on completing the Rx
	function setArchivedRx($rxId)
	{
		$date=date('Y-m-d H:i:s');
		$sql="UPDATE prescription SET status='Archive',updateDate='".$date."', changedStatusBy='0', priority='0' WHERE rxId='".$rxId."'";
		$this->db->query($sql);
	}
	//To call this function for getting captured image
	function getCapturedImage($rxId){
		$query=$this->db->get_where('capturedimage',array('rxId'=>$rxId));
		$rows=$query->num_rows();
		if($rows>0)
		{
			foreach($query->result() as $result)
			{
			  	$image[]=$result ;
			}
			return $image ;
		}
		else {
			return false ;
		}
	}
	//To call this function for getting captured image
	function getDrugImage($rxId){
		$query=$this->db->get_where('drugimage',array('rxId'=>$rxId));
		$rows=$query->num_rows();
		if($rows>0)
		{
			foreach($query->result() as $result)
			{
			  	$image[]=$result ;
			}
			return $image ;
		}
		else {
			return false ;
		}
	}
		//Returns array of MediSpan drug images, or false if none available
	function getNDCDrugImage($ndc){
		// standardize format of NDC number
		$ndc = trim($ndc);
		$ndc = preg_replace('~[^0-9]~', '', $ndc);
		
		 $query = "SELECT img.imgfilename
						FROM medispan_udrug udrug, medispan_udij udij, medispan_img img
						WHERE udrug.udrugid = udij.udrugid
						AND udij.imageid = img.imageid
						AND udij.stopdate =  '0000-00-00'
						AND udrug.extdrugid = ".$this->db->escape($ndc);
						
		$img = $this->db->query($query);
		
		if ($img->num_rows() > 0)
		{
			foreach ($img->result() as $row)
			{
   				$image[] = "uploads/medispan/".trim($row->imgfilename).".JPG";

			}
			return $image;
		}else{
			return false;
		}
	}
 
	function addVideoSession(){
		$this->load->library('OpenTokSDK');
		$apiObj= new OpenTokSDK($this->config->item('API_KEY'), $this->config->item('API_SECRET'));
		$session = $apiObj->createSession($_SERVER["REMOTE_ADDR"]);
		$tokenId=	$apiObj->generateToken($session);
		$newData=array( 'rxId'=>$_POST['rxId'],
						'storeId'=>$_POST['storeId'],
						'status'=>"active",
						'sessionId '=>"$session",
						'tokenid '=>"$tokenId",
						'apiKey '=>$this->config->item('API_KEY')
						);
		$this->db->insert('videoconf', $newData);
		$confId = $this->db->insert_id();
		$this->db->select('users.id,users.firstName,users.lastName')
						->join('store_access_users','store_access_users.userId = users.id')
						->where('users.userType', 'Pharmacist')->where('users.active', '1')
						->where('store_access_users.storeId', $_POST['storeId'])
						->order_by("users.loggedon", "desc");
		$query = $this->db->get('users');
		echo $this->db->last_query();
		$pharmaArray = array();
		$i = 0;
			if($query->num_rows() > 0){
			foreach ($query->result() as $pharmas) {
				if($i == 0)
					$pharmaArray[$i]['startTime'] = time();
				else
					$pharmaArray[$i]['startTime'] = NULL;
				$pharmaArray[$i]['pharmaId'] = $pharmas->id;
				$pharmaArray[$i]['pharmaFirstName'] = $pharmas->firstName;
				$pharmaArray[$i]['pharmaLastName'] = $pharmas->lastName;
				$pharmaArray[$i]['videoconfId'] = $confId;
				$i++;
			}
			$this->db->insert_batch('pharmaqueue', $pharmaArray); 
		}
	}
	function getvideoConfiguration($storeId,$rxId ){
		$call = $this->session->userdata('videoCall');
		if($call != "0")
		{
			$this->db->select("videoconf.*");
			$this->db->join('pharmaqueue', 'pharmaqueue.videoconfId = videoconf.id');
			$this->db->where('pharmaqueue.pharmaId', $this->session->userdata('userId')); 
			$this->db->where('pharmaqueue.startTime !=','NULL'); 
			$this->db->where('videoconf.status','active'); 
			if($rxId != "patient")
				$this->db->where('videoconf.rxId',$rxId); 
			else
				 $this->db->where('videoconf.fromPatient',"1");
			$this->db->where('videoconf.storeId',$storeId);
			$query=$this->db->get('videoconf');
			//echo $this->db->last_query();
			if($query->num_rows() > 0)
					return $query->result();
			else 	return FALSE;
		}
		else return FALSE;
	}
	function checkliveConference()
	{
		$call = $this->session->userdata('videoCall');
		if($call != "0")
		{
			$this->db->select("videoconf.rxId,videoconf.id,stores.storeName");
			$this->db->join('pharmaqueue', 'pharmaqueue.videoconfId = videoconf.id');
			$this->db->join('stores', 'videoconf.storeId = stores.storeId');
			$this->db->where('pharmaqueue.pharmaId', $this->session->userdata('userId')); 
			$this->db->where('pharmaqueue.startTime !=','NULL'); 
			$this->db->where('videoconf.status','active'); 
			$query=$this->db->get('videoconf');
			return $query->result();
		}
		else return FALSE;
	}
	function changePharmaqueue($confId){
		$data = array('startTime' => NULL);
		$this->db->where('pharmaId', $this->session->userdata('userId'));
		$this->db->where('videoconfId', $confId);
		$this->db->where('connected', '0');
		$this->db->update('pharmaqueue', $data); 
		$this->db->select("id");
		$this->db->where('videoconfId',$confId); 
		$this->db->limit(1);
		$query=$this->db->get('pharmaqueue');
		if($query->num_rows() > 0){
			$row = $query->row(); 
			$data = array('startTime' => time());
			$this->db->where('id', $row->id);
			$this->db->update('pharmaqueue', $data); 
		}
	}
	function deletepharmaqueue($confId,$action=""){
		if(empty($action)){
			$data = array('connected' => '1');
			$this->db->where('pharmaId', $this->session->userdata('userId'));
			$this->db->where('videoconfId', $confId);
			$this->db->update('pharmaqueue', $data); 
			$this->db->where('pharmaId !=', $this->session->userdata('userId'));
		}
		$this->db->where('videoconfId', $confId);
		$this->db->delete('pharmaqueue'); 
	}
	function closeliveConference($rxId,$confId = ""){
		if(!empty($confId))
			self::deletepharmaqueue($confId,'all');
		else
		{
			$this->db->select("id");
			$this->db->where('rxId',$rxId); 
			$query=$this->db->get('videoconf');
			if ($query->num_rows() > 0)
			   foreach ($query->result_array() as $row)
					self::deletepharmaqueue($row['id'],'all');
		}
		$this->db->delete('videoconf', array('rxId' => $rxId)); 
	}
	function closePharmaConfCall($confId)
	{
		self::deletepharmaqueue($confId,'all');
		$this->db->delete('videoconf', array('id' => $confId)); 
	}
/* Note: do not put a trailing slash at the end of v2 */
	function curlWrap($url, $json, $action)
	{
		define("ZDAPIKEY", "ERnVbTVVfXIFUchoP030c8ryIFRFO7tLkCSTc5bS");
		define("ZDUSER", "info@techfivesystems.com");
		define("ZDURL", "https://myapitesting.zendesk.com/api/v2");
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
		curl_setopt($ch, CURLOPT_URL, ZDURL.$url);
		curl_setopt($ch, CURLOPT_USERPWD, ZDUSER."/token:".ZDAPIKEY);
		switch($action)
		{
			case "POST":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			break;
			case "GET":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			break;
			case "PUT":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			break;
			case "DELETE":
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
			default:
			break;
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
		curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$output = curl_exec($ch);
		curl_close($ch);
		$decoded = json_decode($output);
		return $decoded;
}
	function reportproblem()
	{
		$mailaddr = $this->input->post('txtEmail');
		if(!empty($mailaddr))
		{
			define("ZDAPIKEY", "ERnVbTVVfXIFUchoP030c8ryIFRFO7tLkCSTc5bS");
			define("ZDUSER", "info@techfivesystems.com");
			define("ZDURL", "https://myapitesting.zendesk.com/api/v2/tickets.json");
			$ticket["ticket"]= array(
				'subject' => 'Ticket by - '.$_POST['txtName'],
				'recipient' => $_POST['txtEmail'],
				'description' =>  $_POST['txtProblem'],
				'priority' => 4 
 			);
			$json=json_encode($ticket);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
			curl_setopt($ch, CURLOPT_URL, ZDURL);
			curl_setopt($ch, CURLOPT_USERPWD, ZDUSER."/token:".ZDAPIKEY);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			$output = curl_exec($ch);
			$decoded = json_decode($output);
			curl_close($ch);
		}
	}
	function countUnreadComments(){
		$this->db->select('commentId');
			$this->db->where('userId',$this->session->userdata('userId'));
			$this->db->where('readOn >=',$this->session->userdata('logintime'));
			$result = $this->db->get('comment_notification');
			$readCnt=intval($result->num_rows());
			
			$this->db->select('commentId');
			$this->db->where('userId <>',$this->session->userdata('userId'));
			$this->db->where('commentDate >=',$this->session->userdata('logintime'));
			$result = $this->db->get('comments');
			$totalCnt=intval($result->num_rows());
			
			return ($totalCnt - $readCnt);
		
	}
		//function for count all unread comments
	function getUnreadComments($limit='',$call='',$after='')
	{
		$this->db->select('commentId');
		$this->db->where('userId',$this->session->userdata('userId'));
		$result = $this->db->get('comment_notification');
		$rows=$result->num_rows();
		$comment=array();	
		if($rows>0)
		{
			foreach($result->result() as $query)
			{
				$comment[] =$query->commentId ;
			}
		}
		$activity=array();
		$this->db->select('comments.commentId,comments.commentDate,comments.rxId,comments.userId,users.firstName,prescription.rxId,users.id,prescription.rxNumber');
		$this->db->join('users','users.id=comments.userId','left');
		$this->db->join('prescription','prescription.rxId=comments.rxId','left');
		if(!empty($comment))
			$this->db->where_not_in('commentId',$comment);
		$this->db->where('userId <>',$this->session->userdata('userId'));
		if(!empty($after))
			$this->db->where('comments.commentDate >=',$this->session->userdata('logintime'));
		$this->db->order_by('comments.commentDate','desc');
		if(!empty($limit))
			$this->db->limit($limit);
		$activity=$this->db->get('comments');
		if($call)
			return $activity->result();
		else 
			return count($activity->result());
	}
		//function for get all comments except current user
	function getAllComments($perpage='',$startCount='',$call='')
	{
		$this->db->select('comments.commentId,comments.commentDate,comments.rxId,comments.userId,users.firstName,prescription.rxId,users.id,prescription.rxNumber');
		$this->db->join('users','users.id=comments.userId','left');
		$this->db->join('prescription','prescription.rxId=comments.rxId','left');
		$this->db->where('comments.userId <>',$this->session->userdata('userId'));
		//$this->db->where('comments.commentDate <=',$this->session->userdata('logintime'));
		if ($perpage!='')
			$this->db->limit($perpage,$startCount);
		$this->db->order_by('comments.commentDate','desc');
		$result = $this->db->get('comments');
		if($call)
			return $result->result();
		else
			return count($result->result());
	}

	//function for count all activities according to store
	function getMoreNotification($perpage='',$startCount='')
	{
		$getcomments=self::getAllComments($perpage,$startCount,'call');
    	$rows=count($getcomments);	
		$activities = array();
		if($rows>0)
		{
			$i=0;
			foreach($getcomments as $query)
			{
				$activities[$i]->readFlag = "0";
				$activities[$i]->rxId = $query->rxId;
				$activities[$i]->commentId = $query->commentId;
				$activities[$i]->alert = $query->firstName." comment on  Rx #".$query->rxNumber." on ".date('m/d/Y h:ia',strtotime($query->commentDate)); 
				$this->db->select('id')->where('commentId',$query->commentId)->where('userId',$this->session->userdata('userId'));
				$qry = $this->db->get('comment_notification');
				//echo $this->db->last_query();
				if($qry->num_rows() > 0)
					$activities[$i]->readFlag = "1";
				$i++;
			}
			return $activities;
		}
		
	}		
	function countMoreNotification()
	{
		$activities=self::getMoreNotification();
		return count($activities);
	}		
	function getnotification($limit='')
	{
		$getnotification=self::getUnreadComments($limit,'call','after');
    	$rows=count($getnotification);	
		$totalUnread = self::getUnreadComments('','','after');
		if($rows>0)
		{
			$i=0;
			foreach($getnotification as $query)
			{
				$link=base_url().strtolower($this->session->userdata('usertype'))."/rx/view/".$query->rxId;
				echo "<li><a  href='javascript:void(0)' onClick=\"javascript:linkComment('".$query->commentId."','".$link."','0')\">
					<b><i>".$query->firstName.
					"</b></i> comment on 
					<b> Rx #".$query->rxNumber.
					"</b> on".date('m/d/Y h:ia',strtotime($query->commentDate)) .
					"</a></li>";
				$i++;
			}
			//if($totalUnread > $rows)
				echo "<div id=clearcomment class=pull-left href='javascript:void(0)' onClick=\"javascript:clearComment()\">Clear</div>
				<div id=viewmore class=pull-right onClick=\"javascript:window.location.href='".base_url().strtolower($this->session->userdata('usertype'))."/viewmore/moreNotification'\">	View more...
				</div>";					
		}
		else 
			echo "<li>No Comments Added.</li>";
	}	
	function insertComment($commentId)
	{
			$newData=array('commentId'=>$commentId,
							'userId'=>$this->session->userdata('userId'));
			$query=$this->db->insert('comment_notification', $newData);
			$rows=$query->num_rows();
			if($rows>0)
				echo "success";
			else
				echo "fail";
	}
	function clearComment()
	{
		$getnotification=self::getUnreadComments('','call');
		$newarray=array();
		$i=0;
		foreach($getnotification as $query)
		{
			$newarray[$i]=array("commentId"=>$query->commentId, 
						"userId"=>$this->session->userdata('userId')
						);
			$i++;
		} 
		$this->db->insert_batch('comment_notification',$newarray);
	}

	function checkconfsession($confId)
	{
		$query = $this->db->select('id')->get_where(' videoconf',array('id'=>$confId));
		if($query->num_rows() < 1)
			echo "error"; 
	}
	
}





