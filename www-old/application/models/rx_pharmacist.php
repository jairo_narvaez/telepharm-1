<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rx_pharmacist extends CI_Model{
			function __construct(){
			parent::__construct();
 			}			
			//To call this function for getting all prescription for pharmacist
			function getAllPrescriptions($status,$priority='',$limit,$storeId='')
			{	
				$pharmacistId=$this->session->userdata('userId');
				
					
				$sql="SELECT rxId ,firstName , lastName , rxNumber , updateDate , status , priority ,storeId FROM prescription ";
				/*if($status=='Approval')
					$sql.= " WHERE ( status='".$status."' or status='Rejected' ) ";
				else*/
					$sql.= " WHERE ( status='".$status."') ";
				
				if($status=='Completed' || $status=='Cancelled' ||  $status=='Archive')
					$sql.=" AND (changedStatusBy='".$pharmacistId."')";
				
				if($priority=='0'){$sql.= " AND ( priority='0') ";}
				elseif($priority=='1'){$sql.= " AND ( priority!='0') ";}
				
				if(!empty($storeId) && $storeId!=''){$sql.= " AND ( storeId=".$storeId.") ";}
				
				$sql.=" ORDER BY priority ASC , updateDate DESC ";
				
				$sql.=" LIMIT ".$limit;
			
					
				$query=$this->db->query($sql);
				//echo $this->db->last_query();		
				$rows=$query->num_rows();
				if($rows>0)
				{
					foreach($query->result() as $result)
					{
							$prescription[]=$result ;
					}
					return $prescription ;
				}
				else
					return false;
			}
			//To call this function for getting all prescription for pharmacist
			function countAllPrescriptions($status,$storeId='')
			{
				$pharmacistId=$this->session->userdata('userId');
				
				$sql="SELECT rxId FROM prescription ";
				/*if($status=='Approval')
					$sql.= " WHERE ( status='".$status."' or status='Rejected' ) ";
				else*/
					$sql.= " WHERE ( status='".$status."') ";
				
 				/*if($status=='Completed' || $status=='Cancelled' ||  $status=='Archive')
					$sql.=" AND (changedStatusBy='".$pharmacistId."')";*/
					
				if(!empty($storeId) && $storeId!=''){$sql.= " AND ( storeId=".$storeId.") ";}
				
				$query=$this->db->query($sql);
				return $query->num_rows();
			}
			//To call this function for getting all activities for pharmacist
			function getAllActivity($rxId='',$limit='',$storeId='')
			{
			$activities=array();
			$this->db->select('activities.activityId , activities.status , activities.activityDate as addDate, users.firstName , prescription.rxNumber');
			$this->db->join('users','users.id=activities.userId','left');
			$this->db->join('prescription','prescription.rxId=activities.rxId','left');		
			if (!empty($rxId)){$this->db->where('activities.rxId',$rxId);}
			if(!empty($storeId) && $storeId!=''){$this->db->where('activities.storeId',$storeId);}
			$this->db->order_by('activities.activityDate','desc');
			if($limit!='')
			$this->db->limit($limit);
			$query=$this->db->get('activities');
			$rows=$query->num_rows();
					if($rows>0)
					{
						$i=0;
						foreach($query->result() as $result)
						{
								
						 	$date=date('m/d/Y h:ia',strtotime($result->addDate));
							if(!empty($result->status) && $result->status=='Approval')
								$activities[$i]->string=$result->firstName." awaiting for ".$result->rxNumber." approval on ".$date ;
							elseif(!empty($result->status) && $result->status=='Draft')
								$activities[$i]->string=$result->firstName." saved ".$result->rxNumber." on ".$date;
							elseif(!empty($result->status) && $result->status=='Edit')
								$activities[$i]->string=$date." ".$result->rxNumber." edited by ".$result->firstName ;
							elseif(!empty($result->status) && $result->status=='Archive')
								$activities[$i]->string=$result->firstName." archived ".$result->rxNumber." on ".$date ;
							elseif(!empty($result->status) && $result->status=='Rejected')
								$activities[$i]->string=$result->firstName." rejected ".$result->rxNumber." on ".$date ;	
							elseif(!empty($result->status) && $result->status=='Completed')
								$activities[$i]->string=$result->firstName." marked completed ".$result->rxNumber." record on ".$date ;
							elseif(!empty($result->status) && $result->status=='Cancelled')
								$activities[$i]->string=$result->firstName." cancelled ".$result->rxNumber." on ".$date ;
							elseif(!empty($result->status) && $result->status=='Counsel')
								$activities[$i]->string=$result->firstName." approved ".$result->rxNumber." on ".$date ;
							elseif(!empty($result->status) && $result->status=='High')
								$activities[$i]->string=$result->firstName." set high priority for ".$result->rxNumber." on ".$date ;
							
							$i++;
						}
							return $activities ;
					}
					else
						return false;
			
			}
			
			//To call this functionn for getting the record of video conferencing of a particular prescription
			function getVideoRecord($rxId){
				$query=$this->db->query("SELECT id FROM videoconf WHERE rxId=".$rxId." AND status='active'");
				return $query->num_rows();
			}
			
						
			//This function is used for getting all prescription
			public function getMorePrescription($perpage , $startCount,$status,$storeId='') 
			{
		 	$pharmacistId=$this->session->userdata('userId');
			$pharmacistType=$this->session->userdata('userType');
				
			$this->db->select('prescription.storeId , prescription.changedStatusBy , users.firstName as fname , users.lastName as lname ,
								prescription.rxId ,prescription.firstName, prescription.lastName , prescription.rxNumber , prescription.updateDate,prescription.createdDate,
								 prescription.status , prescription.priority , birthDate , stores.storeName');	
			//$this->db->from('prescription');
			$this->db->join('users', 'prescription.changedStatusBy=users.id','left');
			$this->db->join('stores', 'stores.storeId=prescription.storeId','left');
			//$this->db->join('videoconf', 'videoconf.rxId=prescription.rxId','left');		
			if($perpage!='')
			$this->db->limit($perpage,$startCount);
			$this->db->where('prescription.status',$status);
			//$this->db->where('videoconf.status','active');
			if(!empty($storeId) && $storeId!=''){
					$sqlStore="(prescription.storeId='".$storeId."')";
					$this->db->where($sqlStore);
				}	
			if($status=='Draft'){
					$sqlStore="(prescription.technicianId='".$pharmacistId."')";
					$this->db->where($sqlStore);
				}


			//$this->db->where('prescription.storeId',$storeId);
			$this->db->order_by('prescription.priority' , 'DESC');		 
			$this->db->order_by('prescription.updateDate','DESC');		
			$query=$this->db->get('prescription');
			
			//  echo $this->db->last_query()."<br/>";
							
			$row = $query->num_rows();		
				if($row > 0) 
				{
					$allPrescription = array();
					$i=0; 
					foreach ($query->result() as $result) 
					{
						$allPrescription[$i]->id =$result->rxId; 	
						$allPrescription[$i]->storeId =$result->storeId; 			
						$allPrescription[$i]->firstname =$result->firstName; 				
						$allPrescription[$i]->lastname=$result->lastName;
						$allPrescription[$i]->fname =$result->fname; 				
						$allPrescription[$i]->lname=$result->lname;
						$allPrescription[$i]->rxNumber=$result->rxNumber;
						$allPrescription[$i]->priority=$result->priority;
						$allPrescription[$i]->status=$result->status;
						$allPrescription[$i]->birthDate=$result->birthDate;
						$allPrescription[$i]->createdDate=$result->createdDate;
						$allPrescription[$i]->updateDate=$result->updateDate;
						$allPrescription[$i]->storeName=$result->storeName;
						if(!empty($result->rxId)){
							$allPrescription[$i]->videoConf=self::getVideoRecord($result->rxId);
						}
							
						$i++;
					}			
					return $allPrescription;		
				} 
				else
					return false;			
			}
			//This function is used for counting all prescription for pharmacist with status.
			function countMorePrescription($status,$storeId='')
			{
				$pharmacistId=$this->session->userdata('userId');
				$pharmacistType=$this->session->userdata('userType');
				$this->db->select('prescription.rxId');
				$this->db->from('prescription');		
				$this->db->where('prescription.status',$status);
				if(!empty($storeId) && $storeId!=''){
					$sqlStore="(prescription.storeId='".$storeId."')";
					$this->db->where($sqlStore);
				}		
				$this->db->order_by('prescription.updateDate DESC');	
				$this->db->join('users', 'prescription.changedStatusBy=users.id','left');	
				$query=$this->db->get();	
				return $query->num_rows();
			}			
			//to call this function for update status for viewmore 
			public function updatePriority($id,$priority)
			{
			$updateDate=date('Y-m-d H:i:s');
			$newData=array('priority'=>'1','updateDate'=>$updateDate);
			$this->db->where('rxId',$id);
			return $this->db->update('prescription',$newData);		
			}	
			//to call this function for update status for viewmore 
			public function updateApprove($id,$comment,$storeId)
			{ 
			$userId= $this->session->userdata('userId');		
			$updateDate=date('Y-m-d H:i:s');
			$newData=array('status'=>'Counsel','updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
			$this->db->where('rxId',$id);
			$result=$this->db->update('prescription',$newData);
				if(!empty($result))
				{
				$comments=array('comments'=>$comment,'rxId'=>$id ,'userId'=>$userId);
				$this->db->insert('comments', $comments);
				$activity=array('status'=>'Counsel','rxId'=>$id ,'userId'=>$userId,'storeId'=>$storeId);
				return $this->db->insert('activities', $activity);
				}
				else 
					return false;		
			}	
			//to call this function for change status from Approve to Reject 
			public function updateReject($id,$comment,$storeId)
			{
				
				$userId= $this->session->userdata('userId');	
				$updateDate=date('Y-m-d H:i:s');
				$newData=array('status'=>'Rejected','updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
				$this->db->where('rxId',$id);
				$result=$this->db->update('prescription',$newData);
				if(!empty($result))
				{
					$comments=array('comments'=>$comment,'rxId'=>$id ,'userId'=>$userId);
					$this->db->insert('comments', $comments);
					$activity=array('status'=>'Rejected','rxId'=>$id ,'userId'=>$userId,'storeId'=>$storeId);
					return $this->db->insert('activities', $activity);
				}
				else 
					return false;		
			}	
			//to call this function for change status from Counsel to Completed 
			public function updateCompleted($id,$comment,$storeId)
			{	
				$userId= $this->session->userdata('userId');			
				$updateDate=date('Y-m-d H:i:s');
				$newData=array('status'=>'Completed','updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
				$this->db->where('rxId',$id);
				$result=$this->db->update('prescription',$newData);
				if(!empty($result))
				{
					$comments=array('comments'=>$comment,'rxId'=>$id ,'userId'=>$userId);
					$this->db->insert('comments', $comments);
					$activity=array('status'=>'Completed','rxId'=>$id ,'userId'=>$userId,'storeId'=>$storeId);
					return $this->db->insert('activities', $activity);
				}
				else 
					return false;		
			}	
			//to call this function for change status from Counsel to Cancelled
			public function updateCancelled($id,$comment,$storeId)
			{
				$userId= $this->session->userdata('userId');			
				$updateDate=date('Y-m-d H:i:s');
				$newData=array('status'=>'Cancelled','updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
				$this->db->where('rxId',$id);
				$result=$this->db->update('prescription',$newData);
				if(!empty($result))
				{
					$comments=array('comments'=>$comment,'rxId'=>$id ,'userId'=>$userId);
					$this->db->insert('comments', $comments);
					$activity=array('status'=>'Cancelled','rxId'=>$id ,'userId'=>$userId,'storeId'=>$storeId);
					return $this->db->insert('activities', $activity);
				}
				else 
					return false;		
			}	
			//to call this function for updating the status to archive.
			public function updateArchive($id,$comment,$storeId)
			{			
			$userId= $this->session->userdata('userId');		
			$updateDate=date('Y-m-d H:i:s');
			$newData=array('status'=>'Archive', 'priority'=>'0' , 'updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
			$this->db->where('rxId',$id);
			$result=$this->db->update('prescription',$newData);
				if(!empty($result))
				{
				$comments=array('comments'=>$comment,'rxId'=>$id ,'userId'=>$userId);
				$this->db->insert('comments', $comments);
				$activity=array('status'=>'Archive','rxId'=>$id ,'userId'=>$userId,'storeId'=>$storeId);
				return $this->db->insert('activities', $activity);
				}
				else 
					return false;		
			}	
			//to call this function for updating the status cancelled  to approval
			public function updateResubmit($id,$comment,$storeId)
			{			
			$userId= $this->session->userdata('userId');		
			$updateDate=date('Y-m-d H:i:s');
			$newData=array('status'=>'Approval','updateDate'=>$updateDate , 'changedStatusBy'=>$userId);
			$this->db->where('rxId',$id);
			$result=$this->db->update('prescription',$newData);
				if(!empty($result))
				{
				$comments=array('comments'=>$comment,'rxId'=>$id ,'userId'=>$userId);
				$this->db->insert('comments', $comments);
				$activity=array('status'=>'Approval','rxId'=>$id ,'userId'=>$userId,'storeId'=>$storeId);
				return $this->db->insert('activities', $activity);
				}
				else 
					return false;		
			}

		//To call this function for getting all stores
		function getAllStores(){
			$query=$this->db->get('stores');
			//echo $this->db->last_query();		
			$rows=$query->num_rows();
			if($rows>0)
			{
				$i=0;
				foreach($query->result() as $result)
				{
					$stores[$i]->storeId=$result->storeId ;
					$stores[$i]->storeName=$result->storeName ;
					$i++;
				}
				return $stores ;
			}
			else
				return false;
		}
		
function getNewRX(){
			$current = date('Y-m-d H:i:s',$this->session->userdata('lastRxtoApprove'));
			$status = array('Approval', 'OnHold');
			$query = $this->db->select('updateDate')->where('updateDate >',$current)->where_in('status',$status)->get('prescription');
			if($query->num_rows()){
				$row = $query->row(); 
				 $this->session->set_userdata('lastRxtoApprove', strtotime($row->updateDate));
				 echo  "success";
			}
			else
				echo "error";
		}		
			
		function logoutPharma(){
			$this->db->select('videoconfId')->where('pharmaId',$this->session->userdata('userId'));
			$query = $this->db->get('pharmaqueue');
			if($query->num_rows() > 0)
				foreach ($query->result() as $conf)
				{
					$this->db->select('videoconfId')->where('videoconfId',$conf->videoconfId)->where('pharmaId !=',$this->session->userdata('userId'));
					$query1 = $this->db->get('pharmaqueue');
					if($query1->num_rows() < 1)
						$this->db->delete('videoconf', array('id' => $conf->videoconfId)); 
				}
			$this->db->delete('pharmaqueue', array('pharmaId' => $this->session->userdata('userId'))); 
		}	
			
			
		function setPrioHigh($pId,$status){
			if(!empty($status))
			{
				$this->db->select_max('priority');
				$query = $this->db->get('prescription');
				$row = $query->row(); 
				$max = intval($row->priority) + 1;
			}
			else
				$max = "0";
			$data = array(
			               'priority' => $max
			            );
			$this->db->where('rxId', $pId);
			$this->db->update('prescription', $data); 
		}
}
