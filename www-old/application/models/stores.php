<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Stores extends CI_Model{		
		function __construct(){		
		parent::__construct();
		}
	//This function is used for getting all stores
	public function getAllStore($perpage , $startCount , $status) 
	{
		$this->db->select('storeId,storeName,description,contactName,phoneNumber,companyId');
		if ($perpage!='')
			$this->db->limit($perpage,$startCount);

		$this->db->order_by("storeName", "asc"); 
		$query =$this->db->get('stores'); 		
		$row = $query->num_rows();
		if($row > 0) 
		{
			$allStore = array();
			$i=0; 
			foreach ($query->result() as $result) 
			{
				$allStore[$i]->storeId =$result->storeId; 				
				$allStore[$i]->storeName=$result->storeName;
				$allStore[$i]->description=$result->description;
				$allStore[$i]->contactName=$result->contactName;
				$allStore[$i]->phoneNumber=$result->phoneNumber;
 				$allStore[$i]->companyId=$result->companyId;
				$i++;
			}			
		
		return $allStore;		
		} 
		else{
			return false;
			}
	}
	//This function is used for counting all store.
	function countAllStore()
	{
		$this->db->select('storeId'); 		
		$query =$this->db->get('stores'); 
		return $query->num_rows();
	}
	function getCompanies()
	{
		$this->db->select('id,companyName'); 		
		$query =$this->db->get('companies'); 
		return 	$query->result();
	}

	function getStore($companyId)
	{
		$this->db->select('*');
		$query = $this->db->get_where('stores', array('companyId'=>$companyId)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$getStoreData = $query->result();		
			return $getStoreData;
		} 
		else 
		return false;
 	}
	
	function getStoreDataComp($companyId)
	{
		$this->db->select('*');
		$query = $this->db->get_where('stores', array('companyId'=>$companyId)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$getStoreData = $query->row();		
			return $getStoreData;
		} 
		else 
		return false;
		
	}
	

	//This function is used for adding new store.
	function addStore($data) 
	{
		$storeObject=array('storeName'=>$data['name'],'description'=>$data['description'],'contactName'=>$data['contactname'],'address'=>$data['address'],
		'city'=>$data['city'],'state'=>$data['state'],'zip'=>$data['zip'],'phoneNumber'=>$data['phonenumber'],'companyId'=>$data['company'],'accessCode'=>$data['accessCode'],'storeUrl'=>$data['serviceURL']);		
		return $this->db->insert('stores' , $storeObject); 
	}
	//This function is calling for getting one particular store by passing its ID
	function getStoreData($id) 
	{
		$this->db->select('*');
		$query = $this->db->get_where('stores', array('storeId'=>$id)); 
		$row = $query->num_rows();
		if(!empty($row) && $row > 0) 
		{
			$getStoreData = $query->row();		
			return $getStoreData;
		} 
		else 
		return false;
	}
	//To call this function for updating the store	
	public function updateStore($data)
	{
		$newData=array('storeName'=>$data['name'],'description'=>$data['description'],'contactName'=>$data['contactname'],'address'=>$data['address'],'city'=>$data['city'],'state'=>$data['state'],'zip'=>$data['zip'],
		'phoneNumber'=>$data['phonenumber'],'companyId'=>$data['company'],'accessCode'=>$data['accessCode'],'storeUrl'=>$data['serviceURL']);
		$this->db->where('storeId',$data['storeId']);
		return $this->db->update('stores',$newData);		
	}
	//To call this function for deleting store
	public function deleteStore($id='')
	{ 
		if(!empty($id))
		{
			$this->db->select('id');
			$query = $this->db->get_where('store_access_users', array('storeId'=>$id)); 
			$row = $query->num_rows();
			if(!empty($row) && $row > 0)		
				return false;		
			else
			{
				$this->db->where('storeId',$id);
				return $this->db->delete('stores');				
			}
		}		
		else 		
		return false;
	}	
}
	