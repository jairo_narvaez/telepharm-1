<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Model {		
		function __construct() {
		// Call the Model constructor
		parent::__construct();
	}	
	//This function is used to check user email and password for login.	
	public function checkUser($username , $password)
	{	
 	$this->db->select('firstName , storeId , lastName ,email ,id , userType , status,photo');  
	$query =$this->db->get_where('users',array('email'=>$username , 'password'=>$password));
	$rows=$query->num_rows(); 
		if($rows>0)
		{ 
			$admins=$query->row(); 		
			$newData=array('active'=>"1",'loggedon'=>time());
			$this->db->where('id',$admins->id);
			$this->db->update('users',$newData);
			return $admins;
		}
		else 
			return false;		
	}
	
	public function logoutUser()
	{
		$newData=array('active'=>"0");
		$this->db->where('id',$this->session->userdata('userId'));
		$this->db->update('users',$newData);
	}
	
	public function pharmaActiveSet()
	{
		$newData=array('active'=>"1");
		$this->db->where('id',$this->session->userdata('userId'));
		$this->db->update('users',$newData);
	}
	
	public function checkUserForAdm($username , $password)
	{
	$sql=" SELECT firstName , lastName ,email ,id , userType , status FROM  users ";
	$sql.= " WHERE ( userType='Administrator') ";	
	$sql.= " AND ( email='".$username."' AND password='".$password."' ) ";		
	$query=$this->db->query($sql);	
	$rows=$query->num_rows(); 
		if($rows>0)
		{ 
			$admins=$query->row(); 
			return $admins;
		}
		else 
			return false;		
	}
	
	
	
	//To call this function for updating the user data
	public function updateUser($data)
	{
			$newData=array('userType'=>$data['usertype'],'password'=>$data['password'],'firstName'=>$data['firstname'],
										'lastName'=>$data['lastname'],'email'=>$data['email'],'phoneNumber'=>$data['phonenumber']);
			if(!empty($data['photo']))
				$newData['photo'] = $data['photo'];
			$this->db->where('id',$data['userId']);
		return $this->db->update('users',$newData);		
	}
	//This function is used to check user email and password for user.	
	public function checkPassword($username , $password)
	{
 	$this->db->select('id');    
	$query =$this -> db -> get_where('users',array('email'=>$username , 'password'=>$password));
	$rows=$query->num_rows(); 
		if($rows>0)
		{ 
			$admins=$query->row(); 
			return $admins;
		}
		else 
			return false;		
	}
	//To call this function for updating the user	
	public function updatePassword($newpassword,$userId)
	{
		$newData=array('password'=>$newpassword);
		$this->db->where('id',$userId);
		return $this->db->update('users',$newData);		
	}
}