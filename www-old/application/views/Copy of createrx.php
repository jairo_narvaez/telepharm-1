﻿<?php
$CI=&get_instance();
$CI->load->view('common/header');
$userId = $this->session->userdata('userId');
$storeId = $this->session->userdata('storeId');
?>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.webcam.js"></script>

<!-- <link href="<?php echo base_url();?>css/webcam.css" rel="stylesheet">-->
<script type="text/javascript">
    function readURL(input)
    {
    	$("#rxImage").hide();
    	$("#imageShow").show();
        if (input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function (e)
            {
                $('#imageShow').attr('src', e.target.result);
            }
			reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script type="text/javascript">
$().ready(function(){
 //$("#draftbutton").click(function(){$("#webcam").show();});
 	
 $("#clickSbmit").hide();
 $("#imageShow").hide();
 
  $("#draftbutton").click(function(){
 	var firstname = $("#firstName").val();
 	var lastName = $("#lastName").val();
 	var rxNumber = $("#rxNumber").val();
 	var drugImage = $("#drugImage").val();
 	
 	var submit = 1;
 	if(firstname == "" && submit == 1)
 	{
 		alert("Please enter First Name!");
 		$('#firstName').focus();
 		submit = 0;
 	}
	if(lastName == "" && submit == 1)
	{
		alert("Please enter Last Name!");
		$('#lastName').focus();
 		submit = 0;
	}	
	if(rxNumber == "" && submit == 1)
	{
		alert("Please enter RX Number!");
		$('#rxNumber').focus();
 		submit = 0;
	}
	if(drugImage == "" && submit == 1)
	{
		alert("Please select drug Image!");
		//$('#lastName').focus();
 		submit = 0;
	}
	if(submit == 1)
		$('#addRx').submit();
	});	
 $("#submitbutton").click(function() {
 	validate_form(this.form);
});
});
function validate_form(thisform) {

	with (thisform)
	{
		if (validate_required(firstName,"Please enter First Name!")==false) {
			firstName.focus();
			return false;
		}
		if (validate_required(lastName,"Please enter Last Name!")==false) {
			lastName.focus();
			return false;
		}
		if (validate_required(rxNumber,"Please enter RX Number!")==false) {
			rxNumber.focus();
			return false;
		}
		if (validate_required(drugImage,"Please select drug Image!")==false) {
			//lastName.focus();
			return false;
		}
		if(!document.getElementById('clickSbmit').click())
		{
			return false;
		}
		
	}

} 
 
function validate_required(field,alerttxt) {
	with (field){
		if (value==null||value=="") {
			alert(alerttxt);
			return false;
		}
		else
		{
			return true;
		}
		
	}
}

</script>

<script type="text/javascript">


$().ready(function(){

var pos = 0;
var ctx = null;
var cam = null;
var image = null;

var filter_on = false;
var filter_id = 0;

/*
function toggleFilter(obj) {
	if (filter_on =!filter_on) {
		obj.parentNode.style.borderColor = "#c00";
	} else {
		obj.parentNode.style.borderColor = "#333";
	}
}
*/
jQuery("#webcam").webcam({

	width: 250,
	height: 220,
	mode: "save",
	swffile: "<?php echo base_url();?>js/jscam.swf",

	onTick: function(remain){

		if (0 == remain) {
			jQuery("#status").text("Cheese!");
		} else {
			jQuery("#status").text(remain + " seconds remaining...");
		}
	},
	onCapture: function () {
		var unique = new Date().getTime();
		jQuery("#status").text("Please wait while capture...");
		
		
		 webcam.save("<?php echo base_url();?>upload.php?fname="+unique+".jpg");
		 $("#capture_image").val(unique+".jpg");
		 url = "<?php echo base_url();?>uploads/"+unique+".jpg";
		  $("#webcam1").html("<span><img src='"+url+"'>  </span>");
		jQuery("#status").text("Capture completed.");
	},
	debug: function (type, string){
		jQuery("#status").html(type + ": " + string);
	} 
 
});
 



 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
	
});

function changeFilter() {
	if (filter_on) {
		filter_id = (filter_id + 1) & 7;
	}
}

</script>
<script type="text/javascript">
$().ready(function(){
 $("#webcam").hide();
 $("#captureButton").click(function(){$("#webcam").show();$("#imageShow").hide(); $("#rxImage").hide();});
 $("#drugImage").click(function(){$("#webcam").hide();$("#imageShow").show();});

})
</script>

	<div class="span10">
	<form method="post" name="addRx" id="addRx" enctype="multipart/form-data" action="<?php echo base_url();?>technician/rx/save/Draft">
		<input type="hidden" name="technicianId" value="<?php if(!empty($userId)) {echo $userId;} ?>" />
		<input type="hidden" name="storeId" value="<?php if(!empty($storeId)) {echo $storeId;} ?>" />
		<input type="hidden" name="parentRxId" value="<?php if(!empty($patientData->parentId)){echo $patientData->parentId ;}elseif(!empty($patientData->rxId)){echo $patientData->rxId ;}elseif(!empty($_POST['parentRxId'])){echo $_POST['parentRxId'];} ?>" />
		<input id="capture_image" type="hidden" value="" name="capture_image">
		 
		<div class="span2 well nameInfo">
             <div class="control-group">
            <label class="control-label" for="input01">First Name<span class="star">*</span></label>
              <div class="controls">
                <input type="text" id="firstName"  name="firstName" class="{validate:{required:true , maxlength:250}} createRx" title="Please enter First Name" value="<?php if(!empty($patientData->firstName)){echo $patientData->firstName;} elseif(!empty($_POST['firstName'])){echo $_POST['firstName'];} ?>" />               
              <span  class="returnError" > <?php echo form_error('firstName'); ?></span>
               </div> 
             </div>
             <div class="control-group">
            <label class="control-label" for="input01">Last Name<span class="star">*</span></label>
              <div class="controls">
                <input type="text" class="createRx" id="lastName" name="lastName" value="<?php if(!empty($patientData->lastName)){echo $patientData->lastName;}elseif(!empty($_POST['lastName'])){echo $_POST['lastName'];} ?>" />               
               
               </div> 
             </div>
             
              
            <div class="control-group">
            <label class="control-label" for="input01">Date of Birth:</label>
              <div class="controls">
                <input type="text"  name="dob"  class="createRx" id="datepicker"  value="<?php if(!empty($patientData->birthDate)){echo $patientData->birthDate;}elseif(!empty($_POST['dob'])){echo $_POST['dob'];} ?>" />               
               </div> 
             </div>
             
             <div class="control-group">
            <label class="control-label" for="input01">Rx Number<span class="star">*</span></label>
              <div class="controls">
                <input type="text" id = "rxNumber" name="rxNumber"  class="{validate:{required:true }} createRx" title="Please enter RX Number" value="<?php if(!empty($_POST['rxNumber'])){echo $_POST['rxNumber'];} ?>" />               
               <span  class="returnError" ><?php echo form_error('rxNumber'); ?></span>
               </div> 
             </div>
             <div class="control-group">
            <label class="control-label" for="input01">Drug Dispensed</label>
              <div class="controls">
                <input type="text" class="createRx"  name="drugDispensed"  value="<?php if(!empty($_POST['drugDispensed'])){echo $_POST['drugDispensed'];} ?>" />               
               </div> 
             </div>
            
             
             <div class="controls">
                <input type="checkbox"  value="1" name="refill" <?php if(!empty($_POST['refill']) && $_POST['refill']=='1'){echo 'checked="checked"';}?> />
                Refill
                <br/>
                <input type="checkbox"  value="<?php if(!empty($highest)){echo ($highest+1);}?>" name="priority" <?php if(!empty($_POST['priority']) && $_POST['priority']=='1'){echo 'checked="checked"';}?> />
                High Priority
            </div>
            
 	</div>
	
	<div class="span3 middleSection">
		
		<div class="control-group">
			<div class="controls captureButton">
				<a class="btn btn-info pull-left" id="captureButton" href="javascript:webcam.capture();changeFilter();void(0);">Capture</a>
            	<span class="myfileupload-buttonbar ">
		            <label class="myui-button">
		                <span>Load from file</a><input type="file" name="drugImage" id="drugImage" class="" onchange="readURL(this);"  />
		            </label>
			     </span> 
             </div>
	        
        </div>
        
        
        <div class="control-group">
			
	         <div class="controls">
				<p id="status" style="height:22px; color:#c00;font-weight:bold;margin-top: 25px;"></p>
				<div id="webcam" ></div>
				<div id="browseImage">
					<img height="260" width="290"  src="<?php echo base_url();?>images/noImage.jpeg" alt="NoImage" id="rxImage"/>
	        		<img id="imageShow" src="#" alt="your image" height="260" width="290" /> 
				</div>
	         </div>
        </div>
		
         
	</div>
	
	<div class="span2 showCapture">
		<div class="control-group">
			<div class="controls">
				<div id="webcam1"><img height="116" width="130" class="thumbnail" src="<?php echo base_url();?>images/noImage.jpeg" alt="NoImage" /></div>
			</div> 
		</div>
		<div class="control-group">
			<div class="controls">
				<div id="webcam1"><img height="116" width="130" class="thumbnail" src="<?php echo base_url();?>images/noImage.jpeg" alt="NoImage"  /></div>
			</div> 
		</div>  
		<div class="control-group">
			<div class="controls">
				<div id="webcam1"><img height="116" width="130" class="thumbnail" src="<?php echo base_url();?>images/noImage.jpeg" alt="NoImage" /></div>
			</div> 
		</div> 
	</div>
	
	<div class="span3 commentBottom">
		<!--<div class="titleImage">Dispensed Drug</div>
		<p id="status" style="height:22px; color:#c00;font-weight:bold;"></p>
		<a href="javascript:webcam.capture();changeFilter();void(0);">Take a picture instantly</a>
		<div id="webcam"></div>-->
		

                
 		 <div class="control-group">
              <div class="controls ">
                <textarea rows="5" class="commentText" name="comments" placeholder="Add Comments..." ><?php if(!empty($_POST['comments'])){echo $_POST['comments'];} ?></textarea>           
              </div> 
          </div>
            
		 <div class="control-group">
             <div class="controls">
             	<input class="btn btn-primary" type="button" id="submitbutton" value="Submit">
             	<input type="button" class="btn btn-primary" id="draftbutton" value="Draft" />
	            <a class="btn" href="<?php echo base_url();?>technician/rx/index">Cancel</a>
	            <a id="clickSbmit" data-toggle="modal" href="#myModal" ></a> 
	          </div> 
           </div>
	</div>
<!--</div>-->

</form>
</div>

<!--------------Modal section-------------------->
<div class="modal hide" id="myModal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">×</button>
  </div>
  <div class="modal-body">
    <p>This record has been submitted to pharmacist for approval, you would like to proceed?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Cancel</a>
    <a href="#" onclick="document.addRx.action='<?=base_url()?>technician/rx/save/add';document.addRx.submit();" class="btn btn-primary">Add more</a>
    <a href="#" onclick="document.addRx.action='<?=base_url()?>technician/rx/save/';document.addRx.submit();" class="btn btn-primary">Submit</a>
  </div>
</div>
<!--------------Modal section-------------------->

<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
	</script>
<?php
$CI->load->view('common/footer');
?>