<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Telepharm</title>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?php echo base_url();?>adminassets/css/bootstrap.min.css" rel="stylesheet">
     <link href="<?php echo base_url();?>adminassets/css/crxadmin.css" rel="stylesheet">
    <link href="<?php echo base_url();?>adminassets/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="<?php echo base_url();?>adminassets/css/font-awesome.css" rel="stylesheet">    
    <link href="<?php echo base_url();?>adminassets/css/base-admin.css" rel="stylesheet">
    <link href="<?php echo base_url();?>adminassets/css/base-admin-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
      </head>
<body>
<div class="navbar navbar-fixed-top">	
	<div class="navbar-inner">		
		<div class="container">			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>			
			<a class="brand" href="./">
				Telepharm				
			</a>
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<!-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-cog"></i>
							Settings
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="javascript:;">Account Settings</a></li>
							<li><a href="javascript:;">Privacy Settings</a></li>
							<li class="divider"></li>
							<li><a href="javascript:;">Help</a></li>
						</ul>						
					</li>		 -->	
					<li class="dropdown">						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i> 
						<?php echo $this->session->userdata('userType');?>
							<b class="caret"></b>
						</a>					
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url();?>admin/admin/profile">My Profile</a></li>
						 
							<li class="divider"></li>
							<li><a href="<?php echo base_url();?>admin/admin/logout">Logout</a></li>
						</ul>						
					</li>
				</ul>			
				<form class="navbar-search pull-right" method="post" action="<?php echo base_url();?>admin/admin/search">
					<input type="text" class="search-query"  name="search" 
					value="<?php if(!empty($_POST['search'])){ echo $_POST['search'];}else if(!empty($search))
					{ echo $search;}?>"  placeholder="Search"/>
				</form>				
			</div><!--/.nav-collapse -->
		</div> <!-- /container -->
	</div> <!-- /navbar-inner -->
</div> <!-- /navbar -->
<div class="subnavbar">
	<div class="subnavbar-inner">
	    <div class="container">
			<ul class="mainnav">			<?php $tab=$this->uri->segment(3); $tab2=$this->uri->segment(2)?>
				<li <?php if(!empty($tab) && $tab=='home'){ echo "class='active'" ;}?>>
					<a href="<?php echo base_url();?>admin/admin/home">
						<i class="icon-home"></i>
						<span>Home</span>
					</a>	    				
				</li>	

				<li class="dropdown <?php if(!empty($tab2) && $tab2=='company'){ echo "active" ;}?>"  >					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-pushpin"></i>
						<span>Companies</span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url();?>admin/company/index">Company List</a></li>
						<li><a href="<?php echo base_url();?>admin/company/add">Add Company</a></li>
					</ul>    				
				</li>	


				<li class="dropdown <?php if(!empty($tab2) && $tab2=='store'){ echo "active" ;}?>"  >					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-pushpin"></i>
						<span>Store</span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url();?>admin/store/index">Store List</a></li>
						<li><a href="<?php echo base_url();?>admin/store/add">Add Store</a></li>
					</ul>    				
				</li>	
				<li class="dropdown <?php if(!empty($tab2) && $tab2=='member'){ echo "active" ;}?>">					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-th-large"></i>
						<span>User</span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url();?>admin/member/index">User List</a></li>
						<li><a href="<?php echo base_url();?>admin/member/add">Add User</a></li>
						
					</ul>    				
				</li>		
				<li class="dropdown <?php if(!empty($tab2) && $tab2=='prescription'){ echo "active" ;}?>" >					
					<a href="" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-share-alt"></i>
						<span>Manage Rx</span>
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo base_url();?>admin/prescription/index/all">Rx List</a></li>											
					</ul>    				
				</li>		
				<li <?php if(!empty($tab) && $tab=='permissions'){ echo "class='active'" ;}?>>
					<a href="<?php echo base_url();?>admin/admin/permissions">
						<i class="icon-home"></i>
						<span>Permissions</span>
					</a>	    				
				</li>		
			</ul>
		</div> <!-- /container -->	
	</div> <!-- /subnavbar-inner -->
</div> <!-- /subnavbar -->    
<div class="main">	
	<div class="main-inner">
	    <div class="container">
			<div class="row">
