<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>
	<div class="span10">	
		<script type="text/javascript">
		$().ready(function() {
			$(".close").click(function() {
		 $('#removeMessage').remove();
			});
		});
		</script>
		<?php 
			if($this->session->flashdata('message'))
			{?>
				<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
			<?php 
			}		
			if($this->session->flashdata('message1'))
			{?>
				<div class="alert alert-success redError"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message1') ; ?></div>
			<?php 
			}
		 ?>
		<div class="alert"> <a class="close" data-dismiss="alert">×</a>Found <?php if(!empty($total)) echo $total ; else echo "0"; ?> records</div>
			<div class="widget widget-table action-table">						
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Company List</h3>
				</div> <!-- /widget-header -->					
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th> 
								<th>City</th>
								<th>State</th>
 							
								<th>Phone</th>
								<th class="td-actions">Options</th>
							</tr>
						</thead>
						<tbody>
							<?php
 						if(!empty($companies)){ 
						foreach($companies as $company)
						{?>
							<tr>
								<td><?=$company->companyName?></td>
								<td><?=$company->city?></td>
								<td><?=$company->state?></td>
						
								<td><?=$company->phone?></td>
								<td class="td-actions">									
									<a class="btn btn-small btn-warning" href="<?php echo base_url();?>admin/company/edit/<?=$company->id?>" title="Click here to edit">
										<i class="btn-icon-only icon-ok"></i>										
									</a>									
									<a class="btn btn-small" href="<?php echo base_url();?>admin/company/delete/<?=$company->id?>" onclick="return confirm('Are you sure want to delete this company');" title="Click here to delete">
										<i class="btn-icon-only icon-remove"></i>										
									</a>
								</td>
								</tr>
								<?php 
						 }
						}
					else{ 
					?>
					<tr>
						<td  colspan="5">No records were found.</td>
					</tr>
				<?php  } ?>
				</tbody>
				</table>						
				</div> <!-- /widget-content -->				
				  <div class="pagination pull-right"">
						<?php echo $this->pagination->create_links(); ?>
					</div>
			</div> <!-- /widget -->									
		  </div> <!-- /span6 -->
		  <script type="text/javascript" src="http://192.168.0.2/team/crx/v1.0/clickheat/js/clickheat.js"></script><a href="http://192.168.0.2/team/crx/v1.0/clickheat/index.php?action=view" title="ClickHeat: clicks heatmap"><img src="http://192.168.0.2/team/crx/v1.0/clickheat/images/logo.png" width="80" height="15" border="0" alt="ClickHeat : track clicks" /></a><script type="text/javascript"><!--
clickHeatSite = 'http.192.168.0.2.team.crx.v1.0.admin.company.ind';clickHeatGroup = 'admin-company-list';clickHeatServer = 'http://192.168.0.2/team/crx/v1.0/clickheat/click.php';initClickHeat(); //-->
</script>
<?php $CI->load->view('admin/common/footer.php');?>
