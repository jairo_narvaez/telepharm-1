<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');	
?> 
 	<div class="span9"> 
				<?php 
			if(!empty($errors))
			{?>
				<div  class="alert alert-error" ><a class="close" data-dismiss="alert" >×</a><?=$errors?></div>
				<?php 
				}	 
			?>
	      		<div class="widget ">	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Company</h3>
	  				</div> <!-- /widget-header -->					
					<div class="widget-content">
						<div class="tabbable">							
						<br>
							<div class="tab-content">
								<div class="tab-pane active" id="editStore">
								<form method="post" class="form-horizontal"action="<?=base_url()?>admin/company/update" name="editStore" id="editStore">
									<fieldset>										
									<input type="hidden" name="id"  value="<?php if(!empty($storeData->id)){echo $storeData->id;} else if(!empty($_POST['id'])){echo $_POST['id'];}?>" />				
										<div class="control-group">											
											<label class="control-label" for="name">Company Name <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-large" id="name" name="name"   value="<?php if(!empty($storeData->companyName)){echo $storeData->companyName;} else if(!empty($_POST['name'])){echo $_POST['name'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="description">Description</label>
											<div class="controls">
												<textarea name="description" class="input-large" ><?php if(!empty($storeData->description)){echo $storeData->description;} else if(!empty($_POST['description'])){echo $_POST['description'];}?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="description">Address</label>
											<div class="controls">
												<textarea name="address" class="input-large" ><?php if(!empty($storeData->address)){echo $storeData->address;} else if(!empty($_POST['address'])){echo $_POST['address'];}?></textarea>
											</div> 			
										</div> 


										<div class="control-group">											
											<label class="control-label" for="contactname">City</label>
											<div class="controls">
												<input type="text" class="input-large" id="city" name="city" class="input-xlarge"  value="<?php if(!empty($storeData->city)){echo $storeData->city;} else if(!empty($_POST['city'])){echo $_POST['city'];}?>"/>
											</div> 				
										</div> 

										<div class="control-group">											
											<label class="control-label" for="contactname">State</label>
											<div class="controls">
												<input type="text" class="input-large" id="state" name="state" class="input-xlarge"  value="<?php if(!empty($storeData->state)){echo $storeData->state;} else if(!empty($_POST['state'])){echo $_POST['state'];}?>"/>
											</div> 			
										</div> 

										<div class="control-group">											
											<label class="control-label" for="contactname">Zip</label>
											<div class="controls">
												<input type="text" class="input-large" id="zipcode" name="zipcode" class="input-xlarge"  value="<?php if(!empty($storeData->zipcode)){echo $storeData->zipcode;} else if(!empty($_POST['zipcode'])){echo $_POST['zipcode'];}?>"/>
											</div> 	
										</div> 

									<div class="control-group">											
											<label class="control-label" for="contactname">Phone</label>
											<div class="controls">
												<input type="text" class="input-large" id="phone" name="phone" class="input-xlarge"  value="<?php if(!empty($storeData->phone)){echo $storeData->phone;} else if(!empty($_POST['phone'])){echo $_POST['phone'];}?>"/>
											</div> 	
										</div> 

									<div class="control-group">											
											<label class="control-label" for="storename">Store Name <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-large" id="storename" name="storename" class="input-xlarge" value="<?php if(!empty($getStoreData->storeName)){echo $getStoreData->storeName;} else  if(!empty($_POST['storename'])){echo $_POST['storename'];}?>"/>
											</div>		
									</div> 
										<div class="control-group">											
											<label class="control-label" for="descriptionforstore">Description</label>
											<div class="controls">
												<textarea name="content" class="input-large" ><?php if(!empty($getStoreData->description)){echo $getStoreData->description;} else  if(!empty($_POST['content'])){echo $_POST['content'];}?></textarea>
											</div> 			
										</div> 
										
										<div class="control-group">											
											<label class="control-label" for="address">Address</label>
											<div class="controls">
												<input type="text" class="input-large" id="addressstore" name="addressstore" class="input-xlarge" value="<?php if(!empty($getStoreData->address)){echo $getStoreData->address;} else  if(!empty($_POST['addressstore'])){echo $_POST['addressstore'];}?>"/>
											</div> 		
										</div> 
										
										<div class="control-group">											
											<label class="control-label" for="citystore">City</label>
											<div class="controls">
												<input type="text" class="input-large" id="citystore" name="citystore" class="input-xlarge"  value="<?php if(!empty($getStoreData->city)){echo $getStoreData->city;} else  if(!empty($_POST['citystore'])){echo $_POST['citystore'];}?>"/>
											</div> 			
										</div> 
										
										<div class="control-group">											
											<label class="control-label" for="state">State</label>
											<div class="controls">
													<input type="text" class="input-large" id="statestore" name="statestore" class="input-xlarge"  value="<?php if(!empty($getStoreData->state)){echo $getStoreData->state;} else if(!empty($_POST['statestore'])){echo $_POST['statestore'];}?>"/>
											</div> 		
										</div> 

										<div class="control-group">											
											<label class="control-label" for="city">Zip</label>
											<div class="controls">
												<input type="text" class="input-large" id="zipstore" name="zipstore" class="input-xlarge" value="<?php if(!empty($getStoreData->zip)){echo $getStoreData->zip;} else if(!empty($_POST['zipstore'])){echo $_POST['zipstore'];}?>"/>
											</div>
										</div>
										
										<div class="control-group">											
											<label class="control-label" for="phonenumber">Phone</label>
											<div class="controls">
												<input type="text" class="input-large" id="phonestore" name="phonestore" class="input-xlarge"  value="<?php if(!empty($getStoreData->phoneNumber)){echo $getStoreData->phoneNumber;} else if(!empty($_POST['phonestore'])){echo $_POST['phonestore'];}?>"/>
											</div>			
										</div> 
										
										<div class="control-group">											
											<label class="control-label" for="accessCode">Access Code <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-large" id="accessCode" name="accessCode" value="<?php  if(!empty($getStoreData->accessCode)){echo $getStoreData->accessCode;} else if(!empty($_POST['accessCode'])){echo $_POST['accessCode'];}?>"/>
											</div> 	
										</div>
										<input type="hidden" name="originalAccessCode" value="<?php if(!empty($getStoreData->accessCode)){echo $getStoreData->accessCode;} else if(!empty($_POST['originalAccessCode'])){echo $_POST['originalAccessCode'];}?>"/>
											
											<br />
										<div class="form-actions">
											<button type="submit" class="btn btn-primary"  type="submit" id="save" value="Submit" name="submit">Save Company</button> 
											<a class="btn" href="<?php echo base_url();?>admin/company/index">Cancel</a>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div>	
						</div>
					</div> 
				</div>
		    </div> 
<?php $CI->load->view('admin/common/footer.php');?>	