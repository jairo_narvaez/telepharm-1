<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>
 <div class="widget widget-box">					
					<div class="widget-header">	
						<i class="icon-star"></i>
						<h3>Dashboard</h3>			
					</div> <!-- /widget-header -->					
					<div class="widget-content">
						<table width="50%">
							<tr>
								<td width="25%"><b>Awaiting Approval</b></td><td><?=$this->data['approvalStatus']?></td><td width="25%"><b>Draft</b></td><td><?=$this->data['draftStatus']?></td>
							</tr>
							<tr>
								<td><b>Completed</b></td><td><?=$this->data['completedStatus']?></td><td><b>Awaiting Counsel</b></td><td><?=$this->data['counselStatus']?></td>
							</tr>
							<tr>
								<td><b>Cancelled</b></td><td><?=$this->data['cancelledStatus']?></td><td><b>Rejected</b></td><td><?=$this->data['rejectedStatus']?></td>								
							</tr>
							<tr>
								<td><b>Total</b></td><td colspan="3"><b><?=$this->data['allStatus']?></b></td>
							</tr>
						</table>
					</div> <!-- /widget-content -->					
				</div>
<?php /*
<script type="text/javascript" src="http://192.168.0.2/team/crx/v1.0/clickheat/js/clickheat.js"></script>
<noscript><p><a href="http://www.dugwood.com/clickheat/index.html">ClickHeat</a></p></noscript>
<script type="text/javascript">
 clickHeatSite = 'http.192.168.0.2.team.crx.v1.0.admin.admin.home';clickHeatGroup = 'admin-home.page';clickHeatServer = 'http://192.168.0.2/team/crx/v1.0/clickheat/click.php';initClickHeat();
</script>
 */ ?>
<?php $CI->load->view('admin/common/footer.php');?>
