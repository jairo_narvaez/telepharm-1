<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Telepharm</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
<link href="<?php echo base_url();?>adminassets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>adminassets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>adminassets/css/crxadmin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>adminassets/css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="<?php echo base_url();?>adminassets/css/base-admin.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>adminassets/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="./">
				Base Admin				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="./signup.html" class="">

							Create an Account
						</a>
						
					</li>
					
					<li class="">						
						<a href="./" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>

				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		 <label><?php echo "<font color=red>".$this->session->flashdata('message')."</font>" ; ?></label>
 <form action="<?=base_url()?>admin/admin/login" method="post" >
		
			<h1>Sign In</h1>		
			
			<div class="login-fields">
				
				<p>Sign in using your registered account:</p>
				
				<div class="field">
					<label for="username">Username:</label>
					<input type="text" id="username" name="username" value="<?php if(!empty($_POST['username'])){echo $_POST['username'];}?>" placeholder="Username" class="login username-field" />
					<span class="help-inline redError"><?php echo form_error('username'); ?></span>
				</div> <!-- /field -->

				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="<?php if(!empty($_POST['password'])){echo $_POST['password'];}?>" placeholder="Password" class="login password-field"/>
					<span class="help-inline redError"><?php echo form_error('password'); ?></span>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
			
									
				<button class="button btn btn-warning btn-large">Sign In</button>
				
			</div> 
			
			 
			
		</form>
		
	</div> <!-- /content -->

	
</div> <!-- /account-container -->


<!-- Text Under Box -->
 

<script src="<?php echo base_url();?>adminassets/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo base_url();?>adminassets/js/bootstrap.js"></script>

<script src="<?php echo base_url();?>adminassets/js/signin.js"></script>

</body>
</html>
