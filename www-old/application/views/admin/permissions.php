<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>
<div class="widget widget-box">					
	<div class="widget-header">	
		<i class="icon-star"></i>
		<h3>Permissions</h3>			
	</div> <!-- /widget-header -->					
	<div class="widget-content">
		<form action="<?php echo base_url();?>/admin/admin/permissions" name="permissionForm" method="post">
		<table width="100%" class="table table-bordered">
			<thead>
				<th>&nbsp;</th>
				<th>Telepharm Admin</th>
				<th>Company Admin</th>
				<th>Pharmacist</th>
				<th>Technician</th>
			</thead>
			<tbody>
				<tr>
					<td>Admin Dashboard</td>
					<td><input type="checkbox" name="permission[teleAdmin][adminDashboard]" value="1" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][adminDashboard]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][adminDashboard]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][adminDashboard]" value="1" /></td>
				</tr>
				<tr>
					<td>Create Stores</td>
					<td><input type="checkbox" name="permission[teleAdmin][createStores]" value="1" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][createStores]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][createStores]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][createStores]" value="1" /></td>
				</tr>
				<tr>
					<td>Create Users</td>
					<td><input type="checkbox" name="permission[teleAdmin][createUsers]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][createUsers]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[pharma][createUsers]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][createUsers]" value="1" /></td>
				</tr>
				<tr>
					<td>Reset Password</td>
					<td><input type="checkbox" name="permission[teleAdmin][resetPassword]" value="1" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][resetPassword]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[pharma][resetPassword]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][resetPassword]" value="1" /></td>
				</tr>
				<tr>
					<td>View all Rx's</td>
					<td><input type="checkbox" name="permission[teleAdmin][viewAllRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][viewAllRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[pharma][viewAllRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][viewAllRx]" value="1" /></td>
				</tr>
				<tr>
					<td>Create Patient</td>
					<td><input type="checkbox" name="permission[teleAdmin][createPatient]" value="1" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][createPatient]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[pharma][createPatient]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][createPatient]" value="1" /></td>
				</tr>
				<th colspan="5">(Application)</th>
				<tr>
					<td>Create RX</td>
					<td><input type="checkbox" name="permission[teleAdmin][createRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][createRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][createRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][createRx]" value="1" /></td>
				</tr>
				<tr>
					<td>Take Screen Capture</td>
					<td><input type="checkbox" name="permission[teleAdmin][screenCapture]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][screenCapture]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][screenCapture]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][screenCapture]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Capture Image of Drug</td>
					<td><input type="checkbox" name="permission[teleAdmin][drugImageCapture]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][drugImageCapture]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][drugImageCapture]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][drugImageCapture]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Mark High Priority</td>
					<td><input type="checkbox" name="permission[teleAdmin][makeHighPriority]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][makeHighPriority]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][makeHighPriority]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][makeHighPriority]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Mark Refill</td>
					<td><input type="checkbox" name="permission[teleAdmin][makeRefill]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][makeRefill]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][makeRefill]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][makeRefill]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Cancel Rx</td>
					<td><input type="checkbox" name="permission[teleAdmin][cancelRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][cancelRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][cancelRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][cancelRx]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Save Draft</td>
					<td><input type="checkbox" name="permission[teleAdmin][saveDraft]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][saveDraft]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][saveDraft]" value="1" /></td>
					<td><input type="checkbox" name="permission[technicain][saveDraft]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Approve Rx</td>
					<td><input type="checkbox" name="permission[teleAdmin][approveRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][approveRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][approveRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][approveRx]" value="1" /></td>
				</tr>
				<tr>
					<td>Reject Rx</td>
					<td><input type="checkbox" name="permission[teleAdmin][rejectRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][rejectRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][rejectRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][rejectRx]" value="1" /></td>
				</tr>
				<tr>
					<td>Comment on Rx</td>
					<td><input type="checkbox" name="permission[teleAdmin][commentOnRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][commentOnRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][commentOnRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][commentOnRx]" value="1" checked="checked" disabled="true" /></td>
				</tr>
				<tr>
					<td>Mark Refused Counsel</td>
					<td><input type="checkbox" name="permission[teleAdmin][markrefuseConsel]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][markrefuseConsel]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][markrefuseConsel]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][markrefuseConsel]" value="1" /></td>
				</tr>
				<tr>
					<td>Archive Completed Rx</td>
					<td><input type="checkbox" name="permission[teleAdmin][archieveCompRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[companyAdmin][archieveCompRx]" value="1" /></td>
					<td><input type="checkbox" name="permission[pharma][archieveCompRx]" value="1" checked="checked" disabled="true" /></td>
					<td><input type="checkbox" name="permission[technicain][archieveCompRx]" value="1" checked="checked" disabled="true" /></td>
				</tr>
			</tbody>
		</table>
		<br />
		<div class="form-actions">
			<button type="submit" class="btn btn-primary"  type="submit" id="save" value="Submit" name="submit">Save</button> 
			<a class="btn" href="<?php echo base_url();?>admin/admin/home">Cancel</a>
		</div> <!-- /form-actions -->
		</form>
	</div> <!-- /widget-content -->					
</div>
<?php $CI->load->view('admin/common/footer.php');?>