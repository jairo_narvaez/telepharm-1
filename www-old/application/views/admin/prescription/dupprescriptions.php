<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>
	<div class="span10">	
		<script type="text/javascript">
		$().ready(function() {
			$(".close").click(function() {
		 $('#removeMessage').remove();
			});
		});
		</script>
		
		<?php 
			if($this->session->flashdata('message'))
			{?>
				<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
			<?php 
			}
		 ?>
		 <div class="widget widget-box">
					
					<div class="widget-header">	
						<h3>Rx Information</h3>			
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<table width="80%">
							<tr>
								<td><b>First Name</b></td><td><?=$this->data['parentPrescription']->firstName?></td><td><b>Last Name</b></td><td><?=$this->data['parentPrescription']->lastName?></td>
							</tr>
							<tr>
								<td><b>Rx Number</b></td><td><?=$this->data['parentPrescription']->rxNumber?></td><td><b>Store Name</b></td><td><?=$this->data['storeName']->storeName?></td>
							</tr>
							<tr>
								<td><b>Status</b></td><td><?php if($this->data['parentPrescription']->status=='Approval') { echo "Awaiting Approval";}elseif($this->data['parentPrescription']->status=='Counsel'){ echo "Awaiting Counsel";}else{echo $this->data['parentPrescription']->status; }?></td>
								<td><b>Priority</b></td>
								<td><?php if($this->data['parentPrescription']->priority=='1'){ echo 'High'; }else{ echo "Normal";}?></td>
							</tr>
						</table>
					</div> <!-- /widget-content -->
					
				</div>
		<div class="alert"> <a class="close" data-dismiss="alert">×</a>Found <?php if(!empty($total)) echo $total ; else echo "0"; ?> records</div>
			<div class="widget widget-table action-table">						
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Rx List</h3>
				</div> <!-- /widget-header -->					
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>First Name</th> 
								<th>Last Name</th>
								<th>rx Number</th>
								<th>Store Name</th>
								<th>priority</th>
								<th>status</th>		
								<th class="td-actions">Options</th>										
							</tr>
						</thead>
						<tbody>
							<?php
						if(!empty($allDupPrescription)){ 
						foreach($allDupPrescription as $prescription)
						{?>
							<tr>
								<td><?php echo $prescription->firstname?></td>
								<td><?php echo $prescription->lastname?></td>
								<td><?=$prescription->rxNumber?></td>
								<td><?=$prescription->storeName?></td>
								<td><?php if($prescription->priority=='1'){ echo 'High'; }else{ echo "Normal";}?></td>
								<td><?php if($prescription->status=='Approval') { echo "Awaiting Approval";}elseif($prescription->status=='Counsel'){ echo "Awaiting Counsel";}else{echo $prescription->status; }?></td>
								<td>
									<a class="btn btn-small" href="<?php echo base_url();?>admin/prescription/view/<?=$prescription->id?>"  title="Click here to view details">
											<i class="">View Details</i>										
									</a>
								</td>
							</tr>
								<?php 
						 }
						}
					else{ 
					?>
					<tr>
						<td  colspan="6">No records were found.</td>
					</tr>
				<?php  } ?>
				</tbody>
				</table>						
				</div> <!-- /widget-content -->				
				  <div class="pagination pull-right"">
						<?php echo $this->pagination->create_links(); ?>
					</div>
			</div> <!-- /widget -->									
		  </div> <!-- /span6 -->
<?php $CI->load->view('admin/common/footer.php');?>
