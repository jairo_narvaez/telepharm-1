<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>		
	<div class="span10">	
		<script type="text/javascript">
		$().ready(function() {
			$(".close").click(function() {
		 $('#removeMessage').remove();
			});
		});					
		</script>		
		<?php 
			if($this->session->userdata('storeId')){ $storeId = $this->session->userdata('storeId'); }
			if($this->session->userdata('status')){ $status= $this->session->userdata('status'); }
			if($this->session->flashdata('message'))
			{?>
				<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
			<?php 
			}
		 ?>
		 <form method="post" action="<?php echo base_url();?>admin/prescription/index" class="form-horizontal"  name="addUser" id="addUser">				
		  	<div class="widget widget-box">					
					<div class="widget-header">
						<i class="icon-star"></i>	
						<h3>Search</h3>			
					</div> <!-- /widget-header -->					
					<div class="widget-content">
						<table>
							<tr>
								<td>Store Name</td><td>
										<select name="storeId" id="storeId">
							            	<?php
												if(!empty($allStore))
												{?>
													<option  value="temp">Select Store Name</option>
												<?php foreach($allStore as $all)
												{		
													?>
														<option value="<?=$all->storeId?>" <?php if(!empty($storeId) && $storeId==$all->storeId){ echo "selected='selected'";}?>><?=$all->storeName?></option>
													<?php 
												}				
											}
							            	?>            	
							            </select></td>
							      <td>Status</td>
											<td>
												<select id="status"  name="status">
												<option value="">Select Status</option>
												<option value="Approval" <?php if((!empty($_POST['status']) && $_POST['status']=='Approval') || (isset($status)&& $status=='Approval' )){ echo 'selected="selected"';}?> >Awaiting Approval</option>
												<option value="Counsel" <?php if((isset($_POST['status']) && $_POST['status']=='Counsel')|| (isset($status) && $status=='Counsel' )){ echo 'selected="selected"';}?>>Awaiting Counsel</option>
												<option value="Cancelled" <?php if((!empty($_POST['status']) && $_POST['status']=='Cancelled') || (isset($status)&& $status=='Cancelled' )){ echo 'selected="selected"';}?> >Cancelled</option>
												<option value="Completed" <?php if((isset($_POST['status']) && $_POST['status']=='Completed')|| (isset($status) && $status=='Completed' )){ echo 'selected="selected"';}?>>Completed</option>
												<option value="Rejected" <?php if((!empty($_POST['status']) && $_POST['status']=='Rejected') || (isset($status)&& $status=='Rejected' )){ echo 'selected="selected"';}?> >Rejected</option>
												<option value="Draft" <?php if((isset($_POST['status']) && $_POST['status']=='Draft')|| (isset($status) && $status=='Draft' )){ echo 'selected="selected"';}?>>Draft</option>
												</select>
												</td>
												<td valign="top">
											<button type="submit" class="btn btn-primary"  id="save" value="Submit" name="submit">Search</button> 											
										</td>
							</tr>
						</table>
					</div> <!-- /widget-content -->					
				</div>				
				</form>
		 
		<div class="alert"> <a class="close" data-dismiss="alert">×</a>Found <?php if(!empty($total)) echo $total ; else echo "0"; ?> records</div>
			<div class="widget widget-table action-table">						
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Rx List</h3>
				</div> <!-- /widget-header -->					
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>First Name</th> 
								<th>Last Name</th>
								<th>rx Number</th>
								<th>Store Name</th>
								<th>Priority</th>
								<th>Status</th>	
								<th class="td-actions">Options</th>							
							</tr>
						</thead>
						<tbody>
							<?php
						if(!empty($allPrescription)){ 
						foreach($allPrescription as $prescription)
						{?>
							<tr>
								<td><?php echo $prescription->firstname;if(isset($prescription->countDuplicate) && !empty($prescription->countDuplicate))
								 {?> <a href="<?php echo base_url();?>admin/prescription/DupPres/<?=$prescription->id?>"><?php echo '('.$prescription->countDuplicate.')';?></a><?php }?></td>
								<td><?=$prescription->lastname?></td>
								<td><?=$prescription->rxNumber?></td>
								<td><?=$prescription->storeName?></td>
								<td><?php if($prescription->priority=='1'){ echo 'High'; }else{ echo "Normal";}?></td>
								<td><?php if($prescription->status=='Approval') { echo "Awaiting Approval";}elseif($prescription->status=='Counsel'){ echo "Awaiting Counsel";}else{echo $prescription->status; }?></td>
								<td>
									<a class="btn btn-small" href="<?php echo base_url();?>admin/prescription/view/<?=$prescription->id?>"  title="Click here to view details">
											<i class="">View Details</i>										
									</a>
								</td>
							</tr>
								<?php 
						 }
						}
					else{ 
					?>
					<tr>
						<td  colspan="6">No records were found.</td>
					</tr>
				<?php  } ?>
				</tbody>
				</table>						
				</div> <!-- /widget-content -->				
				  <div class="pagination pull-right"">
						<?php echo $this->pagination->create_links(); ?>
					</div>
			</div> <!-- /widget -->									
		  </div> <!-- /span6 -->
<?php $CI->load->view('admin/common/footer.php');?>
