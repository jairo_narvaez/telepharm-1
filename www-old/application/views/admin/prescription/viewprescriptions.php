<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
//echo "<pre>";print_r($this->data);echo "</pre>";
?>		
	
<div class="row">
	      		<div class="span5">	
	      		<div class="widget">
					<div class="widget-content">
					<?php
	          	if(!empty($allPrescription->drugImage))
	          	{
	          		$value=explode('.', $allPrescription->drugImage);
				//	echo "<pre>";print_r($value);echo "</pre>";
					?>
					<img height="270" width="440" src="<?php echo base_url();?>uploads/technician/thumbnail/<?php echo $value[0].'_thumb.'.$value[1]; ?>" alt="NoImage"  /> 
					<?php
	          	}				
				else
				{
					?>
					<img height="270" width="440" src="<?php echo base_url();?>images/noImage.jpeg" alt="NoImage"  /> 
					<?php
				}
	          	?>           	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
				</div>	
	      		<div class="span7">	      		
	      		<div class="widget">							
					<div class="widget-header">
						<i class="icon-list-alt"></i>
						<h3>Rx Information</h3>
					</div> <!-- /widget-header -->					
					<div class="widget-content">
					<table width="100%">
							<tr>
								<td><b>First Name</b></td><td><?=$allPrescription->firstName?></td><td><b>Last Name</b></td><td><?$allPrescription->lastName?></td>
							</tr>
							<tr>
								<td><b>Rx Number</b></td><td><?=$allPrescription->rxNumber?></td><td><b>Store Name</b></td><td><?=$allPrescription->storeName?></td>
							</tr>
							<tr>
								<td><b>Status</b></td><td><?php if($allPrescription->status=='Approval') { echo "Awaiting Approval";}elseif($allPrescription->status=='Counsel'){ echo "Awaiting Counsel";}else{echo $allPrescription->status; }?></td>
								<td><b>Priority</b></td>
								<td><?php if($allPrescription->priority=='1'){ echo 'High'; }else{ echo "Normal";}?></td>
							</tr>
							<tr>
								<td><b>Drug Dispensed</b></td><td colspan="3"><?=$allPrescription->drugDispensed?></td>
							</tr>
						</table>						
					</div> <!-- /widget-content -->				
					</div> <!-- /widget -->									
		      	</div>
		      	<div class="span3">	
	      		<div class="widget">
					<div class="widget-content">
					<?php
	          	if(!empty($allPrescription->captureImage))
	          	{
	          		$value=explode('.', $allPrescription->captureImage);
				//	echo "<pre>";print_r($value);echo "</pre>";
					?>
					<img height="200" width="240" src="<?php echo base_url();?>uploads/technician/thumbnail/<?php echo $value[0].'_thumb.'.$value[1]; ?>" alt="NoImage"  /> 
					<?php
	          	}				
				else
				{
					?>
					<img height="200" width="240" src="<?php echo base_url();?>images/noImage.jpeg" alt="NoImage"  /> 
					<?php
				}
	          	?>           	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
				</div>	
</div>	
	      
		    			
<?php $CI->load->view('admin/common/footer.php');?>
