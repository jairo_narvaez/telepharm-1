<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?> 
 		<div class="span9">
				<?php  
			if(!empty($errors)) 
			{?>
				<div  class="alert alert-error" ><a class="close" data-dismiss="alert" >×</a><?=$errors?></div>
				<?php 
				}	 
			?>
	      		<div class="widget ">	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Profile</h3>
	  				</div> <!-- /widget-header -->					
					<div class="widget-content">
						<div class="tabbable">							
						<br>
							<div class="tab-content">
								<div class="tab-pane active" id="editUser">
								<form method="post" class="form-horizontal" action="<?=base_url()?>admin/admin/update" name="editUser" id="editUser">
									<fieldset>										
									<input type="hidden" name="userId"  value="<?php if(!empty($userData->id)){echo $userData->id;} else if(!empty($_POST['userId'])){echo $_POST['userId'];}?>" />				
									<input type="hidden" name="storeId"  value="<?php if(!empty($userData->storeId)){echo $userData->storeId;} else if(!empty($_POST['storeId'])){echo $_POST['storeId'];}?>" />
									<input type="hidden" name="usertype"  value="<?php if(!empty($userData->userType)){echo $userData->userType;} else if(!empty($_POST['usertype'])){echo $_POST['usertype'];}?>" />										
										<div class="control-group">											
											<label class="control-label" for="firstname">First Name <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-large" id="firstname" name="firstname"   value="<?php if(!empty($userData->firstName)){echo $userData->firstName;} else if(!empty($_POST['firstname'])){echo $_POST['firstname'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="lastname">Last Name</label>
											<div class="controls">
												<input type="text" class="input-large" id="lastname" name="lastname"  value="<?php if(!empty($userData->lastName)){echo $userData->lastName;} else if(!empty($_POST['lastname'])){echo $_POST['lastname'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="email">Username </label>
											<div class="controls">
												<input type="text" class="input-large" id="email" name="email" readonly="readonly" value="<?php if(!empty($userData->email)){echo $userData->email;} else if(!empty($_POST['email'])){echo $_POST['email'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->		
										<div class="control-group">											
											<label class="control-label" for="password">Password <font style="color: red">*</font></label>
											<div class="controls">
												<input type="password" class="input-large" id="password" name="password"   value="<?php if(!empty($_POST['password'])){echo $_POST['password'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->								
										<div class="control-group">											
											<label class="control-label" for="phonenumber">Phone Number </label>
											<div class="controls">
												<input type="text" class="input-large" id="phonenumber" name="phonenumber"   value="<?php if(!empty($userData->phoneNumber)){echo $userData->phoneNumber;} else if(!empty($_POST['phonenumber'])){echo $_POST['phonenumber'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
											<br />
										<div class="form-actions">
											<button type="submit" class="btn btn-primary"  type="submit" id="save" value="Submit" name="submit">Save User</button> 
											<a class="btn" href="<?php echo base_url();?>admin/admin/home">Cancel</a>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div>	
						</div>
					</div> <!-- /widget-content -->
				</div> <!-- /widget -->
		    </div> <!-- /span8 -->	
<?php $CI->load->view('admin/common/footer.php');?>	