<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>		
	<div class="span10">	
		<script type="text/javascript">
		$().ready(function() {
			$(".close").click(function() {
		 $('#removeMessage').remove();
			});
		});
		</script>		
		<?php 
			if($this->session->flashdata('message'))
			{?>
				<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
			<?php 
			}
		 ?>
		
		<div class="alert"> <a class="close" data-dismiss="alert">×</a>Found <?php if(!empty($total)) echo $total ; else echo "0"; ?> records</div>
			<div class="widget widget-table action-table">						
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Rx List</h3>
				</div> <!-- /widget-header -->					
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>First Name</th> 
								<th>Last Name</th>
								<th>rx Number</th>
								<th>Store Name</th>
								<th>priority</th>
								<th>status</th>								
							</tr>
						</thead>
						<tbody>
							<?php
						if(!empty($allPrescription)){ 
						foreach($allPrescription as $prescription)
						{?>
							<tr>
								<td><?php echo $prescription->firstname;if(isset($prescription->countDuplicate) && !empty($prescription->countDuplicate))
								 {?> <a href="<?php echo base_url();?>admin/prescription/DupPres/<?=$prescription->id?>"><?php echo '('.$prescription->countDuplicate.')';?></a><?php }?></td>
								<td><?=$prescription->lastname?></td>
								<td><?=$prescription->rxNumber?></td>
								<td><?=$prescription->storeName?></td>
								<td><?php if($prescription->priority=='1'){ echo 'High'; }else{ echo "Normal";}?></td>
								<td><?php if($prescription->status=='Approval') { echo "Awaiting Approval";}elseif($prescription->status=='Counsel'){ echo "Awaiting Counsel";}else{echo $prescription->status; }?></td>
							</tr>
								<?php 
						 }
						}
					else{ 
					?>
					<tr>
						<td  colspan="6">No records were found.</td>
					</tr>
				<?php  } ?>
				</tbody>
				</table>						
				</div> <!-- /widget-content -->				
				  <div class="pagination pull-right"">
						<?php echo $this->pagination->create_links(); ?>
					</div>
			</div> <!-- /widget -->									
		  </div> <!-- /span6 -->
<?php $CI->load->view('admin/common/footer.php');?>
