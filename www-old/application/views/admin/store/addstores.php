<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?> 
	      	<div class="span9"> 
	      		
				<?php 
			if(!empty($errors))
			{?>
				<div  class="alert alert-error" ><a class="close" data-dismiss="alert" >×</a><?=$errors?></div>
				<?php 
				}	 
			?>
	      		<div class="widget ">	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Store</h3>
	  				</div> <!-- /widget-header -->					
					<div class="widget-content">
						<div class="tabbable">							
						<br>
							<div class="tab-content">
								<div class="tab-pane active" id="addStore">
								<form method="post" class="form-horizontal" action="<?=base_url()?>admin/store/save" name="addStore" id="addStore">
									<fieldset>										
									<input type="hidden" name="storeId"  value="<?php if(!empty($storeData->storeId)){echo $storeData->storeId;} else if(!empty($_POST['storeId'])){echo $_POST['storeId'];}?>" />					
										<div class="control-group">											
											<label class="control-label" for="name">Store Name <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" id="name" name="name" class="{validate:{required:true,maxlength:150}} input-xlarge" title="Please enter Name." value="<?php if(!empty($_POST['name'])){echo $_POST['name'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="description">Description</label>
											<div class="controls">
												<textarea name="description" class="input-xlarge" ><?php if(!empty($_POST['description'])){echo $_POST['description'];}?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="contactname">Contact Name</label>
											<div class="controls">
												<input type="text" id="contactname" name="contactname" class="input-xlarge"  value="<?php if(!empty($_POST['contactname'])){echo $_POST['contactname'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="address">Address</label>
											<div class="controls">
												<textarea name="address" class="input-xlarge" ><?php if(!empty($_POST['address'])){echo $_POST['address'];}?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="city">City</label>
											<div class="controls">
												<input type="text" id="city" name="city" class="input-xlarge" value="<?php if(!empty($_POST['city'])){echo $_POST['city'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="state">State</label>
											<div class="controls">
												<input type="text" id="state" name="state" class="input-xlarge" value="<?php if(!empty($_POST['state'])){echo $_POST['state'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="zip">Zip</label>
											<div class="controls">
												<input type="text" id="zip" name="zip" class="input-xlarge"  value="<?php if(!empty($_POST['zip'])){echo $_POST['zip'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="phonenumber">Phone Number</label>
											<div class="controls">
												<input type="text" id="phonenumber" name="phonenumber" class="input-xlarge"  value="<?php if(!empty($_POST['phonenumber'])){echo $_POST['phonenumber'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
									
										
										<div class="control-group">											
											<label class="control-label" for="company">Select Company</label>
											<div class="controls">
												<select name="company" class="input-xlarge">
													<?php
														foreach($allcompanies as $company)
														{
															if($company->id==$_POST['company'])
																echo "<option value='".$company->id."' selected>".$company->companyName."</option>";
															else
																echo "<option value='".$company->id."'>".$company->companyName."</option>";
														}
													?>
												</select>
											 
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="accessCode">Access Code <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-xlarge" id="accessCode" name="accessCode" value="<?php if(!empty($_POST['accessCode'])){echo $_POST['accessCode'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
											<div class="control-group">											
											<label class="control-label" for="accessCode">RxKey Service URL: <font style="color: red">*</font></label>
												<div class="controls">
												<input type="text" class="input-xlarge" id="serviceURL" name="serviceURL" value="<?php if(!empty($_POST['serviceURL'])){echo $_POST['serviceURL'];}?>"/>
 
												</div> <!-- /controls -->				
											</div> <!-- /control-group -->



											<br />
										<div class="form-actions">
											<button type="submit" class="btn btn-primary"  id="save" value="Submit" name="submit">Add Store</button> 
											<a class="btn" href="<?php echo base_url();?>admin/store/index">Cancel</a>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div>	
						</div>
					</div> <!-- /widget-content -->
				</div> <!-- /widget -->
		    </div> <!-- /span8 -->				
<?php $CI->load->view('admin/common/footer.php');?>	