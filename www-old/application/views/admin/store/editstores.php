<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?> 
 	<div class="span9"> 
	      	
				<?php 
			if(!empty($errors))
			{?>
				<div  class="alert alert-error" ><a class="close" data-dismiss="alert" >×</a><?=$errors?></div>
				<?php 
				}	 
			?>
	      		<div class="widget ">	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Store</h3>
	  				</div> <!-- /widget-header -->					
					<div class="widget-content">
						<div class="tabbable">							
						<br>
							<div class="tab-content">
								<div class="tab-pane active" id="editStore">
								<form method="post" class="form-horizontal"action="<?=base_url()?>admin/store/update" name="editStore" id="editStore">
									<fieldset>										
									<input type="hidden" name="storeId"  value="<?php if(!empty($storeData->storeId)){echo $storeData->storeId;} else if(!empty($_POST['storeId'])){echo $_POST['storeId'];}?>" />				
										<div class="control-group">											
											<label class="control-label" for="name">Store Name <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-xlarge" id="name" name="name" value="<?php if(!empty($storeData->storeName)){echo $storeData->storeName;} else if(!empty($_POST['name'])){echo $_POST['name'];}?>"/>
												
											</div> 		
										</div> 

										<div class="control-group">											
											<label class="control-label" for="description">Description</label>
											<div class="controls">
												<textarea name="description" class="input-xlarge" ><?php if(!empty($storeData->description)){echo $storeData->description;} else if(!empty($_POST['description'])){echo $_POST['description'];}?></textarea>
											</div> 
										</div>
										<div class="control-group">											
											<label class="control-label" for="contactname">Contact Name</label>
											<div class="controls">
												<input type="text" id="contactname" name="contactname" class="input-xlarge"  value="<?php if(!empty($storeData->contactName)){echo $storeData->contactName;} else if(!empty($_POST['contactname'])){echo $_POST['contactname'];}?>"/>
											</div> 			
										</div> 
										<div class="control-group">											
											<label class="control-label" for="address">Address</label>
											<div class="controls">
												<textarea name="address" class="input-xlarge" ><?php if(!empty($storeData->address)){echo $storeData->address;} else if(!empty($_POST['address'])){echo $_POST['address'];}?></textarea>
											</div> 			
										</div> 
										<div class="control-group">											
											<label class="control-label" for="city">City</label>
											<div class="controls">
												<input type="text" id="city" name="city" class="input-xlarge" value="<?php if(!empty($storeData->city)){echo $storeData->city;} else if(!empty($_POST['city'])){echo $_POST['city'];}?>"/>
											</div> 			
										</div> 
										<div class="control-group">											
											<label class="control-label" for="state">State</label>
											<div class="controls">
												<input type="text" id="state" name="state" class="input-xlarge" value="<?php if(!empty($storeData->state)){echo $storeData->state;} else if(!empty($_POST['state'])){echo $_POST['state'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="zip">Zip</label>
											<div class="controls">
												<input type="text" id="zip" name="zip" class="input-xlarge"  value="<?php if(!empty($storeData->zip)){echo $storeData->zip;} else if(!empty($_POST['zip'])){echo $_POST['zip'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="phonenumber">Phone Number</label>
											<div class="controls">
												<input type="text" id="phonenumber" name="phonenumber" class="input-xlarge"  value="<?php if(!empty($storeData->phoneNumber)){echo $storeData->phoneNumber;} else if(!empty($_POST['phonenumber'])){echo $_POST['phonenumber'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="companysize">Select Company</label>
											<div class="controls">
												<select name="company" class="input-xlarge">
													<?php
														foreach($allcompanies as $company)
														{
															if(( !empty($_POST['company']) && $company->id==$_POST['company'] ) || ( !empty($storeData->companyId) && $company->id==$storeData->companyId))
																echo "<option value='".$company->id."' selected>".$company->companyName."</option>";
															else
																echo "<option value='".$company->id."'>".$company->companyName."</option>";
														}
													?>
												</select>
											 
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="accessCode">Access Code <font style="color: red">*</font></label>
											<div class="controls">
												<input type="text" class="input-xlarge" id="accessCode" name="accessCode" value="<?php if(!empty($storeData->accessCode)){echo $storeData->accessCode;} else if(!empty($_POST['accessCode'])){echo $_POST['accessCode'];}?>"/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
											<div class="control-group">											
											<label class="control-label" for="accessCode">RxKey Service URL: <font style="color: red">*</font></label>
												<div class="controls">
													<input type="text" class="input-xlarge" id="serviceURL" name="serviceURL" value="<?php if(!empty($storeData->storeUrl)){echo $storeData->storeUrl;} else if(!empty($_POST['serviceURL'])){echo $_POST['serviceURL'];}?>"/>
												</div> <!-- /controls -->				
											</div> <!-- /control-group -->


										<input type="hidden" name="originalAccessCode" value="<?php if(!empty($storeData->accessCode)){echo $storeData->accessCode;} else if(!empty($_POST['originalAccessCode'])){echo $_POST['originalAccessCode'];}?>"/>
											<br />
										<div class="form-actions">
											<button type="submit" class="btn btn-primary"  type="submit" id="save" value="Submit" name="submit">Save Store</button> 
											<a class="btn" href="<?php echo base_url();?>admin/store/index">Cancel</a>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div>	
						</div>
					</div> <!-- /widget-content -->
				</div> <!-- /widget -->
		    </div> <!-- /span8 -->				
 

<?php $CI->load->view('admin/common/footer.php');?>	