<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?>
	<div class="span10">	
		<script type="text/javascript">
		$().ready(function() {
			$(".close").click(function() {
		 $('#removeMessage').remove();
			});
		});
		</script>
		<?php 
			if($this->session->flashdata('message'))
			{?>
				<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
			<?php 
			}		
			if($this->session->flashdata('message1'))
			{?>
				<div class="alert alert-success redError"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message1') ; ?></div>
			<?php 
			}
		 ?>
		<div class="alert"> <a class="close" data-dismiss="alert">×</a>Found <?php if(!empty($total)) echo $total ; else echo "0"; ?> records</div>
			<div class="widget widget-table action-table">						
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>Store List</h3>
				</div> <!-- /widget-header -->					
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Name</th> 
								<th>Description</th>
								<th>Contact Name</th>
								<th>Phone Number</th>
 								<th class="td-actions">Options</th>
							</tr>
						</thead>
						<tbody>
							<?php
						if(!empty($allStore)){ 
						foreach($allStore as $stores)
						{?>
							<tr>
								<td><?=$stores->storeName?></td>
								<td><?=$stores->description?></td>
								<td><?=$stores->contactName?></td>
								<td><?=$stores->phoneNumber?></td>
 								<td class="td-actions">									
									<a class="btn btn-small btn-warning" href="<?php echo base_url();?>admin/store/edit/<?=$stores->storeId?>" title="Click here to edit">
										<i class="btn-icon-only icon-ok"></i>										
									</a>									
									<a class="btn btn-small" href="<?php echo base_url();?>admin/store/delete/<?=$stores->storeId?>" onclick="return confirm('Are you sure want to delete this store?');" title="Click here to delete">
										<i class="btn-icon-only icon-remove"></i>										
									</a>
								</td>
								</tr>
								<?php 
						 }
						}
					else{ 
					?>
					<tr>
						<td  colspan="3">No records were found.</td>
					</tr>
				<?php  } ?>
				</tbody>
				</table>						
				</div> <!-- /widget-content -->				
				  <div class="pagination pull-right"">
						<?php echo $this->pagination->create_links(); ?>
					</div>
			</div> <!-- /widget -->									
		  </div> <!-- /span6 -->
<?php $CI->load->view('admin/common/footer.php');?>
