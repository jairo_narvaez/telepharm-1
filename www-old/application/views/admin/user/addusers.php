<?php
$CI = &get_instance();
$CI -> load -> view('admin/common/header.php');
?>

<script src="<?php echo base_url();?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script language=javascript type='text/javascript'>
	$().ready(function() {
		//changeFunc();
		document.getElementById('mainstore').style.display = 'none';
		//document.getElementById('mainstore').style.display = '';
	});

</script>
<script type="text/javascript">
	function changeFunc()
{
document.getElementById('mainstore').style.display = '';
var selectBox = $("#selectBox option:selected").val();
if(!selectBox){
selectBox=0;
}
var posturl="<?=base_url()?>admin/member/displaystore/"+selectBox;
var myData={companyId:selectBox};
$.ajax({url : posturl, success : function(msg) {
$('#storeDiv').html(msg);
}});
}
</script>
<div class="span9" >
	<?php
if(!empty($errors))
{
	?>
	<div  class="alert alert-error" >
		<a class="close" data-dismiss="alert" >×</a><?=$errors
		?>
	</div>
	<?php
	}
	?>

	<div class="widget ">
		<div class="widget-header">
			<i class="icon-user"></i>
			<h3>Add User</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
			<div class="tabbable">
				<br>
				<div class="tab-content">
					<div class="tab-pane active" id="addUser">
						<form method="post" action="<?php echo base_url();?>/admin/member/save" class="form-horizontal"  name="addUser" id="addUser">
							<fieldset>
								<div class="mainuser">
									<div class="control-group">
										<label class="control-label" for="companysize">Company <font style="color: red">*</font></label>
										<div class="controls">
											<select name="company" onchange="changeFunc();"  id="selectBox">
												<option value="">Select Company</option>
												<?php
												foreach ($allcompanies as $company) {
													if (isset($_POST['company']) && $company -> id == $_POST['company'])
														//echo "<option value='" . $company -> id . "' selected>" . $company -> companyName . "</option>";
													echo "<option value='" . $company -> id . "'>" . $company -> companyName . "</option>";
													
													else
														echo "<option value='" . $company -> id . "'>" . $company -> companyName . "</option>";
												}
												?>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="firstname">User Type <font style="color: red">*</font></label>
										<div class="controls">
											<select id="usertype"  name="usertype" >
												<option value="0">---Select user type---</option>
												<option value="Administrator" <?php
													if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Administrator') { echo 'selected="selected"';
													};
												?>>Telepharm admin</option>
												<!--<option value="Company" <?php
													if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Company') { echo 'selected="selected"';
													};
												?>>Company admin</option>-->
												<option value="Technician" <?php
													if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Technician') { echo 'selected="selected"';
													};
												?> >Technician</option>
												<option value="Pharmacist" <?php
													if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Pharmacist') { echo 'selected="selected"';
													};
												?>>Pharmacist</option>
												
											</select>
										</div>
										<!-- /controls -->
									</div>
									<!-- /control-group -->
									<div class="control-group" id="mainstore">
										<label class="control-label" for="lastname">Store</label>
										<div class="controls"  id="storeDiv"></div>
									</div>
									<div class="control-group">
										<label class="control-label" for="firstname">First Name <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" class="input-large" id="firstname" name="firstname"   value="<?php
											if (!empty($_POST['firstname'])) {echo $_POST['firstname'];
											}
										?>"/>
										</div>
										<!-- /controls -->
									</div>
									<!-- /control-group -->
									<div class="control-group">
										<label class="control-label" for="lastname">Last Name</label>
										<div class="controls">
											<input type="text" class="input-large" id="lastname" name="lastname"  value="<?php
											if (!empty($_POST['lastname'])) {echo $_POST['lastname'];
											}
										?>"/>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" for="email">Email Address <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" class="input-large" id="useremail" name="useremail"  value="<?php
											if (!empty($_POST['useremail'])) {echo $_POST['useremail'];
											}
										?>"/>
										</div>
										<!-- /controls -->
									</div>


									<div class="control-group">
										<label class="control-label" for="email">Username <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" class="input-large" id="email" name="email"  value="<?php
											if (!empty($_POST['email'])) {echo $_POST['email'];
											}
										?>"/>
										</div>
										<!-- /controls -->
									</div>

									<!-- /control-group -->
									<div class="control-group">
										<label class="control-label" for="password">Password <font style="color: red">*</font></label>
										<div class="controls">
											<input type="password" class="input-large" id="password" name="password"  value="<?php
											if (!empty($_POST['password'])) {echo $_POST['password'];
											}
										?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="phonenumber">Phone</label>
										<div class="controls">
											<input type="text" class="input-large" id="phonenumber" name="phonenumber"   value="<?php
											if (!empty($_POST['phonenumber'])) {echo $_POST['phonenumber'];
											}
										?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="notes">Notes</label>
										<div class="controls">
											<textarea class="input-large" name="notes"><?php
											if (!empty($_POST['notes'])) {echo $_POST['notes'];
											}
										?></textarea>
										</div>
									</div>
									<div class="form-actions widthclass">
										<button type="submit" class="btn btn-primary"  id="save" value="Submit" name="submit">
											Add user
										</button>
										<a class="btn" href="<?php echo base_url();?>admin/member/index">Cancel</a>
									</div>
								</div>
								<div class="subuser">
									<table>	
										<tr>
											<td class="pad">
											<input type="checkbox"  name="companyadmin" class="input-xlarge" value="1" <?php
												if (isset($_POST['companyadmin']) && $_POST['companyadmin'] == '1') {echo 'checked="checked"';
												}
											?> />
											</td>
											<td>Company Admin </td>
										</tr>									
										<tr>
											<td class="pad">
											<input type="checkbox"  name="primarycontact" class="input-xlarge" value="1" <?php
												if (isset($_POST['primarycontact']) && $_POST['primarycontact'] == '1') {echo 'checked="checked"';
												}
											?> />
											</td>
											<td>Primary Contact </td>
										</tr>
									</table>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /widget-content -->
	</div>
	<!-- /widget -->
</div>
<!-- /span8 -->
<?php $CI -> load -> view('admin/common/footer.php');?>	