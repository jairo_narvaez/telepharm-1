<?php
$CI = &get_instance();
$CI -> load -> view('admin/common/header.php');
?>
<script src="<?php echo base_url();?>js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script language=javascript type='text/javascript'>
	$().ready(function() {
		//changeFunc();
		//	document.getElementById('mainstore').style.display = 'none';
		//document.getElementById('mainstore').style.display = '';
	});

</script>
<script type="text/javascript">
	function changeFunc()
{
document.getElementById('mainstore').style.display = '';
var selectBox = $("#selectBox option:selected").val();
if(!selectBox){
selectBox=0;
}
var posturl="<?=base_url()?>admin/member/displaystore/"+selectBox;
var myData={companyId:selectBox};
$.ajax({url : posturl, success : function(msg) {
$('#storeDiv').html(msg);
}});
}
</script>
<div class="span9">
	<?php
if(!empty($errors))
{
	?>
	<div  class="alert alert-error" >
		<a class="close" data-dismiss="alert" >×</a><?=$errors
		?>
	</div>
	<?php
	}
	?>
	<div class="widget ">
		<div class="widget-header">
			<i class="icon-user"></i>
			<h3>Edit User</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
			<div class="tabbable">
				<br>
				<div class="tab-content">
					<div class="tab-pane active" id="editUser">
						<form method="post" class="form-horizontal"action="<?=base_url()?>admin/member/update" name="editUser" id="editUser">
							<fieldset>
								<div class="mainuser">
									<input type="hidden" name="userId"  value="<?php
									if (!empty($userData -> id)) {echo $userData -> id;
									} else if (!empty($_POST['userId'])) {echo $_POST['userId'];
									}
								?>" />
									<div class="control-group">
										<label class="control-label" for="companysize">Company <font style="color: red">*</font></label>
										<div class="controls">
											<select name="company" onchange="changeFunc();"  id="selectBox">
												<?php
												foreach ($allcompanies as $company) {
													if ((isset($_POST['company']) && $company -> id == $_POST['company']) || (isset($userData -> companyId) && $company -> id == $userData -> companyId))
														echo "<option value='" . $company -> id . "' selected>" . $company -> companyName . "</option>";
													else
														echo "<option value='" . $company -> id . "'>" . $company -> companyName . "</option>";
												}
												?>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="firstname">User Type <font style="color: red">*</font></label>
										<div class="controls">
											<select id="usertype"  name="usertype"  >
												<option value="0">---Select user type---</option>
												<option value="Administrator" <?php
													if (!empty($userData -> userType) && $userData -> userType == 'Administrator') {echo 'selected="selected"';
													} else if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Administrator') { echo 'selected="selected"';
													};
												?>>Telepharm admin</option>
											<!--	<option value="Company" <?php
													if (!empty($userData -> userType) && $userData -> userType == 'Company') {echo 'selected="selected"';
													} else if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Company') { echo 'selected="selected"';
													};
												?>>Company admin</option>-->
												
												<option value="Technician" <?php
													if (!empty($userData -> userType) && $userData -> userType == 'Technician') {echo 'selected="selected"';
													} else if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Technician') { echo 'selected="selected"';
													}
													;
												?> >Technician</option>
												<option value="Pharmacist" <?php
													if (!empty($userData -> userType) && $userData -> userType == 'Pharmacist') {echo 'selected="selected"';
													} else if (!empty($_POST['usertype']) && $_POST['usertype'] == 'Pharmacist') { echo 'selected="selected"';
													}
													;
												?>>Pharmacist</option>
											</select>
										</div>
									</div>
									<div class="control-group" id="mainstore">
										<label class="control-label" for="lastname">Store</label>
										<div class="controls"  id="storeDiv">
											<div id="categories">
												<?php
													if(!empty($allStore)){
												?>
												<ul class="caegories reset fl">
													<?php
														foreach($allStore as $store){
													?>
													<li style="margin-top: -5px;">
														<input type="checkbox"  name="storeId[]"  style="margin-right: 10px;" value="<?=$store->storeId?>" <?php
														if (!empty($storeData)) {
															foreach ($storeData as $storeselected) {
																if ((!empty($store -> storeId) && $store -> storeId == $storeselected -> storeId)) {echo 'checked="checked"';
																}
															}
														}
													?> /><?=$store->storeName
														?>
													</li>
													<?php
													}
													?>
												</ul>
												<?php
												}
												else{ echo "<h6 class='nofound'>No store found</h6>";}
												?>
											</div>
										</div>
									</div>
								
									<div class="control-group">
										<label class="control-label" for="firstname">First Name <font style="color: red">*</font></label>
										<div class="controls">
											<input type="text" class="input-large" id="firstname" name="firstname"   value="<?php
											if (!empty($userData -> firstName)) {echo $userData -> firstName;
											} else if (!empty($_POST['firstname'])) {echo $_POST['firstname'];
											}
										?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="lastname">Last Name</label>
										<div class="controls">
											<input type="text" class="input-large" id="lastname" name="lastname"  value="<?php
											if (!empty($userData -> lastName)) {echo $userData -> lastName;
											} else if (!empty($_POST['lastname'])) {echo $_POST['lastname'];
											}
										?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="email">Email Address</label>
										<div class="controls">
											<input type="text" class="input-large" id="useremail" name="useremail"  value="<?php
											if (!empty($userData->useremail)) {echo $userData->useremail;
											} else if (!empty($_POST['useremail'])) {echo $_POST['useremail'];
											}
										?>"/>
										</div>
									</div>


									<div class="control-group">
										<label class="control-label" for="email">Username </label>
										<div class="controls">
											<input type="text" class="input-large" id="email" name="email" readonly="readonly" value="<?php
											if (!empty($userData -> email)) {echo $userData -> email;
											} else if (!empty($_POST['email'])) {echo $_POST['email'];
											}
										?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="phonenumber">Phone</label>
										<div class="controls">
											<input type="text" class="input-large" id="phonenumber" name="phonenumber"   value="<?php
											if (!empty($userData -> phoneNumber)) {echo $userData -> phoneNumber;
											} else if (!empty($_POST['phonenumber'])) {echo $_POST['phonenumber'];
											}
										?>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="notes">Notes</label>
										<div class="controls">
											<textarea class="input-large" name="notes"><?php
											if (!empty($_POST['notes'])) {echo $_POST['notes'];
											} else if (!empty($userData -> notes)) { echo $userData -> notes;
											}
										?></textarea>
										</div>
									</div>
									<div class="form-actions widthclass">
										<button type="submit" class="btn btn-primary"  type="submit" id="save" value="Submit" name="submit">
											Save User
										</button>
										<a class="btn" href="<?php echo base_url();?>admin/member/index">Cancel</a>
									</div>
								</div>
								<div class="subuser">
									<table>	
										<tr>
											<td class="pad">
											<input type="checkbox"  name="companyadmin" class="input-xlarge" value="1" <?php
												if (isset($userData -> companyAdmin) && $userData -> companyAdmin == '1') {echo 'checked="checked"';
												} else if (isset($_POST['companyadmin']) && $_POST['companyadmin'] == '1') {echo 'checked="checked"';
												}
											?> /> </td>
											<td> Company Admin </td>
										</tr>									
										<tr>
											<td class="pad">
											<input type="checkbox"  name="primarycontact" class="input-xlarge" value="1" <?php
												if (isset($userData -> primaryContact) && $userData -> primaryContact == '1') {echo 'checked="checked"';
												} else if (isset($_POST['primarycontact']) && $_POST['primarycontact'] == '1') {echo 'checked="checked"';
												}
											?> /> </td>
											<td> Primary Contact </td>
										</tr>
									</table>
								</div>
							</fieldset>
						</form>	
					</div>
				</div>
			</div>
		</div>
		<!-- /widget-content -->
	</div>
	<!-- /widget -->
</div>
<!-- /span8 -->
<?php $CI -> load -> view('admin/common/footer.php');?>	