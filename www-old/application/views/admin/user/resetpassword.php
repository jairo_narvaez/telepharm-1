<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
?> 

 	<div class="span9"> 
				<?php  // echo "<pre>";print_r($this->data);echo "</pre>";
			if(!empty($errors)) 
			{?>
				<div  class="alert alert-error" ><a class="close" data-dismiss="alert" >×</a><?=$errors?></div>
				<?php 
				}	 
			?>
	      		<div class="widget ">	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Reset Password</h3>
	  				</div> <!-- /widget-header -->					
					<div class="widget-content">
						<div class="tabbable">							
						<br>
							<div class="tab-content">
								<div class="tab-pane active" id="editUser">
								<form method="post" class="form-horizontal"action="<?=base_url()?>admin/member/updatepassword" name="editUser" id="editUser">
									<fieldset>										
									<input type="hidden" name="userId"  value="<?php if(!empty($userData->id)){echo $userData->id;} else if(!empty($_POST['userId'])){echo $_POST['userId'];}?>" />				
										
									
										<div class="control-group">											
											<label class="control-label" for="email">Username </label>
											<div class="controls">
												<input type="text" class="input-large" id="email" name="email" readonly="readonly" value="<?php if(!empty($userData->email)){echo $userData->email;} else if(!empty($_POST['email'])){echo $_POST['email'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
										<div class="control-group">											
											<label class="control-label" for="newpassword">New Password <font style="color: red;">*</font> </label>
											<div class="controls">
												<input type="password" class="input-large" id="newpassword" name="newpassword"   value="<?php  if(!empty($_POST['newpassword'])){echo $_POST['newpassword'];}?>"/>
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
											<br />
										<div class="form-actions">
											<button type="submit" class="btn btn-primary"  type="submit" id="save" value="Submit" name="submit">Reset Password</button> 
											<a class="btn" href="<?php echo base_url();?>admin/member/index">Cancel</a>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div>	
						</div>
					</div> <!-- /widget-content -->
				</div> <!-- /widget -->
		    </div> <!-- /span8 -->	
<?php $CI->load->view('admin/common/footer.php');?>	