<?php
$CI = &get_instance();
$CI->load->view('admin/common/header.php');
//echo "<pre>";print_r($this->data);echo "</pre>";
?>
	<div class="span10">	
		<script type="text/javascript">
		$().ready(function() {
			$(".close").click(function() {
		 $('#removeMessage').remove();
			});
		});
		</script>		
		<?php 
			if($this->session->userdata('usertype')){ $usertype = $this->session->userdata('usertype'); }
			if($this->session->userdata('status')){ $status= $this->session->userdata('status'); }
			if($this->session->flashdata('message'))
			{?>
				<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
			<?php 
			}
			if($this->session->flashdata('message1'))
			{?>
				<div class="alert alert-success redError"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message1') ; ?></div>
			<?php 
			}
		 ?>
		 <form method="post" action="<?php echo base_url();?>admin/member/index"   name="addUser" id="addUser">								
		  	<div class="widget widget-box">					
					<div class="widget-header">	
						<i class="icon-star"></i>
						<h3>Search</h3>			
					</div> <!-- /widget-header -->					
					<div class="widget-content">
						<table>
							<tr>
								<td><label class="control-label" for="usertype">User Type </label>
												</td><td>
												 <select id="usertype"  name="usertype" >
												<option value="">Select User Type</option>											
												<option value="Technician" <?php if(!empty($_POST['usertype']) && $_POST['usertype']=='Technician'){ echo 'selected="selected"';}elseif(!empty($usertype) && $usertype=='Technician'){ echo 'selected="selected"';}?> >Technician</option>
												<option value="Pharmacist" <?php if(!empty($_POST['usertype']) && $_POST['usertype']=='Pharmacist'){ echo 'selected="selected"';}elseif(!empty($usertype) && $usertype=='Pharmacist'){ echo 'selected="selected"';}?> >Pharmacist</option>
												</select>
										</td><td><label class="control-label" for="status">Status </label></td>
											<td>
												 <select id="usertype"  name="status">
												<option value="">Select Status</option>
												<option value="1" <?php if((!empty($_POST['status']) && $_POST['status']=='1')){ echo 'selected="selected"';} elseif(!empty($status) && $status=='1'){ echo 'selected="selected"';}?> >Active</option>
												<option value="0" <?php if((isset($_POST['status']) && $_POST['status']=='0')){ echo 'selected="selected"';}elseif(isset($status) && $status=='0'){ echo 'selected="selected"';}?>>Inactive</option>
												</select>
												</td><td valign="top">
											<button type="submit" class="btn btn-primary"  id="save" value="Submit" name="submit">Search</button> 											
										</td>
							</tr>
						</table>
					</div> <!-- /widget-content -->					
				</div>				
				</form>
		 
		<div class="alert"> <a class="close" data-dismiss="alert">×</a>Found <?php if(!empty($total)) echo $total ; else echo "0"; ?> records</div>
			<div class="widget widget-table action-table">						
				<div class="widget-header">
					<i class="icon-th-list"></i>
					<h3>User List</h3>
				</div> <!-- /widget-header -->					
				<div class="widget-content">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>User Name</th> 
								<th>First Name</th> 
								<th>Last Name</th>
								<th>Phone Number</th>
								<th>Company Name</th>
								<th>User Type</th>
								<th>Status</th>
								<th class="td-actions">Options</th>
							</tr>
						</thead>
						<tbody>
							<?php
						if(!empty($allUser)){ 
						foreach($allUser as $users)
						{?>
							<tr>
								<td><?=$users->email?></td>
								<td><?=$users->firstname?></td>
								<td><?=$users->lastname?></td>
								<td><?=$users->phonenumber?></td>
								<td><?=$users->companyname?></td>							
								<td><?=$users->usertype?>
								</td>
								<td><?php if(isset($users->status) && $users->status=='1'){ echo 'Active';}else{ echo "Inactive";}?></td>
								<td class="td-actions">
									<a class="btn btn-small btn-warning" href="<?php echo base_url();?>admin/member/edit/<?=$users->id?>" title="Click here to edit">
										<i class="btn-icon-only icon-ok"></i>										
									</a>									
									<a class="btn btn-small" href="<?php echo base_url();?>admin/member/delete/<?=$users->id?>" onclick="return confirm('Are you sure want to delete this store?');" title="Click here to delete">
										<i class="btn-icon-only icon-remove"></i>										
									</a>
									<a class="btn btn-small" href="<?php echo base_url();?>admin/member/respassword/<?=$users->id?>"  title="Click here to reset password">
										<i class="btn-icon-only icon-">Reset Password</i>										
									</a>
								</td>
								</tr>
								<?php 
						 }
						}
					else{ 		
					?>
					<tr>
						<td  colspan="8">No records were found.</td>
					</tr>
				<?php  } ?>
				</tbody>
				</table>						
				</div> <!-- /widget-content -->				
				  <div class="pagination pull-right"">
						<?php echo $this->pagination->create_links(); ?>
					</div>
			</div> <!-- /widget -->									
		  </div> <!-- /span6 -->
<?php $CI->load->view('admin/common/footer.php');?>
