<?php
$CI=&get_instance();
$CI->load->view('common/header');

?>
<script type="text/javascript">
	jQuery( function($) {
		$('tbody td[data-href]').addClass('clickable').click( function() {
			window.location = $(this).attr('data-href');
		}).find('a').hover( function() {
			$(this).parents('td').unbind('click');
		}, function() {
			$(this).parents('td').click( function() {
				window.location = $(this).attr('data-href');
			});
		});
	});
</script>
<script type="text/javascript">

$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
 ajaxManager.run(); 
});

$(document).on("click", ".open-AddBookDialogCompleted", function () {
	
     var myBookId = $(this).data('id2');
     var values=myBookId.split("-");    
     $(".modal-body #rxId2").val( values[0] );
      $(".modal-body #storeId2").val( values[1] );
    $('#myModalCompleted').modal('show');
});

$(document).on("click", ".open-AddBookDialogDelete", function () {
	
     var myBookId = $(this).data('id');
     var values=myBookId.split("-");    
     $(".modal-body #rxId").val( values[0] );
      $(".modal-body #storeId").val( values[1] );
    $('#myModalDelete').modal('show');
});

$(document).on("click", ".clickTocall", function () {
     var myBookId = $(this).data('id5');   
     $(".modal-header #rxNumber").replaceWith('<span id="rxNumber">'+myBookId+'</span>');
    $('#myModalPhone').modal('show');
});
</script>
<script type="text/javascript">
function addSession(rxId, storeId){
	var parameters = [{ name: 'rxId', value: rxId }, { name: 'storeId', value: storeId }];
	$.ajax({
		url: '<?=base_url()?>technician/viewmore/addVideoSession',
		type: 'POST', 
		data: parameters,
		success: function(transport) {
			$("#addVideo_"+rxId).hide();
		}
	});
}
</script>
<div class="span12">
<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php if(!empty($count)) echo $count; else echo "0";?> records found</div>
<?php if(!empty($message)){ ?><div class="advanceMessage"><i><?php echo $message; ?></i></div><?php } ?>
<table class="table table-bordered">
  			<thead>
 				<th class="viewHeaders">&nbsp;</th>
 				<th class="viewHeaders">Rx #</th>
 				<th class="viewHeaders">Patient Name</th>
 				<th class="viewHeaders">Status</th>
 				<th class="viewHeaders">Date/Time</th>
 				<th class="viewHeaders">Options</th>
 			</thead>
			<tbody>
				<?php
					if(!empty($prescription))
					{
						foreach($prescription as $rx)
						{
							?>
							<tr >
								<td><?php
						if(!empty($rx->priority) )
							{?><span id="low_<?php echo $rx->rxId;?>" class="markHigh" onclick="javascript:setHigh('<?php echo $rx->rxId;?>','<?php echo base_url();?>index/setHigh','0');" >&nbsp;</span><?php }
							else{?><span id="low_<?php echo $rx->rxId;?>" class="markLow" onclick="javascript:setHigh('<?php echo $rx->rxId;?>','<?php echo base_url();?>index/setHigh','1');">&nbsp;</span><?php }
						 ?></td>
								<td data-href="<?=base_url()?>technician/rx/view/<?=$rx->rxId?>"><?=$rx->rxNumber?></td>
								<td data-href="<?=base_url()?>technician/rx/view/<?=$rx->rxId?>"><?=$rx->firstName." ".$rx->lastName?></td>
								<td data-href="<?=base_url()?>technician/rx/view/<?=$rx->rxId?>"><?php 
									if($rx->status=='Approval')
										echo 'Awaiting Approval';
									elseif($rx->status=='Counsel')
										echo 'Awaiting Counsel';
									elseif($rx->status=='Completed')
										echo 'Completed';
									elseif($rx->status=='Cancelled')
										echo 'Cancelled';
									elseif($rx->status=='Draft')
										echo 'Draft';
									elseif($rx->status=='Rejected')
										echo 'Rejected';
									elseif($rx->status=='Archive')
										echo 'Archived';
									elseif($rx->status=='OnHold')
										echo 'On Hold';
									
								?></td>
								<td data-href="<?=base_url()?>technician/rx/view/<?=$rx->rxId?>">
									<?php
										$date=date('m/d/Y h:ia',strtotime($rx->updateDate));
										echo $date ;
									?>
								</td>
								<td>
									<a id="addVideo_<?php echo $rx->rxId;?>" class="clickTocall"  data-toggle="modal" href="#myModalPhone" data-id5="<?=$rx->rxNumber."-".$rx->rxId?>" title="Call pharmacist" 
											onclick="javascript:addSession('<?php echo $rx->rxId;?>','<?php echo $rx->storeId;?>');"><i class="icon-facetime-video"></i></a>
									<a id="removeVideo_<?php echo $rx->rxId;?>" title="Cancel Call" href="javascript:void(0);"
											onclick="javascript:cancelConference('<?php echo $rx->rxId;?>');"><i class="icon-remove-circle"></i></a>
								<?php	
									if($rx->videoConf=='0'){
										?>
										<script type="text/javascript">
											$("#removeVideo_"+<?php echo $rx->rxId;?>).hide();
										</script>
										<?php
									}
									else{
										?>
										<script type="text/javascript">
											$("#addVideo_"+<?php echo $rx->rxId;?>).hide();
										</script>
										<?php
									}?>
									</td>
							</tr>
							<?php
						}
						
					}
					else{?>
						 <tr> <td colspan="4">no record found</td></tr>					
				<?php 	}
					?>               
			</tbody>
		</table>

<div class="pagination">
<div class="pull-right">
	<?php echo $this->pagination->create_links(); ?>
</div>
</div>
</div>


<!------------------Modal section----------------------->
<div class="modal hide" id="myModalCompleted">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Refuse Rx</h3>
	</div>
	<div class="modal-body">  
	<form name="completeCommentCompleted" method="post">
	<p>Add comment</p>
	 <input type="hidden" name="pageName" value="advancesearch"/> 
	<input type="hidden" name="storeId" id="storeId2" value=""/>      		
	<input type="hidden" name="rxId"  id="rxId2"  value=""/>
	<textarea name="comment" rows="3" cols="60"></textarea>    		
	</form>		
	</div>
	<div class="modal-footer">
	<a href="#" onclick="document.completeCommentCompleted.action='<?=base_url()?>pharmacist/viewmore/setComplete';document.completeCommentCompleted.submit();" class="btn btn-primary">Refuse</a>
	<a href="#" class="btn" data-dismiss="modal">Cancel</a>
	</div>
</div>



<div class="modal hide" id="myModalDelete">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h3>Archive Rx</h3>
	</div>
	<div class="modal-body">  
		<form name="completeCommentDelete" method="post">
			<p>Add comment</p>
			<input type="hidden" name="pageName" value="advancesearch"/> 	
			<input type="hidden" name="storeId" id="storeId" value=""/>    			
			<input type="hidden" name="rxId"  id="rxId"  value=""/>
			<textarea name="comment" rows="3" cols="60"></textarea>    		
		</form>		
	</div>
	<div class="modal-footer">
		<a href="#" onclick="document.completeCommentDelete.action='<?=base_url()?>pharmacist/viewmore/setArchive';document.completeCommentDelete.submit();" class="btn btn-primary">Archive</a>
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
	</div>
</div>

<div class="modal hide" id="myModalPhone">
	<div class="modal-header" id="modal-header" >
	<button type="button" class="close marginTop" data-dismiss="modal">x</button>
	A call has been initiated to the pharmacist for script <span id="rxNumber"></span> &nbsp;&nbsp;<a href="#"  data-dismiss="modal">(Cancel call)</a>
	</div>
	
</div>
<!------------------Modal section----------------------->
<script type="text/javascript">
function addSession(rxId, storeId){
 	$("#addVideo_"+rxId).hide();
	$("#removeVideo_"+rxId).show();
	var parameters = [{ name: 'rxId', value: rxId }, { name: 'storeId', value: storeId }];
	ajaxManager.addReq({
		url: baseUrl+'technician/viewmore/addVideoSession',
		type: 'POST', 
		data: parameters,
		success: function(transport) {
		}
	});
}

function cancelConference(rxId){
	$("#removeVideo_"+rxId).hide();
	$("#addVideo_"+rxId).show();
	ajaxManager.addReq({
		url: baseUrl+'technician/viewmore/closeVideoConferencing/'+rxId,
		success: function(transport) {
		}
	});
}
 </script>
<?php
$CI->load->view('common/footer');
?>
