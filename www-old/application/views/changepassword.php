<?php
$CI=&get_instance();
$usertype=$this->session->userdata('userType');
if($usertype=='Technician')
$CI->load->view('common/header');
if($usertype=='Pharmacist')
$CI->load->view('pharmacist/common/header');
?>

<script type="text/javascript">
$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});

}); 
</script> 

<div class="span10">     
<?php 
	$seg=$this->uri->segment(4);   
	$email = $this->session->userdata('email');
	if($this->session->flashdata('message'))
	{?>		
		<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>		
	<?php 
	}
?> 
 		
	 <form method="post" name="changepassowrd_frm" class="well"  action="<?php echo base_url();?>index/resetpassword">	    
             <div class="control-group">
            <label class="control-label" for="input01">Username</label>
              <div class="controls">
                <input type="text" class="input-large"   id="emailaddress" name="emailaddress" readonly="readonly" 
                value="<?php if(!empty($email)){echo $email;} else if(!empty($_POST['emailaddress'])){echo $_POST['emailaddress'];}?>"/>                
               </div> 
             </div> 
             
              <div class="control-group">
            	<label class="control-label" for="input01">Current Password <span class="star">*</span></label>
              	<div class="controls">
                <input type="password" class="input-large" id="currentpass" name="currentpass" value="<?php if(!empty($_POST['currentpass'])){echo $_POST['currentpass'];}?>" /> 
              <span class="help-inline redError"><?php echo form_error('currentpass'); ?></span>
               </div> 
             </div>
               <div class="control-group">
            <label class="control-label" for="input01">New Password <span class="star">*</span></label>
              <div class="controls">
                <input type="password" class="input-large" id="newpass" name="newpass" value="<?php if(!empty($_POST['newpass'])){echo $_POST['newpass'];}?>" /> 
              <span class="help-inline redError"><?php echo form_error('newpass'); ?></span>
               </div> 
             </div>
              <div class="control-group">
           		 <label class="control-label" for="input01">Confirm New Password <span class="star">*</span></label>
              	<div class="controls">
               	 <input type="password" class="input-large" id="confirmpass" name="confirmpass" value="<?php if(!empty($_POST['confirmpass'])){echo $_POST['confirmpass'];}?>" /> 
              <span class="help-inline redError"><?php echo form_error('confirmpass'); ?></span>
               </div> 
             </div>
             <div class="control-group">
				<div class="controls">
					<input type="submit" value="Submit" name="submit" class="btn btn-primary">
				</div>
			</div>
 		</form>	    
</div>
<?php
if($usertype=='Technician')
$CI->load->view('common/footer');
if($usertype=='Pharmacist')
$CI->load->view('pharmacist/common/footer');
?>
