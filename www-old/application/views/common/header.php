<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Telepharm | Remote Verification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="cetw6E-mBWFOc6vNE7ocleImJelXGUW8h6b8lAY9Pw0" />
    <script src="<?php echo site_url();?>js/jquery-1.7.1.min.js"></script>
    <script src="<?php echo site_url();?>js/jquery.ui.datepicker.js"></script>
    <script src="<?php echo site_url();?>js/jquery.ui.core.js"></script>
    <script src="<?php echo site_url();?>assets/js/crx.js"></script>
    <link href="<?php echo site_url();?>assets/css/bootstrap.css" rel="stylesheet">
  <link href="<?php echo site_url();?>css/jquery.datepick.css" rel="stylesheet">
  <link href="<?php echo site_url();?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
   		<script src="<?php echo site_url();?>js/ajaxqueue.js"></script>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
	  #advanceSearch{margin-top:6px !important;}
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/crx.css" rel="stylesheet">
<SCRIPT LANGUAGE="JavaScript">
 var baseUrl="<?php echo base_url();?>";
 var notiUrl="<?php echo base_url();?>technician/viewmore/addComment";
 var clearUrl="<?php echo base_url();?>technician/viewmore/clearNotification";
 </SCRIPT>
 <!--script for notification--> 
 <script type="text/javascript">
   $(document).ready(function(){
   $("#notification").click(function(){ 				
		$.ajax({			
			url : '<?php echo base_url();?>technician/viewmore/getNotification',						
			success : function(msg) {$("#tblDetails").html(msg);}
		});
    });
    
});
</script>
 <?php
$storeId = $this->session->userdata('storeId');
?>	   
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo base_url();?>technician/rx/index"">Telepharm</a>
          <div class="nav-collapse">
          	<ul class="nav">
				 <li  <?php if($this->uri->segment(4) == "Rejected") echo "class='active'";?>>
				<a href="<?=base_url()?>technician/viewmore/index/Rejected">Rejected&nbsp;&nbsp;<div  id="nRejected" style="margin-left: 54px; margin-top: -28px;"><?php echo ($countRejected>0)? '<span class="bubble">'.$countRejected.'</span>':'';?></span></div></a>
			  </li>
          		 <li  <?php if($this->uri->segment(4) == "Approval") echo "class='active'";?>>
				<a href="<?=base_url()?>technician/viewmore/index/Approval">Approval&nbsp;&nbsp;<div  id="nApproval" style="margin-left: 54px; margin-top: -28px;"><?php echo ($countApproval>0)? '<span class="bubble">'.$countApproval.'</span>':'';?></span></div></a>
			  </li>
			<li  <?php if($this->uri->segment(4) == "Counsel") echo "class='active'";?>>
				<a href="<?=base_url()?>technician/viewmore/index/Counsel">Counsel&nbsp;&nbsp;<div id="nCounsel" style="margin-left: 54px; margin-top: -28px;"><?php echo ($countCounsel>0)? '<span class="bubble">'.$countCounsel.'</span>':'';?></span></div></a>
			  </li>
 
			  <li <?php if($this->uri->segment(4) == "OnHold") echo "class='active'";?>>
				<a href="<?=base_url()?>technician/viewmore/index/OnHold">On Hold&nbsp;&nbsp;<div id="nOnhold" style="margin-left: 54px; margin-top: -11px;"><?php echo ($countOnHold>0)? '<span class="bubblemenu" >'.$countOnHold.'</span>':'';?></span></div></a>
				</li>
           		<li class="dropdown">
              		<a data-toggle="dropdown" class="dropdown-toggle" href="#">more...<b class="caret"></b></a>
		              <ul class="dropdown-menu">
							<li>
							<a href="<?=base_url()?>technician/viewmore/index/Completed">Completed&nbsp;&nbsp;<div id="nCompleted"><?php echo ($countCompleted>0)? '<span class="bubblemenu" >'.$countCompleted.'</span>':'';?></span></div></a>
							</li>
							<li >
							<a href="<?=base_url()?>technician/viewmore/index/Cancelled">Cancelled&nbsp;&nbsp;<div id="nCancelled" ><?php echo ($countCancelled>0)? '<span class="bubblemenu" >'.$countCancelled.'</span>':'';?></span></div></a>
							</li>
							<!--
								<li >
							<a href="<?=base_url()?>technician/viewmore/index/OnHold">On Hold&nbsp;&nbsp;<div id="nOnhold" ><?php echo ($countOnHold>0)? '<span class="bubblemenu" >'.$countOnHold.'</span>':'';?></span></div></a>
							</li>
							<li >-->
								<li>
							<a href="<?=base_url()?>technician/viewmore/index/Draft">Draft&nbsp;&nbsp;<div id="nDraft" ><?php echo ($countDraft>0)? '<span class="drfatbubble" >'.$countDraft.'</span>':'';?></span></div></a>
							</li>
		              </ul>
            	</li>
					<li class="dropdown">
              		<a data-toggle="dropdown" id="notification" class="dropdown-toggle" href="#">Comments&nbsp;
              		&nbsp;&nbsp;&nbsp;<div id="nNotification" style="margin-left: 68px; margin-top: -28px;"><?php echo ($countNotification>0)? '<span class="bubble" id="countnotification">'.$countNotification.'</span>':'';?></span></div>
              		</a> 
		           <ul class="dropdown-menu" id="tblDetails" style="margin-top:10px">
				</ul>
            	</li>
				 
 					 <a id="advanceSearch" class="btn btn-danger" href="<?=base_url()?>technician/rx/createrx">Create Rx</a>
 				</ul>
				<ul class="nav pull-right">
					<form action="<?=base_url()?>technician/rx/search" class="navbar-search" method="post" >
					<input type="text" placeholder="Search Rx" class="search-query span3" name="search" style="width:152px" value="<?php if(!empty($_POST['search'])){echo $_POST['search'];} ?>"  />&nbsp;<a id="advanceSearch" style="margin-top:0px !important" data-toggle="modal" href="#myModalSearch" class="btn btn-small btn-info"><i class="icon-search icon-white"></i></a>
					</form>
          			<li class="divider-vertical"></li>
          		<li class="dropdown pull-right">
              		<a data-toggle="dropdown" class="dropdown-toggle" href="#">
					<?php
							$pname =$this->session->userdata('photo');
							if($pname!=''){?> <img style="width:21px;height:20px;margin-top:-8px;" src="<?php echo base_url();?>uploads/pharma/<?php echo $this->session->userdata('photo');?>" ><?php } ?>
						&nbsp;
						<?php echo $this->session->userdata('userName');?> <b class="caret"></b></a>
		              <ul class="dropdown-menu">
		              	<li><a href="<?=base_url()?>index/logout">Logout</a></li>
			                <li><a href="<?=base_url()?>index/profile">Update profile</a></li>
			                <li><a href="<?=base_url()?>index/changepassword">Change password</a></li>
							<li><a title="Contact support" data-id4="37-2" href="#csupport" data-toggle="modal" id="contactsupportclass" class="contactsupportclass">Contact support</a>
							 </li>
		              </ul>
            	</li>
			
            	 
               </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
    <div class="row-fluid">
  	<div class="span12">

 
 <div class="modal hide" id="myModalSearch">
	  <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3>Advanced Search</h3>
        </div>
	
	  <div class="modal-body">
	   <div class="">	
		<form action="<?=base_url()?>technician/rx/advancesearch" class="navbar-search form-horizontal Advance" method="post" >
         		
			<div class="control-group">
				<label class="control-label">Status:</label>
				<div class="controls ">
					<select name="status">
						<option value="">---Select Status---</option>
						<option value="All">All</option>
						<option value="Approval">Awaiting Approval</option>
						<option value="OnHold">On Hold</option>
						<option value="Counsel">Awaiting Counsel</option>
						<option value="Completed">Completed</option>
						<option value="Cancelled">Cancelled</option>
						<option value="Draft">Drafts</option>
						<option value="Archive">Archived</option>
					</select>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label">Patient Name:</label>
				<div class="controls">
					<input type="text" placeholder="Patient Name"  name="patientName" value="" />
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="input01">Rx Number:</label>
				<div class="controls">
					<input type="text" placeholder="Rx Number"  name="rxNumber" value="" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Date within:</label>
				<div class="controls ">
					<select name="dateWithin">
						<option value="">--Select--</option>
						<option value="1">Today</option>
						<option value="2">This week</option>
						<option value="3">Last week</option>
						<option value="4">1 month</option>
						<option value="5">2 months</option>
						<option value="6">6 months</option>
						<option value="7">1 year</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-primary"  name="submit" value="Search" />
				</div>
			</div>
        </form>
		</div>
	  </div>
	  <div class="modal-footer">
	   </div>
	</div>