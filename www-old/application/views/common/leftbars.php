<?php
$segment4='';
$segment4=$this->uri->segment(4);
if(!empty($patientData)){
	$segment4=$patientData->status;
}
elseif($segment4!=''){
	$segment4=$segment4;
}
else{
	$segment4='Approval';
}
?>
<div class="well sidebar-nav">
	<ul class="nav nav-list">
		<li class="nav-header">View Rx By Status</li>
		<li  <?php if($segment4=='Approval') echo 'class="active"';?>><a <?php if($segment4=='Approval') echo 'class="leftBarActive"';?>  href="<?=base_url()?>technician/viewmore/index/Approval">Awaiting Approval (<?php if(!empty($countApproval)) echo $countApproval ;else echo '0'; ?>)</a></li>
		<li <?php if($segment4=='Counsel') echo 'class="active"';?> ><a <?php if($segment4=='Counsel') echo 'class="leftBarActive"';?> href="<?=base_url()?>technician/viewmore/index/Counsel">Awaiting Counsel (<?php if(!empty($countCounsel)) echo $countCounsel ;else echo '0'; ?>)</a></li>
		<li <?php if($segment4=='Completed') echo 'class="active"';?> ><a <?php if($segment4=='Completed') echo 'class="leftBarActive"';?> href="<?=base_url()?>technician/viewmore/index/Completed">Completed (<?php if(!empty($countCompleted)) echo $countCompleted ;else echo '0'; ?>)</a></li>
		<li <?php if($segment4=='Cancelled') echo 'class="active"';?> ><a <?php if($segment4=='Cancelled') echo 'class="leftBarActive"';?> href="<?=base_url()?>technician/viewmore/index/Cancelled">Cancelled (<?php if(!empty($countCancelled)) echo $countCancelled ;else echo '0'; ?>)</a></li>
		<li <?php if($segment4=='Draft') echo 'class="active"';?> ><a <?php if($segment4=='Draft') echo 'class="leftBarActive"';?> href="<?=base_url()?>technician/viewmore/index/Draft">Draft (<?php if(!empty($countDraft)) echo $countDraft ;else echo '0'; ?>)</a></li>
		<!-- <li <?php if($segment4=='Archive') echo 'class="active"';?> ><a  <?php if($segment4=='Archive') echo 'class="leftBarActive"';?> href="<?=base_url()?>technician/viewmore/index/Archive">Archived (<?php if(!empty($countArchive)) echo $countArchive ;else echo '0'; ?>)</a></li>
 -->	</ul>
</div>