﻿<?php
$CI=&get_instance();
$CI->load->view('common/header');
$userId = $this->session->userdata('userId');
$storeId = $this->session->userdata('storeId');
?>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamnew.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamInitiator.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/webcamnew.css" />

<script type="text/javascript">
$().ready(function(){

 $("#clickSbmit").hide();
  $("#draftbutton").click(function(){
 	var firstname = $("#firstName").val();
 	var lastName = $("#lastName").val();
 	var rxNumber = $("#rxNumber").val();
 	var drugImage = $("#uploadImage").val();
 	var captureImage=$("#capture_image").val();
 	
 	var submit = 1;
 	if(firstname == "" && submit == 1)
 	{
 		alert("Please enter First Name!");
 		$('#firstName').focus();
 		submit = 0;
 	}
	if(lastName == "" && submit == 1)
	{
		alert("Please enter Last Name!");
		$('#lastName').focus();
 		submit = 0;
	}	
	if(rxNumber == "" && submit == 1)
	{
		alert("Please enter RX Number!");
		$('#rxNumber').focus();
 		submit = 0;
	}
	if((drugImage == null && captureImage == null) && submit == 1)
	{
		alert("Please select or capture drug Image!");
		//$('#lastName').focus();
 		submit = 0;
	}
	if(submit == 1){
		document.addRx.action ="<?=base_url()?>technician/rx/save/Draft";
		$('#addRx').submit();
	}
	});
$("#submitbutton").click(function() { 	 	
 	validate_form(this.form);	
});
$("#drugImage").click(function(){
	$("#webcam").hide();$("#ShowImage").show();
});
 $("#drugImage").change(function(){
	// prepare Options Object 
	 var options = { 
	     url: '<?php echo base_url();?>images.php', 
	     success:    function(data) { 
		    $("#showgallery").prepend('<li id="img"><img  class="thumbnail samllthumb" src="<?=base_url()?>tmpimages/'+data+'"></li>');
		    $("#screen").html('<img class="mainImage" src="<?=base_url()?>tmpimages/'+data+'">');
			$("#allImages").append('<input type="hidden" id="uploadImage" name="uploadImage[]" value="'+data+'"/>');
	     } 
	 }; 
	 // pass options to ajaxForm 
	 $('#addRx').ajaxSubmit(options);
  });
$("#rxNumber").keypress(function(e) {
    if(e.which == 13) {
        getRXData();
    }
});
$('#rxNumber').blur(function() {
 	getRXData();
});

$('input[name=ndc_number]')
	.keyup(updateNDCEvent)
	.change(updateNDCEvent);

});

// store persistent data for looking up drug images
var drugImgRequests = {images: {}, serial: 0};
/**
 * Event handler for changes in the NDC number input
 */
function updateNDCEvent() {
	if ( $("#ndc_number").val().length > 10 )
		getDrugImage($("#ndc_number").val());
}
/**
 * Retrieves the image of the drug for a certain NDC number
 */
function getDrugImage(ndc) {
	drugImgRequests.serial++;
	var serial = drugImgRequests.serial;
	
	if ( drugImgRequests.images[ndc] ) {
		showDrugImage(ndc);
		return;
	}
	
	$('#ndclookupworking').show();
	$.post(
		"<?=base_url()?>technician/rx/ajaxImgLookup",
		{ndc: ndc},
		function(response) {
			if ( serial == drugImgRequests.serial )
				$('#ndclookupworking').hide();
				
			if ( !response || !response.length ) {
				noDrugImage(ndc);
				return;
			}

			drugImgRequests.images[ndc] = response;
			if ( serial == drugImgRequests.serial )
				showDrugImage(ndc);
		}
	);
}
/**
 * Takes a serialized string of drug image filenames and displays them
 */
function showDrugImage(ndc) {
	$('#drugimages')
		.text('Images for NDC #'+ndc)
		.html('<p><strong>'+$('#drugimages').html()+'</strong></p>');

	var images = drugImgRequests.images[ndc];
	images = images.split(';');
	for ( i in images )
		$('#drugimages').append('<img src="'+images[i]+'" />');
}
/**
 * Shows error message if there is no drug image
 */
function noDrugImage(ndc) {
	$('#drugimages')
		.text('There is no image for NDC #'+ndc)
		.html('<p><strong>'+$('#drugimages').html()+'</strong></p>');
}
function getRXData(){
	var rxNumber=$('#rxNumber').val();
	var data ={
			rxNumber: rxNumber,
            ajax : '1'
		};
		var url="<?=base_url()?>technician/rx/getRxData";
		$.ajax({ url: url,timeout:5000, type: 'POST',  async : false, data: data, success: function(message) {
			var json =$.parseJSON(message);
			if(json!="" && json!=null ){
	   			var Dob=json.dob.date;
	   			var sDate= Dob.replace(' 00:00:00','');
	   			var sDate= sDate.replace('-','/');
	   			var sDate= sDate.replace('-','/');
				$('#firstName').val(json.Firstname);
				$('#lastName').val(json.lastname);
				$('#datepicker').val(sDate);

				$('#plname').val(json.plname);
				$('#pfname').val(json.pfname);
				$('#sigcoded').val(json.SigCoded);
				$('#sigexpanded').val(json.Sigexpanded);
				$('#identifer').val(json.identifier);
				$('#npi').val(json.npi);
				$('#pphone').val(json.phone);
				$('#drugDispensed').val(json.description);
				$('#ndc_number').val(json.newndc);
				
				$('#newndc').val(json.newndc);
 				$('#manf').val(json.manf);
				$('#description').val(json.description);
				$('#packagesize').val(json.packagesize);
				$('#DaysSupply').val(json.DaysSupply);
				$('#qtywritten').val(json.qtywritten);
				$('#DispensedQuantity').val(json.DispensedQuantity);
				$('#FillsOwed').val(json.FillsOwed);
				$('#Strength').val(json.Strength);
				$('#refillsAuth').val(json.refill);
				$('#packagesizeunbitcode').val(json.PackageSizeUnitCode);
				updateNDCEvent();
			}
			else{
				$('#firstName').val("");
				$('#lastName').val("");
				$('#datepicker').val("");
				$('#plname').val("");
				$('#pfname').val("");
				$('#sigcoded').val("");
				$('#sigexpanded').val("");
				$('#identifer').val("");
				$('#npi').val("");
				$('#pphone').val("");
				$('#drugDispensed').val("");
				$('#ndc_number').val("");
				$('#newndc').val("");
 				$('#manf').val("");
				$('#description').val("");
				$('#packagesize').val("");
				$('#DaysSupply').val("");
				$('#qtywritten').val("");
				$('#DispensedQuantity').val("");
				$('#FillsOwed').val("");
				$('#Strength').val("");
				$('#refillsAuth').val("");
				$('#packagesizeunbitcode').val("");
			}
			} });
		return false;
}

function validate_form(thisform) {

	with (thisform)
	{
		if (validate_required(firstName,"Please enter First Name!")==false) {
			firstName.focus();
			return false;
		}
		if (validate_required(lastName,"Please enter Last Name!")==false) {
			lastName.focus();
			return false;
		}
		if (validate_required(rxNumber,"Please enter RX Number!")==false) {
			rxNumber.focus();
			return false;
		}
		
var uploadImage = $("#uploadImage").val();
var captureImage = $("#capture_image").val();

		if(uploadImage == null && captureImage == null)
		{
			alert("Please select or capture drug Image!");
			return false;
		}
		if(!document.getElementById('clickSbmit').click())
		{
			return false;
		}
	}
} 
 
function validate_required(field,alerttxt) {
	with (field){
		if (value==null||value=="") {
			alert(alerttxt);
			return false;
		}
		else
		{
			return true;
		}
	}
}
</script>
<script type="text/javascript">
/*----------------------------------
		Setting up the web camera
	----------------------------------*/

	webcam.set_swf_url(baseUrl+'assets/js/webcam.swf');
	webcam.set_api_url(baseUrl+'upload.php');	// The upload script
	webcam.set_quality(80);				// JPEG Photo Quality
	webcam.set_shutter_sound(true, baseUrl+'assets/js/shutter.mp3');
function addImage(data)
{
	$("#showgallery").prepend('<li id="img"><img class="thumbnail samllthumb" src="<?php echo base_url()?>tmpimages/'+data+'"></li>');
		    $("#screen").html('<img class="thumbnail mainImage" src="<?php echo base_url()?>tmpimages/'+data+'">');
			$("#allImages").append('<input type="hidden" id="capture_image" name="capture_image[]" value="'+data+'"/>');
}
</script>
	<form method="post" name="addRx" id="addRx" enctype="multipart/form-data">
		<input type="hidden" name="plname"  id="plname" value="" />
		<input type="hidden" name="pfname" id="pfname" value="" />
		<input type="hidden" name="sigcoded" id="sigcoded" value="" />
		<input type="hidden" name="sigexpanded"  id="sigexpanded" value="" />
		<input type="hidden" name="identifer" id="identifer" value="" />
		<input type="hidden" name="npi" id="npi" value="" />
		<input type="hidden" name="pphone" id="pphone" value="" />
		<input type="hidden" name="newndc" id="newndc" value="" />
		<input type="hidden" name="oldndc" id="oldndc" value="" />
		<input type="hidden" name="manf" id="manf" value="" />
		<input type="hidden" name="description" id="description" value="" />
		<input type="hidden" name="packagesize" id="packagesize" value="" />
		<input type="hidden" name="DaysSupply" id="DaysSupply" value="" />
		<input type="hidden" name="qtywritten" id="qtywritten" value="" />
		<input type="hidden" name="DispensedQuantity" id="DispensedQuantity" value="" />
		<input type="hidden" name="FillsOwed" id="FillsOwed" value="" />
		<input type="hidden" name="Strength" id="Strength" value="" />
		<input type="hidden" name="refillsAuth" id="refillsAuth" value="" />
		<input type="hidden" name="packagesizeunbitcode" id="packagesizeunbitcode" value="" />
		
		<input type="hidden" name="technicianId" value="<?php if(!empty($userId)) {echo $userId;} ?>" />
		<input type="hidden" name="storeId" value="<?php if(!empty($storeId)) {echo $storeId;} ?>" />
		<input type="hidden" name="parentRxId" value="<?php if(!empty($patientData->parentId)){echo $patientData->parentId ;}elseif(!empty($patientData->rxId)){echo $patientData->rxId ;}elseif(!empty($_POST['parentRxId'])){echo $_POST['parentRxId'];} ?>" />
 				<table width="100%" class="span12">
					<tr>
						<td valign="top" class="span3 wellbox">
 								 <div class="control-group">
					            <label class="control-label" for="input01">Rx Number<span class="star">*</span></label>
					              <div class="controls">
					                <input type="text" id ="rxNumber" name="rxNumber"  class="{validate:{required:true }} createRx" title="Please enter RX Number" value="<?php if(!empty($_POST['rxNumber'])){echo $_POST['rxNumber'];} ?>" />               
					               <span  class="returnError" ><?php echo form_error('rxNumber'); ?></span>
					               </div> 
					             </div>
					            <div class="control-group">
					            <label class="control-label" for="input01">First Name<span class="star">*</span></label>
					              <div class="controls">
					                <input type="text" id="firstName"  name="firstName" class="{validate:{required:true , maxlength:250}} createRx" title="Please enter First Name" value="<?php if(!empty($patientData->firstName)){echo $patientData->firstName;} elseif(!empty($_POST['firstName'])){echo $_POST['firstName'];} ?>" />               
					              <span  class="returnError" > <?php echo form_error('firstName'); ?></span>
					               </div> 
					             </div>
					             <div class="control-group">
					            	<label class="control-label" for="input01">Last Name<span class="star">*</span></label>
					              	<div class="controls">
					                	<input type="text" class="createRx" id="lastName" name="lastName" value="<?php if(!empty($patientData->lastName)){echo $patientData->lastName;}elseif(!empty($_POST['lastName'])){echo $_POST['lastName'];} ?>" />               
					               </div> 
					             </div>
					            <div class="control-group">
					            <label class="control-label" for="input01">Date of Birth:</label>
					              <div class="controls">
					                <input type="text"  name="dob"  class="createRx" id="datepicker"  value="<?php if(!empty($patientData->birthDate)){echo $patientData->birthDate;}elseif(!empty($_POST['dob'])){echo $_POST['dob'];} ?>" />               
					               </div> 
					             </div>					            
					            <div class="control-group">
					            <label class="control-label" for="input01">Drug Dispensed</label>
					              <div class="controls">
					                <input type="text" class="createRx"  name="drugDispensed"  id="drugDispensed"  value="<?php if(!empty($_POST['drugDispensed'])){echo $_POST['drugDispensed'];} ?>" />               
					               </div> 
					             </div>
					             <div class="control-group">
					            <label class="control-label" for="input01">NDC #</label>
					              <div class="controls">
					                <input type="text" class="createRx"  name="ndc_number" id="ndc_number"  value="<?php if(!empty($_POST['ndc_number'])){echo $_POST['ndc_number'];} ?>" />               
					               </div> 
					             </div>
					             <div class="controls">
					                <input type="checkbox"  value="1" name="refill" <?php if(!empty($_POST['refill']) && $_POST['refill']=='1'){echo 'checked="checked"';}?> />
					                Refill
					                <br/>
					                <input type="checkbox"  value="<?php if(!empty($highest)){echo ($highest+1);}?>" name="priority" <?php if(!empty($_POST['priority']) && $_POST['priority']=='1'){echo 'checked="checked"';}?> />
					                High Priority
					                <br/>
					                <input type="checkbox"  name="onhold" value="1" <?php if(!empty($_POST['onhold']) && $_POST['onhold']=='1'){echo 'checked="checked"';}?>  />
					                On Hold
					            </div>					            					 
					</td>
				<td valign="top" style="padding:2px;" class="span7">
 					<div id="camera">
						<span class="tooltip"></span>
						<span class="camTop"></span>
					    <div class="buttonPane">
					    		<a id="captureButton" class="blueButton display" href="javascript:void(0);">Capture</a>
					        	<a id="cancelCapture" href="javascript:void(0);" class="blueButton">Cancel</a> 
					        	<a id="shootButton" href="javascript:void(0);" class="greenButton">Shoot</a>
					        	<span class="myui-button myfileupload-buttonbar btn btn-success">
					        		Load from file<input type="file" id="drugImage" name="drugImage">
					        	</span>
					    </div>
					    <div id="buttons">
					        <div class="buttonPane" id="canupload" style="display: none;">
					        	<a id="cancelButton" href="javascript:void(0);" class="blueButton">Cancel</a> 
					        	<a id="uploadButton" href="javascript:void(0);" class="greenButton">Save</a>
					        </div>
					    </div>
					    <div id="screen"></div>
					   <!-- <span class="settings"></span>-->
					</div>
					<div>
				        <div class="row-fluid">
		 		        	<ul id="showgallery" class="catlist1 createRxImg showImage" ></ul>
		 		        	<span id="allImages" class="span12" ></span>
				  	</div>
 				</td>
				<td valign="top" class="pull-right span2">				 
					<textarea   class="commentText" name="comments" placeholder="Add Comments..." ><?php if(!empty($_POST['comments'])){echo $_POST['comments'];} ?></textarea>           
					 <br/>
	             	<input class="btn btn-primary" type="button" id="submitbutton" value="Submit">
	             	<input type="button" class="btn btn-primary" id="draftbutton" value="Draft" />
		            <a class="btn" href="<?php echo base_url();?>technician/rx/index">Cancel</a>
		            <a id="clickSbmit" data-toggle="modal" href="#myModal" ></a> 				 
					<div class="drugpreview" style="margin-top: 20px;">
						<div id="ndclookupworking" style="display: none;">Loading image...</div>
						<div id="drugimages">
						</div>
					</div>
	        	</td>
			</tr>
	</table>
</form>
<!--&nbsp;<span class="myui-button myfileupload-buttonbar btn btn-success">Load from file<input type="file" name="drugImage" id="drugImage"  onchange="readURL(this);"  />--></span>

 <!--------------Modal section-------------------->
<div class="modal hide" id="myModal">
  <div class="modal-header">
    <i class="icon-remove pull-right"data-dismiss="modal" style="margin-top: -7px;cursor:pointer;"></i>
  </div>
  <div class="modal-body">
    <p>This record has been submitted to pharmacist for approval, you would like to proceed?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Cancel</a>
   <!-- <a id="addmore" href="#" onclick="addmore()" class="btn btn-primary">Add more</a>-->
    <a href="#" onclick="document.addRx.action='<?=base_url()?>technician/rx/save/add';document.addRx.submit();" class="btn btn-primary">Add more</a>
    <!--<a id="resubmit" href="#" onclick="resubmit()" class="btn btn-primary">Submit</a>-->
    <a id="resubmit" href="#" onclick="document.addRx.action='<?=base_url()?>technician/rx/save/';document.addRx.submit();" class="btn btn-primary">Submit</a>
  </div>
</div>
<!--------------Modal section-------------------->
<script type="text/javascript">
	$(function() {
		$( "#datepicker" ).datepicker({
			dateFormat: "mm/dd/yy",
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<?php
$CI->load->view('common/footer');
?>