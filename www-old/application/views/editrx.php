<?php
$CI=&get_instance();
$CI->load->view('common/header');
$userId = $this->session->userdata('userId');
$storeId = $this->session->userdata('storeId');

?>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamnew.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamInitiator.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/webcamnew.css" />

<script type="text/javascript"> 
function validate_form(thisform) {

	with (thisform)
	{
		if (validate_required(firstName,"Please enter First Name!")==false) {
			firstName.focus();
			return false;
		}
		if (validate_required(lastName,"Please enter Last Name!")==false) {
			lastName.focus();
			return false;
		}
		if(!document.getElementById('clickSbmit').click())
		{
			return false;
		}
		
	}

}  
function validate_required(field,alerttxt) {
	with (field){
		if (value==null||value=="") {
			alert(alerttxt);
			return false;
		}
		else
		{
			return true;
		}
	}
}

</script>

<script type="text/javascript">

$().ready(function(){

updateNDCEvent(); 

$('input[name=ndc_number]')
	.keyup(updateNDCEvent)
	.change(updateNDCEvent);

	
 $("#clickSbmit").hide();	

 $("#draftbutton").click(function(){
 	
 	var firstname = $("#firstName").val();
 	var lastName = $("#lastName").val();
 	var submit = 1;
 	if(firstname == "" && submit == 1)
 	{
 		alert("Please enter First Name!");
 		$('#firstName').focus();
 		submit = 0;
 	}
	if(lastName == "" && submit == 1)
 	{
 		alert("Please enter Last Name!");
 		$('#lastName').focus();
 		submit = 0;
 	}
	if(submit == 1){
		document.addRx.action ="<?=base_url()?>technician/rx/update/Draft";
		$('#addRx').submit();
	}
		
	});	
	
 $("#resubmitButton").click(function(){
 	
 	var firstname = $("#firstName").val();
 	var lastName = $("#lastName").val();
 	var submit = 1;
 	if(firstname == "" && submit == 1)
 	{
 		alert("Please enter First Name!");
 		$('#firstName').focus();
 		submit = 0;
 	}
	if(lastName == "" && submit == 1)
 	{
 		alert("Please enter Last Name!");
 		$('#lastName').focus();
 		submit = 0;
 	}
	if(submit == 1){
		document.addRx.action='<?=base_url()?>technician/rx/setDraftToApproval';
		document.addRx.submit();
	}
		
	});		
	
$("#submitbutton").click(function() {
 	validate_form(this.form);
});	


$("#drugImage").click(function(){
	$("#webcam").hide();$("#ShowImage").show();
});
 $("#drugImage").change(function(){
	// prepare Options Object 
	 var options = { 
	     url:        '<?php echo base_url();?>images.php', 
	     success:    function(data) { 
 
		    $("#showgallery").prepend('<li id="img"><img class="thumbnail  samllthumb" src="<?=base_url()?>tmpimages/'+data+'"></li>');
		    $("#screen").html('<img class="mainImage" src="<?=base_url()?>tmpimages/'+data+'">');
			$("#allImages").append('<input type="hidden" id="uploadImage" name="uploadImage[]" value="'+data+'"/>');
			
	     } 
	 }; 
	 // pass options to ajaxForm 
	 $('#addRx').ajaxSubmit(options);
  });

});

// store persistent data for looking up drug images
var drugImgRequests = {images: {}, serial: 0};
/**
 * Event handler for changes in the NDC number input
 */
function updateNDCEvent() {
	if ( $("#ndc_number").val().length > 10 )
		getDrugImage($("#ndc_number").val());
}
/**
 * Retrieves the image of the drug for a certain NDC number
 */
function getDrugImage(ndc) {
	drugImgRequests.serial++;
	var serial = drugImgRequests.serial;
	
	if ( drugImgRequests.images[ndc] ) {
		showDrugImage(ndc);
		return;
	}
	
	$('#ndclookupworking').show();
	$.post(
		"<?=base_url()?>technician/rx/ajaxImgLookup",
		{ndc: ndc},
		function(response) {
			if ( serial == drugImgRequests.serial )
				$('#ndclookupworking').hide();
				
			if ( !response || !response.length ) {
				noDrugImage(ndc);
				return;
			}

			drugImgRequests.images[ndc] = response;
			if ( serial == drugImgRequests.serial )
				showDrugImage(ndc);
		}
	);
}
/**
 * Takes a serialized string of drug image filenames and displays them
 */
function showDrugImage(ndc) {
	$('#drugimages')
		.text('Images for NDC #'+ndc)
		.html('<p><strong>'+$('#drugimages').html()+'</strong></p>');

	var images = drugImgRequests.images[ndc];
	images = images.split(';');
	for ( i in images )
		$('#drugimages').append('<img src="'+images[i]+'" />');
}
/**
 * Shows error message if there is no drug image
 */
function noDrugImage(ndc) {
	$('#drugimages')
		.text('No image found for NDC #'+ndc)
		.html('<p><strong>'+$('#drugimages').html()+'</strong></p>');
}
</script>
<script type="text/javascript">
/*----------------------------------
		Setting up the web camera
	----------------------------------*/

	webcam.set_swf_url('../../../assets/js/webcam.swf');
	webcam.set_api_url('../../../upload.php');	// The upload script
	webcam.set_quality(80);				// JPEG Photo Quality
	webcam.set_shutter_sound(true, '../../../assets/js/shutter.mp3');
function addImage(data)
{
	$("#showgallery").prepend('<li id="img"><img class="thumbnail samllthumb" src="<?php echo base_url()?>tmpimages/'+data+'"></li>');
		    $("#screen").html('<img class="thumbnail mainImage" src="<?php echo base_url()?>tmpimages/'+data+'">');
			$("#allImages").append('<input type="hidden" id="capture_image" name="capture_image[]" value="'+data+'"/>');
}
</script>

 	<form method="post" name="addRx" id="addRx" enctype="multipart/form-data">
		<input type="hidden" name="technicianId" value="<?php if(!empty($userId)) {echo $userId;} ?>" />
		<input type="hidden" name="storeId" value="<?php if(!empty($storeId)) {echo $storeId;} ?>" />
		<input type="hidden" name="rxId" value="<?php if(!empty($patientData->rxId)){echo $patientData->rxId;}elseif(!empty($_POST['rxId'])){echo $_POST['rxId'];} ?>" />
				<table width="100%"  class="span12">
					<tr>
						<td valign="top" class="span3 wellbox">
 								 <div class="control-group">
					            <label class="control-label" for="input01">Rx Number<span class="star">*</span></label>
					              <div class="controls">
					                <input type="text" id = "rxNumber" name="rxNumber" readonly="readonly" class="{validate:{required:true }} createRx" title="Please enter RX Number" value="<?php if(!empty($patientData->rxNumber)){echo $patientData->rxNumber;} elseif(!empty($_POST['rxNumber'])){echo $_POST['rxNumber'];} ?>" />               
					               <span  class="returnError" ><?php echo form_error('rxNumber'); ?></span>
					               </div> 
					             </div>
					             
					            <div class="control-group">
					            <label class="control-label" for="input01">First Name<span class="star">*</span></label>
					              <div class="controls">
					                <input type="text" id="firstName"  name="firstName" class="{validate:{required:true , maxlength:250}} createRx" title="Please enter First Name" value="<?php if(!empty($patientData->firstName)){echo $patientData->firstName;} elseif(!empty($_POST['firstName'])){echo $_POST['firstName'];} ?>" />               
					              <span  class="returnError" > <?php echo form_error('firstName'); ?></span>
					               </div> 
					             </div>
					             <div class="control-group">
					            <label class="control-label" for="input01">Last Name<span class="star">*</span></label>
					              <div class="controls">
					                <input type="text" class="createRx" id="lastName" name="lastName" value="<?php if(!empty($patientData->lastName)){echo $patientData->lastName;}elseif(!empty($_POST['lastName'])){echo $_POST['lastName'];} ?>" />               
					               
					               </div> 
					             </div>
					            <div class="control-group">
					            <label class="control-label" for="input01">Date of Birth:</label>
					              <div class="controls">
					                <input type="text"  name="dob"  class="createRx" id="datepicker"  value="<?php if(!empty($patientData->birthDate)){echo date('m/d/Y',strtotime($patientData->birthDate));}elseif(!empty($_POST['dob'])){echo $_POST['dob'];} ?>" />               
					               </div> 
					             </div>
					             
					            
					            <div class="control-group">
					            <label class="control-label" for="input01">Drug Dispensed</label>
					              <div class="controls">
					                <input type="text" class="createRx"  name="drugDispensed"  value="<?php if(!empty($patientData->drugDispensed)){echo $patientData->drugDispensed;} elseif(!empty($_POST['drugDispensed'])){echo $_POST['drugDispensed'];} ?>" />               
					               </div> 
					             </div>
					           
					           <div class="control-group">
					            <label class="control-label" for="input01">NDC #</label>
					              <div class="controls">
					                <input type="text" class="createRx" id="ndc_number"  name="ndc_number"  value="<?php if(!empty($patientData->ndc_number)){echo $patientData->ndc_number;} elseif(!empty($_POST['ndc_number'])){echo $_POST['ndc_number'];} ?>" />               
					               </div> 
					             </div>
					            
					             <div class="controls">
					                <input type="checkbox"  value="1" name="refill" 
					                <?php if(!empty($patientData->refill) && $patientData->refill=='1')
					                {echo 'checked="checked"';} 
					                elseif(!empty($_POST['refill']) && $_POST['refill']=='1')
					                {echo 'checked="checked"';}?> />
					                Refill
					                <br/>
					                <input type="checkbox" value="<?php if(!empty($patientData->priority)){echo $patientData->priority;}elseif(!empty($highest)){echo ($highest+1);}?>" name="priority" <?php if(!empty($patientData->priority) && $patientData->priority!='0'){echo 'checked="checked"';} elseif(!empty($_POST['priority']) && $_POST['priority']!='0'){echo 'checked="checked"';}?> />					               
					                 <!--<input type="checkbox"  value="1" name="priority" <?php if(!empty($patientData->priority) && $patientData->priority!='0'){echo 'checked="checked"';} elseif(!empty($_POST['priority']) && $_POST['priority']!='0'){echo 'checked="checked"';}?> />-->
					                High Priority
					                <br/>
					                <input type="checkbox"  name="onhold" value="1"
					                <?php if(!empty($patientData->status) && $patientData->status=='OnHold')
					                {echo  'checked="checked" disabled="true"';}
									elseif(!empty($_POST['onhold']) && $_POST['onhold']=='1')
					                 {echo 'checked="checked"';}?>  />
					                On Hold
					            </div>
 					</td>
					<td valign="top" style="padding:2px;" class="span7">
						<div id="camera">
							<span class="tooltip"></span>
							<span class="camTop"></span>
						    <div class="buttonPane">
						    		<a id="captureButton" class="blueButton display" href="javascript:void(0);">Capture</a>
						    		<a id="cancelCapture" href="javascript:void(0);" class="blueButton">Cancel</a> 
						        	<a id="shootButton" href="javascript:void(0);" class="greenButton">Shoot</a>
						        	<span class="myui-button myfileupload-buttonbar btn btn-success">
						        		Load from file<input type="file" id="drugImage" name="drugImage">
						        	</span>
						    </div>
						    <div id="buttons">
						        <div class="buttonPane" id="canupload" style="display: none;">
						        	<a id="cancelButton" href="javascript:void(0);" class="blueButton">Cancel</a> 
						        	<a id="uploadButton" href="javascript:void(0);" class="greenButton">Save</a>
						        </div>
						    </div>
						    <div id="screen"></div>
						</div>       
				        
				        <div class="row-fluid ">
	 		        	<ul id="showgallery" class="catlist1 createRxImg showImage" >
	 		        		<?php
				        	if(!empty($capturedImage))
				          	{
				          		for($counter=0; $counter<count($capturedImage);$counter++){
								?>
				          		<li id="img">
								<img class="thumbnail samllthumb" src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>" alt="NoImage"  />
								</li>
				          		<?php
				          		}
				          	}
							if(!empty($drugImage))
				          	{
				          		for($counter=0; $counter<count($drugImage);$counter++){
								?>
				          		<li id="img">
								<img class="thumbnail samllthumb" src="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>" alt="NoImage"  />
								</li>
								<?php
				          		}
				          	}
				          	?>
	 		        	</ul>
	 		        	<span id="allImages" ></span>
				  	</div>
				</td>
				<td valign="top" class="pull-right span2">
					<textarea   class="commentText" name="comments" placeholder="Add Comments..." ></textarea>           
					<br/>
	             	
		            <div class="control-group">
		             <div class="controls">
		             	<?php if($patientData->status=='Draft'||$patientData->status=='Archive' ||$patientData->status=='Rejected'||$patientData->status=='OnHold')
		             	{
		             		?>
		             			<input type="hidden" id="draftArchive" value="<?php if($patientData->status){echo $patientData->status;}?>" />
		             			<input class="btn btn-primary" type="button" id="resubmitButton" value="Re-submit"  />
		             		<?php
		             	}
						else
						{
							?>
								<input class="btn btn-primary" type="button" id="submitbutton" value="Submit" />
			             	<?php	
						}
						 ?>
		             	<?php if($patientData->status!='Draft' && $patientData->status!='Archive' ){ ?><input type="button" class="btn btn-primary" id="draftbutton" value="Draft" /><?php } ?>
		             	<a class="btn" href="<?php echo base_url();?>technician/rx/index" >Cancel</a>
		             	<a id="clickSbmit" data-toggle="modal" href="#myModal" ></a> 
 
							<div class="drugpreview" style="margin-top: 20px;">
								<div id="ndclookupworking" style="display: none;">Loading image...</div>
								<div id="drugimages">
								</div>
							</div>

 
			         </div> 
		         </div>
	        	</td>
			</tr>
	</table>
</form>
</div>



<div class="modal hide" id="myModal">
  <div class="modal-header">
    <i class="icon-remove pull-right"data-dismiss="modal" style="margin-top: -7px;cursor:pointer;"></i>
  </div>
  <div class="modal-body">
    <p>This record has been submitted to pharmacist for approval, you would like to proceed?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal">Cancel</a>
    <a href="#" onclick="document.addRx.action='<?=base_url()?>technician/rx/update/';document.addRx.submit();" class="btn btn-primary">Save</a>
  </div>
</div>


<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			dateFormat: "mm/dd/yy",
			changeMonth: true,
			changeYear: true
		});
	});
	</script>
<?php
$CI->load->view('common/footer');
?>