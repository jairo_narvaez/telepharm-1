<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head>	
  <title>Telepharmacy Solution | Remote Verification | Remote Patient Counseling</title>
 <meta name="google-site-verification" content="cetw6E-mBWFOc6vNE7ocleImJelXGUW8h6b8lAY9Pw0" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
<meta name="keywords" content="google telepharmacy, remote pharmacy, telemedicine">
    <meta name="author" content="Telepharm ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css">
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.7.1.min.js"></script>
<link href="<?php echo site_url();?>css/message.css" rel="stylesheet" type="text/css" media="all" />

<?php 

if($this->session->flashdata('message'))
{?>
	<div id="boxes">
	<div style="top: 20px; left: 551.5px; display: none;" id="dialog" class="window">
	<a href="#" class="close" style="float:right">x</a>
	<br />
	<?php echo $this->session->flashdata('message') ;?>
	</div>
	</div>
<?php 
}
 $message=$this->session->flashdata('message');
?>
 <script src="<?php echo site_url();?>assets/js/jquery.cookie.js"></script>

<script type="text/javascript">
$(document).ready(function() {	
//$.removeCookie('vidoprompt', { path: '/telepharm' });
var message=<?php echo json_encode($message)  ?>;
if(message!='' && message!=null)
{
	var id = '#dialog';
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(500); 	
	$(id).fadeOut(8000);
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		$('.window').hide();
	});		
}
});
</script>
</head>
<body style="background:url(<?php echo base_url();?>images/bg.png) repeat;">

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="brand" href="#">Telepharm</a>
	    <form class="form-inline pull-right" action="<?=base_url()?>index/login" method="post" style="margin:0 !important;">
		  <input type="text" name="userName" class="{validate:{required:true}} input-medium" title="Please enter username" placeholder="Username" />
		  <input type="password" name="password" class="{validate:{required:true}} input-medium" placeholder="Password" title="Please enter password" />
		  <button type="submit" class="btn btn-danger">Sign in</button>
		</form>
    </div>
  </div>
 

</div>
     <div class="container-fluid"  style=" margin-top:60px !important;">
  
      <div class="pull-right footer" ><a style="color:#fff;margin-top:60px !important;font-weight:bold;margin-right:40px;" href="https://www.google.com/landing/chrome/beta/" target="_blank"><b>Download chrome for video comptability</b>&nbsp;<i class="icon-download icon-white"></i></a></div>

 </div>

</body>

 </html>