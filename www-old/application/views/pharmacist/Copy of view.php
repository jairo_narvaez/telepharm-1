<?php
$CI=&get_instance();
$CI->load->view('pharmacist/common/header');
$segment4 = $this->uri->segment(4);
$segment = $this->uri->segment(5);
$userId = $this->session->userdata('userId');
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mglass.css">
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.lightbox-0.5.css" media="screen" />

 <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
<script>
$().ready(function() {
 $('.close').click(function() {
		$('.alert').css('display', 'none');
	});
	
$('.dropdown-toggle').dropdown();
	
});
	function getData()
	{
		var parentRxId=$("#childRxNumber").val();
		var rxId=$("#childRxNumber").val();
		var url="<?=base_url()?>pharmacist/rx/view/"+<?=$this->uri->segment(4)?>;
		if(rxId!="" && rxId!=null)
		{
			url+="/"+rxId;
		}
		document.location.href=url;	
	}
$(document).on("click", ".clickToview", function () {
     //var variable = $(this).data('id1'); 
     //alert(variable);
    $('#myModalNdc').modal('show');
});
</script>


<div class="span7">
 <?php 
if($this->session->flashdata("message"))
{?>
<div class="alert alert-success " ><a class="close" data-dismiss="alert">×</a><?=$this->session->flashdata("message")?></div>
<?php 
}
?>

		<table class="table table-bordered">
			<tbody>
			<tr>
				<th class="viewHeaders">Name</th>
				<th class="viewHeaders">Rx #</th>
				<th class="viewHeaders">Drug</th>
				<th class="viewHeaders">NDC #</th>
				<th class="viewHeaders">DOB</th>
				<th class="viewHeaders">Date/Time</th>
				<!--
				<th class="viewHeaders">Store</th>
				-->
				<th class="viewHeaders">Refill</th>
				<th class="viewHeaders">Filled By</th>
				<?php if ($patientData->status!="Approval"){?><th class="viewHeaders">Approved By</th><?php }?>
				<?php  if(!empty($patientData->priority)){ ?><th class="viewHeaders">&nbsp;</th><?php } ?>
			</tr>
			<tr>
				<!--<td><?php echo $patientData->lastName." ".$patientData->firstName;?></td>-->
				<td><?php echo $patientData->firstName." ".$patientData->lastName;?></td>
				<td><b><?php echo $patientData->rxNumber?></b></td>
				<td><?php echo $patientData->drugDispensed;?></td>
				<td><a class="clickToview"  data-toggle="modal" href="#myModalNdc" data-id1="<?=$patientData->rxNumber?>" ><?php echo $patientData->ndc_number;?></a></td>
				<td><?php echo $patientData->birthDate;?></td>
				<td><?php echo date('d/m/Y h:iA',strtotime($patientData->createdDate));?></td>
				<!--
				<td><?php echo $patientData->storeName;?></td>
				-->
				<td><?php if($patientData->refill=='1'){echo 'Yes';}else {echo 'No';}?></td>
				<td><?php echo $filledBy->name;?>  </td>
				<?php if ($patientData->status!="Approval"){?><td> <?php echo $statusChangedBy->name;?> </td><?php }?>

				<?php if(!empty($patientData->priority)){ ?><td><i class="icon-warning-sign"></td><?php } ?>
		
			</tr>         
			</tbody>
		</table>
 

	</div> <!--  span6 -->
			 
	
		<div class="span3" >
			<?php
				if(!empty($patientData->rxId))
					{$parentRxId=$patientData->rxId;}
				if($segment!="" && !empty($segment))
					{$currentRxId=$segment;}
				else
					{$currentRxId=$parentRxId;}
				if($patientData->status!="Archive"){
				?>
				<ul class="nav nav-pills pull-left">
				  <li class=" dropdown active" id="menu1" >
				    <a  style="width:110px" class="dropdown-toggle liSearch " data-toggle="dropdown"  href="#menu1">I want to... <b class="caret pull-right"></b></a>
				    <ul class="dropdown-menu" >
				    <?php
				   	 if(empty($patientData->priority) && $patientData->priority=="0" && ($patientData->status=="Approval" || $patientData->status=="Counsel"))
					  {
					  	?>
					  	 <li><a href="<?=base_url()?>technician/rx/sethigh/<?php if(!empty($segment4)){echo $segment4;}else{echo $parentRxId;}?>/<?=$currentRxId?>" onclick="return confirm('Are you sure want to set high Rx?');" ><i class="icon-warning-sign"></i>&nbsp;High Priority</a></li>
					  	<?php
					  }
				      if($patientData->status=="Approval")
					  {
					  	?>
					  	 <li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Counsel" onclick="return confirm('Are you sure want to approve Rx?');" ><i class="btn-icon-only icon-ok"></i>&nbsp;Approve Rx</a></li>
					  	  <li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Rejected"  onclick="return confirm('Are you sure want to reject Rx?');" ><i class="btn-icon-only icon-check"></i>&nbsp;Reject Rx</a></li>
					  	<?php
					  }
					   if($patientData->status=="Counsel")
					  {
					      ?>
							   <li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Completed" onclick="return confirm('Are you sure want to complete Rx?');" ><i class="btn-icon-only icon-ok"></i>&nbsp;Complete Counseling</a></li>
							   <li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Refused" onclick="return confirm('Are you sure want to refuse Rx?');" ><i class="icon-check"></i>&nbsp;Refused Counseling</a></li>
							   <li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Cancelled"  onclick="return confirm('Are you sure want to cancel Rx?');" ><i class="icon-remove-sign"></i>&nbsp;Cancel Rx</a></li>
					      <?php
					  }
					  if($patientData->status=="Cancelled")
					  {
					  	?>
					  		<li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Approval" onclick="return confirm('Are you sure want to re-submit Rx?');" ><i class="btn-icon-only icon-tag"></i>&nbsp;Re-submit Rx</a></li>
					  	<?php
					  }
				      ?>
				      <li><a href="<?=base_url()?>pharmacist/rx/change/<?=$currentRxId?>/Archive" onclick="return confirm('Are you sure want to archive Rx?');" ><i class="btn-icon-only icon-remove"></i>&nbsp;Archive Rx</a></li>
				    </ul>
				  </li>
				</ul>
				<?php }	 ?>
				<select name="childRxNumber" id="childRxNumber"  onchange="javascript:getData();"  style="width:78px;" >
						<?php if(!empty($childRx)){
						foreach($childRx  as $Rx)
						{
							?>
								<option value="<?=$Rx->childRxId?> "<?php if(!empty($segment) && $segment==$Rx->childRxId){echo 'selected="selected"';}?> ><?=$Rx->childRxNumber?></option>
							<?php
						}
					}
					?>
					</select>


					<br>
					<form method="post" >
 				<input type="hidden" name="commentRxId" value="<?php  echo  $patientData->rxId ;?>"/>
				<input type="hidden" name="userId" value="<?php echo $userId ;?>"/>
				<input type="text" name="comments"  class="viewCommentText" placeholder="Add Comments..." />
			</form> 

			<br>
			 <div class="commentblock">
        	<div><h3 class="label label-info">Comments</h3></div>
             <div class="recent">
	          	 <?php  if(!empty($comments)){?>
	          	 	<ul class="recentnew">
	          	 <?php
				 	foreach($comments as $comment)
					{
					?>
						<li><b><?=$comment->fullname."</b> on ". $comment->date."<br><i>".$comment->comments?></i></li>
					<?php }?>
					<li class="viewRecentMore"><a  id="clickSbmit" data-toggle="modal" href="#myModalComm" >view more..</a></li> 
				</ul>
			<?php
			 }
			else {echo "No comments were found";}?>
			</div>
  	 	</div>


		</div>
				
		  <!-- <div class="span2" >
			<div class="control-group">
				<div class="controls">
					<select name="childRxNumber" id="childRxNumber"  onchange="javascript:getData();">
						<?php if(!empty($childRx)){
						foreach($childRx  as $Rx)
						{
							?>
								<option value="<?=$Rx->childRxId?> "<?php if(!empty($segment) && $segment==$Rx->childRxId){echo 'selected="selected"';}?> ><?=$Rx->childRxNumber?></option>
							<?php
						}
					}
					?>
					</select>
				</div>
			</div>
		</div>   -->
			 
<!--</div>-->
<!--  span 10 -->
<!--<div class="span10" >-->
<div class="row-fulid">
  <div class="span12">

 		<div class="control-group">
          <div class="controls ">
          	
          	<ul class="catlist showImage"  id="gallery">
          	<?php
        	if(!empty($capturedImage))
          	{
          		for($counter=0; $counter<count($capturedImage);$counter++){
				?>
          		<li>
					<a rel="lightbox" href="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>">
						<img height="100" width="120" class="thumbnail" src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>" alt="NoImage"  />
					</a>
 				</li>
          		<?php
          		}
          	}
			if(!empty($drugImage))
          	{
          		for($counter=0; $counter<count($drugImage);$counter++){
				?>
          		<li>
				<a rel="lightbox" href="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>"><img height="100" width="120" class="thumbnail" src="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>" alt="NoImage"  /></a>
				</li>
           		<?php
          		}
          	}
          	?>
          	</ul>
             
           </div> 
        </div>
	
	</div>

 	</div><!-- row-fulid -->
	<div class="span3">
	
       
      
	</div>
<!--</div>-->

<!--<div class="span10">-->
<div class="span6" id="actTechBlock">
		
		<div class="actBlock actTechBlock1">
		<a  data-toggle="modal" href="#myModalAct" class="pull-right"><i class="icon-resize-full icon-white"></i></a>
		<?php 
		if(!empty($activities)) { ?>
			<ul class="recent">
				  <?php foreach($activities as $activity) {?>
				  <li><?=$activity->string;?></li>
				  <?php } ?>
			</ul>
		<?php } else echo "No activities found"; ?>
	</div>
	
</div>
<!--</div>-->
<!----Modal section start---->

<div class="modal hide widthSetting" id="myModalNdc">
	<div class="modal-header" id="modal-header" >
	<button type="button" class="close marginTop" data-dismiss="modal">x</button>
	
	</div>
	<div class="modal-body">
   <img src="<?=base_url()?>images/an441.jpeg" alt=""/>
  </div>
</div>
	
<div class="modal hide" id="myModalComm">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Comments</h3>
  </div>
  <div class="modal-body">
  	 		<div class="recent">
	          	 <?php  if(!empty($commentsMore)){?>
	          	 	<ul class="recent">
	          	 <?php
				 	foreach($commentsMore as $comment)
					{?>
						<li><B><?=$comment->fullname."</B> on ". $comment->date."<br><i>".$comment->comments?></i></li>
					<?php }?>					
				</ul>
			<?php
			 }
			else {echo "No comments were found";}?>
			</div>  
  </div>
  <div class="modal-footer">
   </div>
</div>
<div class="modal hide" id="myModalAct">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Recent Activities</h3>
            </div>

  <div class="modal-body">
   <div class="recentblock">	
		<?php if(!empty($activitiesMore)) { ?>
			<ul class="recent">
	              <?php foreach($activitiesMore as $activity) {?>
	              <li><?=$activity->string?></li>
	              <?php } ?>
	           
	        </ul>
		<?php } else echo "No activities found"; ?> 	 
	</div>
  </div>
  <div class="modal-footer">
   </div>
</div>
 <!----Modal section end------->
 <!-- for video conferencing ---------------->
 <?php 
 if(!empty($videoConf)){?>
<!-- <script src="<?php echo base_url();?>video/js/TB.min.js" type="text/javascript" charset="utf-8"></script>
 -->
 <script src='http://static.opentok.com/v0.92-alpha/js/TB.min.js' type='text/javascript'></script>   

<div class="span11" id="closeConf" style="display: none;">
 	<form name="closeVideoForm" method="post" action="<?php echo base_url();?>pharmacist/rx/closeVideoConferencing/<?php echo $patientData->rxId;?>/<?php echo $videoConf[0]->id;?>">
 		<input type="submit" name="closeConf" id="closeConf" value="Close Conference" class="btn btn-primary" />
 	</form>
 </div>
 <div class="span11" id="videoConf">
	 <div id="publisher" style="float: left;">
		<div id="mypublisher">
		</div>
	</div>
	<div id="subscriber" style="margin-left: 20px;float: left"></div>
</div>
<script type="text/javascript">
	$.ajax({
			url : '<?php echo base_url();?>pharmacist/rx/deletepharmaqueue/<?php echo $videoConf[0]->id;?>',
			success : function(msg) {
			}
	});
	$("#videoConf").addClass("loading");
		var apiKey = '<?php echo $this->config->item("API_KEY");?>';
		var sessionId = '<?php echo $videoConf[0]->sessionId;?>';
		var token = "<?php echo $videoConf[0]->tokenid;?>";
		var session = TB.initSession(sessionId);
		session.addEventListener('sessionConnected', sessionConnectedHandler);
		session.addEventListener('streamCreated', streamCreatedHandler);
		session.connect(apiKey, token);
		 TB.setLogLevel(TB.DEBUG);
		function sessionConnectedHandler(event) {
		    session.publish("mypublisher");
		    $("#closeConf").show();
		    $("#videoConf").removeClass("loading");
		 	for (var i = 0; i < event.streams.length; i++) {
		  		if (session.connection.connectionId!=event.streams[i].connection.connectionId) {
		     		subscribeToStreams(event.streams[i]);
		    	}
		 	 }
		}
		 
		function streamCreatedHandler(event) {
			for (var i = 0; i < event.streams.length; i++) {
		  		if (session.connection.connectionId!=event.streams[i].connection.connectionId) {
		     		subscribeToStreams(event.streams[i]);
		    	}
		  	}
		}
		 
		function subscribeToStreams(stream) {
			var div=document.createElement("div");
			div.setAttribute('id','stream-'+stream.streamId);
			$("#subscriber").append(div);
			 session.subscribe(stream,div.id);
		}
		</script><?php
}?>
 <!----------- end ------------------------------->
<?php
$CI->load->view('pharmacist/common/footer');
?>