<?php
$CI=&get_instance();
$CI->load->view('pharmacist/common/header');
$segment4='';
$segment4=$this->uri->segment(4);
?>
<style>
.clickable{cursor:pointer;}
</style>
<script type="text/javascript">
		jQuery( function($) {
			$('tbody tr[data-href]').addClass('clickable').click( function() {
				window.location = $(this).attr('data-href');
			}).find('a').hover( function() {
				$(this).parents('tr').unbind('click');
			}, function() {
				$(this).parents('tr').click( function() {
					window.location = $(this).attr('data-href');
				});
			});
		});
	</script>
<script type="text/javascript">
$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
}); 
</script> 
<script type="text/javascript">
		jQuery( function($) {
			$('tbody tr[data-href]').addClass('clickable').click( function() {
				window.location = $(this).attr('data-href');
			}).find('a').hover( function() {
				$(this).parents('tr').unbind('click');
			}, function() {
				$(this).parents('tr').click( function() {
					window.location = $(this).attr('data-href');
				});
			});
		});
	</script>
<script type="text/javascript">

$(document).on("click", ".open-AddBookDialog", function () {
     var myBookId = $(this).data('id');
     var values=myBookId.split("-");
     $(".modal-body #rxId").val( values[0] );
      $(".modal-body #storeId").val( values[1] );
    $('#myModalReject').modal('show');
});


$(document).on("click", ".open-AddBookDialogApprove", function () {
	
     var myBookId = $(this).data('id1');
     var values=myBookId.split("-");    
     $(".modal-body #rxId1").val( values[0] );
      $(".modal-body #storeId1").val( values[1] );
    $('#myModalApprove').modal('show');
});

$(document).on("click", ".open-AddBookDialogCompleted", function () {
	
     var myBookId = $(this).data('id2');
     var values=myBookId.split("-");    
     $(".modal-body #rxId2").val( values[0] );
      $(".modal-body #storeId2").val( values[1] );
    $('#myModalCompleted').modal('show');
});

$(document).on("click", ".open-AddBookDialogCancelled", function () {
	
     var myBookId = $(this).data('id3');
     var values=myBookId.split("-");    
     $(".modal-body #rxId3").val( values[0] );
      $(".modal-body #storeId3").val( values[1] );
    $('#myModalCancelled').modal('show');
});

$(document).on("click", ".open-AddBookDialogDelete", function () {
	
     var myBookId = $(this).data('id4');
     var values=myBookId.split("-");    
     $(".modal-body #rxId4").val( values[0] );
      $(".modal-body #storeId4").val( values[1] );
    $('#myModalDelete').modal('show');
});

$(document).on("click", ".open-AddBookDialogResubmit", function () {
	
     var myBookId = $(this).data('id5');
     var values=myBookId.split("-");    
     $(".modal-body #rxId5").val( values[0] );
      $(".modal-body #storeId5").val( values[1] );
    $('#myModalResubmit').modal('show');
});


</script>  
<?php 
	$seg=$this->uri->segment(4);   
	
	if($this->session->flashdata('message'))
	{?>		
		<div class="alert alert-success" style="width: 940px !important;"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>		
	<?php 
	}
?> 
<!-- <meta http-equiv="refresh" content="600"> 
 --><div class="span3">
	<?php $CI->load->view('pharmacist/common/leftBar.php'); ?>
</div>	
<div class="span9">		
		<table class="table table-bordered">
		<thead>
			<th class="viewHeaders">&nbsp;</th>
			<th class="viewHeaders">Rx #</th>
			<th class="viewHeaders">Patient Name</th>
			<th class="viewHeaders" >DOB</th>
			<th class="viewHeaders">Date/Time</th>
			<?php if(!empty($allPrescription) && $allPrescription[0]->status=='Counsel'){?> 					
				<th class="viewHeaders">Approved By</th>
			<?php }elseif(!empty($allPrescription) && $allPrescription[0]->status=='Completed'){?>
				<th class="viewHeaders">Completed By</th> <?php }?>
			<th class="viewHeaders">Store</th>					
			<th class="viewHeaders">Options</th>
		</thead>
		<tbody>
			<?php
			if(!empty($allPrescription))
			{
				$count=0;	
				foreach($allPrescription as $prescription)
				{
					?>
					<tr class="<?php if($prescription->status=='Approval' && !empty($prescription->priority) ) {}elseif($prescription->status=='Rejected'){ echo 'highprioRow';}?> clickable" data-href="<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>" >	
					<td><?php
						if(!empty($prescription->priority)  && $prescription->status=='Counsel')
						{?><i class="icon-warning-sign pull-left"></i><?php }
						elseif(!empty($prescription->priority)  && ($prescription->status=='Approval' || $prescription->status=='Rejected'))
						{?><i class="icon-warning-sign pull-left "></i><?php }
						 ?></td>
						<td><?=$prescription->rxNumber?></td>
					<td>
						<span class="patientName">
						<?php
						echo $prescription->firstname." ".$prescription->lastname;
						?><span></td>
				
					<td><?=$date=date('m/d/Y',strtotime($prescription->birthDate))?></td>
					<td><?=$date=date('m/d/Y h:ia',strtotime($prescription->updateDate))?></td>
					
					<?php
					 if(!empty($allPrescription) && $allPrescription[0]->status=='Counsel')
					 {
					 	?> 					
						<td><?=$prescription->fname." ".$prescription->lname?></td>
						<?php
					 }
					 elseif(!empty($allPrescription) && $allPrescription[0]->status=='Completed')
					 {
					 	?>
					 		<td><?=$prescription->fname." ".$prescription->lname?></td>
					 	<?php
					 }
					?>
					<td><?=$prescription->storeName?></td>
					<td><a href="<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>"  title="Click here to view Rx"><i class="icon-search"></i></a></td>
												
					</tr>
					<?php
			$count++;
			}					
		}
		else{ 
			?>
			<tr>
				<td  colspan="<?php if(!empty($allPrescription) && ($allPrescription[0]->status=='Counsel' || $allPrescription[0]->status=='Completed') )echo '8';else echo '7'; ?>">no record found</td>
			</tr>
		<?php  } ?>		 
		</tbody>
		</table>
	
 

	<div class="pagination pull-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>

</div>



<!-- <div class="row-fluid">
<div class="span2"></div>	
<div class="span6">
	
	<div class="recentblock">
		<a id="clickSbmit" data-toggle="modal" href="#myModal" class="pull-right"><i class="icon-resize-full icon-white"></i></a>
		<?php if(!empty($activities)) { ?>
			<ul class="recent">
	              <?php foreach($activities as $activity) {?>
	              <li><?=$activity->string?></li>
	              <?php } ?>
	        </ul>
		<?php } else echo "No activities found"; ?>
	</div>
</div>

</div>
 -->

 

<?php
$CI->load->view('pharmacist/common/footer');
?>
