</div>
<hr>

      <footer>
        <p>&copy; Telepharm <?php echo date('Y');?></p>
      </footer>
 
    </div>

    <!-- <script src="<?php echo site_url();?>assets/js/bootstrap-transition.js"></script>
 -->    <script src="<?php echo site_url();?>assets/js/bootstrap-modal.js"></script>
    <script src="<?php echo site_url();?>assets/js/bootstrap-dropdown.js"></script>
	<script src="<?php echo site_url();?>assets/js/bootstrap-lightbox.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/js/jquery_notification_v.1.js"></script>
	<link href="<?php echo site_url();?>assets/css/jquery_notification.css" rel="stylesheet">
	<link href="<?php echo site_url();?>assets/css/loader.css" rel="stylesheet">
	 <script src="<?php echo site_url();?>assets/js/jquery.jplayer.min.js"></script>

<div id="notiAudio" style="visibility: hidden">
</div>
<?php
	if(empty($videoConf)){?>
		<script type="text/javascript">
			var timerId = 0;
			var timerId1 = 0;
			var timerId2 = 0;
			
			/* to check for active conference */
			timerId = setInterval(function() {
			     checkVideoConferencing();
			}, 5000);
			/* end */
			
			 /* to play audio */
			playNotiAudio();
			timerId1 = setInterval(function() {
					if ($('div#info_message').is(':visible'))
							$("#notiAudio").jPlayer("play");
				}, 2000);
			/* end */
			
			function checkVideoConferencing(){ 
					$.ajax({
							url : '<?php echo base_url();?>pharmacist/rx/checkVideoConferencing',
							success : function(msg) {
								if(msg != "error"){
									var strarray =msg.split("-");
									showNotification({
			                            message: "<a href='<?php echo base_url();?>pharmacist/rx/view/"+strarray[0]+"'>Victor store is calling...</a>",
			                            type : "information" // type of notification is error/success/warning/information,
			                        });
								}
								else{
									if ($('div#info_message').is(':visible'))
											$('div#info_message').remove();
								}
							}
					});
			}
			
			/* to initialize audio file */
			function playNotiAudio(){
				$("#notiAudio").jPlayer({
			      ready: function () {
			        $(this).jPlayer("setMedia", {
			        mp3: "<?php echo base_url();?>assets/mp3/bonk.mp3",
			        });
			      },
			      	swfPath: "<?php echo base_url();?>assets/js/",
					supplied: "m4a,mp3"
			    });
			}
			/* end */
			
		</script><?php
	}?>
  </body>
</html>