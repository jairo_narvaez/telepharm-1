</div>
</div>
<hr>

      <footer>
        <p>&copy; Telepharm <?php echo date('Y');?></p>
      </footer>
     </div>

    <!-- <script src="<?php echo site_url();?>assets/js/bootstrap-transition.js"></script>
 -->    <script src="<?php echo site_url();?>assets/js/bootstrap-modal.js"></script>
    <script src="<?php echo site_url();?>assets/js/bootstrap-dropdown.js"></script>
	<script src="<?php echo site_url();?>assets/js/bootstrap-lightbox.js"></script>
	<script type="text/javascript" src="<?php echo site_url();?>assets/js/jquery_notification_v.1.js"></script>
	<link href="<?php echo site_url();?>assets/css/jquery_notification.css" rel="stylesheet">
	<link href="<?php echo site_url();?>assets/css/loader.css" rel="stylesheet">
	 <script src="<?php echo site_url();?>assets/js/jquery.jplayer.min.js"></script>
 
<div id="notiAudio" style="visibility: hidden"></div>
<div id="newRx" style="visibility: hidden"></div>
<div id="patientChat" class="modal hide">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h3>Advanced Search</h3>
    </div>
    <div class="modal-body">
    </div>
    <div class="modal-footer">
	</div>
</div> 
<?php
	if(empty($videoConf)){?>
		<script type="text/javascript">
			var timerId = 0;
			var timerId1 = 0;
			var timerId2 = 0;
			
			/* to check for active conference */
			timerId = setInterval(function() {
			     checkVideoConferencing();
			}, 5000);
			/* end */
			
			 /* to play audio */
			playNotiAudio();
			timerId1 = setInterval(function() {
					if ($('div#info_message').is(':visible'))
							$("#notiAudio").jPlayer("play");
				}, 2000);
			/* end */
			
			function checkVideoConferencing(){ 
					$.ajax({
							url : '<?php echo base_url();?>pharmacist/rx/checkVideoConferencing',
							success : function(msg) {
								if(msg != "error"){
									var strarray =msg.split("-");
									if(strarray[0] != "0")
										showNotification({
				                            message: "<a href='<?php echo base_url();?>pharmacist/rx/view/"+strarray[0]+"'>"+strarray[2]+" store is calling...</a>",
				                            type : "information" // type of notification is error/success/warning/information,
				                        });
				                    else if(strarray[2])
				                    	showNotification({
				                            message: "<a href='<?php echo base_url();?>pharmacist/rx/patientCall'>"+strarray[2]+" store is calling...</a>",
				                            type : "information" // type of notification is error/success/warning/information,
				                        });
								}
								else{
									if ($('div#info_message').is(':visible'))
											$('div#info_message').remove();
								}
							}
					});
			}
			
			/* to initialize audio file */
			function playNotiAudio(){
				$("#notiAudio").jPlayer({
			      ready: function () {
			        $(this).jPlayer("setMedia", {
			        mp3: "<?php echo base_url();?>assets/mp3/bonk.mp3",
			        });
			      },
			      	swfPath: "<?php echo base_url();?>assets/js/",
					supplied: "m4a,mp3"
			    });
			}
			/* end */
			
			function setCall(call){
				$.ajax({
					url : '<?php echo base_url();?>pharmacist/rx/setcall/'+call,
					success : function(msg) {
						if(call == "1")
						{
							//$("#callOff").removeClass("btn-warning");
							//$("#callOn").addClass("btn-warning");
							$('#iphonecheck').attr("checked","checked");
							
						}
						else
						{
							//$("#callOn").removeClass("btn-warning");
							//$("#callOff").addClass("btn-warning");
							$('#iphonecheck').removeAttr("checked");
								
							}
						}
				});
			}
			
		</script><?php
	}?>

<script src="<?php echo site_url();?>assets/js/crx.js"></script>
<script type="text/javascript">
	var scripturl1 = "<?php echo base_url();?>pharmacist/rx/getnotification/<?php echo $this->session->userdata('storeId');?>";
	var $notifydiv=setInterval(function() {
		Notifyloader();
	}, 15000);



/* to get beep on new RX for approval */
	function setbeep(){
		$("#newRx").jPlayer({
		      ready: function () {
		        $(this).jPlayer("setMedia", {
		        mp3: "<?php echo base_url();?>assets/mp3/newrx_telephone.mp3",
		        });
		      },
		      	swfPath: "<?php echo base_url();?>assets/js/",
				supplied: "m4a,mp3"
		    });
	}
	setbeep();
	timerId = setInterval(function() {
	     getNewRX();
	}, 7000);
	function getNewRX(){ 
		$.ajax({
				url : '<?php echo base_url();?>pharmacist/viewmore/getNewRX',
				success : function(msg) {
					if(msg != "error"){
					    $("#newRx").jPlayer("play");
					}
				}
			});
	}

/* end */
 </script>
</body>
</html>