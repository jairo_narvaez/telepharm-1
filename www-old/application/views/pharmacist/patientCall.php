<?php
$CI=&get_instance();
$CI->load->view('pharmacist/common/header');
?>
<br />
<br />
 <!-- for video conferencing ---------------->
 <?php 
  if(!empty($videoConf)){?>
<!-- <script src="<?php echo base_url();?>video/js/TB.min.js" type="text/javascript" charset="utf-8"></script>
 
 <script src='http://static.opentok.com/v0.92-alpha/js/TB.min.js' type='text/javascript'></script>   
-->
<script src='http://static.opentok.com/v0.92-alpha/js/TB.min.js' type='text/javascript'></script>   

 <div class="span11" id="closeConf" style="display: none;">
 	<form name="closeVideoForm" method="post" action="<?php echo base_url();?>pharmacist/rx/closeVideoPatientCall/<?php echo $videoConf[0]->id;?>">
 		<input type="submit" name="closeConf" id="closeConf" value="Close Conference" class="btn btn-primary" />
 	</form>
 </div>
 <div class="span11" id="videoConf">
	 <div id="publisher" style="float: left;">
		<div id="mypublisher">
		</div>
	</div>
	<div id="subscriber" style="margin-left: 20px;float: left"></div>
</div>
<script type="text/javascript">
	$.ajax({
			url : '<?php echo base_url();?>pharmacist/rx/deletepharmaqueue/<?php echo $videoConf[0]->id;?>',
			success : function(msg) {
			}
	});
	$("#videoConf").addClass("loading");
		var apiKey = '<?php echo $this->config->item("API_KEY");?>';
		var sessionId = '<?php echo $videoConf[0]->sessionId;?>';
		var token = "<?php echo $videoConf[0]->tokenid;?>";
		var session = TB.initSession(sessionId);
		session.addEventListener('sessionConnected', sessionConnectedHandler);
		session.addEventListener('streamCreated', streamCreatedHandler);
		session.connect(apiKey, token);
		 TB.setLogLevel(TB.DEBUG);
		function sessionConnectedHandler(event) {
		    session.publish("mypublisher");
		    $("#closeConf").show();
		    $("#videoConf").removeClass("loading");
		 	for (var i = 0; i < event.streams.length; i++) {
		  		if (session.connection.connectionId!=event.streams[i].connection.connectionId) {
		     		subscribeToStreams(event.streams[i]);
		    	}
		 	 }
		}
		 
		function streamCreatedHandler(event) {
			for (var i = 0; i < event.streams.length; i++) {
		  		if (session.connection.connectionId!=event.streams[i].connection.connectionId) {
		     		subscribeToStreams(event.streams[i]);
		    	}
		  	}
		}
		 
		function subscribeToStreams(stream) {
			var div=document.createElement("div");
			div.setAttribute('id','stream-'+stream.streamId);
			$("#subscriber").append(div);
			 session.subscribe(stream,div.id);
		}
		
		checklivesession('<?php echo $videoConf[0]->id;?>');
		function checklivesession(confId)
		{
			$.ajax({
					url : baseUrl+'pharmacist/rx/checklivesession/'+confId,
					success : function(msg) {
						if(msg == "error")
						{
							location.reload();
						}
						else
						{
							setTimeout(function(){checklivesession(confId);},10000);
						}
					}
			});
		}
		
		</script><?php
}
else
{?>
	<div class="span11" id="videoConf">
		 <h1>No live conference ...</h1>
	</div><?php
}
?>
 <!----------- end ------------------------------->
<?php
$CI->load->view('pharmacist/common/footer');
?>