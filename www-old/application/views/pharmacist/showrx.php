<?php
$CI=&get_instance();
$CI->load->view('pharmacist/common/header');
?>  

<script type="text/javascript">
		jQuery( function($) {
			$('tbody tr[data-href]').addClass('clickable').click( function() {
				window.location = $(this).attr('data-href');
			}).find('a').hover( function() {
				$(this).parents('tr').unbind('click');
			}, function() {
				$(this).parents('tr').click( function() {
					window.location = $(this).attr('data-href');
				});
			});
		});
	</script>
<script type="text/javascript">

$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
	

});
</script>

<?php  
	if($this->session->flashdata('message'))
	{?>
		<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
	<?php 
	}
 ?>
 <meta http-equiv="refresh" content="600"> 
<div class="row-fluid">
	
<div class="span8">
	<ul class="nav nav-tabs" id="myTab " >
		<li class="active"><a data-toggle="tab" href="#approval"> Awaiting Approval (<?php if(!empty($countApproval)) echo $countApproval ;else echo '0'; ?>)</span></a></li>
		<li><a data-toggle="tab" href="#counsel">Awaiting Counsel (<?php if(!empty($countCounsel)) echo $countCounsel ;else echo '0'; ?>)</span></a></li>
		<li><a data-toggle="tab" href="#completed">Completed (<?php if(!empty($countCompleted)) echo $countCompleted ;else echo '0'; ?>)</span></a></li>
		<li><a data-toggle="tab" href="#cancelled">Cancelled (<?php if(!empty($countCancelled)) echo $countCancelled ;else echo '0'; ?>)</span></a></li>
		<li><a data-toggle="tab" href="#archive">Archived (<?php if(!empty($countArchive)) echo $countArchive ;else echo '0'; ?>)</span></a></li>
	</ul>
<div class="tab-content" id="myTabContent">	
	<div id="approval" class="tab-pane  fade  active in">
		<h4>Awaiting Approval</h4>
      <p>      	
  		<table class="dashboardtable table-bordered">
  			<thead>
 				<th>Rx #</th>
 				<th>Patient Name</th>
 				<th>Date/Time</th>
				<th>Action</th>
 			</thead>
			<tbody>
				<?php
				if(!empty($approvalPrescription) || !empty($lowApproval))
				{
					if(!empty($approvalPrescription))
					foreach($approvalPrescription as $approval)
					{
						?>
						<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$approval->rxId?>" <?php if($approval->status=='Rejected') {echo 'class="rejetcedRow"';}?> >
							<td><?=$approval->rxNumber?></td>
							<td><?=$approval->firstName." ".$approval->lastName?></td>
							<td>
							<?php
								$date=date('m/d/Y h:ia',strtotime($approval->updateDate));
								echo $date ;
							?><i class="icon-warning-sign pull-right"></i></td></tr><?php
					}
					if(!empty($lowApproval))
					foreach($lowApproval as $approval)
					{
						?>
						<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$approval->rxId?>" <?php if($approval->status=='Rejected') {echo 'class="rejetcedRow"';}?> >
							<td><?=$approval->rxNumber?></td>
							<td><?=$approval->firstName." ".$approval->lastName?></td>
							<td>
							<?php
								$date=date('m/d/Y h:ia',strtotime($approval->updateDate));
								echo $date ;
							?></td><td><a href="google.com">test</a></td></tr><?php
					}
					?>
					<tr>
						<td colspan="3"><a href="<?=base_url()?>pharmacist/viewmore/index/1" class="pull-right">view more..</a></td>
					</tr>
					<?php
				}
				else {?> <tr> <td colspan="3">no record found</td></tr><?php }
				?>               
			</tbody>
		</table>
	</p>
    </div>
    
    <div id="counsel" class="tab-pane fade">
    		<h4>Awaiting Counsel</h4>
             <p>
	     		<table class="dashboardtable table-bordered" style="height: 0px">
	     			<thead>
	     				<th>Rx #</th>
	     				<th>Patient Name</th>
	     				<th>Date/Time</th>
	     			</thead>
					<tbody>
					<?php
					if(!empty($counselPrescription) || !empty($lowCounsel))
					{
						if(!empty($counselPrescription))
						foreach($counselPrescription as $counsel)
						{
							?>
							<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$counsel->rxId?>">
								<td><?=$counsel->rxNumber?></td>
								<td><?=$counsel->firstName." ".$counsel->lastName?></td>
								<td>
									<?php
										$date=date('m/d/Y h:ia',strtotime($counsel->updateDate));
										echo $date ;
							?><i class="icon-warning-sign pull-right"></i></td></tr><?php
						}
						if(!empty($lowCounsel))
						foreach($lowCounsel as $counsel)
						{
							?>
							<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$counsel->rxId?>">
								<td><?=$counsel->rxNumber?></td>
								<td><?=$counsel->firstName." ".$counsel->lastName?></td>
								<td>
									<?php
										$date=date('m/d/Y h:ia',strtotime($counsel->updateDate));
										echo $date ;
							?></td></tr><?php
						}
						?>
						<tr>
							<td colspan="3"><a href="<?=base_url()?>pharmacist/viewmore/index/2" class="pull-right">view more..</a></td>
						</tr>
						<?php
					}else{?> <tr> <td colspan="3">no record found</td></tr>		 <?php }?>          
					</tbody>
				</table>
			</p>
            </div>
            
<div id="completed" class="tab-pane fade">
		<h4>Completed Rx</h4>
      <p>
      		<table class="dashboardtable table-bordered">
      			<thead>
     				<th>Rx #</th>
     				<th>Patient Name</th>
     				<th>Date/Time</th>
     			</thead>
				<tbody>
					<?php
					if(!empty($completedPrescription))
					{
						foreach($completedPrescription as $completed)
						{
							?>
							<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$completed->rxId?>">
								<td><?=$completed->rxNumber?></td>
								<td><?=$completed->firstName." ".$completed->lastName?></td>
								<td>
									<?php
										$date=date('m/d/Y h:ia',strtotime($completed->updateDate));
										echo $date ;
										if($completed->priority=='1')
										{
											?><i class="icon-warning-sign pull-right"></i><?php
										}
									?>
								</td>
							</tr>
							<?php
						}
						?>
						<tr>
								<td colspan="3"><a href="<?=base_url()?>pharmacist/viewmore/index/3" class="pull-right">view more..</a></td>
						</tr>
						<?php
					}else{ ?>
						<tr> <td colspan="3">no record found</td></tr>		
					<?php }
					?>               
				</tbody>
			</table>
	</p>
    </div> 
    <div id="cancelled" class="tab-pane fade">
		<h4>Cancelled Rx</h4>
      <p>
      		<table class="dashboardtable table-bordered">
      			<thead>
     				<th>Rx #</th>
     				<th>Patient Name</th>
     				<th>Date/Time</th>
     			</thead>
				<tbody>
					<?php
					if(!empty($cancelledPrescription))
					{
						foreach($cancelledPrescription as $cancelled)
						{
							?>
							<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$cancelled->rxId?>">
								<td><?=$cancelled->rxNumber?></td>
								<td><?=$cancelled->firstName." ".$cancelled->lastName?></td>
								<td>
									<?php
										$date=date('m/d/Y h:ia',strtotime($cancelled->updateDate));
										echo $date ;
										if($cancelled->priority=='1')
										{
											?><i class="icon-warning-sign pull-right"></i><?php
										}
									?>
								</td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td colspan="3"><a href="<?=base_url()?>pharmacist/viewmore/index/6" class="pull-right">view more..</a></td>
						</tr>
						<?php
					}
					else{?>
						 <tr> <td colspan="3">no record found</td></tr>
					<?php }?>               
				</tbody>
			</table>
	</p>
    </div>
  <div id="archive" class="tab-pane fade">
 		<h4>Archived Rx</h4>
      <p>
      	<table class="dashboardtable table-bordered">
  			<thead>
 				<th>Rx #</th>
 				<th>Patient Name</th>
 				<th>Date/Time</th>
 			</thead>
			<tbody>
				<?php
					if(!empty($archivePrescription))
					{
						foreach($archivePrescription as $archive)
						{
							?>
							<tr data-href="<?=base_url()?>pharmacist/rx/view/<?=$archive->rxId?>" >
								<td><?=$archive->rxNumber?></td>
								<td><?=$archive->firstName." ".$archive->lastName?></td>
								<td>
									<?php
										$date=date('m/d/Y h:ia',strtotime($archive->updateDate));
										echo $date ;
										if($archive->priority=='1')
										{
											?><i class="icon-warning-sign pull-right"></i><?php
										}
									?>
								</td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td colspan="3"><a href="<?=base_url()?>pharmacist/viewmore/index/5" class="pull-right">view more..</a></td>
						</tr>
						<?php
					}else
						{?>
							<tr> <td colspan="3">no record found</td></tr>
					<?php }
					?>               
			</tbody>
		</table>    
	</p>
  </div>
 	
<div id="dropdown2" class="tab-pane fade">
<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan, sartorial keffiyeh echo park vegan.</p>
</div>
</div>	
	
	
</div>	
	<div class="span4">
	<div class="recentblock">
		<div><h3 class="label label-info">Recent Activities</h3></div>
		<?php if(!empty($activities)) { ?>
			<ul class="recent">
	              <?php foreach($activities as $activity) {?>
	              <li><?=$activity->string?></li>
	              <?php } ?>
	             <li class="viewRecentMore"><a  id="clickSbmit" data-toggle="modal" href="#myModal" >view more..</a></li> 
	        </ul>
		<?php } else echo "No activities found"; ?> 
	</div>
	</div>		
</div>

<div class="modal hide" id="myModal">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Recent Activities</h3>
            </div>

  <div class="modal-body">
   <div class="recentblock">	
		<?php if(!empty($activitiesMore)) { ?>
			<ul class="recent">
	              <?php foreach($activitiesMore as $activity) {?>
	              <li><?=$activity->string?></li>
	              <?php } ?>
	           
	        </ul>
		<?php } else echo "No activities found"; ?> 	 
	</div>
  </div>
  <div class="modal-footer">
   </div>
</div>
<?php
$CI->load->view('pharmacist/common/footer');
?>
