<?php
$CI=&get_instance();
$CI->load->view('pharmacist/common/header');
$seg=$this->uri->segment(4); 
$videoprop = $this->session->userdata('videoprop');
?>  
 <link href="<?php echo site_url();?>assets/css/modal.css" rel="stylesheet">
<!--  <script src="<?php echo site_url();?>assets/js/jquery.cookie.js"></script>
 <SCRIPT LANGUAGE="JavaScript">
	jQuery(window).load(function() {
	jQuery('#loading-image').hide();
});
	</SCRIPT>
 --> 


<script type="text/javascript">
		jQuery( function($) {
			var videoprop ='<?php echo $videoprop;?>';
 			if (videoprop=="")
			{
				$('#testing').modal('show');
				$('#videoprop').val("1");
			}
   			$('tbody tr[data-href]').addClass('clickable').click( function() {
				window.location = $(this).attr('data-href');
			}).find('a').hover( function() {
				$(this).parents('tr').unbind('click');
			}, function() {
				$(this).parents('tr').click( function() {
					window.location = $(this).attr('data-href');
				});
			});
		});
	</script>
<script type="text/javascript">
$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
}); 
</script> 
<!-- <script type="text/javascript">
		jQuery( function($) {
			$('tbody tr[data-href]').addClass('clickable').click( function() {
				window.location = $(this).attr('data-href');
			}).find('a').hover( function() {
				$(this).parents('tr').unbind('click');
			}, function() {
				$(this).parents('tr').click( function() {
					window.location = $(this).attr('data-href');
				});
			});
		});
	</script>
 -->
 
<!-- 
<?php 
	if($this->session->flashdata('message'))
	{?>		
		<div class="alert alert-success" style="width: 940px !important;"><a class="close" data-dismiss="alert">�</a><?php echo $this->session->flashdata('message') ; ?></div>		
	<?php 
	}
?> 
-->
<!-- for video conferencing -->
<!--------------------- Contact support ------------------------------------->
<?php
          			$call = $this->session->userdata('videoCall');
          			?>
<div class="modal hide testing" id="testing">
	<div class="modal-header">
	<h4>You would like to recieve video conference calls on requests?</h4>
	</div>
	 
	<div class="modal-footer">
		<button id="callOn"  data-dismiss="modal" class="btn <?php if($call == "1") echo "btn-warning"?>" type="button" onclick="javascript:setCall('1');">Yes</button>
          			<button id="callOff" data-dismiss="modal" class="btn <?php if($call == "0") echo "btn-warning"?>" type="button" onclick="javascript:setCall('0');">No</button>


	</div>
</div>
 

<!------------------ end ----------------------->
<div class="pageheader"><H3><?php echo $this->uri->segment(4);?> Records</H3></div>
<!-- <div id="loading-image">
	 <B>Loading...</B>
</div> -->
 <table class="table table-bordered">
		<thead>
			<th class="viewHeaders">&nbsp;</th>
			<th class="viewHeaders">Patient Name</th>
			<th class="viewHeaders">Rx #</th>
			<th class="viewHeaders" >DOB</th>
			<th class="viewHeaders">Date/Time</th>
			<?php if(!empty($allPrescription) && $allPrescription[0]->status=='Counsel'){?> 					
				<th class="viewHeaders">Approved By</th>
			<?php }elseif(!empty($allPrescription) && $allPrescription[0]->status=='Completed'){?>
				<th class="viewHeaders">Completed By</th> <?php }?>
			<th class="viewHeaders">Store</th>					
		</thead>
		<tbody>   
			<?php
 			if(!empty($allPrescription))
			{
				$count=0;	
				foreach($allPrescription as $prescription)
				{
			?>
					<tr class="<?php if($prescription->status=='Approval' && !empty($prescription->priority) ) {}elseif($prescription->status=='Rejected'){ echo 'highprioRow';}?>" >	
					<td width="20px"><?php
						if(!empty($prescription->priority))
						{?><span id="low_<?php echo $prescription->id;?>" class="markHigh" onclick="javascript:setHigh('<?php echo $prescription->id;?>','<?php echo base_url();?>index/setHigh','0');" >&nbsp;</span><?php }
						else{?><span id="low_<?php echo $prescription->id;?>" class="markLow" onclick="javascript:setHigh('<?php echo $prescription->id;?>','<?php echo base_url();?>index/setHigh');">&nbsp;</span><?php }
						 ?></td>					
					<td class="clickable"  onClick="urlView('<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>')"><span class="patientName"><?=$prescription->firstname." ".$prescription->lastname?><span></td>
					<td class="clickable"  onClick="urlView('<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>')"><?=$prescription->rxNumber?></td>
					<td class="clickable"  onClick="urlView('<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>')"><?=$date=date('m/d/Y',strtotime($prescription->birthDate))?></td>
					<td class="clickable"  onClick="urlView('<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>')"><?=$date=date('m/d/Y h:ia',strtotime($prescription->updateDate))?></td>					
					<?php
					 if(!empty($allPrescription) && $allPrescription[0]->status=='Counsel')
					 {
					 ?> 					
						<td><?=$prescription->fname." ".$prescription->lname?></td>
						<?php
					 }
					 elseif(!empty($allPrescription) && $allPrescription[0]->status=='Completed')
					 {
					 	?>
					 	<td><?=$prescription->fname." ".$prescription->lname?></td>
					 	<?php
					 }
					?>
					<td class="clickable"  onClick="urlView('<?=base_url()?>pharmacist/rx/view/<?=$prescription->id?>')"><?=$prescription->storeName?></td>
					</tr>
					<?php
			$count++;
			}					
		}
		else{ 
			?>
			<tr>
				<td  colspan="<?php if(!empty($allPrescription) && ($allPrescription[0]->status=='Counsel' || $allPrescription[0]->status=='Completed') )echo '8';else echo '7'; ?>">no record found</td>
			</tr>
		<?php  } ?>	
		</tbody>
		</table>  
		<div class="pagination pull-right">
	<?php echo $this->pagination->create_links(); ?>
</div>

<div class="span12">
	<center>
			<div class="actBlock actTechBlock">
				<a id="clickSbmit" data-toggle="modal" href="#myModal" class="pull-right"><i class="icon-resize-full icon-white"></i></a>
				<?php if(!empty($activities)) { ?>
					<ul class="recent">
						  <?php foreach($activities as $activity) {?>
						  <li><?=$activity->string?></li>
						  <?php } ?>
					</ul>
				<?php } else echo "No activities found"; ?>
			</div>
	</center>
</div>

 <div class="modal hide" id="myModal">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Recent Activities</h3>
	</div>
	
	<div class="modal-body">
	<div class="recentblock">	
	<?php if(!empty($activitiesMore)) { ?>
	<ul class="recent">
	  <?php foreach($activitiesMore as $activity) {?>
	  <li><?=$activity->string?></li>
	  <?php } ?>
	   
	</ul>
	<?php } else echo "No activities found"; ?> 	 
	</div>
	</div>
	<div class="modal-footer">
	</div>
</div>

 <script type="text/javascript">
 
 function urlView(url){ 
 	window.location = url;
 }
 </script>
<?php
	$CI->load->view('pharmacist/common/footer');
?>  
 