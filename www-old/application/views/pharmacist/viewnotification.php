<?php
$CI=&get_instance();
$CI->load->view('pharmacist/common/header');
$seg=$this->uri->segment(4); 
$videoprop = $this->session->userdata('videoprop');
?>  
<script type="text/javascript" src="<?php echo base_url();?>assets/crx.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/form.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamnew.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/webcamInitiator.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/webcamnew.css" />
<div class="span12"> 
	<div class="pageheader"><H3>Current Comments</H3></div><br/>
	
		<table class="table table-bordered">
			<tbody>
				<?php $i=0;
				if(!empty($notification))
				{
					foreach($notification as $query)
					{
						$url=base_url()."pharmacist/rx/view/".$query->rxId;
						?>
						<tr class="clickable" onclick="javascript:linkComment('<?=$query->commentId?>','<?=$url?>','<?php echo $query->readFlag?>')">
							<td class="<?php if($query->readFlag == "0") echo 'unread'?>">
								<?php echo $query->alert=$query->alert;?>
							</td>
						</tr>	
						<?php $i++;
					}
				}
				else
					echo "No Comments Found.";?>
			</tbody>
		</table>
		<div class="pagination pull-right">
			<?php echo $this->pagination->create_links(); ?>
		</div>
</div>
<?php
	$CI->load->view('pharmacist/common/footer');
?>  












