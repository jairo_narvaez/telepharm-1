<?php
$CI=&get_instance();
$usertype=$this->session->userdata('userType');
if($usertype=='Technician')
$CI->load->view('common/header');
if($usertype=='Pharmacist')
$CI->load->view('pharmacist/common/header');
?>
<script type="text/javascript">
$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
}); 
</script>
<div class="span10">
	<?php 
		$seg=$this->uri->segment(4);   
		$email = $this->session->userdata('email');
		if($this->session->flashdata('message'))
		{?>		
			<div class="alert alert-success" ><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>		
		<?php 
		} 
 	?>	
 	
	 <form method="post" enctype="multipart/form-data" name="updateprofile_frm" class="well"  action="<?php echo base_url();?>index/update">		 		
	 	<input type="hidden" name="userId"  value="<?php if(!empty($userData->id)){echo $userData->id;} else if(!empty($_POST['userId'])){echo $_POST['userId'];}?>" />				
		<input type="hidden" name="usertype"  value="<?php if(!empty($userData->userType)){echo $userData->userType;} else if(!empty($_POST['usertype'])){echo $_POST['usertype'];}?>" />	
		<input type="hidden" name="password"  value="<?php if(!empty($userData->password)){echo $userData->password;} else if(!empty($_POST['password'])){echo $_POST['password'];}?>" />																			
		           
             <div class="control-group">
            <label class="control-label" for="input01">First Name <span class="star">*</span></label>
              <div class="controls">
               <input type="text" class="input-large" id="firstname" name="firstname"   value="<?php if(!empty($userData->firstName)){echo $userData->firstName;} else if(!empty($_POST['firstname'])){echo $_POST['firstname'];}?>"/>
              <span class="help-inline redError"><?php echo form_error('firstname'); ?></span>
               </div> 
             </div>
              <div class="control-group">
            <label class="control-label" for="input01">Last Name <span class="star">*</span></label>
              <div class="controls">               
                <input type="text" class="input-large" id="lastname" name="lastname"  value="<?php if(!empty($userData->lastName)){echo $userData->lastName;} else if(!empty($_POST['lastname'])){echo $_POST['lastname'];}?>"/> 
               <span class="help-inline redError"><?php echo form_error('lastname'); ?></span>
               </div> 
             </div>
               <div class="control-group">
            <label class="control-label" for="input01">Username</label>
              <div class="controls">
              <input type="text" class="input-large" id="email" name="email" readonly="readonly" value="<?php if(!empty($userData->email)){echo $userData->email;} else if(!empty($_POST['email'])){echo $_POST['email'];}?>"/>
               </div> 
             </div>
              <div class="control-group">
            <label class="control-label" for="input01">Phone Number</label>
              <div class="controls">
               <input type="text" class="input-large" id="phonenumber" name="phonenumber"   value="<?php if(!empty($userData->phoneNumber)){echo $userData->phoneNumber;} else if(!empty($_POST['phonenumber'])){echo $_POST['phonenumber'];}?>"/>
               </div> 
             </div>
             <div class="control-group">
	            <label class="control-label" for="input01">Photo</label>
	              <div class="controls">
	               <input type="file" class="input-large" id="photo" name="photo"   value=""/>
	               <img src="" height="" width="" alt=""/>
	               </div> 
	               <?php
	               if(!empty($userData->photo))
				   		echo '<img src="'.base_url().'uploads/pharma/'.$userData->photo.'" height="60" width="100" alt="No Image"/>';
				   ?>
             </div>
             <div class="control-group">
				<div class="controls">
					<input type="submit" value="Submit" name="submit" class="btn btn-primary">
				</div>
			</div>
 	</form>
</div>
<?php
if($usertype=='Technician')
$CI->load->view('common/footer');
if($usertype=='Pharmacist')
$CI->load->view('pharmacist/common/footer');
?>
