<?php
$CI=&get_instance();
$CI->load->view('common/header');
$segment4 = $this->uri->segment(4);
$segment = $this->uri->segment(5);
$userId = $this->session->userdata('userId');
$photostring1='';
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mglass.css">
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.lightbox-0.5.css" media="screen" />
 <script type="text/javascript">
    $(function() {
          $('#gallery a').lightBox();
    }) ;
    </script>
	 <script type="text/javascript"  src="<?php echo base_url();?>assets/js/mrglass.js"/>
   <script>
	$().ready(function() {
 	 $(".close").click(function() {
			$('.alert').css('display', 'none');
		});
	$('.dropdown-toggle').dropdown();
	});
 </script>
	<div class="span9">
		<?php
		$status=array('status'=>$patientData->status);
		$this->session->set_userdata($status);
		$this->session->userdata('status');
		if($this->session->flashdata('message'))
		{?>
			<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>
		<?php 
		}
		?>	
		
			<table class="table table-bordered">
				<tbody>
				<tr>
					<th class="viewHeaders">Name</th>
					<th class="viewHeaders">Rx #</th>
					<th class="viewHeaders">DOB</th>
					<th class="viewHeaders">Date/Time</th>
					<th class="viewHeaders">Counsel</th>
					<th class="viewHeaders">Refill</th>
					<th class="viewHeaders">&nbsp;</th>
				</tr>
				<tr>
					<td><?php echo $patientData->firstName." ".$patientData->lastName;?></td>
					<td><b><?php echo $patientData->rxNumber?></b></td>
					<td><?php echo date('m/d/Y',strtotime($patientData->birthDate));?></td>
					<td><?php echo date('m/d/Y h:ia',strtotime($patientData->updateDate));?></td>
					<td><?php if($patientData->requiredCounsel=='1'){echo 'Yes';}else {echo 'No';}?></td>
					<td><?php if($patientData->refill=='1'){echo 'Yes';}else {echo 'No';}?></td>
					<td><?php
						if(!empty($patientData->priority) )
							{?><span class="markHigh" >&nbsp;</span><?php }
							else{?><span class="markLow" >&nbsp;</span><?php }?>
					</td>	
				</tr>         
				</tbody>
			</table>

<div class="span7" style="margin:0px !important;">
	<?php
 	if(!empty($otherdata)){
	?>
			<table class="table table-bordered">
				<tbody>
				<tr>
					<th class="viewHeaders">Prescriber Name</th>
					<th class="viewHeaders">Prescriber #</th>
					<th class="viewHeaders">NPI #</th>
					<th class="viewHeaders">Phone</th>
					<!-- <th class="viewHeaders">Drugs</th> -->
  				</tr>
				<tr>
					<td><?php echo $otherdata[0]->firstName." ".$otherdata[0]->lastName;?></td>
					<td><b><?php echo $otherdata[0]->identifier?></b></td>
					<td><b><?php echo $otherdata[0]->Npi?></b></td>
					<td><?php echo $otherdata[0]->phone;?></td>
					<!-- <td><?php echo $otherdata[0]->packagesize." - ".$otherdata[0]->manf;?></td> -->
				</tr>         
				</tbody>
			</table>
			<table class="table table-bordered">
				<tbody>
					<tr>
					<th class="viewHeaders">Drugs</th>
					<th class="viewHeaders">NDC #</th>
					<th class="viewHeaders">Dispensed</th>
					<th class="viewHeaders">Package</th>
					<th class="viewHeaders">STR.</th>
					</tr>
					<tr>
						<td><?php echo $otherdata[0]->manf;?></td>
						<td><?php echo $otherdata[0]->newndc ;?></td>
						<td><?php echo $otherdata[0]->description;?></td>
						<td><?php echo $otherdata[0]->packagesize;?></td>
						<td><?php echo $otherdata[0]->strength;?></td>
					</tr>
			</table>
			</table>
			<table class="table table-bordered">
				<tbody>
					<tr>
					<th class="viewHeaders">Quantity Written</th>
					<th class="viewHeaders">Quantity Dispense</th>
					<th class="viewHeaders">SIG</th>
					<th class="viewHeaders">Days Supply</th>
					<th class="viewHeaders">Fills Owed</th>
					<th class="viewHeaders">Refills Auth</th>
					</tr>
					<tr>
						<td><?php echo $otherdata[0]->qtyWritten;?></td>
						<td><?php echo $otherdata[0]->qtyDispensed ;?></td>
						<td><?php echo $otherdata[0]->sig ;?></td>
						<td><?php echo $otherdata[0]->daysSupply;?></td>
						<td><?php echo $otherdata[0]->fillsOwed ;?></td>
						<td><?php echo $otherdata[0]->refillsAuth ;?></td>
					</tr>
			</table>

		
		<?php
		}
	else
		echo "&nbsp;";
 		?>
 </div>
		
		<?php if(!empty($NDCdrugImage[0])){?>
		<?php
		
			 $idname = 'medispandrug';
			 $photostring1.=' var  '.$idname.'= new MGlass("'.$idname.'","'.base_url().$NDCdrugImage[0].'");';
		?>
		 	<div class="pull-right span2"  style="margin-left:4px !important; padding: 3px; width: 190px;">
				<div style="height: 145px;">
					<?php
					 $idname = 'medispandrug';
					 $photostring1.=' var  '.$idname.'= new MGlass("'.$idname.'","'.base_url().$NDCdrugImage[0].'");';
					 ?><img style="height: 125px; width: 190px;" class="thumbnail" id="<?php echo $idname;?>" src="<?php echo base_url().$NDCdrugImage[0];?>"/>  <a  href="<?php echo base_url().$NDCdrugImage[0];?>"></span></a>
				 </div>
			</div>
			<?php } ?>
</div><!-- span9 -->
	<div class="span3">
		<?php
			if(!empty($patientData->rxId))
				{$parentRxId=$patientData->rxId;}
			if($segment!="" && !empty($segment))
				{$currentRxId=$segment;}
			else
				{$currentRxId=$parentRxId;}
			if($patientData->status!="Archive" && $this->session->userdata('status')!='Completed'){	
			?>
			<ul class="nav nav-pills pull-left">
			  <li class="dropdown active" id="menu1" >
			    <a class="dropdown-toggle liSearch" data-toggle="dropdown"  href="#menu1"  style="width:94px !important">I want to... <b class="caret pull-right"></b></a>
			    <ul class="dropdown-menu">
			    <?php
			      if(empty($patientData->priority) && $patientData->priority=="0" && ($patientData->status=="Approval" || $patientData->status=="Counsel"))
				  {
				  	?>
				  	 <li><a href="<?=base_url()?>technician/rx/sethigh/<?php if(!empty($segment4)){echo $segment4;}else{echo $parentRxId;}?>/<?=$currentRxId?>" ><i class="icon-warning-sign"></i>&nbsp;High Priority</a></li>
				  	<?php
				  }
				  if($patientData->status!="Counsel" && $patientData->status!="Completed")
				  {
					  if($patientData->status=="OnHold"){
				      ?>
				    	<li><a href="<?=base_url()?>technician/rx/edit/<?=$currentRxId?>" ><i class="icon-edit"></i>&nbsp;Fill Rx</a></li>
				      <?php
					  }
					  else{
					?>
						<li><a href="<?=base_url()?>technician/rx/edit/<?=$currentRxId?>" ><i class="icon-edit"></i>&nbsp;Edit</a></li>
					<?php
						}//else
				  }
			      if($patientData->status=="Counsel")
				  {
				  	?>
				  	<li><a  href="<?=base_url()?>technician/rx/completecounsel/<?=$patientData->rxId?>" >Refused Counsel</a></li>
				  	<?php
				  }
			      ?>
			    </ul>
			  </li>
			</ul>
			<?php
			}
			?>
			<select name="childRxNumber" id="childRxNumber"  onChange="getData('<?=base_url()?>technician/rx/view/<?=$this->uri->segment(4)?>');"  style="width:125px !important;margin-top:3px" >
					<?php if(!empty($childRx)){
						foreach($childRx  as $Rx)
						{
							?>
								<option value="<?=$Rx->childRxId?> "<?php if(!empty($segment) && $segment==$Rx->childRxId){echo 'selected="selected"';}?> ><?=$Rx->childRxNumber?></option>
							<?php
						}
					}
					?>
				</select>
				<br>
				<div class="control-group ">
			<form method="post" style="margin:0px !important;">
 				<input type="hidden" name="commentRxId" value="<?php  echo  $patientData->rxId ;?>"/>
				<input type="hidden" name="userId" value="<?php echo $userId ;?>"/>
				<input type="text" name="comments"  class="viewCommentText" rows="3" placeholder="Add Comments..." />
			</form> 
        </div>
		 <div class="commentblock">
					<div><h3 class="label label-info">Comments</h3></div>
						 <div class="recent">
								 <?php  if(!empty($comments)){?>
									<ul class="recentnew">
								 <?php
									foreach($comments as $comment)
									{
									?>
										<li><B><?=$comment->fullname."</B> on ". $comment->date."<br><I>".$comment->comments?></I></li>
									<?php }?>
									<li class="viewRecentMore"><a  id="clickSbmit" data-toggle="modal" href="#myModalComm" >view more..</a></li> 
								</ul>
							<?php
							 }
							else {echo "No comments were found";}?>
						</div>
		  </div>
	</div>
</div>
<div class="row-fulid">
  <div style="margin-left: -10px;">
      	<div class="control-group">
          <div class="controls ">
          	<ul class="catlist showImage" id="gallery">
          	<?php
			$photostring="";
			//$photostring1="";
        	if(!empty($capturedImage))
          	{
				$startCount=100;
          		for($counter=0; $counter<count($capturedImage);$counter++){
					$u="uploads/technician/captureimage/".$capturedImage[$counter]->imageName;
					if(file_exists($u))
					{
 					$idname="mr".$startCount;
					$photostring1.=' var  '.$idname.'= new MGlass("'.$idname.'","'.base_url().'/uploads/technician/captureimage/'.$capturedImage[$counter]->imageName.'");';
				?>
          		<li>
					<img height="315" width="260" class="thumbnail" id="<?php echo $idname;?>" src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>"/>  
					<a rel="lightbox" href="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>"><span class="magglass"></span></a>
				</li>
 				</li>
           		<?php
					}
					$startCount++;
          		}
          	}
			if(!empty($drugImage))
          	{
          		for($counter=0; $counter<count($drugImage);$counter++){
				$u="uploads/technician/uploadimage/".$drugImage[$counter]->imageName;
					if(file_exists($u) && $drugImage[$counter]->imageName!="")
					{
					$idname="mr".$counter;
					$photostring1.=' var  '.$idname.'= new MGlass("'.$idname.'","'.base_url().'/uploads/technician/uploadimage/'.$drugImage[$counter]->imageName.'");';
				?>
           		<li>
				 <img height="260" width="376" class="thumbnail" id="<?php echo $idname;?>" src="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>"/>  
				  <a rel="lightbox" href="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>"><span class="magglass"></span></a>
				</li>
           		<?php
					}
          		}
          	}
			$photostring.='<script type="text/javascript">'.$photostring1;
			$photostring.="</script>";
			echo $photostring;
          	?>
          	</ul>
            
           </div> 
        </div>
      	
	</div>

</div>

<div class="row-fulid">
<center>
<div class="span12" id="actTechBlock">
		<br>
		<div class="actBlock actTechBlock1">
		<a  id="clickSbmit" data-toggle="modal" href="#myModalAct" class="pull-right" ><i class="icon-resize-full icon-white"></i></a>
		<?php 
		if(!empty($activities)) { ?>
			<ul class="recent">
				  <?php foreach($activities as $activity) {?>
				  <li><?=$activity->string;?></li>
				  <?php } ?>
			</ul>
		<?php } else echo "No activities found"; ?>
	</div>
	
</div>
</center>
</div>



<div class="modal hide" id="myModalComm">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Comments</h3>
  </div>
  <div class="modal-body">
  	 		<div class="recent">
	          	 <?php  if(!empty($commentsMore)){?>
	          	 	<ul class="recent">
	          	 <?php
				 	foreach($commentsMore as $comment)
					{?>
						<li><B><?=$comment->fullname."</B> on ". $comment->date."<br><I>".$comment->comments?></I></li>
					<?php }?>					
				</ul>
			<?php
			 }
			else {echo "No comments were found";}?>
			</div>  
  </div>
  <div class="modal-footer">
   </div>
</div>
<div class="modal hide" id="myModalAct">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Recent Activities</h3>
            </div>

  <div class="modal-body">
   <div class="recentblock">	
		<?php if(!empty($activitiesMore)) { ?>
			<ul class="recent">
	              <?php foreach($activitiesMore as $activity) {?>
	              <li><?=$activity->string?></li>
	              <?php } ?>
	           
	        </ul>
		<?php } else echo "No activities found"; ?> 	 
	</div>
  </div>
  <div class="modal-footer">
   </div>
</div>

<?php
$CI->load->view('common/footer');
?>