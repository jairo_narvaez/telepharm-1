<?php
$CI=&get_instance();
$CI->load->view('common/header');
$segment4 = $this->uri->segment(4);
$segment = $this->uri->segment(5);
$userId = $this->session->userdata('userId');
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mglass.css">
<!-- <script type="text/javascript" src="<?php echo base_url();?>js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.lightbox-0.5.css" media="screen" />
 -->

 <script type="text/javascript">
    $(function() {
	//	$('a[@rel*=lightbox]').lightBox();
         $('#gallery a').lightBox();
    }) ;
    </script>
	 <script type="text/javascript"  src="<?php echo base_url();?>assets/js/mrglass.js"/>

 <script>
	$().ready(function() {
 	 $(".close").click(function() {
			$('.alert').css('display', 'none');
		});
		
	$('.dropdown-toggle').dropdown();
		
	});
	function getData()
	{
		var parentRxId=$("#childRxNumber").val();
		var rxId=$("#childRxNumber").val();
		var url="<?=base_url()?>technician/rx/view/"+<?=$this->uri->segment(4)?>;
		if(rxId!="" && rxId!=null)
		{
			url+="/"+rxId;
		}
		document.location.href=url;	
	}

 
</script>


	<div class="span9">
		<?php
		if($this->session->flashdata('message'))
		{?>
			<div class="alert alert-success"><a class="close" data-dismiss="alert">�</a><?php echo $this->session->flashdata('message') ; ?></div>
		<?php 
		}
		?>	
			<table class="table table-bordered">
				<tbody>
				<tr>
					
					<th class="viewHeaders">Name</th>
					<th class="viewHeaders">Rx #</th>
					<th class="viewHeaders">Drug</th>
					<th class="viewHeaders">DOB</th>
					<th class="viewHeaders">Date/Time</th>
					<th class="viewHeaders">Refill</th>
					<?php if(!empty($patientData->priority)){?><th class="viewHeaders">&nbsp;</th><?php } ?>
				</tr>
				<tr>
					<td><?php echo $patientData->firstName." ".$patientData->lastName;?></td>
					<td><b><?php echo $patientData->rxNumber?></b></td>
					<td><?php echo $patientData->drugDispensed;?></td>
					<td><?php echo $patientData->birthDate;?></td>
					<td><?php echo date('d/m/Y h:iA',strtotime($patientData->createdDate));?></td>
					
					<td><?php if($patientData->refill=='1'){echo 'Yes';}else {echo 'No';}?></td>
					<?php if(!empty($patientData->priority)){?><td><i class="icon-warning-sign"></td><?php } ?>

				</tr>         
				</tbody>
			</table>
	</div><!-- span7 -->
	<div class="span3">
		<?php
			if(!empty($patientData->rxId))
				{$parentRxId=$patientData->rxId;}
			if($segment!="" && !empty($segment))
				{$currentRxId=$segment;}
			else
				{$currentRxId=$parentRxId;}
			if($patientData->status!="Archive"){	
			?>
			<ul class="nav nav-pills pull-left">
			  <li class="dropdown active" id="menu1" >
			    <a class="dropdown-toggle liSearch" data-toggle="dropdown"  href="#menu1"  style="width:94px !important">I want to... <b class="caret pull-right"></b></a>
			    <ul class="dropdown-menu">
			    <?php
			      if(empty($patientData->priority) && $patientData->priority=="0" && ($patientData->status=="Approval" || $patientData->status=="Counsel"))
				  {
				  	?>
				  	 <li><a href="<?=base_url()?>technician/rx/sethigh/<?php if(!empty($segment4)){echo $segment4;}else{echo $parentRxId;}?>/<?=$currentRxId?>" ><i class="icon-warning-sign"></i>&nbsp;High Priority</a></li>
				  	<?php
				  }
				  if($patientData->status!="Counsel" && $patientData->status!="Completed")
				  {
				      ?>
				    	<li><a href="<?=base_url()?>technician/rx/edit/<?=$currentRxId?>" ><i class="icon-edit"></i>&nbsp;Edit</a></li>
				      <?php
				  }
			      if($patientData->status=="Counsel")
				  {
				  	?>
				  	<li><a  href="<?=base_url()?>technician/rx/completecounsel/<?=$patientData->rxId?>" >Refused Counsel</a></li>
				  	<?php
				  }
				  if($patientData->status!="Archive")
				  {
				  	?>
				  	<li><a href="<?php echo base_url();?>technician/viewmore/update/<?=$patientData->rxId?>" onclick="return confirm('Are you sure want to archive this Rx?');" ><i class="icon-remove"></i>&nbsp;Archive</a></li>
				  	<?php
				  }
			      ?>
			      
			    </ul>
			  </li>
			</ul>
			<?php
			}
			?>
 
			<select name="childRxNumber" id="childRxNumber"  onchange="javascript:getData();"  style="width:125px !important;margin-top:3px" >
					<?php if(!empty($childRx)){
						foreach($childRx  as $Rx)
						{
							?>
								<option value="<?=$Rx->childRxId?> "<?php if(!empty($segment) && $segment==$Rx->childRxId){echo 'selected="selected"';}?> ><?=$Rx->childRxNumber?></option>
							<?php
						}
					}
					?>
				</select>

				<br>

				<div class="control-group ">
			<form method="post" >
 				<input type="hidden" name="commentRxId" value="<?php  echo  $patientData->rxId ;?>"/>
				<input type="hidden" name="userId" value="<?php echo $userId ;?>"/>
				<input type="text" name="comments"  class="viewCommentText" rows="3" placeholder="Add Comments..." />
			</form> 
        </div>

 <div class="commentblock">
       		<div><h3 class="label label-info">Comments</h3></div>
             <div class="recent">
	          	 <?php  if(!empty($comments)){?>
	          	 	<ul class="recentnew">
	          	 <?php
				 	foreach($comments as $comment)
					{
					?>
						<li><B><?=$comment->fullname."</B> on ". $comment->date."<br><I>".$comment->comments?></I></li>
					<?php }?>
					<li class="viewRecentMore"><a  id="clickSbmit" data-toggle="modal" href="#myModalComm" >view more..</a></li> 
				</ul>
			<?php
			 }
			else {echo "No comments were found";}?>
			</div>
  	 </div>

	</div>
	
	
	


    
	 
	
      
  	 
	</div>
	

 

<div class="row-fulid">
  <div class="span12">
      	
      	<div class="control-group">
          <div class="controls ">
          	<style>
		.black_overlay{
			display: none;
			position: absolute;
			top: 0%;
			left: 0%;
			width: 100%;
			height: 100%;
			background-color: black;
			z-index:1001;
			-moz-opacity: 0.8;
			opacity:.80;
			filter: alpha(opacity=80);
		}
		.white_content img{
			 
			position: absolute;
			top: 10%;
			left: 10%;
			padding: 0px;
 		}
		.white_content {
			display: none;
			position: absolute;
			top: 10%;
			left: 10%;
			width: 60%;
			height: 60%;
			padding: 0px;
			border: 16px solid orange;
			background-color: white;
			z-index:1002;
			overflow: auto;
		}
	</style>
          	<ul class="catlist showImage" id="gallery">
          	<?php
			$photostring="";
			$photostring1="";

        	if(!empty($capturedImage))
          	{
				$startCount=100;
          		for($counter=0; $counter<count($capturedImage);$counter++){
					$u="uploads/technician/captureimage/".$capturedImage[$counter]->imageName;
					if(file_exists($u))
					{
					$idname="mr".$startCount;
					$photostring1.=' var  '.$idname.'= new MGlass("'.$idname.'","'.base_url().'uploads/technician/captureimage/'.$capturedImage[$counter]->imageName.'");';
				?>
          		<li>
				 <img height="260" width="260" class="thumbnail"  src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>"/>  
				  <!-- <a rel="lightbox"  id="<?php echo $idname;?>" href="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>"><span class="magglass"></span></a> -->
				</li>


		<p>This is the main content. To display a lightbox click <a >here</a></p>

<a  href = "javascript:void(0)" onclick = "document.getElementById('a_<?php echo $idname;?>').style.display='block';document.getElementById('fade').style.display='block'"><img height="160" width="188" class="thumbnail" src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>" alt="NoImage"  /></a>


<div id="a_<?php echo $idname;?>" class="white_content"> <img   id="<?php echo $idname;?>"   class="thumbnail"  src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>"/>  <a href = "javascript:void(0)" onclick = "document.getElementById('a_<?php echo $idname;?>').style.display='none';document.getElementById('fade').style.display='none'">Close</a></div>
		<div id="fade" class="black_overlay"></div>


				<!-- <a rel="lightbox" href="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>"><img height="160" width="188" class="thumbnail" src="<?php echo base_url();?>uploads/technician/captureimage/<?=$capturedImage[$counter]->imageName?>" alt="NoImage"  /></a> -->
				</li>
           		<?php
					}
					$startCount++;
          		}
          	}
			if(!empty($drugImage))
          	{
				
          		for($counter=0; $counter<count($drugImage);$counter++){
				$u="uploads/technician/uploadimage/".$drugImage[$counter]->imageName;
					if(file_exists($u) && $drugImage[$counter]->imageName!="")
					{
					$idname="mr".$counter;
					$photostring1.=' var  '.$idname.'= new MGlass("'.$idname.'","'.base_url().'/uploads/technician/uploadimage/'.$drugImage[$counter]->imageName.'");';
				?>
				
           		<li>
				 <img height="260" width="260" class="thumbnail" id="<?php echo $idname;?>" src="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>"/>  
<!-- 				  <a rel="lightbox" href="<?php echo base_url();?>uploads/technician/uploadimage/<?=$drugImage[$counter]->imageName?>"><span class="magglass"></span></a>
 -->				</li>
           		<?php
					}
				
          		}
				$photostring.='<script type="text/javascript">'.$photostring1;
				$photostring.="</script>";
          	}
			echo $photostring;
          	?>
          	</ul>
             
           </div> 
        </div>
      	
	</div>

</div>

<div class="row-fulid">
<center>
<div class="span12" id="actTechBlock">
		
		<div class="actBlock actTechBlock1">
		<a  id="clickSbmit" data-toggle="modal" href="#myModalAct" class="pull-right" ><i class="icon-resize-full icon-white"></i></a>
		<?php 
		if(!empty($activities)) { ?>
			<ul class="recent">
				  <?php foreach($activities as $activity) {?>
				  <li><?=$activity->string;?></li>
				  <?php } ?>
			</ul>
		<?php } else echo "No activities found"; ?>
	</div>
	
</div>
</center>
</div>



<div class="modal hide" id="myModalComm">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Comments</h3>
  </div>
  <div class="modal-body">
  	 		<div class="recent">
	          	 <?php  if(!empty($commentsMore)){?>
	          	 	<ul class="recent">
	          	 <?php
				 	foreach($commentsMore as $comment)
					{?>
						<li><B><?=$comment->fullname."</B> on ". $comment->date."<br><I>".$comment->comments?></I></li>
					<?php }?>					
				</ul>
			<?php
			 }
			else {echo "No comments were found";}?>
			</div>  
  </div>
  <div class="modal-footer">
   </div>
</div>
<div class="modal hide" id="myModalAct">
  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h3>Recent Activities</h3>
            </div>

  <div class="modal-body">
   <div class="recentblock">	
		<?php if(!empty($activitiesMore)) { ?>
			<ul class="recent">
	              <?php foreach($activitiesMore as $activity) {?>
	              <li><?=$activity->string?></li>
	              <?php } ?>
	           
	        </ul>
		<?php } else echo "No activities found"; ?> 	 
	</div>
  </div>
  <div class="modal-footer">
   </div>
</div>

<?php
$CI->load->view('common/footer');
?>