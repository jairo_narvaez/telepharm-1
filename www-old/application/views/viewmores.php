<?php
$CI=&get_instance();
$CI->load->view('common/header');
$segment4='';
$segment4=$this->uri->segment(4);
$seg=$this->uri->segment(4);
?>

<style>
.clickable{cursor:pointer;}
</style>
<script type="text/javascript">
		jQuery( function($) {
			$('tbody td[data-href]').addClass('clickable').click( function() {
				window.location = $(this).attr('data-href');
			}).find('a').hover( function() {
				$(this).parents('td').unbind('click');
			}, function() {
				$(this).parents('td').click( function() {
					window.location = $(this).attr('data-href');
				});
			});
		});

$().ready(function() {
 $(".close").click(function() {
		$('.alert').css('display', 'none');
	});
}); 
ajaxManager.run(); 

$(document).on("click", ".open-AddBookDialogCompleted", function () {
	
     var myBookId = $(this).data('id2');
     var values=myBookId.split("-");    
     $(".modal-body #rxId2").val( values[0] );
      $(".modal-body #storeId2").val( values[1] );
    $('#myModalCompleted').modal('show');
});

$(document).on("click", ".open-AddBookDialogDelete", function () {
	
     var myBookId = $(this).data('id4');
     var values=myBookId.split("-");    
     $(".modal-body #rxId4").val( values[0] );
      $(".modal-body #storeId4").val( values[1] );
    $('#myModalDelete').modal('show');
});

$(document).on("click", ".clickTocall", function () {
	var myBookId = $(this).data('id5');
     var values=myBookId.split("-");    
     $(".modal-header #phoneText").html('<span id="rxNumber">'+values[0]+'</span>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="javascript:cancelConference(\''+values[1]+'\');"  data-dismiss="modal">(Cancel call)</a>');
    $('#myModalPhone').modal('show');
});

	jQuery(window).load(function() {
	jQuery('#loading-image').hide();
});
</script>

<div class="span12"> 
	 <?php
	   if($this->session->flashdata('message'))
		{?>		
			<div class="alert alert-success" style="width: 940px !important;" ><a class="close" data-dismiss="alert">×</a><?php echo $this->session->flashdata('message') ; ?></div>		
		<?php 
		}
	?>   
<div class="pageheader"><H3><?php echo $this->uri->segment(4);?> Records</H3></div>
<BR>

<!--  -->
<table class="table table-bordered">
		<thead>
			<th class="viewHeaders" >&nbsp;</th>
			<th class="viewHeaders" >Patient Name</th>
			<th class="viewHeaders" >Rx #</th>
			<th class="viewHeaders" >DOB</th>
			<th class="viewHeaders" >Date/Time</th>
			<?php if(!empty($allPrescription) && $allPrescription[0]->status=='Counsel'){?> 					
			<th class="viewHeaders" >Approved By</th>
			<?php }elseif(!empty($allPrescription) && $allPrescription[0]->status=='Completed'){?><th class="viewHeaders" >Completed By</th> <?php }?>
								
			<th class="viewHeaders" >Options</th>
		</thead>
		<tbody>
			<?php
			if(!empty($allPrescription))
			{
				$count=0;	
				foreach($allPrescription as $prescription)
				{?>
					<tr <?php if($prescription->status=='Approval' && !empty($prescription->priority) ) {}elseif($prescription->status=='Rejected'){ echo 'class="highprioRow"';}?>>	
						<td width="20px"><?php
						if(!empty($prescription->priority) )
							{?><span id="low_<?php echo $prescription->id;?>" class="markHigh" onclick="javascript:setHigh('<?php echo $prescription->id;?>','<?php echo base_url();?>index/setHigh','0');" >&nbsp;</span><?php }
							else{?><span id="low_<?php echo $prescription->id;?>" class="markLow" onclick="javascript:setHigh('<?php echo $prescription->id;?>','<?php echo base_url();?>index/setHigh','1');">&nbsp;</span><?php }
						 ?></td>	
						<td  data-href="<?=base_url()?>technician/rx/view/<?=$prescription->id?>"><span class="patientName"><?=$prescription->firstname." ".$prescription->lastname?><span></td>
						<td  data-href="<?=base_url()?>technician/rx/view/<?=$prescription->id?>"><?php echo $prescription->rxNumber; ?></td>
						<td  data-href="<?=base_url()?>technician/rx/view/<?=$prescription->id?>"><?=$date=date('m/d/Y',strtotime($prescription->birthDate))?></td>
						<td  data-href="<?=base_url()?>technician/rx/view/<?=$prescription->id?>"><?=$date=date('m/d/Y h:ia',strtotime($prescription->updateDate))?></td>
						<?php 
						if(!empty($allPrescription) && $allPrescription[0]->status=='Counsel'){
								?> 					
									<td  data-href="<?=base_url()?>technician/rx/view/<?=$prescription->id?>"><?=$prescription->fname." ".$prescription->lname?></td>
								<?php
						}
						elseif(!empty($allPrescription) && $allPrescription[0]->status=='Completed')
						{
							?>
								<td  data-href="<?=base_url()?>technician/rx/view/<?=$prescription->id?>"><?=$prescription->fname." ".$prescription->lname?></td>
							<?php
						}
						
						?>
								<td>
									<?php
									
									if($prescription->status=='Draft')
									{
										?><a  href="<?php echo base_url();?>technician/rx/edit/<?=$prescription->id?>" title="Click here to edit Rx"><i class="icon-edit"></i></a><?php 
									}
									elseif($prescription->status=='Counsel')
									{
										?>
										<a class="open-AddBookDialogCompleted"  data-toggle="modal" href="#myModalCompleted"  title="Click here to refuse Rx" data-id2="<?=$prescription->id."-".$prescription->storeId?>"  ><i class="btn-icon-only icon-ok" ></i></a>
										<?php
									}
									elseif($prescription->status=='Approval')
									 { 
									 	?>
									 	<a  href="<?php echo base_url();?>technician/rx/edit/<?=$prescription->id?>" title="Click here to edit Rx"><i class="icon-edit"></i></a>								
										<?php 
										if(!empty($prescription->priority))
										{
											 ?>									
											<a  href="<?php echo base_url();?>technician/viewmore/updatePrio/<?=$prescription->id?>/<?=($prescription->priority)?>/<?=$seg?>/<?php if(!empty($allPrescription[$count-1]->id)) {echo $allPrescription[$count-1]->id;}else{echo '0';}?>"  title="Click here to step up"><i class="icon-chevron-up"></i></a>
											<a  href="<?php echo base_url();?>technician/viewmore/updatePrio/<?=$prescription->id?>/<?=($prescription->priority)?>/<?=$seg?>/<?php if(!empty($allPrescription[$count+1]->id)) {echo $allPrescription[$count+1]->id;}else{echo '0';}?>"  title="Click here to step down"><i class="icon-chevron-down"></i></a>											
											<?php 	
										}
										
										/*if(empty($prescription->priority)  && $prescription->priority=='0' )
										{
											 ?>									
											<a href="<?php echo base_url();?>technician/viewmore/updatePrio/<?=$prescription->id?>/<?=($prescription->priority+1)?>/<?=$seg?>/"  title="Click here to set high priority"><i class="icon-warning-sign"></i></a>
											<?php 	
										} 	*/ 
									 }
									elseif($prescription->status=='Cancelled')
									{ 
										?>	
											<a  href="<?php echo base_url();?>technician/rx/edit/<?=$prescription->id?>" title="Click here to edit Rx"><i class="icon-edit"></i></a>
										<?php 
									} ?>
									
									<a id="addVideo_<?php echo $prescription->id;?>" class="clickTocall"  data-toggle="modal" href="#myModalPhone" data-id5="<?=$prescription->rxNumber."-".$prescription->id?>" title="Call pharmacist" 
											onclick="javascript:addSession('<?php echo $prescription->id;?>','<?php echo $prescription->storeId;?>');">
											<i class="icon-facetime-video"></i>  
									</a>
									<a id="removeVideo_<?php echo $prescription->id;?>" title="Cancel Call" href="javascript:void(0);"
											onclick="javascript:cancelConference('<?php echo $prescription->id;?>');"><i class="icon-remove-circle"></i></a>
								<?php	
									if($prescription->videoConf=='0'){
										?>
										<script type="text/javascript">
											$("#removeVideo_"+<?php echo $prescription->id;?>).hide();
										</script>
										<?php
									}
									else{
										?>
										<script type="text/javascript">
											$("#addVideo_"+<?php echo $prescription->id;?>).hide();
										</script>
										<?php
									}?>
									</td>								
					</tr>
					<?php
					$count++;
				}
			}
			else
			{ 
				?>
			<tr>
			<td  colspan="<?php if(!empty($allPrescription) && ($allPrescription[0]->status=='Counsel' || $allPrescription[0]->status=='Completed') )echo '7';else echo '6'; ?>">no record found</td>
			</tr>
			<?php  } ?>		 
		</tbody>
		</table>

<div class="pagination pull-right">
	<?php echo $this->pagination->create_links(); ?>
</div>

<div class="span12">
	<center>
			<div class="actBlock actTechBlock">
				<a id="clickSbmit" data-toggle="modal" href="#myModal" class="pull-right"><i class="icon-resize-full icon-white"></i></a>
				<?php if(!empty($activities)) { ?>
					<ul class="recent">
						  <?php foreach($activities as $activity) {?>
						  <li><?=$activity->string?></li>
						  <?php } ?>
					</ul>
				<?php } else echo "No activities found"; ?>
			</div>
	</center>
</div>
 
</div>
	


<div class="modal hide" id="myModalCompleted">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Refuse Rx</h3>
	</div>
	<div class="modal-body">  
	<form name="completeCommentCompleted" method="post">
	<p>Add comment</p>
	<input type="hidden" name="segId" value="<?=$seg?>"/>  
	<input type="hidden" name="storeId" id="storeId2" value=""/>      		
	<input type="hidden" name="rxId"  id="rxId2"  value=""/>
	<textarea name="comment" rows="3" cols="60"></textarea>    		
	</form>		
	</div>
	<div class="modal-footer">
	<a href="#" onclick="document.completeCommentCompleted.action='<?=base_url()?>pharmacist/viewmore/updatecomplete';document.completeCommentCompleted.submit();" class="btn btn-primary">Refuse</a>
	<a href="#" class="btn" data-dismiss="modal">Cancel</a>
	</div>
</div>

<div class="modal hide" id="myModalDelete">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Archive Rx</h3>
	</div>
	<div class="modal-body">  
	<form name="completeCommentDelete" method="post">
	<p>Add comment</p>
	<input type="hidden" name="segId" value="<?=$seg?>"/>    	
	<input type="hidden" name="storeId" id="storeId4" value=""/>    			
	<input type="hidden" name="rxId"  id="rxId4"  value=""/>
	<textarea name="comment" rows="3" cols="60"></textarea>    		
	</form>		
	</div>
	<div class="modal-footer">
	<a href="#" onclick="document.completeCommentDelete.action='<?=base_url()?>pharmacist/viewmore/updatearchive';document.completeCommentDelete.submit();" class="btn btn-primary">Archive</a>
	<a href="#" class="btn" data-dismiss="modal">Cancel</a>
	</div>
</div>



<div class="modal hide" id="myModal">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Recent Activities</h3>
	</div>
	
	<div class="modal-body">
	<div class="recentblock">	
	<?php if(!empty($activitiesMore)) { ?>
	<ul class="recent">
	  <?php foreach($activitiesMore as $activity) {?>
	  <li><?=$activity->string?></li>
	  <?php } ?>
	   
	</ul>
	<?php } else echo "No activities found"; ?> 	 
	</div>
	</div>
	<div class="modal-footer">
	</div>
</div>

<div class="modal hide" id="myModalPhone">
	<div class="modal-header" id="modal-header" >
	<button type="button" class="close marginTop" data-dismiss="modal">x</button>
	A call has been initiated to the pharmacist for Rx # 
	<span id="phoneText">
		<span id="rxNumber"></span> &nbsp;&nbsp;
		<a href="javascript:void(0);" onclick="javascript:cancelConference();"  data-dismiss="modal">(Cancel call)</a>
	</span>
	</div>
	
</div>
<script type="text/javascript">
 function urlView(url){ 
	window.location = url;
 }

function addSession(rxId, storeId){
 	$("#addVideo_"+rxId).hide();
	$("#removeVideo_"+rxId).show();
	var parameters = [{ name: 'rxId', value: rxId }, { name: 'storeId', value: storeId }];
	ajaxManager.addReq({
		url: baseUrl+'technician/viewmore/addVideoSession',
		type: 'POST', 
		data: parameters,
		success: function(transport) {
		}
	});
}

function cancelConference(rxId){
	$("#removeVideo_"+rxId).hide();
	$("#addVideo_"+rxId).show();
	ajaxManager.addReq({
		url: baseUrl+'technician/viewmore/closeVideoConferencing/'+rxId,
		success: function(transport) {
		}
	});
}

 // $(location).attr('href');
/*var scripturl = "<?php echo base_url();?>technician/viewmore/getrecords/<?php echo $this->uri->segment(4);?>";
var $results = $('#results'),
    loadInterval = 15000;
(function loader() {
    $.get(scripturl, function(html){
            $results.hide(0, function() {
                $results.empty();
				//alert(html);
                 $results.html(html);
                $results.show(0, function() {
                    setTimeout(loader, loadInterval);
                });
            });
    });
})(); 

*/
 </script>
<?php
$CI->load->view('common/footer');
?>
