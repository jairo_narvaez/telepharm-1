function Notifyloader() {
	$.ajax({
		url : scripturl1,
		success : function(html) {
			var obj = $.parseJSON(html);
			if(obj["countCounsel"] !=""){
				$("#nCounsel").html('<span class="bubble" id="nCounsel">'+obj["countCounsel"]+'</span>');
			}
			else{
				 $("#nCounsel").removeClass("bubble");
				 $("#nCounsel").html('');
			}

			if(obj["countApproval"] !=""){
				$("#nApproval").html('<span class="bubble">'+obj["countApproval"]+'</span>');
			}
			else{
				 $("#nApproval").removeClass("bubble");
				 $("#nApproval").html('');
			}

			if(obj["countCompleted"] !=""){
				$("#nCompleted").html('<span class="bubblemenu">'+obj["countCompleted"]+'</span>');
			}
			else{
				 $("#nCompleted").removeClass("bubble");
				 $("#nCompleted").html('');
			}


			if(obj["countCancelled"] !=""){
				$("#nCancelled").html('<span class="bubblemenu">'+obj["countCancelled"]+'</span>');
			}
			else{
				 $("#nCancelled").removeClass("bubble");
				 $("#nCancelled").html('');
			}

			if(obj["countDraft"] !=""){
				$("#nDraft").html('<span class="drfatbubble">'+obj["countDraft"]+'</span>');
			}
			else{
				 $("#nDraft").removeClass("drfatbubble");
				 $("#nDraft").html('');
			}
			if(obj["countOnHold"] !=""){
				$("#nonhold").html('<span class="bubblemenu">'+obj["countOnHold"]+'</span>');
			}
			else{
				 $("#nRejected").removeClass("bubble");
				 $("#nRejected").html('');
			}


			if(obj["countRejected"] !=""){
				$("#nRejected").html('<span class="bubble">'+obj["countRejected"]+'</span>');
			}
			else{
				 $("#nRejected").removeClass("bubble");
				 $("#nRejected").html('');
			}
			
			if(obj["countNotification"] !=""){
				$("#nNotification").html('<span class="bubble" id="countnotification">'+obj["countNotification"]+'</span>');
			}
			else{
				 $("#nNotification").removeClass("bubble");
				 $("#nNotification").html('');
			}

		}
	});				
 }

function setHigh(prescId,url1,current)
{
	$.ajax({			
			url :url1+"/"+prescId+"/"+current,
			success : function(msg) {
				if($('#low_'+prescId).hasClass('markLow'))
				{
					$('#low_'+prescId).removeClass('markLow');
					$('#low_'+prescId).addClass('markHigh');
				}
				else
				{
					$('#low_'+prescId).removeClass('markHigh');
					$('#low_'+prescId).addClass('markLow');
				}
				location.reload();
			}
	});
}

 function getData(url)
	{
		var parentRxId=$("#childRxNumber").val();
		var rxId=$("#childRxNumber").val();
 		//var url="<?=base_url()?>technician/rx/view/"+<?=$this->uri->segment(4)?>;
		if(rxId!="" && rxId!=null)
		{
			url+="/"+rxId;
		}
		document.location.href=url;	
	}
function linkComment(commentid,link,flag)
{
	if(flag == "0")
	$.ajax({			
			url :notiUrl,
			type:'POST',
			data:'commentid='+commentid,
			success : function(msg) {
				var count=new Number($('#countnotification').html());
				var newcount=count-1;
				if(newcount == 0)
				{
					$("#nNotification").removeClass("bubble");
					$("#nNotification").html('');
				}
				else
					$('#countnotification').html(newcount);
			}
	});

	window.location=link;
}
function clearComment()
{
	$.ajax({			
			url :clearUrl,
			success : function(msg) {
					$("#nNotification").removeClass("bubble");
					$("#nNotification").html('');
					$('#countnotification').html('');
			}
	});
}
