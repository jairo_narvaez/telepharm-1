$(document).ready(function(){
	$('.tooltip').fadeIn();
	var camera = $('#camera'),
		screen =  $('#screen');

	$("#captureButton").click(function(){
		// Generating the embed code and adding it to the page:	
		screen.html(
			//webcam.get_html(screen.width(), screen.height())
		webcam.get_html(530, 380)
		);
		$("#shootButton").css("display","inline-block");
		$("#captureButton").css("display","none");
		$(".myui-button").css("display","none");
		$("#cancelCapture").css("display","inline-block");
	})

	/*----------------------------------
		Binding event listeners
	----------------------------------*/


	var shootEnabled = false;
		
	$('#shootButton').click(function(){
		
		if(!shootEnabled){
			return false;
		}
		
		webcam.freeze();
		togglePane();
		return false;
	});
	
	$('#cancelButton').click(function(){
		webcam.reset();
		togglePane();
		return false;
	});
	
	$('#cancelCapture').click(function(){
		webcam.reset();
		$("#shootButton").css("display","none");
		$("#cancelCapture").css("display","none");
		$("#captureButton").css("display","inline-block");
		$(".myui-button").css("display","inline-block");
		return false;
	});
	
	$('#uploadButton').click(function(){
		webcam.upload();
		webcam.reset();
		togglePane();
		return false;
	});

	camera.find('.settings').click(function(){
		if(!shootEnabled){
			return false;
		}
		
		webcam.configure('camera');
	});

	// Showing and hiding the camera panel:
	
	var shown = false;
	$('.camTop').click(function(){
		
		$('.tooltip').fadeOut('fast');
		
		if(shown){
			camera.animate({
				bottom:-466
			});
		}
		else {
			camera.animate({
				bottom:-5
			},{easing:'easeOutExpo',duration:'slow'});
		}
		
		shown = !shown;
	});

	$('.tooltip').mouseenter(function(){
		$(this).fadeOut('fast');
	});


	/*---------------------- 
		Callbacks
	----------------------*/
	
	
	webcam.set_hook('onLoad',function(){
		// When the flash loads, enable
		// the Shoot and settings buttons:
		shootEnabled = true;
	});
	
	webcam.set_hook('onComplete', function(msg){
		
		// Customized response to string
		msg = msg.split(",");
		if(msg[0] == "error"){
			alert(msg[1]);
		}
		else {
			// Adding it to the page;
		 	addImage(msg[1]); 
		}
	});
	
	webcam.set_hook('onError',function(e){
		screen.html(e);
	});
	
	
	/*----------------------
		Helper functions
	------------------------*/

	
	// This function toggles the two
	// .buttonPane divs into visibility:
	
	function togglePane(){
		if ($('#shootButton').is(':visible')) {
			$("#shootButton").css("display","none");
			$("#cancelCapture").css("display","none");
			$("#canupload").css("display","block");
			$("#cancelButton").css("display","inline-block");
			$("#uploadButton").css("display","inline-block");
		}
		else{
			$("#canupload").css("display","none");
			$("#captureButton").css("display","inline-block");
			$(".myui-button").css("display","inline-block");
		}
	}
});
