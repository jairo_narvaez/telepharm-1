<?php
 if (isset($_POST['trouble_login']) && $_POST['trouble_login']=='Submit')
{
		$msg="We received your trouble ticket. We will contact you soon.";
		header("Location:connecttrouble.php?msg=".$msg);	
		exit();	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Telepharmacy Solution | Remote Verification | Remote Patient Counseling</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->

   
     <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 100px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
	  <link href="assets/css/style.css" rel="stylesheet">
  </head>

  <body>

  
    <div class="container">
		<div class="row">    
		<div class="span3">&nbsp;</div>
    <div class="span6"  style="width:620px">    		
    		<center class="msg"><b><?php if(!empty($_GET['msg'])) { echo $_GET['msg'];}?></b></center>
			 <form action="" method="post" name="patient_home" class="well">		 
			 	<div class="control-group">
            	<h3>Sorry, you're having trouble. We'll help you </h3>
				</div>
				
				 <div  class="form-inline control-group">
			  	 <input type="text" name="storename" class="input-large" placeholder="Store Name" value="<?php if(!empty($_POST['storename'])){ echo $_POST['storename'];}?>">
			 	  <input type="email" name="email" class="input-large" placeholder="Email address" value="<?php if(!empty($_POST['email'])){ echo $_POST['email'];}?>">			 
			  	 <button type="submit" class="btn btn-primary" name="trouble_login" value="Submit">Submit</button>
			 	 </div>
			
			   </form>
    
    </div>
    <div class="span3"></div>
 	 </div>
    </div> 
  </body>
</html>
