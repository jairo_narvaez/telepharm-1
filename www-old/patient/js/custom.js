

function callpharma(confId,pharmaId,firstName,lastName){
		var parameters = [{ name: 'confId', value: confId }, { name: 'pharmaId', value: pharmaId },
									{ name: 'firstName', value: firstName }, { name: 'lastName', value: lastName }, 
									{ name: 'action', value: 'setCall' }];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
				window.location.reload();
			}
		});
	}
	
	function setpharam(confId){
		var parameters = [{ name: 'confId', value: confId } , { name: 'action', value: 'getActive'}];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
				if(transport != "error")
				{
					$("#connectHead").html("You are connected to "+transport);
				}
				else
				{
					$("#connectHead").html("Connecting...");
				}
			}
		});
	}
	
	function checkliveSession(confId){
		var parameters = [{ name: 'confId', value: confId } ,{ name: 'action', value: 'checkActiveSession'}];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
				if(transport == "error")
				{
					window.location.reload();
				}
			}
		});
	}
	
	function checklivePharma(confId){
		var parameters = [{ name: 'confId', value: confId } ,{ name: 'action', value: 'checkLivePharma'}];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
				if(transport != "error")
				{
					var strarray =transport.split("*");
					/* to set new pharma from queue*/
	                    timerId2 = setTimeout(function(){
	                    	changePharma(strarray[2],confId);
	                    },30000);
                    /* end */
				}
				else
					setTimeout(function() {checklivePharma(confId);},5000);
					
			}
		});
	}
	
	function changePharma(queueId,confId){
		var parameters = [{ name: 'queueId', value: queueId },{ name: 'confId', value: confId } ,{ name: 'action', value: 'changePharma'}];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
				if(transport == "error")
				{
					checklivePharma(confId);
				}
			}
		});
	}


	function initiateCall(storeId){
		
		$(".btn-speak").hide();
		var parameters = [{ name: 'storeId', value: storeId },{ name: 'action', value: 'initiatePatientCall'}];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
					window.location = "loggedin.php";
			}
		});
	}
	
	function closeConf(confId){
		var parameters = [{ name: 'confId', value: confId },{ name: 'action', value: 'closeConf'}];
		$.ajax({
			url: 'ajaxCall.php',
			type: 'POST', 
			data: parameters,
			success: function(transport) {
					window.location = "loggedin.php";
			}
		});
	}
