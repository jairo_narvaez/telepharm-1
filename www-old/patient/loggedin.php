<?php
require_once 'config.php';

if(empty($_SESSION['storeId']) && !isset($_SESSION['storeId']))
{
	header("Location:index.php");	
	exit();
}

/* to check for online pharmacists */
$sql = "SELECT users.id,users.firstName,users.lastName,users.photo FROM users 
				JOIN  store_access_users ON users.id = store_access_users.userId WHERE 
				users.userType='Pharmacist' && store_access_users.storeId='".$_SESSION['storeId']."' && 
				users.active='1' ORDER BY loggedon DESC LIMIT 1";
$resultPharmas = mysql_query($sql,$con);	

/* end */
/* to get session data */
$sql = "SELECT id,sessionId,apiKey,tokenid FROM videoconf WHERE videoconf.storeId = '".$_SESSION['storeId']."' && videoconf.status='active' LIMIT 1";
$result = mysql_query($sql,$con);
if($result)
$row = mysql_fetch_row($result);
/* end */
/* to get connected pharmacist info */
$sql = "SELECT pharmaId,pharmaFirstName,pharmaLastName FROM pharmaqueue WHERE 
			videoconfId = '".$row[0]."' AND startTime != '' AND connected = '1' LIMIT 1";
$result1 = mysql_query($sql,$con);
$name="";
if($result1){
	$row2 = mysql_fetch_row($result1);
	$name=$row2[1]." ".$row2[2];
}
mysql_close($con);
/* end */
?>
<!DOCTYPE html>
<HTML>
 <HEAD>
  <TITLE>Telepharm | Patient</TITLE>
	<link href="assets/css/loader.css" rel="stylesheet">
	<link href="assets/css/main.css" rel="stylesheet">  
	<link rel="stylesheet" type="text/css" href="assets/css/slide.css"/>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<!-- <script type="text/javascript" src="js/script.js"></script>
 -->	<script src="js/custom.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="js/cufon-yui.js"></script>
	<script type="text/javascript" src="js/Century_Gothic_700.font.js"></script>
	<script src='http://static.opentok.com/v0.92-alpha/js/TB.min.js' type='text/javascript'></script>   
<script type="text/javascript">
	Cufon.replace('.text', {  fontFamily:'Century Gothic' });   

</script>
 


</head>

<!-- main continer of the page -->
	<?php
	if(!empty($row)){
		?>  	
<body>

<div id="wrapper" >
  <div id="header">
    <h1 id="logo"> <a href="#">&nbsp;</a> </h1>
  </div>
  <!-- Maincontent starts -->
  <div id="crxVideo">
  	<div id="maincont" class="ovfl-hidden">
		<script type="text/javascript">
		    $("#crxVideo").addClass("loading");
			var VIDEO_WIDTH = 680;
			var VIDEO_HEIGHT =440;
			var publisher;
				var apiKey = '<?php echo $row[2];?>';
				var sessionId = '<?php echo $row[1];?>';
				var token = '<?php echo $row[3];?>';
				var session = TB.initSession(sessionId);
				session.addEventListener('sessionConnected', sessionConnectedHandler);
				session.addEventListener('streamCreated', streamCreatedHandler);
				session.connect(apiKey, token);
				 TB.setLogLevel(TB.DEBUG);
				function sessionConnectedHandler(event) {
				    session.publish("mypublisher");
				  $("#crxVideo").removeClass("loading");
				    $(".marg").show();
				 	for (var i = 0; i < event.streams.length; i++) {
				  		if (session.connection.connectionId!=event.streams[i].connection.connectionId) {
				     		subscribeToStreams(event.streams[i]);
				    	}
				 	 }
				}
				function streamCreatedHandler(event) {
					for (var i = 0; i < event.streams.length; i++) {
				  		if (session.connection.connectionId!=event.streams[i].connection.connectionId) {
				     		subscribeToStreams(event.streams[i]);
				    	}
				  	}
				}
				function subscribeToStreams(stream) {
					var div=document.createElement("div");
					div.setAttribute('id','stream-'+stream.streamId);
					$("#subscriber").append(div);
					publisherProps = {width: VIDEO_WIDTH, height: VIDEO_HEIGHT};
					session.subscribe(stream,div.id,publisherProps);
					setpharam('<?php echo $row[0];?>');
				}
		</script> <?php
	}
	else
	{?>
	<body style="background:url('images/background.png'); !important;">

		<div id="header1">
		  	<!-- jQuery handles to place the header background images -->
			<!-- <div id="headerimgs">
				<div id="headerimg1" class="headerimg"></div>
				<div id="headerimg2" class="headerimg"></div>
			</div> -->
			<!-- Top navigation on top of the images -->
			<div id="nav-outer">
				<div id="navigation">
					<div id="slideLogo"></div>
					<div id="pharm">
						<?php
							if (mysql_num_rows($resultPharmas) == 0)
								echo "<h2 class='text'>Currently there is no pharmacist online</h2>";
							else
							{
								$resultPharmas = mysql_fetch_assoc($resultPharmas);
								?>
								<div id="content">
									<h2 class="text">You're local pharmacist today is <?php echo $resultPharmas['firstName']." ".$resultPharmas['lastName'];?></h2>
									
									<a class='btn btn-primary btn-speak text' href='javascript:void(0);' onclick="javascript:initiateCall('<?php echo $_SESSION['storeId'];?>')" >Speak to pharmacist</a>
								</div>
								<div id="search" style="margin-top: -25px;">
									<?php
										$photo = BASEURL."uploads/pharma/".$resultPharmas['photo'];
										if(empty($resultPharmas['photo']))
											$photo = BASEURL."images/noImage.jpeg";
									?>
									<img height="250px" width="250px" src="<?php echo $photo;?>" alt="Pharmacist"/>
								</div>
								<?php
							}
						?>
 
						<div id="healthTip" style="margin-top:10px;">
						<h1 class="tips text">Did you know...</h1>
							<div id="textrotator" ></div>

							<!-- <h2>health tip 1 .........</h2> -->
						</div>  
					</div>
				</div>
			</div>
	</div><?php
	}?>
		<!--	if session found start video conference -->
<?php
	if(!empty($row)){ ?>
	    <div id="contect2" class="fl marg">
	      <div class="top">
	        <div class="ovfl-hidden">
	          <h3></h3>
	        </div>
	      </div>
	      <div class="bg">
	        <div class="innercontent ovfl-hidden">
	          <div class="ovfl-hidden">
	              <div id="mypublisher"></div>
	          </div>
	        </div>
	      </div>
	      <div class="end">
	        <div>&nbsp;</div>
	      </div>
	      <div class="clr">&nbsp;</div>
	      <div>
		      	<a class="btn btn-primary" href="javascript:void(0);" onclick="closeConf('<?php echo $row[0];?>');">
		      		Close conference 
		      	</a>
	      </div>
	    </div>
	    <div id="contect" class="fr marg">
	      <div class="top"> 
	        <div class="ovfl-hidden">
	          <h3 id="connectHead">Connecting...</h3>
	        </div>
	      </div>
	      <div class="bg">
	        <div class="innercontent ovfl-hidden">
	          <div class="ovfl-hidden">
	              <div id="subscriber"></div>
	          </div>
	        </div>
	      </div>
	      <div class="end">
	        <div>&nbsp;</div>
	      </div>
	      <div class="clr">&nbsp;</div>
	    </div>
		<div class="clr">&nbsp;</div>
		    </div>
		  </div>
		  <!-- Maincontent ends -->
		</div><?php
	}?>
<!-- main continer of the page ends -->
		 <script type="text/javascript">
		$(function() {
			checklivePharma('<?php echo $row[0];?>');
			setInterval(function()
			{
				setpharam('<?php echo $row[0];?>');
				checkliveSession('<?php echo $row[0];?>');
			},20000);
			
		});
		</script>  
	</body>
</html>
<script type = "text/javascript">

var hexinput = 255; // initial color value.

quotation = new Array()
quotation[0] = "More than 125,000 Americans die each year due to presrciption medication non-adherence. That is twice the number killed in automobile accidents."
quotation[1] = "About 50% of the 2 billion prescriptions filled each year are not taken correctly."
quotation[2] = "32 million Americans use three or more medicines daily, and 75% of adults are non-adherent in one or more ways."
quotation[3] = "12% of Americans don't take medication at all after they fill the prescription, and almost 29% of Americans stop taking their medication before it runs out."
quotation[4] = "People who miss doses need 3 times as many doctor visits as others who do not, leading to increased medical costs."
quotation[5] = "Surveys of older adults indicate that 55% do not follow, in some way, their medication regimens."

function fadingtext(){ 
if(hexinput >0) { 
hexinput -=11; // increase color value
document.getElementById("textrotator").style.color="rgb("+hexinput+","+hexinput+","+hexinput+")"; // Set color value.
setTimeout("fadingtext()",200); // 200ms per step
}
else {
hexinput = 255; //reset hex value
}
}

function changetext(){
if(!document.getElementById){return}

var which = Math.round(Math.random()*(quotation.length - 1)); //select random quote
document.getElementById("textrotator").innerHTML = quotation[which]; // and display it

fadingtext();

setTimeout("changetext()",13000); // nine seconds
}

window.onload = changetext();

</script> 