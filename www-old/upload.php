<?php
if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	exit;
}

$folder = 'tmpimages/';
$filename =uniqid("", true).'.jpg';

$original = $folder.$filename;

$input = file_get_contents('php://input');

if(md5($input) == '7d4df9cc423720b7f1f3d672b89362be'){
	exit;
}

$result = file_put_contents($original, $input);
if (!$result) {
	echo '"error",	"Failed save the image. Make sure you chmod the uploads folder and its subfolders to 777."';
	exit;
}

$info = getimagesize($original);
if($info['mime'] != 'image/jpeg'){
	unlink($original);
	exit;
}

echo '"Success",'.$filename;
 
?>
