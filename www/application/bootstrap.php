<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('America/Chicago');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

Cookie::$salt = 'D1g1nt3nt-T3l3ph4rm-C00k13-S4lt';

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
    Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
	'index_file' => '',
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Load Mefworks Libraries
 */
$config = Kohana::$config->load('application');

spl_autoload_register(function($class_name)
{
    if (substr($class_name, 0, 7) == 'traits\\')
    {
        $file = Kohana::find_file('traits', strtr(strtolower(substr($class_name, 7)), ['\\' => '/', '_' => '/']));
        if ($file) require_once $file;
    }
});

require_once APPPATH . 'classes/mef/bootstrap.php';
require_once APPPATH . 'classes/as3/bootstrap.php';

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
	'di'           => MODPATH.'diglib-kohana',  // Digintent Module
	// 'auth'      => MODPATH.'auth',       // Basic authentication
	// 'cache'     => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench' => MODPATH.'codebench',  // Benchmarking tool
	// 'image'     => MODPATH.'image',      // Image manipulation
	// 'userguide' => MODPATH.'userguide',  // User guide and API documentation
	'database'     => MODPATH.'database',   // Database access
	'orm'          => MODPATH.'orm',        // Object Relationship Mapping
	'opentok'      => MODPATH.'opentok',    // TokBox SDK for PHP wrapped in kohana module
	'unittest'     => MODPATH.'unittest',   // Unit testing
	));

//Session::$default = 'database';


/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
if (PHP_SAPI == 'cli')
{
	$_SESSION = array('CLI'=>true);
	Route::set('cli-specialized', 'cli/(<controller>/(<action>(/<id>)))')->defaults(
		array(
			'directory' => 'cli',
			'controller' => 'cli',
			'action'     => 'index',
		)
	);
}
Route::set('store_pages', 'admin/company/<company_id>/store(/<action>(/<id>))')
	->defaults(array(
		'directory' => 'admin',
		'controller' => 'store',
		'action'     => 'index',
	));

Route::set('company_role_pages', 'admin/company/<company_id>/accounts(/<action>(/<id>))')
	->defaults(array(
		'directory' => 'admin',
		'controller' => 'companyrole',
		'action'     => 'index',
	));

Route::set('store_role_pages', 'admin/store/<store_id>/accounts(/<action>(/<id>))')
	->defaults(array(
		'directory' => 'admin',
		'controller' => 'storerole',
		'action'     => 'index',
	));

Route::set('roles_pages', 'admin/roles/<role_id>(/<action>(/<id>))')
	->defaults(array(
		'directory' => 'admin',
		'controller' => 'roles',
		'action'     => 'accounts',
	));

Route::set('account_roles_pages', 'admin/account/<account_id>/roles(/<action>(/<id>))')
	->defaults(array(
		'directory' => 'admin',
		'controller' => 'accountrole',
		'action'     => 'index',
	));

Route::set('admin_pages', 'admin(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory' => 'admin',
		'controller' => 'dashboard',
		'action'     => 'index',
	));

Route::set('ajax_actions', 'ajax(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory' => 'ajax',
		'controller' => 'prescription',
	));

Route::set('api_actions', 'api(/<controller>(/<action>(/<id>)))')
	->defaults(array(
		'directory' => 'api',
		'controller' => 'default',
	));
Route::set('login', 'login(/<action>(/<token>))', ['token'=> '.*'])
    ->defaults(array(
        'controller'=>'login',
        'action'=>'index'
    ));
Route::set('default', '(<controller>(/<action>(/<id>)))')
	->defaults(array(
		'controller' => 'welcome',
		'action'     => 'index',
	));
