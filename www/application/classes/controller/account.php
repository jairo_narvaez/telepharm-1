<?php
class Controller_Account extends Controller_Authenticated
{
    public function before(){
        parent::before();
    }

    public function action_index()
    {
        if(!$this->currentUser->account->loaded())
            $this->request->redirect($this->redirectUrls['default']);

        $view = View::factory('account/index');

        // necessary over $view->set, as set checks if the value is an array, and then does funky stuff
        $view->bind('account', $this->currentUser->account);

        $this->template->content = $view;
    }

	public function action_update()
	{
		$form = new Form_Account_Edit($this->currentUser->account);

		if ($this->request->method() === Request::POST && $form->validate($_POST))
		{
            $this->currentUser->account->values($form->as_array());

			//update photo
			if (isset($_FILES['image']) && !$_FILES['image']['error'] )
			{
				try
				{
					$image = Model_Image::processUpload('image',
						'account',   
						[
							'resizedCrop' => [[
								'width'  => '80',
								'height' => '80',
							]]
						]
					);
                    $this->currentUser->account->image_id = $image->id;
				}
				catch(Kohana_Exception $e )
				{
					TelePharm_Flash::instance()->message('error', $e->getMessage());
					$this->request->redirect('account/update');
				}

			}
            $this->currentUser->account->save();
			TelePharm_Flash::instance()->message('success', 'Account successfully updated');
			$this->request->redirect('account/update');
		}
		$view = View::factory('account/update');
        $company_stores = ORM::factory('store')->
            where('company_id', '=', $this->currentUser->account->company_id)->
            find_all();
        $view->company_stores = $company_stores;
        $view->user_stores = $this->currentUser->getStores();
        $view->currently_associated = false;
		$view->form = $form;
		$view->account = $this->currentUser->account;
		$this->template->content = $view;
	}

	public function action_change_password()
	{
		$form = new Form_Account_ChangePassword($this->currentUser->account);

		if ($this->request->method() === Request::POST)
		{
			if ($form->validate($_POST))
			{
				// Save changed data to account
                $this->currentUser->account->password = $form['new_password']->saneValue;
                $this->currentUser->account->updated_dt = date_create('now');
                $this->currentUser->account->save();
				TelePharm_Flash::instance()->message('success', 'Password saved');
				$this->request->redirect('/account/update');
			}
		}

		$view = View::factory('account/change_password');
		$view->form = $form;
		$this->template->content = $view;
	}

	/**
	 * Show the user it's new perspective
	 * of the site, b/c a user can have access
	 * in different ways and/or for different
	 * companies/stores
	 *
	 * @return void
	 */
	public function action_switch_perspective()
	{
		$new_perspective = $this->request->query('new_perspective');
        $web_session = new Model_AccountToken($this->currentUser->account->web_session->id);
        $web_session->set('current_perspective', $new_perspective)->update();
		$this->request->redirect(url::www());
	}

	public function action_profile_image()
	{
		$id = $this->request->param('id');
		$account = ORM::factory('Account')->where('id', '=', $id)->
			find();

		if (!$account->loaded()) throw new HTTP_Exception_404;

		$this->response->status(302)
			->headers('Location', url::account_image($account))
			->send_headers()
			->body();
		exit();

#		header('Content-Type:'.File::mime($file));
#		header('Content-Length: ' . filesize($file));
#		readfile($file);
#		exit();
	}

    public function action_logout()
    {
        $now = new DateTime();
        setcookie('session', '', $now->modify('- 1 year')->getTimestamp(), '/');
        if ($this->currentUser->account->web_session->loaded()){
            $this->currentUser->account->web_session->delete();
        }
        $this->request->redirect($this->redirectUrls['logout']);
    }
}

