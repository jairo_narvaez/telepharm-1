<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Admin super controller
 */
class Controller_Admin extends Controller_Authenticated
{
	/**
	 * Do some check for all admin controllers here
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('admin', 'view');
		$this->template->set_global('is_ko_page', true);
	}
}
