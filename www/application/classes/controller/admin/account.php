<?php

/**
 * Allows administer users for the system
 * 
 */
class Controller_Admin_Account extends Controller_Admin
{
	/**
	 * Security check for permissions
	 * to admin the accounts
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('account', 'admin');
	}

	use traits\TelePharm_Scaffold;

	public function getModelName() { return 'account'; }
	public function getHumanName() { return 'Account'; }
	public function getUrl() { return url::www('admin/account/'); }
	public function getDataProvider()
	{
		$manager = new Telepharm_Admin_AccountProvider;
		$manager->setCurrentUser($this->currentUser);
		return $manager;
	}
	public function getAdditionalActions() { return 'admin/account/additional_actions'; }
}
