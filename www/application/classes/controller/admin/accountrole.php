<?php 

/**
 * Show a list of roles a user has
 */
class Controller_Admin_AccountRole extends Controller_Admin
{
	protected $account;

	/**
	 * Check if account id exists
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$account_id = $this->request->param('account_id');
		$account = Model::factory('Account')->
			where('id', '=', $account_id)->
			find();
		if (!$account->loaded())
			throw new HTTP_Exception_404;
		$this->account = $account;
	}

	/**
	 * Show a list of roles a user has
	 * 
	 * @return void 
	 */
	public function action_index()
	{
		$view = View::factory('admin/accountrole/index');
		$result = [];
		foreach($this->account->roles->find_all() as $r)
		{
			$result[] = [
				'id' => $r['id'],
				'name' => $r['name'],
				'type' => 'Global',
				];
		}
		foreach($this->account->company_roles->find_all() as $r)
		{
			$result[] = [
				'id' => $r['role']['id'],
				'name' => $r['role']['name'],
				'type' => $r['company']['name'],
				];
		}
		foreach($this->account->store_roles->find_all() as $r)
		{
			$result[] = [
				'id' => $r['role']['id'],
				'name' => $r['role']['name'],
				'type' => $r['store']['name'],
				];
		}
		$view->rows = $result;
		$view->account = $this->account;
		$this->template->content = $view;
	}
}
