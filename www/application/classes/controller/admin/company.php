<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Company Administration class
 */
class Controller_Admin_Company extends Controller_Admin
{

	/**
	 * Security check for permissions
	 * to admin the companies
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('company', 'admin');
	}

	use traits\TelePharm_Scaffold;

	public static function getModelName() { return 'company'; }
	public static function getHumanName() { return 'Company'; }
	public static function getUrl() { return url::www('admin/company/'); }
	public static function getDataProvider() { return new Telepharm_Admin_CompanyProvider; }
	public static function getAdditionalActions() { return 'admin/company/additional_actions'; }
}
