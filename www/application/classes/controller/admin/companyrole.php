<?php

/**
 * Assign company roles to accounts
 */
class Controller_Admin_CompanyRole extends Controller_Admin
{
	protected $company = null;

	/**
	 * Check for permissions to use this controller
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('companyroles', 'assign');
		$company_id = $this->request->param('company_id');
		if ($company_id)
		{
			$this->company = Model::factory('company')->
				where('id', '=', $company_id)->
				find();

			if (! $this->company->loaded() )
				throw new HTTP_Exception_404;

			$this->template->bind_global('company', $this->company);
		}
	}

	/**
	 * Show a list of current assigned accounts
	 * 
	 * @return void
	 */
	public function action_index()
	{
		$view = View::factory('admin/companyrole/index');
		$view->rows = $this->company->company_roles->find_all()->
			as_array();
		$this->template->content = $view;
	}

	/**
	 * Show a list of unassigned accounts
	 * to add to the company with a role
	 * 
	 * @return void
	 */
	public function action_assign()
	{
		# find out if user is a super admin

		$view = View::factory('admin/companyrole/assign');

		$view->rows = Model::factory('Account')->
			where('id', 'not in', DB::expr('(SELECT `account_id` FROM `company_role` 
			WHERE `company_id` = ' . $this->company->id .')'))->
			and_where('company_id', '=', $this->company->id)->
			find_all();

		$this->template->content = $view;
	}


	/**
	 * Show a list of company roles
	 * that a user can be added/removed
	 * 
	 * @return void
	 */
	public function action_modify()
	{
		$account_id = $this->request->param('id');
		$account = Model::factory('Account')->
			where('id', '=', $account_id)->
			find();

		if ($account->company_id != $this->company->id)
		{
			throw new Exception('Unable to add user to this company');
		}

		if (! $account->loaded() )
			throw new HTTP_Exception_404;

		if ($this->request->method() == 'POST')
		{
			$post = new TelePharm_InputFilter($_POST);
			$post->sanitize([
				'roles' => [
					'callback' => function($r) {
						$role = Model::factory('Role')->
							where('id', '=', $r)->
							find();

						return $role->loaded() ? 
							$role 
							: null;
					},
					'multiple' => true,
					'unique' => true,
				]]);
			$data = $post->as_array();
			# remove previous roles
			DB::query(Database::DELETE, 
				"DELETE FROM company_role 
				WHERE company_id = {$this->company->id} 
				AND account_id = {$account->id}"
			)->execute();
			foreach($data['roles'] as $role)
			{
				$cr = Model::factory('CompanyRole');
				$cr->company_id = $this->company->id;
				$cr->account_id = $account->id;
				$cr->role_id = $role->id;
				$cr->save();
			}
			TelePharm_Flash::instance()->message('success', 'The roles of the user has been modified successfully');
			$this->request->redirect(url::www('admin/company/'.$this->company->id.'/accounts'));
		}
		$view = View::factory('admin/companyrole/modify');
		$view->rows = Model::factory('Role')->
			where('type', '=', 'company')->
			find_all()->
			as_array();
		$view->current_roles = array_map(
			function($cr) {
				return $cr->role_id;
			},	
			$this->company->company_roles->
			where('account_id', '=', $account->id)->
			find_all()->as_array()
		);
		$view->account = $account;
		$this->template->content = $view;
	}


}
