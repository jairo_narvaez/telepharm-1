<?php

/**
 * Initial screen for admins
 */
class Controller_Admin_Dashboard extends Controller_Admin
{
	/**
	 * For now show a list of links 
	 * a user has access to
	 * 
	 * @return void
	 */
	public function action_index()
	{
        $completedScripts = Model_Prescription::getCompletedByStores($this->currentUser);

        $view = View::factory('admin/dashboard/index')
            ->set('completedScripts', $completedScripts)
            ->set('interval', 'week');

		$this->template->content = $view;
	}

    /**
     * For now show a list of links
     * a user has access to
     *
     * @return void
     */
    public function action_statistic()
    {
        $interval = $this->request->param('id', 0);
        $completedScripts = Model_Prescription::getCompletedByStores($this->currentUser, $interval);

        $view = View::factory('admin/dashboard/index')
            ->set('completedScripts', $completedScripts)
            ->set('interval', $interval);

        $this->template->content = $view;
    }

}
