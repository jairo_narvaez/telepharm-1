<?php

/**
 * Allows administer prescriptions for the all system
 *
 */
class Controller_Admin_Prescription extends Controller_Admin
{
	/**
	 * Security check for permissions
	 * to admin the prescriptions
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('prescriptions', 'admin');
	}

	use traits\TelePharm_Scaffold;

	public static function getModelName() { return 'prescription'; }
	public static function getHumanName() { return 'Prescription'; }
	public static function getUrl() { return url::www('admin/prescription/'); }
	public static function getDataProvider() { return new Telepharm_Admin_PrescriptionProvider; }
	public static function getAdditionalActions() { return 'admin/prescription/additional_actions'; }
}
