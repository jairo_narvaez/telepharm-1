<?php

/**
 * Manage the roles assignation
 * to check for security
 */
class Controller_Admin_Roles extends Controller_Admin
{
	protected $role = null;

	/**
	 * Check for permissions to use this controller
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('roles', 'assign');
		$role_id = $this->request->param('role_id');
		if ($role_id)
		{
			$this->role = Model::factory('Role')->
				where('id', '=', $role_id)->
				find();

			if (! $this->role->loaded() )
				throw new HTTP_Exception_404;
		}
	}

	/**
	 * List all
	 * 
	 * @return void
	 */
	public function action_index()
	{
		$roles = Model::factory('Role')->
			where('type', '=', 'admin')->
			find_all()->
			as_array();
		$view = View::factory('admin/roles/index');
		$view->rows = $roles;
		$this->template->content = $view;
	}

	/**
	 * Show all the accounts assigned to this role
	 * 
	 * @return void
	 */
	public function action_accounts()
	{
		$view = View::factory('admin/roles/accounts');
		$view->role = $this->role;
		$view->rows = $this->role->accounts->find_all()->as_array();
		$this->template->content = $view;
	}

	/**
	 * Delete the relationship of account and role
	 * 
	 * @return void
	 */
	public function action_remove()
	{
		$role_id = $this->request->param('role_id');
		$account_id = $this->request->param('id');
		if ($this->request->method() == 'POST')
		{
			$ar = Model::factory('AccountRole')->
				where('role_id', '=', $role_id)->
				where('account_id', '=', $account_id)->
				find();

			$ar->delete();

			TelePharm_Flash::instance()->message('success', 'Account removed from Role');
		}
		else
		{
			TelePharm_Flash::instance()->message('error', 'Sorry this action should be done using POST');
		}
		$this->request->redirect(url::www('admin/roles/'.$role_id.'/accounts'));
	}

	/**
	 * Show a list of accounts to add to the role
	 * 
	 * @return void
	 */
	public function action_assign()
	{
		$role_id = $this->request->param('role_id');
		$view = View::factory('admin/roles/assign');
		$view->role = $this->role;
		$view->rows = Model::factory('Account')->
			where('id', 'not in', DB::expr('(SELECT `account_id` FROM `account_role` WHERE `role_id` = ' . $role_id .')'))->
			find_all();
		$this->template->content = $view;
	}

	/**
	 * Add the relationship of account and role
	 * 
	 * @return void
	 */
	public function action_add()
	{
		$role_id = $this->request->param('role_id');
		$account_id = $this->request->param('id');
		if ($this->request->method() == 'POST')
		{
			$ar = Model::factory('AccountRole');
			$ar->role_id = $role_id;
			$ar->account_id = $account_id;
			$ar->save();

			TelePharm_Flash::instance()->message('success', 'Account added to Role');
		}
		else
		{
			TelePharm_Flash::instance()->message('error', 'Sorry this action should be done using POST');
		}
		$this->request->redirect(url::www('admin/roles/'.$role_id.'/assign'));
	}

}
