<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Store Administration class
 */
class Controller_Admin_Store extends Controller_Admin
{

	/**
	 * Scoped controller for a specific company
	 */
	public static $companyId = null;
	public static $company = null;

	/**
	 * Security check for permissions
	 * to admin the stores 
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('store', 'admin');
		self::$companyId = $this->request->param('company_id');
		self::$company = Model::factory('Company')->
			where('id', '=', self::$companyId)->find();
		if (! self::$company->loaded() )
			throw new HTTP_Exception_404;
		$this->requestAccess(self::$company, 'view');
	}

	use traits\TelePharm_Scaffold;

	public static function getModelName() { return 'store'; }
	public static function getHumanName() { return 'Store'; }
	public static function getUrl() { return url::www('admin/company/'.self::$companyId.'/store/'); }
	public static function getDataProvider() { return new Telepharm_Admin_StoreProvider(self::$company); }
	public static function getAdditionalActions() { return 'admin/store/additional_actions'; }
}

