<?php

/**
 * Assign store roles to accounts
 */
class Controller_Admin_StoreRole extends Controller_Admin
{
	protected $store = null;

	/**
	 * Check for permissions to use this controller
	 * 
	 * @return void
	 */
	public function before()
	{
		parent::before();
		$this->requestAccess('storeroles', 'assign');
		$store_id = $this->request->param('store_id');
		if ($store_id)
		{
			$this->store = Model::factory('store')->
				where('id', '=', $store_id)->
				find();

			if (! $this->store->loaded() )
				throw new HTTP_Exception_404;

			$this->template->bind_global('store', $this->store);
		}
	}

	/**
	 * Show a list of current assigned accounts
	 * 
	 * @return void
	 */
	public function action_index()
	{
		$view = View::factory('admin/storerole/index');
		$view->rows = $this->store->store_roles->find_all()->
			as_array();
		$this->template->content = $view;
	}

	/**
	 * Show a list of unassigned accounts
	 * to add to the store with a role
	 * 
	 * @return void
	 */
	public function action_assign()
	{
		$view = View::factory('admin/storerole/assign');

		$view->rows = Model::factory('Account')->
			where('id', 'not in', DB::expr('(SELECT `account_id` FROM `store_role` WHERE `store_id` = ' . $this->store->id .')'))->
			and_where('company_id', '=', $this->store->company_id)->
			find_all();

		$this->template->content = $view;
	}


	/**
	 * Show a list of store roles
	 * that a user can be added/removed
	 * 
	 * @return void
	 */
	public function action_modify()
	{
		$account_id = $this->request->param('id');
		$account = Model::factory('Account')->
			where('id', '=', $account_id)->
			find();

		if ($account->company_id != $this->store->company_id)
		{
			throw new Exception('Unable to add user to store.');
		}

		if (! $account->loaded() )
			throw new HTTP_Exception_404;

		if ($this->request->method() == 'POST')
		{
			$post = new TelePharm_InputFilter($_POST);
			$post->sanitize([
				'roles' => [
					'callback' => function($r) {
						$role = Model::factory('Role')->
							where('id', '=', $r)->
							find();

						return $role->loaded() ? 
							$role 
							: null;
					},
					'multiple' => true,
					'unique' => true,
				]]);
			$data = $post->as_array();
			# remove previous roles
			DB::query(Database::DELETE, 
				"DELETE FROM store_role 
				WHERE store_id = {$this->store->id} 
				AND account_id = {$account->id}"
			)->execute();
			foreach($data['roles'] as $role)
			{
				$cr = Model::factory('StoreRole');
				$cr->store_id = $this->store->id;
				$cr->account_id = $account->id;
				$cr->role_id = $role->id;
				$cr->save();
			}
			TelePharm_Flash::instance()->message('success', 'The roles of the user has been modified successfully');
			$this->request->redirect(url::www('admin/store/'.$this->store->id.'/accounts'));
		}
		$view = View::factory('admin/storerole/modify');
		$view->rows = Model::factory('Role')->
			where('type', '=', 'store')->
			find_all()->
			as_array();
		$view->current_roles = array_map(
			function($cr) {
				return $cr->role_id;
			},	
			$this->store->store_roles->
			where('account_id', '=', $account->id)->
			find_all()->as_array()
		);
		$view->account = $account;
		$this->template->content = $view;
	}
}

