<?php
/**
 * Generates data for the account
 */
class Controller_AJAX_Account extends Controller_RPC
{
    public function action_pharmacist_modify_store_association()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        if ($this->acl->isAllowed($this->currentUser, 'pharmacist', 'dashboard'))
        {
            $post = new TelePharm_InputFilter($this->request->post());
            $post->sanitize([
                'store_id' => 'integer',
                'add' => 'boolean'
            ]);
            $store = new Model_Store($post['store_id']);

            //this stuff is going to have to change when we rewrite the super admin.
            $store_roles = $this->currentUser->account->store_roles->find();
            $first_store_role = $store_roles;
            $first_role = $first_store_role->role;
            $association = null;
            if ($post['add']){
                $association = Model_StoreRole::create_store_role($store, $this->currentUser->account, $first_role);
                $association = $association->save_store_role(new DateTime());
            }else{
                $association = Model_StoreRole::delete_store_role($store, $this->currentUser, $first_role);
            }

            if (count($association['errors']) == 0){
                $this->result = $association['success'];
            }else{
                $this->error = true;
                $this->messages = $association['errors'];
            }
        }else{
            throw new Kohana_HTTP_Exception_403;
        }
    }

    public function action_technician_modify_store_association()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        if ($this->acl->isAllowed($this->currentUser, 'technician', 'dashboard'))
        {
            $post = new TelePharm_InputFilter($this->request->post());
            $post->sanitize([
                'store_id' => 'integer'
            ]);
            $store = new Model_Store($post['store_id']);

            //this stuff is going to have to change when we rewrite the super admin.
            $store_roles = $this->currentUser->account->store_roles->find();
            $first_store_role = $store_roles;
            $first_role = $first_store_role->role;
            $association = Model_StoreRole::update_store_role($store, $this->currentUser, $first_role);

            if (count($association['errors']) == 0){
                $this->result = $association['success'];
            }else{
                $this->error = true;
                $this->messages = $association['errors'];
            }
        }else{
            throw new Kohana_HTTP_Exception_403;
        }
    }
}
