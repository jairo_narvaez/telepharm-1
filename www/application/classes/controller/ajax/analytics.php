<?php

/**
 * Generates data for the graphs
 */
class Controller_AJAX_Analytics extends Controller_RPC
{
	/**
	 * Return the counters for the analytics chart
	 * on the pharmacist/technician dashboard
	 *
	 * @return void
	 */
	public function action_get_for_store()
	{
		$this->result = Model_Prescription::getAnalytics($this->currentUser);
	}
}
