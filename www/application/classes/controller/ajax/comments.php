<?php

/**
 * Group comment ajax functions
 */
class Controller_AJAX_Comments extends Controller_RPC
{

	/**
	 * Add a new comment for the prescription
	 *
	 * @return void
	 */
	public function action_create()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			'text' => 'string',
		]);
		$data = $post->as_array();

        $this->result = Model_PrescriptionComment::createComment(
            $this->currentUser,
            $data['prescription_id'],
            $data['text']
        );
	}

    public function action_create_multi()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_501;

        $post = new TelePharm_InputFilter($this->request->post());
        $post->sanitize([
            'call_id'=>'integer',
            'text' => 'string',
        ]);
        $data = $post->as_array();

        $this->result = Model_PrescriptionComment::createMultiComments(
            $this->currentUser,
            $data['call_id'],
            $data['text']
        );
    }

	/**
	 * Return a list with a prescription comments
	 */
	public function action_for_prescription()
	{
		$query = new TelePharm_InputFilter($this->request->query());
		$query->sanitize([
			'prescription_id' => 'integer',
			'last_comment_id' => 'integer',
		]);

		$this->result = Model_PrescriptionComment::getNewComments(
				$this->currentUser,
				$query['prescription_id'],
				$query['last_comment_id']
			);
	}

	/**
	 * Set prescription comment as read
	 */
	public function action_set_as_read()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_comment_id' => 'integer',
			]);

		$this->result = Model_PrescriptionComment::setAsRead($this->currentUser,
			$post['prescription_comment_id']);
	}

}
