<?php
class Controller_AJAX_ExternalPharmacist extends Controller_RPC{

    public function action_fetch_external_pharmacists_store_external_store(){
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        $post = new TelePharm_InputFilter($this->request->post());
        $post->sanitize([
            'store_id' => 'integer',
            'is_internal_store' => 'boolean'
        ]);
        $post->as_array();
        //get current store of current user
        //for right now, there is only one pharmacy associated with each tech
        //we can get away with using the first value in the store list associated with the current user
        //whenever we fully implement multiple stores for each user, we will have to revisit this
        //probably have to add a "currentStore" property/function to $currentUser
        $stores = $this->currentUser->getStores();
        $current_internal_store = array_shift($stores);
        if (isset($post['store_id'])){
            $this->result = Model_ExternalPharmacist::fetch_all_for_external_store(
                $current_internal_store,
                $post['store_id']);
        }else{
            $this->result = [];
        }
    }

    public function action_fetch_all(){
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        //get current store of current user
        //for right now, there is only one pharmacy associated with each tech
        //we can get away with using the first value in the store list associated with the current user
        //whenever we fully implement multiple stores for each user, we will have to revisit this
        //probably have to add a "currentStore" property/function to $currentUser
        $stores = $this->currentUser->getStores();
        $current_internal_store = array_shift($stores);
        $this->result = ['pharmacists'=>Model_ExternalPharmacist::fetch_all_for_internal_store(
            $current_internal_store), 'current_state'=>$current_internal_store->location->state];
    }
}