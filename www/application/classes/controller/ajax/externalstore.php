<?php
class Controller_AJAX_ExternalStore extends Controller_RPC{

    public function action_fetch_all(){
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        //get current store of current user
        //for right now, there is only one pharmacy associated with each tech
        //we can get away with using the first value in the store list associated with the current user
        //whenever we fully implement multiple stores for each user, we will have to revisit this
        //probably have to add a "currentStore" property/function to $currentUser
        $stores = $this->currentUser->getStores();
        $current_internal_store = array_shift($stores);
        $this->result = Model_ExternalStore::fetch_existing($current_internal_store);
    }
}