<?php

/**
 * controller to actions related to prescription log
 */
class Controller_AJAX_Logs extends Controller_RPC
{
	/**
	 * Return a list with a prescription comments
	 */
	public function action_for_prescription()
	{
		$query = new TelePharm_InputFilter($this->request->query());
		$query->sanitize([
			'prescription_id' => 'integer',
			'last_log_id' => 'integer',
		]);

		$this->result = Model_PrescriptionLog::getNewLogs(
				$this->currentUser,
				$query['prescription_id'],
				$query['last_log_id']
			);
	}
}
