<?php

/**
 * Notifications-related ajax functions
 */
class Controller_AJAX_Notifications extends Controller_RPC
{
	public function action_get()
	{
		$query = new TelePharm_InputFilter($this->request->query());
		$query->sanitize([
			'last_notification_id'	 => 'integer', // id of last notification received
		]);

		$result = Model_Notification::getNotProcessed(
			$this->currentUser,
			$query['last_notification_id']
		);

		$this->result = $result->untaint();
	}

	public function action_get_all()
	{
		$query = new TelePharm_InputFilter($this->request->query());
		$query->sanitize([
			'page' => 'integer', // page number to get 
		]);

		$result = Model_Notification::getAll(
			$this->currentUser,
			$query['page']
		);

		$this->result = $result->untaint();
	}
	/**
	 * Set a comment as processed
	 * 
	 * @return void
	 */
	public function action_set_processed() 
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'notification_id' => 'integer',
		]);

		$this->result = Model_Notification::setProcessed(
			$this->currentUser,
			$post['notification_id']);
	}

	/**
	 * Change the is_audio_enabled flag
	 * on current user account record
	 *
	 * @return void
	 */
	public function action_set_audio_enabled()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'enabled' => 'boolean',
		]);
		$this->currentUser->account->is_audio_enabled = $post['enabled'];
		$this->currentUser->account->save();
		$this->result = $this->currentUser->account->saved();
	}
}
