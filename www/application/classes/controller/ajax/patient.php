<?php

/**
 * Ajax methods related to patient data
 */
class Controller_AJAX_Patient extends Controller_RPC
{
	/**
	 * Get the patient history from the PMS
	 * 
	 * @return array of Telepharm_PMS_Result_Prescription
	 */
	public function action_history()
	{
        throw new HTTP_Exception_404;

//		$query = new DI_InputFilter($this->request->query());
//		$query->sanitize(
//			[
//			'patient_id' => 'string',
//			'prescription_id' => 'string',
//			]
//		);
//		$patientId = $query['patient_id'];
//		$prescriptionId = $query['prescription_id'];
//		$query->rule(function ($inputfilter) {
//			if ($inputfilter['patient_id'] == '' ||
//				$inputfilter['prescription_id'] == '')
//				return [
//					'errors' => ['Missing parameters'],
//					'fields' => ['patient_id', 'prescription_id'],
//				];
//		});
//		if (!$query->check())
//			throw new Exception(implode(' ', $query->errors));
//
//		$prescription = Model_Prescription::getScopedQuery($this->currentUser)->
//			where('prescription.id', '=', $prescriptionId)->
//			find();
//
//		if (!$prescription->loaded())
//			throw new HTTP_Exception_404;
//
//		$store = $prescription->prescription_store->store;
//		$pmsQueryObject = Telepharm_PMS::factory($store);
//        $pmsQueryObject->setCurrentUser($this->currentUser);
//		$result = [];
//		$history = $pmsQueryObject->getPatientHistory($patientId);
//		if (empty($history)) $history = [];
//		foreach($history as $rx)
//		{
//			$result[] = $rx->as_array();
//		}
//
//		$this->result = $result;
	}
}
