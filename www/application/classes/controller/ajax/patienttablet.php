<?php

class Controller_Ajax_PatientTablet extends Controller_RPC
{

	/**
	 * Send a prescription to a selected tablet
	 * to use in the counseling session
	 *
	 * @return void
	 */
	public function action_initiate()
	{
		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			'prescriptions' => 'string',
			'tablet_id' => 'integer',
		]);

		$prescriptions = explode(',', $post['prescriptions']);

		$syncManager = new Telepharm_Manager_TabletSync();
		$initiated = $syncManager->initiate(
			$this->currentUser,
			$post['prescription_id'],
			$prescriptions,
			$post['tablet_id']
		);

		$this->result = [
			'success' => $initiated,
			'message' => 'Patient tablet initiated successfully',
		];
	}

		/**
	 * Send a prescription to a selected tablet
	 * to use in the counseling session
	 *
	 * @return void
	 */
	public function action_initiate_direct()
	{
		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'patient_name' => 'string',
			'pharmacist_id' => 'integer',
			'tablet_id' => 'integer',
		]);

		$syncManager = new Telepharm_Manager_TabletSync();
		$initiated = $syncManager->initiate_direct(
			$this->currentUser,
			$post['patient_name'],
			$post['pharmacist_id'],
			$post['tablet_id']
		);

		$this->result = [
			'success' => $initiated,
			'message' => 'Patient tablet initiated successfully',
		];
	}

	/**
	 * check an rx to see if there are related 
	 */
	public function action_check_rx()
	{
		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
		]);

		$prescription = ORM::factory('Prescription', $post['prescription_id']);

		$related_rx = Model_Prescription::getScopedQuery($this->currentUser)
            ->where('canceled', '=', 0)
            ->where('is_draft', '=', 0)
			->where('prescription.patient_id', '=', $prescription->patient_id)
			->where('prescription.approved_for_counsel', '=', 1)
			->where('completed', '=', 0)
            ->where('completed_transfer', '=', 0)
			->where('prescription.id', '!=', $post['prescription_id'])
            ->removeVirtualFields()
			->find_all();
        $results = [];
        foreach ($related_rx as $rx){
            $description = $rx->drug['description'];
            $result = $rx->as_array();
            $result['description'] = $description;
            $results[] = $result;
        }

		$this->result = ['prescriptions' => $results];
	}

	/**
	 * get a list of current opened sessions of currentUser's ipad's store
	 */
	public function action_current()
	{
		$syncManager = new Telepharm_Manager_TabletSync();
		$this->result = $syncManager->getUnfinishedTabletSessions(
			$this->currentUser
		);
	}

	/**
	 * deactivate a tablet to be used with other prescription
	 */
	public function action_unsync()
	{
		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'session_id' => 'integer',
		]);

		$syncManager = new Telepharm_Manager_TabletSync();
		$this->result = $syncManager->
			deactivateSession($post['session_id']);
	}
}
