<?php

/**
 * Notifications-related ajax functions
 */
class Controller_AJAX_PollingRequest extends Controller_Authenticated
{
	function action_index()
	{
		header('Content-type: application/json');

		$response = [];

		foreach ($_POST['requests'] as $request)
		{
			$hmvc = $this->request->factory($request['url'])->headers('X-Requested-With', 'XMLHttpRequest');

			if (isset($request['data']['query'])) $hmvc->query($request['data']['query']);

			if (isset($request['data']['post'])) $hmvc->post($request['data']['post']);

			try
			{
                $json_response = $hmvc->execute()->body();
				$response[] = json_decode($json_response);
			}
			catch (Exception $e)
			{
                if ($e->getCode() == 403){
                    throw new HTTP_Exception_403;
                }
				$response[]= ['error'=> 1, 'message' => $e->getMessage()];
				Log::instance()->add(Log::ERROR, json_encode(['message' => $e->getMessage(), 'trace' => $e->getTrace()]));
			}
		}

		exit(json_encode($response));
	}
}