<?php

class Controller_Ajax_Prescription extends Controller_RPC
{
	/**
	 * Return one prescription to see changes on statuses
	 * in the client side
	 *
	 * @return json
	 */
	public function action_get()
	{
		if ($this->request->method() != 'GET')
			throw new HTTP_Exception_501;

		$prescription = Model_Prescription::getScopedQuery($this->currentUser)->
			where('prescription.id', '=', $this->request->param('id'))->
			find();

		if (!$prescription->loaded())
			throw new HTTP_Exception_404;

		$prescription->addVirtualField('images');
        $prescription->addVirtualField('counseling_pharmacist_full_name');
        $prescription->addVirtualField('transfer');
        $this->result = $prescription->untaint();
	}

	/**
	 * create a new prescription based on the prescription
	 * number that is queried in the PMS system
	 */
    public function action_create()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_501;

        $post = new TelePharm_InputFilter($this->request->post());
        $post->sanitize([
            'prescription_number' => 'string',
        ]);

        $multipleMatches = Model_Prescription::searchBeforeCreate(
            $this->currentUser,
            $post['prescription_number']
        );
        $first = null;
        if (count($multipleMatches) == 1){
            $first = $multipleMatches[0];
        }
        if ((count($multipleMatches) == 0) || (isset($first) && $first['pre_draft'] == 1)){
            $manager = new Telepharm_Manager_CreatePrescription(
                $this->currentUser,
                $post['prescription_number']);
            $prescription = $manager->getPrescription();
            $this->result =
                [
                    'created' => true,
                    'id' => $prescription->id,
                    'multiple'=>false
                ];
        }else if (count($multipleMatches) > 0) {
            $this->result =
                [
                    'created' => false,
                    'multiple' => $multipleMatches,
                ];
            return;
        }
    }

	/**
	 * edit an existing prescription based on the prescription
	 * number that is queried in the PMS system
	 */
	public function action_update()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
            'edit_reason_type' => 'integer',
            'message' => 'string'
		]);
        $message = 'Edit prescription because: ';
        switch ($post['edit_reason_type']){
            case Model_PrescriptionEditType::Partial:
                $message .= 'Partial';
                break;
            break;
                case Model_PrescriptionEditType::Rejected:
                $message .= 'Rejected';
                break;
            break;
                case Model_PrescriptionEditType::Technician_Error:
                $message .= 'Technician Error';
                break;
            case Model_PrescriptionEditType::Other:
                if (strlen($post['message']) == 0){
                    throw new Exception('A reason must be supplied for other.');
                }
                $message .= 'Other - '.$post['message'];
                break;
            default:
                throw new Exception('Invalid reason');
        }
        $prescription = Model_Prescription::getScopedQuery($this->currentUser)->where('prescription.id','=', $post['prescription_id'])->find();
        if ($prescription->loaded()){
            $prescription->update_edit();
            Model_PrescriptionLog::createLog(
                $this->currentUser,
                $prescription,
                'edit-prescription',
            $message);
            $this->result = $prescription->id;
        }else{
            throw new HTTP_exception_404;
        }

	}

	/**
	 * Updates the values of prescriptions options
	 */
	public function action_update_option()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			'option_name' => array('on_hold', 'is_refill', 'is_new', 'is_partial'),
			'value' => 'integer',
		]);

		$this->result = Model_Prescription::updateOption(
			$this->currentUser,
			$post['prescription_id'],
			$post['option_name'],
			$post['value']);
	}

	/**
	 * Toggles is_high_priority
	 */
	public function action_set_high_priority()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			'value' => 'integer',
		]);
        $post = $post->as_array();

		$this->result = Model_Prescription::setHighPriority(
			$this->currentUser,
			$post['prescription_id'],
			$post['value']);
	}

	/**
	 * change the status of the prescription from is_draft = 1
	 * to is_draft = 0, this allows the prescription to be seen
	 * by other users
	 */
	public function action_submit()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
		]);

		$this->result = Model_Prescription::submit(
			$this->currentUser,
			$post['prescription_id']
		);

		if ($this->result)
		{
			TelePharm_Flash::instance()->message('success', 'Prescription was submitted successfully');
		}
	}

	/**
	 * change the status of the prescription from on_hold = 1
	 * to on_hold = 0, this allows the prescription to be seen
	 * by other users and processed by normal workflow
	 */
	public function action_re_submit()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
		]);

		$this->result = Model_Prescription::reSubmit(
			$this->currentUser,
			$post['prescription_id']
		);

		if ($this->result)
		{
			TelePharm_Flash::instance()->message('success', 'Prescription was submitted successfully');
		}
	}

	/**
	 * Move the prescription to an rejected state
	 *
	 * @return void
	 */
	public function action_cancel()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer'
		]);
		$data = $post->as_array();

		$this->result = Model_Prescription::cancel(
			$this->currentUser,
			$data['prescription_id']
		);
	}

	/**
	 * Move the prescription to an approved state
	 *
	 * @return void
	 */
	public function action_approve()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer'
		]);
		$data = $post->as_array();

		$this->result = [
			'statuses' => Model_Prescription::approve(
				$this->currentUser,
				$data['prescription_id']),
			'next_prescription' => $this->
				nextPrescriptionToApprove()
		];
	}

	/**
	 * Move the prescription to an approved state
	 *
	 * @return void
	 */
	public function action_approve_on_hold()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer'
		]);
		$data = $post->as_array();

		$this->result = [
			'statuses' => Model_Prescription::approveOnHold(
				$this->currentUser,
				$data['prescription_id']),
			'next_prescription' => $this->
				nextPrescriptionToApprove()
		];

	}

    /**
     * Move the prescription to an approved state
     *
     * @return void
     */
    public function action_required_counsel()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_501;

        $post = new Telepharm_InputFilter($this->request->post());
        $post->sanitize([
            'prescription_id' => 'integer'
        ]);
        $data = $post->as_array();

        $this->result = [
            'statuses' => Model_Prescription::requiredCounsel(
                $this->currentUser,
                $data['prescription_id']),
            'next_prescription' => $this->
                nextPrescriptionToApprove()
        ];

    }

	/**
	 * Move the prescription to an approved state
	 * requiring counsel
	 *
	 * @return void
	 */
	public function action_request_counsel()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer'
		]);
		$data = $post->as_array();

		$this->result = [
			'statuses' => Model_Prescription::approve(
				$this->currentUser,
				$data['prescription_id'],
				true),
			'next_prescription' => $this->
				nextPrescriptionToApprove()
		];

	}

	/**
	 * Move the prescription to an rejected state
	 *
	 * @return void
	 */
	public function action_reject()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer'
		]);
		$data = $post->as_array();

		$this->result = [
			'statuses' => Model_Prescription::reject(
				$this->currentUser,
				$data['prescription_id']
			),
			'next_prescription' => $this->
				nextPrescriptionToApprove()
		];
	}

	/**
	 * Search prescriptions for the current user
	 * using:
	 * - patient name
	 * - rx number
	 *
	 * @return void
	 */
	public function action_search()
	{
		$query = trim($this->request->query('q'));
		$this->result = Model_Prescription::searchPrescriptionsFor($this->currentUser, $query);
	}

	/**
	 * Search prescriptions for the current user
	 * using advanced search parameters
	 *
	 * @return void
	 */
	public function action_full_search()
	{
		$query = new TelePharm_InputFilter($this->request->query());
		$query->sanitize(
			[
			'store' => 'integer',
			'status' => ['all', 'approval', 'on-hold', 'counsel', 'completed', 'cancelled', 'archived', 'error'],
			'name' => 'string',
			'rxNumber' => 'string',
			'fromDate' => 'date',
			'toDate' => 'date',
			]
		);
		$this->result = Model_Prescription::fullSearchPrescriptions($this->currentUser, $query->as_array());
	}

	/**
	 * get the patient queue per each kind of user
	 */
	public function action_get_queue()
	{
		$this->result = Model_Prescription::get_queue($this->currentUser, $this->acl);
	}

    public function action_get_patient_history()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;

        $post = new Telepharm_InputFilter($this->request->post());
        $post->sanitize([
            'current_index' => 'integer',
            'prescription_id' => 'integer'
        ]);
        $data = $post->as_array();
        $prescription =  Model_Prescription::getScopedQuery($this->currentUser)->
            where('prescription.id', '=', $data['prescription_id'])->
            removeVirtualFields(['transfer'])->
            find();
        if (!$prescription->loaded()){
            throw new HTTP_Exception_403;
        }
        $this->result = $prescription->additionalPatientPrescriptions($this->currentUser, $data['current_index']);
    }

	/**
	 * Activate the prescription refused_counsel flag
	 */
	public function action_decline()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer'
		]);

		$this->result = Model_Prescription::completeCounsel(
				$this->currentUser,
				$post['prescription_id'],
				false
			);
	}


	/**
	 * Complete the counseling of a prescription
	 *
	 * @return void
	 */
	public function action_complete_counsel()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
            'technician_completion_type' => 'integer',
            'message' => 'string'
		]);
		$data = $post->as_array();

		$this->result = Model_Prescription::completeCounsel(
			$this->currentUser,
			$data['prescription_id'],
			true,
            $this->currentUser->account->id,
            $data['technician_completion_type'],
            $data['message']
		);
	}


	/**
	 * Update the priority of the prescription
	 *
	 * @return void
	 */
	public function action_update_priority()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			'priority' => 'integer',
		]);
		$data = $post->as_array();

		$current = new Model_Prescription($data['prescription_id']);
		$current = $current->as_array();

		$higher = $data['priority'] > $current['priority'];

		# now get a list of entities of like is_high_priority_type
		$like = Model_Prescription::getScopedQuery($this->currentUser)->
			where('show', '=', true)->
			where('completed', '=', false)->
			where('canceled', '=', false)->
			where('archived', '=', false)->
			where('is_high_priority', '=', $current['is_high_priority'])->
			order_by('priority', 'asc')->
			order_by('created_dt', 'desc')->
            removeVirtualFields(['transfer'])->
            find_all()->
			untaint();

		$i = 0;
		$carry_id = 0;
		$swapping_id = 0;

		$new_list = array();
		foreach ($like as $l)
		{
			$i++;

			if ($carry_id == $current['id'] && $higher)
			{
				$swapping_id = $l['id'];
			}

			$new_list[$l['id']] = $i;

			if ($l['id'] == $current['id'] && !$higher)
			{
				$swapping_id = $carry_id;
			}

			$carry_id = $l['id'];
		}

		if ($swapping_id)
		{
			$current_priority = $new_list[$current['id']];
			$swapping_priority = $new_list[$swapping_id];

			$new_list[$current['id']] = $swapping_priority;
			$new_list[$swapping_id] = $current_priority;
		}

		foreach ($new_list as $p_id => $priority)
		{
			Model_Prescription::updatePriority(
				$this->currentUser,
				$p_id,
				$priority
			);
		}

		$this->result = true;
	}

	/**
	 * Get the next prescription or url to go after
	 * an approval
	 * 
	 * @return String (where url should go after approve)
	 */
	private function nextPrescriptionToApprove()
	{
		$prescriptions = Model_Prescription::getScopedQuery($this->currentUser)->
			where('pre_draft', '=' , false)->
			where('is_draft', '=', false)->
			where('canceled', '=', false)->
			where('completed', '=', false)->
			where('rejected', '=', false)->
			where('approved_for_counsel', '=', false)->
			where('rejected', '=', false)->
			where('show', '=', true)->
			order_by('created_dt', 'desc')->
            removeVirtualFields(['transfer'])->
            find_all();

		foreach($prescriptions as $p)
		{
			return url::www('prescription/view/'.$p->id);
		}

		return url::www('/');
	}


	/**
	 * Create a new label for the prescription
	 *
	 * @return void
	 */
	public function action_add_label()
	{
		$label = json_decode($this->request->post()['label'], true);
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($label);
		$post->sanitize([
			'id' => 'integer',
			'label' => 'string',
			'color' => 'string',
			'checked' => 'boolean',
			'displayOrder' => 'integer',
		]);
		$data = $post->as_array();

		$prescription_id = $this->request->param('id');

		$this->result = Model_PrescriptionLabel::addToPrescription(
			$this->currentUser,
			$prescription_id,
			$data['label'],
			$data['color']
		)->as_array();
	}

	/**
	 * Remove an existing label of a prescription
	 *
	 * @return void
	 */
	public function action_remove_label()
	{
		$label = json_decode($this->request->post()['label'], true);
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($label);
		$post->sanitize([
			'id' => 'integer',
			'label' => 'string',
			'color' => 'string',
			'checked' => 'boolean',
			'displayOrder' => 'integer',
		]);
		$data = $post->as_array();

		$prescription_id = $this->request->param('id');

		$this->result = Model_PrescriptionLabel::removeFromPrescription(
			$this->currentUser,
			$prescription_id,
			$data['id']
		);
	}

	/**
	 * Upload encoded prescription image
	 */
	public function action_store_image()
	{
		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'picture_type' => 'string',
			'display_order' => 'integer',
			'prescription_id' => 'integer',
			'id' => 'integer',
		]);

		$prescription_image = Model_PrescriptionImage::storePrescriptionImage(
			$this->currentUser,
			isset($this->request->post()['picture']) ? $this->request->post()['picture'] : NULL,
			$post['id'],
			$post['prescription_id'],
			$post['display_order'],
			$post['picture_type']
			);

		$this->result = [
			'success' => $prescription_image->saved(),
			'prescription_image' => $prescription_image->as_array(),
		];
	}

    /**
     * Upload encoded prescription image
     */
    public function action_delete_image()
    {
        $post = new Telepharm_InputFilter($this->request->post());
        $post->sanitize([
            'id' => 'integer',
        ]);
        $success = Model_PrescriptionImage::deletePrescriptionImage($this->currentUser, $post['id']);
        $this->result = [
            'success' => $success
        ];
    }
	/**
	 * Mark the prescription as error
	 * and save the error comment
	 *
	 * @return void
	 */
	public function action_report_error()
	{
		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			'error_text' => 'string',
		]);

		$this->result = Model_Prescription::reportError($this->currentUser,
			$post['prescription_id'],
			$post['error_text']);
	}

	/**
	 * get pharmacist/technician names participants
	 * in this prescription
	 *
	 * @return void
	 */
	public function action_get_participants()
	{
		$get = new TelePharm_InputFilter($this->request->query());
		$get->sanitize([
			'prescription_id' => 'integer',
		]);

		$this->result = Model_Prescription::getParticipants($this->currentUser,
			$get['prescription_id']);
	}

    /**
     * get next Prescription Url in Queue
     *
     *  @return void
     */
    public function action_getNextPrescription()
    {
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_501;

        $post = new Telepharm_InputFilter($this->request->post());
        $post->sanitize([
            'prescription_id' => 'integer'
        ]);
        $data = $post->as_array();

        $this->result = [
            'next_prescription' => $this->
                nextPrescriptionInQueue($data['prescription_id'])
        ];

    }

    public function nextPrescriptionInQueue($afterId){

        $nextPrescription = Model_Prescription::getScopedQuery($this->currentUser)->
            where('is_draft', '=', false)->
            where('canceled', '=', false)->
            where('completed', '=', false)->
            where('rejected', '=', false)->
            where('approved_for_counsel', '=', false)->
            where('rejected', '=', false)->
            where('show', '=', true)->
            where('prescription.id', '<>', $afterId)->
            order_by('created_dt', 'desc')->
            limit(1)->
            removeVirtualFields(['transfer'])->
            find_all();

        foreach($nextPrescription as $p)
        {
            return url::www('prescription/view/'.$p->id);
        }

        return url::www('/');
    }
}
