<?php
class Controller_AJAX_PrescriptionImage extends Controller_RPC{
    public function action_rotate(){
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;

        $post = new TelePharm_InputFilter($this->request->post());
        $post->sanitize([
            'prescription_image_id' => 'integer',
            'rotate_direction' => 'integer'
        ]);

        $result = Model_PrescriptionImage::rotate_image($this->currentUser, $post['prescription_image_id'], $post['rotate_direction']);
        if (count($result['errors']) == 0){
            $this->result = true;
        }else{
            $this->messages = $result['errors'];
        }
    }
}