<?php
class Controller_AJAX_PrescriptionLabels extends Controller_RPC{
    public function action_fetch(){
        $post = new TelePharm_InputFilter($this->request->post());
        $post->sanitize([
            'prescription_id' => 'integer'
        ]);

        $prescription = Model_Prescription::getScopedQuery($this->currentUser)->
            where('prescription.id', '=', $post['prescription_id'])->find();
        if ($prescription->loaded()){
            $this->result =  $prescription->getLabels();
        }else{
            $this->result = 'bogus';
        }
    }
}