<?php
class Controller_AJAX_PrescriptionTransfer extends Controller_RPC{

    public function action_initiate(){

        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        $post = $this->request->post();

        //try to perform the transfer with the data that was submitted
        $result = Model_PrescriptionTransfer::initiate_transfer($this->currentUser, $post);
        //get any errors that may have occurred in the processs
        $errors = $result['errors'];
        //check if any errors happened
        if (count($errors) == 0){
            //no errors, return the PrescriptionTransfer model object in the response
            $this->result = ['transfer' => $result['transfer'], 'statuses' => $result['statuses']];
        }else{
            //there was an error in the process, send an array of errors in the response
            $this->messages = $errors;
        }
    }

    public function action_complete(){

        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;

        $post = new TelePharm_InputFilter($this->request->post());
        $post->sanitize([
            'transfer_id' => 'integer'
        ]);

        $data = $post->as_array();
        $success = Model_PrescriptionTransfer::complete_transfer(
            $this->currentUser,
            $data['transfer_id']
        );
        $errors = $success['errors'];
        if (count($errors) == 0){
            $this->result = $success;
        }else{
            $this->messages = $errors;
        }
    }

    public function action_fetch(){
        if ($this->request->method() != 'GET')
            throw new HTTP_Exception_405;

        $transfer = new Model_PrescriptionTransfer($this->request->param('id'));

        $this->result = $transfer->as_array();
    }
}