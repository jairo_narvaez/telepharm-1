<?php

class Controller_AJAX_States extends Controller_RPC{

    public function action_fetch_all(){
        if ($this->request->method() != 'POST')
            throw new HTTP_Exception_405;
        $result = Model_State::fetch_all();
        $this->result = $result;
    }
}