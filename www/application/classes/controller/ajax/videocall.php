<?php

/**
 * AJAx controller to manage a call workflow
 */
class Controller_AJAX_VideoCall extends Controller_RPC
{

	/**
	 * Get the list of users that the current
	 * user can do a video conf with
	 *
	 * @return void
	 */
	public function action_get_available_users()
	{
		$this->openBrowser();

		if ($this->acl->isAllowed($this->currentUser, 'pharmacist', 'dashboard'))
		{
			# Get all techinicians available for my stores
			$this->result = Model_Account::getAvailableUsersFor($this->currentUser, 'Technician');
		}
		else
		{
			# Get all pharmacists available for my stores# Get all techinicians available for my stores
			$this->result = Model_Account::getAvailableUsersFor($this->currentUser, 'Pharmacist');
		}
	}

	/**
	 * Get the list of users that the current
	 * user can do a video conf with
	 *
	 * @return void
	 */
	public function action_get_available_stores()
	{
		$this->openBrowser();
		
		if ($this->acl->isAllowed($this->currentUser, 'pharmacist', 'dashboard'))
		{
			# Get all techinicians available for my stores
			$this->result = Model_Account::getAvailableStoresForPharmacist($this->currentUser);
		}
		else
		{
			$this->result = [];
		}
	}

	
	/**
	 * Change the availability of the current user
	 *
	 * @return void
	 */
	public function action_toggle_availability()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$this->result = $this->currentUser->account->toggleAvailability();
	}

	/**
	 * Change the availability of the current user to unavailable (on logout)
	 *
	 * @return void
	 */
	public function action_browser_close()
	{
		if ($this->currentUser && $this->currentUser->account->is_browser_open)
		{
			$this->currentUser->account->setBrowser(0);
		}

		$this->result = 1;
	}

	/**
	 * Change the availability of the current user to unavailable (on logout)
	 *
	 * @return void
	 */
	public function action_browser_open()
	{
		$this->openBrowser();

		$this->result = 1;
	}

	/**
	 * Try to place a call with a user
	 * 
	 * @return json
	 */
	public function action_place()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'to' => 'integer',
			'type' => ['user', 'store', 'next-available'],
			]);
		$call = new Telepharm_Call($this->request->uri());
		$call->fromAccount = $this->currentUser->account->id;

		if ($post['type'] == 'user')
		{
			$call->toAccount = $post['to'];
			$placed = $call->tryCall();
		}
		else if ($post['type'] == 'store')
		{
			$call->toStore = $post['to'];
			$placed = $call->placePharmacistCall();
		}
		else if ($post['type'] == 'next-available')
		{
			$store_ids = array_keys($this->currentUser->getStores());
			$store_id = $store_ids[0];
			$call->toStore = $store_id;
			$placed = $call->placeNextPharmacistCall(true);
		}

		$this->result = [
			'placed' => $placed,
			'call_id' => $call->callQueueId,
			];
	}

	/**
	 * Try to place a call from a patient
	 * 
	 * @return json
	 */
	public function action_patient_place()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'prescription_id' => 'integer',
			]);
		$prescription = Model_Prescription::getScopedQuery($this->currentUser)->
			where('prescription.id', '=', $post['prescription_id'])->
			find();

		if (!$prescription->loaded())
			throw new Exception('Prescription to counsel not found');

		$call = new Telepharm_Call($this->request->uri());
		$call->fromAccount = $this->currentUser->account->id;
		$call->toStore = $prescription->prescription_store->store_id;
		$call->fromPrescriptionId = $prescription->id;

		$placed = $call->placePatientCall() && $call->generatePossibleDestinations(); 

		$callQueue = $call->getCallQueue();

		$this->result = [
			'placed' => $placed,
			'call_id' => $call->callQueueId,
			'call' => $callQueue->untaint(),
		];
	}

	/**
	 * Check if I got a new call
	 * 
	 * @return json
	 */
	public function action_check_incoming()
	{
		// Check incoming calls from patient to a pharmacist
        $call = new Telepharm_Call($this->request->uri());
		$callQueue = Model::factory('CallQueue')->
			where('to_store_id', 'in', array_keys($this->currentUser->stores))->
			where('call_status', '=', 'initiated')->
			where('to_account_id', '=', null)->
			find();
        if ($callQueue->loaded()){
            $call->callQueueId = $callQueue->id;
            if($call->validate_candidate($this->currentUser->account->id)){
                $currently_on_call = $call->checkCurrentCall($this->currentUser->account->id);
                if ($currently_on_call){
                    $retry = $call->haveMoreCandidates();
                    if (!$retry){
                        $call->cancel();
                    }
                    $this->result = [];
                }else{
                    $call->toAccount = $this->currentUser->account->id;
                    $call->startCall();
                    $this->result = $callQueue;
                }
            }else{
                $retry = $call->haveMoreCandidates();
                if (!$retry){
                    $call->cancel();
                }
                $this->result = [];
            }
        }
	}

	/**
	 * Check if call was answered
	 * 
	 * @return json
	 */
	public function action_check_call()
	{
		$call = new Telepharm_Call($this->request->uri());
		$call->callQueueId = $this->request->query('call_id');
		$this->result = [
            'dismissed' => $call->wasDismissed(),
			'answered' => $call->wasAnswered(),
			'call' => $call->untaint(),
		];
	}


	/**
	 * Accept a call with a user
	 * 
	 * @return json
	 */
	public function action_accept()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'call_id' => 'integer',
			]);
		$call = new Telepharm_Call($this->request->uri());
		$call->callQueueId = $post['call_id'];

		$accepted = $call->accept(); 

		$this->result = [
			'accepted' => $accepted,
			'call' => $call->untaint(),
			];
	}

	/**
	 * Dismiss a call
	 * 
	 * @return json
	 */
	public function action_dismiss()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'call_id' => 'integer',
			'action' => 'string',
			]);
		$call = new Telepharm_Call($this->request->uri());
		$call->callQueueId = $post['call_id'];

		$methodName = $post['action'];
		$dismissed = $call->$methodName(); 

		$this->result = [
			'dismissed' => $dismissed,
			];
	}

	/**
	 * HangUp a Call
	 * 
	 * @return json
	 */
	public function action_hangup()
	{
		if ($this->request->method() != 'POST')
			throw new HTTP_Exception_501;

		$post = new TelePharm_InputFilter($this->request->post());
		$post->sanitize([
			'call_id' => 'integer',
			]);
		$call = new Telepharm_Call($this->request->uri());
		$call->callQueueId = $post['call_id'];

		$hanged = $call->hangup($this->currentUser->account->id); 

		$this->result = [
			'hanged' => $hanged,
			];
	}

	/***
	 * open a user's browser
	 */
	private function openBrowser()
	{
		if (!$this->currentUser) return;
		
		if (!$this->currentUser->account->is_active || !$this->currentUser->account->is_browser_open)
		{
			$this->currentUser->account->setBrowser(1);
		}
	}
}
