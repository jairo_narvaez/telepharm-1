<?php

/**
 * API Controller to manage patient interactions
 */
class Controller_API extends Controller
{
	public $request;
	public $session;
	public $result;

	/**
	 * Sets up the environment for APIs
	 *
	 * @return void
	 */
	public function before()
	{
		$this->session = Session::instance();
		$this->request = Request::current();

		if ($this->request->is_initial())
		{
			$this->file_protocol = Kohana::$config->load('application.use_images_from');

			if ( $this->file_protocol == 's3' )
			{
				$s3_config = Kohana::$config->load('application.s3_credentials');

				if (!$s3_config)
					throw new Exception('s3_credentials are not defined');

				$wrapper = new aS3StreamWrapper();

				$wrapper->register([
					'protocol' => 's3', 
					'acl' => AmazonS3::ACL_PRIVATE, 
					'key' => $s3_config['access_key_id'],
					'secretKey' => $s3_config['secret_access_key'],
					'region' => AmazonS3::REGION_US_W1
				]);
			}
		}
	}

	/**
	 * Encodes the result and set the correct headers
	 *
	 * @return void
	 */
	public function after()
	{
		$this->response->headers('Content-type', 'application/json; charset='.Kohana::$charset);
		$this->response->body(json_encode($this->result));
	}

	/**
	 * Returns an error message and json message
	 *
	 * @return json
	 */
	public function error_message($code, $message)
	{
		# TODO use a more systemic approach here
		http_response_code($code);
		header('Content-type', 'application/json; charset='.Kohana::$charset);
		exit(json_encode(['message' => $message]));
	}
}