<?php

/**
 * this methods are call when the Computer RX
 * interface generates a new RX so here is 
 * created
 */
class Controller_API_CRX extends Controller_API
{
	protected $token = null;

	public function before()
	{
		parent::before();
		if ($this->request->uri() != 'api/crx/start_session')
		{
			$session_id = $this->request->post('session_id');

			$token = Model::factory('AuthToken')->
				where('id', '=', $session_id)->
				where('type', '=', 'crx-api')->
				where('expires_dt', '>', DB::expr('UTC_TIMESTAMP()'))->
				find();
			if (!$token->loaded())
				$this->error_message(401, 'Session not initalized');
			$this->token = $token;
		}
	}

	public function action_start_session()
	{
		$store_ip = $this->request->post('local_ip');
		// find a store with that ip
		$store = Model::factory('Store')->
			where('server_ip', '=', $store_ip)->
			find();

		if (!$store->loaded())
		{
			$this->error_message(401, 'Store not recognized');
		}

		$token = Model::factory('AuthToken');
		$token->type = 'crx-api';
		$token->account_id = $store->id; # for this token account id is store id
		$token->expires_dt = date_create('now UTC + 2 minutes');
		$token->save();

		$this->result = [
			'session_id' => $token->id
		];

	}

	public function action_end_session()
	{
		$this->token->expires_dt = date_create('now UTC');
		$this->token->save();

		$this->result = [
			'ended' => $this->token->saved()
		];
	}


	public function action_create()
	{
		$dataToCreate = $this->request->post();
		$query = DB::query(Database::SELECT, "
			select sr.account_id from store_role sr
			join role r on (sr.role_id = r.id)
			where store_id = :store_id
			and r.type = 'store'
			and r.name = 'Technician'
			limit 1
			");
		$query->param(':store_id', $this->token->account_id);
		$row = $query->execute()->current();

		if (!$row)
			$this->error_message(404, 'Store does not have a Technician');

		$accountId = $row['account_id'];

		$crxAccount = new Model_Account($accountId);
		$crxStore = new Model_Store($this->token->account_id);
		$currentUser = new CurrentUser($crxAccount);
		$currentUser->addStore($crxStore->id);

        $result = Telepharm_PMS::factory($crxStore)->
            createPreDraftPrescription($dataToCreate,
            $currentUser);
        $prescription = $result['prescription'];
        $updated = $result['update'];
        $created = !$updated;

		$this->result = [
			'created' => $created,
			'updated' => $updated,
			'prescription_id' => $prescription->id
		];
	}
}
