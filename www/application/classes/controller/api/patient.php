<?php

/**
 * API Controller to manage patient interactions
 */
class Controller_API_Patient extends Controller_API
{
    //we will need to remove this flag after both applications have successfully been deployed.
    protected $use_app_store_distributable;
    public function before(){
        parent::before();
        $config = Kohana::$config->load('application');
        $this->use_app_store_distributable = $config['use_app_store_distributable'];
    }
    /**
     * Start an api session with a valid key
     *
     * @return json
     */
    public function action_start_session()
    {
        $session_token = null;
        if ($this->use_app_store_distributable){
            $token = Security_NativeClientSession::check_native_client_session($this->request);
            if (!$token) {
                $this->error_message(401, 'Invalid client session');
            }
            $session_token = ['session_token' => $token];
        }
        $uuid = $this->request->post('device_id');

        if (!$uuid) {
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'No credentials provided');
        }

        $account = ORM::factory('PatientAPIAccount')
            ->where('uuid', '=', $uuid)
            ->find();

        if (!$account->loaded()) {
            $this->error_message($this->use_app_store_distributable ? 200 : 403, 'Invalid credentials');
        }

		$session = ORM::factory('PatientAPISession')
			->where('patient_api_account_id', '=', $account->id)
			->and_where('end_dt', '=', NULL)
			->find();

        if (!$session->loaded()) {
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'No active session found');
        }

		$type = $session->prescription_id ? 'prescription' : 'call-only';

		$this->result = ['session_id' => $session->token, 'type' => $type];
        if (isset($session_token)){
            $this->result = array_merge($this->result, $session_token);
        }
	}

	/**
	 * Send a call to a pharmacist
	 * 
	 * @return json
	 */
	public function action_send_call()
	{
		$s = $this->validateToken();

        try
        {
            # TODO: specify to_account_id here
            $call = new Telepharm_Call($this->request->uri());
            $call->fromAccount = $s['technician_id'];

            $account = ORM::factory('PatientAPIAccount', $s['patient_api_account_id']);
            $call->toStore = $account->store_id;

            if ($s['prescription_id'])
            {
                $call->fromPrescriptionId = $s['prescription_id'];
            }

            $call->fromAsPatient = 1;
            $placed = false;

            if (!$s['pharmacist_id'])
            {
                $placed = $call->placeNextPharmacistCall(false);
            }
            else
            {
                $placed = $call->placePatientCallToSpecificPharmacist($s['pharmacist_id']);
            }

            $call_queue = $call->getCallQueue();

            $num_candidates = 0;
            if ($call_queue->possible_candidates)
            {
                $possible_candidates = json_decode($call_queue->possible_candidates);
                $num_candidates = sizeof($possible_candidates);
            }

            switch ($num_candidates)
            {
                case 0:
                case 1:
                    $waiting_time = 30;
                    break;

                case 2:
                case 3:
                    $waiting_time = 30 * $num_candidates;
                    break;

                default:
                    $waiting_time = 25 * $num_candidates;
                    break;
            }

            # save the call_queue_id to the session
            $patient_api_session = new Model_PatientAPISession($s['id']);
            $patient_api_session->call_queue_id = $call_queue->id;
            $patient_api_session->save();

            $this->result = [
                'placed' => $placed,
                'token' => $call_queue->token,
                'call_session_id' => $call_queue->session_id,
                'waiting_time' => $waiting_time
            ];
        }
        catch (Exception $e)
        {
            $this->error_message(500, $e->getMessage());
        }
	}

	/**
	 * Save a signature from the tablet
	 * 
	 * @return json
	 */
	public function action_save_signature()
	{
		$s = $this->validateToken();

        $signature = $this->request->post('signature');

        if (!$signature)
        {
            $this->error_message($this->use_app_store_distributable ? 200 : 400, 'Signature must be provided');
        }

        try
        {
            $prescriptions = ORM::factory('PatientApiSessionPrescription')
                ->where('patient_api_session_id', '=', $s['id'])
                ->find_all();

            if (!$prescriptions)
            {
                throw new Exception('No prescriptions are found with this session');
            }

            $signature_id = 0;

            foreach ($prescriptions as $p)
            {
                $prescription = new Model_Prescription($p->prescription_id);

                if (!$prescription->loaded())
                {
                    throw new Exception('Prescription not found');
                }

                if (!$signature_id)
                {
                    $signature_id = $prescription->saveSignature(urldecode($signature));
                }
                else
                {
                    $prescription->signature_image_id = $signature_id;
                    $prescription->save();
                }
            }
        }
        catch (Exception $e)
        {
            $this->error_message(500, $e->getMessage());
        }

		$this->result = ['message' => 'Signature saved'];
	}

	/**
	 * Get the prescription info
	 * 
	 * @return json
	 */
	public function action_get_prescriptions()
	{
		$s = $this->validateToken();
        
        $array = [
            'patient' => [],
            'prescriptions' => []
        ];

        $prescriptions = ORM::factory('PatientApiSessionPrescription')
            ->where('patient_api_session_id', '=', $s['id'])
            ->find_all();

        foreach ($prescriptions as $p)
        {
            $prescription = ORM::factory('Prescription', $p['prescription_id']);

            if (!$prescription->loaded())
            {
                $this->error_message($this->use_app_store_distributable ? 200 : 401, 'Prescription not found');
            }

            if (!$array['patient'])
            {
                $array['patient'] = [
                    'first_name' => $prescription->patient_fname,
                    'last_name' => $prescription->patient_lname
                ];
            }

            $labels = array();
            foreach ($prescription->getLabels() as $k => $v)
                $labels[]= $v['label'];

            $disp = $prescription->prescription_dispenses->find();
            $drug = $prescription->prescription_drugs->find();

            $script = [];
            $script['rxid'] = $prescription->number;
            $script['medication'] = $drug->description;
            $script['description'] = $disp->sig_expanded;
            $script['image'] = '';

            $array['prescriptions'][] = [
                'data' => $script,
                'labels' => $labels
            ];
        }
		$this->result = $array;
	}

    /**
     * Get the queue of pharmacists that are being called
     *
     * @return json
     */
    public function action_get_pharmacist_queue()
    {
        $s = $this->validateToken();

        $pharmacists = [];
        if ($s['call_queue_id'])
        {
            $call_queue = ORM::factory('CallQueue', $s['call_queue_id']);
            $account = ORM::factory('Account', $call_queue->to_account_id);
            $pharmacists[]= [
                'first_name' => $account->first_name,
                'last_name' => $account->last_name,
                'image' => $account->image_id ? $this->getImageJSON($account->image_id) : ''
            ];
        }
        else
        {
            $api_account = ORM::factory('PatientApiAccount', $s['patient_api_account_id']);
            $call_to_pharmacist_id = 0;
            $call_to_pharmacist = null;
            if (isset($s['pharmacist_id']) && $s['pharmacist_id'] > 0){
                $call_to_pharmacist_id = $s['pharmacist_id'];
            }

            $pharmacist_role = ORM::factory('Role')
                ->where('type', '=', 'store')
                ->and_where('name', '=', 'Pharmacist')
                ->find();

            if ($pharmacist_role->loaded())
            {
                $store_role = ORM::factory('StoreRole')
                    ->where('store_id', '=', $api_account->store_id)
                    ->where('role_id', '=', $pharmacist_role->id)
                    ->find_all();

                foreach ($store_role as $sr)
                {
                    $account = ORM::factory('Account', $sr->account_id);
                    if ($account->is_active && $account->is_available && $account->web_session->is_authenticated()){

                        if ($sr->account_id == $call_to_pharmacist_id){
                            $call_to_pharmacist = [
                                'first_name' => $account->first_name,
                                'last_name' => $account->last_name,
                                'image' => $account->image_id ? $this->getImageJSON($account->image_id) : ''
                            ];
                        }else{
                            $pharmacists[] = [
                                'first_name' => $account->first_name,
                                'last_name' => $account->last_name,
                                'image' => $account->image_id ? $this->getImageJSON($account->image_id) : ''
                            ];
                        }
                    }
                }
                if (isset($call_to_pharmacist)){
                    array_unshift($pharmacists, $call_to_pharmacist);
                }
            }
        }

        if (!$pharmacists)
        {
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'No pharmacists found in queue');
        }

        $this->result = ['pharmacists' => $pharmacists];
    }


    /**
	 * Get the pharmacist info
	 * 
	 * @return json
	 */
	public function action_get_pharmacist()
	{
		$s = $this->validateToken();

        $call_queue = ORM::factory('CallQueue', $s['call_queue_id']);

        if (!$call_queue->loaded()) {
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'Call information not found');
        }

        $pharmacist = null;

        if ($s['pharmacist_id']) {
            $account = new Model_Account($s['pharmacist_id']);
            if ($account->loaded && $account->web_session->is_authenticated() && $account->is_available){
                $pharmacist = $account;
            }
        }
        if (!isset($pharmacist)) {
            $pharmacist = $this->getPharmacistIdFromCallQueue($s);
        }

        if (!isset($pharmacist)) {
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'Pharmacist account not found');
        }
//		# TODO: right now this will grab the first store association that matches the pharmacist.
//		# TODO: if possible it should get the store that the pharmacist is currently at.
//		$pharmacist = DB::select('account.first_name', 'account.last_name', DB::expr('store.name as store_name'), DB::expr('store.phone as store_phone'))
//			->from('account')
//			->join('store_role')->on('account.id', '=', 'store_role.account_id')
//			->join('store')->on('store.id', '=', 'store_role.store_id')
//			->join('role')->on('role.id', '=', 'store_role.role_id')
//			->where('account.id', '=', $call_queue->to_account_id)
//			->and_where('role.type', '=', 'store')
//			->and_where('role.name', '=', 'Pharmacist')
//			->execute()
//			->current();

        if ($pharmacist->loaded() && $pharmacist->web_session->is_authenticated()) {
            $this->result = ['first_name'=>$pharmacist->first_name, 'last_name'=>$pharmacist->last_name];
        }else{
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'Pharmacist not found');
        }
	}

    public function action_is_call_active()
    {
        $s = $this->validateToken();

        $active = 0;

        if ($s['call_queue_id'])
        {
            try
            {
                $call_queue = ORM::factory('CallQueue', $s['call_queue_id']);
                if ($call_queue->call_status != 'canceled' && $call_queue->complete_status == 'incomplete')
                {
                    $active = 1;
                }else{
                    $now = new DateTime();
                    if (!isset($call_queue->end_dt)){
                        $call_queue->end_dt = $now->format('Y-m-d H:i:s');
                        $call_queue->save();
                    }
                }
            }
            catch (Exception $e)
            {
                $this->error_message(500, $e->getMessage());
            }
        }

        $this->result = ['active' => $active];
    }

	/**
	 * Get the patient info
	 * 
	 * @return json
	 */
	public function action_get_patient()
	{
		$s = $this->validateToken();

		$this->result = json_decode($s['patient_info']);
	}

	/**
	 * Gracefully end the session
	 * 
	 * @return json
	 */
	public function action_end_session()
	{
		$s = $this->validateToken();

        $call_queue = ORM::factory('CallQueue', $s['call_queue_id']);

        # set the call queue to be complete.
        # we don't want it to ring anybody else or hang open
        $diff = null;
        if ($call_queue->loaded())
        {
            $call_queue->complete_status = 'complete';
            $now = new DateTime();
            if (!isset($call_queue->end_dt)){
                $call_queue->end_dt = $now->format('Y-m-d H:i:s');
                $call_queue->save();
            }
            $call_queue->save();
            $begin = new DateTime($call_queue->accepted_dt);
            $end = new DateTime($call_queue->end_dt);
            $diff = date_diff($end, $begin);
        }
        
		$action = $this->request->post('action');

		switch ($action)
		{
			case 'decline-counsel':
                $prescriptions = ORM::factory('PatientApiSessionPrescription')
                    ->where('patient_api_session_id', '=', $s['id'])
                    ->find_all()->untaint();

                foreach ($prescriptions as $p)
                {
                    $prescription = new Model_Prescription($p['prescription_id']);
                    if (!$prescription->completed){

                        $account = new Model_Account($prescription->approved_by_pharmacist_account_id);
                        $pharmacist = new CurrentUser($account);
                        foreach($account->store_roles->find_all() as $srole)
                        {
                            $pharmacist->addStore($srole['store_id']);
                         }
                        Model_Prescription::endCounsel($pharmacist, $prescription->id, false);
                    }
                }

                break;
			case 'complete-counsel':
            $prescriptions = ORM::factory('PatientApiSessionPrescription')
                ->where('patient_api_session_id', '=', $s['id'])
                ->find_all()->untaint();
                $account = new Model_Account($call_queue->to_account_id);
                $pharmacist = new CurrentUser($account);
                foreach($account->store_roles->find_all() as $srole)
                {
                    $pharmacist->addStore($srole['store_id']);
                }
                foreach ($prescriptions as $p)
                {
                    $prescription = Model_Prescription::getScopedQuery($pharmacist)->where('prescription.id', '=', $p['prescription_id'])->find();
                    if (!$prescription->completed){
                        Model_Prescription::endCounsel($pharmacist, $prescription->id, true, $pharmacist->account->id);
                        if (isset($diff)){
                            Model_PrescriptionLog::createLog($pharmacist, $prescription, 'video-call-ended', 'Call length: '.$diff->format('%i minutes, %s seconds'));
                        }else{
                            Model_PrescriptionLog::createLog($pharmacist, $prescription, 'video-call-ended');
                        }
                    }
                }
			break;

			default:
				# no workflow steps done
			break;
		}
        $patient_api_session = new Model_PatientAPISession($s['id']);
        $patient_api_session->end_dt = date('Y-m-d H:i:s');
        $patient_api_session->save();

		$this->result = ['message' => 'Session ended successfully'];
	}

	/**
	 * Validate token passed to ensure that session is valid
	 * 
	 * @return array
	 */
	private function validateToken()
	{
        $session_token = null;
        //we will remove this code once we have pushed this branch and released a version of the iPad app through the AppStore.
        if ($this->use_app_store_distributable) {
            $token = Security_NativeClientSession::check_native_client_session($this->request);
            if (!$token) {
                $this->error_message(401, 'Invalid client session');
            }
            $session_token = ['session_token' => $token];
        }
        $token = $this->request->post('session_id');

        if (!$token) {
            $this->error_message($this->use_app_store_distributable ? 200 : 401, 'No session identifier provided');
        }

		$session = ORM::factory('PatientAPISession')
			->where('token', '=', $token)
			->and_where('end_dt', '=', null)
			->find();

        if (!$session->loaded()) {
            $this->error_message($this->use_app_store_distributable ? 200 : 403, 'Invalid session identifier');
        }
        if (isset($session_token)) {
            return array_merge($session_token, $session->as_array());
        } else {
            return $session->as_array();
        }
	}

    public function action_login(){
        if ($this->use_app_store_distributable){
            $account = TelePharm_Model_Account::authenticate(
                $this->request->post('username'),
                $this->request->post('password')
            );
            if ($account->loaded()){
                $session_token = Security_NativeClientSession::create_native_client_session($account);
                if (isset($session_token)){
                    $this->result = ['session_token'=>$session_token];
                }else{
                    $this->error_message(403, 'Invalid token');
                }
            }else{
                $this->error_message(403, 'Invalid credentials');
            }
        }else{
            $this->error_message(405, 'Invalid action');
        }
    }

    /**
     * Load json image and base64 encode the contents
     * Returns empty string if image doesn't exist or if error
     *
     * @return json
     */
    private function getImageJSON($image_id)
    {
        $image = ORM::factory('Image', $image_id);

        if (!$image->loaded()) return '';

        try
        {
            $filepath = $image->getOriginalImagePath();
            $contents = file_get_contents($filepath);

            return json_encode(['type' => $image->type, 'contents' => base64_encode($contents)]);
        }
        catch (Exception $e)
        {
            return '';
        }
    }

    /**
     * finds the most recently tried pharmacist from the call queue
     *
     * @return int
     */
    private function getPharmacistIdFromCallQueue($s)
    {
        $pharmacist_id = 0;

        $call_queue = ORM::factory('CallQueue', $s['call_queue_id']);
        $next_pharmacist = null;
        if ($call_queue->possible_candidates)
        {
            $candidates = json_decode($call_queue->possible_candidates, 1);

            foreach ($candidates as $c)
            {
                if (!$c['tried'])
                {
                    $pharmacist = new Model_Account($c['account_id']);
                    if ($pharmacist->loaded() && $pharmacist->web_session->is_authenticated() && $pharmacist->is_available){
                        $next_pharmacist = $pharmacist;
                        break;

                    }
                }
            }
        }

        return $next_pharmacist;
    }
}