<?php
class Controller_Application extends Kohana_Controller_Template
{

    use traits\TelePharm_EventSource;

	/**
	 * ACL Manager
	 *
	 * @var mef\ACL\Manager
	 */
	public $acl = null;

	/**
	 * The current logged in user or null if not logged in
	 *
	 * @var CurrentUser Defaults to null.
	 */
	protected $currentUser = null;

	/**
	 * File upload protocol (null for filesystem)
	*/
	public $file_protocol = null;

    protected $redirectUrls = [
        'default' => '/',             # on exception (already logged in, etc)
        'login' => '/',               # after login
        'logout' => '/login',         # after logout
        'new_password' => '/',        # after changing password
        'unauthenticated' => '/login'
    ];

    public function before()
	{
        parent::before();
        # Wrap the redirect URLs
        foreach ($this->redirectUrls as $key => $val)
        {
            $this->redirectUrls[$key] = URL::www($val);
        }
        // Set up the event to check if we should log the pageview or not
		$request = $this->request;
		$this->addEventListener('pageview-log', function(TelePharm_Event $event) use ($request) {
			if ($request->is_ajax())
				$event->cancel();
		});

		if (PHP_SAPI !== 'cli') {
			if ($this->request->uri() != 'welcome/unsupported_browser' && !$this->request->is_ajax())
			{
				if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'chrome') === FALSE)
				{
					$this->request->redirect(url::www('welcome/unsupported_browser'));
				}
			}
		}

            //echo Debug::dump($this->request);exit;
            $this->template->title = ucwords($this->request->controller() .' '.$this->request->action());


//		$this->tmp_session = Model::factory('PHPSessionLog');
//		$this->tmp_session->php_session_id = $this->session->id();
//		$this->tmp_session->before_data = json_encode($this->session->as_array());
//		$this->tmp_session->save();


//		if ($this->currentUser->account && !isset($this->session['checked_enabled']))
//		{
//			if (! $this->currentUser->account->is_available ||
//				! $this->currentUser->account->is_audio_enabled)
//			{
//				// Is pharmacist, become available by default
//				$this->currentUser->account->is_audio_enabled = true;
//				$this->currentUser->account->is_available = true;
//				$this->currentUser->account->save();
//
//				// Notify the user about the enabled
//				Model_Notification::publishToAccount(
//					$this->currentUser,
//					'generic',
//					'You have become available',
//					$this->currentUser->account->id,
//					[]);
//			}
//			$this->session['checked_enabled'] = true;
//		}
	}

//	public function after()
//	{
//		//$this->tmp_session->after_data = json_encode($this->session->as_array());
//		//$this->tmp_session->save();
//		//$this->session->write();
//	}

    protected function _logError($error_obj) {
        ob_start();
        print_r($error_obj);
        $dump = ob_get_contents();
        ob_end_clean();
        error_log('Error: '.$dump);
    }
}
