<?php
class Controller_Authenticated extends Controller_Application
{
    public $template = 'containers/authenticated';

    public function before()
    {
        parent::before();
        //check web session
        //invalidate the currently set session identity.
        $this->currentUser = new CurrentUser(null);
        $account = Security_WebSession::check_web_session(true);
        if ($account->loaded() && $account->web_session->loaded()) {
            $this->currentUser = new CurrentUser($account);
            //authentication succeeded: proceed to set up roles.
             $this->setup($this->currentUser->account->web_session->current_perspective);
        } else {
            //authentication failed
            //for one reason or another, whatever the client supplied was not valid.
            if ($this->request->is_ajax()){
                //its a polling request, tell the browser that the session is dead.
                $this->response->body(json_encode(['kill_session'=>true]));
            }else{
                //boot the user to the login page.
                $this->request->redirect($this->redirectUrls['unauthenticated']);
            }
        }
    }


    protected function setup($perspective)
    {
        if ($this->auto_render) {

            $this->template->set_global('account', $this->currentUser->account);
            $this->template->set_global('dtFormatter', new DateTimeZone($this->currentUser->account->timezone));
        }
        # ACL
        $result = $this->loadACL($perspective);
        $this->template->bind_global('current_perspective', $result['cp']);
        $this->template->bind_global('perspectives', $result['p']);
        if ($this->auto_render) {
            $this->template->bind_global('current_user', $this->currentUser);
            $permissions = Helper_Permission::instance($this);
            $this->template->bind_global('permissions', $permissions);
            $this->template->set('controller', $this->request->controller());
            $this->template->set('action', $this->request->action());
        }

        if ($this->request->is_initial()) {
            $this->file_protocol = Kohana::$config->load('application.use_images_from');

            if ($this->file_protocol == 's3') {
                $s3_config = Kohana::$config->load('application.s3_credentials');

                if (!$s3_config)
                    throw new Exception('s3_credentials are not defined');

                $wrapper = new aS3StreamWrapper();

                $wrapper->register([
                    'protocol' => 's3',
                    'acl' => AmazonS3::ACL_PRIVATE,
                    'key' => $s3_config['access_key_id'],
                    'secretKey' => $s3_config['secret_access_key'],
                    'region' => AmazonS3::REGION_US_W1
                ]);
            }
        }

        # intervals for reload
        # TODO: build these per role
        $this->template->set('intervals', Kohana::$config->load('application.ajax_intervals'));
        $this->template->set('env', Kohana::$config->load('release.env'));
        $this->template->set('release_time', Kohana::$config->load('application.serve_js_with_release_version') ? strtotime(Kohana::$config->load('release.build')) : time());
    }

    /**
     * Prepare the code for use the ACL
     *
     * @return void
     */
    protected function loadACL($perspective = null)
    {
        # Add new acl permissions
        $this->acl = new mef\ACL\Manager();

        # Super admin doesn't require to give detail
        # of what is grant, b/c it's granted all
        $this->acl->grant('Site Administrator');
        $this->acl->revoke('Site Administrator', 'prescription', 'create');
        $this->acl->revoke('Site Administrator', 'pharmacist', 'dashboard');
        $this->acl->revoke('Site Administrator', 'technician', 'dashboard');
        $this->acl->revoke('Site Administrator', 'prescription', 'edit');
        $this->acl->revoke('Site Administrator', 'prescription', 'delete');

        # For this other roles we need to specify
        # to what they have access
        $this->acl->grant('Company Administrator', 'admin', 'view');
        $this->acl->grant('Company Administrator', 'account', 'admin');
        $this->acl->grant('Company Administrator', 'account', 'create');
        $this->acl->grant('Company Administrator', 'account', 'edit');
        $this->acl->grant('Company Administrator', 'company', 'admin');
        $this->acl->grant('Company Administrator', 'company', 'view', new Telepharm_ACL_CompanyOwnerAssertion('id'));
        $this->acl->grant('Company Administrator', 'company', 'edit');
        $this->acl->grant('Company Administrator', 'companyroles', 'assign');
        $this->acl->grant('Company Administrator', 'store', 'view', new Telepharm_ACL_CompanyOwnerAssertion('company_id'));
        $this->acl->grant('Company Administrator', 'store', 'admin');
        $this->acl->grant('Company Administrator', 'store', 'create');
        $this->acl->grant('Company Administrator', 'store', 'edit');
        $this->acl->grant('Company Administrator', 'store', 'delete');
        $this->acl->grant('Company Administrator', 'storeroles', 'assign');
        $this->acl->revoke('Company Administrator', 'pharmacist', 'dashboard');
        $this->acl->revoke('Company Administrator', 'technician', 'dashboard');
        $this->acl->revoke('Company Administrator', 'prescription', 'edit');
        $this->acl->revoke('Company Administrator', 'prescription', 'delete');

        $this->acl->grant('Pharmacist', 'pharmacist', 'dashboard');
        $this->acl->grant('Pharmacist', 'prescription', 'read-comments');
        $this->acl->grant('Pharmacist', 'prescription', 'view');
        $this->acl->grant('Pharmacist', 'prescription', 'search');

        $this->acl->grant('Technician', 'technician', 'dashboard');
        $this->acl->grant('Technician', 'prescription', 'create');
        $this->acl->grant('Technician', 'prescription', 'read-comments');
        $this->acl->grant('Technician', 'prescription', 'view');
        $this->acl->grant('Technician', 'prescription', 'search');

        $perspectives = [];
        $accountRoles = [];
        # First get all possible roles of an account
        # to use next for add Roles depending on the one selected by the
        # user

        $perspectives = $this->currentUser->refreshAvailablePerspectives($perspective);

        # Get first key as default
        reset($perspectives);
        $default = key($perspectives);
        $current_perspective = null;

        if (isset($perspective)){
            $current_perspective = $perspective;
        } else{
            $current_perspective = $default;
        }
        $accountRoles[] = $current_perspective;

        $this->acl->addRole($this->currentUser->getRoleId(), $accountRoles);
        return ['cp'=>$current_perspective, 'p'=> $perspectives];
    }

    /**
     * Check if a role can access a resource with some privilege
     */
    public function requestAccess($resource = null, $privilege = null)
    {
        if (!$this->acl->isAllowed($this->currentUser, $resource, $privilege)) {
            throw new HTTP_Exception_403();
        }
    }
}
