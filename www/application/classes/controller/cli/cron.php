<?php

/**
 * This controller manage all the cron 
 * actions
 */
class Controller_CLI_Cron extends Controller_TelePharm_CLI
{
	/**
	 * cron to purge calls not answered
	 * run with php www/index.php --uri=cli/cron/purge_invalid_calls
	 *
	 * In the crontab:
	 *
     * * * * * * <full_path_to>/php <full_path_to>/www/index.php --uri=cli/cron/purge_invalid_calls
	 *
	 */
	public function action_purge_invalid_calls()
	{
		Telepharm_Call::purgeUnAnsweredCalls();
	}
}
