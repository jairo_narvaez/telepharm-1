<?php

/**
 * CLI Controller to perform some db actions
 * 
 */
class Controller_CLI_Database extends Controller_TelePharm_CLI
{
	/**
	 * got the structure sql of the default db
	 * configured in kohana and make a mysqldump
	 * with that configuration
	 * 
	 * @return string
	 */
	public function action_dump_structure()
	{
		$db = Kohana::$config->load('database.default');

		if ($db['type'] != 'mysql')
			die("Sorry only mysql is supported.");

		$con = $db['connection'];

		$mysqldump  = "mysqldump -d -h {$con['hostname']} ";
		$mysqldump .= "-u {$con['username']} ";
		if ($con['password'] !== FALSE)
			$mysqldump .= "-p{$con['password']} ";
		$mysqldump .= "{$con['database']}";
			
		echo `$mysqldump`;
	}


	/**
	 * load the structure saved in a sql file
	 * into the testing db
	 * 
	 * @return string
	 */
	public function action_load_structure()
	{
		$file = $this->getString("");
		$db = Kohana::$config->load('database.testing');

		if ($db['type'] != 'mysql')
			die("Sorry only mysql is supported.");

		$con = $db['connection'];

		$mysqldump  = "mysql -h {$con['hostname']} ";
		$mysqldump .= "-u {$con['username']} ";
		if ($con['password'] !== FALSE)
			$mysqldump .= "-p{$con['password']} ";
		$mysqldump .= "{$con['database']} < $file";
			
		echo `$mysqldump`;
	}
}
