<?php

/**
 * here are the gearman workers used in telepharm
 */
class Controller_CLI_Gearman extends Controller_TelePharm_CLI
{
	public function action_upload_s3_server()
	{
		// register s3 handle to make the upload
		// this not happen automatically b/c this controller
		// is not extended from Controller_Application

		$s3_config = Kohana::$config->load('application.s3_credentials');

		if (!$s3_config)
			throw new Exception('s3_credentials are not defined');

		$wrapper = new aS3StreamWrapper();

		$wrapper->register([
			'protocol' => 's3', 
			'acl' => AmazonS3::ACL_PRIVATE, 
			'key' => $s3_config['access_key_id'],
			'secretKey' => $s3_config['secret_access_key'],
			'region' => AmazonS3::REGION_US_W1
			]);

		$worker = new GearmanWorker();

		$servers = Kohana::$config->load('gearman.servers');

		# servers in format host:port as each item of an array
		$worker->addServers(implode(',', $servers));

		$worker->addFunction('upload_s3', 'Controller_CLI_Gearman::upload_s3_fn');

		while (true)
		{
			print "Waiting for job...\n";

			$ret = $worker->work();
			if ($worker->returnCode() != GEARMAN_SUCCESS)
				break;
		}
	}

	public static function upload_s3_fn($job)
	{
		$workload = unserialize($job->workload());
		try
		{
			$image = new Model_Image($workload['image_id']);

			echo date_create('now UTC')->format('Y-m-d H:i:s') . ' :: ' .
				'Uploading to S3 image ' . $image['id'] . ' name: ' . $image['name'] . "\n";


			$originalSourcePath = Image::getOriginalImagesFolderPath('filesystem') . 
				$image->getPrefixByHash() . '.' . $image->type;
			$originalDestinationPath = Image::getOriginalImagesFolderPath('s3') . 
				$image->getPrefixByHash() . '.' . $image->type;

			copy($originalSourcePath, $originalDestinationPath);

			$thumbnailSourcePath = Model_Image::getThumbnailsImageFolder($image->kind, 'filesystem') .
				$image->getPrefixByHash() . '.jpeg';
			$thumbnailDestinationPath = Model_Image::getThumbnailsImageFolder($image->kind, 's3') .
				$image->getPrefixByHash() . '.jpeg';

			copy($thumbnailSourcePath, $thumbnailDestinationPath);

			echo date_create('now UTC')->format('Y-m-d H:i:s') . ' :: ' .
				'Finished upload to S3 image ' . $image['id'] . ' name: ' . $image['name'] . "\n";
		}
		catch (Exception $e)
		{
			echo 'EXCEPTION: ' . $e->getMessage() . "\n";
			return 'Failed';
		}
		return 'Uploaded to S3 image ' . $workload['image_id'];
	}
}
