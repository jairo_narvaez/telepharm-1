<?php

/**
 * Medispan related CLI actions
 */
class Controller_CLI_Medispan extends Controller_TelePharm_CLI
{
	public function action_download_files()
	{
		$download_directory = Kohana::$config->load('medispan.download_directory');
		$ruby_file_location = Kohana::$config->load('medispan.ruby_file_location');

		if (!$download_directory)
		{
			throw new Exception('You must provide a download_directory path in medispan config file');
		}

		if (!file_exists($download_directory . '/active'))
		{
			mkdir($download_directory . '/active');
		}

		# delete everything that is in the active working directory
		shell_exec('rm -rf ' . $download_directory . '/active/*');

		if ($ruby_file_location)
		{
			shell_exec('ruby ' . $ruby_file_location);
		}

		$dir_archive = date('Ymdhis');
		shell_exec('cp -R ' . $download_directory . '/active ' . $download_directory . '/archive/' . $dir_archive);

		# unzip each file
		$list = explode("\n", shell_exec('ls ' . $download_directory . '/active'));
		foreach ($list as $l)
		{
			if (strstr($l, 'image_0_0_mo_jpg_2.0_u'))
			{
				shell_exec('unzip ' . $download_directory . '/active/' . $l . ' -d ' . $download_directory . '/active/image');
				Request::factory('cli/medispan/import_image_files')->query(['action' => 'automated'])->execute();
			}
			elseif (strstr($l, 'wldb_0_0_mo_standard_1.0_u'))
			{
				shell_exec('unzip ' . $download_directory . '/active/' . $l . ' -d ' . $download_directory . '/active/label');
				Request::factory('cli/medispan/import_label_files')->query(['action' => 'automated'])->execute();
			}
		}
	}

	/**
	 * Load all files required for query
	 * images from medispan
	 * 
	 * @return void
	 */
	public function action_import_image_files()
	{
		$path_to_files = $this->getParam('images_directory');

		$image_files = ['IMG', 'DMFG', 'UDRUG', 'UDIJ'];

		do
		{
			$is_update = $this->getParam('is_update');
		}
		while($is_update != 'Y' && $is_update != 'N');

		if ($is_update == 'N')
		{
			$this->getParam('truncate_warning');
			$db = Database::instance();
			foreach($image_files as $img_name)
				$db->query(Database::DELETE, 'TRUNCATE TABLE `medispan_'.strtolower($img_name).'`');	
		}

		foreach($image_files as $img_name)
		{
			$loader = Medispan_Load::factory($img_name, $path_to_files.'/IM2'.$img_name);
			list($number, $saved) = $loader->load();
			echo "Processed $number lines and saved $saved records on file $img_name\n";
		}

		$images_dir = Kohana::$config->load('application.images');
		$filesystem = Kohana::$config->load('application.use_images_from');

		$medispan_images_path = $images_dir[$filesystem]['medispan'];
		$files = explode("\n", shell_exec("ls $path_to_files | grep '.JPG'"));
		foreach ($files as $f)
		{
			if (!$f) continue;

			# if this fails, the whole process will fail
			copy($path_to_files . '/' . $f, $medispan_images_path . $f);
		}
	}

	public function action_import_med_files()
	{
		$path_to_files = $this->getString("Path to the med files: ");
		$med_files = ['NAME', 'NDC'];

		do
		{
			$is_update = $this->getString("Files are an update? (Y/N) ");
		}
		while($is_update != 'Y' && $is_update != 'N');

		if ($is_update == 'N')
		{
			$this->getString('We are going to truncate the tables, (Y) or Ctrl+C');
			$db = Database::instance();
			foreach($med_files as $med_name)
			{
				$med_name = strtolower($med_name);
				if ($med_name == 'name') $med_name = 'drug';

				$db->query(Database::DELETE, 'TRUNCATE TABLE `medispan_mf_' . $med_name . '`');	
			}
		}

		foreach($med_files as $med_name)
		{
			$loader = Medispan_Load::factory($med_name, $path_to_files . '/MF2' . $med_name);
			list($number, $saved) = $loader->load();
			echo "Processed $number lines and saved $saved records on file $med_name\n";
		}
	}

	public function action_import_label_files()
	{
		$path_to_files = $this->getParam('labels_directory');
		$label_files = ['LBL', 'GPI'];

		do
		{
			$is_update = $this->getParam('is_update');;
		}
		while($is_update != 'Y' && $is_update != 'N');

		if ($is_update == 'N')
		{
			$this->getParam('truncate_warning');
			$db = Database::instance();
			foreach($label_files as $label_name)
			{
				$label_name = strtolower($label_name);
				if ($label_name == 'gpi') $label_name = 'lbl_gpi';

				$db->query(Database::DELETE, 'TRUNCATE TABLE `medispan_lb_' . $label_name . '`');	
			}
		}

		foreach($label_files as $label_name)
		{
			$loader = Medispan_Load::factory($label_name, $path_to_files.'/WL'.$label_name);
			list($number, $saved) = $loader->load();
			echo "Processed $number lines and saved $saved records on file $label_name\n";
		}
	}

	private function getParam($key)
	{
		$subkey = $this->request->query('action') == 'automated' ? 'automated' : 'manual';

		$download_directory = Kohana::$config->load('medispan.download_directory');

		$lookup = [
			'images_directory' => [
				'automated' => $download_directory . '/active/image',
				'manual' => 'Path to the images files: '
			],
			'labels_directory' => [
				'automated' => $download_directory . '/active/label',
				'manual' => 'Path to the label files: '
			],
			'is_update' => [
				'automated' => 'Y',
				'manual' => 'Files are an update? (Y/N): '
			],
			'truncate_warning' => [
				'automated' => false,
				'manual' => 'The tables are going to be truncated if you continue, (Y) or Ctrl+C'
			]
		];

		if (!isset($lookup[$key]))
		{
			throw new Exception('Config path does not exist');
		}

		if (!isset($lookup[$key][$subkey]))
		{
			throw new Exception('Subkey not set up for: ' . $subkey);
		}

		$val = $lookup[$key][$subkey];

		if ($val === false)
		{
			throw new Exception("Exception thrown while trying to run: $key => $subkey");
		}

		if ($subkey === 'manual')
		{
			$return = $this->getString($val);
		}
		else
		{
			$return = $val;
		}

		return $return;
	}
}
