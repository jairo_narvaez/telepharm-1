<?php

/**
 * All the actions related to a single prescription
 */
class Controller_Image extends Controller_Authenticated
{
    public function before()
    {
        //parent::before();
        //check web session
        //invalidate the currently set session identity.
        $this->currentUser = new CurrentUser(null);
        $account = Security_WebSession::check_web_session(true);
        if ($account->loaded() && $account->web_session->loaded()) {
            $this->currentUser = new CurrentUser($account);
            //authentication succeeded: proceed to set up roles.
            //$this->setup();
        } else {
            //authentication failed
            //for one reason or another, whatever the client supplied was not valid.
            //boot the user to the login page.
            $this->request->redirect($this->redirectUrls['unauthenticated']);
        }
    }

	public function action_get()
	{
		$id = $this->request->param('id');
		$original = $this->request->query('original');
		
		$image = Model::factory('Image')->
			where('id', '=', $id)->
			find();

		if (!$image->loaded())
			throw new HTTP_Exception_404('Image not found');

		if ($original == 'true')
		{
			$file_on_harddisk = $image->getOriginalImagePath();
			$file_to_download = $image->sha1 . '.' . $image->type;
			$format = $image->type;
		}
		else
		{
			$file_on_harddisk = $image->getThumbnailImagePath();
			$file_to_download = $image->sha1 . '.jpeg';
			$format = 'jpeg';
		}

		$this->serveFile($file_to_download, $file_on_harddisk, $format);
	}

	public function action_get_medispan()
	{
		$name = $this->request->query('name');
		$thumbnailPath = Model_Image::getThumbnailsImageFolder('medispan') . $name;
		$file_on_harddisk = Model_Image::getThumbnailsImageFolder('medispan', 'filesystem') . $name;
		$file_to_download = $name;
		$format = 'JPG';

		$storage = Kohana::$config->load('application.use_images_from');
		if ($storage == 's3' && !file_exists($file_on_harddisk))
		{
			// cache file locally to serve by the web server
			if (!is_dir(dirname($file_on_harddisk)))
			{
				mkdir(dirname($file_on_harddisk), 0777, true);
			}
			copy($thumbnailPath, $file_on_harddisk);
		}
		
		$this->serveFile($file_to_download, $file_on_harddisk, $format);
	}

	private function serveFile($file_to_download, $file_on_harddisk, $format)
	{
		if (!file_exists($file_on_harddisk))
			throw new HTTP_Exception_404('File not found');
        header('Pragma: public');
        header('Cache-Control: max-age=15');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 15));

		$xsend = Kohana::$config->load('application.use_x_send_file');
		if ($xsend)
		{
			header( "Content-Disposition: attachment; filename=\"" . $file_to_download . '"' );
			Header( "X-LIGHTTPD-send-file: " . $file_on_harddisk);
			exit();
		}
		else
		{
			header('Content-Length: ' . filesize($file_on_harddisk));
			header('Content-Type: image/' . $format );
			readfile($file_on_harddisk);
			exit();
		}
		
	}
}
