<?php

class Controller_Login extends Controller_UnAuthenticated
{
    public function action_index()
    {
        $form = new Form_Login_Index();
        if (count($_POST)) {
            if ($form->validate($_POST)) {
                if ($this->login($form)) {
                    $this->request->redirect($this->redirectUrls['login']);
                }
            }
            $form['remember_me']->value = $_POST['remember_me'];
        }

        $view = View::factory('login/index');
        $view->form = $form;

        $this->template->content = $view;
    }

    public function action_forgot_password()
    {
        $site_name = isset(Kohana::$config->load('application')->site_name) ?
            Kohana::$config->load('application')->site_name : "";

        $form = new Form_Login_ForgotPassword();

        if (count($_POST))
        {
            if($form->validate($_POST))
            {
               if ($form->account->loaded()){
                    Security_ForgotPasswordSession::create_forgot_password_session($form->account);
                    $view = View::factory("login/forgot_password_confirmed");
                    $view->email_address = $form->account->email;
                    $this->template->content = $view;
                    return;
               }
            }
        }

        $view = View::factory("login/forgot_password");
        $view->site_name = $site_name;
        $view->form = $form;

        $this->template->content = $view;
    }

    protected function login($form)
    {
        $account = $form->account;
        if (!$account->loaded()) return false;
        $loginEvent = new TelePharm_Event('login', $this);
        $this->dispatchEvent($loginEvent);
        if ($loginEvent->canceled)
            return false;
        return Security_WebSession::create_web_session($account, $form['remember_me']->IsChecked());
    }

    public function action_new_password()
    {
        $encrypted = rawurldecode($this->request->param('token'));
         // skip this if we don't have a token
        $account = Security_ForgotPasswordSession::check_forgot_password_session($encrypted);

        if (!($account->loaded() && $account->forgot_password_session->loaded()))
        {
            $this->request->redirect($this->redirectUrls['logout']);
        }

        // we have a valid token
        $form = new Form_Login_NewPassword($encrypted);

        if (count($_POST))
        {
            // new password submitted
            if ($form->validate($_POST))
            {
                //update the password
                $account->password = $form['password']->value;
                $account->login_dt =  date_create('now');
                //save the updated password.
                $account->save();
                //delete the session as it has been consumed.
                $account->forgot_password_session->delete();
                //the current rule is that once a user successfully changes their password, they are considered authenticated
                //thus they should be granted a web session.
                Security_WebSession::create_web_session($account, false);
                //redirect the use to the confirmation.
                $this->request->redirect($this->redirectUrls['new_password']);
            }
        }

        $view = View::factory('login/new_password');
        $view->form = $form;

        $this->template->content = $view;
    }
}