<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Patient extends Controller_Authenticated
{
	/**
	 * Patients landing page
	 */
	public function action_index()
	{
		$view = View::factory('patient/index');
		$this->template->content = $view;
	}
	/**
	 * Patient prescription page
	 */
	public function action_prescription()
	{
		$id = $this->request->param('id');
		$prescription = new Model_Prescription($id);

		if (!$prescription->loaded())
			throw new HTTP_Exception_404;

		$view = View::factory('patient/prescription');
		$prescription->addVirtualField('approved_by_pharmacist');
		$view->prescription = $prescription;
		$view->prescription_labels = $prescription->getLabels();
		$this->template->content = $view;
	}
}
