<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Pharmacist extends Controller_Authenticated
{
	/**
	 * Security Check for Pharmacist
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();

		$this->template->set_global('is_ko_page', true);
		$this->requestAccess('pharmacist', 'dashboard');
	}

	/**
	 * Show the dashboard view
	 *
	 * @return void
	 */
	public function action_index()
	{
		$view = View::factory('pharmacist/index');
		$call_id = $this->request->query('start_call');
		$call = new Telepharm_Call($this->request->uri());
		try
		{
			$call->callQueueId = $call_id;
			if ($call->readyToTalk())
				$view->call_id = $call_id;
			else
				$view->call_id = null;
		} catch (Exception $e)
		{
			$view->call_id = null;
		}
		//Get the list of prescriptions associated to the stores that the current user has access to
        $prescription_queue =  Model_Prescription::get_queue($this->currentUser, $this->acl);
		$view->prescriptions = json_encode($prescription_queue);
		$this->template->content = $view;
	}
}
