<?php

/**
 * All the actions related to a single prescription
 */
class Controller_Prescription extends Controller_Authenticated
{
	/**
	 * Security check for see a prescription
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();

		$this->template->set_global('is_ko_page', true);
		$this->requestAccess('prescription', 'view');
	}

	/**
	 * Show the counsel screen for a prescription
	 *
	 * @return void
	 */
	public function action_view()
	{
		$prescription_id = $this->request->param('id');

		$prescription = Model_Prescription::getScopedQuery($this->currentUser)->
			where('prescription.id', '=', $prescription_id);
        $prescription->addVirtualField('associated_store');
        $prescription->addVirtualField('images');
        $prescription->addVirtualField('transfer');
        $prescription->addVirtualField('signature_dt');
        $prescription->find();
		if (!$prescription->loaded())
			throw new HTTP_Exception_404;

		if (!$prescription->canceled && $prescription->is_draft)
			$this->request->redirect('prescription/create/'.$prescription_id);

		$view = View::factory('prescription/view');

		$call_id = $this->request->query('start_call');
		$call = new Telepharm_Call($this->request->uri());
		try
		{
			$call->callQueueId = $call_id;
			if ($call->readyToTalk())
				$view->call_id = $call_id;
			else
				$view->call_id = null;
		} catch (Exception $e)
		{
			$view->call_id = null;
		}

		$view->prescription = $prescription;
		$prescription->addVirtualField('images');
		$view->prescription = $prescription->as_array();
		$query = new Medispan_Query_Image();
		$view->medispan_images = $query->get($prescription->prescription_drug->new_ndc);
        $other_rx = $prescription->additionalPatientPrescriptions($this->currentUser, 0, true);
		$view->additional_prescriptions = $other_rx['results'];
        $view->total_load_count = $other_rx['total_count'];
		$view->default_labels = Kohana::$config->load('application.default_labels');
		$view->current_labels = $prescription->getLabels();
		$view->prescription_comments =
			Model_PrescriptionComment::commentsWithReadFlagByAccount($this->currentUser, $prescription->id);
		$view->prescription_logs =
			Model_PrescriptionLog::getLogForPrescription($this->currentUser, $prescription->id);
		$this->template->content = $view;
	}

	/**
	 * Prescription create page
	 */
	public function action_create()
	{
		$id = $this->request->param('id');
		$prescription =
			Model_Prescription::getScopedQuery($this->currentUser)->
				where('prescription.id', '=', $id)->
				find();

		if (!$prescription->loaded())
			throw new HTTP_Exception_404;

		if ($prescription->inFinalStatus() && !$prescription->getStatuses()['is_partial'])
		{
			$this->request->redirect('prescription/view/'.$id);
		}

		$view = View::factory('prescription/create');
		$prescription->addVirtualField('images');
		$view->prescription = $prescription->as_array();
		$query = new Medispan_Query_Image();
        //get current store of current user
        //for right now, there is only one pharmacy associated with each tech
        //we can get away with using the first value in the store list associated with the current user
        //whenever we fully implement multiple stores for each user, we will have to revisit this
        //probably have to add a "currentStore" property/function to $currentUser
        $stores = $this->currentUser->getStores();
        $current_internal_store = array_shift($stores);
        $this->template->set('current_store_rx_required_image_number', intval($current_internal_store->rx_required_image_number));
        $view->current_store_rx_num_length = intval($current_internal_store->rx_num_length);
		$view->medispan_images = $query->get($prescription->prescription_drug->new_ndc);
		$this->template->content = $view;
	}
}
