<?php

/**
 * This class is the one used to manage all
 * the ajax calls on the system
 */
class Controller_RPC extends Controller_Authenticated
{
	/**
	 * This var is going to hold the result that
	 * is going to be sent to the browser
	 *
	 * @var array Defaults to null.
	 */
	protected $result = null;
    protected $error = false;
    protected $messages;
	/**
	 * Check that the request is done using
	 * xmlhttprequest, and disable the
	 * auto render of template
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();

		if ($this->currentUser->account == null)
			throw new HTTP_Exception_403('Authentication needed');

		if (!$this->request->is_ajax())
			throw new HTTP_Exception_406('This is only allowed using XHR');

		set_exception_handler(array($this, 'exception_handler'));

		$this->auto_render = false;
	}

	/**
	 * Encodes the result and set the correct headers
	 *
	 * @return void
	 */
	public function after()
	{
		$this->response->headers('Content-type', 'application/json; charset='.Kohana::$charset);
        if (count($this->messages)==0){
            $this->response->body(json_encode(
                [
                    'error' => false,
                    'result' => $this->result,
                ]
            ));
        }else{
            $this->response->body(json_encode(
                [
                    'error' => true,
                    'message' => $this->messages,
                ]
            ));
        }
	}

	/**
	 * When an exception occur this method is call to 
	 * generate a valid error answer as json
	 */
	public function encode_exception_as_array(Exception $e)
	{
		return array(
			'error' => true,
			'message' => $e->getMessage(),
			'result' => null
		);
	}

	/**
	 * this handles the exception during the execution of
	 * a ajax request
	 */
	public function exception_handler(Exception $e)
	{
		header('Content-Type: application/json');
		$exception = $this->encode_exception_as_array($e);
		echo json_encode($exception);
		die();
	}
}
