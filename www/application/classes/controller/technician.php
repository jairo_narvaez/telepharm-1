<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Technician controller
 */
class Controller_Technician extends Controller_Authenticated
{
	/**
	 * Security Check for Technician
	 *
	 * @return void
	 */
	public function before()
	{
		parent::before();

		$this->template->set_global('is_ko_page', true);
		$this->requestAccess('technician', 'dashboard');
	}

	/**
	 * show the dashboard for the technician
	 */
	public function action_index()
	{
		$view = View::factory('technician/index');
		$call_id = $this->request->query('start_call');
		$call = new Telepharm_Call($this->request->uri());
		try
		{
			$call->callQueueId = $call_id;
			if ($call->readyToTalk())
				$view->call_id = $call_id;
			else
				$view->call_id = null;
		} catch (Exception $e)
		{
			$view->call_id = null;
		}
		//Get the list of prescriptions associated to the stores that the current user has access to
		$result = $this->result = Model_Prescription::get_queue($this->currentUser, $this->acl);
        //get current store of current user
        //for right now, there is only one pharmacy associated with each tech
        //we can get away with using the first value in the store list associated with the current user
        //whenever we fully implement multiple stores for each user, we will have to revisit this
        //probably have to add a "currentStore" property/function to $currentUser
        $stores = $this->currentUser->getStores();
        $current_internal_store = array_shift($stores);
		$view->prescriptions = json_encode($result);
        $this->template->set('current_store_rx_num_length', intval($current_internal_store->rx_num_length));
		$this->template->content = $view;
	}


	public function action_notifications()
	{
		$view = View::factory('technician/notifications');
		$this->template->title = "Technician Notifications";
		$this->template->content = $view;
	}

	public function action_comments()
	{
		$view = View::factory('technician/comments');
		$this->template->title = "Technician Comments";
		$this->template->content = $view;
	}

	public function action_fill_rx()
	{
		$view = View::factory('technician/fill_rx');
		$this->template->title = "Technician Fill Rx";
		$this->template->content = $view;
	}
}
