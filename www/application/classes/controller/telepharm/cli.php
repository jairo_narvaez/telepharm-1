<?php
class Controller_TelePharm_CLI extends Controller
{
    protected function getString($caption)
    {
        do {
            echo $caption;
            $value = trim(fgets(STDIN));
        } while (!$value);

        return $value;
    }

    protected function getEnum($caption, array $values)
    {
        do {
            $value = $this->getString($caption);
        } while (!in_array($value, $values));

        return $value;
    }

    public function before()
    {
        parent::before();

        set_exception_handler(array('Controller_TelePharm_CLI', 'handler'));

        if (PHP_SAPI != 'cli') throw new Exception_FileNotFound();

        @ob_end_clean();
    }

    public function action_config()
    {
        echo json_encode(Kohana::config($this->request->param('id'))->as_array());
    }


    /**
     * Redirect the exception output to stderr
     *
     * @param   object   exception object
     * @return  boolean
     */
    public static function handler(Exception $e)
    {
        try
        {
            fprintf(STDERR, "Exception: \n".$e->getMessage()."\n\n");

            ob_start();
            var_export(debug_backtrace());
            $debug = ob_get_clean();

            fprintf(STDERR, "Debug Trace: \n".$debug."\n\n");
            exit(1);
        }
        catch (Exception $internal)
        {
            // Clean the output buffer if one exists
            ob_get_level() and ob_clean();

            // Display the exception text
            fprintf(STDERR, "Exception: \n".Kohana_Exception::text($internal). "\n");

            // Exit with an error status
            exit(1);
        }
    }
}