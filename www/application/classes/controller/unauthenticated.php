<?php
class Controller_UnAuthenticated extends Controller_Application{
    public $template = 'containers/default';
    public function before()
    {
        parent::before();
        $account = Security_WebSession::check_web_session(true);
        if ($account->loaded()
        && $account->web_session->loaded()){
            $now = new DateTime();
            $expires = new DateTime($account->web_session->token->expires);
            if ($now < $expires){
                $this->request->redirect($this->redirectUrls['login']);
            }else{
                $account->web_session->delete();
            }
        }
    }
}