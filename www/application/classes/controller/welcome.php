<?php defined('SYSPATH') or die('No direct script access.');

/**
 * root public controller
 * here we detect the kind of user 
 * to redirect to the correct controller
 */
class Controller_Welcome extends Controller_Authenticated {
	/**
	 * Check what kind of user is logged in
	 * and present the dashboard
	 *
	 * @return void
	 */
    public $template = 'containers/authenticated';
	public function before()
	{
        $this->currentUser = new CurrentUser(null);
        $account = Security_WebSession::check_web_session(true);
        if ($account->loaded() && $account->web_session->loaded()) {
            $this->currentUser = new CurrentUser($account);
            //authentication succeeded: proceed to set up roles.
            $this->loadACL($this->currentUser->account->web_session->current_perspective);
        } else {
            //authentication failed
            //for one reason or another, whatever the client supplied was not valid.
            //boot the user to the login page.
            $this->request->redirect($this->redirectUrls['unauthenticated']);
        }
		if ($this->request->uri() == 'welcome/unsupported_browser')
		{
			return;
		}

		$query = '';
		if (isset($_SERVER['QUERY_STRING']))
		{
			$query = '?'.$_SERVER['QUERY_STRING'];
		}

		if ($this->acl->isAllowed($this->currentUser, 'pharmacist', 'dashboard'))
		{
            if (isset($this->currentUser->account->web_session->current_perspective)){
                # is a pharmacist show its dashboard
                if ($this->currentUser->account->web_session->current_perspective == 'Pharmacist'){
                    $this->request->redirect(url::www('pharmacist').$query);
                }
            }else{
                $this->request->redirect(url::www('pharmacist').$query);
            }
		}
		elseif ($this->acl->isAllowed($this->currentUser, 'technician', 'dashboard'))
		{
			# is a technician
            if (isset($this->currentUser->account->web_session->current_perspective)){
                if ($this->currentUser->account->web_session->current_perspective == 'Technician'){
                    $this->request->redirect(url::www('technician').$query);
                }
            }else{
                $this->request->redirect(url::www('technician').$query);

            }
		}
		elseif ($this->acl->isAllowed($this->currentUser, 'admin', 'view'))
		{
            # is a technician
            if (isset($this->currentUser->account->web_session->current_perspective)){
                if ($this->currentUser->account->web_session->current_perspective == 'Company Administrator' ||
                    $this->currentUser->account->web_session->current_perspective == 'Site Administrator'){
                    $this->request->redirect(url::www('admin').$query);
                }
            }else{
                $this->request->redirect(url::www('admin').$query);
            }
		}
		else
		{
			# unknown kind of user
			//$this->request->redirect(url::www('login'));
            throw new Exception("User type unknown.");
		}
	}

	public function action_unsupported_browser()
	{
		$this->template = View::factory('welcome/unsupported');
	}
} // End Welcome
