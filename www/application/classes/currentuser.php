<?php
/**
 * Class that holds information about 
 * the current logged in user
 * and it's used to check for privileges
 * using mef\ACL
 */
class CurrentUser implements mef\ACL\RoleInterface
{
	use mef\Getter;
	use mef\Setter;

	protected $__account;
	protected $__companies;
	protected $__stores;

	/**
	 * Creates a current user pointing to an account
	 * 
	 * @param Model_Account $account Optional
	 * 
	 */
	public function __construct(Model_Account $account = null)
	{
		$this->__account = $account;
		$this->__companies = [];
		$this->__stores = [];
	}

	/**
	 * Get the current account
	 * 
	 * @return Model_Account
	 */
	public function getAccount()
	{
		return $this->__account;
	}

	/**
	 * Set the account of the current user
	 * 
	 * @param Model_Account $account Optional
	 * 
	 */
	protected function setAccount(Model_Account $account)
	{
		$this->__account = $account;
	}

	/**
	 * return the name of the role used by current user
	 */
	public function getRoleId()
	{
		return 'self';
	}

	/**
	 * String representation of the current user
	 * his name
	 * 
	 * @return string
	 */
	public function __toString()
	{
		return  $this->__account->first_name . ' ' . 
				$this->__account->last_name;
	}

	/**
	 * Add a company to the list of companies
	 * a user has access to
	 * 
	 * @return void
	 */
	public function addCompany($companyId)
	{
		$company = Model::factory('Company')->
			where('id', '=', $companyId)->
			find();
		$this->__companies[$company['id']] = $company;
	}

	/**
	 * Add a store to the list of companies
	 * a user has access to
	 * 
	 * @return void
	 */
	public function addStore($storeId)
	{
		$store = Model::factory('Store')->
			where('id', '=', $storeId)->
			find();
		$this->__stores[$store['id']] = $store;
	}

	/**
	 * List of companies the user has access to
	 * 
	 * @return array
	 */
	public function getCompanies()
	{
		return $this->__companies;
	}

	/**
	 * List of stores the user has access to
	 * 
	 * @return array
	 */
	public function getStores()
	{
		return $this->__stores;
	}

	/**
	 * Query the account to get the
	 * list of perspectives available
	 * to him by the security model
	 * 
	 * @return array
	 */
	public function refreshAvailablePerspectives($perspective = null)
	{
		$this->__companies = [];
		$this->__stores = [];
		$perspectives = [];
		# First get all possible roles of an account
		# to use next for add Roles depending on the one selected by the
		# user
        $perspective_is_valid = !isset($perspective);
		if ($this->account)
		{
			foreach($this->account->roles->find_all() as $role)
			{
				$perspectives[$role['name']] = $role['name'];
                if ($role['name'] == $perspective){
                    $perspective_is_valid = true;
                }
			}

            foreach($this->account->store_roles->find_all() as $srole)
            {
                $perspectives[$srole['role']['name']] = $srole['role']['name'];
                $this->addStore($srole['store_id']);
                if (!$perspective_is_valid && $srole['role']['name'] != $perspective){
                    $perspective_is_valid = true;
                }
            }

			foreach($this->account->company_roles->find_all() as $crole)
			{
				$perspectives[$crole['role']['name']] = $crole['role']['name'];
				$this->addCompany($crole['company_id']);
                if (!$perspective_is_valid && $crole['role']['name'] != $perspective){
                    $perspective_is_valid = true;
                }
			}
		}
		return $perspectives;
	}
}
