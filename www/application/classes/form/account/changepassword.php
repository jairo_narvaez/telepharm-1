<?php defined('SYSPATH') or die('No direct access allowed.');

class Form_Account_ChangePassword extends TelePharm_HTMLForm
{
	public function __construct($account)
	{
		parent::__construct();
		$this->addField(new TelePharm_Field_Password('old_password',['label' => 'Current Password']));
		$this->addField(new TelePharm_Field_Password('new_password',['label' => 'New Password']));
		$this->addField(new TelePharm_Field_Password('confirmation',['label' => 'Confirm New Password']));

		$this->addField(new TelePharm_Field_Submit('submit', ['caption' => 'Save Password']));

		$this['old_password']->required = TRUE;
		$this['new_password']->required = TRUE;
		$this['confirmation']->required = TRUE;

		$this->rules->add(function() use ($account)
		{
			// old password field and actual old password don't match
			if (!TelePharm_Password::validate($this['old_password']->saneValue, $account->password))
			{
				return new TelePharm_RuleResult_Failure('Old password is not correct');
			}

			// new password field and confirmation field don't match
			if ($this['new_password']->saneValue != $this['confirmation']->saneValue)
			{
				return new TelePharm_RuleResult_Failure('New password and confirmation do not match');
			}

			// password actually hasn't changed
			if (TelePharm_Password::validate($this['new_password']->saneValue, $account->password))
			{
				return new TelePharm_RuleResult_Failure("Old password is equals to new password");
			}
			return new TelePharm_RuleResult_Success();
		});

	}
}
