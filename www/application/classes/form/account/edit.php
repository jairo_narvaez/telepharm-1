<?php defined('SYSPATH') or die('No direct access allowed.');

class Form_Account_Edit extends TelePharm_HTMLForm
{
	public function __construct($account)
	{
		parent::__construct();

		$this->addField(new TelePharm_Field_File('image', ['label' => 'Upload new photo']));
		$this->addField(new TelePharm_Field_Email('email', ['label' => 'Email', 'value'=> $account->email]));
		$this->addField(new TelePharm_Field_Text('first_name', ['label' => 'First Name', 'value' => $account->first_name]));
		$this->addField(new TelePharm_Field_Text('last_name', ['label' => 'Last Name', 'value' => $account->last_name]));
		$this->addField(new TelePharm_Field_Submit('submit', ['caption' => 'Save Profile']));

		$this['email']->required = TRUE;
		$this['first_name']->required = TRUE;
		$this['last_name']->required = TRUE;

	}
}
