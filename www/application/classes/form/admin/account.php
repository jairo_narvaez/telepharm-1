<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Define a form to be used in account/create
 * account/edit
 * 
 */
class Form_Admin_Account extends TelePharm_HTMLForm
{
	public $action = NULL;
	private $show_company_select = false;

	/**
	 * Adds a company select field to the form
	 */
	public function showCompanySelect($bool)
	{
		$this->show_company_select = (bool) $bool;
	}

	/**
	 * Define the form with their fields
	 */
	public function create()
	{
		$timezones = DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, 'US');
		$this->addField(new TelePharm_Field_Hidden('id', ['label' => 'ID']));
		$this->addField(new TelePharm_Field_Text('username', ['label' => 'Username']));
		$this->addField(new TelePharm_Field_Email('email', ['label' => 'Email']));
		if ($this->show_company_select)
		{
			$companies = [0 => '--Select a company--'];
			$company = ORM::factory('Company')->order_by('name', 'ASC')->find_all();
			foreach ($company as $c)
			{
				$companies[$c->id] = $c->name;
			}

			$this->addField(new TelePharm_Field_Select('company_id', ['label' => 'Company', 'options' => $companies])); 	
		}
		$this->addField(new TelePharm_Field_Select('timezone', ['label' => 'Time Zone', 'options' => array_combine($timezones, $timezones)])); 
		$this->addField(new TelePharm_Field_Password('password', ['label' => 'Password']));
		$this->addField(new TelePharm_Field_Password('confirm_password', ['label' => 'Confirm Password']));
		$this->addField(new TelePharm_Field_Text('first_name', ['label' => 'First Name']));
		$this->addField(new TelePharm_Field_Text('last_name', ['label' => 'Last Name']));
		$this->addField(new TelePharm_Field_Submit('action', ['caption' => 'Save']));

		# default value
		$this['timezone']->value = 'America/Chicago';

		$this['email']->required = true;
		$form = $this;
		$this['username']->rules->add(new TelePharm_Rule_Callback(function() {
			# Check username uniqueness
			$repeated = Model::factory('Account')->
				where('username', '=', $this['username']->saneValue)->
				where('id', '<>', $this['id']->saneValue)->
				find();
			if(!$repeated->loaded())
			{
				return new TelePharm_RuleResult_Success();
			}
			else
			{
				return new TelePharm_RuleResult_Failure('unique-username');
			}
		}));
		$this['email']->rules->add(new TelePharm_Rule_Callback(function() {
			# Check email uniqueness
			$repeated = Model::factory('Account')->
				where('email', '=', $this['email']->saneValue)->
				where('id', '<>', $this['id']->saneValue)->
				find();
			if(!$repeated->loaded())
			{
				return new TelePharm_RuleResult_Success();
			}
			else
			{
				return new TelePharm_RuleResult_Failure('unique-email');
			}
		}));
		$this['first_name']->required = true;
		$this['last_name']->required = true;
		$this['confirm_password']->rules->add(
			new TelePharm_Rule_FieldComparator($this['password'], '==')
		);
	}

	/**
	 * Define which view to render
	 */
	public function render(array $attributes = [])
	{
		return parent::render([':template'=>'admin/account', 'show_company_select' => $this->show_company_select]);
	}
}

