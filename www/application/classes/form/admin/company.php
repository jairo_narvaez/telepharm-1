<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Define a form to be used in company/new
 * company/edit
 * 
 */
class Form_Admin_Company extends TelePharm_HTMLForm
{

	public $action = 'admin/company/create';
	public $location = null;

	/**
	 * Define the form with their fields
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addField(new TelePharm_Field_Hidden('id', ['label' => 'ID']));
		$this->addField(new TelePharm_Field_Text('name', ['label' => 'Name']));
		$this->addField(new TelePharm_Field_TextArea('description', ['label' => 'Description']));
		$this->addField(new TelePharm_Field_Phone('phone', ['label' => 'Phone']));
		$this->addField(new TelePharm_Field_Text('size', ['label' => '# of Employees']));
		$this->location = new Form_Admin_Location;
		$this->addField(new TelePharm_Field_Submit('action', ['caption' => 'Save']));

		$this['name']->required = true;
		$this['description']->required = true;
		$this['phone']->required = true;
	}

	/**
	 * load the company data & location data
	 * 
	 * @param array $data 
	 */
	public function load(array $data)
	{
		parent::load($data);

		$this->location->load($data);
		if (isset($data['location']))
		{
			$this->location->load($data['location']);
			$this->location['location_id']->value = $data['location']['id'];
			$this->location['location_name']->value = $data['location']['name'];
		}
	}

	/**
	 * Define which view to render
	 */
	public function render(array $attributes = [])
	{
		return parent::render([':template'=>'admin/company']);
	}

	/**
	 * Validate the location form too
	 * 
	 * @return boolean
	 */
	public function validate(array $data = NULL)
	{
		$companyValidation = parent::validate();
		$locationValidation = $this->location->validate();
		return $companyValidation && $locationValidation;
	}
}
