<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Define a form to be used in all the places where
 * location is used
 * 
 */
class Form_Admin_Location extends TelePharm_HTMLForm
{

	/**
	 * Define the form with their fields
	 */
	public function __construct()
	{
		parent::__construct();
		$this->addField(new TelePharm_Field_Hidden('location_id', ['label' => 'ID']));
		$this->addField(new TelePharm_Field_Text('location_name', ['label' => 'Location Name']));
		$this->addField(new TelePharm_Field_Text('address1', ['label' => 'Street Address']));
		$this->addField(new TelePharm_Field_Text('address2', ['label' => 'Major Intersection']));
		$this->addField(new TelePharm_Field_Text('city', ['label' => 'City']));
		$this->addField(new TelePharm_Field_USAState('state', ['label' => 'State']));
		$this->addField(new TelePharm_Field_Text('zip', ['label' => 'Postal Code']));

		$this['location_name']->required = true;
		$this['address1']->required      = true;
		$this['city']->required          = true;
		$this['state']->required         = true;
		$this['zip']->required           = true;
	}

	/**
	 * Define which view to render
	 */
	public function render(array $attributes = [])
	{
		return parent::render([':template'=>'admin/location']);
	}
}

