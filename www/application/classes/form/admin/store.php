<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Define a form to be used in store/new
 * store/edit
 * 
 */
class Form_Admin_Store extends TelePharm_HTMLForm
{

	public $action = 'admin/store/create';
	public $location = null;
	protected $company = null;

	/**
	 * Define the form with their fields
	 */
	public function __construct(Model_Company $company)
	{
		$this->company = $company;
		parent::__construct();
		$this->addField(new TelePharm_Field_Hidden('id', ['label' => 'ID']));
		$this->addField(new TelePharm_Field_Hidden('company_id', ['label' => 'Company']));
		$this->addField(new TelePharm_Field_Text('company', ['label' => 'Company', 'value' => $this->company->name]));
		$this->addField(new TelePharm_Field_Text('name', ['label' => 'Name']));
		$this->addField(new TelePharm_Field_TextArea('description', ['label' => 'Description']));
		$this->addField(new TelePharm_Field_Text('contact', ['label' => 'Contact']));
		$this->addField(new TelePharm_Field_Phone('phone', ['label' => 'Phone']));
		$this->addField(new TelePharm_Field_Select('pms_query_class', ['label' => 'PMS Interface', 'options' => [
			'Telepharm_PMS_RxKey' => 'Rx Key',
			'Telepharm_PMS_CRX' => 'Computer Rx',
			'Telepharm_PMS_Testing' => 'Testing',
			]
			]));
		$this->addField(new TelePharm_Field_Text('url', ['label' => 'Url if Rx Key']));
		$this->addField(new TelePharm_Field_Text('server_ip', ['label' => 'Server IP if Computer RX']));
		$this->location = new Form_Admin_Location;
		$this->addField(new TelePharm_Field_Submit('action', ['caption' => 'Save']));
        $this->addField(new TelePharm_Field_Select('rx_num_length', ['label' => 'Rx Number Length', 'options' => [
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
            11 => 11,
            12 => 12
        ]
        ]));
        $this->addField(new TelePharm_Field_Select('rx_required_image_number', ['label' => 'Rx Required Image Number', 'options' => [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5
        ]
        ]));
		$this->addField(new TelePharm_Field_Submit('action', ['caption' => 'Save']));

		$this['name']->required = true;
		$this['description']->required = true;
		$this['pms_query_class']->required = true;

		$this['url']->rules->add(function() {
			if ($this['pms_query_class']->saneValue == 'Telepharm_PMS_RxKey')
			{
				if ($this['url']->saneValue == '')
					return new TelePharm_RuleResult_Failure('url required');
			}
			return new TelePharm_RuleResult_Success();
		});
		$this['server_ip']->rules->add(function() {
			if ($this['pms_query_class']->saneValue == 'Telepharm_PMS_CRX')
			{
				if ($this['server_ip']->saneValue == '')
					return new TelePharm_RuleResult_Failure('server ip required');
			}
			return new TelePharm_RuleResult_Success();
		});
	}

	/**
	 * load the store data & location data
	 * 
	 * @param array $data 
	 */
	public function load(array $data)
	{
		parent::load($data);

		$this->location->load($data);
		if (isset($data['location']))
		{
			$this->location->load($data['location']);
			$this->location['location_id']->value = $data['location']['id'];
			$this->location['location_name']->value = $data['location']['name'];
		}
	}

	/**
	 * Define which view to render
	 */
	public function render(array $attributes = [])
	{
		return parent::render([':template'=>'admin/store']);
	}

	/**
	 * Validate the location form too
	 * 
	 * @return boolean
	 */
	public function validate(array $data = NULL)
	{
		$storeValidation = parent::validate();
		$locationValidation = $this->location->validate();
		return $storeValidation && $locationValidation;
	}
}

