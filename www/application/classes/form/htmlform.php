<?php

abstract class Form_HTMLForm implements ArrayAccess, Untainted
{
	use traits\TelePharm_Getter, traits\TelePharm_Setter;

	protected $action = '';
	protected $fields = [];
	protected $types = [];
	protected $errors = [];
	protected $rules = [];

	protected $template = 'form';
	protected $__nonce;
	protected $loadedNonce;

	public function __construct()
	{
		$this->autoGetters = array_merge($this->autoGetters, ['action', 'fields', 'errors']);
		$this->rules = new TelePharm_RuleCollection();
	}

	public function offsetExists($key)
	{
		return array_key_exists($key, $this->fields);
	}

	public function offsetGet($key)
	{
		return $this->fields[$key];
	}

	public function offsetUnset($key)
	{
		unset($this->fields[$key]);
	}

	public function offsetSet($key, $val)
	{
		throw new Exception("The form array is read-only.");
	}
	
	public function addField(TelePharm_Field $field)
	{
		$this->fields[$field->id] = $field;
		$this->types[$field->id] = $field->type;
		$field->parent = $this;
	}

	public function addFields(array $fields)
	{
		foreach ($fields as $field)
			$this->addField($field);
	}

	public function getField($id)
	{
		return array_key_exists($id, $this->fields) ? $this->fields[$id] : null;
	}

	protected function getNonce()
	{
		return $this->__nonce;
	}

	public function setNonce($value)
	{
		$this->__nonce = (string) $value;
	}

	public function load(array $data)
	{
		if (isset($data['__nonce']))
			$this->loadedNonce = $data['__nonce'];

		foreach ($data as $key => $val)
		{
			if (array_key_exists($key, $this->fields))
			{
				$this->fields[$key]->value = $val;
			}
		}
	}

	public function validate(array $data = null)
	{
		if ($data !== null) $this->load($data);

		$has_errors = false;

		if ($this->nonce && $this->nonce != $this->loadedNonce)
		{
			$has_errors = true;
		}

		foreach ($this->fields as $field)
		{
			if (!$field->validate())
				$has_errors = true;
		}

		foreach ($this->rules as $rule)
		{
			$result = $rule->exec();
			if ($result->code)
			{
				$has_errors = true;
				$result['field'] = $this;
				$this->errors[] = $result;				
			}
		}

		return !$has_errors;
	}

	public function render(array $attributes = [])
	{
		$tpl = isset($attributes[':template']) ? $attributes[':template'] : $this->template;
		$view = View::factory('forms/'.$tpl);
		$view->form = $this;
		$view->attributes = $attributes;
		echo $view;
	}

	/**
	 * Render the all of the rules as a hidden field named _rules.
	 * The output could be parsed by a JavaScript library to implement client
	 * side error handling. 
	 *
	 * @return string A hidden input field containing the rules data.
	 */
	public function renderRules()
	{
		$rules = [];
		foreach ($this->fields as $field)
		{
			foreach ($field->exportRules() as $rule)
			{
				if (!isset($rules[$rule['fieldId']]))
					$rules[$rule['fieldId']] = [$rule['fieldId'], [$rule['rule']]];
				else
					$rules[$rule['fieldId']][1][] = $rule['rule'];
			}
		}
		$rules = array_values($rules);

		echo '<input type="hidden" name="_rules" value="', htmlspecialchars(json_encode($rules)), '" disabled="disabled" />';
	}

	public function as_array()
	{
		$values = [];
		foreach ($this->fields as $field)
			$values[$field->id] = $field->saneValue;

		return $values;
	}

	public function transformName(TelePharm_Field $field, $name)
	{
		return $name;
	}
}