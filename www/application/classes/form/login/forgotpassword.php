<?php
class Form_Login_ForgotPassword extends TelePharm_HTMLForm
{
	protected $account;

	public function __construct()
	{
		parent::__construct();
		$this->autoGetters[] = 'account';

		$this->action = '/login/forgot_password';

		$this->addFields([
			new TelePharm_Field_Text('email', ['label' => 'Email Address']),
			new TelePharm_Field_Submit('action', ['caption' => 'Request Change'])
		]);

		$this['email']->required = true;

		$this['email']->rules->add(function()
		{
			if ($this['email']->errors)
			{
				# Don't try to authenticate blank data
				return new TelePharm_RuleResult_Success();
			}

			$this->account = Model::factory("Account")->where('email', '=', $this['email']->saneValue)->find();
            //need to figure out a better way to get the sessions set here.
            $this->account = new Model_Account($this->account->id);
			return $this->account->loaded() ? new TelePharm_RuleResult_Success() :
				new TelePharm_RuleResult_Failure('Email address not found.');
		});
	}
}