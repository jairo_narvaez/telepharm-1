<?php
class Form_Login_Index extends TelePharm_HTMLForm
{
	protected $account;
	protected $template = 'login/index';

	public function __construct()
	{
		parent::__construct();
		$this->autoGetters[] = 'account';

		$this->addFields([
			new TelePharm_Field_Text('username', ['label' => 'Username']),
			new TelePharm_Field_Password('password', ['label' => 'Password']),
			new TelePharm_Field_Checkbox('remember_me', ['caption' => 'Remember me']),
			new TelePharm_Field_Submit('action', ['caption' => 'Log In'])
		]);

		$this['username']->required = true;
		$this['password']->required = true;

		$this['username']->rules->add(function()
		{
			if ($this['username']->errors || $this['password']->errors)
			{
				# Don't try to authenticate blank data
				return new TelePharm_RuleResult_Success();
			}

			$this->account = TelePharm_Model_Account::authenticate(
				$this['username']->saneValue,
				$this['password']->saneValue
			);

			return $this->account ? new TelePharm_RuleResult_Success() :
				new TelePharm_RuleResult_Failure('Authentication failed');
		});
	}
}
