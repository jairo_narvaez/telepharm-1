<?php
class Form_Login_NewPassword extends TelePharm_HTMLForm
{
	public function __construct($encrypted_token)
	{
		parent::__construct();
		$this->autoGetters[] = 'account';

		$this->action = '/login/new_password/'.rawurlencode($encrypted_token);

		$this->addFields([
			new TelePharm_Field_Password('password', ['label' => 'New password']),
			new TelePharm_Field_Password('confirmation', ['label' => 'Confirmation']),
			new TelePharm_Field_Submit('action', ['caption' => 'Request Change'])
		]);

		$this['password']->required = true;
		$this['confirmation']->required = true;

		$this['password']->rules->add(function()
		{
			if ($this['password']->errors || $this['confirmation']->errors)
			{
				# Don't try to authenticate blank data
				return new TelePharm_RuleResult_Success();
			}

			if ($this['password']->value != $this['confirmation']->value)
				return new TelePharm_RuleResult_Failure('Password and Confirmation do not match');

			return new TelePharm_RuleResult_Success();
		});
	}
}