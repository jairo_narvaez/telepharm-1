<?php

/**
 * encapsulate the gearman client boilerplate code
 */
class Gearman_Client
{
	public static function doBackground($function, $workload)
	{
		$client = new GearmanClient();
		$servers = Kohana::$config->load('gearman.servers');
		$client->addServers(implode(',', $servers));
		$result = $client->doBackground($function, serialize($workload));
		return $result;
	}
}
