<?php

/**
 * Help in the view to check if a user 
 * has access to some resource using
 * the ACL present on the ApplicationController
 */
class Helper_Permission
{

	private static $permissionInstance;

	/**
	 * Reference to the controller
	 * so we can use the ACL created there
	 */
	protected $controller = null;

	/**
	 * Initialize the object with the controller
	 */
	private function __construct($controller)
	{
		$this->controller = $controller;
	}

	public function setController($controller)
	{
		$this->controller = $controller;
	}

	/**
	 * static method to get this instance
	 * 
	 * @param Controller_Application $controller Optional, defaults to null. 
	 * 
	 * @return Helper_Permission
	 */
	public static function instance($controller=null)
	{
		if (!self::$permissionInstance)
		{
			self::$permissionInstance = new Helper_Permission($controller);
		}
		if ($controller!=null)
		{
			self::$permissionInstance->setController($controller);
		}
		return self::$permissionInstance;
	}  

	/**
	 * verify with the acl->isAllowed for a resource/privilege
	 *
	 * @param string $resource  the name of the resource
	 * @param string $privilege the name of the privilege to check
	 *
	 * @return bool
	 */
	public function isAllowed($resource=null, $privilege=null)
	{
		if ($this->controller == NULL) return false;
		try
		{
			$this->controller->requestAccess($resource, $privilege);
			return true;
		} catch (Exception $e)
		{
			return false;
		}
	}
}

