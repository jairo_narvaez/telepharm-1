<?php defined('SYSPATH') or die('No direct access allowed.');

class Image extends TelePharm_Image
{
	private $_file_path;
	private $_hash;

	public function __get($key)
	{
		switch ($key)
		{
			case 'file_path':
				if (!$this->_file_path)
				{
					# Always perform image operations on filesystem
					$this->_file_path = self::getOriginalImagesFolderPath('filesystem') .
						$this->getPrefixByHash() .
						'.' . 
						$this->format;
				}
				return $this->_file_path;

			case 'hash':
				if (!$this->_hash)
				{
					$this->_hash = sha1_file($this->filename);
				}
				return $this->_hash;
            case 'gmagick':
                return $this->gmagick;
			default:
				return parent::__get($key);
		}
	}

	/**
	 * Verify if the original images folder has been define, exists and return its path
	 * @param string $storage, if we want to force to get a specific storage
	 *
	 * @return string, images folder path
	 */
	public static function getOriginalImagesFolderPath($storage = null)
	{
		$storage = $storage ?: Kohana::$config->load('application.use_images_from');

		$folder = Kohana::$config->load("application.images.$storage.original");

		if(!$folder)
		{
			throw new Kohana_Exception("Images folder has not been defined,
				please review the applications settings file");
		}

		if(!is_dir($folder))
		{
			if ( !mkdir($folder, 0777, true))
				throw new Exception("Image folder does not exist, verify the
					folder: $folder exist");
		}

		return $folder;
	}

	/**
	 * Verify if the images folder has been define, exists and return its path
	 *
	 * @return string, images folder path
	 */
	public static function getTmpImagesFolderPath()
	{
		$folder = Kohana::$config->load('application.tmp_images_folder');

		if(!$folder)
		{
			throw new Kohana_Exception("Tmp images folder has not been defined,
				please review the applications settings file");
		}

		if(!is_dir($folder))
		{
			if ( !mkdir($folder, 0777, true))
				throw new Exception("Tmp images folder does not exist, verify that
					folder: $folder exists");
		}

		return $folder;
	}
	
	/**
	 * Create images thumbnails
	 * @param  array $params ,  array of options which can contain: hcrop,
	 *                          vcrop, format, file_path
	 * @return string, file path to the thumbnail image
	 */
	public function resizeAndCrop($params)
	{
		$settings = array_merge(
			[
				'hcrop'  => Image::CENTER,
				'vcrop'  => Image::CENTER,
				'format' => 'JPEG'
			],
			$params
		);

		$fileFormat = $settings['format'];

		$filePath = $settings['file_path'] . $this->getPrefixByHash() . '.' .
			strtolower($fileFormat);

		$this->resizedCrop($settings['width'], $settings['height']);

		if (!file_exists(dirname($filePath)))
			mkdir(dirname($filePath),  0777, TRUE);

		$tmpPath = self::getTmpImagesFolderPath() . uniqid() . '.' . $fileFormat;

		$this->save($tmpPath, $fileFormat);

		copy($tmpPath, $filePath);

		# TODO probably this is not required b/c we can use this as a local cache
		unlink($tmpPath);

		return $filePath;
	}

	/**
	 * return the name of the file generated by
	 * 2 directories from the hash name
	 *
	 * @return string
	 */ 
	public function getPrefixByHash()
	{
		return $this->hash[0] . '/' . $this->hash[1] . '/' . $this->hash;
	}

}
