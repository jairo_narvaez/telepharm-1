<?php

/**
 * Abstract class for the different kind
 * of medispan files to be loaded to the system
 */
abstract class Medispan_Load
{
	// This method should be implemented by specific class
	abstract public function getFieldsDefinition();
	// This method should be implemented to save the info to the db
	abstract public function processRecord($data);

	/**
	 * to cache the fields definition
	 */
	protected $fieldsDefinition = null;
	/**
	 * the full path of the file name to load
	 */
	protected $filePath = null;

	/**
	 * Creates an instance of the loader dynamically 
	 *
	 * @param string $medispanFileName the name of the class to instantiate 
	 * @param string $filePath full path to load the file
	 *
	 * @return Medispan_Load_*
	 */
	public static function factory($medispanFileName, $filePath)
	{
		$className = 'Medispan_Load_'.$medispanFileName;
		return new $className($filePath);
	}

	/**
	 * Creates an instance of the loader
	 * 
	 * @param string $filePath 
	 * 
	 * @return Medispan_Load_*
	 */
	public function __construct($filePath)
	{
		$this->fieldsDefinition = $this->getFieldsDefinition();
		if (!file_exists($filePath))
			throw new Exception('File not found '. $filePath);
		$this->filePath = $filePath;
	}


	/**
	 * Get the sum of all size fields
	 * 
	 * @return int
	 */
	private function getFullLineLength()
	{
		$total = 0;
		foreach($this->fieldsDefinition as $field)
			$total += $field['size'];
		return $total;
	}

	/**
	 * Giving a string as it comes in a line on 
	 * the medispan file, we should return a list of
	 * data using the fields definition
	 * 
	 * @param String $line the source line to be decomposed
	 *
	 * @return Array
	 */
	public function processLine($line)
	{
		$validLength = $this->getFullLineLength();

		if ($validLength != strlen($line))
		{
			throw new Exception('Line is not the same length as the definition; line: ' . strlen($line) . ', definition: ' . $validLength);
		}

		$currentPosition = 0;
		$data = [];
		foreach($this->fieldsDefinition as $field)
		{
			$data[$field['name']] = $this->castData(
				substr($line,
					$currentPosition,
					$field['size']), 
				$field['type']
			);
			$currentPosition += $field['size'];
		}
		return $data;
	}

	/**
	 * cast some data to the specified type
	 * 
	 * @return string
	 */
	public function castData($data, $type)
	{
		switch ($type)
		{
		case 'string': return trim(''.$data);
		case 'number': return preg_replace('/[^0-9]/','0',$data);
		case 'date': return $data == '00000000' ? null : date_create_from_format('Ymd', $data)->format('Y-m-d');
		}
	}

	/**
	 * Read the file and load 
	 * 
	 * @return array, number of records processed / saved
	 */
	public function load()
	{
		$numberOfLinesProcessed = 0;
		$numberOfLinesSaved = 0;
		$handle = fopen($this->filePath, "r");
		if ($handle) {
			while (($buffer = fgets($handle, 4096)) !== false) {
				$numberOfLinesSaved += $this->processRecord(
					$this->processLine(str_replace("\n",'',$buffer))
				) ? 1 : 0;
				$numberOfLinesProcessed++;
				if ($numberOfLinesProcessed % 100 == 0)
					gc_collect_cycles();
			}
			if (!feof($handle)) {
				throw new Exception("Error: unexpected fgets() fail");
			}
			fclose($handle);
		}
		return [$numberOfLinesProcessed, $numberOfLinesSaved];
	}
}
