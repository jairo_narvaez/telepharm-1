<?php

/**
 * Process the loading of the DMFG file
 */
class Medispan_Load_DMFG extends Medispan_Load
{

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Drug Image v2 Documentation Manual.pdf
	 * page 51
	 * Drug Manufacturer File (IM2DMFG)
	 * http://cl.ly/image/473d402w301i
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
			'name' => 'manufacturer_id',
			'type' => 'number',
			'size' => 10,
			],
			[
			'name' => 'transaction_code',
			'type' => 'string',
			'size' => 1,
			],
			[
			'name' => 'manufacturer_name',
			'type' => 'string',
			'size' => 30,
			],
			[
			'name' => 'reserve',
			'type' => 'string',
			'size' => 39,
			],
		];
	}


	/**
	 * Process the record on the db
	 * to create, update or delete a record
	 * depending on the transaction_code
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{

		switch ($data['transaction_code'])
		{
			case 'C':

				try
				{
					DB::update('medispan_dmfg')
						->set(['manufacturer_name' => $data['manufacturer_name']])
						->where('manufacturer_id', '=', $data['manufacturer_id'])
						->execute();

					return true;
				}
				catch (Exception $e)
				{
					return false;
				}

			break;

			case 'D':
				# do not delete for now
				return true;

			break;

			case 'A':
			default:
				
				try
				{
					//insert
					$rec = Model::factory('MedispanDMFG');
					$rec->values($data);
					$rec->save();
					return $rec->saved();
				}
				catch (Exception $e)
				{
					return false;
				}

			break;
		}

		return false;
	}
}

