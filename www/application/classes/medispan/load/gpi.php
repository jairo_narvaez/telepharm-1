<?php

/**
 * Process the loading of the IMG file
 */
class Medispan_Load_GPI extends Medispan_Load
{

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Warning Label Database - Documentation Manual.pdf
	 * page 50
	 * Warning Label - Generic Product Identifier file (WLGPI)
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
				'name' => 'generic_product_id',
				'type' => 'string',
				'size' => 14,
			],
			[
				'name' => 'priority',
				'type' => 'number',
				'size' => 2
			],
			[
				'name' => 'label_id',
				'type' => 'string',
				'size' => 6
			],
			[
				'name' => 'reserve',
				'type' => 'string',
				'size' => 33
			],
			[
				'name' => 'activity_code',
				'type' => 'string',
				'size' => 1
			],
			[
				'name' => 'last_change_date',
				'type' => 'string',
				'size' => 8
			]
		];
	}


	/**
	 * Process the record in the db
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{
		$rec = Model::factory('MedispanLBLGPI')->
			where('label_id', '=', $data['label_id'])->
			and_where('generic_product_id', '=', $data['generic_product_id'])->
			and_where('priority', '=', $data['priority'])->
			find();

		if ($rec->loaded())
		{
			switch ($data['activity_code'])
			{
				case 'D':
					$rec->delete();
					return true;
				break;

				default:
					$rec->values($data);
					$rec->save();
					return $rec->saved();
				break;
			}
		}
		else
		{
			switch ($data['activity_code'])
			{
				case 'D':
					return false;
				break;

				default:
					$rec->values($data);
					$rec->save();
					return $rec->saved();
				break;
			}
		}
		
		return false;
	}
}
