<?php

/**
 * Process the loading of the IMG file
 */
class Medispan_Load_IMG extends Medispan_Load
{

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Drug Image v2 Documentation Manual.pdf
	 * page 52
	 * Drug Manufacturer File (IM2IMG)
	 * http://cl.ly/image/0k0u1l0K1J1H
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
			'name' => 'image_id',
			'type' => 'number',
			'size' => 10,
			],
			[
			'name' => 'transaction_code',
			'type' => 'string',
			'size' => 1,
			],
			[
			'name' => 'image_filename',
			'type' => 'string',
			'size' => 20,
			],
			[
			'name' => 'reserve',
			'type' => 'string',
			'size' => 17,
			],
		];
	}


	/**
	 * Process the record on the db
	 * to create, update or delete a record
	 * depending on the transaction_code
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{
		switch ($data['transaction_code'])
		{
			case 'C':
				//update

				# can't use kohana ORM here; non-incrementing id does not show loaded()
				try
				{
					DB::update('medispan_img')
						->set(['image_filename' => $data['image_filename']])
						->where('image_id', '=', $data['image_id'])
						->execute();

					return true;
				}
				catch (Exception $e)
				{
					return false;
				}

			break;

			case 'D':
				//delete
				# do not delete for the time being
				return true;

			break;

			case 'A':
			default:

				try
				{
					//insert
					$rec = Model::factory('MedispanIMG');
					$rec->values($data);
					$rec->save();
					return $rec->saved();
				}
				catch (Exception $e)
				{
					return false;
				}

			break;
		}

		return false;
	}
}

