<?php

/**
 * Process the loading of the IMG file
 */
class Medispan_Load_LBL extends Medispan_Load
{
	private $last_data = [ 'label_id' => '', 'label' => '' ];
	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Warning Label Database - Documentation Manual.pdf
	 * page 58
	 * Label file (WLLBL)
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
				'name' => 'source_indicator_code:',
				'type' => 'number',
				'size' => 3,
			],
			[
				'name' => 'label_id',
				'type' => 'string',
				'size' => 6,
			],
			[
				'name' => 'language_code',
				'type' => 'string',
				'size' => 2,
			],
			[
				'name' => 'sequence_code:not_used',
				'type' => 'string',
				'size' => 1,
			],
			[
				'name' => 'label',
				'type' => 'string',
				'size' => 100,
			],
			[
				'name' => 'reserve',
				'type' => 'string',
				'size' => 7,
			],
			[
				'name' => 'activity_code',
				'type' => 'string',
				'size' => 1,
			],
			[
				'name' => 'last_change_date',
				'type' => 'string',
				'size' => 8,
			]
		];
	}


	/**
	 * Process the record at the db
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{
		if (isset($this->last_data['label_id']) && ($this->last_data['label_id'] == $data['label_id']))
		{
			$this->last_data['label'] .= $data['label'];
			return 0;
		}

		if (isset($this->last_data['label_id']) && isset($this->last_data['label']) && $this->last_data['label_id'])
		{
			$rec = Model::factory('MedispanLBL')->
				where('label_id', '=', $this->last_data['label_id'])->
				find();

			$rec->values($this->last_data);
			$rec->save();
		
			if (!is_null($data))
			{
				$this->last_data = $data;
			}

			return $rec->saved();
		}

		$this->last_data = $data;

		return 0;
	}

	/**
	 * Override load function so that processRecord is called one extra time due to the carry-over nature of this file
	 * 
	 * @return array, number of records processed / saved
	 */
	public function load()
	{
		$result = parent::load();

		$saved = $this->processRecord(null);

		$result[1] += $saved;

		return $result;
	}
}

