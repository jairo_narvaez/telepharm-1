<?php

/**
 * Process the loading of the IMG file
 */
class Medispan_Load_Name extends Medispan_Load
{
	public function processLine($line)
	{
		$line = explode('|', $line);

		$data = [];
		foreach($this->fieldsDefinition as $field)
		{
			$data[$field['name']] = $this->castData(
				$line[$field['index']],
				$field['type']
			);
		}
		return $data;
	}

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Drug Image v2 Documentation Manual.pdf
	 * page 52
	 * Drug Manufacturer File (IM2IMG)
	 * http://cl.ly/image/0k0u1l0K1J1H
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return
		[
			[
				'name' => 'mf_drug_id',
				'type' => 'number',
				'index' => 0
			],
			[
				'name' => 'generic_product_id',
				'type' => 'number',
				'index' => 13
			],
			[
				'name' => 'name',
				'type' => 'string',
				'index' => 1
			],
			[
				'name' => 'dosage_form',
				'type' => 'string',
				'index' => 3
			],
			[
				'name' => 'strength',
				'type' => 'string',
				'index' => 4
			],
			[
				'name' => 'strength_unit_of_measure',
				'type' => 'string',
				'index' => 5
			],
			[
				'name' => 'bioequivalence_code',
				'type' => 'string',
				'index' => 6
			],
			[
				'name' => 'controlled_substance_code',
				'type' => 'string',
				'index' => 7
			],
			[
				'name' => 'efficacy_code',
				'type' => 'string',
				'index' => 8
			],
			[
				'name' => 'legend_indicator_code',
				'type' => 'string',
				'index' => 9
			],
			[
				'name' => 'knowledge_base_code',
				'type' => 'string',
				'index' => 14
			]
		];
	}


	/**
	 * Process the record at the db
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{
		$id = DB::select()
			->from('medispan_mf_drug')
			->where('mf_drug_id', '=', $data['mf_drug_id'])
			->execute()
			->get('id');

		if ($id)
		{
			DB::update('medispan_mf_drug')->set($data)->where('id', '=', $id)->execute();
		}
		else
		{
			DB::insert('medispan_mf_drug', array_keys($data))->values(array_values($data))->execute();
		}

		return 1;
	}
}

