<?php

/**
 * Process the loading of the NDC file
 */
class Medispan_Load_Ndc extends Medispan_Load
{
	public function processLine($line)
	{
		$line = explode('|', $line);

		$data = [];
		foreach($this->fieldsDefinition as $field)
		{
			$data[$field['name']] = $this->castData(
				$line[$field['index']],
				$field['type']
			);
		}
		return $data;
	}

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Medi-Span Electronic Drug File (MED-File) v2 Documentation Manual.pdf
	 * page 118
	 * NDC File (MF2NDC)
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
				'name' => 'external_drug_id',
				'type' => 'number',
				'index' => 0
			],
			[
				'name' => 'mf_drug_id',
				'type' => 'number',
				'index' => 1
			]
		];
	}


	/**
	 * Process the record at the db
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{
		$id = DB::select()
			->from('medispan_mf_ndc')
			->where('mf_drug_id', '=', $data['mf_drug_id'])
			->and_where('external_drug_id', '=', $data['external_drug_id'])
			->execute()
			->get('id');

		if ($id)
		{
			DB::update('medispan_mf_ndc')->set($data)->where('id', '=', $id)->execute();
		}
		else
		{
			DB::insert('medispan_mf_ndc', array_keys($data))->values(array_values($data))->execute();
		}

		return 1;
	}
}

