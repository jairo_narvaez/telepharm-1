<?php

/**
 * Process the loading of the UDIJ file
 */
class Medispan_Load_UDIJ extends Medispan_Load
{

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Drug Image v2 Documentation Manual.pdf
	 * page 48
	 * Drug Image Journal File (IM2UDIJ)
	 * http://cl.ly/image/1Z1s2y2a1f1F
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
			'name' => 'unique_drug_id',
			'type' => 'number',
			'size' => 10,
			],
			[
			'name' => 'start_date',
			'type' => 'date',
			'size' => 8,
			],
			[
			'name' => 'transaction_code',
			'type' => 'string',
			'size' => 1,
			],
			[
			'name' => 'stop_date',
			'type' => 'date',
			'size' => 8,
			],
			[
			'name' => 'image_id',
			'type' => 'number',
			'size' => 10,
			],
			[
			'name' => 'reserve',
			'type' => 'string',
			'size' => 35,
			],
		];
	}


	/**
	 * Process the record on the db
	 * to create, update or delete a record
	 * depending on the transaction_code
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{

		switch ($data['transaction_code'])
		{
			case 'C':

				try
				{
					DB::update('medispan_udij')
						->set([
							'stop_date' => $data['stop_date'],
							'image_id' => $data['image_id']
						])
						->where('unique_drug_id', '=', $data['unique_drug_id'])
						->and_where('start_date', '=', $data['start_date'])
						->execute();

					return true;
				}
				catch (Exception $e)
				{
					return false;
				}

			case 'D':
				
				return true;

			break;

			case 'A':
			default:

				try
				{
					//insert
					$rec = Model::factory('MedispanUDIJ');
					$rec->values($data);
					$rec->save();
					return $rec->saved();
				}
				catch (Exception $e)
				{
					return false;
				}
			}

			break;

		return false;
	}
}

