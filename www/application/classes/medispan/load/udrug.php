<?php

/**
 * Process the loading of the UDRUG file
 */
class Medispan_Load_UDRUG extends Medispan_Load
{

	/**
	 * Return the definition of the file as mentioned in the
	 * documentation:
	 * Drug Image v2 Documentation Manual.pdf
	 * page 45
	 * Unique Drug File (IM2UDRUG)
	 * http://cl.ly/image/0D0C0e1q1M3k
	 *
	 * @return Array
	 */
	public function getFieldsDefinition()
	{
		return [
			[
			'name' => 'unique_drug_id',
			'type' => 'number',
			'size' => 10,
			],
			[
			'name' => 'transaction_code',
			'type' => 'string',
			'size' => 1,
			],
			[
			'name' => 'dosage_form_id',
			'type' => 'number',
			'size' => 5,
			],
			[
			'name' => 'external_drug_id',
			'type' => 'string',
			'size' => 20,
			],
			[
			'name' => 'manufacturer_id',
			'type' => 'number',
			'size' => 10,
			],
			[
			'name' => 'reserve',
			'type' => 'string',
			'size' => 26,
			],
	];
	}


	/**
	 * Process the record on the db
	 * to create, update or delete a record
	 * depending on the transaction_code
	 *
	 * @param Array $data
	 * 
	 * @return bool
	 */
	public function processRecord($data)
	{

		switch ($data['transaction_code'])
		{
			case 'C':

				try
				{
					DB::update('medispan_udrug')
						->set([
							'dosage_form_id' => $data['dosage_form_id'],
							'dosage_form_id' => $data['dosage_form_id'],
							'manufacturer_id' => $data['manufacturer_id']
						])
						->where('unique_drug_id', '=', $data['unique_drug_id'])
						->execute();

					return true;
				}
				catch (Exception $e)
				{
					return false;
				}

			break;

			case 'D':

				# do not delete for now
				return true;

			break;

			case 'A':
			default:

				try
				{
					//insert
					$rec = Model::factory('MedispanUDRUG');
					$rec->values($data);
					$rec->save();
					return $rec->saved();
				}
				catch (Exception $e)
				{
					return false;
				}

			break;
		}

		return false;
	}
}
