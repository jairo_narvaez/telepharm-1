<?php

/**
 * Class used to query the medispan db
 * to get images
 * 
 */
class Medispan_Query_Image
{
	/**
	 * Get the image path based on the
	 * ndc of a medicament
	 * 
	 * @param string $ndc 
	 * 
	 * @return string
	 */
	public function get($ndc)
	{
		if (empty($ndc)) return [];

		$standardNDC = preg_replace('/[^0-9]/','',$ndc);

		$images = [];
		$query = DB::query(Database::SELECT, 'SELECT * 
			FROM medispan_udrug udrug, medispan_udij udij, medispan_img img
			WHERE
			udrug.unique_drug_id = udij.unique_drug_id
			AND udij.image_id = img.image_id
			AND udij.stop_date IS NULL
			AND udrug.external_drug_id = :ndc');
		$query->param(':ndc', $standardNDC);
		foreach($query->execute() as $rows)
		{
			$images[] = url::medispan_image($rows['image_filename'].'.JPG');
		}

		return $images;
	}
}
