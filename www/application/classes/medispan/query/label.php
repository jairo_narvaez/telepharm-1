<?php

/**
 * Class used to query the medispan db
 * to get images
 * 
 */
class Medispan_Query_Label
{
	/**
	 * Get the image path based on the
	 * ndc of a medicament
	 * 
	 * @param string $ndc 
	 * 
	 * @return string
	 */
	public function get($ndc)
	{
		if (empty($ndc)) return [];

		$standardNDC = preg_replace('/[^0-9]/','',$ndc);

		$labels = [];
		$query = DB::query(
			Database::SELECT,
			'select g.priority, l.label
			from medispan_lb_lbl as l
			join medispan_lb_lbl_gpi as g on g.label_id = l.label_id
			join medispan_mf_drug as d on d.generic_product_id = g.generic_product_id
			join medispan_mf_ndc as n on n.mf_drug_id = d.mf_drug_id
			where n.external_drug_id = :ndc
			order by g.priority ASC'
		);
		$query->param(':ndc', $standardNDC);

		foreach($query->execute() as $row)
		{
			$labels[] = $row;
		}

		return $labels;
	}
}
