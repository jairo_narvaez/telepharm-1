<?php namespace mef\ACL;

interface AssertionInterface
{
	public function assert(Manager $acl, RoleInterface $role = null, ResourceInterface $resource = null, $privilege = null);
}