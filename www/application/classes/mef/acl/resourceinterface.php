<?php namespace mef\ACL;

interface ResourceInterface
{
	public function getResourceId();
}