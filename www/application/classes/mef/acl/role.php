<?php namespace mef\ACL;

class Role implements RoleInterface
{
	protected $id;

	public function __construct($id)
	{
		$this->id = (string) $id;
	}

	public function getRoleId()
	{
		return $this->id;
	}

	public function __toString()
	{
		return $this->id;
	}
}