<?php

require __DIR__.'/getter.php';
require __DIR__.'/setter.php';
require __DIR__.'/acl/assertioninterface.php';
require __DIR__.'/acl/resourceinterface.php';
require __DIR__.'/acl/roleinterface.php';
require __DIR__.'/acl/graphiterator.php';
require __DIR__.'/acl/manager.php';
require __DIR__.'/acl/resource.php';
require __DIR__.'/acl/role.php';
