<?php namespace mef;

use \ReflectionClass;
use \Exception;

/**
 * Provides readable attributes by replacing the PHP magic __get with a
 * function per property.
 *
 * Also overrides __isset.
 *
 * For example, to provide read access to an id:
 *
 * <code>
 * class Foo
 * {
 *   private $__id;
 *
 *   public function __construct($id)
 *   {
 *     $this->__id = $id;
 *   }
 *
 *   private function __getId()
 *   {
 *     return $this->__id;
 *   }
 * }
 *
 * $foo = new Foo(42);
 * echo $foo->id;
 * </code>
 *
 * @todo Remove legacy support for getFoo (no underscores)
 */
Trait Getter
{
	final public function __get($name)
	{
		$method = '__get'.$name;
		return is_callable([$this, $method]) ? $this->{$method}() :
			$this->{'get'.$name}();
	}

	final public function __isset($name)
	{
		$method = '__get'.$name;
		return is_callable([$this, $method]) && $this->$method() !== null;
	}
}