<?php namespace mef;

/**
 * Provides writable attributes by replacing the PHP magic __set with a
 * function per property.
 *
 * For example, to enforce an integer property:
 *
 * <code>
 * class Foo
 * {
 *   private $__bar;
 *
 *   private function __setBar($bar)
 *   {
 *     $this->__bar = (int) $bar;
 *   }
 * }
 *
 * $foo = new Foo();
 * $foo->bar = '42';
 * </code>
 *
 * @todo Remove legacy support for setFoo (no underscores)
 */
Trait Setter
{
	final public function __set($name, $value)
	{
		$method = '__set'.$name;
		if (is_callable([$this, $method]))
			$this->{$method}($value);
		else
			$this->{'set'.$name}($value);
	}
}