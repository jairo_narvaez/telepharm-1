<?php
class Model_Account extends TelePharm_Model_Account
{
	protected $_table_name = 'account';

	protected $_has_many = [
		'roles' => ['model' => 'Role', 'through' => 'account_role'],
		'company_roles' => ['model' => 'CompanyRole'],
		'store_roles' => ['model' => 'StoreRole']
	];


	protected $_belongs_to = [
		'image' => ['model' => 'Image'],
	];

	protected $_virtual_fields = [
		'full_name',
	];

    public $web_session;
    public $forgot_password_session;

    public function __construct($account_id = null){
        parent::__construct($account_id);
        if ($this->loaded()){
            $this->web_session = Model_AccountToken::fetch($account_id, Model_AccountSessionType::Web);
            $this->forgot_password_session = Model_AccountToken::fetch($account_id, Model_AccountSessionType::Forgot_Password);
        }else{
            $this->web_session = new Model_AccountToken();
            $this->forgot_password_session = new Model_AccountToken();
        }
    }


	/**
	 * Get virtual fields or default behaviour
	 * 
	 * @param string $key 
	 * 
	 * @return mixed
	 */
	public function __get($key)
	{
		switch($key)
		{
		case 'full_name':
			return parent::__get('first_name') . ' ' . parent::__get('last_name') ;
		case 'store':
			$firstRole = $this->store_roles->find();
			return $firstRole->store->name;
		default:
			return  parent::__get($key);
		}
	}

	public static function createAccount($info, $encrypted_password)
	{
		$acct = Model::factory('Account');
		$acct->values(
			[
				'username' => $info['username'],
				'email' => $info['email'],
				'first_name' => $info['patient']['first_name'],
				'last_name' => $info['patient']['last_name'],
			]
		);

		# must separate out the hash since it is a virtual field
		$acct->password_hash = $encrypted_password;
		$acct->save();

		return $acct;
	}

	/**
	 * Return a list of accounts that can be shown as available
	 * for a pharmacist or a techincian
	 * 
	 * @param CurrentUser $currentUser The user context
	 * @param string      $type        Optional, defaults to 'Technician'. 
	 * 
	 * @return array
	 */
	public static function getAvailableUsersFor(CurrentUser $currentUser, $type = 'Technician')
	{
		$accounts = [];

		foreach (self::factory('Account')->
				join('store_role')->on('store_role.account_id', '=', 'account.id')->
				join('role')->on('role.id', '=', 'store_role.role_id')->
				where('role.type', '=', 'store')->
				where('role.name', '=', $type)->
				where('store_role.store_id', 'in', array_keys($currentUser->stores))->
				where('account.is_active', '=', true)->
				where('account.is_available', '=', true)->
				find_all() as $act)
		{
            $act->addVirtualField('store');
            $act = $act->publicArray();
            $websession = Model_AccountToken::fetch($act['id'], Model_AccountSessionType::Web);
            if ($websession->loaded()){
                if ($websession->token->loaded()){
                    $expire = new DateTime($websession->token->expires);
                    if($expire < new DateTime()){
                        $websession->delete();
                    }else{
                        $accounts[] = $act;
                    }
                }else{
                    $websession->delete();
                }
            }
        }
		return $accounts;
	}

	/**
	 * Return a list of stores with at least one technician
	 * logged in
	 *
	 * @param CurrentUser $currentUser The user context
	 *
	 * @return array
	 */
	public static function getAvailableStoresForPharmacist(CurrentUser $currentUser)
	{
		$stores = [];
		$query = DB::query(Database::SELECT, <<<EOF
			SELECT DISTINCT s.id AS store_id
			FROM   store s
				   INNER JOIN store_role sr
						   ON ( s.`id` = sr.`store_id` )
				   INNER JOIN role r
						   ON ( r.`id` = sr.`role_id` )
				   INNER JOIN account a
						   ON ( a.`id` = sr.`account_id` )
			       Inner JOIN account_token ato
			               ON ato.account_id = a.id
			       INNER JOIN token t
			               ON t.id = ato.token_id
			WHERE  r.name = 'Technician'
			   AND sr.store_id IN :stores
			   AND a.is_active = 1
			   AND a.is_available = 1
			   AND t.expires > :now
EOF
		);
        $now = new DateTime();
		$query->param(':stores', array_keys($currentUser->stores));
        $query->param(':now', $now->format("Y-m-d H:i:s"));
		foreach ($query->execute() as $row)
		{
			$store = Model::factory('Store')->
				where('id', '=', $row['store_id'])->find();
			$stores[] = $store->as_array();
		}

		return $stores;
	}

	/**
	 * change the availability status
	 * 
	 * @return boolean
	 */
	public function toggleAvailability()
	{
		$this->is_available = !$this->is_available;
		$this->is_active = true;
		$this->save();
		return $this->saved();
	}

	/**
	 * change the availability status
	 *
	 * @param  boolean $on_off
	 * 
	 * @return boolean
	 */
	public function setBrowser($on_off)
	{
		$this->is_browser_open = $on_off ? 1 : 0;
		if ($on_off) { $this->is_active = 1; } # make sure the user is also marked as active
		$this->save();
		return $this->saved();
	}

	/**
	 * Return a curated list of fields
	 * 
	 * @return array
	 */
	public function publicArray()
	{
		$sensitive = ['username', 'password', 'image_id', 'timezone', 'login_dt', 'created_dt', 'updated_dt', 'image'];
		$result = $this->as_array();

		foreach($sensitive as $key)
		{
			unset($result[$key]);
		}

		return $result;
	}

	/**
	 * Implementation of the ResourceInterface
	 * 
	 * @return string
	 */
	public function getResourceId()
	{
		return 'account';
	}
}

