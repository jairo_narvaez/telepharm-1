<?php

/**
 * AccountRole model to interact with the db
 */
class Model_AccountRole extends ORM
{
	protected $_table_name = 'account_role';

	protected $_belongs_to = [
		'account' => ['model' => 'Account'],
		'role' => ['model' => 'Role'],
	];
}

