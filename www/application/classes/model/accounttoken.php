<?php
class Model_AccountToken extends ORM
{
    protected $_table_name ='account_token';

    protected $_type;
    protected $_slide_minutes;
    protected $_length;
    protected $_issued;
    protected $_current_perspective;
    public $token;

    public static function make($account, $type, $length = 32, $slide_minutes = 5, $now, $ipaddress){
        $obj = new static();
        $obj->_slide_minutes = $slide_minutes;
        $obj->_type = $type;
        $obj->account_id = $account->id;
        $obj->token = Model_Token::make($length, $obj->_slide_minutes, $now, $ipaddress);
        $obj->_current_perspective = null;
        return $obj;
    }

    public function __construct($account_token_id = null){
        parent::__construct($account_token_id);
        if ($this->loaded())
            $this->token = new Model_Token($this->token_id);
    }

    public function set_issued($issued){
        $this->_issued = $issued;
    }

    public function get_token(){
        return $this->token;
    }

    public function save(Validation $validation = NULL)
    {
        //check to see if this is a creating a new session or updating an already active session.
        //no ID means no active session
        //having an ID means an active session already exists.
        if ($this->id > 0) {
            //update active session.
            //get the slide minutes
            $slide_minutes = $this->token->slide_minutes;
            //get length of current session token value
            $length = intval(strlen($this->token->value) / 2);

            $ipaddress = $this->token->ipaddress;
            $created = new DateTime($this->token->created);

            //delete current token.
            $this->token->delete();
            $account_id = $this->account_id;
            $type_id = $this->type_id;
            $current_perspective = $this->current_perspective;
            //delete current account session
            $this->delete();
            $this->account_id = $account_id;
            $this->_type = $type_id;
            $this->_current_perspective = $current_perspective;
            //instantiate new token object for updated session.
            $this->token = Model_Token::make($length, $slide_minutes, $created, $ipaddress);
        }
        $this->token->set_issued($this->_issued);
        //save the instantiated token.
        $this->token = $this->token->save($validation);
        //save the account token
        $this->values(['account_id' => $this->account_id,
            'token_id' => $this->token->id,
            'type_id' => $this->_type,
            'current_perspective'=>$this->_current_perspective]);
        parent::save($validation);
        return $this;
    }

    //whenever an account token is deleted, we want to delete the associated token.
    public function delete()
    {
        if ($this->token->loaded())
            $this->token->delete();
        parent::delete();
    }

    public static function fetch($account_id, $type)
    {
        $record = ORM::factory('accounttoken')->
            where('account_id', '=', $account_id)->
            where('type_id', '=', $type)->
            find();
        if($record->loaded()){
            $record->token = new Model_Token($record->token_id);
        }
        return $record;
    }

    public function is_authenticated(){
        if (!$this->loaded()){
            return false;
        }
        if (!$this->token->loaded()){
            return false;
        }
        $now = new DateTime();
        $expires = new DateTime($this->token->expires);
        if ($now > $expires){
            return false;
        }
        return true;
    }
}