<?php
class Model_Address extends ORM {
    protected $_table_name = 'address';
    protected $_virtual_fields = [
        'state'
    ];
    public function __get($key)
    {
        switch($key)
        {
            case 'state':
                $state = new Model_State($this->state_id);
                return $state->as_array();

            default:
                return  parent::__get($key);
        }
    }

    public static function create_address($line1, $city, $state_id, $postal_code){
        $self = new static();
        $self->line1 = $line1;
        $self->city = $city;
        $self->state_id = $state_id;
        $self->postal_code = $postal_code;
        return $self;
    }

    public function save(Validation $validation = null){
        $self = $this;
        //check if there was any custom validation passed for this save
        if ($validation){
            //custom validation was supplied, use it.
            $self = parent::save($validation);
        }else{
            //validate line 1
            if (strlen($self->line1) == 0){
                $self->error_messages[] = new Telepharm_ErrorMessage('Address line 1 is required', 1);
            }

            //validate city
            if (strlen($self->city) == 0){
                $self->error_messages[] = new Telepharm_ErrorMessage('Address city is required', 1);
            }

            //validate state_id
            $state = new Model_State($self->state_id);
            if (!$state->loaded()){
                $self->error_messages[] = new Telepharm_ErrorMessage('Address state is invalid', 1);
            }

            //validate postal code
            if (strlen($self->postal_code) == 0){
                $self->error_messages[] = new Telepharm_ErrorMessage('Address postal code is required', 1);
            }

            //make sure there were no errors
            if (count($self->error_messages) == 0){
                //no errors? set meta data columns
                $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                //save to database.
                $self = parent::save();
            }
        }
        return $self;
    }

    public function unset_unused_properties(){
        $array = $this->as_array();
        $array['state'] = $this->state;
        unset($array['state_id']);
        return $array;
    }
}