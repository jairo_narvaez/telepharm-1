<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_AuthToken extends ORM
{
    protected $_table_name = 'auth_token';

    public function __set($key, $val)
    {
        switch ($key)
        {
            case 'details':
                if (is_array($val))
                    parent::__set('details', new JSONArray($val));
                else
                    parent::__set('details', json_decode($val, true));
                break;

            default:
                return parent::__set($key, $val);
        }
    }

    protected function _load_values(array $values)
    {
        if (array_key_exists('details', $values))
        {
            $values['details'] = new TelePharm_JSONArray($values['details'] ? json_decode($values['details'], true) : array());
        }

        return parent::_load_values($values);
    }

    public function save(Validation $validation = NULL)
    {
        if (!$this->id) $this->id = TelePharm_UUID::generate();
        if (!$this->account_id) $this->account_id = Session::instance()->get('account_id'); # TODO: remove me

        $this->_changed['details'] = 'details';
        $this->_saved = false;

        parent::save($validation);
    }
}