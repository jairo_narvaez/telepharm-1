<?php

/**
 * Company model
 */
class Model_Company extends ORM implements mef\ACL\ResourceInterface
{
	protected $_table_name = 'company';

	protected $_belongs_to = [
		'location' => ['model' => 'Location'],
	];

	protected $_has_many = [
		'company_roles' => ['model' => 'CompanyRole'],
		'accounts' => ['model' => 'Account', 'through' => 'company_role'],
	];

	/**
	 * Implementation of the ResourceInterface
	 * 
	 * @return string
	 */
	public function getResourceId()
	{
		return 'company';
	}
}
