<?php

/**
 * CompanyRole model to interact with the db
 */
class Model_CompanyRole extends ORM
{
	protected $_table_name = 'company_role';

	protected $_belongs_to = [
		'account' => ['model' => 'Account'],
		'company' => ['model' => 'Company'],
		'role' => ['model' => 'Role'],
	];

	protected $_virtual_fields = [
		'account', 'role'
		];
}


