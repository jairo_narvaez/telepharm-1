<?php
class Model_ExternalPharmacist extends ORM
{

    protected $_store;
    protected $_person;
    protected $_internal_store_id;

    protected $_table_name = 'external_pharmacist';
    protected $_virtual_fields = [
        'person',
        'store',
        'store_record',
        'internalstorerelationship'
    ];

    public function __get($key)
    {
        switch($key)
        {
            case 'person':
                $person = new Model_Person($this->person_id);
                return $person->as_array();
            case 'store':
                $external_store = new Model_ExternalStore($this->external_store_id);
                return $external_store->unset_unused_properties();
            case 'store_record':
                $external_store = new Model_ExternalStore($this->external_store_id);
                $external_store->set_internal_store_id($this->_internal_store_id);
                return $external_store;
            case 'internalstorerelationship':
                $internal_store_external_pharmacist = ORM::factory('InternalStoreExternalPharmacist')->
                    where('external_pharmacist_id', '=', $this->id)->
                    where('internal_store_id', '=', $this->_internal_store_id)->
                    find();
                return $internal_store_external_pharmacist;

            default:
                return parent::__get($key);
        }
    }

    public function set_internal_store_id($id){
        $this->_internal_store_id = $id;
    }

    public static function create_external_pharmacist(Model_ExternalStore $store, Model_Person $person){
        $self = new static();
        $self->_store = $store;
        $self->_person = $person;
        return $self;
    }

    public function save(Validation $validation = null){
        $self = $this;
        //check if there was any custom validation passed for this save
        if ($validation){
            //custom validation was supplied, use it.
            $self = parent::save($validation);
        }else{
            //validate external store
            if (!$self->_store->loaded()){
                $self->error_messages[] = new Telepharm_ErrorMessage('External store is invalid', 2);
            }

            //validate pharmacist (person)
            if (!$self->_person->loaded()){
                $self->error_messages[] = new Telepharm_ErrorMessage('Pharmacist is invalid', 2);
            }

            //make sure there were no errors
            if (count($self->error_messages) == 0){
                $person = $self->_person->as_array();
                $store = $self->_store->as_array();
                //no errors? set columns up for insert
                $this->person_id = $person['id'];
                $this->external_store_id = $store['id'];
                $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                //save to database.
                $self = parent::save();
            }
        }
        return $self;
    }

    public static function fetch_pharmacist_by_store_id($internal_store_id, $pharmacist_id){
        $pharmacist = ORM::factory('ExternalPharmacist')->
            join('internal_store_external_pharmacist')->
            on('internal_store_external_pharmacist.external_pharmacist_id', '=', 'externalpharmacist.id')->
            where('internal_store_external_pharmacist.internal_store_id', '=', $internal_store_id)->
            where('externalpharmacist.id', '=', $pharmacist_id)->
            find();
        return $pharmacist;
    }

    public static function fetch_pharmacist_at_store($first_name, $last_name, Model_ExternalStore $store){
        $store = $store->as_array();
        $pharmacist = ORM::factory('ExternalPharmacist')->
            join('person')->
            on('person.id', '=', 'externalpharmacist.person_id')->
            where('externalpharmacist.external_store_id', '=', $store['id'])->
            and_where('person.first_name', '=', $first_name)->
            and_where('person.last_name', '=', $last_name)->
            limit(1)->
            find();

        return $pharmacist;
    }

    public static function fetch_all_for_internal_store(Model_Store $current_store)
    {
        $pharmacists = ORM::factory('ExternalPharmacist')->
            join('internal_store_external_pharmacist')->
            on('internal_store_external_pharmacist.external_pharmacist_id','=','externalpharmacist.id')->
            where('internal_store_external_pharmacist.internal_store_id', '=', $current_store->id)->
            order_by('internal_store_external_pharmacist.updated_dt', 'DESC')->
            find_all();
        $result = array();
        foreach($pharmacists as $pharmacist){
            $result[] = $pharmacist->unset_unused_properties();
        }
        return $result;
    }

    public static function fetch_all_for_external_store(Model_Store $current_store, $store_id)
    {
        $pharmacists = ORM::factory('ExternalPharmacist')->
            join('internal_store_external_store')->
            on('internal_store_external_store.external_store_id','=','externalpharmacist.external_store_id')->
            where('externalpharmacist.external_store_id', '=', $store_id)->
            where('internal_store_external_store.internal_store_id', '=', $current_store->id)->
            find_all()->
            as_array();
        $result = array();
        foreach($pharmacists as $pharmacist){

            $result[] = $pharmacist->unset_unused_properties();
        }
        return $result;
    }

    public function unset_unused_properties(){
        $array = $this->as_array();
        unset($array['internalstorerelationship']);
        unset($array['store_record']);
        $array['person'] = $this->person;
        unset($array['person_id']);
        $array['store'] = $this->store;
        unset($array['external_store_id']);
        return $array;
    }
}
