<?php

class Model_ExternalStore extends ORM {

    protected $_table_name = 'external_store';
    protected $_address;
    protected $_internal_store_id;

    protected $_virtual_fields = [
        'address',
        'internalstorerelationship'
    ];

    public function set_internal_store_id($id){
        $this->_internal_store_id = $id;
    }

    public function __get($key)
    {
        switch($key)
        {
            case 'address':
                $address = new Model_Address($this->address_id);
                return $address->unset_unused_properties();
            case 'internalstorerelationship':
                $internal_store_external_store = ORM::factory('InternalStoreExternalStore')->
                    where('external_store_id', '=', $this->id)->
                    where('internal_store_id', '=', $this->_internal_store_id)->
                    find();
                return $internal_store_external_store;
            default:
                return  parent::__get($key);
        }
    }

    public static function create_external_store($store_name, Model_Address $address){
        $self = new static();
        $self->name = $store_name;
        $self->_address = $address;
        return $self;
    }

    public function save(Validation $validation = null){
        $self = $this;
        //check if there was any custom validation passed for this save
        if ($validation){
            //custom validation was supplied, use it.
            $self = parent::save($validation);
        }else{
            //validate store name
            if (strlen($self->name) == 0){
                $self->error_messages[] = new Telepharm_ErrorMessage('Store name is required', 1);
            }

            //validate store address
            if (!$self->_address->loaded()){
                $self->error_messages[] = new TelePharm_ErrorMessage('Store address is invalid', 2);
            }

            //make sure there were no errors
            if (count($self->error_messages) == 0){
                $address = $self->_address->as_array();
                //no errors? set ref columns
                $this->address_id = $address['id'];
                //set meta data columns
                $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                //save to database.
                $self = parent::save();
            }
        }
        return $self;
    }

    public static function fetch_existing_by_name($name){
        $store = ORM::factory('ExternalStore')->
            where('name', '=', trim($name))->
            limit(1)->
            find();
        return $store;
    }

    public static function fetch_existing(Model_Store $current_internal_store){
        $stores = ORM::factory('ExternalStore')->
            join('internal_store_external_store')->
            on('internal_store_external_store.external_store_id', '=', 'externalstore.id')->
            where('internal_store_external_store.internal_store_id', '=', $current_internal_store->id)->
            order_by('updated_dt', 'DESC')->
            find_all();
        $result = array();
        foreach($stores as $store){
            $result[] = $store->unset_unused_properties();
        }
        return $result;
    }

    public function unset_unused_properties(){
        $array = $this->as_array();
        unset($array['internalstorerelationship']);
        $array['address'] = $this->address;
        unset($array['address_id']);
        return $array;
    }
}