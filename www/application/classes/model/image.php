<?php defined('SYSPATH') or die('No direct access allowed.');
class Model_Image extends ORM
{
	protected $_table_name = 'image';
	protected $thumbnail = null;
	public    $originalSize = [];  # ['height' => x, 'width' => y]
	public    $img = null;  # the TelePharm_image

	/**
	 * Creates an instance of image
	 *
	 * @param Image $img the TelePharm_Image wrapper for gmagick
	 * @param string $kind the kind of image it is (to save accordingly)
	 * @param string $name the name of the image
	 * @param integer $size the size of the file on disk
	 *
	 * @return Model_Image
	 */
	public static function createInstance(Image $img, $kind, $name, $size)
	{
		$image = new Model_Image();
		$image->kind = $kind;
		$image->sha1 = $img->hash;
		$image->name = $name;
		$image->type = $img->format;
		$image->size = $size;
		$image->height = $img->height;
		$image->width = $img->width;
		$image->save();
		return $image;
	}

	/**
	 * process a multipart form data upload
	 * @param string $name the name on $_FILES
	 * @param string $kind the kind of image (account, prescription, signature, 
	 *                     etc)
	 * @param array  $params resize and crop params
	 *
	 * @return Model_Image
	 */
	public static function processUpload($name, $kind, $params = array())
	{
		if (!isset($_FILES[$name]))
			throw new Kohana_Exception("Invalid upload name");

		$upload = $_FILES[$name];

		if ($upload['error'])
			throw new Kohana_Exception("Upload Error");

		if (!is_uploaded_file($upload['tmp_name']))
			throw new Kohana_Exception("Invalid temp name");

		$img = new Image($_FILES[$name]['tmp_name']);

		if ($img->format != 'jpeg' && $img->format != 'png')
			throw new Kohana_Exception("Invalid image format");

		$file_path = $img->file_path;

		if (!file_exists(dirname($file_path)))
		{
			# if the directory doesn't exist yet, create it
			mkdir(dirname($file_path));
		}

		if (!file_exists($file_path))
		{
			# move the original image to the source directory so that it can be
			# used for future resizing, etc. Note that the hash is taken from the
			# contents of the image, so that if the same image is uploaded multiple
			# times, then it will not be duplicated on the file system.
			move_uploaded_file($_FILES[$name]['tmp_name'], $file_path);
		}

		$image = self::createInstance($img, $kind, 
			$_FILES[$name]['name'], filesize($file_path)
		);

		$image->originalSize = [
			'height' => $img->height,
			'width' => $img->width,
			];

		foreach($params['resizedCrop'] as $settings) {
			# Always perform resize operations on filesystem
			$settings['file_path'] = self::getThumbnailsImageFolder($kind, 'filesystem');
			$image->setThumbnail($img->resizeAndCrop($settings));
		}

		$image->img = $img;

		$storage = Kohana::$config->load('application.use_images_from');
		if ($storage == 's3')
		{
			Gearman_Client::doBackground('upload_s3', ['image_id' => $image->id]);
		}

		return $image;
	}

	/**
	 * Store a base 64 encoding image
	 * @param  string $picture   base64 image
	 * @param  string $name      image name
	 * @param  string $kind      kind of image (account, prescription, signature, 
	 *                           etc)
	 *
	 * @return Model_Image
	 */
	public static function processBase64Upload($picture, $name, $kind, $params = array())
	{
		$img_data = substr($picture, strpos($picture, 'base64,') + 7);
		$img_data = str_replace(' ', '+', $img_data);
		$data = base64_decode(str_replace(' ', '+', $img_data));
		if ($data === FALSE)
			throw new Exception('base64_decode has failed');

		$tmp_file = Image::getTmpImagesFolderPath() . uniqid();
		file_put_contents($tmp_file, $data);

		$img = new Image($tmp_file);

		if ($img->format != 'jpeg' && $img->format != 'png')
			throw new Kohana_Exception("Invalid image format");

		$file_path = $img->file_path;

		if (!file_exists(dirname($file_path)))
		{
			mkdir(dirname($file_path) , 0777, TRUE );
		}

		# save on original directory
		copy($tmp_file, $file_path);

		$image = self::createInstance($img, $kind, 
			$name, filesize($tmp_file)
		);

		$image->originalSize = [
			'height' => $img->height,
			'width' => $img->width,
			];

		foreach($params['resizedCrop'] as $settings) {
			# Always perform resize operations on filesystem
			$settings['file_path'] = self::getThumbnailsImageFolder($kind, 'filesystem');
			$image->setThumbnail($img->resizeAndCrop($settings));
		}

		$image->img = $img;

		unlink($tmp_file);

		$storage = Kohana::$config->load('application.use_images_from');
		if ($storage == 's3')
		{
			Gearman_Client::doBackground('upload_s3', ['image_id' => $image->id]);
		}

		return $image;
	}

	public function setThumbnail($thumbnail)
	{
		$this->thumbnail = $thumbnail;
	}

	public function getThumbnail()
	{
		return $this->thumbnail;
	}

	/**
	 * get the directory depending on the kind of image
	 *
	 * @param string $kind, the kind of image
	 * @param string $storage, if we want to force to get a specific storage
	 *
	 * @return string
	 */
	public static function getThumbnailsImageFolder($kind, $storage = null)
	{
		if ($kind == '')
			throw new Exception('Specify the kind');

		$storage = $storage?: Kohana::$config->load('application.use_images_from');

		$folder = Kohana::$config->load("application.images.$storage.$kind");

		if(!$folder)
		{
			throw new Kohana_Exception("Images folder (application.images.$storage.$kind) has not been defined,
				please review the applications settings file");
		}

		if(!is_dir($folder))
		{
			if ( !mkdir($folder, 0777, true))
				throw new Exception("Image folder does not exist, verify the
					folder: $folder exist");
		}

		return $folder;
	}

	/**
	 * return the name of the file generated by
	 * 2 directories from the sha1
	 *
	 * @return string
	 */ 
	public function getPrefixByHash()
	{
		return $this->sha1[0] . '/' . $this->sha1[1] . '/' . $this->sha1;
	}

	public function getOriginalImagePath()
	{
		$originalPath = Image::getOriginalImagesFolderPath() .
			$this->getPrefixByHash() . '.' . $this->type;
		$localPath = Image::getOriginalImagesFolderPath('filesystem') .
			$this->getPrefixByHash() . '.' . $this->type;

		$storage = Kohana::$config->load('application.use_images_from');
		if ($storage == 's3' && !file_exists($localPath))
		{
			// cache file locally to serve by the web server
			if (!is_dir(dirname($localPath)))
			{
				mkdir(dirname($localPath), 0777, true);
			}
			copy($originalPath, $localPath);
		}
		
		return $localPath;
	}

	public function getThumbnailImagePath()
	{
		$thumbnailPath = self::getThumbnailsImageFolder($this->kind) .
			$this->getPrefixByHash() . '.jpeg'; # thumbnails are always jpeg
		$localPath = self::getThumbnailsImageFolder($this->kind, 'filesystem') .
			$this->getPrefixByHash() . '.jpeg';

		$storage = Kohana::$config->load('application.use_images_from');
		if ($storage == 's3' && !file_exists($localPath))
		{
			// cache file locally to serve by the web server
			if (!is_dir(dirname($localPath)))
			{
				mkdir(dirname($localPath), 0777, true);
			}
			copy($thumbnailPath, $localPath);
		}
		
		return $localPath;
	}

    public function delete(){
       $original = $this->getOriginalImagePath();
        //delete original image if it exists
        if (file_exists($original)){
            unlink($original);
        }
        $thumb = $this->getThumbnailImagePath();
        //delete thumb image if it exists
        if (file_exists($thumb)){
            unlink($thumb);
        }
        //delete the reference to those files from the db.
        parent::delete();
    }

    public function rotate($direction){
        $original = $this->getOriginalImagePath();
        $this->performRotation($original, $direction);
        $thumb = $this->getThumbnailImagePath();
        $this->performRotation($thumb, $direction);
    }

    private function performRotation($filename, $direction){
        $degrees = 0;
        if ($direction == Model_ImageRotateDirection::CounterClockwise){
            $degrees = -90;
        }else if ($direction == Model_ImageRotateDirection::Clockwise){
            $degrees = 90;
        }else{
            throw new Exception('Invalid rotate direction');
        }
        $source = new Image($filename);
        $source->gmagick->rotateImage('white', $degrees);
        $source->save($filename);
        $source->gmagick->destroy();
    }
}

