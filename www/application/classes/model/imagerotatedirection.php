<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nicholasstarke
 * Date: 4/22/13
 * Time: 8:57 AM
 * To change this template use File | Settings | File Templates.
 */
class Model_ImageRotateDirection{
    const Clockwise = 1;
    const CounterClockwise = 2;
}