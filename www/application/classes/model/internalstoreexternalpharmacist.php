<?php

class Model_InternalStoreExternalPharmacist extends ORM{
    protected $_table_name = "internal_store_external_pharmacist";
    protected $_internal_store;
    protected $_external_pharmacist;
    protected $_updated;

    public function __get($key)
    {
        switch($key)
        {
            default:
                return  parent::__get($key);
        }
    }

    public function set_updated($updated){
        $this->_updated = $updated;
    }

    public static function create_internal_store_external_pharmacist(Model_Store $internal_store, Model_ExternalPharmacist $external_pharmacist){
        $self = new static();
        $self->_internal_store = $internal_store;
        $self->_external_pharmacist = $external_pharmacist;
        return $self;
    }

    public function save(Validation $validation = null){
        $self = $this;
        if (isset($validation)){
            $self = parent::save($validation);
        }else{
            if (isset($self->_internal_store) && isset($self->_external_pharmacist)){
                if (!$self->_internal_store->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Internal store not set', 2);
                }else{
                    $internal_store = $self->_internal_store->as_array();
                    $this->internal_store_id = $internal_store['id'];
                }
                if (!$self->_external_pharmacist->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('External pharmacist not set', 2);
                }else{
                    $external_pharmacist = $self->_external_pharmacist->as_array();
                    $this->external_pharmacist_id = $external_pharmacist['id'];
                }
            }

            if (count($self->error_messages) == 0){
                //set meta data columns
                if (isset($this->_created)){
                    $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                    $this->updated_dt = $this->created_dt;
                }
                if (isset($this->_updated)){
                    $this->updated_dt = $self->_updated->format('Y-m-d H:i:s');
                }
                $self = parent::save();
            }
        }
        return $self;
    }
}