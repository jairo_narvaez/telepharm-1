<?php
class Model_InternalStoreExternalStore extends ORM {
    protected $_table_name = 'internal_store_external_store';
    protected $_internal_store;
    protected $_external_store;
    protected $_updated;

    public function __get($key)
    {
        switch($key)
        {
            default:
                return  parent::__get($key);
        }
    }

    public function set_updated($updated){
        $this->_updated = $updated;
    }

    public static function create_internal_store_external_store(Model_Store $internal_store, Model_ExternalStore $external_store){
        $self = new static();
        $self->_internal_store = $internal_store;
        $self->_external_store = $external_store;
        return $self;
    }

    public function save(Validation $validation = null){
        $self = $this;
        if (isset($validation)){
            $self = parent::save($validation);
        }else{
            if (isset($self->_internal_store) && isset($self->_external_store)){
                if (!$self->_internal_store->loaded()){
                $self->error_messages[] = new Telepharm_ErrorMessage('Internal store not set', 2);
                }
                if (!$self->_external_store->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('External store not set', 2);
                }
                $internal_store = $self->_internal_store->as_array();
                $external_store = $self->_external_store->as_array();
                //no errors? set ref columns
                $this->internal_store_id = $internal_store['id'];
                $this->external_store_id = $external_store['id'];
            }
            if (count($self->error_messages) == 0){
                //set meta data columns
                if (isset($this->_created)){
                    $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                    $this->updated_dt = $this->created_dt;
                }
                if (isset($this->_updated)){
                    $this->updated_dt = $self->_updated->format('Y-m-d H:i:s');
                }
                $self = parent::save();
            }
        }
        return $self;
    }
}