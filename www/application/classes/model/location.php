<?php

/**
 * Location model
 */
class Model_Location extends ORM
{
	protected $_table_name = 'location';

	public function loadLocation(Telepharm_PMS_Result_Location $location)
	{
		$this->name = $location->name;
		$this->address1 = $location->address1;
		$this->address2 = $location->address2;
		$this->city = $location->city;
		$this->state = $location->state;
		$this->zip = $location->zip;
	}
}

