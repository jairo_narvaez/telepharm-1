<?php

/**
 * Notification model
 */
class Model_Notification extends ORM
{
	protected $_table_name = 'notification';

	public static function getRoleId()
	{
		$roleName = 'not found';
		$permissions = Helper_Permission::instance();
		if ($permissions->isAllowed('pharmacist', 'dashboard'))
		{
			$roleName = 'Pharmacist';
		}
		elseif ($permissions->isAllowed('technician', 'dashboard'))
		{
			$roleName = 'Technician';
		}
		$role = Model::factory('Role')->where('name', '=', $roleName)->find();

		if (!$role->loaded())
			return null;

		return $role->id;
	}

	public static function getNotProcessed(CurrentUser $currentUser, $lastNotificationId)
	{
		$role = Model_Notification::getRoleId();

		$notifications =  self::factory('Notification')->
			where_open()->
			or_where('account_id', '=', $currentUser->account->id)->
			or_where('store_id', 'in', array_keys($currentUser->stores))->
			where_close()->
			where_open()->
			or_where('role_id', '=', $role)->
			or_where('role_id', 'is', null)->
			where_close()->
			where('processed', '=', false)->
            limit(20)->
			order_by('created_dt', 'DESC');

		if ($lastNotificationId)
			$notifications = $notifications->where('id', '>', $lastNotificationId);

		return $notifications->find_all();
	}

	public static function getAll(CurrentUser $currentUser, $page = 1)
	{
		$notifications =  self::factory('Notification')->
			where_open()->
			or_where('account_id', '=', $currentUser->account->id)->
			or_where('store_id', 'in', array_keys($currentUser->stores))->
			where_close()->
			limit(100)->
			order_by('created_dt', 'DESC');
		return $notifications->find_all();
	}

	/**
	 * Set a notification as processed
	 * 
	 * @param CurrentUser $currentUser the user executing the method
	 * @param integer $notificationId 
	 * 
	 * @return bool
	 */
	public static function setProcessed(CurrentUser $currentUser, $notificationId)
	{
		$n = self::factory('Notification')->
			where('id', '=', $notificationId)->
			where('processed', '=', false)->
			find();

		if (!$n->loaded())
			throw new HTTP_Exception_404;
		
		$n->processed_by = $currentUser->account->id;
		$n->processed = true;
		$n->save();

		return $n->saved();
	}

	/**
	 * Creates a notification
	 * 
	 * @param CurrentUser $currentUser the user executing the method
	 * @param string  $type
	 * @param string  $message
	 * @param integer $to
	 * @param array   $details
	 * @param string  $roleName the role for which this notifications is intended
	 * 
	 * @return bool
	 */
	public static function publish(CurrentUser $currentUser, $type, $message, $to, $details, $roleName = 'All')
	{
		$n = self::factory('Notification');
		$role = Model::factory('Role')->where('name', '=', $roleName)->find();
		if (!$role->loaded())
			$roleId = null;
		else
			$roleId = $role->id;

		$n->type = $type;
		$n->message = Text::limit_chars($message, 100, '...');
		$n->store_id = $to;
		$n->role_id = $roleId;
		$n->details = json_encode($details);
		$n->created_by = $currentUser->account->id;

		$n->save();

		return $n->saved();
	}

	/**
	 * Creates a notification
	 * 
	 * @param CurrentUser $currentUser the user executing the method
	 * @param string  $type
	 * @param string  $message
	 * @param integer $to
	 * @param array   $details
	 * 
	 * @return bool
	 */
	public static function publishToAccount(CurrentUser $currentUser, $type, $message, $to, $details)
	{
		$n = self::factory('Notification');

		$n->type = $type;
		$n->message = Text::limit_chars($message, 100, '...');
		$n->account_id = $to;
		$n->details = json_encode($details);
		$n->created_by = $currentUser->account->id;

		$n->save();

		return $n->saved();
	}
}
