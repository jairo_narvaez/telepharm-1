<?php
class Model_PageView extends ORM
{
	protected $_table_name = 'page_view';

	protected $_belongs_to = [
		'session' => ['model' => 'Session']
	];

	static protected $privateKeys = ['password', 'confirm', 'ssn', 'old_password', 'new_password', 'confirmation'];
	static protected $noSaveKeys = ['picture'];

	static public function log($session_id, $account_id)
	{
		if (!isset($_SERVER['REQUEST_URI']))
			return null;

		$self = new static();
		$self->session_id = $session_id;
		$self->account_id = $account_id;
		$self->url = $_SERVER['REQUEST_URI'];

		if ($_POST)
		{
			# Should not log sensitive data.
			$filtered_post = $_POST;
			foreach (static::$privateKeys as $key)
			{
				if (isset($filtered_post[$key]))
					$filtered_post[$key] = str_repeat('*', strlen($filtered_post[$key]));
			}
			foreach (static::$noSaveKeys as $key) {
				if (isset($filtered_post[$key]))
					unset($filtered_post[$key]);
			}

			$self->post = @json_encode($filtered_post);
		}

		$self->save();

		return $self;
	}
}
