<?php

class Model_PatientAPISession extends ORM
{
	protected $_table_name = 'patient_api_session';

	protected $_belongs_to = [
		'patient_api_account' => ['model' => 'PatientAPIAccount'],
		'prescription' => ['model' => 'Prescription'],
	];

	protected $_virtual_fields = ['human_name'];

	public function __get($key)
	{
		switch($key)
		{
		case 'human_name': 
			return $this->patient_api_account->name . ' (#' . $this->prescription->number . ')';
		default:
			return parent::__get($key);
		}
	}
}


