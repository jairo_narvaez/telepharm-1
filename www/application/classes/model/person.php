<?php
class Model_Person extends ORM {

    protected $_table_name = 'person';

    public static function create_person($first_name, $last_name){
        $self = new static();

        $self->first_name = $first_name;
        $self->last_name = $last_name;

        return $self;
    }

    public function save(Validation $validation = null){
        $self = $this;
        //check if there was any custom validation passed for this save
        if ($validation){
            //custom validation was supplied, use it.
            $self = parent::save($validation);
        }else{
            //validate first name
            if (strlen($self->first_name) == 0){
                $self->error_messages[] = new Telepharm_ErrorMessage('First name is required', 1);
            }

            //validate last name
            if (strlen($self->last_name) == 0){
                $self->error_messages[] = new Telepharm_ErrorMessage('Last name is required', 1);
            }

            //make sure there were no errors
            if (count($self->error_messages) == 0){
                //no errors? set meta data columns
                $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                //save to database.
               $self = parent::save();
            }
        }
        return $self;
    }
}