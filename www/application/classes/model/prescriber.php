<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Prescriber extends ORM
{
	protected $_table_name = 'prescriber';

	protected $_has_many = [
		'prescription_prescribers' => [ 'model' => 'PrescriptionPrescriber'],
	];

	/**
	 * Find an existing or creates a new prescriber
	 *
	 * @param Telepharm_PMS_Result_Prescriber $prescriber the prescriber info to be saved
	 *
	 * @return Model_Prescriber
	 */
	public static function findOrCreate(Telepharm_PMS_Result_Prescriber $prescriber)
	{
        $fPrescriber = self::factory('Prescriber');
		if ($prescriber->npi != '-')
		{
			# if no npi or patient identifier is present, search on first and last name
			$fPrescriber = self::factory('Prescriber')->
				where('npi', '=', $prescriber->npi)->
				find();
		}
        else if ($prescriber->phone != '-'){
            $fPrescriber = self::factory('Prescriber')->
                where('identifier', '=', $prescriber->identifier)->
                where('first_name', '=', $prescriber->firstName)->
                where('last_name', '=', $prescriber->lastName)->
                find();
        }
		else if ($prescriber->identifier != '-')
		{
			$fPrescriber = self::factory('Prescriber')->
				where('identifier', '=', $prescriber->identifier)->
                where('first_name', '=', $prescriber->firstName)->
                where('last_name', '=', $prescriber->lastName)->
				find();
		}

		if ($fPrescriber->loaded())
			return $fPrescriber;

		$nPrescriber = self::factory('Prescriber');
		$nPrescriber->first_name = $prescriber->firstName;
		$nPrescriber->last_name = $prescriber->lastName;
		$nPrescriber->phone = $prescriber->phone;
		$nPrescriber->npi = $prescriber->npi;
		$nPrescriber->identifier = $prescriber->identifier;
		$nPrescriber->save();

		return $nPrescriber;
	}
}
