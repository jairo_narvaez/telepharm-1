<?php

/**
 * Prescription Model
 */
class Model_Prescription extends ORM
{
	protected $_table_name = 'prescription';

	protected $_belongs_to = [
		'account' => [ 'model' => 'Account' ],
		'patient_location' => [ 'model' => 'Location'],
	];

	protected $_has_one = [
		'prescription_store' => [ 'model' => 'PrescriptionStore' ],
	];

	protected $_has_many = [
		'prescription_images' => ['model' => 'PrescriptionImage'],
		'prescription_labels' => ['model' => 'PrescriptionLabel'],
		'prescription_drugs' => ['model' => 'PrescriptionDrug'],
		'prescription_dispenses' => ['model' => 'PrescriptionDispense'],
		'prescription_prescribers' => ['model' => 'PrescriptionPrescriber'],
		'prescription_logs' => ['model' => 'PrescriptionLog'],
		'prescription_comments' => ['model' => 'PrescriptionComment']
	];

	protected $_virtual_fields = [
		'store',
		'patient_location',
		'prescription_drug',
		'prescription_dispense',
		'prescription_prescriber',
		'prescription_prescriber',
		'approved_by_pharmacist',
        'transfer'
	];

	/**
	 * default flags filled on constructor
	 */
	public function __construct($id = NULL)
	{
		parent::__construct($id);
		if (! $this->loaded())
		{
			$this->is_draft = true;
			$this->show = true;

			$this->on_hold = false;
			$this->approved_on_hold = false;
			$this->approved_for_counsel = false;
			$this->required_counsel = false;
			$this->refused_counsel = false;
			$this->rejected = false;
			$this->completed = false;
			$this->canceled = false;
			$this->archived = false;
		}
	}

	/**
	 * Return a query on prescriptions
	 * already scoped by the user privileges
	 *
	 * @param CurrentUser $currentUser
	 *
	 * @return query
	 */
	public static function getScopedQuery(CurrentUser $currentUser)
	{
		$permissions = Helper_Permission::instance();
		if ($permissions->isAllowed('prescription', 'admin'))
		{
			$query = self::factory('Prescription');
		}
		else
		{
			$query = self::factory('Prescription')->
					join('prescription_store')->on('prescription_store.prescription_id', '=', 'prescription.id')->
					join('store_role')->on('store_role.store_id', '=', 'prescription_store.store_id')->
					join('role')->on('role.id', '=', 'store_role.role_id')->
					where('role.type', '=', 'store')->
					where('store_role.account_id', '=', $currentUser->account->id)->
					where('store_role.store_id', 'in', array_keys($currentUser->stores));
		}
		return $query;
	}

	/**
	 * Search all the prescriptions based on a query parameter
	 *
	 * @param CurrentUser $currentUser The current context
	 * @param string      $query       Optional, defaults to ''.
	 *
	 * @return array
	 */
	public static function searchPrescriptionsFor(CurrentUser $currentUser, $query = '')
	{
		$prescriptions = [];
		foreach(Model_Prescription::getScopedQuery($currentUser)->
			where('pre_draft', '=', false)->
			where_open()->
			where('patient_fname', 'like', '%'.$query.'%')->
			or_where('patient_lname', 'like', '%'.$query.'%')->
			or_where('number', '=', $query)->
			or_where('prescription.id', '=', $query)->
			where_close()->
            removeVirtualFields(['transfer'])->
			find_all() as $rx)
		{
			$prescriptions[] = $rx->as_array();
		}
		return $prescriptions;
	}

	/**
	 * Search all the prescriptions based on a query array
	 * of parameters sent by advanced search
	 *
	 * @param CurrentUser $currentUser The current context
	 * @param Array       $query       List of parameters of advanced search
	 *
	 * @return Array
	 */
	public static function fullSearchPrescriptions(CurrentUser $currentUser, $query)
	{
		$resul = [];
		$prescriptions = Model_Prescription::getScopedQuery($currentUser)->
			where('pre_draft', '=', false);

		if ($query['store'] != 0)
		{
			$prescriptions = $prescriptions->
				where('store_role.store_id', '=', $query['store']);
		}

		if ($query['name'] != '')
		{
			$prescriptions = $prescriptions->
				where_open()->
				where('prescription.patient_fname', 'like', '%'.$query['name'].'%')->
				or_where('prescription.patient_lname', 'like', '%'.$query['name'].'%')->
				or_where(DB::expr('CONCAT(prescription.patient_fname,' .
					'\' \', prescription.patient_lname)'),
				   'like', '%'.$query['name'].'%')->
				where_close();
		}

		if ($query['rxNumber'] != '')
		{
			$prescriptions = $prescriptions->
				where('prescription.number', '=', $query['rxNumber']);
		}

		if ($query['fromDate'] != '')
		{
			$prescriptions = $prescriptions->
				where('prescription.created_dt', '>=', $query['fromDate']);
		}

		if ($query['toDate'] != '')
		{
			$prescriptions = $prescriptions->
				where('prescription.created_dt', '<=', $query['toDate']);
		}
        $prescriptions->removeVirtualFields(['transfer']);
		$prescriptions_all = $prescriptions->find_all();
		foreach($prescriptions_all as $rx)
		{
			switch($query['status'])
			{
			case 'approval' : 
				if ($rx->isAwaitingApproval())
					$result[] = $rx->as_array();
				break;
			case 'on-hold' : 
				if ($rx->isOnHold() && !$rx->inFinalStatus())
					$result[] = $rx->as_array();
				break;
			case 'counsel' : 
				if ($rx->isAwaitingCounsel())
					$result[] = $rx->as_array();
				break;
			case 'completed' : 
				if ($rx->completed)
					$result[] = $rx->as_array();
				break;
			case 'cancelled' : 
				if ($rx->canceled)
					$result[] = $rx->as_array();
				break;
			case 'archived' : 
				if ($rx->archived)
					$result[] = $rx->as_array();
				break;
			case 'error' :
				if ($rx->reported_error)
					$result[] = $rx->as_array();
				break;
			case 'all':
			default:
			case 'approval' : 
				$result[] = $rx->as_array();
				break;
			}
		}
		return $result;
	}

	/**
	 * Calculate the counters for the analytics
	 *
	 * @param CurrentUser $currentUser
	 *
	 * @return array
	 */
	public static function getAnalytics(CurrentUser $currentUser)
	{
		$refills = Model_Prescription::getScopedQuery($currentUser)->
			where('is_refill', '=', true)->
			where('on_hold', '=', false)->
			where('completed', '=', true)->
			where('reported_error', '=', false)->
			count_all();
		$news = Model_Prescription::getScopedQuery($currentUser)->
			where('is_refill', '=', false)->
			where('on_hold', '=', false)->
			where('completed', '=', true)->
			where('reported_error', '=', false)->
			count_all();
		$rejected = Model_Prescription::getScopedQuery($currentUser)->
			where('required_counsel', '=', true)->
			where('refused_counsel', '=', true)->
			where('completed', '=', true)->
			where('reported_error', '=', false)->
			count_all();
		$accepted = Model_Prescription::getScopedQuery($currentUser)->
			where('required_counsel', '=', true)->
			where('refused_counsel', '=', false)->
			where('completed', '=', true)->
			where('reported_error', '=', false)->
			count_all();
		$withError = Model_Prescription::getScopedQuery($currentUser)->
			where('completed', '=', true)->
			where('reported_error', '=', true)->
			count_all();

		return [
			'refills' => intval($refills),
			'news' => intval($news),
			'rejected' => intval($rejected),
			'accepted' => intval($accepted),
			'with_error' => intval($withError),
		];
	}

	/**
	 * Get the list of statuses for a prescription
	 *
	 * @return array[statuses]
	 */
	public function getStatuses()
	{
		$result = [];
		$statuses = [
			'is_new',
			'is_partial',
			'is_refill',
			'on_hold',
			'approved_on_hold',
			'show',
			'is_draft',
            'transfer_rx',
            'completed_transfer',
			'pre_draft',
			'approved_for_counsel',
			'required_counsel',
			'refused_counsel',
			'rejected',
			'canceled',
			'archived',
			'completed',
			'priority',
			'reported_error',
			];
		foreach ($this->as_array() as $key => $value)
		{
			if (in_array($key, $statuses))
				$result[$key] = $value;
		}
		return $result;
	}

	/**
	 * Update the value of priority on a prescription
	 * only if the user has access to that
	 *
	 * @param CurrentUser $currentUser
	 * @param int         $prescriptionId
	 * @param int         $newPriority
	 *
	 * @return boolean
	 */
	public static function updatePriority(CurrentUser $currentUser, $prescriptionId, $newPriority)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if ($prescription->inFinalStatus())
			throw new Exception('Prescription can not change the priority');

		$prescription->priority = $newPriority;
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'update-priority');

		return $prescription->getStatuses();
	}

	/**
	 * Change the status of approved for counsel
	 * of a prescription
	 *
	 * @param CurrentUser $currentUser
	 * @param integer     $prescriptionId
	 * @param boolean     $andRequestCounsel
	 *
	 * @return array[statuses]
	 */
	public static function approve(CurrentUser $currentUser, $prescriptionId, $andRequestCounsel = false)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		$prescription->approveInternal($andRequestCounsel);
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'approve');

		/*
		// publish the approval as notification
		Model_Notification::publish($currentUser,
			'approved', 'Prescription #'.$prescription->number.' has been approved',
			$prescription->prescription_store->store_id, [
			'prescription_id' => $prescription->id,
			],
			'Technician'
		);
		*/

		return $prescription->getStatuses();
	}

    /**
     * Change the status of required_counsel
     * of a prescription
     *
     * @param CurrentUser $currentUser
     * @param integer     $prescriptionId
     * @param boolean     $andRequestCounsel
     *
     * @return array[statuses]
     */
    public static function requiredCounsel(CurrentUser $currentUser, $prescriptionId)
    {
        $prescription = self::getScopedQuery($currentUser)->
            where('prescription.id', '=', $prescriptionId)->
            find();
        $now = new DateTime();
        $prescription->updated_dt = $now->format('"Y-m-d H:i:s');
        $prescription->approveInternal(true);
        $prescription->save();

        Model_PrescriptionLog::createLog($currentUser, $prescription, 'approve');
        // publish the approval as notification
        Model_Notification::publish($currentUser,
            'approved', 'Prescription #'.$prescription->number.' has been approved',
            $prescription->prescription_store->store_id, [
                'prescription_id' => $prescription->id,
            ],
            'Technician'
        );

        return $prescription->getStatuses();
    }

	/**
	 * change the flags to approve the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function approveInternal($andRequestCounsel = false)
	{
		if (!$this->canApprove())
			throw new Exception('Prescription can not be approved');

		$this->approved_for_counsel = true;
		$this->required_counsel = $andRequestCounsel;
	}

	/**
	 * Change the status of approved on hold
	 * of a prescription
	 *
	 * @param CurrentUser $currentUser
	 * @param integer     $prescriptionId
	 *
	 * @return array[statuses]
	 */
	public static function approveOnHold(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		$prescription->approveOnHoldInternal();
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'approve-on-hold');
		// publish the approval as notification
		Model_Notification::publish($currentUser,
			'approved',
            'Prescription #'.$prescription->number.' has been approved on hold',
			$prescription->prescription_store->store_id, [
			'prescription_id' => $prescription->id,
			],
			'Technician'
		);

		return $prescription->getStatuses();
	}

	/**
	 * change the flags to approve on hold the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function approveOnHoldInternal()
	{
		if (! $this->canApprove() )
			throw new Exception('Prescription can not be approved');

		$this->approved_on_hold = true;
		# show to false in order to not appear on queue
		$this->show = false;
	}

	/**
	 * Change the status of rejected
	 * of a prescription
	 *
	 * @param CurrentUser $currentUser
	 * @param integer     $prescriptionId
	 *
	 * @return array[statuses]
	 */
	public static function reject(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		$prescription->rejectInternal();
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'reject');
		// publish the reject as notification
		Model_Notification::publish($currentUser,
			'rejected', 'Prescription #'.$prescription->number.' has been rejected',
			$prescription->prescription_store->store_id, [
			'prescription_id' => $prescription->id,
			],
			'Technician'
		);

		return $prescription->getStatuses();
	}

	/**
	 * change the flags to reject the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function rejectInternal()
	{
		if (! $this->canReject() )
			throw new Exception('Prescription can not be rejected');
		$this->rejected = true;
	}

	/**
	 * Change the status to completed counsel
	 * of a prescription
	 *
	 * @param CurrentUser $currentUser
	 * @param integer     $prescriptionId
	 * @param boolean     $patientAcceptedCounsel
	 *
	 * @return array[statuses]
	 */
    public static function completeCounsel(CurrentUser $currentUser,
                                           $prescriptionId,
                                           $patientAcceptedCounsel = false,
                                           $counselingPharmacistId = 0,
                                           $technician_complete_type = 0,
                                           $other_message = null)
    {
        $prescription = self::getScopedQuery($currentUser)->
            where('prescription.id', '=', $prescriptionId)->
            find();
        $permissions = Helper_Permission::instance();
        if ($permissions->isAllowed('pharmacist', 'dashboard')){
            $call_queue = ORM::factory('CallQueue')
                ->where('from_prescription_id', '=', $prescription->id)
                ->order_by('id', 'desc')->find();
            $patient_api_session = ORM::factory('PatientAPISession')
                ->where('prescription_id', '=', $prescription->id)
                ->order_by('id', 'desc')->find();
            $patient_api_session_prescriptions = ORM::factory('PatientAPISessionPrescription')
                ->where('patient_api_session_id', '=', $patient_api_session->id)
                ->find_all()->untaint();
            $diff = null;
            if ($call_queue->loaded() && $patient_api_session->loaded())
            {
                $now = new DateTime();
                if (!isset($call_queue->end_dt)){
                    $call_queue->end_dt = $now->format('Y-m-d H:i:s');
                    $call_queue->save();
                }
                $begin = new DateTime($call_queue->accepted_dt);
                $end = new DateTime($call_queue->end_dt);
                $diff = date_diff($end, $begin);
            }
            foreach ($patient_api_session_prescriptions as $next_prescription){
                $other_rx = Model_Prescription::getScopedQuery($currentUser)->where('prescription.id', '=', $next_prescription['prescription_id'])->find();
                if ($other_rx->loaded() && !$other_rx->completed){
                    if ($counselingPharmacistId > 0){
                        $other_rx->pharmacist_account_id = $counselingPharmacistId;
                        $other_rx->counsel_dt = date('Y-m-d H:i:s');
                    }

                    $other_rx->completeCounselInternal($patientAcceptedCounsel);
                    $other_rx->save();
                    Model_PrescriptionLog::createLog($currentUser, $other_rx, 'complete-counsel');
                    if (isset($diff)){
                        Model_PrescriptionLog::createLog($currentUser, $other_rx, 'video-call-ended', 'Call length: '.$diff->format('%i minutes, %s seconds'));
                    }else{
                        Model_PrescriptionLog::createLog($currentUser, $other_rx, 'video-call-ended');
                    }
                }
            }
        }else{
            if ($technician_complete_type == 0){
                throw new Exception('You must provide a reason for completing this prescription.');
            }
            $prescription->completeCounselInternal($patientAcceptedCounsel);
            $prescription->save();
            $message = null;
            switch ($technician_complete_type){
                case Model_TechnicianCompleteType::No_Pick_Up:
                    $message = 'No Pick up';
                    break;
                case Model_TechnicianCompleteType::Mail_Order:
                    $message = 'Mail Order';
                    break;
                case Model_TechnicianCompleteType::Delivery:
                    $message = 'Delivery';
                    break;
                case Model_TechnicianCompleteType::Other:
                    if (strlen($other_message) == 0){
                        throw new Exception('Must provide a message when selecting other');
                    }
                    $message = $other_message;
                        break;
                default:
                    throw new Exception('Invalid Reason');
            }
            Model_PrescriptionLog::createLog($currentUser, $prescription, 'complete-counsel-technician', $message);
        }



		/*
		// publish the complete as notification
		Model_Notification::publish($currentUser,
			'completed', 'Prescription #'.$prescription->number.' has been completed',
			$prescription->prescription_store->store_id, [
			'prescription_id' => $prescription->id,
			],
			'Technician'
		);
		*/

		return $prescription->getStatuses();
	}


    public static function endCounsel(CurrentUser $currentUser, $prescriptionId, $patientAcceptedCounsel = false, $counselingPharmacistId = 0)
    {
        $prescription = self::getScopedQuery($currentUser)->
            where('prescription.id', '=', $prescriptionId)->
            find();
        if ($counselingPharmacistId > 0){
            $prescription->pharmacist_account_id = $counselingPharmacistId;
            $prescription->counsel_dt = date('Y-m-d H:i:s');
        }
        Model_PrescriptionLog::createLog($currentUser, $prescription, 'complete-counsel');
        $prescription->completeCounselInternal($patientAcceptedCounsel);
        $prescription->save();



        /*
        // publish the complete as notification
        Model_Notification::publish($currentUser,
            'completed', 'Prescription #'.$prescription->number.' has been completed',
            $prescription->prescription_store->store_id, [
            'prescription_id' => $prescription->id,
            ],
            'Technician'
        );
        */

        return $prescription->getStatuses();
    }
	/**
	 * change the flags to complete counsel the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function completeCounselInternal($patientAcceptedCounsel)
	{
		if (! $this->canCompleteCounsel())
			throw new Exception('Prescription can not be completed');
		$this->refused_counsel = ! $patientAcceptedCounsel;
        if ($this->refused_counsel){
            $this->show = false;
        }
		$this->completed = true;
	}

	/**
	 * Change the status of canceled
	 * of a prescription
	 *
	 * @param CurrentUser $currentUser
	 * @param integer     $prescriptionId
	 *
	 * @return array[statuses]
	 */
	public static function cancel(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (!$prescription->loaded())
			throw new HTTP_Exception_404('Prescription not found');
		
		$prescription->cancelInternal();
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'cancel');

		return $prescription->getStatuses();
	}

	/**
	 * change the flags to cancel the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function cancelInternal()
	{
		if (!$this->canCancel())
			throw new Exception('Prescription can not be canceled');
		$this->canceled = true;
	}
	/**
	 * Find a prescription filter by current user account read privileges
	 *
	 * @param CurrentUser $currentUser
	 * @param  int $prescriptionId
	 *
	 * @return Prescription_Model
	 */
	public static function findRestrictedToCurrentAccount(CurrentUser $currentUser, $prescriptionId)
	{
		return self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			where('prescription.account_id', '=', $currentUser->account->id)->
			find();
	}
	/**
	 * Update values of prescription options files
	 *
	 * @param  CurrentUser $currentUser
	 * @param  int         $prescriptionId
	 * @param  string      $optionName
	 * @param  int         $value
	 *
	 * @return new statuses
	 */
	public static function updateOption(CurrentUser $currentUser, $prescriptionId, $optionName, $value)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			where('prescription.is_draft', '=', true)->
			find();
		if(!$prescription->loaded())
			throw new HTTP_Exception_404;

		$prescription->updateOptionInternal($optionName, $value);
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'update-option');

		return $prescription->getStatuses();
	}

	/**
	 * change the flags to update an option the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function updateOptionInternal($optionName, $value)
	{
		if (!$this->canUpdateOption())
			throw new Exception('Prescription can not update an option');

		$this->set($optionName, $value);
	}

	/**
	 * Get related prescriptions by patient name
	 * and current user security
	 *
	 * @param CurrentUser $currentUser
	 *
	 * @return array<Model_Prescription>
	 */
	public function additionalPatientPrescriptions(CurrentUser $currentUser, $current_index = 0)
	{
        $take = 10;
        $start = $current_index * $take;
        $count = self::getScopedQuery($currentUser)->
            where('patient_fname', '=', $this->patient_fname)->
            where('patient_lname', '=', $this->patient_lname)->
            where('patient_dob', '=', $this->patient_dob)->
            where('prescription.id', '<>', $this->id)->
            count_all();
        $final_index = $start + $take;
        if (($final_index) > $count && $count > 0){
            $take = $take - ($final_index % $count);
        }
        
		$prescriptions = self::getScopedQuery($currentUser)->
			where('patient_fname', '=', $this->patient_fname)->
			where('patient_lname', '=', $this->patient_lname)->
			where('patient_dob', '=', $this->patient_dob)->
			where('prescription.id', '<>', $this->id)->
            limit($take, $start)->
			find_all();
        $results = array();
        foreach ($prescriptions as $prescription){
            $results[] = $prescription->
                removeVirtualFields()->
                addVirtualField('drug')->
                addVirtualField('dispense')->
                addVirtualField('prescriber')->
                as_array();
        }

        return ['results'=>$results, 'for_index'=>$current_index, 'total_count'=> $count];
	}

	/**
	 * Get the list of prescription_labels
	 *
	 * @return array<Model_PrescriptionLabel>
	 */
    public function getLabels()
    {
        return $this->prescription_labels->find_all()->untaint();

    }


	/**
	 * Get virtual fields or default behavior
	 *
	 * @param string $key
	 *
	 * @return mixed
	 */
	public function __get($key)
	{
		switch($key)
		{
		case 'images':
			$images = [];
			foreach($this->prescription_images->find_all() as $pi)
				$images[] = $pi->as_array();
			return $images;
        case 'drug':
            return $this->prescription_drugs->find()->as_array();
        case 'prescriber':
            return $this->prescription_prescribers->find()->as_array();
		case 'prescription_drug':
			return $this->prescription_drugs->find();
		case 'prescription_dispense':
			return $this->prescription_dispenses->find();
        case 'dispense':
            return $this->prescription_dispenses->find()->as_array();
		case 'prescription_prescriber':
			return $this->prescription_prescribers->find();
        case 'associated_store':
            $store = new Model_Store($this->prescription_store->store_id);
            $store->addVirtualField('location');
            return $store;
		case 'store':
			return $this->prescription_store->untaint();
            case 'approved_by_pharmacist_account_id':
                $log = $this->prescription_logs->
                    where('action', '=', 'approve')->
                    order_by('created_dt', 'DESC')->
                    find();
                if ($log->loaded())
                    return $log->account_id;
                return null;
		case 'approved_by_pharmacist':
			$log = $this->prescription_logs->
				where('action', '=', 'approve')->
				order_by('created_dt', 'DESC')->
				find();
			if ($log->loaded())
				return $log->created_by;
			return null;
        case 'counseling_pharmacist_full_name':
            $pharmacist = new Model_Account($this->pharmacist_account_id);
            if ($pharmacist->loaded()){
                return $pharmacist->full_name;
            }else{
                return null;
            }
        case 'transfer':
             $transfer = ORM::factory('PrescriptionTransfer')->
                 where('prescription_id', '=', $this->id)->
                 find();
            if ($transfer->loaded()){
                return $transfer->unset_unused_properties();
            }else{
                return [];
            }
            case 'signature_dt':
                $log = $this->prescription_logs->
                    where('action', '=', 'complete-counsel')->
                    order_by('created_dt', 'DESC')->
                    find();
                return $log->loaded() ? $log->created_dt : null;
		default:
			return  parent::__get($key);
		}
	}

	/**
	 * load the data from the result object
	 */
	public function loadResultPrescription(Telepharm_PMS_Result_Prescription $prescription)
	{
		$this->patient_id    = $prescription->patientID;
		$this->patient_fname = $prescription->patientFirstName;
		$this->patient_lname = $prescription->patientLastName;
		$this->patient_dob   = $prescription->patientDob;
		$this->patient_phone = $prescription->patientPhone;
		$this->patient_email = $prescription->patientEmail;
		$this->number        = $prescription->number;
		$this->refill_number = $prescription->refillNumber;
		$this->is_refill     = $prescription->isRefill;
		$this->date_filled   = $prescription->dateFilled;
		$this->date_expires  = $prescription->dateExpires;
		$this->date_written  = $prescription->dateWritten;
        $this->is_new = $prescription->isNew;
		return $this;
	}

	/**
	 * Change is_draft status
	 *
	 * @param  CurrentUser $currentUser
	 * @param  int         $prescriptionId
	 *
	 * @return boolean
	 */
	public static function submit(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if(!$prescription->loaded())
			throw new HTTP_Exception_404;

		$prescription->submitInternal();
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'submit');

		// publish the submit as notification
		Model_Notification::publish($currentUser,
			'submitted', 'Prescription #'.$prescription->number.' is Awaiting Approval',
			$prescription->prescription_store->store_id, [
			'prescription_id' => $prescription->id,
			],
			'Pharmacist'
		);

		return $prescription->saved();
	}

	/**
	 * change the flags to submit the prescription
	 * this is used on unit testing to avoid save the prescription
	 */
	public function submitInternal()
	{
		if (!$this->canSubmit() && !$this->is_partial)
			throw new Exception('Prescription can not be submitted');

		// check all images uploaded if not on hold
		if ($this->prescription_images->count_all() < $this->associated_store->rx_required_image_number && !$this->on_hold)
		{
			throw new Exception("To submit we need all images uploaded");
		}

		$this->is_draft = false;
		$this->completed = false;
		$this->rejected = false;
		$this->show = true;
	}

	/**
	 * 2nd submit when a rx is on-hold status
	 *
	 * @param  CurrentUser $currentUser
	 * @param  int         $prescriptionId
	 *
	 * @return boolean
	 */
	public static function reSubmit(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			where('prescription.on_hold', '=', true)->
			where('prescription.approved_on_hold', '=', true)->
			find();

		if(!$prescription->loaded())
			throw new HTTP_Exception_404;

		$prescription->reSubmitInternal();
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 're-submit');
		
		// publish the submit as notification
		Model_Notification::publish($currentUser,
			'submitted', 'Prescription #'.$prescription->number.' is Awaiting Approval',
			$prescription->prescription_store->store_id, [
			'prescription_id' => $prescription->id,
			],
			'Pharmacist'
		);

		return $prescription->saved();
	}

	/**
	 * change the flags to resubmit prescriptions
	 * this is used on unit testing to avoid save the prescription
	 */
	public function reSubmitInternal()
	{
		if (! $this->canSubmit() && !$this->is_partial)
			throw new Exception('Prescription can not be submitted');

		// check all images uploaded
		if ($this->prescription_images->count_all() < $this->associated_store->rx_required_image_number)
			throw new Exception("To submit we need all images uploaded");

		$this->is_draft = false;
		$this->show = true;
		$this->on_hold = false;
		$this->approved_on_hold = false;
		$this->rejected = false;
		$this->approved_for_counsel = false;
		$this->required_counsel = false;
		$this->refused_counsel = false;
	}

	/**
	 * save the signature based on the raw body of the image
	 *
	 * @param  String $image_data: base64-encoded image data
	 */
	public function saveSignature($image_data)
	{
		$name = $this->id . '_signature';

		$image = Model_Image::processBase64Upload('base64,' . $image_data,
			$name,
			'signature',
			['resizedCrop' => []]);

		$settings = array_merge($image->originalSize,
			[
			'file_path' => Model_Image::getThumbnailsImageFolder('signature')
			]);
		// copy to signatures directory with same size
		$image->img->resizeAndCrop($settings);

		$this->signature_image_id = $image->id;
		$this->save();

		return $image->id;
	}

	/**
	 * toggle is_high_priority
	 *
	 * @param  CurrentUser $currentUser
	 * @param  int         $prescriptionId
	 * @param  int         $value
	 *
	 * @return new statuses
	 */
	public static function setHighPriority(CurrentUser $currentUser, $prescriptionId, $value)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if(!$prescription->loaded())
			throw new HTTP_Exception_404;

		if ($prescription->inFinalStatus())
			throw new Exception('Prescription can not change the priority');

		$prescription->priority = $value;
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'toggle-priority');

		return $prescription->getStatuses();
	}

	public static function reportError(CurrentUser $currentUser, $prescriptionId, $text)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if(!$prescription->loaded())
			throw new HTTP_Exception_404;

		if (!$prescription->canReportError())
			throw new Exception('Prescription can not report an error');

		$prescription->reported_error = true;
		$prescription->error_message = $text;
		$prescription->save();

		Model_PrescriptionLog::createLog($currentUser, $prescription, 'report-error');

		return $prescription->getStatuses();
	}

	/**
	 * get the stage of a prescription if awaiting counsel
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function isAwaitingCounsel() {
	  return $this->approved_for_counsel &&
			 !$this->completed &&
			 !$this->canceled;
	}

	/**
	 * get the stage of a prescription if awaiting approval
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function isAwaitingApproval() {
	  return !$this->is_draft &&
			 !$this->canceled &&
			 !$this->completed &&
			 !$this->rejected &&
			 !$this->isAwaitingCounsel() &&
			 $this->show;
	}

	/**
	 * get the stage of a prescription if in final status
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function inFinalStatus() {
	  return $this->completed ||
			 $this->canceled ||
			 $this->archived ||
             $this->completed_transfer;
	}

	/**
	 * get the stage of a prescription if on hold
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function isOnHold () {
	  return $this->on_hold || $this->approved_on_hold;
	}

	/**
	 * check if a prescription can be submitted
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canSubmit() {
		if ($this->inFinalStatus() && !$this->is_partial) return false;
		if ($this->is_draft ||
			$this->rejected ||
			$this->approved_on_hold ) return true;
	  return false;
	}

	/**
	 * check if a prescription can be canceled
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canCancel() {
	  if ($this->inFinalStatus()) return false;
	  if ($this->is_draft ||
		$this->isAwaitingCounsel() ||
		$this->rejected ||
		$this->approved_on_hold ) {
		return true;
	  }
	  return false;
	}

	/**
	 * check if a prescription can be updated
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canUpdateOption() {
	  if ($this->inFinalStatus()) return false;
	  if ($this->is_draft) return true;
	  return false;
	}

	/**
	 * check if a prescription can be approved
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canApprove() {
	  if ($this->inFinalStatus() ||
		  $this->isAwaitingCounsel() ||
		  $this->is_draft ||
		  $this->rejected ||
		  !$this->show ) return false;
	  return true;
	}

	/**
	 * check if a prescription can be request counsel
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canRequestCounsel() {
	  if ($this->inFinalStatus() ||
		$this->isAwaitingCounsel() ||
		$this->is_draft ||
		$this->rejected ) return false;
	  return true;
	}

	/**
	 * check if a prescription can be completed
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canCompleteCounsel() {
	/*
	  if ($this->inFinalStatus() ||
		$this->is_draft ||
		!$this->isAwaitingCounsel()) return false;
	*/

		if ($this->inFinalStatus() || $this->is_draft) return false;

		return true;
	}

	/**
	 * check if a prescription can be refused
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canRefuseCounsel() {
	  if ($this->inFinalStatus() ||
		$this->is_draft ||
		!$this->isAwaitingCounsel()) return false;
	  return true;
	}

	/**
	 * check if a prescription can be rejected
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canReject() {
	  if ( $this->on_hold && $this->approved_on_hold) return false;
	  if ( $this->inFinalStatus() ||
		   $this->is_draft ||
		   $this->isAwaitingCounsel() ||
		   $this->rejected ) return false;
	  return true;
	}

	/**
	 * check if a prescription can be reported as error
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canReportError() {
	  return $this->completed && ! $this->reported_error;
	}

	/**
	 * check if a prescription can be edited/updated
	 * this function should be in sync with js/app/model/prescription.js
	 */
	public function canEdit() {
		if ($this->inFinalStatus()) return false;
		if ($this->approved_for_counsel) return false;
		if ($this->rejected) return true;
		if ($this->canSubmit()) return true;
		return false;
	}

	/**
	 * get pharmacist and technician of the prescription
	 */
	public static function getParticipants(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = self::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if(!$prescription->loaded())
			throw new HTTP_Exception_404;

		$technician = $prescription->prescription_logs->
			where('action', '=', 'create')->
			find();

		$pharmacist = $prescription->prescription_logs->
			where('action', '=', 'approve')->
			find();

		return [
			'technician' => $technician->created_by,
			'pharmacist' => $pharmacist->created_by,
			];
	}

    /**
    /**
     * check if a prescription number is already created on
     * the system
     */
    public static function searchBeforeCreate(CurrentUser $currentUser, $prescriptionNumber)
    {
        $number = $prescriptionNumber;
        $refill = 0;
        $numberParts = explode('-', $prescriptionNumber);
        if (count($numberParts) == 1)
        {
            $number = trim($prescriptionNumber);
        }
        elseif (count($numberParts) == 2)
        {
            $number = trim($numberParts[0]);
            $refill = trim($numberParts[1]);
        }
        else
            throw new Exception('Invalid prescription number');

        $result = [];
        $query = Model_Prescription::getScopedQuery($currentUser)->
            where('canceled' , '=', false)->
            where('number', '=', $number);

        if ($refill > 0){
            $query->where('refill_number', '=', $refill);
        }else{
            $query->order_by('refill_number', 'ASC');
        }

        foreach($query->find_all()  as $prescription)
        {
            $result[] = $prescription->untaint();
        }

        return $result;
    }
        
    /**
     * Return an array of prescriptions
     * for week statistic on completed
     * prescription by stores
     *
     * @param CurrentUser $currentUser
     *
     * @return array
     */
     public static function getCompletedByStores(CurrentUser $currentUser, $interval='week'){

         $storeNames = Model_Prescription::getWeekCompletedStoreName($currentUser, $interval);

         $permissions = Helper_Permission::instance();
         if ($permissions->isAllowed('prescription', 'admin'))
         {
             $queryText = "SELECT COUNT(prescription.id) as total, prescription_store.store_name, "
                 ."prescription_store.store_id, prescription.updated_dt  FROM `prescription` "
                 ."LEFT JOIN `prescription_store` ON prescription.id = prescription_store.prescription_id "
                 ."WHERE prescription.completed=:completed AND "
                 ."prescription.updated_dt BETWEEN :from AND :to "
                 ."GROUP BY prescription_store.store_id;";

             $statistics = array();
             for($i=7; $i>=0; $i--)
             {
                 if ($interval=='month')
                 {
                     $startDate = date("Y-m-d", time()-(345600*($i+1)));
                     $endDate = date("Y-m-d", time()-(345600*($i)));
                     $chartDate = date("d.m", time()-(345600*($i+1)))."-".date("d.m", time()-(345600*($i)));
                 }
                 else
                 {
                     $startDate = date("Y-m-d", time()-(86400*($i+1)));
                     $endDate = date("Y-m-d", time()-(86400*($i)));
                     $chartDate = date("d.m", time()-(86400*($i)));
                 }

                 $result = DB::query(Database::SELECT,$queryText)
                     ->param(':completed', 1)
                     ->param(':from', $startDate)
                     ->param(':to', $endDate)
                     ->execute()
                     ->as_array();

                 $statistics[$i][] = $chartDate;
                 foreach($storeNames AS $storeId => $storeName)
                 {
                     if (count($result)!==0)
                     {
                         $statistics[$i][$storeId] = 0;
                         foreach($result AS $line ){
                             if ($line['store_id']==$storeId){
                                 $statistics[$i][$storeId] = (int) $line['total'];
                                 break;
                             }
                         }
                     }
                     else {
                         $statistics[$i][$storeId] = 0;
                     }
                 }
             }

             $stores = array('Date');
             foreach ($storeNames AS $store)
             {
                 array_push($stores , $store);
             }

             return array_merge( array($stores), $statistics );
         }
         else
         {
             return false;
         }
     }

    /**
     * Return an array of store
     * names with completed prescription on last week
     * prescription by stores
     *
     * @param CurrentUser $currentUser
     *
     * @return array
     */
    public static function getWeekCompletedStoreName(CurrentUser $currentUser, $interval)
    {
        $result = false;

        $permissions = Helper_Permission::instance();
        if ($permissions->isAllowed('prescription', 'admin'))
        {

            $queryText = "SELECT prescription_store.store_id, prescription_store.store_name "
                ."FROM `prescription` "
                ."LEFT JOIN `prescription_store` ON prescription.id = prescription_store.prescription_id "
                ."WHERE prescription.completed=:completed AND prescription.updated_dt BETWEEN :from AND :to "
                ."GROUP BY prescription_store.store_id ;";

            $result = DB::query(Database::SELECT,$queryText)
                ->param(':completed', 1);

            if ($interval=='month')
            {
               $result->param(':from', date("Y-m-d", time()-(86400*(30))))
                    ->param(':to', date("Y-m-d", time()+86400));
            }
            else
            {
               $result->param(':from', date("Y-m-d", time()-(86400*(7))))
                    ->param(':to', date("Y-m-d", time()+86400));
            }

            $result = $result->execute()
                             ->as_array();

            if (count($result)!==0)
            {
                $temp = array();
                foreach($result AS $line)
                {
                    $temp[$line['store_id']] = $line['store_name'];
                }
                $result = $temp;
                unset($temp);
            }
        }

        return $result;
    }

    public function begin_transfer(CurrentUser $currentUser)
    {
        $errors = array();
        if (!$this->loaded()){
            $errors = array_merge($errors, new Telepharm_ErrorMessage('Prescription does not exist', 1));
        }else{
            if (!($this->approved_on_hold || $this->completed)){
                $errors = array_merge($errors, new Telepharm_ErrorMessage('Prescription cannot be transfered', 1));
            }
        }
        if (count($errors) == 0){
            $this->transfer_rx = true;
            $this->approved_on_hold = false;
            $this->show = true;
            $this->save();

            $prescription = $this->as_array();
            Model_PrescriptionLog::createLog($currentUser, $this, 'transfer');

            Model_Notification::publish($currentUser,
                'transfer', 'Prescription #'.$prescription['number'].' has been transfered',
                $prescription['store']['store_id'], [
                    'prescription_id' => $prescription['id'],
                ],
                'Pharmacist'
            );
        }
        return array('prescription' => $this, 'statuses' => $this->getStatuses(), 'errors' => $errors);
    }

    public function complete_transfer(CurrentUser $currentUser)
    {
        $errors = array();
        $permissions = Helper_Permission::instance();
        if (!$permissions->isAllowed('pharmacist', 'dashboard')) {
            $errors = array_merge($errors, new Telepharm_ErrorMessage('You are not authorized to perform this action', 1));
        }else{
            if (!$this->loaded()){
                $errors = array_merge($errors, new Telepharm_ErrorMessage('Prescription does not exist', 1));
            }else{
                if (!($this->transfer_rx)){
                    $errors = array_merge($errors, new Telepharm_ErrorMessage('Prescription transfer cannot be completed - this prescription has not been transferred', 1));
                }
            }
            if (count($errors) == 0){
                $this->transfer_rx = false;
                $this->completed_transfer = true;
                $this->completed = false;
                $this->show = false;
                $this->save();

                Model_PrescriptionLog::createLog($currentUser, $this, 'transfer_completed');
            }
        }
        return array('prescription' => $this, 'statuses' => $this->getStatuses(), 'errors' => $errors);
    }

    public static function get_queue(CurrentUser $currentUser, $acl){
        if ($acl->isAllowed($currentUser, 'pharmacist', 'dashboard'))
        {
            // pharmacist
            $normal = Model_Prescription::getScopedQuery($currentUser)->
                where('pre_draft', '=', false)->
                where('is_draft', '=', false)->
                where('show', '=', true)->
                where('completed', '=', false)->
                where('canceled', '=', false)->
                where('archived', '=', false)->
                where('approved_for_counsel', '=', false)->
                find_all()->
                untaint();

            $transfers = Model_Prescription::getScopedQuery($currentUser)->
                where('transfer_rx', '=', true)->
                where('completed_transfer', '=', false)->
                find_all()->
                untaint();

            return array_merge($transfers, $normal);
        }
        elseif ($acl->isAllowed($currentUser, 'technician', 'dashboard'))
        {
            $results = [];
            $all = Model_Prescription::getScopedQuery($currentUser)->
                where('pre_draft', '=', false)->
                where('is_draft', '=', false)->
                where('show', '=', true)->
                where('completed', '=', false)->
                where('canceled', '=', false)->
                where('archived', '=', false)->
                where('transfer_rx', '=', false)->
                where('approved_for_counsel', '=', false)->
                order_by('is_draft', 'DESC')->
                removeVirtualFields(['transfer'])->
                find_all()->
                untaint();

            $startDate = new DateTime();
            $yesterday = $startDate->add(DateInterval::createFromDateString('yesterday'));
            $query = Model_Prescription::getScopedQuery($currentUser)->
                where('show', '=', true)->
                where('pre_draft', '=', false)->
                where('is_draft', '=', false)->
                where('completed', '=', false)->
                where('rejected', '=', false)->
                where('canceled', '=', false)->
                where('archived', '=', false)->
                where('transfer_rx', '=', false)->
                where('approved_for_counsel', '=', true)->
                where('prescription.created_dt', '>', $yesterday->format('Y-m-d'));
            $approved_for_counsel = $query->removeVirtualFields(['transfer'])->
                find_all()->untaint();
            $all = array_merge($results, $approved_for_counsel);
            $twodaysago = $yesterday->add((DateInterval::createFromDateString('yesterday')));
            $query = Model_Prescription::getScopedQuery($currentUser)->
                where('show', '=', true)->
                where('pre_draft', '=', false)->
                where('is_draft', '=', true)->
                where('completed', '=', false)->
                where('rejected', '=', false)->
                where('canceled', '=', false)->
                where('archived', '=', false)->
                where('transfer_rx', '=', false)->
                where('approved_for_counsel', '=', false)->
                where('prescription.created_dt', '>', $twodaysago->format('Y-m-d'));
            $drafts = $query->removeVirtualFields(['transfer'])->
                find_all()->untaint();

            $all = array_merge($all, $drafts);

            $query = Model_Prescription::getScopedQuery($currentUser)->
                where('show', '=', true)->
                where('pre_draft', '=', false)->
                where('is_draft', '=', false)->
                where('completed', '=', false)->
                where('rejected', '=', false)->
                where('canceled', '=', false)->
                where('archived', '=', false)->
                where('transfer_rx', '=', false)->
                where('approved_for_counsel', '=', false);
            $awaitingapproval = $query->removeVirtualFields(['transfer'])->
                find_all()->untaint();
            $all = array_merge($all, $awaitingapproval);
            // technician
           return $all;
        }
        else
        {
            return [];
        }
    }

    public function update_edit(){
        $this->canceled = false;
        $this->pre_draft = false;
        $this->is_draft = true;
        $this->completed = false;
        $this->approved_for_counsel = false;
        $this->approved_on_hold = false;
        $this->rejected = false;
        $this->show = true;
        $now = new DateTime();
        $this->updated_dt = $now->format('"Y-m-d H:i:s');
        $this->save();
    }
}
