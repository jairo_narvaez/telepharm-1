<?php

/**
 * Model for prescription_comment table
 */
class Model_PrescriptionComment extends ORM
{
	protected $_table_name = 'prescription_comment';

	protected $_belongs_to = [
		'prescription' => ['model' => 'Prescription'],
		];

	protected $_has_many = [
		'prescription_comment_reads' => ['model' => 'PrescriptionCommentRead', 'foreign_key'=>'prescription_comment_id'],
	];

	/**
	 * Verify if an account has read a prescription comment
	 *
	 * @param  int  $account_id    account id
	 * @return boolean
	 */
	public function isReadByAccount($account_id)
	{
		$prescription_comment_read = $this->prescription_comment_reads
			->where('account_id', '=',$account_id)
			->find();
		return $prescription_comment_read->loaded();
	}

    /**
     * Create new comments for the prescription on video call
     *
     * @param CurrentUser $currentUser
     * @param integer     $prescriptionId
     * @param string      $comment
     *
     * @return Model_PrescriptionComment
     */
    public static function createMultiComments(CurrentUser $currentUser, $call_id, $comment)
    {
        $call_queue = new Model_CallQueue($call_id);
        if (!$call_queue->loaded())
            throw new Exception('Call not found');
        $patient_api_session = ORM::factory('PatientAPISession')
            ->where('call_queue_id', '=', $call_queue->id)
            ->order_by('id', 'desc')->find();
        if (!$patient_api_session->loaded()){
            throw new Exception('Session not found');
        }
        $patient_api_session_prescriptions = ORM::factory('PatientAPISessionPrescription')
            ->where('patient_api_session_id', '=', $patient_api_session->id)
            ->find_all()->untaint();
        $results = [];
        foreach ($patient_api_session_prescriptions as $next_prescription){
            $rx = Model_Prescription::getScopedQuery($currentUser)->where('prescription.id', '=', $next_prescription['prescription_id'])->find();
            if ($rx->loaded()){
                $newComment = Model::factory('PrescriptionComment');
                $newComment->account_id = $currentUser->account->id;
                $newComment->created_by = $currentUser->account->full_name;
                $newComment->prescription_id = $rx->id;
                $newComment->comment = $comment;
                $newComment->save();

                if ($newComment->saved())
                {
                    Model_PrescriptionLog::createLog($currentUser, $rx, 'create-comment');
                    self::setAsRead($currentUser, $newComment->id);
                    // Display the comment to the other side of the app
                    $permissions = Helper_Permission::instance();
                    if ($permissions->isAllowed('pharmacist', 'dashboard'))
                    {
                        $role = 'Technician';
                    }
                    else
                    {
                        $role = 'Pharmacist';
                    }

                    $results[] = $newComment->as_array();
                }
            }else{
                throw new Exception('User does not have access to this prescription');
            }
        }

        return $results;
    }

	/**
	 * Create a new comment for the prescription
	 *
	 * @param CurrentUser $currentUser
	 * @param integer     $prescriptionId
	 * @param string      $comment
	 *
	 * @return Model_PrescriptionComment
	 */
	public static function createComment(CurrentUser $currentUser, $prescriptionId, $comment)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (!$prescription->loaded())
			throw new Exception('Prescription not found');

		$newComment = Model::factory('PrescriptionComment');
		$newComment->account_id = $currentUser->account->id;
		$newComment->created_by = $currentUser->account->full_name;
		$newComment->prescription_id = $prescription->id;
		$newComment->comment = $comment;
		$newComment->save();

		if ($newComment->saved())
		{
			Model_PrescriptionLog::createLog($currentUser, $prescription, 'create-comment');
			self::setAsRead($currentUser, $newComment->id);
			// Display the comment to the other side of the app
			$permissions = Helper_Permission::instance();
			if ($permissions->isAllowed('pharmacist', 'dashboard'))
			{
				$role = 'Technician';
			}
			else
			{
				$role = 'Pharmacist';
			}
			
		}


		return $newComment->as_array();
	}
	/**
	 * Search entries that has been posted after a prescription comment id
	 *
	 * @param  CurrentUser    $currentUser
	 * @param  int            $prescriptionId
	 * @param  int            $lastCommentId
	 * @return array
	 */
	public static function getNewComments(CurrentUser $currentUser, $prescriptionId,
		$lastCommentId)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (!$prescription->loaded())
			throw new Exception('Prescription not found');

		$comments = [];
		foreach ($prescription->prescription_comments
			->where('prescriptioncomment.id', '>', $lastCommentId)
			->order_by('created_dt', 'ASC')
			->find_all() as $comment)
		{

			$comments[] = array_merge($comment->as_array(),
				array('is_read' => $comment->isReadByAccount($currentUser->account->id)));
		}
		return $comments;
	}
	/**
	 * Add a record into the prescription comment read table, set the prescription
	 * comment status as ready for an account
	 *
	 * @param CurrentUser $currentUser
	 * @param int      $prescriptionCommentId
	 * @return boolean
	 */
	public static function setAsRead(CurrentUser $currentUser, $prescriptionCommentId)
	{

		$comment = new Model_PrescriptionComment($prescriptionCommentId);
		if (!$comment->loaded())
			throw new Exception('Prescription Comment not found');

		//avoid duplicate records
		if (!$comment->isReadByAccount($currentUser->account->id))
		{
			$commentRead = new Model_PrescriptionCommentRead();
			$commentRead->account_id = $currentUser->account->id;
			$commentRead->prescription_comment_id = $comment->id;
			$commentRead->save();
			return $commentRead->saved();
		}
		return TRUE;
	}
	/**
	 * Comments with a isRead flag based on an account
	 *
	 * @param  CurrentUser       $currentUser
	 * @param  int               $prescriptionId
	 * @return array
	 */
	public static function commentsWithReadFlagByAccount(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (!$prescription->loaded())
			throw new Exception('Prescription not found');

		$comments = [];
		foreach ($prescription->prescription_comments
			->order_by('prescriptioncomment.id', 'DESC')
			->find_all() as $comment)
		{
			$comments[] = array_merge($comment->as_array(),
				array('is_read' => $comment->isReadByAccount($currentUser->account->id)));
		}
		return $comments;
	}
}
