<?php

/**
 * Model for table prescription_comment_read
 *
 * This table holds the info of the comments read by
 * other users, so we can know what comments are unread
 *
 */
class Model_PrescriptionCommentRead extends ORM
{
	protected $_table_name = 'prescription_comment_read';

	protected $_belongs_to = [
		'prescription_comment' => ['model' => 'PrescriptionComment'],
	];
}
