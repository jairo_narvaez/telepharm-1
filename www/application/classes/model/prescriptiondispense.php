<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_PrescriptionDispense extends ORM
{
	protected $_table_name = 'prescription_dispense';
	protected $_belongs_to = [
		'prescription' => [ 'model' => 'Prescription' ],
	];

	/**
	 * Creates a new dispense record for a prescription
	 *
	 * @param integer $prescriptionID the id of the new prescription
	 * @param Telepharm_PMS_Result_Dispense $dispense the dispense info to be saved
	 *
	 * @return Model_PrescriptionDispense
	 */
	public static function createWithPrescription($prescriptionID, Telepharm_PMS_Result_Dispense $dispense)

	{
		$pDispense = self::factory('PrescriptionDispense');
		$pDispense->prescription_id = $prescriptionID;
		$pDispense->qty_written = $dispense->qtyWritten;
		$pDispense->qty_dispensed = $dispense->qtyDispensed;
		$pDispense->sig = $dispense->sig;
		$pDispense->sig_coded = $dispense->sigCoded;
		$pDispense->sig_expanded = $dispense->sigExpanded;
		$pDispense->days_supply = $dispense->daysSupply;
		$pDispense->fills_owed = $dispense->fillsOwed;
		$pDispense->refill_auth = $dispense->refillAuth;
		$pDispense->num_refills = $dispense->numRefills;

		return $pDispense->save();
	}

	/**
	 * Updates a dispense record for a prescription
	 *
	 * @param Telepharm_PMS_Result_Dispense $dispense the dispense info to be saved
	 *
	 * @return Model_PrescriptionDispense
	 */
	public function updateDispense(Telepharm_PMS_Result_Dispense $dispense)

	{
		$this->qty_written = $dispense->qtyWritten;
		$this->qty_dispensed = $dispense->qtyDispensed;
		$this->sig = $dispense->sig;
		$this->sig_coded = $dispense->sigCoded;
		$this->sig_expanded = $dispense->sigExpanded;
		$this->days_supply = $dispense->daysSupply;
		$this->fills_owed = $dispense->fillsOwed;
		$this->refill_auth = $dispense->refillAuth;
		$this->num_refills = $dispense->numRefills;

		return $this->save();
	}
}

