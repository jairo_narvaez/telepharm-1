<?php defined('SYSPATH') or die('No direct access allowed.');
class Model_PrescriptionDrug extends ORM
{
	protected $_table_name = 'prescription_drug';

	protected $_belongs_to = [
		'prescription' => [ 'model' => 'Prescription' ],
	];

	/**
	 * Creates a new drug record for a prescription
	 *
	 * @param integer $prescriptionID the id of the new prescription
	 * @param Telepharm_PMS_Result_Drug $drug the drug info to be saved
	 *
	 * @return Model_PrescriptionDrug
	 */
	public static function createWithPrescription($prescriptionID, Telepharm_PMS_Result_Drug $drug)

	{
		$pDrug = self::factory('PrescriptionDrug');
		$pDrug->prescription_id = $prescriptionID;
		$pDrug->new_ndc = $drug->newNdc;
		$pDrug->old_ndc = NULL;
		$pDrug->manf = $drug->manf;
		$pDrug->description = $drug->description;
		$pDrug->package_size = $drug->packageSize;
		$pDrug->strength = $drug->strength;
		# $pDrug->unit_code = $drug->packageSizeUnitCode;

		return $pDrug->save();
	}

	/**
	 * Updates a drug record for a prescription
	 *
	 * @param Telepharm_PMS_Result_Drug $drug the drug info to be saved
	 *
	 * @return Model_PrescriptionDrug
	 */
	public function updateDrug(Telepharm_PMS_Result_Drug $drug)

	{
		$this->new_ndc = $drug->newNdc;
		$this->old_ndc = NULL;
		$this->manf = $drug->manf;
		$this->description = $drug->description;
		$this->package_size = $drug->packageSize;
		$this->strength = $drug->strength;
		# $this->unit_code = $drug->packageSizeUnitCode;

		return $this->save();
	}

}

