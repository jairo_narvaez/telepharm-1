<?php
class Model_PrescriptionEditType {
    const Partial = 1;
    const Rejected = 2;
    const Technician_Error = 3;
    const Other = 4;
}