<?php
class Model_PrescriptionImage extends ORM
{
	protected $_table_name = 'prescription_image';

	protected $_belongs_to = [
		'prescription' => [ 'model' => 'Prescription' ],
		'image' => [ 'model' => 'Image' ],
	];

    protected $_image;
    protected $_prescription;
    protected $_type;
    protected $_display_order;

	/**
	 * Store an image and link it to the prescription
	 * @param  string $image_data       encode image
	 * @param  int $id                  prescription image id
	 * @param  int $prescription_id
	 * @param  int $display_order
	 * @param  string $picture_type     prescription image type
	 * @return Model_PrescriptionImage  prescription image model
	 */
	public static function storePrescriptionImage(CurrentUser $currentUser,
		$image_data, $id, $prescription_id, $display_order, $picture_type)
	{
		$params = ['resizedCrop' =>[
				[
					'width' => '224',
					'height' => '168',
				]
			]
		];

		$image = NULL;

		if (isset($_FILES['picture']) && !$_FILES['picture']['error'] )
		{
			try
			{
				$image = Model_Image::processUpload('picture', 
					'prescription',
					$params);
			}
			catch(Kohana_Exception $e)
			{
				return false;
			}
		}
		else if (!empty($image_data))
		{
			$name = $prescription_id . '_' . $picture_type;
			$image = Model_Image::processBase64Upload($image_data, 
				$name,
				'prescription',
				$params);
		}

		$prescription_image = new Model_PrescriptionImage($id);
		$prescription_image->prescription_id = $prescription_id;
		$prescription_image->image_id = $image->id;
		$prescription_image->type = $picture_type;
		$prescription_image->display_order = $display_order;
		$prescription_image->save();

		if ($prescription_image->saved())
			Model_PrescriptionLog::createLog($currentUser, $prescription_image->prescription, 'store-image');

		return $prescription_image;
	}

    public static function create_prescription_image(Model_Prescription $prescription, Model_Image $image, $type, $display_order){
        $self = new static();
        $self->_prescription = $prescription;
        $self->_image = $image;
        $self->_type = $type;
        $self->_display_order = $display_order;
        return $self;
    }

    //one day this will override the existing save function.  one day.
    public function save_prescription_image(Validation $validation = null){
        $self = $this;
        if($validation){
            $self = parent::save($validation);
        }else{
            if (isset($self->_prescription) &&
            isset($self->_image)){
                if(!$self->_prescription->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Prescription not loaded', 2);
                }else{
                    $this->prescription_id = $self->_prescription->id;
                }
                if (!$self->_image->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Image not loaded', 2);
                }else{
                    $this->image_id = $self->_image->id;
                }
            }
            if (!isset($self->_type)){
                $self->error_messages[] = new Telepharm_ErrorMessage('Type not set', 2);
            }else{
                $this->type = $self->_type;
            }
            if (!isset($self->_display_order)){
                $self->error_messages[] = new Telepharm_ErrorMessage('Display order not set', 2);
            }else{
                $this->display_order = $self->_display_order;
            }
            if (count($self->error_messages)==0){
               if ($self->_created){
                   $this->created_dt = $self->_created->format('Y-m-d H:i:s');
               }
               $self = parent::save();
            }
        }
        return $self;
    }

    public static function deletePrescriptionImage(CurrentUser $currentUser, $id){
        if (!$currentUser->account->loaded()){
            return false;
        }
        $prescription_image = new Model_PrescriptionImage($id);
        if (!$prescription_image->loaded()){
            return false;
        }
        $prescription = new Model_Prescription($prescription_image->prescription_id);
        if (!$prescription->loaded()){
            return false;
        }
        //the only person who should be able to delete an image is the person who created the prescription.
        if ($currentUser->account->id != $prescription->account_id){
            return false;
        }
        $image = new Model_Image($prescription_image->image_id);
        if (!$image->loaded()){
            return false;
        }
        if (!$prescription->is_draft){
            return false;
        }
        //if we got to this point, the request to delete the image is valid.
        $prescription_image->delete();
        $image->delete();
        return true;
    }

    public static function rotate_image(CurrentUser $currentUser, $id, $direction){
        $errors = array();
        $permissions = Helper_Permission::instance();
        if ($permissions->isAllowed('pharmacist', 'dashboard')){
            if (!$currentUser->account->loaded()){
                $errors[] = new Telepharm_ErrorMessage('User is not authenticated', 1);
            }
            $prescription_image = new Model_PrescriptionImage($id);
            if (!$prescription_image->loaded()){
                $errors[] = new Telepharm_ErrorMessage('Reference to image for prescription does not exist', 1);
            }
            $prescription = new Model_Prescription($prescription_image->prescription_id);
            if (!$prescription->loaded()){
                $errors[] = new Telepharm_ErrorMessage('Prescription does not exist', 1);
            }
            $image = new Model_Image($prescription_image->image_id);
            if (!$image->loaded()){
                $errors[] = new Telepharm_ErrorMessage('Image does not exist', 1);
            }
            if ($prescription->pre_draft || $prescription->is_draft){
                $errors[] = new Telepharm_ErrorMessage('Image cannot be rotated for this prescription', 1);
            }
        }else{
            $errors[] = new Telepharm_ErrorMessage('User does not have permission to rotate images', 1);
        }
        //if we got to this point, the request to rotate the image is valid.
        if (count($errors) == 0){
            $image->rotate($direction);
            return ['success' => true, 'errors'=>$errors];
        }else{
            return ['errors'=>$errors];
        }
    }
}

