<?php

/**
 * PrescriptionLabel Model
 */
class Model_PrescriptionLabel extends ORM
{
	protected $_table_name = 'prescription_label';

	protected $_belongs_to = [
		'account' => [ 'model' => 'Account' ],
		'prescription' => [ 'model' => 'Prescription'],
	];

	/*
	 * Add labels to a prescription from Medispan
	 *
	 * @param $prescriptionID
	 * @param $drug
	 *
	 */
	public static function createFromMedispan(CurrentUser $currentUser,
		$prescriptionID,
		Telepharm_PMS_Result_Drug $drug)
	{
		$label = new Medispan_Query_Label;
		$labels = $label->get($drug->newNdc);

		foreach ($labels as $l)
		{
			$newLabel = new self;
			$newLabel->prescription_id = $prescriptionID;
			$newLabel->account_id = $currentUser->account->id;
			$newLabel->label = $l['label'];
			$newLabel->color = 'white';
			$newLabel->display_order = $l['priority'];
			$newLabel->created_by = $currentUser->account->full_name;

			$newLabel->save();
		}

		return 1;
	}

	/*
	 * Add a label to a prescription
	 *
	 * @param CurrentUser $currentUser    to check if can add labels to the prescription
	 * @param integer     $prescriptionId to find the prescription
	 * @param string      $label          text of the label
	 * @param string      $color          the hex color of the label
	 *
	 * @return Model_PrescriptionLabel
	 */
	public static function addToPrescription(CurrentUser $currentUser,
		$prescriptionId,
		$label,
		$color)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (! $prescription->loaded())
			throw new HTTP_Exception_404;


		$newLabel = new self;
		$newLabel->prescription_id = $prescription->id;
		$newLabel->account_id = $currentUser->account->id;
		$newLabel->label = $label;
		$newLabel->color = $color;
		$newLabel->display_order = 0; # For future reordering
		$newLabel->created_by = $currentUser->account->full_name;

		$newLabel->save();

		return $newLabel;
	}

	/**
	 * Remove the label associated to that prescription
	 * 
	 * @param CurrentUser $currentUser    
	 * @param integer     $prescriptionId 
	 * @param integer     $labelId        
	 * 
	 * @return boolean
	 */
	public static function removeFromPrescription(CurrentUser $currentUser, 
		$prescriptionId, 
		$labelId)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (! $prescription->loaded())
			throw new HTTP_Exception_404;


		$label = $prescription->prescription_labels->where('id','=',$labelId)->find();

		if (! $label->loaded())
			throw new HTTP_Exception_404;
			
		$label->delete();

		return !$label->loaded();
	}
}
