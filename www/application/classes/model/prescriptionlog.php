<?php

/**
 * Model for table prescription log
 * 
 * This model should create records
 * for any change on prescription record
 * for auditing
 */
class Model_PrescriptionLog extends ORM
{
	protected $_table_name = 'prescription_log';

	protected $_belongs_to = [
		'prescription' => ['model' => 'Prescription'],
		'account' => ['model' => 'Account'],
		];

	protected $_virtual_fields = [ 'prescription' ];


	/**
	 * Creates a record for audit
	 * 
	 * @param CurrentUser        $currentUser  
	 * @param Model_Prescription $prescription 
	 * @param string             $action       
	 * 
	 * @return Model_PrescriptionLog
	 */
	public static function createLog(CurrentUser $currentUser, Model_Prescription $prescription, $action, $message = null)
	{
		$newLog = Model::factory('PrescriptionLog');
		$newLog->account_id = $currentUser->account->id;
		$newLog->created_by = $currentUser->account->full_name;
		$newLog->prescription_id = $prescription->id;
		$newLog->action = $action;
        $newLog->message = $message;
		$newLog->save();
		return $newLog->as_array();
	}

	/**
	 * Return a list of users that have interacted with the prescription
	 * except the current user
	 * 
	 * @param integer     $prescriptionId 
	 * @param CurrentUser $currentUser    
	 * 
	 * @return array
	 */
	public static function interestedAccounts($prescriptionId, CurrentUser $currentUser)
	{
		$query = DB::query(Database::SELECT, 'SELECT distinct `account_id` FROM `prescription_log` 
			WHERE `prescription_id` = :prescription_id
			and `account_id` <> :user ');
	 
		$query->parameters(array(
			':user' => $currentUser->account->id,
			':prescription_id' => $prescriptionId,
		));	

		$accounts = [];
		foreach($query->execute() as $row)
		{
			$accounts[] = $row['account_id'];
		}

		return $accounts;
	}


	/**
	 * Get a list of activity log sorted
	 * chronologically for a prescription
	 * 
	 * @param CurrentUser $currentUser the user making the call
	 * @param integer $prescriptionId 
	 * 
	 * @return Array
	 */
	public static function getLogForPrescription(CurrentUser $currentUser, $prescriptionId)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (!$prescription->loaded())
			throw new Exception('Prescription not found');

		return self::factory('PrescriptionLog')->
			where('prescription_id', '=', $prescription->id)->
			order_by('created_dt', 'ASC')->
			find_all()->untaint();
	}


	/**
	 * Search entries that has been posted after a prescription log id
	 *
	 * @param CurrentUser $currentUser the user making the call
	 * @param  int            $prescriptionId
	 * @param  int            $lastLogId
	 * @return array
	 */
	public static function getNewLogs(CurrentUser $currentUser,
		$prescriptionId, $lastLogId)
	{
		$prescription = Model_Prescription::getScopedQuery($currentUser)->
			where('prescription.id', '=', $prescriptionId)->
			find();

		if (!$prescription->loaded())
			throw new Exception('Prescription not found');

		$logs = [];
		foreach ($prescription->prescription_logs
			->where('prescriptionlog.id', '>', $lastLogId)
			->order_by('created_dt', 'ASC')
			->find_all() as $log)
		{
            $result = $log->as_array();
            $result['prescription'] = $prescription->as_array();
			$logs[] = $result;
		}
		return $logs;
	}
}
