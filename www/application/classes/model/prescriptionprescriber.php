<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_PrescriptionPrescriber extends ORM
{
	protected $_table_name = 'prescription_prescriber';
	protected $_belongs_to = [
		'prescription' => [ 'model' => 'Prescription' ],
		'prescriber' => [ 'model' => 'Prescriber' ],
	];

	/**
	 * Creates a new prescriber record for a prescription
	 *
	 * @param integer $prescriptionID the id of the new prescription
	 * @param Telepharm_PMS_Result_Prescriber $prescriber the prescriber info to be saved
	 *
	 * @return Model_PrescriptionPrescriber
	 */
	public static function createWithPrescriptionAndPrescriber($prescriptionID, Telepharm_PMS_Result_Prescriber $prescriber)
	{
		$presc = Model_Prescriber::findOrCreate($prescriber);

		$pPrescriber = self::factory('PrescriptionPrescriber');
		$pPrescriber->prescription_id = $prescriptionID;
		$pPrescriber->prescriber_id = $presc->id;
		$pPrescriber->first_name = $presc->first_name;
		$pPrescriber->last_name = $presc->last_name;
		$pPrescriber->phone = $presc->phone;
		$pPrescriber->npi = $presc->npi;
		$pPrescriber->identifier = $presc->identifier;

		return $pPrescriber->save();
	}

	/**
	 * Updates a prescriber record for a prescription
	 *
	 * @param Telepharm_PMS_Result_Prescriber $prescriber the prescriber info to be saved
	 *
	 * @return Model_PrescriptionPrescriber
	 */
	public function updatePrescriber(Telepharm_PMS_Result_Prescriber $prescriber)
	{
		$presc = Model_Prescriber::findOrCreate($prescriber);

		$this->prescriber_id = $presc->id;
		$this->first_name = $presc->first_name;
		$this->last_name = $presc->last_name;
		$this->phone = $presc->phone;
		$this->npi = $presc->npi;
		$this->identifier = $presc->identifier;

		return $this->save();
	}
}
