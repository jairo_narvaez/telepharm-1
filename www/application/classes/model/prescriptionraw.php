<?php

class Model_PrescriptionRaw extends ORM
{
	protected $_table_name = 'prescription_raw';

	protected $_belongs_to = [
		'prescription' => ['Model' => 'Prescription']
	];
}
