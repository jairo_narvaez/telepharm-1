<?php

/**
 * PrescriptionStore Model
 */
class Model_PrescriptionStore extends ORM
{
	protected $_table_name = 'prescription_store';

	protected $_belongs_to = [
		'prescription' => [ 'model' => 'Prescription' ],
		'store'        => [ 'model' => 'Store' ],
	];

	/**
	 * Creates a new prescription_store record for a prescription
	 *
	 * @param integer $prescriptionID the id of the new prescription
	 * @param Model_Store $store the store info to be saved
	 *
	 * @return Model_PrescriptionStore
	 */
	public static function createWithPrescriptionAndStore($prescriptionID, Model_Store $store)
	{
		$pStore = self::factory('PrescriptionStore');
		$pStore->prescription_id = $prescriptionID;
		$pStore->store_id = $store->id;
		$pStore->company_name = $store->company->name;
		$pStore->store_name = $store->name;
		$pStore->store_description = $store->description;
		$pStore->store_contact = $store->contact;
		$pStore->store_phone = $store->phone;

		return $pStore->save();
	}
}

