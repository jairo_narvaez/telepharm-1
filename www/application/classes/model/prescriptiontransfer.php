<?php
class Model_PrescriptionTransfer extends ORM
{
    protected $_table_name = 'prescription_transfer';

    protected $_virtual_fields = [
        'store',
        'pharmacist',
        'called_in_by'
    ];

    protected $_prescription;
    protected $_pharmacist;
    protected $_called_in_person;
    protected $_store;
    protected $_initiated_by_account;
    protected $_completed_by_account;

    public function __get($key)
    {
        switch($key)
        {
            case 'pharmacist':
                $pharmacist = new Model_ExternalPharmacist($this->external_pharmacist_id);
                return $pharmacist->unset_unused_properties();
            case 'store' :
                $store = new Model_ExternalStore($this->external_store_id);
                return $store->unset_unused_properties();
            case 'called_in_by':
                $person = new Model_Person($this->called_in_by_person_id);
                return $person->as_array();
            case 'initiated_by':
                $account = new Model_Account($this->initiated_by_account_id);
                return $account->full_name;
            case 'completed_by':
                $account = new Model_Account($this->completed_by_account_id);
                return $account->full_name;
            default:
                return  parent::__get($key);
        }
    }

    public static function create_prescription_transfer(Model_Prescription $prescription,
                                                        Model_ExternalPharmacist $pharmacist,
                                                        Model_Person $called_in_person,
                                                        Model_ExternalStore $external_store,
                                                        CurrentUser $currentUser){
        $self = new static();
        $self->_is_new = true;
        $self->_prescription = $prescription;
        $self->_pharmacist = $pharmacist;
        $self->_called_in_person = $called_in_person;
        $self->_store = $external_store;
        $self->_initiated_by_account = $currentUser->account;
        return $self;
    }

    public function set_completed_by(CurrentUser $currentUser){
        $this->_completed_by_account = $currentUser->account;
    }

    public function save(Validation $validation = null){
        $self = $this;
        //check if there was any custom validation passed for this save
        if ($validation){
            //custom validation was supplied, use it.
            $self = parent::save($validation);
        }else{
            if ($self->_is_new){
                //validate prescription
                if (!$self->_prescription->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Prescription is invalid', 2);
                }

                //validate external pharmacist
                if (!$self->_pharmacist->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Pharmacist is invalid', 2);
                }

                //validate called in person
                if (!$self->_called_in_person->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Called in person is invalid', 2);
                }

                //validate external store
                if (!$self->_store->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('External store is invalid', 2);
                }

                if (!$self->_initiated_by_account->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Initiated by account is invalid', 2);
                }
                //make sure there were no errors
                if (count($self->error_messages) == 0){
                    $prescription = $self->_prescription->as_array();
                    $store = $self->_store->as_array();
                    $call_in_person = $self->_called_in_person->as_array();
                    $pharmacist = $self->_pharmacist->as_array();
                    //no errors? set ref columns up for insert
                    $this->prescription_id = $prescription['id'];
                    $this->external_store_id = $store['id'];
                    $this->external_pharmacist_id = $pharmacist['id'];
                    $this->called_in_by_person_id = $call_in_person['id'];
                    $this->initiated_by_account_id = $self->_initiated_by_account->id;
                    //set columns for meta data
                    $this->created_dt = $self->_created->format('Y-m-d H:i:s');
                    //save to database.
                    $self = parent::save();
                }
            }else{
                //update the already existing record
                if (!isset($self->id) || $self->id == 0){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Transfer record is invalid', 2);
                }
                if (!$self->_completed_by_account->loaded()){
                    $self->error_messages[] = new Telepharm_ErrorMessage('Completed by account is invalid', 2);
                }
                //make sure there were no errors
                if (count($self->error_messages) == 0){
                    $this->updated_dt = $self->_updated->format('Y-m-d H:i:s');
                    //no errors? set ref columns up for insert
                    $this->completed_by_account_id = $self->_completed_by_account->id;
                    //save to database.
                    $self = parent::save();
                }
            }
        }
        return $self;
    }

    //I am not sure I like this function here.  It seems like it should be somewhere else,
    //and that the models should only handle creating, updating, and deleting
    //I must contemplate this.
    public static function initiate_transfer(CurrentUser $currentUser, $transfer_data)
    {
        $errors = array();

        //do some simple data validation
        if (!isset($transfer_data['prescription'])){
            $errors[] = new Telepharm_ErrorMessage('Must provide prescription data.', 1);
        }
        if (!isset($transfer_data['pharmacist'])){
            $errors[] = new Telepharm_ErrorMessage('Must provide pharmacist data.', 1);
        }
        if (!isset($transfer_data['called_in_person'])){
            $errors[] = new Telepharm_ErrorMessage('Must provide call in by person data.', 1);
        }

        if (count($errors) > 0){
            //if there are any errors at this point we want to short circuit because the data submitted was junk
            return ['errors'=>$errors];
        }
        //get date time to associate with all inserted data.
        $now = new DateTime();
        //get prescription data from data submitted
        $prescription_data = $transfer_data['prescription'];
        //get prescription
        $prescription = Model_Prescription::getScopedQuery($currentUser)->
            where('prescription.id', '=', $prescription_data['id'])->
            find();

        if(!$prescription->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Prescription not found', 1);
            //if there are any errors at this point we want to short circuit because the data submitted was junk
            return ['errors'=>$errors];
        }
        //get instance of the database for transactioning purposes
        $db = Database::instance();
        //begin transaction for external store / external pharmacy / call in person data
        $db->begin();
        //get pharmacist data from data submitted
        $pharmacist_data = $transfer_data['pharmacist'];
        //create empty ExternalPharmacist Model.
        $pharmacist = new Model_ExternalPharmacist();
        //create empty ExternalStore model;
        $external_store = new Model_ExternalStore();
        //check if a pre existing pharmacist was selected to use for this transfer
        //get current store of current user
        //for right now, there is only one pharmacy associated with each tech
        //we can get away with using the first value in the store list associated with the current user
        //whenever we fully implement multiple stores for each user, we will have to revisit this
        //probably have to add a "currentStore" property/function to $currentUser
        $stores = $currentUser->getStores();
        $current_internal_store = array_shift($stores);
        if (isset($pharmacist_data['id']) && $pharmacist_data['id'] > 0){
            $pre_existing_pharmacist_id = $pharmacist_data['id'];
            $pharmacist = Model_ExternalPharmacist::fetch_pharmacist_by_store_id($current_internal_store->id, $pre_existing_pharmacist_id);
            if ($pharmacist->loaded()){
                $pharmacist->set_internal_store_id($current_internal_store->id);
                if ($pharmacist->internalstorerelationship->loaded()){
                    $pharmacist->internalstorerelationship->set_updated($now);
                    $pharmacist->internalstorerelationship->save();
                    if ($pharmacist->store_record->internalstorerelationship->loaded()){
                        $pharmacist->store_record->internalstorerelationship->set_updated($now);
                        $pharmacist->store_record->internalstorerelationship->save();
                        //a pre existing pharmacist was selected - use that record for the transfer
                        $external_store = $pharmacist->store_record;
                    }else{
                        $errors[] = new Telepharm_ErrorMessage('Pharmacist is not associated with current pharmacy or store', 1);
                    }
                }else{
                    $errors[] = new Telepharm_ErrorMessage('Pharmacist is not associated with current pharmacy', 1);
                }
            }else{
                $errors[] = new Telepharm_ErrorMessage('Pharmacist is not associated with current pharmacy', 1);
            }
        }else{
            //a pre existing pharmacist was NOT selected - create a new one.
            //first we need to create a person for this pharmacist
            $person = Model_Person::create_person($pharmacist_data['first_name'], $pharmacist_data['last_name']);
            $person->set_created($now);
            $person = $person->save();
            $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($person->error_messages, 'Pharmacist'));
            //then we need to see if the user selected an existing store.
            //get external store data from submitted data;
            $external_store_data = $pharmacist_data['store'];

            if (isset($external_store_data['id']) && $external_store_data['id'] > 0){
                $external_store = new Model_ExternalStore($external_store_data['id']);
                if ($external_store->loaded()){
                   $external_store->set_internal_store_id($current_internal_store->id);
                    if ($external_store->internalstorerelationship->loaded()){
                        $external_store->internalstorerelationship->set_updated($now);
                        $external_store->internalstorerelationship->save();
                    }else{
                        $errors[] = new Telepharm_ErrorMessage('Supplied external store is not associated with current store', 2);
                    }
                }else{
                    $errors[] = new Telepharm_ErrorMessage('Supplied external store does not exist', 2);
                }
            }else{
                //we need to check and see if the store already exists in the system, but is just not associated with the current techs store.
                $external_store = Model_ExternalStore::fetch_existing_by_name($current_internal_store, $external_store_data['name']);
                //check to see if we found a store already existing.
                if (!$external_store->loaded()){
                    //an existing store was NOT found to exist already - create a new one
                    //get external store address information from submitted data
                    $external_store_address_data = $external_store_data['address'];
                    //first we need to create an address for the store
                    $external_store_address = Model_Address::create_address($external_store_address_data['line1'],
                        $external_store_address_data['city'],
                        $external_store_address_data['state']['id'],
                        $external_store_address_data['postal_code']);
                    if (isset($external_store_address_data['line2'])){
                        $external_store_address->line2 = $external_store_address_data['line2'];
                    }
                    $external_store_address->set_created($now);
                    $external_store_address = $external_store_address->save();
                    $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($external_store_address->error_messages, 'Store Address'));
                    //then we use the address in combination with the store name the user submitted to create the store
                    $external_store = Model_ExternalStore::create_external_store($external_store_data['name'], $external_store_address);
                    $external_store->set_created($now);
                    $external_store = $external_store->save();
                    $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($external_store->error_messages, 'Store'));
                }
                //an external store was found - associate it with the current pharmacy
                $internal_store_external_store = Model_InternalStoreExternalStore::
                    create_internal_store_external_store($current_internal_store,
                        $external_store);
                $internal_store_external_store->set_created($now);
                $internal_store_external_store->save();
                //there should be no errors, I am not going to check for them because I want the app to blow up if there are any.
            }
            if (!$pharmacist->loaded()){
                //now we have the external store and the pharmacist person to create the pharmacist
                $pharmacist = Model_ExternalPharmacist::fetch_pharmacist_at_store($pharmacist_data['first_name'], $pharmacist_data['last_name'], $external_store);
                if (!$pharmacist->loaded()){
                    $pharmacist = Model_ExternalPharmacist::create_external_pharmacist($external_store, $person);
                    $pharmacist->set_created($now);
                    $pharmacist = $pharmacist->save();
                    $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($pharmacist->error_messages, 'Pharmacist'));
                }
            }
            //relate the pharmacist to the current store
            $internal_store_external_pharmacist = Model_InternalStoreExternalPharmacist::
                create_internal_store_external_pharmacist($current_internal_store,
                    $pharmacist);
            $internal_store_external_pharmacist->set_created($now);
            $internal_store_external_pharmacist->save();
        }

        if (count($errors) == 0){
            //if there were no problems with the data submitted for the external store / external pharmacist, save the data the tech submitted
            //we do this so we have this in the future in case the tech wants to use it again, regardless of whether or not they successfully transfer the RX
            $db->commit();
        }else{
            //there was some problem with the external store / external pharmacist data submitted, don't write it to the database
            $db->rollback();
        }
        //begin transaction for actual prescription transfer
        $db->begin();
        $transfer = new Model_PrescriptionTransfer();
        //next we want to try to create the call in person
        //get the data for call in person from the submitted data
        $call_in_person_data = $transfer_data['called_in_person'];
        $call_in_person = Model_Person::create_person($call_in_person_data['first_name'], $call_in_person_data['last_name']);
        $call_in_person->set_created($now);
        $call_in_person = $call_in_person->save();
        $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($call_in_person->error_messages, 'Call in person'));
        //check if there were any problems creating the call in person
         if (count($errors) == 0){
             //then try to actually transfer the prescription
             $result = $prescription->begin_transfer($currentUser);
             $errors = array_merge($errors,  Telepharm_ErrorMessage::apply_prefix($result['errors'], 'Prescription'));
             if (count($errors) == 0){
                 //if there was no problem transferring the prescription, write out the transfer data.
                 $transfer = Model_PrescriptionTransfer::create_prescription_transfer($result['prescription'], $pharmacist, $call_in_person, $external_store, $currentUser);
                 $transfer->set_created($now);
                 $transfer = $transfer->save();
                 $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($transfer->error_messages, 'Prescription Transfer'));
                 if (count($errors) == 0){
                     //there were no problems writing out the transfer data, commit it.
                     $db->commit();
                 }else{
                     //this shouldn't ever happen, at this point we should always have a valid transfer object
                     //but it is here just in case.
                     $db->rollback();
                 }
             }else{
                 //there was a problem with the prescription (it was probably not in a status where it could be transferred)
                 //roll back the transaction.
                 //no call in person will be persisted
                 $db->rollback();
             }
        }else{
             //there was a problem with the call in person data the user entered, roll back the transaction
             //no update to the prescription was performed, and no transfer data was written
            $db->rollback();
        }
        if(count($errors) > 0){
            return ['errors'=>$errors];
        }else{
            return ['transfer'=>$transfer->unset_unused_properties(), 'statuses'=> $result['prescription']->getStatuses(), 'errors'=>$errors];
        }
    }

    public static function complete_transfer(CurrentUser $currentUser, $transfer_id){
        $errors = array();
        if (!isset($transfer_id) || $transfer_id == 0){
            $errors[] = new Telepharm_ErrorMessage('Must provide a transfer id', 1);
        }else{
            $db = Database::instance();
            //begin transaction for external store / external pharmacy / call in person data
            $db->begin();
            $transfer = new Model_PrescriptionTransfer($transfer_id);
            if (!$transfer->loaded()){
                $errors[] = new Telepharm_ErrorMessage('No active transfer found for that id', 1);
            }else{
                $now = new DateTime();
                $transfer->set_completed_by($currentUser);
                $transfer->set_updated_dt($now);
                $transfer = $transfer->save();
                $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($transfer->error_messages, 'Prescription Transfer'));
                if (count($errors) == 0){
                    //get prescription
                    $prescription = Model_Prescription::getScopedQuery($currentUser)->
                        where('prescription.id', '=', $transfer->prescription_id)->
                        find();
                    //there is only one operation performed here - that is to try to update the prescription
                    $prescription = $prescription->complete_transfer($currentUser);
                    $errors = array_merge($errors, Telepharm_ErrorMessage::apply_prefix($prescription['errors'], 'Prescription'));
                    if (count($errors) == 0){
                        $db->commit();
                    }else{
                        $db->rollback();
                    }
                    return array('transfer'=>$transfer->unset_unused_properties(), 'statuses' => $prescription['statuses'], 'errors' => $errors);
                }else{
                    $db->rollback();
                }
                return array('transfer'=>$transfer->unset_unused_properties(), 'errors' => $errors);
            }
        }
        return array('errors' => $errors);
    }

    public function unset_unused_properties(){
        $array = $this->as_array();
        $array['called_in_by'] = $this->called_in_by;
        unset($array['called_in_by_person_id']);
        $array['pharmacist'] = $this->pharmacist;
        unset($this['external_pharmacist_id']);
        $array['store'] = $this->store;
        unset($array['external_store_id']);
        unset($array['prescription_id']);
        $array['initiated_by'] = $this->initiated_by;
        unset($array['initiated_by_account_id']);
        $array['completed_by'] = $this->completed_by;
        unset($array['completed_by_account_id']);
        return $array;
    }
}

