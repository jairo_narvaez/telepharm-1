<?php

/**
 * Role model to interact with the db
 */
class Model_Role extends ORM
{
	protected $_table_name = 'role';

	protected $_has_many = [
		'account_roles' => ['model' => 'AccountRole'],
		'accounts' => ['model' => 'Account', 'through' => 'account_role'],
		];

	protected $_virtual_fields = ['count_accounts'];

	/**
	 * Get parent field, or virtual/calculated fields
	 * 
	 * @param string $key 
	 * 
	 * @return mixed
	 */
	public function __get($key)
	{
		switch ($key)
		{
		case 'count_accounts':
			return parent::__get('account_roles')->count_all();
		default:
			return parent::__get($key);
		}
	}
}
