<?php
class Model_Session extends ORM
{
	protected $_table_name = 'session';

	protected $_has_many = [
		'pageViews' => ['model' => 'PageView']
	];

	public static function init()
	{
		$self = new static();
		$self->php_session_id = session_id();
		$self->ip_address = isset($_SERVER['REMOTE_ADDR']) ? 
			sprintf("%ul", ip2long($_SERVER['REMOTE_ADDR'])) : 0;
		$self->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
		$self->referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
		$self->save();

		return $self;
	}
}

