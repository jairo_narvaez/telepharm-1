<?php
class Model_State extends ORM {

    protected $_table_name = 'state';

    public static function fetch_all(){
        $states = ORM::factory('State')->
            order_by('sort_order', 'ASC')->
            find_all()->
            as_array();
        $result = array();
        foreach($states as $state){
            //$result[] = array_merge($state->as_array(), ['error_messages' => $state->error_messages]);
            $result[] = $state->as_array();
        }
        return $result;
    }
}