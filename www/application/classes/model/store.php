<?php

/**
 * Store model
 */
class Model_Store extends ORM implements mef\ACL\ResourceInterface
{
	protected $_table_name = 'store';

	protected $_belongs_to = [
		'location' => ['model' => 'Location'],
		'company'  => [ 'model' => 'Company' ],
	];

	protected $_has_many = [
		'store_roles' => ['model' => 'StoreRole'],
		'accounts' => ['model' => 'Account', 'through' => 'store_role'],
	];

    public function __get($key)
    {
        switch($key)
        {
            case 'location':
                $location = new Model_Location($this->location_id);
            default:
                return  parent::__get($key);
        }
    }
	/**
	 * Implementation of the ResourceInterface
	 * 
	 * @return string
	 */
	public function getResourceId()
	{
		return 'store';
	}
}
