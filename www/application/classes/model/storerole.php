<?php

/**
 * StoreRole model to interact with the db
 */
class Model_StoreRole extends ORM
{
	protected $_table_name = 'store_role';

	protected $_belongs_to = [
		'account' => ['model' => 'Account'],
		'store' => ['model' => 'Store'],
		'role' => ['model' => 'Role'],
	];

	protected $_virtual_fields = [
		'account', 'role'
		];

    public function __get($key)
    {
        switch($key)
        {
            case 'role':
                $role = new Model_Role($this->role_id);
                return $role;

            default:
                return  parent::__get($key);
        }
    }

    protected $_store;
    protected $_account;
    protected $_role;

    public static function create_store_role(Model_Store $store, Model_Account $account, Model_Role $role){
        $self = new static();
        $self->_store = $store;
        $self->_account = $account;
        $self->_role = $role;
        return $self;
    }

    public static function fetch_store_role(Model_Store $store, Model_Account $account, Model_Role $role){
        $self = ORM::factory('StoreRole');
        if (!$self->_store->loaded()){
            $self->error_messages[] = new Telepharm_ErrorMessage('Store is invalid', 2);
        }
        if (!$self->_account->loaded()){
            $self->error_messages[] = new Telepharm_ErrorMessage('Account is invalid', 2);
        }
    }

    public function save_store_role(DateTime $now){
        $self = $this;
        if (!$self->_store->loaded()){
            $self->error_messages[] = new Telepharm_ErrorMessage('Store is invalid', 2);
        }
        if (!$self->_account->loaded()){
            $self->error_messages[] = new Telepharm_ErrorMessage('Account is invalid', 2);
        }
        if (!$self->_role->loaded()){
            $self->error_messages[] = new Telepharm_ErrorMessage('Role is invalid', 2);
        }
        if (count($self->error_messages) == 0){
            $this->store_id = $self->_store->id;
            $this->account_id = $self->_account->id;
            $this->role_id = $self->_role->id;
            $this->created_dt = $now->format('Y-m-d H:i:s');
            $self = parent::save();
        }
        return ['success'=>count($self->error_messages)==0, 'errors'=>$self->error_messages];
    }

    public static function delete_store_role(Model_Store $store, CurrentUser $currentUser, Model_Role $role){
       $errors = array();
        if (!$store->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Store is invalid', 2);
        }
        if (!$currentUser->account->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Account is invalid', 2);
        }
        if (!$role->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Role is invalid', 2);
        }
        if (count($errors) == 0){
            $stores = $currentUser->getStores();
            if (count($stores) > 1){
                //make sure is already associated with store
                foreach ($stores as $associatedStore){
                    if ($store->id == $associatedStore->id){
                        # remove previous role
                        DB::query(Database::DELETE,
                            "DELETE FROM store_role
                    WHERE store_id = {$associatedStore->id}
                    AND account_id = {$currentUser->account->id}
                    AND role_id = {$role->id}"
                        )->execute();
                        break;
                    }
                }
            }else{
                $errors[] = new Telepharm_ErrorMessage('You cannot unassigned yourself from all stores', 1);
            }
        }
        return ['success'=> count($errors) == 0, 'errors'=> $errors];
    }

    public static function update_store_role(Model_Store $store, CurrentUser $currentUser, Model_Role $role){
        $errors = array();
        if (!$store->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Store is invalid', 2);
        }
        if (!$currentUser->account->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Account is invalid', 2);
        }
        if (!$role->loaded()){
            $errors[] = new Telepharm_ErrorMessage('Role is invalid', 2);
        }
        if (count($errors) == 0){
            $stores = $currentUser->getStores();
            $db = Database::instance();
            //begin transaction for role update
            $db->begin();
            # remove previous roles
            DB::query(Database::DELETE,
                "DELETE FROM store_role
        WHERE account_id = {$currentUser->account->id}
        AND role_id = {$role->id}"
            )->execute();
            $new_store_role = self::create_store_role($store, $currentUser->account, $role);
            $new_store_role = $new_store_role->save_store_role(new DateTime());
            $db->commit();
            $errors = array_merge($errors, $new_store_role['errors']);
        }
        return ['success'=> count($errors) == 0, 'errors'=> $errors];
    }
}



