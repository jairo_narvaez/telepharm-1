<?php
class Model_TechnicianCompleteType {
    const No_Pick_Up = 1;
    const Mail_Order = 2;
    const Delivery = 3;
    const Other = 4;
}