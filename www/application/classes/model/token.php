<?php

class Model_Token extends ORM
{
    protected $_length;
    protected $_table_name = 'token';
    protected $_issued;
    protected $_created;
    protected $_ipaddress;
    protected $_slide_minutes;
    //the token table supports up to 128-bit (length) tokens.
    //for reference, a 128-bit token is 256 characters long.
    //should we need bigger tokens, it shouldn't be a problem to increase the column size
    public function __construct($token_id = null){
        parent::__construct($token_id);
    }
    public static function make($length, $slide_minutes, $created, $ipaddress)
    {
        $obj = new static();
        $obj->_slide_minutes = $slide_minutes;
        $obj->_length = $length;
        $obj->_created = $created;
        $obj->_ipaddress = $ipaddress;
        return $obj;
    }

    public function set_issued($issued){
        $this->_issued = $issued;
    }

    public function save(Validation $validation = NULL)
    {
        $expiration = new DateTime($this->_issued->format('Y-m-d H:i:s'));
        $expiration = $expiration->modify('+ ' . $this->_slide_minutes . ' minutes');
        $token = Security_Token::generate($this->_length);

        $this->values(['value' => $token,
            'created' => $this->_created->format('Y-m-d H:i:s'),
            'issued' => $this->_issued->format('Y-m-d H:i:s'),
            'expires' => $expiration->format('Y-m-d H:i:s'),
            'ipaddress'=>$this->_ipaddress,
            'slide_minutes' => $this->_slide_minutes]);
        parent::save($validation);
        return $this;
    }
}