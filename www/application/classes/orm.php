<?php

class ORM extends TelePharm_ORM {

	/**
	 * Add a virtual field to get the info on as_array
	 * this should be done before calling as_array
	 * 
	 * @return void
	 */
	public function addVirtualField($newVirtualField)
	{
		$this->_virtual_fields[] = $newVirtualField;
        return $this;
	}

    //A property for storing errors for new instance methods
    public $error_messages;

    //a property for passing the created date down the object chain so that all records have the same created_dt
    protected $_created;
    protected $_updated;
    protected $_is_new;

    public function __construct($id = null){
        parent::__construct($id);
        //create new array for error_message whenever a model is instantiated
        $this->error_messages = array();
        $this->_is_new = false;
    }
    //setter for created date which will be based down the chain.
    public function set_created(DateTime $now){
        $this->_created = $now;
    }
    //setter for created date which will be based down the chain.
    public function set_updated_dt(DateTime $now){
        $this->_updated = $now;
    }

    public function removeVirtualFields($fieldsToRemove = null){
        if (isset($fieldsToRemove)){
            foreach ($fieldsToRemove as $field){
                unset($this->_virtual_fields[$field]);
            }
        }else{
            $this->_virtual_fields = array();
        }

        return $this;
    }
}
