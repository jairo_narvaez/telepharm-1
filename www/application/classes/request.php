<?php

class Request extends Kohana_Request {

/**
	 * Returns whether this is an ajax request (as used by JS frameworks)
	 *
	 * @return  boolean
	 */
	public function is_ajax()
	{
		if ($this->headers('x-requested-with')) return true;

		return parent::is_ajax();
	}
}