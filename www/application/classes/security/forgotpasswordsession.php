<?php
class Security_ForgotPasswordSession
{
    public static function create_forgot_password_session($account)
    {
        //check to see if account already has a forgot password session.
        if ($account->forgot_password_session->loaded()) {
            //if a user currently has an active forgot password session, we need to delete it regardless of whether or not it has expired.
            $account->forgot_password_session->delete();
        }
        //load configuration settings related to session creation
        $config = Kohana::$config->load('session.forgot_password_session');
        //load specific token configuration based on whether or not the user has selected 'remember_me'
        //if the user has selected 'remember_me', they have opted into an extended session
        //security for extended sessions is heightened by raising the length of the session token['
        $token_config = $config['token'];
        $token_type_config = $token_config['regular'];
        $now = new DateTime();

        $account->forgot_password_session = Model_AccountToken::make($account,
            Model_AccountSessionType::Forgot_Password,
            $token_type_config['strength'],
            $token_type_config['slide_minutes'],
            $now,
            $_SERVER['REMOTE_ADDR']
        );
        //set the token created datetime
        $account->forgot_password_session->set_issued($now);
        //save the forgot password session token tied to the account to the database.
        $account->forgot_password_session = $account->forgot_password_session->save();

        $crypto_config = $config['crypto'];
        //get the size needed for a proper iv.
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        //create random iv for signing this session.
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        //encrypt session
        $encrypted_token = mcrypt_encrypt(MCRYPT_RIJNDAEL_256,
            $crypto_config['key'],
            $account->id . $token_config['seperator_character'] . $account->forgot_password_session->token->value,
            MCRYPT_MODE_CBC,
            $iv);

        $session = base64_encode($iv) . $crypto_config['seperator_character'] . base64_encode($encrypted_token);

        $mail_view = View::factory('login/email/forgot_password');
        $mail_view->reset_url = 'login/new_password/' . rawurlencode($session);
        $mail_view->account = $account;
        // Send out our password reset confirmation
        $mail = TelePharm_Email::compose();
        $mail->to($account->email, $account->first_name . " " . $account->last_name);

        $site_name = isset(Kohana::$config->load('application')->site_name) ?
            Kohana::$config->load('application')->site_name : "";

        $mail->subject($site_name . " Password Reset Confirmation");
        $mail->html($mail_view->render());

        $mail_transport = TelePharm_Email_Transport::factory();
        $mail_transport->send($mail);

        return true;
    }

    public static function check_forgot_password_session($encrypted)
    {
        $config = Kohana::$config->load('session.forgot_password_session');
        // skip this if we don't have a token
        if (!isset($encrypted) || strlen($encrypted) == 0)
            return new Model_Account();

        $crypto_config = $config['crypto'];

        $crypto_breaker_index = strpos($encrypted, $crypto_config['seperator_character']);

        $iv = base64_decode(substr($encrypted, 0, $crypto_breaker_index));
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);

        if (strlen($iv) == $iv_size) {
            $token_encrypted = base64_decode(substr($encrypted, $crypto_breaker_index + 1, strlen($encrypted)));
            $token_decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256,
                $crypto_config['key'],
                $token_encrypted,
                MCRYPT_MODE_CBC,
                $iv);
            //check to make sure decryption succeeded and a valid encrypted value was supplied.
            if (isset($token_decrypted)) {

                $token_config = $config['token'];
                //get int index of the position of the seperator_character config value within the unencrypted cookie.
                $token_breaker_index = strpos($token_decrypted, $token_config['seperator_character']);
                //break out the account_id from the unencrypted cookie value.
                $account_id = substr($token_decrypted, 0, $token_breaker_index);
                //break out the forgot password session token
                $token = trim(substr($token_decrypted, $token_breaker_index + 1, strlen($token_decrypted)));
                //create account model with account_id supplied by the cookie
                $active_account = new Model_Account($account_id);

                if ($active_account->loaded() //check to make sure the account model is loaded
                    && $active_account->forgot_password_session->loaded() //check to make sure the account has a forgot password session
                ) {
                    //get current date
                    $now = new DateTime();
                    //get token expiration date.
                    $expire = new DateTime($active_account->forgot_password_session->token->expires);
                    //check to make sure session has not expired.
                    if ($now < $expire) {
                        //compare unencrypted token supplied by the client with token stored in the database
                        if (strcmp($active_account->forgot_password_session->token->value, $token) == 0) {
                            //check to make sure the IP address which the supplied token came from is the same as the IP address which the token was created with.
                            //if they aren't, that's bad news bears.
                            if ($config['validate_ip']) {
                                if (strcmp($active_account->forgot_password_session->token->ipaddress, $_SERVER['REMOTE_ADDR']) == 0) {
                                    return $active_account;
                                } else {
                                    //a session hijacking attempt has occurred.
                                    //we should log this or somehow otherwise document that it occurred.
                                }
                            } else {
                                return $active_account;
                            }
                        } else {
                            //invalid token supplied, do something?
                        }
                    } else {
                        //session has expired and should be deleted.
                        $active_account->forgot_password_session->delete();
                    }
                }
            }
        }
        return new Model_Account();
    }
}