<?php
class Security_NativeClientSession{
    //returns a new session token valid for the next request if the session token submitted with the current request is valid
    //otherwise returns null.
    public static function check_native_client_session($request){
        $self = new static();
        $session_token = $request->post('session_token');
        $config = Kohana::$config->load('session.native_client_session');
        $crypto_config = $config['crypto'];
        $crypto_breaker_index = strpos($session_token, $crypto_config['seperator_character']);

        $iv = base64_decode(substr($session_token, 0, $crypto_breaker_index));
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);

        if (strlen($iv) == $iv_size) {
            $token_encrypted = base64_decode(substr($session_token, $crypto_breaker_index + 1, strlen($session_token)));

             $token_decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256,
                $crypto_config['key'],
                $token_encrypted,
                MCRYPT_MODE_CBC,
                $iv);

            $token_config = $config['token'];

            $token_breaker_index = strpos($token_decrypted, $token_config['seperator_character']);

            $account_id = substr($token_decrypted, 0, $token_breaker_index);

            $token = trim(substr($token_decrypted, $token_breaker_index + 1, strlen($token_decrypted)));

            $active_account = new Model_Account($account_id);

            if ($active_account->loaded() && $active_account->native_client_session->loaded()){

                $now = new DateTime();

                $expire = new DateTime($active_account->native_client_session->token->expires);

                if ($now < $expire) {
                    if (strcmp($active_account->native_client_session->token->value, $token) == 0) {
                          if ($config['validate_ip']) {
                            if (strcmp($active_account->native_client_session->token->ipaddress, $_SERVER['REMOTE_ADDR']) == 0) {
                                return $self->write_session($active_account, $config, $now);
                            } else {
                                //a session hijacking attempt has occurred.
                                //we should log this or somehow otherwise document that it occurred.
                            }
                        } else {
                            return $self->write_session($active_account, $config, $now);
                        }
                    } else {
                        //invalid token supplied, do something?
                    }
                } else {
                    //session has expired and should be deleted.
                    $active_account->native_client_session->delete();
                }
            }
        }
        return null;
    }

    //creates a new session, saves it to the database, and returns a session token if the state of the application is valid
    //otherwise returns null.
    public static function create_native_client_session($account)
    {
        if ($account->native_client_session->loaded()) {
            $now = new DateTime();
            $expires = new DateTime($account->native_client_session->token->expires);

            if ($now < $expires) {
                return null;
            } else {
                $account->native_client_session->delete();
            }
        }

        $config = Kohana::$config->load('session.native_client_session');

        $token_config = $config['token'];
        $token_type_config =  $token_config['regular'];

        $now = new DateTime();

        $account->native_client_session = Model_AccountToken::make($account,
            Model_AccountSessionType::Native_Client,
            $token_type_config['strength'],
            $token_type_config['slide_minutes'],
            $now,
            $_SERVER['REMOTE_ADDR']
        );
        //set the token created datetime
        $account->native_client_session->set_issued(new DateTime());
        //save the web session token tied to the account to the database.
        $account->native_client_session = $account->native_client_session->save();

        $crypto_config = $config['crypto'];
        //get the size needed for a proper iv.
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        //create random iv for signing this session.
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        //encrypt session
        $encrypted_token = mcrypt_encrypt(MCRYPT_RIJNDAEL_256,
            $crypto_config['key'],
            $account->id . $token_config['seperator_character'] . $account->native_client_session->token->value,
            MCRYPT_MODE_CBC,
            $iv);

        $session_token = base64_encode($iv) . $crypto_config['seperator_character'] . base64_encode($encrypted_token);
        $account->login_dt = date_create('now');
        $account->save();

        return $session_token;
    }

    private function write_session($account, $config, $now, $is_ajax)
    {
            $account->native_client_session->set_issued($now);

            $account->native_client_session = $account->native_client_session->save();

            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);

            $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

            $encrypted_token = mcrypt_encrypt(MCRYPT_RIJNDAEL_256,
                $config['crypto']['key'],
                $account->id . $config['token']['seperator_character'] . $account->native_client_session->token->value,
                MCRYPT_MODE_CBC,
                $iv);

            $session_token = base64_encode($iv) . $config['crypto']['seperator_character'] . base64_encode($encrypted_token);
        
        return $session_token;
    }
}