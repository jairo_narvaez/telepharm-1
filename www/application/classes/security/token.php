<?php
class Security_Token{
    public static function generate($length){
        $token = '';
        if (function_exists('openssl_random_pseudo_bytes'))
        {
            //never ever sign a session with a cryptographically insecure random string.  NEVER.
            $crypto_strong = true;
            //get a bunch of cryptographically secure random bytes.
            $bytes = openssl_random_pseudo_bytes($length, $crypto_strong);
            //convert those cryptographically secure random bytes into a nice string.
            $token = bin2hex($bytes);
        }
        else
        {
            //this is unbelievably bad; this should never be run in a production instance.
            //this scares me.  it is on my hit list.
            //we should be throwing an exception and puking if the application tries to sign a token like this.
            for ($i = 0; $i < intValue($length / 2); ++$i)
                $token .= pack('S1', mt_rand(0, 0xffff));
        }
        return $token;
    }
}