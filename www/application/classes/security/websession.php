<?php
class Security_WebSession
{
    public static function check_web_session($is_ajax)
    {
        $security_session = new static();
//load session related configuration options.
        $config = Kohana::$config->load('session.web_session');
        //check to see if session exists.
        if (array_key_exists('session', $_COOKIE)) {
            //get encrypted session cookie.
            $encrypted = urldecode($_COOKIE['session']);

            $crypto_config = $config['crypto'];

            $crypto_breaker_index = strpos($encrypted, $crypto_config['seperator_character']);

            $iv = base64_decode(substr($encrypted, 0, $crypto_breaker_index));
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);

            if (strlen($iv) == $iv_size) {
                $token_encrypted = base64_decode(substr($encrypted, $crypto_breaker_index + 1, strlen($encrypted)));

                //decrypt session cookie using session configuration key.
                //remember, sessions are signed so that they can be validated across platforms/stacks (not just in PHP/Kohana)
                $token_decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256,
                    $crypto_config['key'],
                    $token_encrypted,
                    MCRYPT_MODE_CBC,
                    $iv);
                //check to make sure decryption succeeded and a valid encrypted value was supplied.
                if (isset($token_decrypted)) {

                    $token_config = $config['token'];
                    //get int index of the position of the seperator_character config value within the unencrypted cookie.
                    $token_breaker_index = strpos($token_decrypted, $token_config['seperator_character']);
                    //break out the account_id from the unencrypted cookie value.
                    $account_id = substr($token_decrypted, 0, $token_breaker_index);
                    //break out the web session token from the unencrypted cookie value.
                    $token = trim(substr($token_decrypted, $token_breaker_index + 1, strlen($token_decrypted)));
                    //create account model with account_id supplied by the cookie
                    $active_account = new Model_Account($account_id);

                    if ($active_account->loaded() //check to make sure the account model is loaded
                        && $active_account->web_session->loaded() //check to make sure the account has a web session
                    ) {
                        //get current date
                        $now = new DateTime();
                        //get token expiration date.
                        $expire = new DateTime($active_account->web_session->token->expires);
                        //check to make sure session has not expired.
                        if ($now < $expire) {
                            //compare unencrypted token supplied by the client with token stored in the database
                            if (strcmp($active_account->web_session->token->value, $token) == 0) {
                                //check to make sure the IP address which the supplied token came from is the same as the IP address which the token was created with.
                                //if they aren't, that's bad news bears.
                                if ($config['validate_ip']) {
                                    if (strcmp($active_account->web_session->token->ipaddress, $_SERVER['REMOTE_ADDR']) == 0) {
                                        return $security_session->write_session($active_account, $config, $now, $is_ajax);
                                    } else {
                                        //a session hijacking attempt has occurred.
                                        //we should log this or somehow otherwise document that it occurred.
                                    }
                                } else {
                                    return $security_session->write_session($active_account, $config, $now, $is_ajax);
                                }
                            } else {
                                //invalid token supplied, do something?
                                $active_account->web_session->delete();
                            }
                        } else {
                            //session has expired and should be deleted.
                            $active_account->web_session->delete();
                        }
                    }
                }
            }
        }
        return new Model_Account();
    }

    public static function create_web_session($account, $extended = false)
    {
        //make sure account exists
        if (!$account->loaded()) return false;
        //check to see if account already has a web session.
        if ($account->web_session->loaded()) {
            //the web session associated with this account has expired, delete it and continue.
            $account->web_session->delete();
        }
        //load configuration settings related to session creation
        $config = Kohana::$config->load('session.web_session');
        //load specific token configuration based on whether or not the user has selected 'remember_me'
        //if the user has selected 'remember_me', they have opted into an extended session
        //security for extended sessions is heightened by raising the length of the session token['
        $token_config = $config['token'];
        $token_type_config = $extended
            ? $token_config['extended']
            : $token_config['regular'];
        $now = new DateTime();
        //try to get a good ip adddress
        $ip_address = '';
//        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '') {
//            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
//        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
//        }
        //create a web session for the account.
        //This creates a cryptographically secure random token for the account.
        //this token is used to authenticate the user on the next request
        //authentication code is in the base controller of authenticated views (currently 'Controller_Authenticated')
        //authentication should occur before every request for authenticated resources (in 'before' controller method)
        $account->web_session = Model_AccountToken::make($account,
            Model_AccountSessionType::Web,
            $token_type_config['strength'],
            $token_type_config['slide_minutes'],
            $now,
            $ip_address
        );
        //set the token created datetime
        $account->web_session->set_issued(new DateTime());
        //save the web session token tied to the account to the database.
        $account->web_session = $account->web_session->save();

        $crypto_config = $config['crypto'];
        //get the size needed for a proper iv.
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        //create random iv for signing this session.
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        //encrypt session
        $encrypted_token = mcrypt_encrypt(MCRYPT_RIJNDAEL_256,
            $crypto_config['key'],
            $account->id . $token_config['seperator_character'] . $account->web_session->token->value,
            MCRYPT_MODE_CBC,
            $iv);
        //issue an encrypted cookie
        //value to be encrypted consists of the account unique identifier and the web session token
        //these two values are seperated by a configurable character
        //changing the seperator_character will invalidate all active sessions
        //the cipher key is configurable and should always remain a secret.
        //Should it be compromised, it would need to be changed immediately.
        //changing the cipher key will invalidate all active sessions.
        //session cookies are signed so that they can be validated across multiple platforms/stacks
        $session = base64_encode($iv) . $crypto_config['seperator_character'] . base64_encode($encrypted_token);
        $expires = new DateTime($account->web_session->token->expires);
        setcookie('session', urlencode($session), $expires->getTimestamp(), '/');
        $account->login_dt = date_create('now');
        $account->save();

        return true;
    }

    private function write_session($account, $config, $now, $is_ajax)
    {
        if (!$is_ajax) {
            //all criteria for validating the supplied token have been met
            //set the token created datetime
            $account->web_session->set_issued($now);
            //update session to issue new token for next request.
            $account->web_session = $account->web_session->save();
            //set the current user session identity

            //issue new token for next request
            //get the size needed for a proper iv.
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
            //create random iv for signing this session.
            $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
            //encrypt session
            $encrypted_token = mcrypt_encrypt(MCRYPT_RIJNDAEL_256,
                $config['crypto']['key'],
                $account->id . $config['token']['seperator_character'] . $account->web_session->token->value,
                MCRYPT_MODE_CBC,
                $iv);
            //issue an encrypted cookie
            //value to be encrypted consists of the account unique identifier and the web session token
            //these two values are seperated by a configurable character
            //changing the seperator_character will invalidate all active sessions
            //the cipher key is configurable and should always remain a secret.
            //Should it be compromised, it would need to be changed immediately.
            //changing the cipher key will invalidate all active sessions.
            //session cookies are signed so that they can be validated across multiple platforms/stacks
            $session = base64_encode($iv) . $config['crypto']['seperator_character'] . base64_encode($encrypted_token);
            $expires = new DateTime($account->web_session->token->expires);
            setcookie('session', urlencode($session), $expires->getTimestamp(), '/');
        }
        return $account;
    }
}