<?php

use mef\ACL\Manager;
use mef\ACL\AssertionInterface;
use mef\ACL\RoleInterface;
use mef\ACL\ResourceInterface;
/**
 *
 * check if a resource can be edited by a user with access to a set
 * of companies
 *
 **/
class Telepharm_ACL_CompanyOwnerAssertion implements AssertionInterface
{
	protected $resourceKey;

	public function __construct($resourceKey)
	{
		$this->resourceKey = $resourceKey;
	}

	public function assert(Manager $acl, RoleInterface $role = null,
		ResourceInterface $resource = null, $privilege = null)
	{
		return in_array($resource->{$this->resourceKey}, array_keys($role->companies));
	}
}

