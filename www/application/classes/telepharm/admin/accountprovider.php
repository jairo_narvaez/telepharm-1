<?php

/**
 * Custom class for account scaffolding
 * 
 */
class Telepharm_Admin_AccountProvider extends \TelePharm_DataProvider
{
	private $current_user;

	/**
	 * Init the provider for an account
	 */
	public function __construct()
	{
		parent::__construct('Account');
	}

	public function setCurrentUser($current_user)
	{
		$this->current_user = $current_user;
	}

	/**
	 * Override of the dataprovider method to use my own form
	 * 
	 * @param string $action (url)
	 * @param int    $id      Optional, defaults to null. 
	 * 
	 * @return Form_Admin_Account
	 */
	public function createForm($action, $id=null)
	{
		$data = [];
		if (isset($id)) 
		{
			$record = $this->model->where('id','=',$id)->
				find();

			#echo "<pre>"; print_r($record); echo "</pre>";

			$permissions = Helper_Permission::instance();
			if (! $permissions->isAllowed('account', 'edit'))
				throw new HTTP_Exception_403();

			if (!$this->isSuperAdmin())
			{
				if ($record->company_id != $this->current_user->getAccount()->company_id)
				{
					throw new HTTP_Exception_403();
				}
			}

			$data = $record->as_array();
		}
		$form = new Form_Admin_Account;
		if ($this->isSuperAdmin()) $form->showCompanySelect(true);
		$form->create();
		$form->load($data);
		$form->action = $action;
		return $form;
	}

	public function getRows()
	{
		$model = Model::factory($this->modelName);
		
		if (!$this->isSuperAdmin())
		{
			$model->where('company_id', '=', $this->current_user->getAccount()->company_id);
		}

		return $model->find_all()->as_array();
	}

	/**
	 * Return only the columns we want to display
	 * 
	 * @return array
	 */
	public function getColumns()
	{
		$columns = parent::getColumns();

		$wanted = ['id', 
			'username', 
			'email', 
			'first_name', 
			'last_name', 
			'created_dt', 
			];

		$result = [];
		foreach($wanted as $col)
			$result[$col] = $columns[$col];
		return $result;
	}

	/**
	 * Override default create
	 * 
	 * @param Form_Admin_Account $form 
	 * 
	 * @return boolean
	 */
	public function create($form)
	{
		$record = Model::factory($this->modelName);

		$data = $form->as_array();

		if ($data['password'] == '')
		{
			unset($data['password']);
		}

		if (!$this->isSuperAdmin())
		{
			$data['company_id'] = $this->current_user->getAccount()->company_id;
		}

		$record->values($data);

		$record->save();

		return $record->saved();
	}

	/**
	 * Override default update 
	 * 
	 * @param Form_Admin_Account $form 
	 * 
	 * @return boolean
	 */
	public function update($form)
	{
		$record = Model::factory($this->modelName)->
			where('id', '=', $form['id']->saneValue)->
			find();

		$data = $form->as_array();
		if ($data['password'] == '')
			unset($data['password']);

		$data['company_id'] = $record->company_id;

		$record->values($data);

		$record->save();

		return $record->saved();
	}

	/**
	 * Override account delete
	 * 
	 * @param int $id 
	 * 
	 * @return boolean
	 */
	public function delete($id)
	{
		$account = Model::factory('Account')->
			where('id', '=', $id)->
			find();

		$account->delete();

		return true;
	}

	private function isSuperAdmin()
	{
		if (!$this->current_user) throw new Exception('Please load current user');

		$super_admin_role_id = Model::factory('Role')
			->where('type', '=', 'admin')
			->find()
			->id;

		$account_role = Model::factory('AccountRole')
			->where('account_id', '=', $this->current_user->getAccount()->id)
			->and_where('role_id', '=', $super_admin_role_id)
			->find();

		return $account_role->loaded();
	}
}
