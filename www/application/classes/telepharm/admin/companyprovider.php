<?php

/**
 * Custom class for company scaffolding
 * 
 */
class Telepharm_Admin_CompanyProvider extends \TelePharm_DataProvider
{
	/**
	 * Init the provider for a company
	 */
	public function __construct()
	{
		parent::__construct('Company');
	}

	/**
	 * Override of the dataprovider method to use my own form
	 * 
	 * @param string $action (url)
	 * @param int    $id      Optional, defaults to null. 
	 * 
	 * @return Form_Admin_Company
	 */
	public function createForm($action, $id=null)
	{
		$data = [];
		if (isset($id)) 
		{
			$record = $this->model->where('id','=',$id)->find();
			$permissions = Helper_Permission::instance();
			if (! $permissions->isAllowed($record, 'view'))
				throw new HTTP_Exception_403();
			$data = $record->as_array();
			$data['location'] = $this->model->location->as_array();
			$data['location']['location_name'] = $this->model->location->name;
		}
		$form = new Form_Admin_Company;
		$form->load($data);
		$form->action = $action;
		return $form;
	}

	/**
	 * Return only the columns we want to display
	 * 
	 * @return array
	 */
	public function getColumns()
	{
		$columns = parent::getColumns();

		$wanted = ['id', 
			'name', 
			'description', 
			'phone', 
		];

		$result = [];
		foreach($wanted as $col)
			$result[$col] = $columns[$col];
		return $result;
	}

	/**
	 * Show only stores of that 
	 */
	public function getRows()
	{
		$result = [];
		$permissions = Helper_Permission::instance();
		foreach( Model::factory($this->modelName)->
			find_all() as $company)
		{
			if (! $permissions->isAllowed($company, 'view'))
				continue;
			$result[] = $company->as_array();
		}

		return $result;
	}

	/**
	 * Override Create method
	 * to create the location too
	 * 
	 * @param Form_Admin_Company $form 
	 * 
	 * @return boolean
	 */
	public function create($form)
	{

		$location = Model::factory('Location');
		$locationData = $form->location->as_array();
		$location->values($locationData);
		$location->name = $locationData['location_name'];
		$location->save();

		$company = Model::factory('Company');
		$company->values($form->as_array());
		$company->location_id = $location->id;
		$company->save();

		return $company->saved();
	}

	/**
	 * Override update method
	 * to update the location too
	 * 
	 * @param Form_Admin_Company $form 
	 * 
	 * @return boolean 
	 */
	public function update($form)
	{
		$company = Model::factory('Company')->
			where('id', '=', $form['id']->saneValue)->
			find();
		$company->values($form->as_array());
		$company->save();

		$locationData = $form->location->as_array();
		$company->location->values($locationData);
		$company->location->name = $locationData['location_name'];
		$company->location->save();

		return $company->location->saved();
	}

	/**
	 * Override company delete
	 * 
	 * @param int $id 
	 * 
	 * @return boolean
	 */
	public function delete($id)
	{
		$company = Model::factory('Company')->
			where('id', '=', $id)->
			find();

		$location = $company->location;
		$company->delete();
		$location->delete();

		return true;
	}
	
}
