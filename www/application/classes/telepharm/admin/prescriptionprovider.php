<?php

/**
 * Custom class for prescription scaffolding
 *
 */
class Telepharm_Admin_PrescriptionProvider extends \TelePharm_DataProvider
{
	/**
	 * Init the provider for an Prescription
	 */
	public function __construct()
	{
		parent::__construct('Prescription');
	}

	/**
	 * Return only the columns we want to display
	 *
	 * @return array
	 */
	public function getColumns()
	{
		$columns = parent::getColumns();

		$wanted = ['id',
			'created_by',
			'patient_fname',
			'patient_lname',
			'number',
			'completed',
			'created_dt'
			];

		$result = [];
		foreach($wanted as $col)
			$result[$col] = $columns[$col];
		return $result;
	}
}
