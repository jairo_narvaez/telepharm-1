<?php

/**
 * Custom class for store scaffolding
 * 
 */
class Telepharm_Admin_StoreProvider extends \TelePharm_DataProvider
{
	/**
	 * The company where this store is
	 * created
	 * 
	 * @var Model_Company
	 */
	protected $company;

	/**
	 * Init the provider for a store
	 */
	public function __construct(Model_Company $company)
	{
		parent::__construct('store');
		$this->company = $company;
		$permissions = Helper_Permission::instance();
		if (!$permissions->isAllowed($company, 'view'))
			throw new HTTP_Exception_403;
	}

	/**
	 * Override of the dataprovider method to use my own form
	 * 
	 * @param string $action (url)
	 * @param int    $id      Optional, defaults to null. 
	 * 
	 * @return Form_Admin_Store
	 */
	public function createForm($action, $id=null)
	{
		# default value of the company
		$data = ['company_id' => $this->company->id];
		if (isset($id)) 
		{
			$record = $this->model->where('id','=',$id)->find();
			$permissions = Helper_Permission::instance();
			if (! $permissions->isAllowed($record, 'view'))
				throw new HTTP_Exception_403();
			$data = $record->as_array();
			$data['location'] = $this->model->location->as_array();
			$data['location']['location_name'] = $this->model->location->name;
		}
		$form = new Form_Admin_Store($this->company);
		$form->load($data);
		$form->action = $action;
		return $form;
	}

	/**
	 * Show only stores of that company
	 */
	public function getRows()
	{
		return Model::factory($this->modelName)->
			where('company_id', '=', $this->company->id)->
			find_all()->as_array();
	}

	/**
	 * Return only the columns we want to display
	 * 
	 * @return array
	 */
	public function getColumns()
	{
		$columns = parent::getColumns();

		$wanted = ['id', 
			'name', 
			'description', 
			'contact', 
			'phone', 
		];

		$result = [];
		foreach($wanted as $col)
			$result[$col] = $columns[$col];
		return $result;
	}

	/**
	 * Override Create method
	 * to create the location too
	 * 
	 * @param Form_Admin_Store $form 
	 * 
	 * @return boolean
	 */
	public function create($form)
	{

		$location = Model::factory('Location');
		$locationData = $form->location->as_array();
		$location->values($locationData);
		$location->name = $locationData['location_name'];
		$location->save();

		$store = Model::factory('Store');
		$store->values($form->as_array());
		$store->location_id = $location->id;
		$store->save();

		return $store->saved();
	}

	/**
	 * Override update method
	 * to update the location too
	 * 
	 * @param Form_Admin_Store $form 
	 * 
	 * @return boolean 
	 */
	public function update($form)
	{
		$store = Model::factory('Store')->
			where('id', '=', $form['id']->saneValue)->
			find();
		$store->values($form->as_array());
		$store->save();

		$locationData = $form->location->as_array();
		$store->location->values($locationData);
		$store->location->name = $locationData['location_name'];
		$store->location->save();

		return $store->location->saved();
	}

	/**
	 * Override store delete
	 * 
	 * @param int $id 
	 * 
	 * @return boolean
	 */
	public function delete($id)
	{
		$store = Model::factory('Store')->
			where('id', '=', $id)->
			find();

		$location = $store->location;
		$store->delete();
		$location->delete();

		return true;
	}
	
}

