<?php

/**
 * A singleton for creating Authorize.net objects with auth parameters 
 *
 * Requires the authorize.net kohana module.
 *
 * Configuration called 'authorizenet' must include:
 *
 * return [
 *   'api_login_id' => 'string',
 *   'transaction_key' => 'string',
 *   'in_sandbox' => false, 
 * ];
 */
class TelePharm_AuthorizeNet
{
	use traits\TelePharm_Singleton;

	protected $config;

	protected function initialize()
	{
		$this->config = Kohana::$config->load('authorizenet');
	}

	/**
	 * Create an AuthorizeNetAIM instance with credentials already set.
	 *
	 * $aim = M2H_AuthorizeNet::getInstance()->createAIM();
	 *
	 * @return AuthorizeNetAIM
	 */
	public function createAIM()
	{
		$aim = new AuthorizeNetAIM($this->config->api_login_id, $this->config->transaction_key);
		$aim->setSandbox($this->config->in_sandbox);

		return $aim;
	}

	/**
	 * Create an AuthorizeNetARB instance with credentials already set.
	 *
	 * $arb = M2H_AuthorizeNet::getInstance()->createARB();
	 *
	 * @return AuthorizeNetARB
	 */
	public function createARB()
	{
		$arb = new AuthorizeNetARB($this->config->api_login_id, $this->config->transaction_key);
		$arb->setSandbox($this->config->in_sandbox);
		
		return $arb;
	}

	/**
	 * Create an AuthorizeNetCIM instance with credentials already set.
	 *
	 * $cim = M2H_AuthorizeNet::getInstance()->createCIM();
	 *
	 * @return AuthorizeNetCIM
	 */
	public function createCIM()
	{
		$cim = new AuthorizeNetCIM($this->config->api_login_id, $this->config->transaction_key);
		$cim->setSandbox($this->config->in_sandbox);
		
		return $cim;
	}

	/**
	 * Checks to see whether a subscription is active.
	 *
	 * This is just a convenience wrapper around getSubscriptionStatus(). If more detail
	 * is needed, then it should be called in place of this function.
	 *
	 * @return bool       true if subscription is active; false if not
	 * @throws Exception  if unable to communicate with the server, an exception is thrown
	 */
	public function isSubscriptionActive($id)
	{
		$arb = $this->createARB();
		$resp = $arb->getSubscriptionStatus($id);
		return $resp->isOK() && $resp->xpath_xml->status == 'active';
	}
}
