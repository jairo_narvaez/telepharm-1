<?php
/**
 * An abstract class for implementing a cache. 
 *
 * Implementations do not have to guarantee that the cached item live as long
 * as the requested expiration date, so anything that is cached must be able
 * to be rebuilt at any time.
 */
abstract class TelePharm_Cache
{

	abstract protected function _get($key);
	abstract protected function _set($key, $val, DateTime $expiration = null);
	abstract protected function _delete($key);

	/**
	 * Store some value as identified by a unique key. 
	 *
	 * @param $key string            A unique identifier
	 * @param $val mixed             Any serializable data
	 * @param $expiration DateTime   The maximum time to live. Actual lifespan
	 *                               may be less than requested.
	 */
	final public function set($key, $val, DateTime $expiration = null)
	{
		return $this->_set($key, $val, $expiration);
	}


	/**
	 * Delete a value from a key
	 * 
	 * @param string $key 
	 * 
	 * @return boolean
	 */
	final public function delete($key)
	{
		return $this->_delete($key);
	}

	/**
	 * Retrieve data by a key.
	 *
	 * If the callback is specified, it will always be called after the data
	 * is retrieved (hit or miss). The callback is passed the retrieved data.
	 * It should receive that data by reference, and modify it as needed.
	 *
	 * The callback returns:
	 *    false     if the data should NOT be cached
	 *    true      if the data should be cached indefinitely
	 *    DateTime  if the data should be cached until that expiration time
	 *
	 * @param $key string     A unique identifier
	 * @param $cb  callback   DateTime function(string &$data)
	 *
	 * @return mixed          The cached (or generated) data   
	 */
	final public function get($key, callable $cb = null)
	{
		$data = $this->_get($key);

		if ($cb)
		{
			$expiration = $cb($data);
			if ($expiration != false)
			{
				if ($expiration === true) $expiration = null;
				$this->set($key, $data, $expiration);
			}	
		}

		return $data;
	}


	/**
	 * Creates an object of a concrete implementation
	 * of a cache backend
	 * 
	 * @param string $name Optional, defaults to null. 
	 * 
	 * @return TelePharm_Cache_${name}
	 */
	public static function factory($name = null)
	{
		$args = func_get_args();
		if (!$name)
		{
			$name = Kohana::$config->load('application.cache_implementation');
			if (!$name) throw new Exception("No cache_implementation specified on config");
		}
		
		$classname = 'TelePharm_Cache_'.$name;
		$reflector = new ReflectionClass($classname);
		array_shift($args); // pop the first element
		$verifier = $reflector->newInstanceArgs($args);
		return $verifier;

	}
}
