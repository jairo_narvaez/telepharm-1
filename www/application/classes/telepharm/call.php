<?php

/**
 * Provide the mechanism to call queue the video
 * conferences between TP users 
 */
class Telepharm_Call implements TelePharm_Untaintable
{
	protected $fromAccount;
	protected $toStore;
	protected $toAccount;
	protected $origin;
	protected $callQueueId = null;
	protected $tokenGeneratorClass = null;
	protected $fromPrescriptionId = null;
	protected $prescription = null;
	protected $toPharmacist = null;
	protected $fromAsPatient = null;

    const num_attempts = 3;

	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;


	/**
	 * Call constructor
	 *
	 * @param string $origin page where the call was made
	 * @param string $tokenGeneratorClass set the name of the class to use to generate the token
	 * 
	 */
	public function __construct($origin, $tokenGeneratorClass='Telepharm_TokBox')
	{
		$this->autoGetters = [
			'fromAccount',
			'toStore',
			'toAccount',
			'origin',
			'callQueueId',
			'fromPrescriptionId',
			'toPharmacist',
			'fromAsPatient'
			];
		$this->fromAccount = null;
		$this->toAccount = null;
		$this->toStore = null;
		$this->fromPrescriptionId = null;
		$this->prescription = null;
		$this->origin = $origin;
		$this->toPharmacist = true;
		$this->fromAsPatient = 0;
		$this->tokenGeneratorClass = $tokenGeneratorClass;
	}

	/**
	 * set the from account
	 * 
	 * @param integer $accountId the id of the account
	 * 
	 * @return void
	 */
    public function setFromAccount($accountId)
    {
        if ($accountId == NULL)
        {
            $this->fromAccount = null;
            return;
        }
        $this->fromAccount = Model::factory('Account')->
            where('id', '=', $accountId)->
            find();
        if (!$this->fromAccount->loaded())
        {
            $this->fromAccount = null;
            throw new Exception('From Account not found');
        }
    }

    /* set the to account
     *
     * @param integer $accountId the id of the account
     *
     * @return void
     */
    public function setToAccount($accountId)
    {
        if ($accountId == NULL)
        {
            $this->toAccount = null;
            return;
        }
        $this->toAccount = Model::factory('Account')->
            where('id', '=', $accountId)->
            find();
        if (!$this->toAccount->loaded())
        {
            $this->toAccount = null;
            throw new Exception('To Account not found');
        }
    }

	/* set the to store
	 * 
	 * @param integer $storeId the id of the store
	 * 
	 * @return void
	 */
	public function setToStore($storeId)
	{
		if ($storeId == NULL) 
		{
			$this->toStore = null;
			return;
		}
		$this->toStore = Model::factory('Store')->
			where('id', '=', $storeId)->
			find();
		if (!$this->toStore->loaded())
		{
			$this->toStore = null;
			throw new Exception('To Store not found');
		}
	}

	/**
	 * Restore a call object from the db
	 * 
	 * @param integer $callId 
	 * 
	 * @return void
	 */
	public function setCallQueueId($callId)
	{
		$this->callQueueId = $callId;
		$callQueue = $this->getCallQueue();
		$this->setFromAccount($callQueue->from_account_id);
		$this->setToAccount($callQueue->to_account_id ?: NULL);
		$this->setToStore($callQueue->to_store_id ?: NULL);
		$this->setFromPrescriptionId($callQueue->from_prescription_id?: NULL);
	}

	/**
	 * Set a prescription id
	 * 
	 * @return void
	 */
	public function setFromPrescriptionId($prescriptionId)
	{
		$this->fromPrescriptionId = $prescriptionId;
		$this->prescription = Model::factory('Prescription')->
			where('id', '=', $prescriptionId)->
			find();
	}

	/**
	 * Set a from as patient
	 * 
	 * @return void
	 */
	public function setFromAsPatient($bool)
	{
		$this->fromAsPatient = $bool;
	}

	/**
	 * TelePharm_Untaintable implementaion
	 * 
	 * @return boolean
	 */
	public function isTainted()
	{
		return true;
	}

	/**
	 * TelePharm_Untaintable implementaion
	 * 
	 * @return array
	 */
	public function untaint()
	{
        $call_queue = $this->getCallQueue();
        $patient_api_session = ORM::factory('PatientAPISession')
            ->where('call_queue_id', '=', $call_queue->id)
            ->order_by('id', 'desc')->find();
        $patient_api_session_prescriptions = ORM::factory('PatientAPISessionPrescription')
            ->where('patient_api_session_id', '=', $patient_api_session->id)
            ->where('prescription_id', '<>', $call_queue->from_prescription_id)
            ->find_all()->untaint();
        $results = [];
        foreach ($patient_api_session_prescriptions as $next_prescription){
            $prescription = new Model_Prescription($next_prescription['prescription_id']);
            $results[] = ['description'=>$prescription->drug['description'], 'number'=>$prescription->number, 'refill_number'=>$prescription->refill_number];
        }
		return array_merge($call_queue->untaint(),
			[
				'from_name' => $this->fromAccount->full_name,
				'to_name' => $this->toAccount != NULL ? $this->toAccount->full_name : '',
                'additional_rx' => $results
			]);
	}

	/**
	 * Check if participants are online
	 * and available to answer a call
	 * 
	 * @return boolean
	 */
    public function tryCall()
    {
        if ($this->toAccount == null)
            throw new Exception('Try Call using method tryCallTo');

        try
        {
            $callQueue = $this->getCallQueue();
        }
        catch(Exception $e)
        {
            $callQueue = null;
        }
        if ($callQueue != NULL && !$callQueue->from_as_patient)
        {
            if (!$this->fromAccount->is_active ||
                !$this->fromAccount->is_available)
            {
                return false;
            }
        }

        if (!$this->toAccount->is_active ||
            !$this->toAccount->is_available)
        {
            return false;
        }

        return $this->placeCall();
    }

    public function checkCurrentCall($account_id){
        $currentCalls = Model::factory('CallQueue')->
            where_open()->
            where('from_account_id', '=', $account_id)->
            or_where('to_account_id', '=', $account_id)->
            where_close()->
            where('call_status', 'in', ['ringing', 'accepted'])->
            where('complete_status', '=', 'incomplete')->
            find();

        return $currentCalls->loaded();
    }

    private function check_active_calls(){
        $currentCalls = Model::factory('CallQueue')->
            where_open()->
            where('from_account_id', 'in', [$this->fromAccount->id, $this->toAccount->id])->
            or_where('to_account_id', 'in', [$this->fromAccount->id, $this->toAccount->id])->
            where_close()->
            where('call_status', 'in', ['ringing', 'accepted'])->
            where('complete_status', '=', 'incomplete')->
            find();

        return $currentCalls->loaded();
    }

    private function placeCall()
    {
        $currentCalls = $this->check_active_calls();

        if ($currentCalls)
            throw new Exception('One of the participants is busy with another call');

        if ($this->callQueueId == NULL)
        {
            $callQueue = Model::factory('CallQueue');
        }
        else
        {
            $callQueue = Model::factory('CallQueue')->
                where('id', '=', $this->callQueueId)->
                find();;
        }
        $callQueue->call_status = 'ringing';
        $callQueue->from_account_id = $this->fromAccount->id;
        $callQueue->from_as_patient = $callQueue->from_as_patient || $this->fromAsPatient || ($this->fromPrescriptionId != null);
        $callQueue->to_account_id = $this->toAccount->id;
        $callQueue->origin = $this->origin;
        $callQueue->save();

        $this->callQueueId = $callQueue->id;

        // publish the incoming call as notification
        if ($callQueue->from_as_patient)
        {
            if (isset($this->prescription))
            {
                $message = 'Call from patient ' . $this->prescription->patient_fname . ' ' . $this->prescription->patient_lname;
            }
            else
            {
                $message = 'Call from patient';
            }
        }
        else
        {
            $message = 'Call from ' . $this->fromAccount->full_name;
        }
        $user = new CurrentUser($this->fromAccount);
        Model_Notification::publishToAccount($user,
            'incoming-call', $message,
            $this->toAccount->id, $callQueue->untaint());


        return true;
    }

	/**
	 * Place a call
	 * 
	 * @return boolean
	 */
	public function startCall()
	{
		$callQueue = $this->getCallQueue();
        $callQueue->call_status = 'ringing';
		$callQueue->is_pending = false;
		$callQueue->to_account_id = $this->toAccount->id;
		$callQueue->save();

		$this->callQueueId = $callQueue->id;

		// publish the incoming call as notification
		if ($callQueue->from_as_patient)
		{
			if (isset($callQueue->from_prescription_id))
			{
				$message = 'Call from patient ' . $this->prescription->patient_fname . ' ' . $this->prescription->patient_lname;
			}
			else
			{
				$message = 'Call from patient';
			}
		}
		else
		{
			$message = 'Call from ' . $this->fromAccount->full_name;
		}
		$user = new CurrentUser($this->fromAccount);
		Model_Notification::publishToAccount($user,
			'incoming-call', $message,
			$this->toAccount->id, $callQueue->untaint());


		return true;
	}

    public function validate_candidate($account_id){
        $callQueue = $this->getCallQueue();
        if ($callQueue->possible_candidates == NULL) return false;
        $candidates = json_decode($callQueue->possible_candidates, true);
        $highest_tried = -1;
        $is_valid = false;
        foreach ($candidates as &$c){
            if ($c['tried'] || $c['un_authenticated'] || $c['on_call']){
                $highest_tried = $c['call_order'];
            }
            $account = new Model_Account($c['account_id']);
            if ($account->is_active && $account->is_available && $account->web_session->is_authenticated()){
                $currently_on_call = $this->checkCurrentCall($account->id);
                if ($currently_on_call){
                    $c['on_call'] = true;
                }else{
                    $c['on_call'] = false;
                    if ($account_id == $c['account_id']){
                        if ($c['un_authenticated']){
                            $c['un_authenticated'] = false;
                            $is_valid = !$c['tried'] && $highest_tried == $c['call_order'];
                        }else{
                            $is_valid = !$c['tried'] && $highest_tried == $c['call_order'] - 1;
                        }
                    }
                }
            }else{
                $c['un_authenticated'] = true;
            }
        }
        $callQueue->possible_candidates = json_encode($candidates);
        $callQueue->save();
        return $is_valid;
    }

	/**
	 * Create a call queue record to keep track of
	 * a patient initiated call
	 * 
	 * @return boolean
	 */
	public function placePatientCall()
	{
		if ($this->callQueueId == NULL)
		{
			$callQueue = Model::factory('CallQueue');
		}
		else
		{
			$callQueue = Model::factory('CallQueue')->
				where('id', '=', $this->callQueueId)->
				find();
		}
        $callQueue->call_status = 'initiated';
		$callQueue->from_account_id = $this->fromAccount->id;
		$callQueue->from_as_patient = $this->fromAsPatient || ($this->fromPrescriptionId != null);
		$callQueue->from_prescription_id = $this->fromPrescriptionId;
		$callQueue->to_account_id = $this->toAccount ? $this->toAccount->id : null;
		$callQueue->to_store_id = $this->toStore->id;
		$callQueue->origin = $this->origin;
		// Create the session/token for tokbox

		list($sessionId, $token) = $this->generateTokBoxInfo();

		$callQueue->session_id = $sessionId;
		$callQueue->token = $token;
        $callQueue->is_pending = true;
		$callQueue->save();

		$this->toPharmacist = true;
		$this->callQueueId = $callQueue->id;
        $this->generatePossibleDestinations();
		return true;
	}

    public function placePatientCallToSpecificPharmacist($pharmacist_id){
        if ($this->callQueueId == NULL)
        {
            $callQueue = Model::factory('CallQueue');
        }
        else
        {
            $callQueue = Model::factory('CallQueue')->
                where('id', '=', $this->callQueueId)->
                find();
        }
        $callQueue->call_status = 'initiated';
        $callQueue->from_account_id = $this->fromAccount->id;
        $callQueue->from_as_patient = true;
        $callQueue->from_prescription_id = null;
        $callQueue->to_account_id = $pharmacist_id;
        $callQueue->to_store_id = null;
        $callQueue->origin = $this->origin;
        // Create the session/token for tokbox

        list($sessionId, $token) = $this->generateTokBoxInfo();

        $callQueue->session_id = $sessionId;
        $callQueue->token = $token;
        $callQueue->is_pending = true;
        $callQueue->save();

        $this->toPharmacist = true;
        $this->callQueueId = $callQueue->id;
        $user = new CurrentUser($this->fromAccount);
        $message = "call from patient";
        Model_Notification::publishToAccount($user,
            'incoming-call', $message,
            $pharmacist_id, $callQueue->untaint());
        //$this->generatePossibleDestinations();
        return true;
    }

    public function placeNextPharmacistCall($is_from_technician){
        if ($this->callQueueId == NULL)
        {
            $callQueue = Model::factory('CallQueue');
        }
        else
        {
            $callQueue = Model::factory('CallQueue')->
                where('id', '=', $this->callQueueId)->
                find();;
        }
        list($sessionId, $token) = $this->generateTokBoxInfo();
        $callQueue->session_id = $sessionId;
        $callQueue->token = $token;
        $callQueue->call_status = 'initiated';
        $callQueue->from_account_id = $this->fromAccount->id;
        $callQueue->from_as_patient = !$is_from_technician;
        $callQueue->from_prescription_id = $this->fromPrescriptionId;
        $callQueue->to_account_id = null;
        $callQueue->to_store_id = $this->toStore->id;
        $callQueue->origin = $this->origin;
        $callQueue->save();

        $this->toPharmacist = true;
        $this->callQueueId = $callQueue->id;

        $this->generatePossibleDestinations();

        return true;
    }

	/**
	 * Create a call queue record to keep track of
	 * a patient initiated call
	 * 
	 * @return boolean
	 */
	public function placePharmacistCall()
	{

		if ($this->callQueueId == NULL)
		{
			$callQueue = Model::factory('CallQueue');
		}
		else
		{
			$callQueue = Model::factory('CallQueue')->
				where('id', '=', $this->callQueueId)->
				find();;
		}

        $callQueue->call_status = 'initiated';
		$callQueue->from_account_id = $this->fromAccount->id;
		$callQueue->from_as_patient = false;
		$callQueue->from_prescription_id = false;
		$callQueue->to_account_id = null;
		$callQueue->to_store_id = $this->toStore->id;
		$callQueue->origin = $this->origin;
		$callQueue->save();
		$this->toPharmacist = false;
		$this->callQueueId = $callQueue->id;

        $this->generatePossibleDestinations();

        return true;
	}

	/**
	 * Check if the call should be tried with
	 * other pharmacist users based on the store
	 * 
	 * @return boolean
	 */
	public function shouldTryOthers()
	{
		return ($this->toAccount == null &&
			$this->toStore->loaded());
	}

	/**
	 * Destination user accept the call
	 *
	 * @return boolean
	 */
	public function accept()
	{
		$callQueue = $this->getCallQueue();

		$callQueue->is_pending = false;
		$callQueue->call_status = 'accepted';

		if (!$callQueue->from_as_patient)
		{
			// Create the session/token for tokbox

			list($sessionId, $token) = $this->generateTokBoxInfo();

			$callQueue->session_id = $sessionId;
			$callQueue->token = $token;
		}

        $now = new DateTime();
        $callQueue->accepted_dt = $now->format('Y-m-d H:i:s');
		$callQueue->save();

		return true;
	}

	/**
	 * Destination user dismiss the call
	 *
	 * @return boolean
	 */
	public function dismiss()
	{
		$callQueue = $this->getCallQueue();
		if (!$this->haveMoreCandidates(true))
		{
			$callQueue->is_pending = false;
			$callQueue->call_status = 'canceled';
			$callQueue->cancel_reason = 'to';
		}
		else
		{
			$this->toAccount = null;
			$callQueue->is_pending = true;
			$callQueue->to_account_id = null;
			$callQueue->call_status = 'initiated';
			$callQueue->cancel_reason = null;
		}

		$callQueue->save();

		return true;
	}

	/**
	 * Cancel a call b/c there was no answer
	 * by any possible user
	 * 
	 * @return boolean
	 */
	public function cancel()
	{
		$callQueue = $this->getCallQueue();

		if (!$this->haveMoreCandidates(true))
		{
			$callQueue->is_pending = false;
			$callQueue->call_status = 'canceled';
			$callQueue->cancel_reason = 'no answer';
		}
		else
		{
			$this->toAccount = null;
			$callQueue->is_pending = true;
			$callQueue->to_account_id = null;
			$callQueue->call_status = 'initiated';
			$callQueue->cancel_reason = null;
		}

		$callQueue->save();
	
		return true;
	}

	/**
	 * hang up the call
	 * 
	 * @param integer $accountId 
	 * @param boolean $withError
	 * 
	 * @return boolean
	 */
	public function hangUp($accountId, $withError = false)
	{
		$callQueue = $this->getCallQueue();
		if ($callQueue->complete_status != 'incomplete')
			throw new Exception('Call already Completed');

		if ($accountId == $this->fromAccount->id)
			$callQueue->cancel_reason = 'from';
		if ($accountId == $this->toAccount->id)
			$callQueue->cancel_reason = 'to';
		if ($withError)
			$callQueue->cancel_reason = 'error';

		$callQueue->complete_status = $withError ? 'error' : 'complete';
        $now = new DateTime();
        if (!isset($callQueue->end_dt)){
            $callQueue->end_dt = $now->format('Y-m-d H:i:s');
        }
		$callQueue->save();

		return true;
	}

	/**
	 * Generate the session and token on tokbox
	 * to allow two users to comunicate using them
	 * 
	 * @return array
	 */
	private function generateTokBoxInfo()
	{
		$class = new $this->tokenGeneratorClass;
		return $class->generate();
	}


	/**
	 * Get a call queue record
	 * 
	 * @return Model_CallQueue
	 */
	public function getCallQueue()
	{
		$callQueue = Model::factory('CallQueue')->
			where('id', '=', $this->callQueueId)->
			find();

		if (!$callQueue->loaded())
			throw new Exception('CallQueue not found');


		return $callQueue;
	}

	/**
	 * Generate a list of possible pharmacists
	 * that can pick up the call
	 * 
	 * @return array
	 */
	private function getPossibleDestinations($seeded)
	{
		if (! $this->shouldTryOthers())
			throw new Exception('This should be used only for calls from patient');

		$c = [];

		if ($seeded)
		{
			if (!is_array($seeded)) $seeded = [$seeded];

			foreach ($seeded as $s)
			{
				$c[]= new Model_Account($s);
			}
		}
		else
		{
			$roleToCall = $this->toPharmacist ?
				'Pharmacist':
				'Technician';

			$callingToRole = Model::factory('Role')->
				where('type', '=', 'store')->
				where('name', '=', $roleToCall)->
				find();

			if (! $callingToRole->loaded())
				throw new Exception('calling role does not exist');

			foreach( Model::factory('StoreRole')->
				where('role_id', '=', $callingToRole->id)->
				where('store_id', '=', $this->toStore->id)->
				order_by('account_id', 'ASC')->
				find_all() as $sr)
			{
				$c[]= $sr->account;
			}
		}

//		foreach ($c as $candidate)
//		{
////            $web_session = Model_AccountToken::fetch($candidate->id, Model_AccountSessionType::Web);
////			if ($web_session->is_authenticated())
////			{
//				$candidates[]= $candidate;
////			}
//		}

		return $c;
	}

	/**
	 * place a call and save possible_destinations
	 * json array with [
	 *    {
	 *      account_id: 1,
	 *      tried: false
	 *    }
	 * ]
	 * 
	 * @return void
	 */
	public function generatePossibleDestinations($destinations = null)
	{
		$candidates = $this->getPossibleDestinations($destinations);
		
		if (count($candidates) == 0) 
			return false;

		$callQueue = $this->getCallQueue();

		$array_candidates = [];
		foreach($candidates as $i=>$c)
		{
			$array_candidates[] = [
				'account_id' => intval($c->id),
				'tried' => false,
                'call_order' => $i,
                 'un_authenticated'=>false,
                'on_call'=>false
			];
		}
		$callQueue->possible_candidates = json_encode($array_candidates);
		$callQueue->save();

		return true;
	}

    public function checkTried($account_id){
        $callQueue = $this->getCallQueue();

        if (!$callQueue->possible_candidates)
        {
            return false;
        }

        $candidates = json_decode($callQueue->possible_candidates, true);

        $nextCandidate = null;
        foreach($candidates as &$c)
        {
            if ($c['account_id'] == $account_id)
            {
                return $c['tried'];
            }
        }
        return false;
    }

	/**
	 * Check if a the call have more candidates
	 * 
	 * @return bool
	 */
	public function haveMoreCandidates($canceled = false)
	{
		$callQueue = $this->getCallQueue();
		if ($callQueue->possible_candidates == NULL) return false;
		$candidates = json_decode($callQueue->possible_candidates, true);
		$nextCandidate = null;
        $still_active = false;
		foreach($candidates as &$c)
		{
            $candidate_account = new Model_Account($c['account_id']);
            if ($candidate_account->web_session->is_authenticated()
                && $candidate_account->is_available
                && $candidate_account->is_active
                && !$c['tried']
                && !$this->checkCurrentCall($c['account_id'])){
                //if (intval($c['attempts']) < count($candidates) * self::num_attempts){
                $still_active = true;
                //}
            }
            if ($canceled && $callQueue->to_account_id == $c['account_id']){
                $c['tried'] = true;
            }
		}
        $callQueue->possible_candidates = json_encode($candidates);
        $callQueue->save();
        return $still_active;
	}

	/**
	 * Get a list of incoming calls to the toAccount
	 * 
	 * @return array
	 */
	public function getIncomingCalls()
	{
		$incomingCalls = Model::factory('CallQueue')->
			with('from_account')->
			where('to_account_id', '=', $this->toAccount->id)->
			where('is_pending', '=', true)->
			find_all();
		$result = $incomingCalls->untaint();
		foreach($result as &$call)
		{
			$call['from_name'] = Model::factory('Account')->
				where('id', '=', $call['from_account_id'])->
				find()->full_name;
			$to = Model::factory('Account')->
				where('id', '=', $call['to_account_id'])->
				find();
			$call['to_name'] = $to->loaded() ? $to->full_name : '';
		}
		return $result;
	}


	/**
	 * check if a call was answered
	 * 
	 * @return boolean
	 */
	public function wasAnswered()
	{
		$callQueue = $this->getCallQueue();
		return $callQueue->call_status == 'accepted';
	}

    /**
     * check if a call was dismissed
     *
     * @return boolean
     */
    public function wasDismissed()
    {
        $callQueue = $this->getCallQueue();
        return $callQueue->call_status == 'canceled';
    }


    /**
	 * check if a call was already completed or not
	 * 
	 * @return boolanc
	 */
	public function readyToTalk()
	{
		$callQueue = $this->getCallQueue();
		return $callQueue->call_status == 'accepted' && $callQueue->complete_status == 'incomplete';
	}


	/*
	 * set un-answered calls as error
	 */
	public static function purgeUnAnsweredCalls()
	{

	}
}
