<?php

class TelePharm_HTMLForm_Generic extends TelePharm_HTMLForm
{
	public function setAction($action)
	{
		$this->action = $action;
	}
}

class TelePharm_DataProvider implements TelePharm_Untainted
{
	protected $modelName = null;
	protected $model = null;

	public function __construct($modelName)
	{
		$this->modelName = $modelName;
		$this->model = Model::factory($modelName);
	}

	public function createForm($action, $id=null)
	{
		$data = [];
		if (isset($id)) $data = $this->model->where('id','=',$id)->find()->as_array();
		$columns  = $this->model->table_columns();
		$form = new TelePharm_HTMLForm_Generic();
		$form->setAction($action);
		foreach($columns as $name => $details)
		{
			if (in_array($name, ["deleted_dt", "created_dt", "updated_dt"]))
			{
				continue;
			}
			if ($details['extra'] != 'auto_increment')
			{
				$field = new TelePharm_Field_Text($name);
				$field->required = $details['is_nullable'];
				$form->addField($field);
			}
			else
			{
				$field = new TelePharm_Field_Hidden($name);
				$form->addField($field);
			}
		}
		$form->addField(new TelePharm_Field_Submit('Save'));
		$form->load($data);
		return $form;
	}

	public function getColumns()
	{
		return $this->model->table_columns();
	}

	public function getRows()
	{
		return Model::factory($this->modelName)->find_all()->as_array();
	}

	/* This two methods receive a validated form*/
	public function create($form)
	{
		$record = Model::factory($this->modelName);

		$record->values($form->as_array());

		$record->save();

		return $record->saved();
	}

	public function update($form)
	{
		$record = Model::factory($this->modelName)->
			where('id', '=', $form['id']->saneValue)->
			find();

		$record->values($form->as_array());

		$record->save();

		return $record->saved();
	}

	public function delete($id)
	{
		$record = Model::factory($this->modelName)->
			where('id', '=', $id)->
			find();

		$columns = $record->table_columns();
		if (array_key_exists('deleted_dt', $columns))
		{
			$record['deleted_dt'] = gmdate('Y-m-d H:i:s');
			$record->save();
		}
		else
		{
			$record->delete();
		}

		return $record->saved();
	}
	
}
