<?php
/**
 * Formats date time strings. 
 *
 * Useful as a global view object:
 * 
 * $this->template->set_global('dtFormatter', new DateTimeFormatter(
 *   $this->account ? new DateTimeZone($this->account->timezone) : null)
 * );
 *
 * In the view:
 *
 * <?= $dtFormatter('2012-01-01 12:32:21', DateTimeFormater::DATE_TIME) ?>
 */
class TelePharm_DateTimeFormatter implements TelePharm_Untainted
{
	const DB = 'Y-m-d H:i:s';
	const DATE_TIME = 'M jS Y g:i A';
	const DATE = 'M jS Y';
	const EXPIRATION = 'F Y';

	protected $localTz;

	/**
	 * The constructor sets the local time zone.
	 *
	 * @param DateTimeZone $localTz  The local time zone to use when formatting 
	 *                               UTC time strings.
	 */
	public function __construct(DateTimeZone $localTz = null)
	{
		$this->localTz = $localTz ?: new DateTimeZone(date_default_timezone_get());
	}

	/**
	 * Formats a date time string.
	 *
	 * The assumption is that date time strings in the format of YYYY-MM-DD HH:MM:SS
	 * are stored in UTC time. In that case, dates will be converted to the local
	 * time zone, and then formatted per the format string.
	 *
	 * @param string $dt        The string to format. Will be passed to DateTime's
	 *                          constructor, so it must be a valid string. 
	 * @param string $format    The format to use, using date()'s syntax. (Optional)
	 * @param string $localize  Convert from UTC to the user's time. (Optional)
	 *
	 * @return string           The formatted string
	 */
	public function format($dt, $format = DateTimeFormatter::DB, $localize = null)
	{
		if (!$dt) return '';

		if ($localize === null)
			$localize = strlen($dt) == 19;

		if ($localize)
		{
			$dt = new DateTime($dt.' UTC');
			$dt->setTimeZone($this->localTz);
		}
		else
		{
			$dt = new DateTime($dt);
		}

		return $dt->format($format);
	}

	/**
	 * Alias for format()
	 */
	public function __invoke($dt, $format = DateTimeFormatter::DB, $localize = null)
	{
		return $this->format($dt, $format, $localize);
	}
}