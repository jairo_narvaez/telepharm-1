<?php
class TelePharm_DateTimeZone
{
	static $timezones = [];

	# Time zones are immutable. Use TelePharm_DateTimeZone::factory to cache timezones
	# to prevent unnecessary duplicates.
	static public function factory($tz)
	{
		if (!isset(self::$timezones[$tz]))
		{
			self::$timezones[$tz] = new DateTimeZone($tz);
		}

		return self::$timezones[$tz];
	}
}