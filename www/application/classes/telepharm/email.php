<?php
include('Mail.php');
include('Mail/mime.php');

class TelePharm_Email
{
	private $to = array();
	private $cc = array();
	private $bcc = array();
	private $from;
	private $subject;
	private $html;
	private $text;
	private $tag;
	private $replyTo;
	
	public function __get($key)
	{
		switch ($key)
		{
			case 'to':
				return implode(',', $this->to);
				
			case 'cc':
				return implode(',', $this->cc);
				
			case 'bcc':
				return implode(',', $this->bcc);
			
			case 'tag':
			case 'from':
			case 'subject':
			case 'html':
			case 'text':
			case 'replyTo':
				return $this->$key;
		}
		
		throw new Exception("Invalid property $key");
	}
	
	public static function compose()
	{
		return new self();
	}
	
	public function to($email, $name = null)
	{
		$this->to = array();
		return $this->addRecipient($email, $name);
	}
	
	public function cc($email, $name = null)
	{
		$this->cc = array();
		return $this->addCc($email, $name);
	}	
	
	public function bcc($email, $name = null)
	{
		$this->bcc = array();
		return $this->addBcc($email, $name);
	}
	
	public function from($email, $name = null)
	{
		$this->from = $this->createAddress($email, $name);
		return $this;
	}

	public function replyTo($email, $name = null)
	{
		$this->replyTo = $this->createAddress($email, $name);
		return $this;
	}		
		
	public function subject($subject)
	{
		$this->subject = $subject;
		return $this;
	}
	
	public function html($html)
	{
		$this->html = $html;
		return $this;
	}
	
	public function text($text)
	{
		$this->text = $text;
		return $this;
	}
	
	public function tag($tag)
	{
		$this->tag = $tag;
		return $this;
	}
	
	public function addRecipient($email, $name = null)
	{
		$this->to[] = $this->createAddress($email, $name);
		return $this;
	}
	
	public function addCc($email, $name = null)
	{
		$this->cc[] = $this->createAddress($email, $name);
		return $this;
	}
	
	public function addBcc($email, $name = null)
	{
		$this->bcc[] = $this->createAddress($email, $name);
		return $this;
	}
	
	private function createAddress($email, $name = null)
	{
		return $name ? '"'.str_replace('"','',$name).'" <'.$email.'>' : $email;
	}
	
	public function encode()
	{
		# Mail_Mime triggers some strict notices, so disable them.		
		$er = error_reporting();
		error_reporting($er & ~E_STRICT);
		try {
			$mime = new Mail_mime("\n");
			
			$headers = array();
			
			if ($this->from) $headers['From'] = $this->from;
			if ($this->to) $headers['To'] = $this->__get('to');
			if ($this->cc) $headers['Cc'] = $this->__get('cc');
			if ($this->bcc) $headers['Bcc'] = $this->__get('bcc');			
			if ($this->replyTo) $headers['Reply-To'] = $this->__get('replyTo');	
			if ($this->subject) $headers['Subject'] = $this->subject;
			
			if ($this->text) $mime->setTXTBody($this->text);
			if ($this->html) $mime->setHTMLBody($this->html);
		
			$body = $mime->get();
			$headers = $mime->headers($headers);
		}
		catch (Exception $e)
		{
			error_reporting($er);
			throw $e;
		}
		
		error_reporting($er);
		
		$msg = '';
		foreach ($headers as $key => $val)
			$msg .= $key.': '.$val."\n";
		
		return "$msg\n$body";		
	}
}