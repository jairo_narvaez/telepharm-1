<?php
abstract class TelePharm_Email_Transport
{
	abstract public function send(TelePharm_Email $mail);
	
	public static function factory($name = null)
	{
		if (!$name)
		{
			$name = Kohana::$config->load('application.email_transport');
			if (!$name) throw new Exception("No transport type specified");
		}
		
		$classname = 'TelePharm_Email_Transport_'.$name;
		$reflector = new ReflectionClass($classname);
		return $reflector->newInstance();
	}	
}
