<?php
class TelePharm_Email_Transport_File extends TelePharm_Email_Transport
{
	public function send(TelePharm_Email $email)
	{
		$directory = APPPATH.'logs'.DIRECTORY_SEPARATOR.'emails';
		if ( ! is_dir($directory) OR ! is_writable($directory))
		{
			throw new Kohana_Exception('Directory :dir must be writable',
				array(':dir' => Debug::path($directory)));
		}

		$directory = realpath($directory).DIRECTORY_SEPARATOR;

		// Set the name of the log file
		$filename = $directory.DIRECTORY_SEPARATOR.date('Y-m-d').'-'.
			url::title($email->to. ' ' .$email->subject).'.eml';

		if (! file_exists($filename) )
		{
			file_put_contents($filename, '');
			// Allow anyone to write to log files
			chmod($filename, 0666);
		}

		file_put_contents($filename, $email->encode());

		return $this;
	}
}

