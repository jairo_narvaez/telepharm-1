<?php
/**
 * A layer on top of another transport.
 *
 * Useful for testing on a staging server where you want all emails to go
 * to a single testing address.
 */
class TelePharm_Email_Transport_Hook extends TelePharm_Email_Transport
{
	private $inner_transport;

	public function __construct()
	{
		$config = Kohana::$config->load('application.email_transport_hook');
		if (!$config)
		{
			throw new Exception("Missing application.email_transport_hook");
		}

		if (!isset($config['inner_transport']))
		{
			throw new Exception("Missing application.email_transport_hook.inner_transport");
		}

		if (!isset($config['to']))
		{
			throw new Exception("Missing application.email_transport_hook.to");
		}

		$this->inner_transport = self::factory($config['inner_transport']);
		$this->to = $config['to'];
	}

	public function send(TelePharm_Email $email)
	{	
		$this->inner_transport->send(
			TelePharm_Email::compose()->
				to($this->to)->
				tag($email->tag)->
				from($email->from)->
				subject($email->subject)->
				html($email->html)->
				text($email->text)->
				replyTo($email->replyTo)
		);

		return $this;
	}
}