<?php
class TelePharm_Email_Transport_Log extends TelePharm_Email_Transport
{
	public function send(TelePharm_Email $email)
	{	
		Kohana::$log->add(TelePharm_KohanaCompat::logLevel(LOG_NOTICE), $email->encode());
		return $this;
	}
}