<?php
class TelePharm_Email_Transport_Postmark extends TelePharm_Email_Transport
{
	public function send(TelePharm_Email $msg)
	{
		# build data
		$data = array(
			'From' => $msg->from,
			'To' => $msg->to,
			'Cc' => $msg->cc,
			'ReplyTo' => $msg->replyTo,
			'Bcc' => $msg->bcc,
			'Subject' => $msg->subject,
			'Tag' => $msg->tag			
		);
		
		if ($msg->text) $data['TextBody'] = $msg->text;
		if ($msg->html) $data['HtmlBody'] = $msg->html;
				
		# check for required fields
		if (!$data['Subject']) throw new Exception("No subject was specified.");
		if (!$data['To']) throw new Exception("No recipient was specified.");
		if (!$msg->text && !$msg->html) throw new Exception("No body was specified.");
		
		# use default parameters for missing fields
		if (!$data['From']) $data['From'] = '"'.Kohana::$config->load('postmark.from_address').'"';
		
		# remove optional fields
		if (!$data['Cc']) unset($data['Cc']);
		if (!$data['ReplyTo']) unset($data['ReplyTo']);
		if (!$data['Bcc']) unset($data['Bcc']);	
		if (!$data['Tag']) unset($data['Tag']);
		
		Kohana::$log->add(TelePharm_KohanaCompat::logLevel(LOG_INFO), "sending: ".json_encode($data));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.postmarkapp.com/email');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Accept: application/json',
			'Content-Type: application/json',
			'X-Postmark-Server-Token: ' . Kohana::$config->load('postmark.api_key')
		));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
#		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
#		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
#		curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/Certificate/cacert.pem');

		$return = curl_exec($ch);
		$curlError = curl_error($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if ($curlError)
		{
			throw new Exception($curlError);
		}
		else if ($httpCode == 422)
		{
			$return = json_decode($return);
			if ($return)
				throw new Exception($return->Message, $return->ErrorCode);
			else
				throw new Exception("Postmark returned 422 along with an invalid response");
		}
		else if ($httpCode < 200 || $httpCode > 299)
		{
			throw new Exception("An error occurred communicating with Postmark. HTTP $httpCode; $return");
		}
		
		return $this;
	}
}