<?php
class TelePharm_Email_Transport_Sendmail extends TelePharm_Email_Transport
{
	private $sendmail;
	
	public function __construct($path = null)
	{
		if (!$path) $path = ini_get('sendmail_path');
		
		if (!$path) throw new Exception("no sendmail path specified");
		
		$this->sendmail = $path;
	}
	
	public function send(TelePharm_Email $email)
	{		
		$p = proc_open($this->sendmail, array(array('pipe', 'r'), array('pipe', 'w')), $pipes);
		if (!is_resource($p))
			throw new Exception("unable to open sendmail");
			
		fwrite($pipes[0], $email->encode());
		
		fclose($pipes[0]);
		fclose($pipes[1]);
		
		$return_value = proc_close($p);
		if ($return_value) throw new Exception("Unable to send message.");
		
		return $this;
	}
}