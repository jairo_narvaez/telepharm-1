<?php
class Telepharm_ErrorMessage {

    public function __construct($message, $severity){
        $this->message = $message;
        $this->severity = $severity;
    }

    public static function apply_prefix($error_messages, $prefix){
        foreach ($error_messages as $error_message){
            $error_message->message = $prefix . ': ' . $error_message->message;
        }
        return $error_messages;
    }
}