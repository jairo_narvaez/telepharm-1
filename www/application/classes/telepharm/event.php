<?php
class TelePharm_Event
{
	use \traits\TelePharm_Getter;

	protected $type;
	protected $source;
	protected $cancelable = true;
	protected $canceled = false;

	public function __construct($type, $source = null)
	{
		$this->type = $type;
		$this->source = $source;
	}

	protected function getType()
	{
		return $this->type;
	}

	protected function getSource()
	{
		return $this->source;
	}

	protected function getCanceled()
	{
		return $this->canceled;
	}

	public function cancel()
	{
		if ($this->cancelable)
			$this->canceled = true;

		return $this;
	}
}