<?php
/**
 * Field for representing credit card expiration date
 */
class TelePharm_Field_CreditCardExpiration extends TelePharm_Field_Object
{
	protected $defaultTemplate = 'creditcardexpiration';

	/**
	 * Constructor adds two fields for month/year.
	 */
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
	
		$months = [];
		for ($i=1; $i<=12; $i++) 
			$months[$i] = str_pad($i, 2, '0', STR_PAD_LEFT);

		$years = [];
		for ($i=date('Y'); $i <= date('Y') + 5; $i++)
			$years[$i] = $i;

		$this->addField(new TelePharm_Field_Select('month', ['options' => $months]));
		$this->addField(new TelePharm_Field_Select('year', ['options' => $years]));
	}

	/**
	 * Set the default value for the field.
	 *
	 * @param $value string|array   If string, it must be in the format of YYYY-MM. Extraneous
	 *                              characters are ignored. If array, then it most be in the form
	 *                              of  [ 'month' => int, 'year' => int ].
	 */
	protected function setValue($value)
	{
		if (is_string($value))
		{
			if (preg_match('/^(\d{4})-(\d{2})/', $value, $m))
			{
				$this['month']->value = $m[2];
				$this['year']->value = $m[1];
			}
		}
		else if (is_array($value))
		{
			parent::setValue($value);
		}
	}

	/**
	 * If the field is required, then both month/year are required.
	 */
	protected function setRequired($value)
	{
		$this['month']->required = $value;
		$this['year']->required = $value;
	}

	/**
	 * 
	 */
	protected function getSaneValue()
	{
		$month = $this['month']->saneValue;
		$year = $this['year']->saneValue;

		return ($month && $year) ? sprintf("%04d-%02d-01", $year, $month) : null;
	}
}