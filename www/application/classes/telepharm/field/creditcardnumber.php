<?php
class TelePharm_Field_CreditCardNumber extends TelePharm_Field_Text
{
	protected $defaultTemplate = 'creditcard';
	protected $hideValue = true;

	public function __construct($id, array $attr = [])
	{
		parent::__construct($id, $attr);
		$this->rules->add(new TelePharm_Rule_LuhnChecksum());
	}

	protected function getSaneValue()
	{
		return $this->value == 'x' ? '' : $this->value;
	}

	protected function getChanged()
	{
		return $this->value != 'x';
	}

	protected function setHideValue($value)
	{
		$this->hideValue = $value ? true : false;
	}

	protected function getHideValue()
	{
		return $this->hideValue && $this->value != '';
	}

}