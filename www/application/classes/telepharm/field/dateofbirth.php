<?php
class TelePharm_Field_DateOfBirth extends TelePharm_Field_Date
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this['class'] = 'date dob';
	}
}