<?php
class TelePharm_Field_Email extends TelePharm_Field_Text
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this->rules->add(new TelePharm_Rule_Email());
	}
}