<?php
class TelePharm_Field_Height extends TelePharm_Field_Group
{
	protected $defaultTemplate = 'height';

	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this->addField(new TelePharm_Field_Text('feet', ['type' => 'integer']));
		$this->addField(new TelePharm_Field_Text('inches', ['type' => 'integer']));

		$this->rules->add(function()
		{
			return ($this['feet']->saneValue < 0 || $this['feet']->saneValue > 7 ||
				$this['inches']->saneValue < 0 || $this['inches']->saneValue > 11) ?
				new TelePharm_RuleResult_Failure('out-of-range') : new TelePharm_RuleResult_Success();
		});
	}

	protected function setValue($value)
	{
		if (is_string($value))
		{
			if (!preg_match('/^(\d+)\'( (\d+)")?$/', $value, $m))
				return;

			$feet = $m[1];
			$inches = isset($m[3]) ? $m[3] : '0';

			$value = ['feet' => $feet, 'inches' => $inches];
		}

		parent::setValue($value);
	}

	protected function getSaneValue()
	{
		$value = $this['feet']->saneValue."'";
		if ($this['inches']->saneValue) $value .= " ".$this['inches']->saneValue.'"';
		return $value;
	}
}