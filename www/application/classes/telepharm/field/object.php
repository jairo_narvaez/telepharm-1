<?php
class TelePharm_Field_Object extends TelePharm_Field implements ArrayAccess
{
	protected $fields = [];
	protected $defaultTemplate = 'object';

	public function __construct($id, array $fields = [])
	{
		parent::__construct($id);

		foreach ($fields as $field)
			$this->addField($field);
	}

	protected function getFields()
	{
		return $this->fields;
	}

	public function offsetExists($key)
	{
		return isset($this->fields[$key]);
	}

	public function offsetSet($key, $val)
	{
		throw new Exception("Read only");
	}

	public function offsetUnset($key)
	{
		unset($this->fields[$key]);
	}

	public function offsetGet($key)
	{
		return $this->fields[$key];
	}

	protected function getSaneValue()
	{
		$values = [];
		foreach ($this->fields as $field)
			$values[$field->id] = $field->saneValue;

		return $values;
	}

	public function addField(TelePharm_Field $field)
	{
		$this->fields[$field->id] = $field;

		$field->parent = $this;
	}

	public function addFields(array $fields)
	{
		foreach ($fields as $field)
			$this->addField($field);
	}

	protected function setValue($values)
	{
		if (is_array($values))
		{
			# Note: must explicitly call setter function here because the 
			#       value property is accessible since this calls extends TelePharm_Field
			foreach ($values as $key => $val)
			{
				if (isset($this->fields[$key]))
					$this->fields[$key]->setValue($val);
			}
		}
	}

	public function transformName(TelePharm_Field $field, $name)
	{
		preg_match('/^([^[]+)(.*)$/', $name, $matches);
		$name = $this->id.'['.$matches[1].']'.$matches[2];
		return $this->parent ? $this->parent->transformName($this, $name) : $name;
	}

	/**
	 * Returns an array of exported rules from each of the fields contained in
	 * the object.
	 *
	 * @return array [['fieldId' => string, 'rule' => TelePharm_Rule], ... ]
	 */
	public function exportRules()
	{
		$rules = [];
		foreach ($this->fields as $field)
		{
			array_splice($rules, count($rules), 0, $field->exportRules());
		}

		return $rules;
	}

	public function validate()
	{
		$valid = true;

		foreach ($this->fields as $field)
		{
			if (!$field->validate())
				$valid = false;
		}

		return $valid;
	}

	public function __clone()
	{
		foreach ($this->fields as $uuid => $field)
		{
			$this->fields[$uuid] = clone $field;
			$this->fields[$uuid]->parent = $this;
		}
	}
}