<?php

class TelePharm_Field_Phone extends TelePharm_Field_Text
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this['class'] = 'phone';
	}
}
