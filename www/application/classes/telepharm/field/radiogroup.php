<?php

class TelePharm_Field_RadioGroup extends TelePharm_Field_Visible
{
	protected $options;
	protected $defaultTemplate = 'radiogroup';

	public function __construct($id, $attr = [])
	{
		$attr = array_merge(['options' => []], $attr);
		parent::__construct($id, $attr);
		
		$this->options = $attr['options'];
	}

	protected function getOptions()
	{
		return $this->options;
	}

	protected function getSaneValue()
	{
		return array_key_exists($this->value, $this->options) ? $this->value : null;
	}
}