<?php

class TelePharm_Field_Select extends TelePharm_Field_Visible
{
	protected $options;
	protected $nullItem;
	protected $defaultTemplate = 'select';

	public function __construct($id, $attr = [])
	{
		$attr = array_merge(['options' => [], 'nullItem' => null], $attr);
		parent::__construct($id, $attr);
		
		$this->options = $attr['options'];
		$this->nullItem = $attr['nullItem'];
	}

	protected function getOptions()
	{
		return $this->options;
	}

	protected function getNullItem()
	{
		return $this->nullItem;
	}

	protected function getSaneValue()
	{
		return array_key_exists($this->value, $this->options) ? $this->value : null;
	}

	public function render($attributes = [])
	{
		$nullItem = $this->nullItem;
		if (isset($attributes[':nullItem']))
			$this->nullItem = $attributes[':nullItem'];

		parent::render($attributes);

		$this->nullItem = $nullItem;
	}
}