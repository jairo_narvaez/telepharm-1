<?php
class TelePharm_Field_Template extends TelePharm_Field implements ArrayAccess
{
	protected $template;
	protected $fields = [];
	protected $defaultTemplate = 'template';

	public function __construct(TelePharm_Field $template)
	{
		parent::__construct($template->id);
		$this->template = $template;
		$template->parent = $this;
	}

	public function offsetExists($key)
	{
		return isset($this->fields[$key]);
	}

	public function offsetGet($key)
	{
		return $this->fields[$key];
	}

	public function offsetSet($key, $val)
	{
		$this->addRecord($key, $val);
	}

	public function offsetUnset($key)
	{
		unset($this->fields[$key]);
	}

	public function addRecord($uuid, $value)
	{
		$field = clone $this->template;
		$field->parent = $this;
		if ($value !== null)
			$field->setValue($value);

		$this->fields[$uuid] = $field;		
	}

	public function addRecords($records)
	{
		foreach ($records as $key => $val)
			$this->addRecord($key, $val);
	}

	protected function getTemplate()
	{
		return $this->template;
	}

	protected function getFields()
	{
		return $this->fields;
	}

	protected function getValue()
	{
		$values = [];
		foreach ($this->fields as $uuid => $field)
		{
			$values[$uuid] = $field->value;
		}

		return $values;
	}

	protected function getSaneValue()
	{
		$values = [];
		foreach ($this->fields as $uuid => $field)
		{
			$values[$uuid] = $field->saneValue;
		}

		return $values;
	}

	protected function setValue($values)
	{
		if (is_array($values))
		{
			foreach ($values as $key => $val)
			{
				if (!isset($this->fields[$key]))
				{
					$this->addRecord($key, $val);
				}
				else
				{
					$this->fields[$key]->setValue($val);
				}
			}
		}
	}

	public function transformName(TelePharm_Field $field, $name)
	{
		if ($field === $this->template)
		{
			$uuid = '_tpl_';
		}
		else
		{
			$uuid = '_undefined_';
			foreach ($this->fields as $key => $val)
			{
				if ($val === $field)
				{
					$uuid = $key;
					break;
				}
			}
		}

		preg_match('/^([^[]+)(.*)$/', $name, $matches);
		$name = $this->id.'['.$uuid.']'.$matches[2];

		return $this->parent ? $this->parent->transformName($this, $name) : $name;
	}
}