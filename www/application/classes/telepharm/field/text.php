<?php

class TelePharm_Field_Text extends TelePharm_Field_Visible
{
	protected $type;
	protected $defaultTemplate = 'text';

	public function __construct($id, $attr = [])
	{
		$attr = array_merge(['type' => 'string'], $attr);
		parent::__construct($id, $attr);
		$this->type = $attr['type'];
	}

	protected function getType()
	{
		return $this->type;
	}

/*
	public function renderField()
	{
		echo '<input name="'.htmlspecialchars($this->transformedName).'" type="text" value="'.htmlspecialchars($this->value).'"';
		$this->renderHTMLAttributes();
		echo ' />';
	}
*/

	protected function getSaneValue()
	{
		$filter = new TelePharm_InputFilter([$this->id => $this->value]);
		$filter->sanitize([$this->id => $this->type]);
		return $filter[$this->id];
	}
}
