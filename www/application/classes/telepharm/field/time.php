<?php
class TelePharm_Field_Time extends TelePharm_Field_Text
{
	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this['class'] = 'time';
	}
}