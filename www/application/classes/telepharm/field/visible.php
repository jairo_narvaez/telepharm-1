<?php
abstract class TelePharm_Field_Visible extends TelePharm_Field implements ArrayAccess
{
	protected $name;

	public function __construct($id, $attr = [])
	{
		parent::__construct($id, $attr);
		$this->name = $id;
	}

	public function offsetExists($key)
	{
		return array_key_exists($key, $this->htmlAttributes);
	}

	public function offsetSet($key, $val)
	{
		$this->htmlAttributes[$key] = $val;
	}

	public function offsetGet($key)
	{
		return $this->htmlAttributes[$key];
	}

	public function offsetUnset($key)
	{
		unset($this->htmlAttributes[$key]);
	}

	protected function setName($name)
	{
		$this->name = $name;
	}

	protected function getTransformedName()
	{
		return $this->parent ? $this->parent->transformName($this, $this->name) : $this->name;
	}

	public function renderHTMLAttributes()
	{
		foreach ($this->htmlAttributes as $key => $val)
		{
			if ($key[0] != ':')
				echo ' ', $key, '="', htmlspecialchars($val), '"';
		}
	}
}