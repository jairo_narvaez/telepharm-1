<?php
class TelePharm_Field_Year extends TelePharm_Field_Text
{
   public function __construct($id, $attr = [])
   {
           parent::__construct($id, $attr);
           $this['class'] = 'year';
           $this->type = 'year';
   }
}