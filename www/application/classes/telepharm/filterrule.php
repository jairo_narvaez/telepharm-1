<?php
class TelePharm_FilterRule
{
	public static function factory($name)
	{
		$reflector = new ReflectionClass("TelePharm_FilterRule_$name");
		return $reflector->newInstanceArgs(array_slice(func_get_args(), 1));
	}
}