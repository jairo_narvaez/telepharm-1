<?php

class TelePharm_Flash
{
	protected static $_instance;
  
	public static function instance()
	{
		if ( ! isset(TelePharm_Flash::$_instance))
		{
			// Create a new session instance
			TelePharm_Flash::$_instance = new TelePharm_Flash;
		}
		return TelePharm_Flash::$_instance;
	}
	
	// maps a status name to a css className
	private $_statuses = array(
	  'success' => 'success',
	  'error' => 'error',
	  'notice' => 'notice'
	);
    
	public function message($status, $message)
	{
		if(in_array($status, $this->_statuses))
		{
			$_SESSION['flash'][$status][] = $message;
		}
	}
  
	private function _render()
	{
		$view = View::factory('flash');
		$view->flash_messages = [];
		if( isset($_SESSION['flash']) && is_array($_SESSION['flash']) )
		{
			$view->flash_messages = $_SESSION['flash'];

		}
		echo $view->render();

		return $this;
	}
  
  private function _delete()
  {
    $_SESSION['flash'] = array();
  }
  
  public function html()
  {
    $this->_render()
         ->_delete();
  }
}
