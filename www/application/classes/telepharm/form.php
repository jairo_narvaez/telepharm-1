<?php
class TelePharm_Form extends Kohana_Form
{
	public static function dob($id, $default = null)
	{
		if ($default)		
			list($y, $m, $d) = explode('-', $default);
		else
			$y = $m = $d = null;
			
		$years = array('' => 'Year') + array_combine(range(date('Y'),1900), range(date('Y'),1900));
		$months = array('Month', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$days = range(0, 31);
		$days[0] = 'Day';

		$html = '<div class="month">' . Form::select($id."_m", $months, $m) . '</div>';			
		$html .= '<div class="day">' . Form::select($id."_d", $days, intval($d)) . '</div>';
		$html .= '<div class="year">' . Form::select($id."_y", $years, $y) . '</div>';

		
		return $html;
	}

	public static function dob_text($id, $default = null)
	{
		if ($default)
		{
			list($y,$m,$d) = explode('-', $default);
			$value = "$m/$d/$y";
		}
		else
		{
			$y = $m = $d = null;	
			$value = "";
		}

		$html = form::input($id, $value, array('mask'=>'##/##/####','placeholder' => 'mm/dd/yyyy'));
		return $html;
	}

  public static function time($id, $default = null)
	{
    if ($default){
      $time = explode(' ', $default);
      list($h, $m, $s) = explode(':', $time[1]);
    }
		else
			$h = $m = $s = null;
			
		$hours = range(0,23);
		$minutes = range(0,59);
		$seconds = range(0, 59);

		$html = '<div class="hours">' . Form::select($id."_h", $hours, intval($h)) . '</div>';			
		$html .= '<div class="minutes">' . Form::select($id."_mi", $minutes, intval($m)) . '</div>';
		$html .= '<div class="seconds">' . Form::select($id."_s", $seconds, intval($s)) . '</div>';

		
		return $html;
	}

	
	public static function state($id, $selected = null)
	{
		$states = array(
			'AK' => 'Alaska',
			'AL' => 'Alabama',
			'AR' => 'Arkansas',
			'AZ' => 'Arizona',
			'CA' => 'California',
			'CO' => 'Colorado',
			'CT' => 'Connecticut',
			'DC' => 'District of Columbia',
			'DE' => 'Delaware',
			'FL' => 'Florida',
			'GA' => 'Georgia',
			'HI' => 'Hawaii',
			'IA' => 'Iowa',
			'ID' => 'Idaho',
			'IL' => 'Illinois',
			'IN' => 'Indiana',
			'KS' => 'Kansas',
			'KY' => 'Kentucky',
			'LA' => 'Louisiana',
			'MA' => 'Massachusetts',
			'MD' => 'Maryland',
			'ME' => 'Maine',
			'MI' => 'Michigan',
			'MN' => 'Minnesota',
			'MO' => 'Missouri',
			'MS' => 'Mississippi',
			'MT' => 'Montana',
			'NC' => 'North Carolina',
			'ND' => 'North Dakota',
			'NE' => 'Nebraska',
			'NH' => 'New Hampshire',
			'NJ' => 'New Jersey',
			'NM' => 'New Mexico',
			'NV' => 'Nevada',
			'NY' => 'New York',
			'OH' => 'Ohio',
			'OK' => 'Oklahoma',
			'OR' => 'Oregon',
			'PA' => 'Pennsylvania',
			'PR' => 'Puerto Rico',
			'RI' => 'Rhode Island',
			'SC' => 'South Carolina',
			'SD' => 'South Dakota',
			'TN' => 'Tennessee',
			'TX' => 'Texas',
			'UT' => 'Utah',
			'VA' => 'Virginia',
			'VT' => 'Vermont',
			'WA' => 'Washington',
			'WI' => 'Wisconsin',
			'WV' => 'West Virginia',
			'WY' => 'Wyoming'
		);
		
		$html = "<select id=\"".htmlspecialchars($id)."\" name=\"".htmlspecialchars($id)."\">\n";
		$html .= " <option value=\"\"></option>\n";
		foreach ($states as $abbr => $name)
		{
			$html .= " <option value=\"$abbr\"";
			if ($abbr == $selected) $html .= ' selected="selected"';
			$html .= ">$name</option>\n";
		}
		$html .= "</select>\n";
		
		return $html;
	}
}
