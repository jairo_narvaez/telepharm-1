<?php
class TelePharm_HTML extends Kohana_HTML
{
	static private $_inline_js = [];
	static private $_inline_js_group = null;
	static public $compress_inline_js = false;
	static public $cache_js = false;
	
	static public function open_script($group = 'inline')
	{
		self::$_inline_js_group = $group;
		ob_start(); 
	}
	
	static public function close_script()
	{
		$script = ob_get_clean();
		$script = preg_replace('!^\s*<script[^>]*>!', '', $script);
		$script = preg_replace('!</script>\s*$!', '', $script);
		self::$_inline_js[self::$_inline_js_group][] = [0, trim($script)."\n", []];
	}

	static public function import_script($path, $group = 'inline', $options = [])
	{
		self::$_inline_js[$group][] = [1, $path, $options];
	}
	
	static public function get_scripts($groups = null)
	{
		if ($groups === null) $groups = ['inline'];

		$js = '';

		foreach ($groups as $group)
			$js .= self::_get_group_of_scripts($group);

		return $js;
	}

	static private function _get_group_of_scripts($group)
	{
		$build = Kohana::$config->load('release.build');

		if (!isset(self::$_inline_js[$group])) return '';

		if (self::$cache_js)
		{
			$hash = sha1(serialize(self::$_inline_js[$group]));

			$cache_file = TelePharm_URL::asset_path('js/cache/'.$hash.'.js');
			if (!file_exists($cache_file))
			{
				$js = '';
				foreach (self::$_inline_js[$group] as $_js)
				{
					list($is_external, $data, $options) = $_js;

					$contents = !$is_external ? $data : file_get_contents(TelePharm_URL::asset_path($data));
										
					if (self::$compress_inline_js && (!isset($options['compress']) || $options['compress'] == true))
						$contents = TelePharm_JSMin::minify($contents)."\n";
				
					$js .= $contents;
				}

				try {
					file_put_contents($cache_file, $js);
				}
				catch (Exception $e) {
					throw new Exception("Unable to create js. Be sure the js/cache folder exists in the asset folder and can be written to by the web server.");
				}
			}
			
			return '<script type="text/javascript" src="'.TelePharm_URL::asset('js/cache/'.$hash.'.js').'?v='.$build.'"></script>'."\n";
		}
		else 
		{
			$js = '';

			foreach (self::$_inline_js[$group] as $_js)
			{
				if ($_js[0] == 0)
				{
					$js .= '<script type="text/javascript">'.(self::$compress_inline_js ? TelePharm_JSMin::minify($_js[1]) : $_js[1]).'</script>'."\n";
				}
				else
				{
					$js .= '<script type="text/javascript" src="'.TelePharm_URL::asset($_js[1]).'?v='.$build.'"></script>'."\n";
				}
			}

			return $js;
		}
	}
}