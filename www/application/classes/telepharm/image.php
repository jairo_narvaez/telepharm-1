<?php
class TelePharm_Image
{
	protected $gmagick;
	protected $filename;
	
	const CENTER = 0, TOP = 1, BOTTOM = 2, LEFT = 3, RIGHT = 4;
	
	public function __construct($filename)
	{
		$this->gmagick = new GMagick($filename);
		$this->filename = $filename;
		$this->gmagick->setcompressionquality(95);
	}
	
	public function __get($key)
	{
		switch ($key)
		{
			case 'format':
				return strtolower($this->gmagick->getImageFormat());
				
			case 'height':
				return $this->gmagick->getImageHeight();
				
			case 'width':
				return $this->gmagick->getImageWidth();
		}
		
		throw new Exception("Invalid key $key");
	}
	
	public function resizedCrop($dw, $dh, $hcrop = self::CENTER, $vcrop = self::CENTER)
	{
		$dw = intval($dw);
		$dh = intval($dh);
		
		$sw = $this->gmagick->getImageWidth();
		$sh = $this->gmagick->getImageHeight();			
		
		$aspect = min($sw / $dw, $sh / $dh);
		
		$w = round($sw / $aspect);
		$h = round($sh / $aspect);
		
		if ($w < $dw) $w = $dw;
		if ($h < $dh) $h = $dh;
		
		$this->gmagick->resizeimage($w, $h, Gmagick::FILTER_CUBIC, 1);
		
		# If the images have different aspect ratios, then one of the 
		# dimensions will be too big. To compensate, crop the center out of
		# the image.
		if ($w != $dw || $h != $dh)
		{
			# Find the crop coordinates
			if ($hcrop == self::LEFT)
				$x = 0;
			else if ($hcrop == self::RIGHT)
				$x = $w - $dw;
			else
				$x = intval($w/2 - $dw/2);
				
			if ($vcrop == self::TOP)
				$y = 0;
			else if ($vcrop == self::BOTTOM)
				$y = $h - $dh;
			else
				$y = intval($h/2 - $dh/2);
			
			# Make sure the crop doesn't extend past the bounds of the image.
			if ($x + $dw > $w) $x = $dw - $w;
			if ($y + $dh > $h) $y = $dh - $h;
			
			$this->gmagick->cropImage($dw, $dh, $x, $y);				
		}
					
		return $this;
	}
	
	public function save($filename = null, $format = null)
	{
		if (!$filename) $filename = $this->filename;
		if ($format) $this->gmagick->setimageformat($format);
		$this->gmagick->write($filename);
		return $this;
	}
}