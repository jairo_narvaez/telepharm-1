<?php
class TelePharm_JSONArray extends ArrayObject implements TelePharm_Untaintable
{
	# All elements in the passed array will be inserted into the existing
	# array if they do not exist. NOTE: Integer indexes are ignored if the
	# existing array has any at all.
	public function insertRequiredElements(array $a1)
	{
		# unfortunately array_merge_recursive won't work, because it doesn't
		# overwrite all values... it appends
		$a2 = $this->getArrayCopy();
		
		self::_rmerge($a1, $a2);
		
		$this->exchangeArray($a1);
		
	}
	
	# recursively merge $a2 on top of $a1
	static private function _rmerge(array &$a1, array &$a2)
	{
		$unset_indexed_items = false;
		
		foreach ($a2 as $a2_key => $a2_value)
		{
			if (is_int($a2_key) && !$unset_indexed_items)
			{
				# If the existing array is indexed, then ignore *all* indexed elements
				# from the default array. This is likely the expected behavior, and is
				# the simplest merge solution.
				foreach (array_keys($a1) as $a1_key)
				{
					if (is_int($a1_key))
						unset($a1[$a1_key]);
				}
				
				$unset_indexed_items = true;
			}
			
			if (!isset($a1[$a2_key]))
				$a1[$a2_key] = $a2_value;
			else if (!is_array($a1[$a2_key]) || !is_array($a2_value))
				$a1[$a2_key] = $a2_value;
			else
				self::_rmerge($a1[$a2_key], $a2[$a2_key]);
		}
	}
	
	public function as_array()
	{
		return $this->getArrayCopy();
	}
	
	public function as_json()
	{
		return json_encode($this->getArrayCopy());
	}
	
	public function __toString()
	{
		return json_encode($this->getArrayCopy());
	}
	
	public function isTainted()
	{
		return true;
	}
	
	public function untaint()
	{
		return TelePharm_TaintedData::clean($this->getArrayCopy());
	}
	
}