<?php

class TelePharm_Kohana extends Kohana_Core
{
    /**
     * Overrides default debug() and adds some formatting for <pre>
     *
     * @return void
     * @author Matthew Anarde
     **/
    public static function debug()
	{
		if (func_num_args() === 0)
			return;

		// Get all passed variables
		$variables = func_get_args();

		$output = array();
		foreach ($variables as $var)
		{
			$output[] = Kohana::_dump($var, 1024);
		}

        $str = "<style type='text/css'>pre{padding:0 3px 2px;font-family:Monaco,Andale Mono,Courier New,monospace;font-size:12px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;background-color:#f5f5f5;display:block;padding:8.5px;margin:0 0 18px;line-height:18px;font-size:12px;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.15);-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;white-space:pre;white-space:pre-wrap;word-wrap:break-word;}</style>";
		$str .= '<pre class="debug">'.implode("\n", $output).'</pre>';
		return $str;
	}
	
	// used as prefix for requisition id on iguana messages
	public static function env()
	{
		return substr(strtoupper(Kohana::$environment), 0, 1);
	}
    
}