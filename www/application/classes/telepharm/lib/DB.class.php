<?php
	# DB.class.php
	# Copyright 2006-07 by Matthew Leverton
	#
	# DB is an extension of PDO that works with its own DBStatement and
	# DBRecordSet objects. [Note: It may end up being wrapper instead of
	# an extension to be consistent with the other objects.]
	#	
	# All error handling is done via exceptions. It is the programmer's
	# responsibility to not do non-sensical things like close a 
	# recordset that isn't opened, etc.
	#
	class DB
	{
		private $trans_depth = 0, $rollback = FALSE;
		private $supportsNestedTransactions = FALSE;
		private $pdo;

		# __construct(string $dsn, [string $user, [string $pass]])
		#
		# Creates a new connection to the database as specified by the DSN.
		# See PDO::__construct() for details on how to contruct the DSN.
		#
		public function __construct($dsn, $user = NULL, $pass = NULL)
		{
			$this->pdo = new PDO($dsn, $user, $pass);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		
		# bool beginTransaction(void)
		#
		# Begins a transaction, providing emulated support for nested
		# transactions as needed.
		#
		public function beginTransaction()
		{
			return ($this->supportsNestedTransactions || ++$this->trans_depth == 1) ?
				$this->pdo->beginTransaction() : FALSE;
		}
		
		# bool commit(void)
		#
		# Commits a transaction, providing emulated support for nested
		# transactions as needed.
		#
		public function commit()
		{
			if ($this->supportsNestedTransactions || --$this->trans_depth == 0)
			{
				if (!$this->rollback)
					$this->pdo->commit();
				else
				{
					$this->pdo->rollBack();
					$this->rollback = FALSE;
				}
			}
		}
		
		# bool rollBack(void)
		#
		# Rolls back a transaction, providing emulated support for nested
		# transactions as needed.
		#
		public function rollBack()
		{
			if ($this->supportsNestedTransactions || --$this->trans_depth == 0)
				$this->pdo->rollBack();
			else
				$this->rollback = TRUE;
		}
		
		# mixed getOne(string $sql)
		#
		# Returns the first field from the first record. 
		#
		public function getOne($sql)
		{
			$sth = $this->pdo->query($sql);
			$value =  $sth ? $sth->fetchColumn() : NULL;
			$sth->closeCursor();
			return $value;
		}

		# mixed[] getCol(string $sql, int $col = 0)
		#
		# Returns the $col'th field from each record.
		#
		public function getCol($sql, $col=0)
		{
			$st = $this->pdo->query($sql);
			$col = $st->fetchAll(PDO::FETCH_COLUMN, $col);
			$st->closeCursor();
			return $col;
		}
		
		# mixed[] getRow(string $sql)
		#
		# Returns the first row as an array.
		#
		public function getRow($sql)
		{
			$st = $this->pdo->query($sql);
			$row = $st->fetch(PDO::FETCH_ASSOC);
			$st->closeCursor();
			return $row;
		}

		# mixed[][] getAll(string $sql)
		#
		# Returns the entire result set as an array of rows.
		#
		public function getAll($sql)
		{
			$st = $this->pdo->query($sql);
			if (!$st) $this->throwError();
			return $st->fetchAll(PDO::FETCH_ASSOC);
		}

		# mixed[mixed key][] getAssoc(string $sql)
		#
		# Returns each row, using the first field as the key. If the record
		# set only has two fields, then a key => val pair is returned;
		# otherwise a key => row pair is returned.
		#		
		public function getAssoc($sql)
		{
			if (!($sth = $this->pdo->query($sql))) throw new Exception($sql);
			$rs = array();
			if ($sth->columnCount() == 2)
			{
				while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$rs[array_shift($row)] = array_shift($row);
			}
			else
			{
				while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$rs[array_shift($row)] = $row;
			}
			return $rs;
		}
		
		# DBRecordSet query(string $sql)
		#
		# Returns an unbuffered DBRecordSet for the given query. 
		#
		# Note for compatibility reasons with PEAR::DB, it also can be used
		# to execute an INSERT/DELETE/etc statement. However, this usage is
		# discouraged. DB::exec() should be used for those types of queries.
		#
		public function query($sql)
		{
			return strtoupper(substr(ltrim($sql), 0, 6)) == "SELECT" ?
				new DBRecordSet($this->pdo->query($sql)) : $this->exec($sql);
		}
		
		public function exec($sql)
		{
			return $this->pdo->exec($sql);
		}
		
		# DBRecordSet bufferedQuery(string $sql)
		#
		# Returns a buffered DBRecordSet for the given query. 
		#
		public function bufferedQuery($sql)
		{
			# TODO: eliminate MySQL specifics
			$this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
			$rs = new DBRecordSet($this->pdo->query($sql));
			$this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, FALSE);
			return $rs;
		}
		
		# DBStatement prepare(string $sql, [mixed $arg1, [mixed $arg2, ...]])
		#
		# Prepares the SQL statement, optionally binds the list of
		# arguments, and returns a DBStatement.
		#
		public function prepare($sql)
		{
			$sth = $this->pdo->prepare($sql);
			if (!$sth) $this->throwError();
			if (func_num_args() == 1)
				return new DBStatement($sth);
			else
			{
				$args = func_get_args();
				unset($args[0]);
				return new DBStatement($sth, $args);
			}
		}

		# void throwError(void)
		#
		# TODO: standardize error handling
		#
		private function throwError()
		{
			$error = $this->pdo->errorInfo();
			throw new Exception($error[0].' ('.$error[1].'): '.htmlspecialchars($error[2]));
		}
	}
?>
