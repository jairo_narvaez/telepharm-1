<?php

/**
* This class is the one in charge to manage
* the process of create a new prescription
* querying the PMS system, transforming the
* result to an homogenized result and then
* creating a prescription using that data
*/
class Telepharm_Manager_CreatePrescription
{
	protected $currentUser;
	protected $prescriptionNumber;

	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;

	/**
	* Creates a manager using a prescription Number
	* to query the PMS
	*
	* @param CurrentUser $currentUser the user that wants to create the prescription
	* @param string $prescriptionNumber the number to query
	*
	* @return Telepharm_Manager_CreatePrescription
	*/
	public function __construct(CurrentUser $currentUser, $prescriptionNumber)
	{
		$this->currentUser = $currentUser;
		$this->prescriptionNumber = $prescriptionNumber;
		$this->autoGetters = ['prescriptionNumber'];
	}

	/**
	 * set the new prescription number to the protected var
	 */
	public function setPrescriptionNumber($newPrescriptionNumber)
	{
		$this->prescriptionNumber = $newPrescriptionNumber;
	}

	/**
	* Get the store where the prescription exists
	*/
	public function getStore()
	{
		return array_slice($this->currentUser->stores, 0, 1)[0];
	}

	/**
	* use the store to create an specific object of Telepharm_PMS_*
	* and call the method to get the info from the external system
	*
	* @return Telepharm_PMS_Prescription
	*/
	public function getPrescription()
	{
		$pmsQueryObject = Telepharm_PMS::factory($this->getStore());
		$pmsQueryObject->setCurrentUser($this->currentUser);
		return $pmsQueryObject->getPrescription($this->prescriptionNumber);
	}

	/**
	 * refactored to make each type of PMS should
	 * be the ones to create the prescription
	 */
	public function createPrescription()
	{
		$pmsQueryObject = Telepharm_PMS::factory($this->getStore());
		$pmsQueryObject->setCurrentUser($this->currentUser);
		return $this->getPrescription();
	}
}
