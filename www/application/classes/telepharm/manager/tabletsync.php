<?php 

class Telepharm_Manager_TabletSync
{
	private $current_user = null;
	private $pharmacist_id = null;
	private $prescription_id = null;
	private $prescriptions = [];
	private $tablet_id = null;

	private $patient_name;
	private $validation_key = '';
	private $validation_value = '';

	/**
	 * Create a session to be used by the patient on the tablet
	 *
	 * @param CurrentUser $currentUser the user initiating the tablet
	 * @param integer $pharmacistId intended to take the call
	 * @param integer $patientName confirming the call
	 * @param integer $tabletId Optional, the tablet to send the 
	 *                          prescription to counsel
	 */
	public function initiate_direct(CurrentUser $currentUser, $patientName, $pharmacistId = null, $tabletId = null)
	{
		$this->current_user = $currentUser;
		$this->pharmacist_id = $pharmacistId;
		$this->patient_name = $patientName;
		$this->tablet_id = $tabletId;

		$this->sync();
	}

	/**
	 * Create a session to be used by the patient on the tablet
	 *
	 * @param CurrentUser $currentUser the user initiating the tablet
	 * @param integer $prescriptionId the prescription to initiate
	 * @param array <integer> $prescriptions an array of prescription ids
	 * @param integer $tabletId Optional, the tablet to send the 
	 *                          prescription to counsel
	 */
	public function initiate(CurrentUser $currentUser, $prescriptionId, array $prescriptions, $tabletId = null)
	{
		$this->current_user = $currentUser;
		$this->prescription_id = $prescriptionId;
		$this->prescriptions = $prescriptions;
		$this->tablet_id = $tabletId;

		$prescription = ORM::factory('Prescription', $prescriptionId);
		if (!$prescription->loaded()) throw new HTTP_Exception_501('Prescription does not exist in the system');

		$this->patient_name = $prescription->patient_fname . ' ' . $prescription->patient_lname;

		$this->sync();
	}

	/**
	 * get a list of unfinished sessions using
	 * the ipads on the store of the currentUser
	 *
	 * @return Array (Model_PatientAPI_Session)
	 */
	public function getUnfinishedTabletSessions(CurrentUser $currentUser)
	{
		if (!$currentUser) throw new HTTP_Exception_501('Current user not set');

		$store = array_slice($currentUser->stores, 0, 1)[0];

		if (!$store)
		{
			throw new HTTP_Exception_501('Unable to find a tablet at this store');
		}

		$store_id = $store->id;

		$sessions = DB::select('patient_api_session.*', DB::expr('patient_api_account.name as human_name'))
			->from('patient_api_session')
			->join('patient_api_account')->on('patient_api_account.id', '=', 'patient_api_session.patient_api_account_id')
			->where('patient_api_account.store_id', '=', $store_id)
			->and_where('patient_api_session.end_dt', '=', null)
			->and_where('patient_api_account.is_active', '=', 1)
			->execute()
			->as_array();

		return $sessions;

		/*
		$prescriptions = [];
		foreach(Model_Prescription::getScopedQuery($currentUser)->
			find_all() as $rx)
		{
			$prescriptions[] = $rx['id'];
		}
		
		if (count($prescriptions) == 0)
			return [];

		return Model::factory('PatientApiSession')->
			where('end_dt', 'is', null)->
			where('prescription_id', 'in', $prescriptions)->
			find_all()->
			untaint();
		*/
	}

	/**
	 * deactivate the session
	 * 
	 * @return bool
	 */
	public function deactivateSession($sessionId)
	{
		$session = Model::factory('PatientApiSession')->
			where('id', '=', $sessionId)->
			where('end_dt', 'is', null)->
			find();

		if (!$session->loaded())
			throw new HTTP_Exception_404('Tablet Session not found');

		$session->end_dt = date_create('now UTC');
		$session->save();

		return $session->saved();
	}

	/**
	 * private method to do the sync
	 * 
	 * @return bool
	 */
	private function sync()
	{
		if (!$this->current_user) throw new HTTP_Exception_501('Current user not set');

		if (!$this->prescriptions) $this->prescriptions = [0];

		$store = array_slice($this->current_user->stores, 0, 1)[0];

		if (!$store)
		{
			throw new HTTP_Exception_501('Unable to find a tablet at this store');
		}

		$store_id = $store->id;

		# this will need to be adjusted if/when we get in a multiple ipad scenario
		$session = DB::select('patient_api_session.*')
			->from('patient_api_session')
			->join('patient_api_account')->on('patient_api_account.id', '=', 'patient_api_session.patient_api_account_id')
			->where('patient_api_session.end_dt', '=', null)
			->and_where('patient_api_account.store_id', '=', $store_id)
			->execute()
			->current();

		if ($session)
		{

			$message = '';
			if ($session['prescription_id'])
			{
				$prescription = ORM::factory('Prescription')
					->where('id', '=', $session['prescription_id'])
					->find();

				$message = ' (Rx # ' . $prescription->number . ')';
			}
			else
			{

			}

			throw new HTTP_Exception_501('The tablet is currently reserved' . $message);
		}

		$range = range('a', 'z');
		$str = '';

		for($i = 0; $i < 50; $i++)
		{
			$val = array_rand($range);

			$str .= $range[$val];
		}

		if ($this->tablet_id)
		{
			# this portion will be relevant when there is more than one ipad
			# at a store
			$account = ORM::factory('PatientAPIAccount')
				->where('id', '=', $this->tablet_id)
				->and_where('store_id', '=', $store_id)
				->and_where('is_active', '=', 1)
				->find();	
		}
		else
		{
			$account = ORM::factory('PatientAPIAccount')
				->where('store_id', '=', $store_id)
				->and_where('is_active', '=', 1)
				->find();
		}

		if (!$account->loaded())
		{
			throw new HTTP_Exception_401('Tablet is not registered in the store');
		}

		$patient_info = [
			'patient_name' => $this->patient_name,
			'validation_key' => $this->validation_key,
			'validation_value' => $this->validation_value
		];

		$session = ORM::factory('PatientAPISession')
			->values([
				'patient_api_account_id' => $account['id'],
				'token' => $str,
				'prescription_id' => $this->prescription_id,
				'technician_id' => $this->current_user->account->id,
				'pharmacist_id' => $this->pharmacist_id,
				'patient_info' => json_encode($patient_info),
				'created_dt' => date('Y-m-d H:i:s')
				])
			->save();

		$prescription = ORM::factory('Prescription', $this->prescription_id);

		# this serves as validation that we are positively linking Rx-es of the same patient
		$session_prescriptions = DB::select('prescription.id')
			->from('prescription')
			->where('prescription.patient_id', '=', $prescription->patient_id)
			->and_where('prescription.approved_for_counsel', '=', 1)
			->and_where('prescription.completed', '=', 0)
			->and_where('prescription.id', 'in', $this->prescriptions)
			->execute()->as_array();

		foreach ($session_prescriptions as $rx)
		{
			ORM::factory('PatientAPISessionPrescription')
				->values([
					'patient_api_session_id' => $session->id,
					'prescription_id' => $rx['id']
				])->save();
		}

		return $session->saved();
	}
}

