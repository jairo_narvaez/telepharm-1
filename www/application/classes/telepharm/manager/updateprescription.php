<?php

/**
* This class is the one in charge to manage
* the process of update/refresh a new prescription
* querying the PMS system, transforming the
* result to an homogenized result and then
* creating a prescription using that data
*/
class Telepharm_Manager_UpdatePrescription
{
	protected $currentUser;
	protected $prescriptionId;
	protected $prescription;

	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;

	/**
	* Creates a manager using a prescription id
	* from a prescription already on the system
	* to query the PMS
	*
	* @param CurrentUser $currentUser the user that wants to create the prescription
	* @param integer $prescriptionId the id of the prescription
	*
	* @return Telepharm_Manager_CreatePrescription
	*/
	public function __construct(CurrentUser $currentUser, $prescriptionId)
	{
		$this->currentUser = $currentUser;
		$this->setPrescriptionId($prescriptionId);
		$this->autoGetters = ['prescriptionId'];
	}

	/**
	 * set the new prescription number to the protected var
	 */
	public function setPrescriptionId($newPrescriptionId)
	{
		$this->prescriptionId = $newPrescriptionId;
		$this->prescription = Model::factory('Prescription')->
			where('id', '=', $this->prescriptionId)->
			find();
		if (!$this->prescription->loaded())
			throw new Exception('Prescription not found');
	}

	/**
	* Get the store where the prescription exists
	*/
	public function getStore()
	{
		return $this->prescription->prescription_store->store;
	}

	/**
	* use the store to create an specific object of Telepharm_PMS_*
	* and call the method to get the info from the external system
	*
	* @return Telepharm_PMS_Prescription
	*/
	public function getPrescription()
	{
		$pmsQueryObject = Telepharm_PMS::factory($this->getStore());
		$pmsQueryObject->setCurrentUser($this->currentUser);
		return $pmsQueryObject->getPrescription($this->prescription->number);
	}

	public function updatePrescription()
	{
		if ($this->prescription->prescription_store->store->pms_query_class ==
			'Telepharm_PMS_CRX')
			throw new Exception('Please resend the prescription using Computer RX software');

		$prescription = $this->getPrescription();
		$pmsQueryObject = Telepharm_PMS::factory($this->getStore());
		$pmsQueryObject->setCurrentUser($this->currentUser);
		return $pmsQueryObject->updatePrescription(
			$this->prescription, $prescription, $this->currentUser);
	}
}
