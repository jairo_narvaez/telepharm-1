<?php
require_once dirname(__FILE__).'/lib/DB.class.php';
require_once dirname(__FILE__).'/lib/DBStatement.class.php';
require_once dirname(__FILE__).'/lib/DBRecordSet.class.php';

class TelePharm_Migrator
{
	private $con = null; // db connection
	private $migrations_path = null; // path where migration files live

	public function __construct($migrations_path, $connection='default')
	{
		$conf = Kohana::$config->load('database.'.$connection.'.connection');

		$db = new DB(
			'mysql:host='.$conf['hostname'].';dbname='.$conf['database'],
			$conf['username'],
			$conf['password']
		);
		$this->migrations_path = $migrations_path;
		$this->con = $db;
	}
	
	public function is_connected()
	{
		return $this->con != FALSE;
	}
	
	public function get_migrations_path()
	{
		return $this->migrations_path;
	}
	
	public function execute_sql($sql)
	{
		$this->con->exec($sql);		
	}
	
	public function get_pending()
	{

		$this->check_schema_setup();
		
		$latest = $this->get_latest_version();
		
		$last_timestamp = isset($latest) ? intval($latest) : 1;
		
		$files = $this->get_migrations_files();
		
		$pending = array();
		foreach($files as $file)
		{
			
			if (intval($this->get_version($file)) > $last_timestamp)
			{
				$pending[] = $file;
			}
		}
		
		sort($pending);
		
		return $pending;
	}
	
	public function get_pending_by_diff()
	{

		$this->check_schema_setup();
		
		$applied_migration_ps = $this->con->prepare("SELECT `version` ".
			" FROM `schema_migrations` ORDER BY `version` desc ");
		
		$applied_migrations = $applied_migration_ps->getAll();
		
		$applied_versions = array();
		foreach($applied_migrations as $mig)
		{
			$applied_versions[] = $mig['version'];
		}

		$files = $this->get_migrations_files();
		
		$pending = array();
		foreach($files as $file)
		{
			
			if (!in_array($this->get_version($file), $applied_versions))
			{
				$pending[] = $file;
			}
		}
		
		sort($pending);
		
		return $pending;
	}
	
	/**
	 * apply pending files to db as migrations
	 *
	 * @return array
	 **/
	public function apply_pending($how="pending")
	{
		$applied = array();
		$method = "get_$how";
		$pending = $this->{$method}();
		foreach($pending as $migration)
		{
			$applied_correctly = false;
			try
			{
				$sql = file_get_contents($this->migrations_path . 
					'/' . $migration);
				error_log( "\nSQL from {$migration}:\n".$sql."\n" );
				$this->execute_sql($sql);
				$applied_correctly = true;
			}
			catch(PDOException $e)
			{
				echo "\nError on migration " . $migration . " with exception " . 
					$e->getMessage() . "\n";
			}
			
			if ($applied_correctly)
			{
				$update_schema_version_ps = $this->con->
					prepare("INSERT INTO `schema_migrations` VALUES(?) ");
				$update_schema_version_ps->
					bindValue(1, $this->get_version($migration))->
					exec();
				$applied[] = $migration;
			}
		}
		
		return $applied;
	}

	/**
	 * return the latest version on db
	 *
	 * @return string
	 **/
	public function get_latest_version()
	{	
		$latest_migration_ps = $this->con->prepare("SELECT max(`version`) latest ".
			" FROM `schema_migrations`");
		
		$latest =  $latest_migration_ps->getRow();
		
		return $latest['latest'];
	}
	
	protected function get_migrations_files()
	{
		$files = array();
		if ($handle = opendir($this->migrations_path)) {

			// read the dir and check for files and sql in the name
		    while (false !== ($file = readdir($handle))) {
				if (is_file($this->migrations_path . '/' . $file) and
				 	preg_match("/(\d*)[_](.*)\.sql$/", $file))
			        $files[] = $file;
		    }

		    closedir($handle);
		}
		
		return $files;
	}

	protected function get_version($file)
	{
		preg_match("/(\d*)[_](.*)\.sql$/", $file, $matches);
		return $matches[1];
	}
	
	protected function check_schema_setup() 
	{
		try
		{
			$check_table_existance = $this->con->prepare('DESC schema_migrations');
			$check_table_existance->getRow();
		}
		catch(PDOException $e)
		{
			// table doesn't exist
			$setup_migrations = <<<EOF
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
EOF;
			$this->con->exec($setup_migrations);
		}
	}

}

