<?php defined('SYSPATH') or die('No direct access allowed.');

class TelePharm_Model_Account extends ORM {

	protected $_table_name = 'account';

	public function __set($key, $val)
	{
		switch ($key)
		{
			case 'password':
				return parent::__set('password', TelePharm_Password::hash($val));
			break;

			case 'password_hash':
				return parent::__set('password', $val);
			break;

			default:
				return parent::__set($key, $val);
		}
	}

	static public function authenticate($username, $password)
	{
		$account = ORM::factory('Account')->where('username', '=', $username)->find();

        if ($account->loaded() && TelePharm_Password::validate($password, $account->password)){
            $account->web_session = Model_AccountToken::fetch($account->id, Model_AccountSessionType::Web);
            $account->forgot_password_session = Model_AccountToken::fetch($account->id, Model_AccountSessionType::Forgot_Password);
        }else{
            $account = null;
        }

		return $account;
	}

}