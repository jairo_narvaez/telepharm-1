<?php
class TelePharm_ORM extends Kohana_ORM implements TelePharm_Untaintable, ArrayAccess
{
	# Virtual fields are automatically pulled in by the as_array() function,
	# so they should be limited to things that are both commonly used
	# and trivial to create.
	protected $_virtual_fields = array();
	
	public function __set($key, $val)
	{
		if (substr($key, -3) == '_dt' && $val instanceof DateTime)
		{
			$val = $val->format('Y-m-d H:i:s');
		}
		
		parent::__set($key, $val);
	}

	public function offsetExists($key)
	{
		return isset($this->$key);
	}
	
	public function offsetSet($key, $val)
	{
		$this->__set($key, $val);
	}
	
	public function offsetGet($key)
	{
		return $this->__get($key);
	}
	
	public function offsetUnset($key)
	{
		# NOOP
	}
	
	public function isTainted()
	{
		return true;
	}
	
	public function untaint()
	{
		return TelePharm_TaintedData::clean($this->as_array());
	}
	
	public function as_array()
	{
		$data = parent::as_array();
		foreach ($this->_virtual_fields as $key)
			$data[$key] = $this->__get($key);
		
		return $data;
	}
	
	public function save(Validation $validation = null)
	{
		if (array_key_exists('created_dt', $this->_object) && !$this->_object['created_dt'])
		{
			$this->created_dt = date('Y-m-d H:i:s');
		}
		
		if (array_key_exists('updated_dt', $this->_object))
		{
			$this->updated_dt = date('Y-m-d H:i:s');
		}
		
		return parent::save($validation);
	}
}
