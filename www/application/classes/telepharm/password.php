<?php
	/* An implementation of a hashed, 128-bit salted password using bcrypt.
	 * The hashed password has a length of 60. 
	 *
	 * Usage:
	 *   $hash = Password::hash('foo');
	 *
	 *   if (Password::validate('foo', $hash))
	 *     echo "Valid password.";
	 *
	 */
	class TelePharm_Password
	{
		# return a hashed version of the plain text password.
		public static function hash($plain_text)
		{
			$cost_factor = (int) Kohana::$config->load('application.password_cost_factor');
			if ($cost_factor < 4 || $cost_factor > 31)
				throw new Exception('Invalid cost factor setting in application config');
			$cost_factor = sprintf('%02d', $cost_factor);			
			
			$salt = Security_Token::generate(16);
			
			# crypt uses a different base64 alphabet, so convert to it for good measure
			$salt = strtr(
				base64_encode($salt), 
				'ABCDEFGHJIKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
				'./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
			);
			
			return crypt($plain_text, '$2a$'.$cost_factor.'$'.$salt);
		}

		# validate that a hashed password is the same as the plain text version
		public static function validate($plain, $hash)
		{
			return crypt($plain, $hash) == $hash;
		}
	}
?>
