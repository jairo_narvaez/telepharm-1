<?php

/**
* Abstract class to define the interface for the Pharmacy Management Systems
* to query and homogenize results to be used in the prescription creation
* process
*/
abstract class Telepharm_PMS
{
	protected $store;
	protected $currentUser;

	/**
	* all the pms systems are installed in a store,
	* so we need the store info to know where to query
	*/
	public function __construct(Model_Store $store)
	{
		$this->store = $store;
	}

	public static function factory(Model_Store $store)
	{
		$className = $store->pms_query_class;
		return new $className($store);
	}

	public function setCurrentUser(CurrentUser $c)
	{
		$this->currentUser = $c;
	}

	/**
	* common interface to create a prescription without querying the PMS
	* @param string $prescriptionData the number to query on the PMS
	*
	* @return Model_Prescription
	*/
	abstract public function createPreDraftPrescription($prescriptionData, CurrentUser $currentUser);

	/**
	* common interface to create a prescription using the standard object
	*
	* @param Telepharm_PMS_Result_Prescription $prescription the standard object
	* @param CurrentUser $currentUser the user creating the prescription
	*
	* @return Model_Prescription
	*/
	abstract public function createPrescription(Telepharm_PMS_Result_Prescription $prescription, CurrentUser $currentUser);

	/**
	* common interface to update a prescription using the standard object
	*
	* @param Model_PrescriptionLog $prescription already on the db
	* @param Telepharm_PMS_Result_Prescription $prescription the standard object
	* @param CurrentUser $currentUser the user creating the prescription
	*
	* @return Model_Prescription
	*/
	public function updatePrescription(Model_Prescription $p, Telepharm_PMS_Result_Prescription $prescription, CurrentUser $currentUser)
	{
		$this->currentUser = $currentUser;

		if (! $prescription->isValid() )
		{
			Kohana::$log->add(Log::ERROR, 'prescription errors '.
				var_export($prescription->errors, 1) );
			throw new Exception('Not valid Prescription information');
		}

		$db = Database::instance();
		try
		{
			$db->begin();
			// begin tx
			// Update Prescription
			$newPrescription = $p;
			$newPrescription->loadResultPrescription($prescription);
			// create patient location if we have address info
			if ($prescription->patientLocation)
			{
				// Creates the location
				$location = new Model_Location($p->patient_location_id);
				$location->loadLocation($prescription->patientLocation);
				$location->save();
				$newPrescription->patient_location_id = $location->id;
			}
			$newPrescription->save();
			$newPrescriptionID = $newPrescription->id;

			if ($prescription->raw)
			{
				$prescriptionRaw = new Model_PrescriptionRaw();
				$prescriptionRaw->prescription_id = $newPrescriptionID;
				$prescriptionRaw->operation_type = 'update';
				$prescriptionRaw->serialized_data = json_encode($prescription->raw, TRUE);
				$prescriptionRaw->save();
			}
			// Create prescription_store, this shouldn't be updated by the update

			// Update drug
			$newPrescription->prescription_drug->updateDrug($prescription->drug);

			// Update Dispense
			$newPrescription->prescription_dispense->updateDispense($prescription->dispense);
	
			// Search for prescriber or create if not exists
			// Update prescription_prescriber
			$newPrescription->prescription_prescriber->updatePrescriber($prescription->prescriber);

			// end tx
			$db->commit();

			// Create a log to show who created the prescription
			Model_PrescriptionLog::createLog(
				$this->currentUser,
				$newPrescription,
				'update-from-pms');
		}
		catch (Exception $e)
		{
			$db->rollback();
			throw $e;
		}

		return $newPrescription;
	}

	/**
	* common interface to query a PMS
	* @param string $prescriptionNumber the number to query on the PMS
	*
	* @return Telepharm_PMS_Result_Prescription
	*/

	abstract public function getPrescription($prescriptionNumber);
	/**
	* common interface to query a PMS
	* @param string $patientID the patient id to query on the PMS
	*
	* @return array Telepharm_PMS_Result_Prescription
	*/
	abstract public function getPatientHistory($patientID);
}
