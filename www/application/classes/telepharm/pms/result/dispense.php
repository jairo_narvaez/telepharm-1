<?php

/**
* The unified result of a query to PMS
* to get the prescription dispense information
*/
class Telepharm_PMS_Result_Dispense
{
	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;

	protected $qtyWritten;
	protected $qtyDispensed;
	protected $sig;
	protected $sigCoded;
	protected $sigExpanded;
	protected $daysSupply;
	protected $fillsOwed;
	protected $refillAuth;
	protected $numRefills;

	private $inputFilter;

	public function __construct()
	{
		$this->autoGetters= [
			'qtyWritten',
			'qtyDispensed',
			'sig',
			'sigCoded',
			'sigExpanded',
			'daysSupply',
			'fillsOwed',
			'refillAuth',
			'numRefills',
		];
	}

	public function __set($key, $value)
	{
		if (in_array($key, $this->autoGetters))
			$this->{$key} = $value;
	}

	/**
	 * Get the errors of the validation
	 */
	public function getErrors()
	{
		if ($this->inputFilter)
			return $this->inputFilter->errors;
		return [];
	}

	/**
	 * return the data as an associative array
	 * only used for inputfilter validation
	 *
	 * @return array
	 */
	public function as_array()
	{
		$result = [];
		foreach ($this->autoGetters as $key) {
			$result[$key] = $this->{$key};
		}
		return $result;
	}

	/**
	 * Check that all the objects inside this object are valid
	 * to use the values for the prescription creation
	 *
	 * @return boolean
	 */
	public function isValid()
	{
		$types = [
			'qtyWritten' => 'integer',
			'qtyDispensed' => 'integer',
			'sig' => 'string',
			'sigCoded' => 'string',
			'sigExpanded' => 'string',
			'daysSupply' => 'string',
			'fillsOwed' => 'string',
			'refillAuth' => 'string',
			'numRefills' => 'string',
		];
		$info = new TelePharm_InputFilter($this->as_array());
		$info->sanitize($types);
		# Check required fields
		$info->rule(function ($inputfilter) {
			$fields = [
				'qtyWritten',
				'qtyDispensed',
				'sig',
				'sigCoded',
				'sigExpanded',
				'daysSupply',
				'fillsOwed',
				'refillAuth',
				'numRefills',
			];
			$missing = [];
			foreach ($fields as $key) {
				if (!isset($inputfilter[$key]) || empty($inputfilter[$key]) && $inputfilter[$key] != 0)
				{
					$missing[] = $key;
					$names[] = $inputfilter->get_field_name($key);
				}
			}

			if (!$missing) return true;

			return array(
				'errors' => array('The following fields must be accurately filled out: '.implode(', ', $names)),
				'fields' => $missing
			);
		});
		$this->inputFilter = $info;
		if ($info->check())
		{
			# set all the fields from the inputfilter sanitized
			foreach ($this->autoGetters as $key) {
				if (isset($info[$key]))
				{
					$this->{$key} = $info[$key];
				}
			}
			return true;
		}
		return false;
	}
}
