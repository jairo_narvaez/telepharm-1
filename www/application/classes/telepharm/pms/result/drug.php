<?php

/**
* The unified result of a query to PMS
* to get the prescription drug information
*/
class Telepharm_PMS_Result_Drug
{
	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;

	protected $newNdc;
	protected $manf;
	protected $description;
	protected $packageSize;
	protected $strength;
	protected $packageSizeUnitCode;

	private $inputFilter;

	public function __construct()
	{
		$this->autoGetters= [
		'newNdc',
		'manf',
		'description',
		'packageSize',
		'strength',
		'packageSizeUnitCode',
		];
	}

	public function __set($key, $value)
	{
		if (in_array($key, $this->autoGetters))
			$this->{$key} = $value;
	}

	/**
	 * Get the errors of the validation
	 */
	public function getErrors()
	{
		if ($this->inputFilter)
			return $this->inputFilter->errors;
		return [];
	}

	/**
	 * return the data as an associative array
	 * only used for inputfilter validation
	 *
	 * @return array
	 */
	public function as_array()
	{
		$result = [];
		foreach ($this->autoGetters as $key) {
			$result[$key] = $this->{$key};
		}
		return $result;
	}

	/**
	 * Check that all the objects inside this object are valid
	 * to use the values for the prescription creation
	 *
	 * @return boolean
	 */
	public function isValid()
	{
		$types = [
			'newNdc' => 'string',
			'manf' => 'string',
			'description' => 'string',
			'packageSize' => 'string',
			'strength' => 'string',
			'packageSizeUnitCode' => 'string',
		];
		$info = new TelePharm_InputFilter($this->as_array());
		$info->sanitize($types);
		# Check required fields
		$info->rule(function ($inputfilter) {
			$fields = [
				'newNdc',
				'manf',
				'description',
				'packageSize',
				//'strength',
				'packageSizeUnitCode',
			];
			$missing = [];
			foreach ($fields as $key) {
				if (!isset($inputfilter[$key]) || empty($inputfilter[$key]))
				{
					$missing[] = $key;
					$names[] = $inputfilter->get_field_name($key);
				}
			}

			if (!$missing) return true;

			return array(
				'errors' => array('The following fields must be accurately filled out: '.implode(', ', $names)),
				'fields' => $missing
			);
		});
		$this->inputFilter = $info;
		if ($info->check())
		{
			# set all the fields from the inputfilter sanitized
			foreach ($this->autoGetters as $key) {
				if (isset($info[$key]))
				{
					$this->{$key} = $info[$key];
				}
			}
			return true;
		}
		Kohana::$log->add(Log::ERROR, var_export($this->errors, true));
		return false;
	}
}
