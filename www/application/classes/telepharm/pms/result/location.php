<?php

/**
* location info for patients
*/
class Telepharm_PMS_Result_Location
{
	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;

	protected $name;
	protected $address1;
	protected $address2;
	protected $city;
	protected $state;
	protected $zip;

	private $inputFilter;

	public function __construct()
	{
		$this->autoGetters= [
		'name',
		'address1',
		'address2',
		'city',
		'state',
		'zip',
		];
	}

	public function __set($key, $value)
	{
		if (in_array($key, $this->autoGetters))
			$this->{$key} = $value;
	}

	/**
	 * Get the errors of the validation
	 */
	public function getErrors()
	{
		if ($this->inputFilter)
			return $this->inputFilter->errors;
		return [];
	}

	/**
	 * return the data as an associative array
	 * only used for inputfilter validation
	 *
	 * @return array
	 */
	public function as_array()
	{
		$result = [];
		foreach ($this->autoGetters as $key) {
			$result[$key] = $this->{$key};
		}
		return $result;
	}

	/**
	 * Check that all the objects inside this object are valid
	 * to use the values for the prescription creation
	 *
	 * @return boolean
	 */
	public function isValid()
	{
		$types = [
			'name' => 'string',
			'address1' => 'string',
			'address2' => 'string',
			'city' => 'string',
			'state' => 'string',
			'zip' => 'string',
		];
		$info = new TelePharm_InputFilter($this->as_array());
		$info->sanitize($types);
		# Check required fields
		$info->rule(function ($inputfilter) {
			$fields = [
				'address1',
			];
			$missing = [];
			foreach ($fields as $key) {
				if (!isset($inputfilter[$key]) || empty($inputfilter[$key]))
				{
					$missing[] = $key;
					$names[] = $inputfilter->get_field_name($key);
				}
			}

			if (!$missing) return true;

			return array(
				'errors' => array('The following fields must be accurately filled out: '.implode(', ', $names)),
				'fields' => $missing
			);
		});
		$this->inputFilter = $info;
		if ($info->check())
		{
			# set all the fields from the inputfilter sanitized
			foreach ($this->autoGetters as $key) {
				if (isset($info[$key]))
				{
					$this->{$key} = $info[$key];
				}
			}
			return true;
		}
		Kohana::$log->add(Log::ERROR, var_export($this->errors, true));
		return false;
	}
}

