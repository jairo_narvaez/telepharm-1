<?php

/**
* The unified result of a query to PMS
* to get the prescription information
*/
class Telepharm_PMS_Result_Prescription
{
	use \traits\TelePharm_Getter;
	use \traits\TelePharm_Setter;

	protected $number;
	protected $refillNumber;
	protected $patientFirstName;
	protected $patientLastName;
	protected $patientID;
	protected $patientDob;
	protected $patientPhone;
	protected $patientEmail;
	protected $isRefill;
    protected $isNew;
	protected $dateFilled;
	protected $dateExpires;
	protected $dateWritten;

	protected $patientLocation;

	protected $prescriber;
	protected $dispense;
	protected $drug;

	protected $raw;

	private $inputFilter;

	public function __construct()
	{
		$this->autoGetters= [
			'number',
			'refillNumber',
			'patientFirstName',
			'patientLastName',
			'patientID',
			'patientDob',
			'patientPhone',
			'patientEmail',
			'isRefill',
            'isNew',
			'dateFilled',
			'dateExpires',
			'dateWritten',
			'patientLocation',
			'prescriber',
			'dispense',
			'drug',
			'raw',
		];
	}

	public function __set($key, $value)
	{
		if (in_array($key, $this->autoGetters))
			$this->{$key} = $value;
	}

	/**
	 * Get the errors of the validation
	 */
	public function getErrors()
	{
		if ($this->inputFilter)
			return $this->inputFilter->errors;
		return [];
	}

	/**
	 * return the data as an associative array
	 * only used for inputfilter validation
	 *
	 * @return array
	 */
	public function as_array()
	{
		$result = [];
		foreach ($this->autoGetters as $key) {
			$result[$key] = $this->{$key};
		}
		if ($this->prescriber != null)
			$result['prescriber'] = $this->prescriber->as_array();
		if ($this->dispense != null)
			$result['dispense'] = $this->dispense->as_array();
		if ($this->drug != null)
			$result['drug'] = $this->drug->as_array();
		return $result;
	}

	/**
	 * Check that all the objects inside this object are valid
	 * to use the values for the prescription creation
	 *
	 * @return boolean
	 */
	public function isValid()
	{
		$types = [
			'number' => 'string',
			'refillNumber' => 'string',
			'patientID' => 'string',
			'patientFirstName' => 'string',
			'patientLastName' => 'string',
			'patientDob' => 'date',
			'patientPhone' => 'string', #'phone',
			'patientEmail' => 'email',
			'isRefill' => 'boolean',
            'isNew' => 'boolean',
			'dateFilled' => 'date',
			'dateExpires' => 'date',
			'dateWritten' => 'date',
		];
		$info = new TelePharm_InputFilter($this->as_array());
		$info->sanitize($types);
		# Check required fields
		$info->rule(function ($inputfilter) {
			$fields = ['number',
				'patientID',
				'patientFirstName',
				'patientLastName',
				'patientDob',
			];
			$missing = [];
			foreach ($fields as $key) {
				if (!isset($inputfilter[$key]) || empty($inputfilter[$key]))
				{
					$missing[] = $key;
					$names[] = $inputfilter->get_field_name($key);
				}
			}

			if ($inputfilter['isRefill'] === NULL)
			{
				$missing[] = 'isRefill';
				$names[] = $inputfilter->get_field_name('isRefill');
			}

			if (!$missing) return true;

			return array(
				'errors' => array('The following fields must be accurately filled out: '.implode(', ', $names)),
				'fields' => $missing
			);
		});
		$prescriber = $this->prescriber;
		$info->rule(function($inputfilter) use ($prescriber) {

			if ($prescriber instanceof Telepharm_PMS_Result_Prescriber && $prescriber->isValid())
			{
				return true;
			}

			return array(
				'errors' => array('The prescriber is invalid'),
				'fields' => ['prescriber']
			);
		});
		$dispense = $this->dispense;
		$info->rule(function($inputfilter) use ($dispense) {

			if ($dispense instanceof Telepharm_PMS_Result_Dispense && $dispense->isValid())
			{
				return true;
			}

			return array(
				'errors' => array('The dispense is invalid'),
				'fields' => ['dispense']
			);
		});
		$drug = $this->drug;
		$info->rule(function($inputfilter) use ($drug) {

			if ($drug instanceof Telepharm_PMS_Result_Drug && $drug->isValid())
			{
				return true;
			}

			return array(
				'errors' => array('The drug is invalid'),
				'fields' => ['drug']
			);
		});
		$this->inputFilter = $info;
		if ($info->check())
		{
			# set all the fields from the inputfilter sanitized
			foreach ($this->autoGetters as $key) {
				if (isset($info[$key]))
				{
					$this->{$key} = $info[$key];
				}
			}
			return true;
		}
		return false;
	}
}
