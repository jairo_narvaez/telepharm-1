<?php

/**
 * Specific class to get rx from the PMS: Computer RX
 */
class Telepharm_PMS_RxKey extends Telepharm_PMS
{
    /**
     * common interface to create a prescription without querying the PMS
     * @param Array $prescriptionData parsed from the crx
     * @param CurrentUser $currentUser the user creating the rx
     *
     * @return Model_Prescription
     */
    public function createPreDraftPrescription($prescriptionData, CurrentUser $currentUser)
    {
        $prescriptionStandard = $this->makeResultStandard($prescriptionData);

        $result = $this->createPrescription($prescriptionStandard, $currentUser);
        $prescription = $result['rx'];
        $prescription = $prescription->save();

        if ($prescription->is_refill && !$result['update']){
            //get original prescription
            $original_prescription = Model_Prescription::
                getScopedQuery($this->currentUser)->
                where('number', '=', $prescription->number)->
                where('refill_number', '<', $prescription->refillNumber)->
                order_by('refill_number', 'asc')->
                find();
            //check to make sure original prescription exists
            if ($original_prescription->loaded()){
                //get original prescription_image
                $original_prescription_image = ORM::factory('PrescriptionImage')->
                    where('prescriptionimage.prescription_id', '=', $original_prescription->id)->
                    where('prescriptionimage.type', '=', 'prescription')->
                    find();
                //check to see if original prescription_image exists
                if ($original_prescription_image->loaded()){
                    //get original image;
                    $original_image = new Model_Image($original_prescription_image->image_id);
                    //check to see if original image exists
                    if ($original_image->loaded()){
                        //you got this far, you should have enough valid data to create a new prescription image to associate with the new prescription
                        $prescription_image = Model_PrescriptionImage::create_prescription_image($prescription, $original_image, $original_prescription_image->type, $original_prescription_image->display_order);
                        $now = new DateTime();
                        $prescription_image->set_created($now);
                        $prescription_image = $prescription_image->save_prescription_image();
                    }
                }
            }
        }
        return ['prescription'=>$prescription, 'update'=>$result['update']];
    }

    /**
     * @param Array $prescriptionData parsed from the crx
     * @param CurrentUser $currentUser the user updating the rx
     * @param integer $prescriptionId id of the rx to update
     *
     * @return Model_Prescription
     */
    public function updateRawPrescription(Telepharm_PMS_Result_Prescription $prescription, CurrentUser $currentUser, $prescriptionId)
    {
        $foundPrescription = new Model_Prescription($prescriptionId);
        if (!$foundPrescription->loaded())
            throw new Exception('Prescription not found');

        return $this->updatePrescription($foundPrescription,
            $prescription,
            $currentUser);
    }

    /**
     * here goes the implementation to query a CRX
     * to get the info of the prescription
     *
     * @param string $prescriptionNumber the number to query on the PMS
     *
     * @return Telepharm_PMS_Prescription
     */
    public function getPrescription($prescriptionNumber)
    {
        $number = $prescriptionNumber;
        $refill = 0;
        $numberParts = explode('-', $prescriptionNumber);
        if (count($numberParts) == 1)
        {
            $number = trim($prescriptionNumber);
        }
        elseif (count($numberParts) == 2)
        {
            $number = trim($numberParts[0]);
            $refill = trim($numberParts[1]);
        }
        else
            throw new Exception('Invalid prescription number');

        $prescription = Model_Prescription::
            getScopedQuery($this->currentUser)->
            where('canceled' , '=', false)->
            where('number', '=', $number);

        if ($refill > 0)
        {
            $prescription->where('refill_number', '=', $refill);
        }

        $prescription = $prescription->find();

        if (!$prescription->loaded())
            throw new Exception('Prescription not found' . ' ' . $prescriptionNumber);

        if ($prescription->pre_draft)
        {
            $prescription->is_draft = 1;
            $prescription->pre_draft = 0;
            $prescription->account_id = $this->currentUser->account->id;
            $prescription->created_by = $this->currentUser->account->full_name;
            $prescription->save();

            if ($prescription->is_refill){
                //get original prescription
                $original_prescription = Model_Prescription::
                    getScopedQuery($this->currentUser)->
                    where('number', '=', $prescription->number)->
                    where('refill_number', '<', $prescription->refill_number)->
                    order_by('refill_number', 'asc')->
                    order_by('id', 'asc')->
                    find();
                //check to make sure original prescription exists
                if ($original_prescription->loaded()){
                    //get original prescription_image
                    $original_prescription_image = ORM::factory('PrescriptionImage')->
                        where('prescriptionimage.prescription_id', '=', $original_prescription->id)->
                        where('prescriptionimage.type', '=', 'prescription')->
                        find();
                    //check to see if original prescription_image exists
                    if ($original_prescription_image->loaded()){
                        //get original image;
                        $original_image = new Model_Image($original_prescription_image->image_id);
                        //check to see if original image exists
                        if ($original_image->loaded()){
                            //you got this far, you should have enough valid data to create a new prescription image to associate with the new prescription
                            $prescription_image = Model_PrescriptionImage::create_prescription_image($prescription, $original_image, $original_prescription_image->type, $original_prescription_image->display_order);
                            $now = new DateTime();
                            $prescription_image->set_created($now);
                            $prescription_image = $prescription_image->save_prescription_image();
                        }
                    }
                }
            }
            return $prescription;
        }

        throw new Exception('Prescription already exists', $prescription->id);
    }

    /**
     * here goes the implementation to query a CRX
     * to get the info of the patient history
     *
     * @param string $patientID the id of the patient on the rxkey system
     *
     * @return array of Telepharm_PMS_Prescription
     */
    public function getPatientHistory($patientID)
    {
        $query = DB::query(Database::SELECT, '
			SELECT * from `prescription_raw`
			WHERE operation_type = \'create\'
			AND prescription_id IN (select DISTINCT p.id from prescription p
			join prescription_store ps on (p.id = ps.prescription_id)
			join store s on (ps.store_id = s.id)
			where p.patient_id = :param_patient_id 
			AND s.id = :param_store_id)
			');
        $query->param(':param_patient_id', $patientID);
        $query->param(':param_store_id', $this->store->id);

        $result = [];
        foreach($query->execute() as $row)
        {
            $data = json_decode($row['serialized_data'], TRUE);
            $result[] = $this->makeResultStandard($data);
        }
        return $result;
    }

    /**
     * This method get the array decoded result
     * and create a standard prescription
     *
     * @param array $result json decoded result
     *
     * @result Telepharm_PMS_Prescription
     */
    public function makeResultStandard($result)
    {
        $rx_number = $this->getResultValue($result, ['SCRIPT_NUM']);

        $rx_refill_number = $this->getResultValue($result, ['REFILL_NUM'], 'integer');
        $rx_refill_number += 1;

        $patient_fname = $this->getResultValue($result, ['CUST_FIRST_NAME']);

        $patient_lname = $this->getResultValue($result, ['CUST_LAST_NAME']);

        $patient_dob = $this->getResultValue($result, ['CUST_DOB'], 'raw');
        $patient_dob = date_create_from_format('m/d/Y', $patient_dob);
        $patient_dob = $patient_dob ? $patient_dob->format('Y-m-d') : null;

        $patient_id = md5($patient_lname . $patient_fname . $patient_dob);

        $patient_phone = $this->getResultValue($result, ['CUST_PHONE']);

        $date_filled = $this->getResultValue($result, ['DATE_FILLED']);
        $date_filled = date_create_from_format('m/d/Y', $date_filled)->format('Y-m-d');

        $date_expires = $this->getResultValue($result, ['DATE_EXPIRES']);
        $date_expires = date_create_from_format('m/d/Y', $date_expires)->format('Y-m-d');

        $date_original = $this->getResultValue($result, ['DATE_WRITTEN']);
        $date_original = date_create_from_format('m/d/Y', $date_original)->format('Y-m-d');

        $patient_address1 = $this->getResultValue($result, ['CUST_ADDRESS']);

        $patient_city = $this->getResultValue($result, ['CUST_CITY']);

        $patient_state = $this->getResultValue($result, ['CUST_STATE']);

        $patient_zip = $this->getResultValue($result, ['CUST_ZIP']);

        $physician_fname = $this->getResultValue($result, ['PRE_FIRST_NAME']);

        $physician_lname = $this->getResultValue($result, ['PRE_LAST_NAME']);

        $physician_identifier = $this->getResultValue($result, ['PRE_DEA']);
        if (!$physician_identifier) { $physician_identifier = '-'; }

        $physician_phone = $this->getResultValue($result, ['PRE_PHONE']);
        if (!$physician_phone) { $physician_phone = '-'; }

        $dispense_qty = doubleval($this->getResultValue($result, ['QUANTITY']));

        $dispense_sig = $this->getResultValue($result, ['SIG']);

        $dispense_days_supply = $this->getResultValue($result, ['DAYS_SUPPLY']);
        $dispense_days_supply = preg_replace('/[^0-9]/', '', $dispense_days_supply);
        $dispense_days_supply = doubleval($dispense_days_supply);

        $dispense_refills = $this->getResultValue($result, ['REFILLS_LEFT']);

        $drug_ndc = $this->getResultValue($result, ['NDC']);

        $drug_mfg = $this->getResultValue($result, ['DRUG_MFG']);
        if (!$drug_mfg) { $drug_mfg = '-'; }

        $drug_name = $this->getResultValue($result, ['DRUG_NAME']);

        $drug_uom = $this->getResultValue($result, ['UOM']);


        # Fill in clas variables

        $prescription = new Telepharm_PMS_Result_Prescription;
        $prescription->number = $rx_number;
        $prescription->refillNumber = $rx_refill_number;
        $prescription->patientFirstName = $patient_fname;
        $prescription->patientLastName = $patient_lname;
        $prescription->patientID = $patient_id;
        $prescription->patientDob = $patient_dob;
        $prescription->patientPhone = $patient_phone;
        $prescription->isRefill = $rx_refill_number > 1 ? 1 : 0;
        $prescription->raw = $result; // To save full raw message

        $prescription->dateFilled = $date_filled;
        $prescription->dateExpires = $date_expires;
        $prescription->dateWritten = $date_original;

        $patientLocation = new Telepharm_PMS_Result_Location;
        $patientLocation->name = 'Patient Address';
        $patientLocation->address1 = $patient_address1;
        $patientLocation->city = $patient_city;
        $patientLocation->state = $patient_state;
        $patientLocation->zip = $patient_zip;
        $prescription->patientLocation = $patientLocation;

        $prescriber = new Telepharm_PMS_Result_Prescriber;
        $prescriber->firstName = $physician_fname;
        $prescriber->lastName = $physician_lname;
        $prescriber->phone = $physician_phone;
        $prescriber->npi = $physician_identifier;
        $prescriber->identifier = $physician_identifier;
        $prescription->prescriber = $prescriber;

        $dispense = new Telepharm_PMS_Result_Dispense;
        $dispense->qtyWritten = $dispense_qty;
        $dispense->qtyDispensed = $dispense_qty;
        $dispense->sig = null;
        $dispense->sigCoded = null;
        $dispense->sigExpanded = $dispense_sig;
        $dispense->daysSupply = $dispense_days_supply;
        $dispense->fillsOwed = $dispense_refills;
        $dispense->numRefills = $dispense_refills;
        $prescription->dispense = $dispense;

        $drug = new Telepharm_PMS_Result_Drug;
        $drug->newNdc = $drug_ndc;
        $drug->manf = $drug_mfg;
        $drug->description = $drug_name;
        $drug->packageSize = '-';
        $drug->strength = null;
        $drug->packageSizeUnitCode = $drug_uom;
        $prescription->drug = $drug;

        return $prescription;
    }

    /**
     * common interface to create a prescription using the standard object
     *
     * @param Telepharm_PMS_Result_Prescription $prescription the standard object
     * @param CurrentUser $currentUser the user creating the prescription
     *
     * @return Model_Prescription
     */
    public function createPrescription(Telepharm_PMS_Result_Prescription $prescription, CurrentUser $currentUser)
    {
        $this->currentUser = $currentUser;

        // Check if prescription already exists on the telepharm
        $foundPrescription = Model_Prescription::getScopedQuery($this->currentUser)->
            where('prescription.number', '=', $prescription->number)->
            where('prescription.canceled', '=', false)->
            find();

        if (!$prescription->isValid()){
            Kohana::$log->add(Log::ERROR, 'prescription errors '.
                var_export($prescription->errors, 1) );
            throw new Exception('Not valid Prescription information');
        }

        if ($prescription->refillNumber == 1){
            $prescription->isNew = true;
        }

        if ($foundPrescription->loaded() && $foundPrescription->refill_number == $prescription->refillNumber){
            return ['rx'=>$this->updateRawPrescription($prescription, $this->currentUser, $foundPrescription->id), 'update'=>true];
        }

        $db = Database::instance();
        try
        {
            $db->begin();
            // begin tx
            // Create Prescription
            $newPrescription = new Model_Prescription();
            $newPrescription->loadResultPrescription($prescription);
            $newPrescription->is_draft = 1;
            $newPrescription->account_id = $this->currentUser->account->id;
            $newPrescription->created_by = $this->currentUser->account->full_name;
            // create patient location if we have address info
            if ($prescription->patientLocation)
            {
                // Creates the location
                $location = new Model_Location();
                $location->loadLocation($prescription->patientLocation);
                $location->save();
                $newPrescription->patient_location_id = $location->id;
            }
            $newPrescription->save();
            $newPrescriptionID = $newPrescription->id;

            if ($prescription->raw)
            {
                $prescriptionRaw = new Model_PrescriptionRaw();
                $prescriptionRaw->prescription_id = $newPrescriptionID;
                $prescriptionRaw->operation_type = 'create';
                $prescriptionRaw->serialized_data = json_encode($prescription->raw, TRUE);
                $prescriptionRaw->save();
            }

            // Create prescription_store
            Model_PrescriptionStore::createWithPrescriptionAndStore(
                $newPrescriptionID,
                $this->store);

            // Create drug
            Model_PrescriptionDrug::createWithPrescription(
                $newPrescriptionID,
                $prescription->drug);

            // Create labels
            Model_PrescriptionLabel::createFromMedispan(
                $this->currentUser,
                $newPrescriptionID,
                $prescription->drug);

            // Create Dispense
            Model_PrescriptionDispense::createWithPrescription(
                $newPrescriptionID,
                $prescription->dispense);

            // Search for prescriber or create if not exists
            // Create prescription_prescriber
            Model_PrescriptionPrescriber::createWithPrescriptionAndPrescriber(
                $newPrescriptionID,
                $prescription->prescriber);

            // end tx
            $db->commit();

            // Create a log to show who created the prescription
            Model_PrescriptionLog::createLog(
                $this->currentUser,
                $newPrescription,
                'create');
        }
        catch (Exception $e)
        {
            $db->rollback();
            throw $e;
        }

        return ['rx'=>$newPrescription, 'update'=>false];
    }

    /**
     * This method looks through an array $result for keys in array $keys.
     * If key exists, return that key.  If none of the keys exist, return ''
     *
     * @param array  $result
     * @param array  $keys
     * @param string $format
     *
     * @return mixed
     */
    private function getResultValue($result, $keys, $format = 'string')
    {
        if (!is_array($keys)) $keys = [$keys];

        foreach ($keys as $key)
        {
            if (array_key_exists($key, $result))
            {
                $res = $result[$key];

                switch ($format)
                {
                    default:
                    case 'string':
                        $res = trim($res);
                        break;

                    case 'integer':
                        $res = intval($res);
                        break;

                    case 'raw':
                        break;
                }

                return $res;
            }
        }

        return '';
    }
}

