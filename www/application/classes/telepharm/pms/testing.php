<?php

/**
* Specific class to query the PMS for testing
*/
class Telepharm_PMS_Testing extends Telepharm_PMS
{
	/**
	* common interface to create a prescription without querying the PMS
	* @param string $prescriptionData the number to query on the PMS
	* @param CurrentUser $currentUser user creating the rx
	*
	* @return Model_Prescription
	*/
	public function createPreDraftPrescription($prescriptionData, CurrentUser $currentUser)
	{
		throw new Exception ('This PMS does not support this method');
	}

	/**
	* here goes the implementation to test the query
	* to get the info of the prescription
	*
	* @param string $prescriptionNumber the number to query on the PMS
	*
	* @return Telepharm_PMS_Prescription
	*/
	public function getPrescription($prescriptionNumber)
	{
		$pmsURL = $this->store->url;

		# Here we need to query the PMS using curl
		# for this class we just burn the json
		$jsonResult = <<<EOD
{
    "patientID": "1",
    "Firstname": "JERRY",
    "lastname": "JOHNSON",
    "dob": {
        "date": "1959-09-25 00:00:00",
        "timezone_type": 3,
        "timezone": "Europe\/Berlin"
    },
    "filled": {
        "date": "2010-09-25 00:00:00",
        "timezone_type": 3,
        "timezone": "Europe\/Berlin"
    },
    "firstFill": {
        "date": "2009-09-25 00:00:00",
        "timezone_type": 3,
        "timezone": "Europe\/Berlin"
    },
    "expires": {
        "date": "2013-09-25 00:00:00",
        "timezone_type": 3,
        "timezone": "Europe\/Berlin"
    },
    "rx": "12345",

    "SigCoded": "1T PO QD",
    "Sigexpanded": "TAKE 1 TABLET BY MOUTH EVERY DAY ",
    "refill": 7,
    "DaysSupply": 60,
    "qtywritten": "480.000",
    "DispensedQuantity": "60.000",
    "FillsOwed": "8.000",

    "identifier": 422639,
    "plname": "HEINEMAN",
    "pfname": "BRIAN",
    "npi": "1548341035",
    "phone": "6415227221",

    "newndc": "00143126301",
    "manf": "WEST-WARD",
    "description": "LISINOPRIL-HCTZ 20-12.5 MG",
    "packagesize": "100.000",
    "Strength": "20-12.5 MG",
    "PackageSizeUnitCode": "EA"
}
EOD;
		# Then we need to use the response of the curl
		# to transform to the standard format to create
		# the prescription
		$result = json_decode($jsonResult, true);
		$result['rx'] = $prescriptionNumber;
        $new_prescription = $this->createPrescription($this->makeResultStandard($result), $this->currentUser);
        if ($new_prescription->is_refill){
            //get original prescription
            $original_prescription = Model_Prescription::
                getScopedQuery($this->currentUser)->
                where('number', '=', $new_prescription->number)->
                where('refill_number', '=', 1)->
                find();
            //check to make sure original prescription exists
            if (!$original_prescription->loaded()){
                //for testing, if an original prescription doesn't exist at refill number 1, try any prescription less than the refill number that has an image
                $original_prescription = Model_Prescription::
                    getScopedQuery($this->currentUser)->
                    join('prescription_image')->
                    on('prescription_image.prescription_id', '=', 'prescription.id')->
                    where('prescription.number', '=', $new_prescription->number)->
                    where('prescription.refill_number', '<', $new_prescription->refill_number)->
                    where('prescription_image.type', '=', 'prescription')->
                    find();
                if (!$original_prescription->loaded()){
                    throw new Exception('No pre-existing refill exists for prescription number: '.$new_prescription->number.'. Tried looking for refills less than: '.$new_prescription->refill_number);
                }
            }
            //get original prescription_image
            $original_prescription_image = ORM::factory('PrescriptionImage')->
                where('prescriptionimage.prescription_id', '=', $original_prescription->id)->
                where('prescriptionimage.type', '=', 'prescription')->
                find();
            //check to see if original prescription_image exists
            if (!$original_prescription_image->loaded()){
                throw new Exception('Original prescription image not found - '.$new_prescription->number.'-'.'1');
            }
            //get original image;
            $original_image = new Model_Image($original_prescription_image->image_id);
            //check to see if original image exists
            if (!$original_image->loaded()){
                throw new Exception('Original image not found - '.$new_prescription->number.'-'.'1');
            }
            //you got this far, you should have enough valid data to create a new prescription image to associate with the new prescription
            $prescription_image = Model_PrescriptionImage::create_prescription_image($new_prescription, $original_image, $original_prescription_image->type, $original_prescription_image->display_order);
            $now = new DateTime();
            $prescription_image->set_created($now);
            $prescription_image = $prescription_image->save_prescription_image();
        }
        return $new_prescription;
	}

    /**
     * common interface to create a prescription using the standard object
     *
     * @param Telepharm_PMS_Result_Prescription $prescription the standard object
     * @param CurrentUser $currentUser the user creating the prescription
     *
     * @return Model_Prescription
     */
    public function createPrescription(Telepharm_PMS_Result_Prescription $prescription, CurrentUser $currentUser)
    {
        $this->currentUser = $currentUser;

        // Check if prescription already exists on the telepharm
        $foundPrescription = Model_Prescription::getScopedQuery($this->currentUser)->
            where('prescription.number', '=', $prescription->number)->
            where('prescription.refill_number', '=', $prescription->refillNumber)->
            where('prescription.canceled', '=', false)->
            find();
        //in test this prescription will already exist
        if ($foundPrescription->loaded())
            throw new Exception('Prescription already exists: '.$foundPrescription->number.'-'.$foundPrescription->refillNumber);

        $db = Database::instance();
        try
        {
            $db->begin();
            // begin tx
            // Create Prescription
            $newPrescription = new Model_Prescription();
            $newPrescription->loadResultPrescription($prescription);
            $newPrescription->is_draft = 1;
            $newPrescription->account_id = $this->currentUser->account->id;
            $newPrescription->created_by = $this->currentUser->account->full_name;
            $newPrescription->is_new = $prescription->isNew;
            // create patient location if we have address info
            if ($prescription->patientLocation)
            {
                // Creates the location
                $location = new Model_Location();
                $location->loadLocation($prescription->patientLocation);
                $location->save();
                $newPrescription->patient_location_id = $location->id;
            }
            $newPrescription->save();
            $newPrescriptionID = $newPrescription->id;

            if ($prescription->raw)
            {
                $prescriptionRaw = new Model_PrescriptionRaw();
                $prescriptionRaw->prescription_id = $newPrescriptionID;
                $prescriptionRaw->operation_type = 'create';
                $prescriptionRaw->serialized_data = json_encode($prescription->raw, TRUE);
                $prescriptionRaw->save();
            }

            // Create prescription_store
            Model_PrescriptionStore::createWithPrescriptionAndStore(
                $newPrescriptionID,
                $this->store);

            // Create drug
            Model_PrescriptionDrug::createWithPrescription(
                $newPrescriptionID,
                $prescription->drug);

            // Create labels
            Model_PrescriptionLabel::createFromMedispan(
                $this->currentUser,
                $newPrescriptionID,
                $prescription->drug);

            // Create Dispense
            Model_PrescriptionDispense::createWithPrescription(
                $newPrescriptionID,
                $prescription->dispense);

            // Search for prescriber or create if not exists
            // Create prescription_prescriber
            Model_PrescriptionPrescriber::createWithPrescriptionAndPrescriber(
                $newPrescriptionID,
                $prescription->prescriber);

            // end tx
            $db->commit();

            // Create a log to show who created the prescription
            Model_PrescriptionLog::createLog(
                $this->currentUser,
                $newPrescription,
                'create');
        }
        catch (Exception $e)
        {
            $db->rollback();
            throw $e;
        }

        return $newPrescription;
    }


    /**
	 * test the patient history api for testing
	 * 
	 * @param string $patientID 
	 * 
	 * @return array of Telepharm_PMS_Prescription
	 */
	public function getPatientHistory($patientID)
	{
		return [$this->getPrescription('100'), $this->getPrescription('200')];
	}

	/**
	* This method get the array decoded result
	* and create a standard prescription
	*
	* @param array $result json decoded result
	*
	* @result Telepharm_PMS_Prescription
	*/
	public function makeResultStandard($result)
	{
		$prescription = new Telepharm_PMS_Result_Prescription;
        $number = $result['rx'];
        $refill = 1;
        $numberParts = explode('-', $number);
        if (count($numberParts) == 1)
        {
            $number = trim($number);
        }
        elseif (count($numberParts) == 2)
        {
            $number = trim($numberParts[0]);
            $refillStr = trim($numberParts[1]);
            if (strlen($refillStr) > 0)
                $refill = $numberParts[1];
        }
		$prescription->number = $number;
		$prescription->refillNumber = $refill;
		$prescription->patientID = $result['patientID'];
		$prescription->patientFirstName = $result['Firstname'];
		$prescription->patientLastName = $result['lastname'];
		$prescription->patientDob = substr($result['dob']['date'],0,10);
		$prescription->isRefill = $refill > 1;
        $prescription->isNew = $refill == 1;
		$prescription->raw = $result; // To save full raw message

		$prescription->dateFilled = substr($result['filled']['date'],0,10);
		$prescription->dateWritten = substr($result['firstFill']['date'],0,10);
		$prescription->dateExpires = substr($result['expires']['date'],0,10);

		$prescriber = new Telepharm_PMS_Result_Prescriber;
		$prescriber->firstName = $result['pfname'];
		$prescriber->lastName = $result['plname'];
		$prescriber->phone = $result['phone'];
		$prescriber->npi = $result['npi'];
		$prescriber->identifier = $result['identifier'];
		$prescription->prescriber = $prescriber;

		$dispense = new Telepharm_PMS_Result_Dispense;
		$dispense->qtyWritten = $result['qtywritten'];
		$dispense->qtyDispensed = $result['DispensedQuantity'];
		$dispense->sig = $result['SigCoded'];
		$dispense->sigCoded = $result['SigCoded'];
		$dispense->sigExpanded = $result['Sigexpanded'];
		$dispense->daysSupply = $result['DaysSupply'];
		$dispense->fillsOwed = $result['FillsOwed'];
		$dispense->refillAuth = $result['refill'];
		$prescription->dispense = $dispense;

		$drug = new Telepharm_PMS_Result_Drug;
		$drug->newNdc = $result['newndc'];
		$drug->manf = $result['manf'];
		$drug->description = $result['description'];
		$drug->packageSize = $result['packagesize'];
		$drug->strength = $result['Strength'];
		$drug->packageSizeUnitCode = $result['PackageSizeUnitCode'];
		$prescription->drug = $drug;

		return $prescription;
	}
}
