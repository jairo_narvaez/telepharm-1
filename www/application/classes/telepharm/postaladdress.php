<?php

/**
 * Class that represent a particular address
 * 
 */
class TelePharm_PostalAddress implements JsonSerializable
{
	use traits\TelePharm_Getter, traits\TelePharm_Setter;

	public $street, $street2;
	public $city;
	public $code;
	public $level1, $level2, $level3;
	public $country = 'US';

	/**
	 * alias for get level1 in US
	 * 
	 * @return string
	 */
	protected function getState()
	{
		if ($this->country == 'US')
			return $this->level1; 
		else 
			throw new Exception('Invalid Property'); 
	}

	/**
	 * alias for set level1 in US
	 * 
	 * @param string $state 
	 * 
	 * @return void
	 */
	protected function setState($state) 
	{ 
		if ($this->country == 'US') 
			$this->level1 = $state; 
		else 
			throw new Exception('Invalid Property'); 
	}

	/**
	 * alias for get level2 in US
	 * 
	 * @return string
	 */
	protected function getCounty() 
	{ 
		if ($this->country == 'US') 
			return $this->level2; 
		else 
			throw new Exception('Invalid Property'); 
	}

	/**
	 * alias for set level2 in US
	 * 
	 * @param string $county
	 * 
	 * @return void
	 */
	protected function setCounty($county) 
	{ 
		if ($this->country == 'US') 
			$this->level2 = $county; 
		else 
			throw new Exception('Invalid Property'); 
	}

	/**
	 * alias for get code in US
	 * 
	 * @return string
	 */
	protected function getZipCode() 
	{ 
		if ($this->country == 'US') 
			return $this->code; 
		else 
			throw new Exception('Invalid Property'); 
	}

	/**
	 * alias for set code in US
	 * 
	 * @param string $zipCode 
	 * 
	 * @return void
	 */
	protected function setZipCode($zipCode)
	{	
		if ($this->country == 'US') 
			$this->code= $zipCode; 
		else 
			throw new Exception('Invalid Property'); 
	}

	/**
	 * string representation of the object
	 * 
	 * @return string
	 */
	public function __toString()
	{
		return "{$this->street} {$this->street2}\n{$this->city}, {$this->level1}, {$this->code}";
	}

	/**
	 * transform the object to an associative array
	 * 
	 * @return array
	 */
	public function asArray()
	{
		return get_object_vars($this);
	}

	/**
	 * return the object as an array for being serialized
	 * 
	 * @return array
	 */
	public function jsonSerialize()
	{
		return $this->asArray();
	}
} 
