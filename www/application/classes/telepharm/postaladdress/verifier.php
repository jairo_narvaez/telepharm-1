<?php

/**
 * Abstract class for all postal address verifiers
 * 
 * define the verify and factory methods
 * 
 */
abstract class TelePharm_PostalAddress_Verifier
{
	use traits\TelePharm_Getter, traits\TelePharm_Setter;

	/**
	 * This needs to be implemented on the concrete verifier class
	 * 
	 * @param TelePharm_PostalAddress $address
	 * @param mixed            $maximumSuggestions Optional, defaults to 0 which means unlimited
	 * 
	 * @return array <TelePharm_PostalAddress_Verifier_Response>
	 */
	abstract public function verify(TelePharm_PostalAddress $address, $maximumSuggestions = 0 /* unlimited */);

	/**
	 * This method creates a new concrete class based on a config or a param
	 * 
	 * @param string $name Optional, defaults to null. (supported values now are Test and SmartyStreets)
	 * 
	 * @return TelePharm_PostalAddress_Verifier_{$name}
	 */
	public static function factory($name = null)
	{	
		if (!$name)
		{
			$name = Kohana::$config->load('application.postal_address_verifier');
			if (!$name) throw new Exception("No postal address verifier specified");
		}
		
		$classname = 'TelePharm_PostalAddress_Verifier_'.$name;
		$reflector = new ReflectionClass($classname);
		$verifier = $reflector->newInstance();
		if ($reflector->hasMethod('setAuthToken')) {
			// Set auth token
			$verifier->authToken = Kohana::$config->load(strtolower($name).'.auth_token');
		}
		return $verifier;
	}	
}
