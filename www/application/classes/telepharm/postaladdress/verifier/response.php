<?php

/**
 * Representation of an address response from a remote service
 * 
 */
class TelePharm_PostalAddress_Verifier_Response implements JsonSerializable
{
	/**
	 * object of postal address
	 * 
	 * @var TelePharm_PostalAddres
	 */
	public $address;
	/**
	 * true, false or NULL
	 *
	 * @var boolean
	 */
	public $deliverable = NULL;
	/**
	 * true, false or NULL
	 *
	 * @var boolean 
	 */
	public $missingData = NULL;

	/**
	 * Constructor
	 * 
	 */
	public function __construct()
	{
		$this->address = new TelePharm_PostalAddress;
		$this->deliverable = NULL;
		$this->missingData = NULL;
	}

	/**
	 * data to be encoded as json
	 * 
	 * @return array
	 */
	public function jsonSerialize() {
		return array_merge(get_object_vars($this->address), ['description' => $this->address->__toString()]);
	}
} 
