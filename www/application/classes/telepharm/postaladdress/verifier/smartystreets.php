<?php

/**
 * Implementation class to verify an address
 * 
 * using the service Smarty Streets
 * https://smartystreets.com/
 * 
 */
class TelePharm_PostalAddress_Verifier_SmartyStreets extends TelePharm_PostalAddress_Verifier
{
	/**
	 * Hold the value of the Auth Token from
	 * Smarty Streets:
	 * https://smartystreets.com/account/api/rest
	 * 
	 * @var string
	 */
	protected $_authToken;

	/**
	 * Return the Auth Token for smarty streets service
	 * 
	 * @return string
	 */
	protected function getAuthToken() 
	{
		return $this->_authToken;
	}

	/**
	 * Set the Auth Token for the smarty streets service
	 * 
	 * @param string $authToken 
	 * 
	 * @return void
	 */
	protected function setAuthToken($authToken)
	{
		$this->_authToken = $authToken;
	}

	/**
	 * check with the smarty street api if an address is valid
	 *
	 * use $address->zipCode, $address->state, etc, instead of the low level fields
	 *
	 * make request. if web service is down, throw exception
	 *
	 * log entire response into kohana log so we can analyze real data
	 * 
	 * @param TelePharm_PostalAddress $address
	 * @param mixed            $maximumSuggestions Optional, defaults to 0 which means unlimited
	 * 
	 * @return array <TelePharm_PostalAddress_Verifier_Response>
	 */
	public function verify(TelePharm_PostalAddress $address, $maximumSuggestions = 0)
	{
		if ($address->country != 'US') 
			throw new Exception('Service not available outside the US');

		$api_result = '';
		$api_url = 'https://api.qualifiedaddress.com/street-address/';
		$api_url .= '?street='.urlencode($address->street);
		if ($address->street2)
			$api_url .= '&street2='.urlencode($address->street2);
		$api_url .= '&city='.urlencode($address->city);
		$api_url .= '&state='.urlencode($address->state);
		$api_url .= '&zipcode='.urlencode($address->zipCode);
		if ($maximumSuggestions >= 0 && $maximumSuggestions <= 10)
			$api_url .= '&candidates='.urlencode($maximumSuggestions);
		// Log without the auth token
		Log::instance()->add(Log::DEBUG, 'Smarty Street API url: '. $api_url);
		$api_url .= '&auth-token='.urlencode($this->_authToken);

		$curl = curl_init($api_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$api_result = curl_exec($curl);
		$response = json_decode($api_result, TRUE);	
		if ($response === FALSE) 
		{
			Log::instance()->add(Log::ERROR, 'Smarty Street API error: '. $api_result);
			throw new Exception('error on remote server: ' . $api_result);
		}
		curl_close($curl);

		Log::instance()->add(Log::DEBUG, 'Smarty Street API result: '. $api_result);

		$addresses = [];
		foreach($response as $candidate)
		{
			$comps = $candidate['components'];
			$result = new TelePharm_PostalAddress_Verifier_Response;
			$result->address->country = 'US'; # hard coded for now
			$result->address->street  = $candidate['delivery_line_1'];
			$result->address->city    = $comps['city_name'];
			$result->address->state   = $comps['state_abbreviation'];
			$result->address->zipCode = rtrim($comps['zipcode'].'-'.$comps['plus4_code'], '-');
			$addresses[] = $result;
		}
		return $addresses;
	}
} 
