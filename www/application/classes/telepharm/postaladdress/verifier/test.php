<?php

/**
 * Fake verifier of a postal address
 *
 * Used to unit test the postal address stuff
 * 
 */
class TelePharm_PostalAddress_Verifier_Test extends TelePharm_PostalAddress_Verifier
{
	/**
	 * Check with fake data using a regexp
	 *
	 * do a regexp on $address->street, searching for 'AV{digit}'
	 *
	 * if no match, then return the same address verbatim
	 *
	 * otherwise:
	 * 
	 * digit == 0: no matches
	 * digit == 1-5: that number of matches, randomly replacing AV{digit} with a random integer
	 * digit >5: throw exception
	 * 
	 * @param TelePharm_PostalAddress $address
	 * @param int              $maximumSuggestions Optional, defaults to 0 which means unlimited suggestions
	 * 
	 * @return array <TelePharm_PostalAddress_Verifier_Response>
	 */
	public function verify(TelePharm_PostalAddress $address, $maximumSuggestions = 0)
	{
		$pattern = '/(AV(\d)*)/i';
		if (preg_match($pattern, $address->street, $matches) == 0) 
		{
			$result = new TelePharm_PostalAddress_Verifier_Response;
			$result->address = $address;
			return [$result];
		} 
		else 
		{
			if (isset($matches[2])) 
			{
				$number = intVal($matches[2]);
				if ($number == 0) 
				{
					return [];
				} 
				else if ($number > 0 and $number <= 5) 
				{
					$addresses = [];
					$street = $address->street;
					for($i = 0; $i < $number; $i++) 
					{
						$result = new TelePharm_PostalAddress_Verifier_Response;
						$result->address = clone $address;
						$result->address->street = preg_replace($pattern, "AV".($i+1), $street);
						$result->address->zipCode = 60600 + $i;
						$addresses[] = $result;
					}
					return $addresses;
				} 
				else 
				{
					throw new Exception('test exception');
				}
			}
			else
			{
				$result = new TelePharm_PostalAddress_Verifier_Response;
				$result->address = $address;
				return [$result];
			}
		}	
	}
} 
