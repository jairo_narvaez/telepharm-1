<?php
class TelePharm_Proc
{
	private $proc;
	private $pipes;
	
	public function __construct($cmd)
	{
		$this->proc = proc_open($cmd, array(array('pipe', 'r'), array('pipe', 'w'), array('pipe', 'w')), $this->pipes);
		if (!is_resource($this->proc))
			throw new Exception("Unable to open process");
	}
	
	public function __get($key)
	{
		switch ($key)
		{
			case 'stdin':
				return $this->pipes[0];
			
			case 'stdout':
				return $this->pipes[1];
				
			case 'stderr':
				return $this->pipes[2];
		}
	}
	
	public function write($data, $close = false)
	{
		fwrite($this->stdin, $data);
		if ($close) fclose($this->stdin);
	}
	
	public function read($close = false)
	{
		$output = stream_get_contents($this->stdout);
		if ($close) fclose($this->stdout);
		return $output;
	}
	
	public function readStdErr($close = false)
	{
		$output = stream_get_contents($this->stderr);
		if ($close) fclose($this->stderr);
		return $output;
	}
	
	public function close()
	{
		#@fclose($this->stdin);
		#@fclose($this->stdout);
		#@fclose($this->stderr);
		
		return proc_close($this->proc);
	}
}