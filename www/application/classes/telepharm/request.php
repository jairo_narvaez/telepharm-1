<?php
class TelePharm_Request extends Kohana_Request
{
	public $webview = false;
	public $api_session = null;
	public $response_format = 'html';
	
	/**
	 * this var holds information to pass between hmvc calls
	 *
	 * @var associative array
	 **/
	public $hmvc_answer = array();
	public $hmvc = false;
	public $redirect_url = null;
	
	public function redirect($url = '', $code = 302)
	{
		if (strpos($url, '://') === FALSE)
		{
			// Make the URI into a URL
			$url = URL::site($url, TRUE);
		}
		
		if ($this->hmvc)
		{
			$this->redirect_url = $url;
			return;
		}
		
		if ($this->response_format == 'javascript')
		{
			echo '<script type="text/javascript">';
			echo 'if (parent.iframe_onload) parent.iframe_onload({redir: "'.$url.'"});';
			echo '</script>';
			exit;
		}
		else
		{
			if (!$this->webview) parent::redirect($url, $code);		
			throw new Exception('redirect');
		}
	}
}