<?php
class TelePharm_Route extends Kohana_Route
{
	const REGEX_SEGMENT = '[^/;?]++'; # TODO: maybe only put this in routes that need it (periods & commas in segments)
	
	public function matches($uri)
	{
		$params = parent::matches($uri);
		
		if ($params)
		{		
			$params['controller'] = str_replace('-', '_', $params['controller']);
			$params['action'] = str_replace('-', '_', $params['action']);
		}
				
		return $params;
	}
	
	public static function clear()
	{
		self::$_routes = array();
	}
	
	/*
		This was used on PPMD to support multiple domains on a single instance of Kohana.

	public static function set($name, $uri = NULL, $regex = NULL)
	{
		$urls = Kohana::$config->load('application.urls');
		
		foreach ($urls as $i => $url)
		{
			$urls[$i] = parse_url($url);
			$urls[$i]['prefix'] = ltrim($urls[$i]['path'], '/');
		}
		
		switch ($name)
		{
			case 'www':
				return parent::set('default', '(<controller>(/<action>(/<id>)))')->defaults(
					array(
						'controller' => 'welcome',
						'action'     => 'index',
					)
				);
				
			default:
				return parent::set($name, $uri, $regex);
		}
	}
	*/
}
