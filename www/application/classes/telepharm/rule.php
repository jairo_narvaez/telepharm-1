<?php
abstract class TelePharm_Rule
{
	public static function factory($name)
	{
		if (is_callable($name))
		{
			return new Rule_Callback($name);
		}
		else
		{
			$reflector = new ReflectionClass("TelePharm_Rule_$name");
			return $reflector->newInstanceArgs(array_slice(func_get_args(), 1));
		}
	}

	public function __construct()
	{
		
	}

	abstract public function exec();

	final public function test()
	{
		$result = call_user_func_array([$this, 'exec'], func_get_args());
		return $result->error == false;
	}

	public function asArray()
	{
		return ['name' => str_replace('_', '.', str_replace('Rule_', '', get_called_class()))];
	}
}