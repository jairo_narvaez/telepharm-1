<?php
class TelePharm_Rule_Equals extends TelePharm_Rule_Value
{
	protected $value;

	public function __construct($value)
	{
		$this->value = $value;
	}

	protected function execValue($data)
	{
		return $data !== $this->value ?
			new TelePharm_RuleResult_Failure('Does not equal value') : new TelePharm_RuleResult_Success();
	}
}