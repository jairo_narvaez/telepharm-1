<?php
class TelePharm_Rule_FieldComparator extends TelePharm_Rule_Value
{
	protected $field, $comparison;

	public function __construct(TelePharm_Field $field, $comparison)
	{
		$this->field = $field;
		$this->comparison = $comparison;
	}

	protected function execValue($value)
	{
		$compareTo = $this->field->saneValue;

		switch ($this->comparison)
		{
			case '<':
				$error = $value >= $compareTo;
			break;

			case '<=':
				$error = $value > $compareTo;
			break;

			case '==':
				$error = $value != $compareTo;
			break;

			case '>':
				$error = $value <= $compareTo;
			break;

			case '>=':
				$error = $value < $compareTo;
			break;

			default:
				$error = false;
		}

		return $error ? new TelePharm_RuleResult_Failure('comparison') : new TelePharm_RuleResult_Success();
	}

	public function asArray()
	{
		return [
			'name' => 'DI.FieldComparator',
			'field' => $this->field->fullyQualifiedId,
			'comparison' => $this->comparison
		];
	}
}