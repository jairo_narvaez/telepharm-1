<?php
class TelePharm_Rule_FileType extends TelePharm_Rule_Value
{
	protected $_file_types_arr;

	public function __construct($file_types_arr)
	{
		$this->_file_types_arr = $file_types_arr;
		parent::__construct();	
	}

	protected function execValue($data)
	{
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$type =  $data['tmp_name'] != '' ? $finfo->file($data['tmp_name']) : '';
		
		return !in_array($type, $this->_file_types_arr) ?
			new TelePharm_RuleResult_Failure('incorrect-file-type') : new TelePharm_RuleResult_Success();
	}
}
