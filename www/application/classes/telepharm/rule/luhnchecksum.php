<?php
class TelePharm_Rule_LuhnChecksum extends TelePharm_Rule_Value
{
	protected function execValue($value)
	{
		static $digits = [
			0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
			0, 2, 4, 6, 8, 1, 3, 5, 7, 9
		];

		$failure = false;

		if (trim($value) != '')
		{
			if (!preg_match('/^[\d\s]+$/', $value))
			{
				$failure = true;
			}
			else
			{
				$value = preg_replace('/[^\d]+/', '', $value);

				$sum = 0; $offset = 0;
				for ($i = strlen($value) - 1; $i >= 0; --$i)
				{
					$sum += $digits[$offset + (int) $value[$i]];
					$offset = 10 - $offset;
				}
				$failure = ($sum % 10) != 0;
			}
		}

		return $failure ? new TelePharm_RuleResult_Failure('luhn-checksum') : new TelePharm_RuleResult_Success();
	}
}
