<?php
class TelePharm_Rule_NotEmpty extends TelePharm_Rule_Value
{
	protected function execValue($data)
	{
		return ($data === '' || $data === null) ?
			new TelePharm_RuleResult_Failure('Cannot be empty') : new TelePharm_RuleResult_Success();
	}
}