<?php
class TelePharm_Rule_Range extends TelePharm_Rule_Value
{
	protected $_min;
	protected $_max;

	public function __construct($min, $max)
	{
		$this->_min = $min;
		$this->_max = $max;
	}

	protected function execValue($data)
	{
		return ($data < $this->_min || $data > $this->_max) ?
			new TelePharm_RuleResult_Failure('out-of-range') : new TelePharm_RuleResult_Success();
	}
}