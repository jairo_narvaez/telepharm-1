<?php
class TelePharm_Rule_RequiredField extends TelePharm_Rule_Value
{
	protected $field;

	public function __construct(TelePharm_Field $field)
	{
		$this->field = $field;
		parent::__construct();
	}

	protected function execValue($value)
	{
		if ($this->field instanceof TelePharm_Field_Checkbox)
			$missing = ($value != '1');
		else
			$missing = ($value === '' || $value === null);

		return $missing ?
			new TelePharm_RuleResult_Failure('missing') : new TelePharm_RuleResult_Success();
	}
}