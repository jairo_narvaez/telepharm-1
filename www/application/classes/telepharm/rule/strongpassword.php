<?php
class TelePharm_Rule_StrongPassword extends TelePharm_Rule_Value
{
	protected function execValue($value)
	{
		return (strlen($value) < 6) ? new TelePharm_RuleResult_Failure('weak-password') : new TelePharm_RuleResult_Success();

	}
}