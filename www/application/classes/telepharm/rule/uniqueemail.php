<?php
class TelePharm_Rule_UniqueEmail extends TelePharm_Rule_Value
{
	protected function execValue($data)
	{
		$person = Model::factory('person')->where('email', '=', $this['email']->value)->find();
		return $person->loaded() ? new TelePharm_RuleResult_Failure('Email already exists.') : new TelePharm_RuleResult_Success();
	}
}