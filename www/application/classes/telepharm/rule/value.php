<?php

# This rule is meant to be subclassed whenever a rule simply needs to test a single value.

abstract class TelePharm_Rule_Value extends TelePharm_Rule
{
	final public function exec()
	{
		$args = func_get_args();
		if (!$args) throw new InvalidArgumentException();

		return $this->execValue($args[0]);
	}

	abstract protected function execValue($value);
}