<?php
class TelePharm_RuleCollection implements IteratorAggregate, Countable
{
	protected $rules = [];

	public function count()
	{
		return count($this->rules);
	}

	public function add($rule)
	{
		if ($rule instanceof TelePharm_Rule)
		{
			$this->rules[] = $rule;
		}
		else if (is_array($rule))
		{
			foreach ($rule as $r)
				$this->add($r);
		}
		else if (is_callable($rule))
		{
			$this->add(new TelePharm_Rule_Callback($rule));
		}
		else
		{
			throw new InvalidArgumentException();
		}

		return $this;
	}

	public function remove($rule)
	{
		if (is_array($rule))
		{
			foreach ($rule as $r)
				$this->remove($r);
		}
		else
		{
			foreach ($this->rules as $i => $r)
			{
				if ($r == $rule)
					unset($this->rules[$i]);
			}
		}

		return $this;
	}

	public function testRules($value)
	{
		foreach ($this->rules as $rule)
		{
			if (!$rule->test($value))
				return false;
		}

		return true;
	}

	public function getIterator()
	{
		return new ArrayIterator($this->rules);
	}
}