<?php
class TelePharm_RuleResult implements ArrayAccess, TelePharm_Untaintable
{
	use traits\TelePharm_Getter, traits\TelePharm_Setter;
	use traits\TelePharm_ArrayAccess;

	protected $code;

	public function __construct($code = null)
	{
		$this->code = $code;
	}

	protected function getCode()
	{
		return $this->code;
	}

	protected function setCode($code)
	{
		$this->code = $value;
	}

	protected function getMessage()
	{
		$message = Kohana::$config->load('error-codes.'.$this->code);

		return $message ?: $this->code;
	}
	
	public function isTainted()
	{
		return true;
	}

	public function untaint()
	{
		return TelePharm_TaintedData::clean(array_merge(
			$this->_arrayData, [
				'code' => $this->code,
				'message' => $this->message
			]
		));		
	}

	public function __toString()
	{
		return (string) $this->message;
	}
}