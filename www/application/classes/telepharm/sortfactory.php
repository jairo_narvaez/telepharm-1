<?php
#
# The SortFactory class can be used to generate callbacks for use with the usort function.
#
class TelePharm_SortFactory
{
	const ASC = 1, DESC = -1;
	
	# Sort an array by the key specified
	public static function byKey($key, $order = self::ASC, $op = 'strcmp')
	{
		return function($a, $b) use ($key, $order, $op)
		{
			return call_user_func($op, $a[$key], $b[$key]) * $order;
		};
	}
}