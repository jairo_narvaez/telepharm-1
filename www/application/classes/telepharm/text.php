<?php defined('SYSPATH') or die('No direct access allowed.');

class TelePharm_Text extends Kohana_Text
{
	public static function gender($gender)
	{
		if(strtolower($gender) == 'm')
			return 'Male';
		if(strtolower($gender) == 'f')
			return 'Female';
	}

	public static function possessive($str)
	{
	return $str.(substr($str, -1) != 's' ? '\'s' : '\'');
	}
	
	public static function plural($count, $str1, $str2 = null)
	{
		if (!$str2) $str2 = $str1.'s';
		
		return $count.' '.($count == 1 ? $str1 : $str2);
	}
	
	public static function boolean($str)
	{
		if($str == 1)
			return 'Yes';
		if($str == 0)
			return 'No';
	}

	public static function height($str)
	{
		$parts = explode(" ", $str);
		if (count($parts)==2)
		{
			$parts[0] = $parts[0]."'";
			$parts[1] = $parts[1].'"';
		}
		elseif (count($parts)==1)
		{
			$parts[0] = $parts[0]."'";
		}
		return implode(' ', $parts);
	}
	
	public static function weight($str)
	{
		return $str.' lbs';
	}
	
	/**
	 * return the name of an abnormal flag
	 *
	 * @return string
	 **/
	public static function abnormal_text($abnormal_flag)
	{
		switch ($abnormal_flag) {
			case 'H':
				return 'High';
			case 'HH':
				return 'Critical High';
			case 'L':
				return 'Low';
			case 'LL':
				return 'Critical Low';
			case 'A':
				return 'Abnormal';
			case 'A!':
				return 'Critical Abnormal';
			default:
				return 'Unknow flag: ' . $abnormal_flag;
		}
	}
}