<?php

class TelePharm_TextMessage
{
	private $subject;
	private $body;
	private $number;
	
	public function __get($key)
	{
		switch ($key)
		{
			case 'subject':
			case 'body':
			case 'number':
				return $this->$key;

			case 'digits':
				return preg_replace('/[^0-9]/', '', $this->number);			
		}
		
		throw new Exception("Invalid property $key");
	}
	
	public static function compose()
	{
		return new self();
	}
	
	public function subject($subject)
	{
		$this->subject = $subject;
		return $this;
	}
	
	public function body($body)
	{
		$this->body = $body;
		return $this;
	}
	
	public function number($number)
	{
		$this->number = $number;
		return $this;
	}
}
