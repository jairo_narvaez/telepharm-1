<?php

abstract class TelePharm_TextMessage_Transport
{
	abstract public function send(TelePharm_TextMessage $message);
	
	public static function factory($name = null)
	{
		if (!$name)
		{
			$name = Kohana::$config->load('application.textmessage_transport');
			if (!$name) throw new Exception("No TextMessage transport type specified");
		}
		
		$classname = 'TelePharm_TextMessage_Transport_'.$name;
		$reflector = new ReflectionClass($classname);
		return $reflector->newInstance();
	}
}

