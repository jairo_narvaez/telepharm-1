<?php

class TelePharm_TextMessage_Transport_GroupTexting extends TelePharm_TextMessage_Transport
{
	private $username, $password;

	public function __construct()
	{
		$this->username = Kohana::$config->load('grouptexting.username');
		$this->password = Kohana::$config->load('grouptexting.password');
	}

	public function send(TelePharm_TextMessage $message)
	{
		$data = [
			'User' => $this->username,
			'Password' => $this->password,
			'PhoneNumbers' => [$message->digits],
			'Subject' => $message->subject,
			'Message' => $message->body
		];
		
		$curl = curl_init(Kohana::$config->load('grouptexting.url'));
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$response = curl_exec($curl);
		curl_close($curl);
		
		Log::instance()->add(Log::DEBUG, "grouptexting response: ".$response);
		$json = json_decode($response);
		if (!$json)
		{
			throw new Exception("Unexpected response from GroupTexting");
		}
		
		$json = $json->Response;
		
		if ($json->Status == 'Failure')
		{
			if (!empty($json->Errors))
			{
				$errors = $json->Errors;
				foreach ($errors as $error)
				{
					Log::instance()->add(Log::DEBUG, "SMS Error: " . $error);
				}
			}
			
			throw new Exception("Failed to sent Text Message to Phone Number: ".$message->number);
		}
		
		return $this;
	}
}
