<?php

class TelePharm_TextMessage_Transport_Log extends TelePharm_TextMessage_Transport
{
	public function send(TelePharm_TextMessage $message)
	{
		$text = "Subject: ".$message->subject."\n";
		$text .= "Number: ".$message->number."\n";
		$text .= "Message: ".$message->body;
		Log::instance()->add(Log::NOTICE, $text);
		return $this;
	}
}
