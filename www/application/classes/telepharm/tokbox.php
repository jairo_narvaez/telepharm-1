<?php

/**
 * Contact tokbox servers to generate session/token
 */
class Telepharm_Tokbox
{
	/**
	 * Generate the info to process a call
	 * 
	 * @return array
	 */
	public function generate()
	{
		$openTokConfig = Kohana::$config->load('opentok');
		$apiObj = new OpenTokSDK($openTokConfig['api_key'], $openTokConfig['api_secret']);
		$sessionId = $apiObj->createSession( $_SERVER["REMOTE_ADDR"], [
			SessionPropertyConstants::P2P_PREFERENCE=> "enabled"
		] );
		$token = $apiObj->generateToken($sessionId);
		return [$sessionId, $token];
	}
}
