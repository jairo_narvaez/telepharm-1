<?php defined('SYSPATH') or die('No direct script access.');

interface TelePharm_Untaintable
{
	public function untaint();
	public function isTainted();
}