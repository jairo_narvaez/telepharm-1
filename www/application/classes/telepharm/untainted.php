<?php defined('SYSPATH') or die('No direct script access.');

/*
	This is an empty interface. A class should implement it if
	it's known to be safe for displaying.
*/
interface TelePharm_Untainted { }