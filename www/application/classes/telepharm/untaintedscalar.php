<?php defined('SYSPATH') or die('No direct script access.');
	class TelePharm_UntaintedScalar implements TelePharm_Untainted, Countable, JsonSerializable
	{
		private $value;
		
		public function __construct($value)
		{
			$this->value = $value;
		}
		
		public function __get($key)
		{
			if ($key == 'value')
				return $this->value;
			
			throw new Exception("Invalid property.");
		}
		
		public function __toString()
		{
			return (string) $this->value;
		}
		
		public function count()
		{
			return count($this->value);
		}

		public function jsonSerialize()
		{
			return $this->value;
		}
	}
