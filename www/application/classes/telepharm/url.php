<?php
class TelePharm_URL extends Kohana_URL
{
	static protected $_asset_path = null;

	/**
	 * Concatenate two pieces of a url together, joining them with a slash.
	 * It makes sure that the pieces are always separated by a single slash.
	 *
	 * @param string $base  The beginning of the url
	 * @param string $tail  The fragment being appended to the url
	 * @return string       The full URL
	 */
	static public function concat($base, $tail)
	{
		$a = substr($base, -1, 1) == '/';
		$b = $tail != '' && $tail[0] == '/';

		if ($a ^ $b)
		{
			# one has a slash, the other doesn't, so just glue them together
			return $base.$tail;
		}
		else if ($a)
		{
			# both have a slash
			return $base.ltrim($tail, '/');
		}
		else
		{
			# neither have a slash
			return $base.'/'.$tail;
		}		
	}

	static public function asset($url = '')
	{
		# remove http to use in secure/insecure installations
		$urls = Kohana::$config->load('application.urls');
		return str_replace("http://","//",$urls['assets']).$url;
	}

	static public function asset_path($url = '')
	{
		if (self::$_asset_path === null)
		{
			self::$_asset_path = Kohana::$config->load('application.asset_path');
			if (!self::$_asset_path)
				throw new Exception('application.asset_path needs to be set in the application configuration and set to the absolute path of the asset folder');
		}

		return self::$_asset_path.'/'.$url;
	}
	
	static public function www($url = '')
	{
		$urls = Kohana::$config->load('application.urls');
		return $urls['www'].$url;
	}
}
