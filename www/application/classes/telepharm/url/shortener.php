<?php

abstract class TelePharm_URL_Shortener
{
	abstract public function make($url);
	
	public static function factory($name = null)
	{
		Log::instance()->add(Log::ERROR, "CREATE Shortener");
		if (!$name)
		{
			$name = Kohana::$config->load('application.url_shortener');
			if (!$name) throw new Exception("No url shortener specified");
		}
		
		$classname = 'TelePharm_URL_Shortener_'.$name;
		Log::instance()->add(Log::ERROR, "url shortener classname: ".$classname);
		$reflector = new ReflectionClass($classname);
		return $reflector->newInstance();
	}
}
