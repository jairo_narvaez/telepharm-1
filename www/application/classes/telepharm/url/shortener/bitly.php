<?php

class TelePharm_URL_Shortener_Bitly extends TelePharm_URL_Shortener
{
	public function make($url)
	{
		$bitly_url = Kohana::$config->load('bitly.url');
		$bitly_user = Kohana::$config->load('bitly.username');
		$bitly_apikey = Kohana::$config->load('bitly.apikey');
		
		$bitly_url .= "?".http_build_query(['login' => $bitly_user, 'apikey' => $bitly_apikey, 'longUrl' => $url]);
		
		Log::instance()->add(Log::DEBUG, "url: ".$bitly_url);
		$curl = curl_init($bitly_url);
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		
		$response = curl_exec($curl);
		curl_close($curl);
		Log::instance()->add(Log::DEBUG, "response: ".$response);
		
		$json = json_decode($response);
		if (!$json)
		{
			throw new Exception("Unexpected response from Bitly");
		}
		
		if ($json->status_code != 200)
		{
			Log::instance()->add(Log::DEBUG, "Bitly Error: ".$json->status_txt);
			throw new Exception("Failed to get short url");
		}
		
		return $json->data->url;
	}
}