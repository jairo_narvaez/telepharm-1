<?php
class TelePharm_UUID
{
	private $parts;
	private $_base64;
	private $_hex;

	public static function generate($type = 'base64')
	{
		$self = new static();
		return $self->__get($type);
	}

	public function __construct()
	{
		$this->parts = array(mt_rand(0, 0xffff), mt_rand(0, 0xffff),
			mt_rand(0, 0xffff),
			mt_rand(0, 0x0fff) | 0x4000,
			mt_rand(0, 0x3fff) | 0x8000,
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

	public function __get($key)
	{
		if ($key == 'base64')
			return $this->getBase64();
		else if ($key == 'hex')
			return $this->getHex();
		else
			throw new Exception("Unknown parameter $key");
	}

	private function hexstr($dec)
	{
		return str_pad(dechex($dec), 4, '0', STR_PAD_LEFT);
	}

	private function getBase64()
	{
		if ($this->_base64 == null)
		{
			$this->_base64 = '';
			foreach ($this->parts as $part)
			{
				$this->_base64 .= chr(($part & 0xff00) >> 8).chr($part & 0xff);
			}
			$this->_base64 = strtr(substr(base64_encode($this->_base64), 0, -2), array('/' => '_', '+' => '-'));
		}

		return $this->_base64;
	}

	private function getHex()
	{
		if ($this->_hex == null)
		{
			$this->_hex = sprintf('%s%s-%s-%s-%s-%s%s%s',
				$this->hexstr($this->parts[0]), $this->hexstr($this->parts[1]), 
				$this->hexstr($this->parts[2]), 
				$this->hexstr($this->parts[3]), 
				$this->hexstr($this->parts[4]), 
				$this->hexstr($this->parts[5]), $this->hexstr($this->parts[6]), $this->hexstr($this->parts[7])
			);
		}

		return $this->_hex;
	}
}