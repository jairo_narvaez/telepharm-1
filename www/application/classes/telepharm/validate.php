<?php
class TelePharm_Validate extends Kohana_Validate
{
	public function fill(&$output)
	{
		$is_valid = $this->check();
		$output = $this->as_array();
		$output['_errors'] = $this->errors();
				
		return $is_valid;
	}
}