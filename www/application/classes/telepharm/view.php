<?php defined('SYSPATH') or die('No direct script access.');

/* Enables support of encoding tainted (insecure) data.
 *
 */

class TelePharm_View extends Kohana_View implements TelePharm_Untainted
{
	private $_auto_taint = true;
	private $_tainted = array();
	
	/**
	 * Set the default behavior for variables that weren't explicitly set.
	 *   true to automatically encode any unknown variable
	 *   false to do nothign to unknown variables
	 *
	 * @return $this
	 * @author Matthew Leverton
	 **/
	public function auto_taint($bool)
	{
		$this->_auto_taint = $bool ? true : false;
		
		return $this;
	}
	
	/**
	 * Mark the passed variables as tainted. Must be called *before* the
	 * variable is set. Can be called in several ways:
	 *
	 * taint('name1', 'name2', 'name3', ...)
	 * taint(array('name1', 'name2', 'name3'))
	 *
	 * @return $this
	 * @author Matthew Leverton
	 **/
	public function taint()
	{
		foreach (func_get_args() as $name)
		{
			if (is_array($name))
				call_user_func_array(array($this, 'taint'), $name);
			else
				$this->_tainted[$name] = true;
		}
		
		return $this;
	}
	
	/**
	 * Mark the passed variables as untainted. Must be called *before* the
	 * variable is set. Can be called in several ways:
	 *
	 * untaint('name1', 'name2', 'name3', ...)
	 * untaint(array('name1', 'name2', 'name3'))
	 *
	 * @return $this
	 * @author Matthew Leverton
	 **/
	public function untaint()
	{
		foreach (func_get_args() as $name)
		{
			if (is_array($name))
				call_user_func_array(array($this, 'untaint'), $name);
			else
				$this->_tainted[$name] = false;
		}
		
		return $this;
	}
	
	/**
	 * Overloads the default View::set(). Check to see if the variable is tainted.
	 * If so, it encodes the variable so that it is safe for display.
	 *
	 * @return $this
	 * @author Matthew Leverton
	 **/
	public function set($key, $val = null)
	{
		if (is_array($key))
		{
			foreach ($key as $k => $v)
				$this->set($k, $v);
		}
		else
		{
			if (array_key_exists($key, $this->_tainted) ? $this->_tainted[$key] : $this->_auto_taint)
			{
				try {
					$this->clean($val);
				}
				catch (Exception $e) {
					throw new Exception("Unable to clean '$key': " . $e->getMessage());
				}
			}
			
			parent::set($key, $val);
		}
		
		return $this;
	}
	
	/**
	 * Does the actual work of cleaning the variable. Objects and arrays
	 * are recursively iterated over. All string data types are encoded.
	 *
	 * @return void
	 * @author Matthew Leverton
	 **/
	private function clean(&$val)
	{
		if (is_array($val))
		{
			foreach ($val as &$v)
				$this->clean($v);
			unset($v);
		}
		else if (is_object($val))
		{
			$type = get_class($val);
			
			if ($type == 'stdClass')
			{
				$val = clone $val;
				foreach ($val as &$v)
					$this->clean($v);
				unset($v);
			}
			else if ($val instanceof TelePharm_Untainted)
			{
				# nothing to do
			}
			else if (($val instanceof TelePharm_Untaintable) && $val->isTainted())
			{
				$val = $val->untaint();
			}
			else
				throw new Exception("Cannot clean an unknown object of type $type. It should probably implement the Untainted or Untaintable interface.");
		}
		else if (is_string($val) && !is_numeric($val))
		{
			$html_val = htmlspecialchars($val);
			if ($html_val != $val) $val = new TelePharm_UntaintedScalar($html_val);
		}
	}
}
