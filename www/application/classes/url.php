<?php
class URL extends TelePharm_URL
{
	static public function www($url = '')
	{
		return self::concat(Kohana::$config->load('application')->urls['www'], $url);
	}

	static public function js_asset($module, $isLib = false)
	{
        $config = Kohana::$config->load('release.env');
        $builtVersion = "js-built/";

        if (strcmp($config, 'development') == 0){
            $builtVersion = "js/";
            if ($isLib){
                $builtVersion = $builtVersion . "lib/";
            }
        }
		return self::concat(Kohana::$config->load('application')->urls['assets'], "{$builtVersion}{$module}");
	}

    static public function css_asset($module)
    {
        $builtVersion = "css-built/";
        $build = Kohana::$config->load('release.build');
        return self::concat(Kohana::$config->load('application')->urls['assets'], "{$builtVersion}{$module}") . "?v=" . strtotime($build);
    }

	/**
	 * Return the url where the image was saved
	 *
	 * @param Model_Image   $image the image in the db
	 * @param string        $placeholder the default image to return if not found
	 * @param boolean       $original if you want the larger version
	 *
	 * @return string
	 */
	static public function uploaded_image(Model_Image $image = null,
		$placeholder = 'photo-technician.jpg',
		$original = false)
	{
		$placeHolder = url::asset('img/'.$placeholder);

		if ($image == null || !$image->loaded())
			return $placeHolder;

		return url::www('image/get/' . $image->id . '?original=' . ($original ? 'true' : 'false'));
	}

	static public function medispan_image($name)
	{
		return url::www('image/get_medispan/?name=' . $name);
	}
}

