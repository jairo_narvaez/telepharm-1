<?php

$rootDirectory = dirname(dirname(dirname(dirname(__FILE__))));

return array(
	'urls'                 => array(
		'www'              => 'http://0.0.0.0:8000/',
		'assets'           => 'http://0.0.0.0:8001/',
		'www-old'          => 'http://0.0.0.0:8002/',
		'www-old-patient'  => 'http://0.0.0.0:8003/',
	),
	'asset_path'           => $rootDirectory . '/static/',
	'email_transport'      => 'File',
	'password_cost_factor' => '11',
	'shared_cookie_domain' => 'localhost',
	'admin_email'          => 'trashman@digintent.com',
	'site_name'            => 'Telepharm',
	
	# this is always a local directory for temp images
	'tmp_images_folder' => $rootDirectory . '/private/images/tmp/',
	
	'use_images_from' => 'filesystem', # filesystem or s3
	'use_x_send_file' => true, # false for development, true for production

	's3_credentials' => [
		'access_key_id' => '',
		'secret_access_key' => ''
	],

	'images' => [
		'filesystem' => [
			'original'     => $rootDirectory . '/private/images/original/',
			'account'      => $rootDirectory . '/private/images/account/',
			'prescription' => $rootDirectory . '/private/images/prescription/',
			'medispan'     => $rootDirectory . '/private/images/medispan/',
			'signature'    => $rootDirectory . '/private/images/signature/',
		],
		's3' => [
			'original'     => 's3://telepharm-prescriptions/original/',
			'account'      => 's3://telepharm-prescriptions/account/',
			'prescription' => 's3://telepharm-prescriptions/prescription/',
			'medispan'     => 's3://telepharm-prescriptions/medispan/',
			'signature'    => 's3://telepharm-prescriptions/signature/',
		],
	],

	'default_labels' => [
		['label' => 'Medicaid Will Not Reimburse', 'color' => '#ddf7c8'],
		['label' => 'For External Use Only', 'color' => '#fecaca'],
		['label' => 'DO NOT CRUSH', 'color' => '#faf7a4'],
	],
	'serve_js_with_release_version' => true,
	'ajax_intervals' => [
		'get_queue' => 10000,
		'tablet_check' => 5000,
		'available_users' => 10000,
		'get_notifications' => 2000,
		'audio_notifications' => 2000,
		'update_prescription' => 5000,
		'update_history' => 3000,
		'prescription_comments' => 3000,
		'call_status_check' => 2000,
		'incoming_call_check' => 5000
	],
    'use_app_store_distributable'=>false
);
