<?php

return [
	'comparison' => 'Value must match',
	'unique-email' => 'Email is already on system',
	'unique-username' => 'Username is already on system',
];
