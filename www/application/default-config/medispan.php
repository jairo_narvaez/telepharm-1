<?php

return [
	'ruby_file_location' => '[path to file]', # location of the ruby file in the system that downloads medispan files
	'download_directory' => '[root path to download medispan files]' # place that the medispan downloads and archives 
];