<?php

return [
    'web_session' => [
        'crypto'=>[
            'key' => 'D1g1nt3nt-T3l3ph4rm-C00k13-S4lt',
            'seperator_character'=>'.'],
        'token' => [
            'regular' => ['strength' => 32, 'slide_minutes' => 30],
            'extended' => ['strength' => 64, 'slide_minutes' => 20160],
            'seperator_character' => '.'
        ],

        'validate_ip'=>true
    ],
    'forgot_password_session'=>[
        'crypto'=>[
            'key' => 'D1g1nt3nt-T3l3ph4rm-C00k13-S4lt',
            'seperator_character'=>'.'],
        'token' => [
            'regular' => ['strength' => 32, 'slide_minutes' => 1440],
            'seperator_character' => '.'
        ],

        'validate_ip'=>false
    ],
    'native_client_session'=>[
        'crypto'=>[
            'key' => 'D1g1nt3nt-T3l3ph4rm-C00k13-S4lt',
            'seperator_character'=>'_'],
        'token' => [
            'regular' => ['strength' => 32, 'slide_minutes' => 600],
            'seperator_character' => '.'
        ],
        'validate_ip'=>true
    ]
];
