<?php defined('SYSPATH') OR die('Kohana bootstrap needs to be included before tests run');

/**
 * Superclass to make tests against a db
 * 
 */
class DatabaseTestCase extends Kohana_Unittest_Database_TestCase
{
	protected $_database_connection = 'testing';

	public function setUp()
	{
		parent::setUp();
		Database::$default = 'testing';
	}

	/**
	 * read enter from keyboard (to stop the phpunit processing)
	 *
	 * @return void
	 * @author Marcelo Andrade
	 **/
	public function wait()
	{
	    $fp1=fopen("/dev/stdin", "r");
	    $input=fgets($fp1, 255);
	    fclose($fp1);
	}

	/**
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getDataSet()
    {
        throw new Exception("Please provide the implementation of getDataSet method");
    }

	public function getSetUpOperation()
	{
		$cascadeTruncates = false; // If you want cascading truncates, false otherwise. If unsure choose false.

		return new \PHPUnit_Extensions_Database_Operation_Composite(array(
			new TruncateOperation($cascadeTruncates),
			new InsertOperation()
		));
	}
}
