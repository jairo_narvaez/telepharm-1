<?php

// initialize kohana
require_once __DIR__.'/../../index.php';
ini_set('fakeupload.enabled', '1');

Kohana::$config->attach(new Config_File('config-test'));
DEFINE('FIXTURES_DIR', DOCROOT . 'application/tests/fixtures/');

// activate s3 wrapper if config is s3
$file_protocol = Kohana::$config->load('application.use_images_from');

if ( $file_protocol == 's3' )
{
	$s3_config = Kohana::$config->load('application.s3_credentials');

	if (!$s3_config)
		throw new Exception('s3_credentials are not defined');

	$wrapper = new aS3StreamWrapper();

	$wrapper->register([
		'protocol' => 's3', 
		'acl' => AmazonS3::ACL_PRIVATE, 
		'key' => $s3_config['access_key_id'],
		'secretKey' => $s3_config['secret_access_key'],
		'region' => AmazonS3::REGION_US_W1
		]);
}

// include core required files
require_once __DIR__.'/TruncateOperation.php';
require_once __DIR__.'/InsertOperation.php';
require_once __DIR__.'/DatabaseTestCase.php';
require_once __DIR__.'/simple_html_dom.php';
