#!/usr/bin/env php
<?php
/**
 * This script creates the phpunit.xml files replacing the domain
 * configured in config/application.php
 **/
$app_config = include '../config/application.php';
$domains = array();
foreach ($app_config['urls'] as $i => $url)
{
	$parsed = parse_url($url);
	$domains[$i] = $parsed['host'];
	if (isset($parsed['port']))
		$domains[$i] .= ':'.$parsed['port'];
}

$root = simplexml_load_file("phpunit.template.xml");

foreach($root->xpath('php/server[@name="HTTP_HOST"]') as $server_var)
{
	$server_var->attributes()->value = $domains['www'];
}

$root->asXML("phpunit.xml");

echo "PHPUnit xml file created correctly \n";
