<?php


class ControllerAPICRXTest extends DatabaseTestCase
{
	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	/**
	 * set up for all testing
	 */
	public function setUp()
	{
		parent::setUp();
		$this->session = Session::instance();
		$this->db = Database::instance();
		$this->db->query(Database::DELETE, 'TRUNCATE TABLE `auth_token`');
		$this->db->query(Database::UPDATE, 'UPDATE `store` SET `server_ip` = \'127.0.0.1\',
		   pms_query_class = \'Telepharm_PMS_CRX\'	WHERE id = 1');
	}

	public function testStartSession()
	{
		$req = Request::factory('api/crx/start_session');
		$req->post('local_ip', '127.0.0.1');
		$req->method('POST');
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertNotNull($result['session_id']);
		return $result['session_id'];
	}

	public function testEndSession()
	{
		$sessionId = $this->testStartSession();

		$req = Request::factory('api/crx/end_session');
		$req->post('session_id', $sessionId);
		$req->method('POST');
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertTrue($result['ended']);
	}

	public function testCreate()
	{
		$sessionId = $this->testStartSession();

		$req = Request::factory('api/crx/create');
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$data1 = @include $fixturesDir.'crx-1.php';
		$data1['session_id'] = $sessionId;
		$req->post($data1);
		$req->method('POST');
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertTrue($result['created']);
		$this->assertEquals(4, $result['prescription_id']);
	}

	
}
