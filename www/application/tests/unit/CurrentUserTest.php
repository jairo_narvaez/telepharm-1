<?php

class CurrentUserTest extends DatabaseTestCase
{
	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}
	/**
	 * Get perspectives of the user
	 * 
	 * @test
	 */
	public function testGetPerspectives()
	{
		$account = new Model_Account(1);
		$user = new CurrentUser($account);
		$this->assertEquals(['Site Administrator' => 'Site Administrator'], $user->refreshAvailablePerspectives());

		$account = new Model_Account(2);
		$user = new CurrentUser($account);
		$this->assertEquals(['Pharmacist' => 'Pharmacist'], $user->refreshAvailablePerspectives());

		$account = new Model_Account(3);
		$user = new CurrentUser($account);
		$this->assertEquals(['Technician' => 'Technician'], $user->refreshAvailablePerspectives());

		$account = new Model_Account(4);
		$user = new CurrentUser($account);
		$this->assertEquals(['Company Administrator' => 'Company Administrator'], $user->refreshAvailablePerspectives());
	}

	/**
	 * Get perspectives for # of stores
	 * 
	 * @test
	 */
	public function testGetPerspectivesDifferentStores()
	{
		$account = new Model_Account(2);
		$user = new CurrentUser($account);
		$this->assertEquals(['Pharmacist' => 'Pharmacist'], $user->refreshAvailablePerspectives());

		$this->assertEquals(1, count($user->stores));
	}
}
