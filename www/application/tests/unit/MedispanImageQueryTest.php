<?php

class MedispanImageQueryTest extends DatabaseTestCase
{
	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm medispan_udij medispan_udrug medispan_dmfg medispan_img > medispan_info.xml
	 */
	public function getDataSet()
	{
		return $this->createMySQLXMLDataSet(FIXTURES_DIR .
			'medispan_info.xml');
	}

	/**
	 * @dataProvider ndcAndImages
	 */
	public function testGetImageForNDC($ndc, $result)
	{
		$query = new Medispan_Query_Image();
		$images = $query->get($ndc);
		$this->assertEquals(count($result), count($images));
		foreach($images as $image)
		{
			$found = false;
			foreach($result as $res)
			{
				if(strpos($image, $res)>0)
					$found = true;
			}
			$this->assertTrue($found);
		}
	}

	/**
	 * List of test cases for retrieving images
	 * from medispan db
	 */
	public function ndcAndImages()
	{
		return [
			[
			'',
			[]
			],
			[
			'45802035803',
			['CP358030']
			],
			[
			'68180044701',
			['LPN04470']
			],

			];
	}
}
