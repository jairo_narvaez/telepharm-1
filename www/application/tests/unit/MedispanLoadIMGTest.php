<?php

class MedispanLoadIMGTest extends DatabaseTestCase
{
	protected $loader = null;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump --no-data -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		return $this->createMySQLXMLDataSet(FIXTURES_DIR .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$this->loader = Medispan_Load::factory('IMG', FIXTURES_DIR . 'IM2IMG');
		$db = Database::instance();
		$db->query(Database::DELETE, 'TRUNCATE TABLE `medispan_img`');
	}

	public function testGetFieldsDefinition()
	{
		$definition = $this->loader->getFieldsDefinition();

		$this->assertEquals(4, count($definition));
		$this->assertEquals('image_id', $definition[0]['name']);
		$this->assertEquals('transaction_code', $definition[1]['name']);
		$this->assertEquals('image_filename', $definition[2]['name']);
		$this->assertEquals('reserve', $definition[3]['name']);

		$this->assertEquals('number', $definition[0]['type']);
		$this->assertEquals('string', $definition[1]['type']);
		$this->assertEquals('string', $definition[2]['type']);
		$this->assertEquals('string', $definition[3]['type']);

		$this->assertEquals(10, $definition[0]['size']);
		$this->assertEquals(1, $definition[1]['size']);
		$this->assertEquals(20, $definition[2]['size']);
		$this->assertEquals(17, $definition[3]['size']);
	}

	/**
	 * @dataProvider linesProvider
	 */
	public function testProcesssALineOfTheFile($line, $result)
	{
		$processed = $this->loader->processLine($line);

		$this->assertEquals(count($result), count($processed));
		$this->assertEquals($result, $processed);
	}

	/**
	 * Generates lines for check the processing
	 * 
	 * @return Array
	 */
	public function linesProvider()
	{
		return [
			[
				'0000022314AH2266050                             ',
				[
					'image_id'   => '0000022314',
					'transaction_code' => 'A',
					'image_filename'   => 'H2266050',
					'reserve'          => '',
				],
				1
			],
		];
	}

	/**
	 * Test the method to actually load the 
	 * info to the db
	 *
	 * @dataProvider linesProvider
	 */
	public function testProcessRecord($line, $result, $records)
	{
		$data = $this->loader->processLine($line);
		$saved = $this->loader->processRecord($data);
		$this->assertTrue($saved);
		$this->assertEquals($records, Model::factory('MedispanIMG')->count_all());
	}

	/**
	 * Test the load method
	 */
	public function testLoad()
	{
		list($processed, $saved) = $this->loader->load();
		$this->assertEquals(3, $processed);
		$this->assertEquals(3, $saved);
	}
}
