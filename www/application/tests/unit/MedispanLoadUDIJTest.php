<?php

class MedispanLoadUDIJTest extends DatabaseTestCase
{
	protected $loader = null;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump --no-data -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		return $this->createMySQLXMLDataSet(FIXTURES_DIR .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$this->loader = Medispan_Load::factory('UDIJ', FIXTURES_DIR . 'IM2UDIJ');
		$db = Database::instance();
		$db->query(Database::DELETE, 'TRUNCATE TABLE `medispan_udij`');
	}

	public function testGetFieldsDefinition()
	{
		$definition = $this->loader->getFieldsDefinition();

		$this->assertEquals(6, count($definition));
		$this->assertEquals('unique_drug_id', $definition[0]['name']);
		$this->assertEquals('start_date', $definition[1]['name']);
		$this->assertEquals('transaction_code', $definition[2]['name']);
		$this->assertEquals('stop_date', $definition[3]['name']);
		$this->assertEquals('image_id', $definition[4]['name']);
		$this->assertEquals('reserve', $definition[5]['name']);

		$this->assertEquals('number', $definition[0]['type']);
		$this->assertEquals('date', $definition[1]['type']);
		$this->assertEquals('string', $definition[2]['type']);
		$this->assertEquals('date', $definition[3]['type']);
		$this->assertEquals('number', $definition[4]['type']);
		$this->assertEquals('string', $definition[5]['type']);

		$this->assertEquals(10, $definition[0]['size']);
		$this->assertEquals(8, $definition[1]['size']);
		$this->assertEquals(1, $definition[2]['size']);
		$this->assertEquals(8, $definition[3]['size']);
		$this->assertEquals(10, $definition[4]['size']);
		$this->assertEquals(35, $definition[5]['size']);
	}

	/**
	 * @dataProvider linesProvider
	 */
	public function testProcesssALineOfTheFile($line, $result)
	{
		$processed = $this->loader->processLine($line);

		$this->assertEquals(count($result), count($processed));
		$this->assertEquals($result, $processed);
	}

	/**
	 * Generates lines for check the processing
	 * 
	 * @return Array
	 */
	public function linesProvider()
	{
		return [
			[
				'000000655220121201A000000000000019713                                   ',
				[
					'unique_drug_id'   => '0000006552',
					'start_date' => '2012-12-01',
					'transaction_code' => 'A',
					'stop_date'   => null,
					'image_id' => '000019713',
					'reserve'          => '',
				],
				1
			],
			[
				'000000655520000216C201211300000013922                                   ',
				[
					'unique_drug_id'   => '0000006555',
					'start_date' => '2000-02-16',
					'transaction_code' => 'C',
					'stop_date'   => '2012-11-30',
					'image_id' => '0000013922',
					'reserve'          => '',
				],
				1
			],
			[
				'000000872920000216D000000000000001406                                   ',
				[
					'unique_drug_id'   => '0000008729',
					'start_date' => '2000-02-16',
					'transaction_code' => 'D',
					'stop_date'   => null,
					'image_id' => '0000001406',
					'reserve'          => '',
				],
				0
			],
		];
	}

	/**
	 * Test the method to actually load the 
	 * info to the db
	 *
	 * @dataProvider linesProvider
	 */
	public function testProcessRecord($line, $result, $records)
	{
		$data = $this->loader->processLine($line);
		$saved = $this->loader->processRecord($data);
		$this->assertTrue($saved);
		$this->assertEquals($records, Model::factory('MedispanUDIJ')->count_all());
	}

	/**
	 * Test the load method
	 */
	public function testLoad()
	{
		list($processed, $saved) = $this->loader->load();
		$this->assertEquals(3, $processed);
		$this->assertEquals(3, $saved);
	}
}
