<?php

class MedispanLoadUDRUGTest extends DatabaseTestCase
{
	protected $loader = null;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump --no-data -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		return $this->createMySQLXMLDataSet(FIXTURES_DIR .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$this->loader = Medispan_Load::factory('UDRUG', FIXTURES_DIR . 'IM2UDRUG');
		$db = Database::instance();
		$db->query(Database::DELETE, 'TRUNCATE TABLE `medispan_udrug`');
	}

	public function testGetFieldsDefinition()
	{
		$definition = $this->loader->getFieldsDefinition();

		$this->assertEquals(6, count($definition));
		$this->assertEquals('unique_drug_id', $definition[0]['name']);
		$this->assertEquals('transaction_code', $definition[1]['name']);
		$this->assertEquals('dosage_form_id', $definition[2]['name']);
		$this->assertEquals('external_drug_id', $definition[3]['name']);
		$this->assertEquals('manufacturer_id', $definition[4]['name']);
		$this->assertEquals('reserve', $definition[5]['name']);

		$this->assertEquals('number', $definition[0]['type']);
		$this->assertEquals('string', $definition[1]['type']);
		$this->assertEquals('number', $definition[2]['type']);
		$this->assertEquals('string', $definition[3]['type']);
		$this->assertEquals('number', $definition[4]['type']);
		$this->assertEquals('string', $definition[5]['type']);

		$this->assertEquals(10, $definition[0]['size']);
		$this->assertEquals(1, $definition[1]['size']);
		$this->assertEquals(5, $definition[2]['size']);
		$this->assertEquals(20, $definition[3]['size']);
		$this->assertEquals(10, $definition[4]['size']);
		$this->assertEquals(26, $definition[5]['size']);
	}

	/**
	 * @dataProvider linesProvider
	 */
	public function testProcesssALineOfTheFile($line, $result)
	{
		$processed = $this->loader->processLine($line);

		$this->assertEquals(count($result), count($processed));
		$this->assertEquals($result, $processed);
	}

	/**
	 * Generates lines for check the processing
	 * 
	 * @return Array
	 */
	public function linesProvider()
	{
		return [
			[
				'0000003280D0000200182050710         0000000000                          ',
				[
					'unique_drug_id'   => '0000003280',
					'transaction_code' => 'D',
					'dosage_form_id'   => '00002',
					'external_drug_id' => '00182050710',
					'manufacturer_id'    => '0000000000',
					'reserve'          => '',
				],
				0
			],
			[
				'0000061586A0000449035010526         0000000000                          ',
				[
					'unique_drug_id'   => '0000061586',
					'transaction_code' => 'A',
					'dosage_form_id'   => '00004',
					'external_drug_id' => '49035010526',
					'manufacturer_id'    => '0000000000',
					'reserve'          => '',
				],
				1
			],
			[
				'0000061586C0000449035010527         0000000000                          ',
				[
					'unique_drug_id'   => '0000061586',
					'transaction_code' => 'C',
					'dosage_form_id'   => '00004',
					'external_drug_id' => '49035010527',
					'manufacturer_id'    => '0000000000',
					'reserve'          => '',
				],
				1
			],
		];
	}

	/**
	 * Test the method to actually load the 
	 * info to the db
	 *
	 * @dataProvider linesProvider
	 */
	public function testProcessRecord($line, $result, $records)
	{
		$data = $this->loader->processLine($line);
		$saved = $this->loader->processRecord($data);
		$this->assertTrue($saved);
		$this->assertEquals($records, Model::factory('MedispanUDRUG')->count_all());
	}

	/**
	 * Test the load method
	 */
	public function testLoad()
	{
		list($processed, $saved) = $this->loader->load();
		$this->assertEquals(3, $processed);
		$this->assertEquals(3, $saved);
	}
}
