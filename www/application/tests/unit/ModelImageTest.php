<?php

/**
 * Test the class Model_Image and Image: the different ways to
 * get an image (filesystem or s3)
 */
class ModelImageTest extends DatabaseTestCase
{
	protected $db;
	protected $fixturesDir;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$this->fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($this->fixturesDir .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$this->db = Database::instance();
		$this->db->query(Database::UPDATE, 'set foreign_key_checks=0');
		$this->db->query(Database::DELETE, 'TRUNCATE TABLE `image`');
		$this->db->query(Database::UPDATE, 'set foreign_key_checks=1');
	}

	public function testCreateImage()
	{
		$filePath = $this->fixturesDir . 'images/avatar.png';
		$img = new Image($filePath);
		$image = Model_Image::createInstance($img, 'account', 
			'avatar.png', filesize($filePath)
		);
		$this->assertNotNull($image);
		$this->assertTrue($image->loaded());
		$this->assertEquals(1, Model::factory('Image')->count_all());
		$this->image = $image;
	}

	public function testCreateThumbnail()
	{
		$resultsDirectory = $this->fixturesDir . 'images-results/';
		exec("rm -fr $resultsDirectory");
		$filePath = $this->fixturesDir . 'images/avatar.png';

		$img = new Image($filePath);
		$thumbnailPath = $img->resizeAndCrop([
		   'height'    => 80,
		   'width'     => 80,
		   'file_path' => $resultsDirectory,
		   ]);
		$thumbnail = new Image($thumbnailPath);
		$this->assertEquals(80, $thumbnail->height);
		$this->assertEquals(80, $thumbnail->width);
		$this->assertEquals('jpeg', $thumbnail->format);

		$thumbnailPath = $img->resizeAndCrop([
		   'height'    => 180,
		   'width'     => 180,
		   'file_path' => $resultsDirectory,
		   ]);
		$thumbnail = new Image($thumbnailPath);
		$this->assertEquals(180, $thumbnail->height);
		$this->assertEquals(180, $thumbnail->width);
		$this->assertEquals('jpeg', $thumbnail->format);
	}

	public function testGetOriginalPath()
	{
		$storage = Kohana::$config->load('application.use_images_from');
		$this->assertEquals(Kohana::$config->load("application.images.$storage.original"),
			Image::getOriginalImagesFolderPath());
	}


	public function testProcessUpload()
	{
		if (!function_exists('fakeupload_file'))
		{
			$this->markTestSkipped('You need to install fakeupload extension ' .
				'from http://valokuva.org/?p=132');
		}

		$uploaded = fakeupload_file('file', 'avatar.png',
			'/tmp/upload.png', 'image/png', file_get_contents($this->fixturesDir . 'images/avatar.png'));

		$resultsDirectory = $this->fixturesDir . 'images-results/';
		exec("rm -fr $resultsDirectory");

		$image = Model_Image::processUpload('file',
			'account',
			[
			'resizedCrop' => [[
				'height' => 80,
				'width'     => 80,
				'file_path' => $resultsDirectory,
			]]
		]);
		$this->assertNotNull($image);
		$this->assertTrue($image->loaded());
		$this->assertEquals('account', $image->kind);
		$this->assertEquals(1, Model::factory('Image')->count_all());
		$thumbnail = new Image($image->getThumbnailImagePath());
		$this->assertEquals(80, $thumbnail->height);
		$this->assertEquals(80, $thumbnail->width);
		$this->assertEquals('jpeg', $thumbnail->format);
	}

	public function testProcessUploadBase64()
	{
		$resultsDirectory = $this->fixturesDir . 'images-results/';
		exec("rm -fr $resultsDirectory");

		$picture = 'base64,' . base64_encode(file_get_contents($this->fixturesDir . 'images/avatar.png'));

		$image = Model_Image::processBase64Upload($picture,
			'avatar.png',
			'account',
			[
			'resizedCrop' => [[
				'height' => 80,
				'width'     => 80,
				'file_path' => $resultsDirectory,
			]]
		]);
		$this->assertNotNull($image);
		$this->assertTrue($image->loaded());
		$this->assertEquals('account', $image->kind);
		$this->assertEquals(1, Model::factory('Image')->count_all());

		$thumbnail = new Image($image->getThumbnailImagePath());
		$this->assertEquals(80, $thumbnail->height);
		$this->assertEquals(80, $thumbnail->width);
		$this->assertEquals('jpeg', $thumbnail->format);
	}
	

	public function testUploadSignature()
	{
		$p = new Model_Prescription(1);
		$resultsDirectory = $this->fixturesDir . 'images-results/';
		exec("rm -fr $resultsDirectory");

		$picture = base64_encode(file_get_contents($this->fixturesDir . 'images/avatar.png'));

		$p->saveSignature($picture);
		$image = Model::factory('Image')->find();
		$this->assertNotNull($image);
		$this->assertTrue($image->loaded());
		$this->assertEquals('signature', $image->kind);
		$this->assertEquals(1, Model::factory('Image')->count_all());
	}

	public function testGetFileToXSendFile()
	{
		$p = new Model_Prescription(1);
		$resultsDirectory = $this->fixturesDir . 'images-results/';
		exec("rm -fr $resultsDirectory");

		$picture = base64_encode(file_get_contents($this->fixturesDir . 'images/avatar.png'));

		$p->saveSignature($picture);
		$image = Model::factory('Image')->find();
		
		$original = $image->getOriginalImagePath();
		$this->assertEquals(Kohana::$config->load('application.images.filesystem.original') . '9/e/9e604f9287011d076038bd27eb272e6a9ae70e66.png',
			$original
			);

		$thumbnail = $image->getThumbnailImagePath();
		$this->assertEquals(Kohana::$config->load('application.images.filesystem.signature') . '9/e/9e604f9287011d076038bd27eb272e6a9ae70e66.jpeg',
			$thumbnail);

		$this->assertTrue(file_exists($original));
		$this->assertTrue(file_exists($thumbnail));
	}

}
