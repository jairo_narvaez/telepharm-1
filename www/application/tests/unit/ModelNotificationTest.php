<?php

/**
 * Test the class Model_Notification and the possible
 * cases for tie notifications to store and accounts
 */
class ModelNotificationTest extends DatabaseTestCase
{
	protected $db;
	protected $currentUser;
	protected $acl;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$this->db = Database::instance();
		$this->db->query(Database::DELETE, 'TRUNCATE TABLE `notification`');
		$this->loadACL();
		$account = new Model_Account(3);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$this->currentUser = $user;
		$permission = Helper_Permission::instance($this);
	}

	protected function loadACL()
	{
		# Add new acl permissions
		$this->acl = new mef\ACL\Manager();

		# Super admin doesn't require to give detail
		# of what is grant, b/c it's granted all
		$this->acl->grant ('Site Administrator');
		$this->acl->revoke('Site Administrator' , 'prescription' , 'create');
		$this->acl->revoke('Site Administrator' , 'pharmacist'   , 'dashboard');
		$this->acl->revoke('Site Administrator' , 'technician'   , 'dashboard');
		$this->acl->revoke('Site Administrator' , 'prescription'  , 'edit');
		$this->acl->revoke('Site Administrator' , 'prescription'  , 'delete');

		# For this other roles we need to specify
		# to what they have access
		$this->acl->grant('Company Administrator' , 'admin'        , 'view');
		$this->acl->grant('Company Administrator' , 'account'      , 'admin');
		$this->acl->grant('Company Administrator' , 'account'      , 'create');
		$this->acl->grant('Company Administrator' , 'account'      , 'edit');
		$this->acl->grant('Company Administrator' , 'company'      , 'admin');
		$this->acl->grant('Company Administrator' , 'company'      , 'view'     , new Telepharm_ACL_CompanyOwnerAssertion('id') );
		$this->acl->grant('Company Administrator' , 'company'      , 'edit');
		$this->acl->grant('Company Administrator' , 'companyroles' , 'assign');
		$this->acl->grant('Company Administrator' , 'store'        , 'view'     , new Telepharm_ACL_CompanyOwnerAssertion('company_id') );
		$this->acl->grant('Company Administrator' , 'store'        , 'admin');
		$this->acl->grant('Company Administrator' , 'store'        , 'create');
		$this->acl->grant('Company Administrator' , 'store'        , 'edit');
		$this->acl->grant('Company Administrator' , 'store'        , 'delete');
		$this->acl->grant('Company Administrator' , 'storeroles'   , 'assign');
		$this->acl->revoke('Company Administrator' , 'pharmacist'   , 'dashboard');
		$this->acl->revoke('Company Administrator' , 'technician'   , 'dashboard');
		$this->acl->revoke('Company Administrator' , 'prescription'  , 'edit');
		$this->acl->revoke('Company Administrator' , 'prescription'  , 'delete');

		$this->acl->grant('Pharmacist' , 'pharmacist'   , 'dashboard');
		$this->acl->grant('Pharmacist' , 'prescription' , 'read-comments');
		$this->acl->grant('Pharmacist' , 'prescription' , 'view');
		$this->acl->grant('Pharmacist' , 'prescription' , 'search');

		$this->acl->grant('Technician' , 'technician'   , 'dashboard');
		$this->acl->grant('Technician' , 'prescription' , 'create');
		$this->acl->grant('Technician' , 'prescription' , 'read-comments');
		$this->acl->grant('Technician' , 'prescription' , 'view');
		$this->acl->grant('Technician' , 'prescription' , 'search');
	}

	public function setRole($roleName)
	{
		$this->acl->addRole($this->currentUser->getRoleId(), [$roleName]);
	}

	/*
	 * mock function in application controller
	 */
	public function requestAccess($resource, $privilege)
	{
		if ($this->currentUser == null)
		{
			throw new HTTP_Exception_403();
		}
		if (!$this->acl->isAllowed($this->currentUser, $resource, $privilege))
		{
			throw new HTTP_Exception_403();
		}
	}

	public function testPublishToStoreForTechnicians()
	{
		$published = Model_Notification::publish(
			$this->currentUser,
			'comment', 
			'New comment for prescription', 
			1, 
			['prescription_id' => 1],
			'Technician'
		);
		$this->assertTrue($published);
		$this->assertEquals(1, Model::factory('Notification')->
			count_all());
	}

	public function testPublishToStoreForPharmacists()
	{
		$published = Model_Notification::publish(
			$this->currentUser,
			'comment', 
			'New comment for prescription', 
			1, 
			['prescription_id' => 1],
			'Pharmacist'
		);
		$this->assertTrue($published);
		$this->assertEquals(1, Model::factory('Notification')->
			count_all());
	}


	public function testPublishToAccount()
	{
		$published = Model_Notification::publishToAccount(
			$this->currentUser,
			'incoming-call', 
			'Patient a is calling', 
			2, 
			['prescription_id' => 1]);
		$this->assertTrue($published);
		$this->assertEquals(1, Model::factory('Notification')->
			count_all());
	}

	public function testGetNotProcessedByStoreForTechnicians()
	{
		$this->testPublishToStoreForTechnicians();
		$notification = Model::factory('Notification')->find();

		$account = new Model_Account(3);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$this->currentUser = $user;
		$this->setRole('Technician');
		$notifications = Model_Notification::getNotProcessed($user, false);

		$this->assertEquals(1, count($notifications));
	}

	public function testGetNotProcessedByStoreForPharmacists()
	{
		$this->testPublishToStoreForPharmacists();
		$notification = Model::factory('Notification')->find();

		$account = new Model_Account(2);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$this->currentUser = $user;
		$this->setRole('Pharmacist');
		$notifications = Model_Notification::getNotProcessed($user, false);

		$this->assertEquals(1, count($notifications));
	}


	public function testGetAll()
	{
		$this->testPublishToStoreForTechnicians();
		$notification = Model::factory('Notification')->find();

		$account = new Model_Account(3);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$notifications = Model_Notification::getAll($user);

		$this->assertEquals(1, count($notifications));
	}

	public function testGetNotProcessedByAccount()
	{
		$this->testPublishToAccount();
		$notification = Model::factory('Notification')->find();

		$account = new Model_Account(3);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$notifications = Model_Notification::getNotProcessed($user, false);

		$this->assertEquals(0, count($notifications));

		$account = new Model_Account(2);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$notifications = Model_Notification::getNotProcessed($user, false);

		$this->assertEquals(1, count($notifications));
	}

	public function testSetProcessedBy()
	{
		$this->testPublishToStoreForTechnicians();
		$notification = Model::factory('Notification')->find();

		$account = new Model_Account(3);
		$user = new CurrentUser($account);
		Model_Notification::setProcessed($user, $notification->id);

		$notification->reload();

		$this->assertEquals(3, $notification->processed_by);
		$this->assertEquals(1, $notification->processed);
	}
}

