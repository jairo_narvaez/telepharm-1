<?php

class ModelPrescriptionLogTest extends DatabaseTestCase
{
	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	public function testGetLogForPrescription()
	{
		$me = new CurrentUser(new Model_Account(3));
		$me->addStore(1);
		$logs = Model_PrescriptionLog::getLogForPrescription($me, 1);
		$this->assertEquals(59, count($logs));
		$oneLog = $logs[0];
		$this->assertEquals('approve', $oneLog['action']);
		$this->assertEquals('Pharmacist Name', $oneLog['created_by']);
	}
}
