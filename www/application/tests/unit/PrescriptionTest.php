<?php

/**
* Test the class Model_Prescription and the different status
* in the workflow
*/
class PrescriptionTest extends DatabaseTestCase
{
	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	public function testCreation()
	{
		$p = Model::factory('Prescription');
		$this->assertEquals(1, $p->is_draft);
	}

	public function testLoadFromDb()
	{
		$p = Model::factory('Prescription')->
			where('id', '=', 1)->find();
		$this->assertEquals(0, $p->is_draft);
	}

	public function testCheckPharmacistCanSeeADraftPrescription()
	{
		$db = Database::instance();
		// set up the test
		$db->query(Database::UPDATE, 'UPDATE `prescription` SET is_draft = 1 WHERE id = 1');

		$session = Session::instance();
		$session['account_id'] = 2; # Pharmacist
		$request = Request::factory('pharmacist');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());

		$prescriptions_data = $body->find('#prescriptions-data',0);
		$prescriptions = json_decode($prescriptions_data->plaintext, TRUE);

		foreach ($prescriptions as $p) {
			if ($p['is_draft'] == true)
			{
				$this->assertTrue(false, 'Prescription in draft status appearing on pharmacist');
			}
		}

		# Check that only the technician can see it
		$session = Session::instance();
		$session['account_id'] = 3; # Techincian
		$request = Request::factory('technician');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());

		$prescriptions_data = $body->find('#prescriptions-data',0);
		$prescriptions = json_decode($prescriptions_data->plaintext, TRUE);

		foreach ($prescriptions as $p) {
			if ($p['is_draft'] == true)
			{
				$this->assertEquals(1, $p['id'], 'Prescription in draft status appearing on technician');
			}
		}
	}

	public function testCancelPrescriptionRightAfterCreation()
	{
		# Create a prescription
		$session = Session::instance();
		$session['account_id'] = 3; # Techincian
		$request = Request::factory('ajax/prescription/create');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_number', '102033');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());
		$result = json_decode($body->innertext, true);
		$prescription_id = $result['result']['id'];

		$this->assertEquals(4, $prescription_id);

		$request = Request::factory('ajax/prescription/cancel');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());
		$statuses = json_decode($body->innertext, true);

		$this->assertEquals(1, $statuses['result']['canceled']);

		# Check that this new prescription shouldn't appear on queue
		$session = Session::instance();
		$session['account_id'] = 3; # Techincian
		$request = Request::factory('technician');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());

		$prescriptions_data = $body->find('#prescriptions-data',0);
		$prescriptions = json_decode($prescriptions_data->plaintext, TRUE);

		foreach ($prescriptions as $p) {
			if ($p['id'] == $prescription_id)
			{
				$this->assertTrue(false, 'Prescription in canceled status should not appear on technician');
			}
		}

		# Check that this new prescription shouldn't appear on pharmacist queue
		$session = Session::instance();
		$session['account_id'] = 2; # Pharmacist
		$request = Request::factory('pharmacist');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());

		$prescriptions_data = $body->find('#prescriptions-data',0);
		$prescriptions = json_decode($prescriptions_data->plaintext, TRUE);

		foreach ($prescriptions as $p) {
			if ($p['id'] == $prescription_id)
			{
				$this->assertTrue(false, 'Prescription in canceled status should not appear on technician');
			}
		}
	}

	public function testTrySubmitACanceledPrescription()
	{
		$db = Database::instance();
		$session = Session::instance();
		$session['account_id'] = 3; # Techincian

		# Create a prescription
		$request = Request::factory('ajax/prescription/create');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_number', '102033');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		# Cancele the prescription
		$request = Request::factory('ajax/prescription/cancel');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', 4);
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$request = Request::factory('ajax/prescription/submit');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', 4);

		$db->query(Database::INSERT, "INSERT INTO prescription_image (prescription_id, image_id, type)
			VALUES (4,28,'prescription'), (4,28,'drug_label'), (4,28,'drug_dispensed');");
		try
		{
			$response = $request->execute();
		}
		catch (Exception $e)
		{
		 	$this->assertEquals('Prescription can not be submitted', $e->getMessage());
		}
	}

	public function testSubmitPrescriptionRightAfterCreation()
	{
		# Create a prescription
		$session = Session::instance();
		$session['account_id'] = 3; # Techincian
		$request = Request::factory('ajax/prescription/create');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_number', '102033');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());
		$result = json_decode($body->innertext, true);
		$prescription_id = $result['result'];

		$this->assertEquals(4, $prescription_id['id']);

		$request = Request::factory('ajax/prescription/submit');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);
		try
		{
			$response = $request->execute();
		}
		catch (Exception $e)
		{
			//$this->assertEquals('To submit we need all images uploaded', $e->getMessage());
			$this->assertEquals('', $e->getMessage());
		}
	}

	public function testOnHoldWorkflowViaAjaxRequests()
	{
		$db = Database::instance();

		# Testing the following workflow:
		# Create
		# Submit On Hold (on_hold = 1)
		# Approve On Hold (approved_on_hold (1st approve))
		# Remove from queues (show = 0)
		# Search by rx number
		# Re-submit (make show = 1)
		# Real Approve from Pharmacist (approved_for_counsel (2nd approve))
		# Continue normal process

		$session = Session::instance();
		$session['account_id'] = 3; # Techincian
		$request = Request::factory('ajax/prescription/create');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_number', '102033');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());
		$result = json_decode($body->innertext, true);
		$prescription_id = $result['result']['id'];

		$this->assertEquals(4, $prescription_id);

		$request = Request::factory('ajax/prescription/update_option');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);
		$request->post('option_name', 'on_hold');
		$request->post('value', '1');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$request = Request::factory('ajax/prescription/submit');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);

		# On hold prescription doesn't require images
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$request = Request::factory('ajax/prescription/approve_on_hold');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		# Check that this new prescription shouldn't appear on pharmacist/technician queue
		$session = Session::instance();
		$session['account_id'] = 2; # Pharmacist
		$request = Request::factory('pharmacist');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());

		$prescriptions_data = $body->find('#prescriptions-data',0);
		$prescriptions = json_decode($prescriptions_data->plaintext, TRUE);

		foreach ($prescriptions as $p) {
			if ($p['id'] == $prescription_id)
			{
				$this->assertTrue(false, 'Prescription in not show status should not appear on pharmacist queue');
			}
		}

		# Check that this new prescription shouldn't appear on pharmacist/technician queue
		$session = Session::instance();
		$session['account_id'] = 3; # Technician
		$request = Request::factory('technician');
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$body = str_get_html($response->body());

		$prescriptions_data = $body->find('#prescriptions-data',0);
		$prescriptions = json_decode($prescriptions_data->plaintext, TRUE);

		foreach ($prescriptions as $p) {
			if ($p['id'] == $prescription_id)
			{
				$this->assertTrue(false, 'Prescription in not show status should not appear on pharmacist queue');
			}
		}

		$db->query(Database::INSERT, "INSERT INTO prescription_image (prescription_id, image_id, type)
			VALUES (4,28,'prescription'), (4,28,'drug_label'), (4,28,'drug_dispensed');");

		$request = Request::factory('ajax/prescription/re_submit');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

		$request = Request::factory('ajax/prescription/approve');
		$request->requested_with('xmlhttprequest');
		$request->method('POST');
		$request->post('prescription_id', $prescription_id);
		$response = $request->execute();

		$this->assertEquals(200, $response->status());

	}

	public function normalSubmit()
	{
		$db = Database::instance();
		$db->query(Database::UPDATE, "UPDATE `prescription` SET is_refill = 0,
			on_hold = 0, approved_on_hold = 0, is_draft = 1, approved_for_counsel = 0,
			required_counsel = 0, refused_counsel = 0, rejected = 0, completed = 0,
			canceled = 0, archived = 0, `show` = 1 WHERE `id` = 1");

		$rx = Model::factory('prescription')->where('id','=',1)->find();

		$this->assertEquals(1, $rx->is_draft);
		$this->assertEquals(1, $rx->show);

		$this->assertEquals(0, $rx->on_hold);
		$this->assertEquals(0, $rx->approved_on_hold);
		$this->assertEquals(0, $rx->approved_for_counsel);
		$this->assertEquals(0, $rx->required_counsel);
		$this->assertEquals(0, $rx->refused_counsel);
		$this->assertEquals(0, $rx->rejected);
		$this->assertEquals(0, $rx->completed);
		$this->assertEquals(0, $rx->canceled);
		$this->assertEquals(0, $rx->archived);

		// A new prescription in draft status can
		$this->assertEquals(1, $rx->canSubmit());
		$this->assertEquals(1, $rx->canCancel());
		$this->assertEquals(1, $rx->canUpdateOption());

		$this->assertEquals(0, $rx->canApprove());
		$this->assertEquals(0, $rx->canRequestCounsel());
		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

		// After a normal submit
		$rx->submitInternal();

		$this->assertEquals(1, $rx->canApprove());
		$this->assertEquals(1, $rx->canRequestCounsel());
		$this->assertEquals(1, $rx->canReject());

		$this->assertEquals(0, $rx->canRefuseCounsel());

		$this->assertEquals(1, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

		return $rx;
	}

	public function onHoldSubmit()
	{
		$db = Database::instance();
		$db->query(Database::UPDATE, "UPDATE `prescription` SET is_refill = 0,
			on_hold = 0, approved_on_hold = 0, is_draft = 1, approved_for_counsel = 0,
			required_counsel = 0, refused_counsel = 0, rejected = 0, completed = 0,
			canceled = 0, archived = 0, `show` = 1 WHERE `id` = 1");

		$rx = Model::factory('prescription')->where('id','=',1)->find();

		$this->assertEquals(1, $rx->is_draft);
		$this->assertEquals(1, $rx->show);

		$this->assertEquals(0, $rx->on_hold);
		$this->assertEquals(0, $rx->approved_on_hold);
		$this->assertEquals(0, $rx->approved_for_counsel);
		$this->assertEquals(0, $rx->required_counsel);
		$this->assertEquals(0, $rx->refused_counsel);
		$this->assertEquals(0, $rx->rejected);
		$this->assertEquals(0, $rx->completed);
		$this->assertEquals(0, $rx->canceled);
		$this->assertEquals(0, $rx->archived);

		// A new prescription in draft status can
		$this->assertEquals(1, $rx->canSubmit());
		$this->assertEquals(1, $rx->canCancel());
		$this->assertEquals(1, $rx->canUpdateOption());

		$this->assertEquals(0, $rx->canApprove());
		$this->assertEquals(0, $rx->canRequestCounsel());
		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

		// After a submit on hold
		$rx->updateOptionInternal('on_hold', 1);
		$rx->submitInternal();

		$this->assertEquals(1, $rx->canApprove());
		$this->assertEquals(1, $rx->canReject());

		$this->assertEquals(0, $rx->canSubmit());
		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canCancel());

		$this->assertEquals(1, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(1, $rx->isOnHold());

		return $rx;
	}

	/**
	 * This is a copy of the tests from the static/js/spec/PrescriptionSpec.js
	 */
	public function testFullNormalWorkflow()
	{
		$rx = $this->normalSubmit();

		// After approve the prescription
		$rx->approveInternal();

		$this->assertEquals(1, $rx->canCompleteCounsel());
		$this->assertEquals(1, $rx->canCancel());

		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(1, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

		// pharmacist set as complete
		$rx->completeCounselInternal(false);

		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(1, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

	}

	/**
	 * This is a copy of the tests from the static/js/spec/PrescriptionSpec.js
	 */
	public function testCancelWorkflow()
	{
		$rx = $this->normalSubmit();

		// After approve the prescription
		$rx->approveInternal();

		// After approve the prescription
		$rx->cancelInternal();

		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(1, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

	}

	/**
	 * This is a copy of the tests from the static/js/spec/PrescriptionSpec.js
	 */
	public function testRejectWorkflow()
	{
		$rx = $this->normalSubmit();

		// After approve the prescription
		$rx->rejectInternal();

		$this->assertEquals(1, $rx->canSubmit());
		$this->assertEquals(1, $rx->canCancel());

		$this->assertEquals(0, $rx->canApprove());
		$this->assertEquals(0, $rx->canRequestCounsel());
		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(1, $rx->rejected);
		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(0, $rx->isOnHold());

	}

	public function testOnHoldWorkflow()
	{
		$rx = $this->onHoldSubmit();

		// After approve the prescription
		$rx->approveOnHoldInternal();

		$this->assertEquals(1, $rx->canSubmit());
		$this->assertEquals(1, $rx->canCancel());

		$this->assertEquals(0, $rx->canApprove());
		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(1, $rx->isOnHold());

		// technician can resubmit
		$rx->reSubmitInternal();

		$this->assertEquals(1, $rx->canApprove());
		$this->assertEquals(1, $rx->canRequestCounsel());
		$this->assertEquals(1, $rx->canReject());

		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canCompleteCounsel());

		$this->assertEquals(1, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(1, $rx->isOnHold());
	}

	/**
	 * This is a copy of the tests from the static/js/spec/PrescriptionSpec.js
	 */
	public function testCancelOnHoldWorkflow()
	{
		$rx = $this->onHoldSubmit();

		// After approve the prescription
		$rx->approveOnHoldInternal();

		// After approve the prescription
		$rx->cancelInternal();

		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());
		$this->assertEquals(0, $rx->canReject());

		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(1, $rx->inFinalStatus());
		$this->assertEquals(1, $rx->isOnHold());

	}

	/**
	 * This is a copy of the tests from the static/js/spec/PrescriptionSpec.js
	 */
	public function testRejectOnHoldWorkflow()
	{
		$rx = $this->onHoldSubmit();

		// reject by pharmacist
		$rx->rejectInternal();

		$this->assertEquals(1, $rx->canSubmit());
		$this->assertEquals(1, $rx->canCancel());

		$this->assertEquals(0, $rx->canApprove());
		$this->assertEquals(0, $rx->canCompleteCounsel());
		$this->assertEquals(0, $rx->canRefuseCounsel());

		$this->assertEquals(1, $rx->rejected);
		$this->assertEquals(0, $rx->isAwaitingApproval());
		$this->assertEquals(0, $rx->isAwaitingCounsel());
		$this->assertEquals(0, $rx->inFinalStatus());
		$this->assertEquals(1, $rx->isOnHold());

	}

	public function testApprovedByPharmacist()
	{
		$rx = Model::factory('Prescription')->
			where('id', '=', '1')->
			find();

		$this->assertEquals('Pharmacist Name', $rx->approved_by_pharmacist);
	}

	public function testReportError()
	{
		$rx = Model::factory('Prescription')->
			where('id' ,'=', '1')->
			find();
		$rx->completed = true;
		$rx->save();

		$account = new Model_Account(2);
		$user = new CurrentUser($account);
		$user->addStore(1);
		$statuses = Model_Prescription::reportError($user, 1, 'not authorized');

		$rx->reload();

		$this->assertEquals('1', $statuses['reported_error']);
		$this->assertEquals('1', $rx->reported_error);
	}
}
