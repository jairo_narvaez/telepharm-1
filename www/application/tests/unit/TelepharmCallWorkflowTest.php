<?php
define('PHARMACIST_ID', 2);
define('PHARMACIST_ID_2', 5);
define('TECHNICIAN_ID', 3);
define('STORE_ID', 1);

/**
 * Mock the Telepharm_Tokbox class
 */
class TelepharmTesting_Tokbox
{
	public function generate()
	{
		return ['session-1', 'token-1'];
	}
}

/**
 * Test the call workflow for:
 * technician user calling a pharmacist
 * technician user on behalf of patient calling a pharmacist to counsel
 * 
 */
class TelepharmCallWorkflowTest extends DatabaseTestCase
{
	protected $call = null;
	protected $lastResult = null;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	/**
	 * set up for all testing
	 */
	public function setUp()
	{
		parent::setUp();
		$this->session = Session::instance();
		$this->call = new Telepharm_Call('testing', 'TelepharmTesting_Tokbox');
		$this->db = Database::instance();
		$this->db->query(Database::DELETE, 'TRUNCATE TABLE `call_queue`');
		$this->lastResult = null;
	}

	private function setAllUsersOfflineAndUnavailable()
	{
		$this->db->query(Database::UPDATE, "UPDATE `account` SET `is_active` = 0
		   , `is_available` = 0");
	}

	private function makeUserOnlineAndAvailable($userId)
	{
		$this->db->query(Database::UPDATE, "UPDATE `account` SET `is_active` = 1
		   	, `is_available` = 1 where `id` = " . $userId);
	}

	private function makeUserOfflineAndUnavailable($userId)
	{
		$this->db->query(Database::UPDATE, "UPDATE `account` SET `is_active` = 0
		   	, `is_available` = 0 where `id` = " . $userId);
	}

	public function testCallUnavailableUsers()
	{
		$this->setAllUsersOfflineAndUnavailable();

		$this->call->fromAccount = TECHNICIAN_ID;
		$this->call->toAccount = PHARMACIST_ID;

		$this->assertFalse($this->call->tryCall());
	}

	public function testCallUnavailableTo()
	{
		$this->setAllUsersOfflineAndUnavailable();
		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);

		$this->call->fromAccount = TECHNICIAN_ID;
		$this->call->toAccount = PHARMACIST_ID;

		$this->assertFalse($this->call->tryCall());
	}

	public function testCallUnavailableFrom()
	{
		$this->setAllUsersOfflineAndUnavailable();
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->call->fromAccount = TECHNICIAN_ID;
		$this->call->toAccount = PHARMACIST_ID;

		$this->assertTrue($this->call->tryCall());
	}

	private function makeCallAvailable()
	{
		$this->setAllUsersOfflineAndUnavailable();
		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->call->fromAccount = TECHNICIAN_ID;
		$this->call->toAccount = PHARMACIST_ID;

		$this->assertTrue($this->call->tryCall());
		$this->assertEquals(1, Model::factory('CallQueue')->count_all());
	}

	public function testCallBusy()
	{
		$this->makeCallAvailable();
		$this->call->accept();
 
		$anotherCall = new Telepharm_Call('busy');

		$anotherCall->fromAccount = TECHNICIAN_ID;
		$anotherCall->toAccount = PHARMACIST_ID;

		try
		{
			$anotherCall->tryCall();
			$this->assertFalse(true, 'it shoul raise an exception');
		}
		catch (Exception $e)
		{
			$this->assertEquals('One of the participants is busy with another call', $e->getMessage());
		}
	}

	public function testCallAfterDismissAnotherCall()
	{
		$this->makeCallAvailable();
		$this->call->dismiss();
 
		$anotherCall = new Telepharm_Call('dismiss');

		$anotherCall->fromAccount = TECHNICIAN_ID;
		$anotherCall->toAccount = PHARMACIST_ID;

		$this->assertTrue($anotherCall->tryCall());
	}

	public function testCheckIncomingCall()
	{
		$this->makeCallAvailable();
		$this->assertEquals(1, count($this->call->getIncomingCalls()));
	}

	public function testRestoreCallFromDB()
	{
		$this->makeCallAvailable();
		$newInstance = new Telepharm_Call('testing');
		$newInstance->callQueueId = 1;
		$this->assertEquals(PHARMACIST_ID, $newInstance->toAccount->id);
		$this->assertEquals(TECHNICIAN_ID, $newInstance->fromAccount->id);
	}


	public function testAcceptCall()
	{
		$this->makeCallAvailable();
		$this->assertTrue($this->call->accept());
		$callQueue = Model::factory('CallQueue')->find();
		$this->assertNotNull($callQueue->session_id);
		$this->assertNotNull($callQueue->token);
	}

	public function testDismissCall()
	{
		$this->makeCallAvailable();
		$this->assertTrue($this->call->dismiss());
		$callQueue = Model::factory('CallQueue')->find();
		$this->assertNull($callQueue->session_id);
		$this->assertNull($callQueue->token);
	}

	public function testFromHangUpCall()
	{
		$this->makeCallAvailable();
		$this->call->accept();
		$this->assertTrue($this->call->hangUp(TECHNICIAN_ID));
		$callQueue = Model::factory('CallQueue')->find();
		$this->assertEquals('from', $callQueue->cancel_reason);
		$this->assertEquals('complete', $callQueue->complete_status);
	}

	public function testToHangUpCall()
	{
		$this->makeCallAvailable();
		$this->call->accept();
		$this->assertTrue($this->call->hangUp(PHARMACIST_ID));
		$callQueue = Model::factory('CallQueue')->find();
		$this->assertEquals('to', $callQueue->cancel_reason);
		$this->assertEquals('complete', $callQueue->complete_status);
	}

	public function testErrorHangUpCall()
	{
		$this->makeCallAvailable();
		$this->call->accept();
		$this->assertTrue($this->call->hangUp(0, true));
		$callQueue = Model::factory('CallQueue')->find();
		$this->assertEquals('error', $callQueue->cancel_reason);
		$this->assertEquals('error', $callQueue->complete_status);
	}

	private function makeCallFromPatient()
	{
		$this->setAllUsersOfflineAndUnavailable();
		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->call->fromAccount = TECHNICIAN_ID;
		$this->call->toStore = STORE_ID;

		$this->assertTrue($this->call->shouldTryOthers());
		$this->assertTrue($this->call->placePatientCall());
	}

	private function makeCallToStore()
	{
		$this->setAllUsersOfflineAndUnavailable();
		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->call->fromAccount = TECHNICIAN_ID;
		$this->call->toStore = STORE_ID;

		$this->assertTrue($this->call->shouldTryOthers());
		$this->assertTrue($this->call->placePharmacistCall());
	}


	/**
	 * @group call-from-patient
	 */
	public function testCallFromPatientOnePharmacistSuccess()
	{
		$this->makeCallFromPatient();
		if ($this->call->shouldTryOthers())
		{
			$call = Model::factory('CallQueue')->find();
			$this->assertNotNull($call->session_id);
			$this->assertNotNull($call->token);
			$this->assertTrue($this->call->generatePossibleDestinations());
			$i = 0;
			while ($ph = $this->call->nextPossibleDestination())
			{
				$this->assertTrue($this->call->tryCallTo($ph));
				$i++;
			}
			$this->assertEquals(1, $i);
		}
		else
			$this->assertTrue(false, 'A call from patient should try other pharmacist');
	}

	/**
	 * @group call-from-patient
	 */
	public function testCallFromPatientOnePharmacistError()
	{
		$this->makeCallFromPatient();
		$this->makeUserOfflineAndUnavailable(PHARMACIST_ID);
		if ($this->call->shouldTryOthers())
		{
			$this->assertTrue($this->call->generatePossibleDestinations());
			while($ph = $this->call->nextPossibleDestination())
			{
				$this->assertFalse($this->call->tryCallTo($ph));
			}
		}
		else
			$this->assertTrue(false, 'A call from patient should try other pharmacist');
	}
	
	/**
	 * @group call-from-patient
	 */
	public function testCallFromPatientTwoPharmacists()
	{
		$this->db->query(Database::INSERT, "
INSERT INTO `account` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `image_id`, `timezone`, `login_dt`, `is_active`, `is_available`, `created_dt`, `updated_dt`)
VALUES
	(5, 'pharmacist2', '2a11azb0kmcyZBZEecyLs830C.kb5BFTElcW8T8fnVPx8ZairAX036qrm', 'pharmacist2@telepharm.com', 'Pharmacist2', 'Name', 4, 'America/Chicago', '2013-01-21 09:55:48', 0, 0, '2012-11-27 22:21:43', '2013-01-21 15:55:48');
			");
		$this->db->query(Database::INSERT, "
INSERT INTO `store_role` (`store_id`, `account_id`, `role_id`, `created_dt`)
VALUES
	(1, 5, 3, '2012-12-12 15:26:05');
			");
		$this->makeCallFromPatient();
		if ($this->call->shouldTryOthers())
		{
			$this->assertTrue($this->call->generatePossibleDestinations());
			while($ph = $this->call->nextPossibleDestination())
			{
				$this->makeUserOfflineAndUnavailable($ph);
				$this->assertFalse($this->call->tryCallTo($ph));
			}
			$this->assertNotNull($this->call->callQueueId);
		}
		else
			$this->assertTrue(false, 'A call from patient should try other pharmacist');
	}
	
	/**
	 * @group call-from-patient
	 */
	public function testCallFromPatientTwoPharmacistsOneDismissed()
	{
		$this->db->query(Database::INSERT, "
INSERT INTO `account` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `image_id`, `timezone`, `login_dt`, `is_active`, `is_available`, `created_dt`, `updated_dt`)
VALUES
	(5, 'pharmacist2', '2a11azb0kmcyZBZEecyLs830C.kb5BFTElcW8T8fnVPx8ZairAX036qrm', 'pharmacist2@telepharm.com', 'Pharmacist2', 'Name', 4, 'America/Chicago', '2013-01-21 09:55:48', 1, 1, '2012-11-27 22:21:43', '2013-01-21 15:55:48');
			");
		$this->db->query(Database::INSERT, "
INSERT INTO `store_role` (`store_id`, `account_id`, `role_id`, `created_dt`)
VALUES
	(1, 5, 3, '2012-12-12 15:26:05');
			");
		$this->makeCallFromPatient();
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID_2);
		if ($this->call->shouldTryOthers())
		{
			$this->assertTrue($this->call->generatePossibleDestinations());
			while($ph = $this->call->nextPossibleDestination())
			{
				$this->assertTrue($this->call->tryCallTo($ph));
				// CallQueue is created, but pharmacist dismissed or not picked up
				$this->call->dismiss();
			}
			$this->assertEquals(1, Model::factory('CallQueue')->count_all());
		}
		else
			$this->assertTrue(false, 'A call from patient should try other pharmacist');
	}

	/**
	 * @group call-from-pharmacist-to-store
	 */
	public function testCallFromPharmacistToStore()
	{
		$this->makeCallToStore();
		if ($this->call->shouldTryOthers())
		{
			$call = Model::factory('CallQueue')->find();
			$this->assertNull($call->session_id);
			$this->assertNull($call->token);
			$this->assertTrue($this->call->generatePossibleDestinations());
			$i = 0;
			while ($ph = $this->call->nextPossibleDestination())
			{
				$this->assertEquals(3, $ph);
				$this->assertTrue($this->call->tryCallTo($ph));
				$i++;
			}
			$this->assertEquals(1, $i);
		}
		else
			$this->assertTrue(false, 'A call from patient should try other pharmacist');
	}



	/**
	 * @group call-ajax
	 */
	public function testAjaxPlaceCall()
	{
		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->session->regenerate();
		$this->session['account_id'] = TECHNICIAN_ID;

		$req = Request::factory('ajax/videocall/place');
		$req->post('to', PHARMACIST_ID);
		$req->post('type', 'user');
		$req->requested_with('xmlhttprequest');
		$req->method('POST');
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertTrue($result['result']['placed']);
		$this->assertEquals(1, $result['result']['call_id']);
	}

	/**
	 * @group call-ajax
	 */
	public function testAjaxCheckCalling()
	{
		$this->testAjaxPlaceCall();

		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->session->regenerate();
		$this->session['account_id'] = PHARMACIST_ID;

		$req = Request::factory('ajax/videocall/check_incoming');
		$req->requested_with('xmlhttprequest');
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertEquals(1, count($result['result']));
		$this->lastResult = $result;
	}

	/**
	 * @group call-ajax
	 */
	public function testAjaxAcceptCalling()
	{
		$this->testAjaxCheckCalling();

		$this->session->regenerate();
		$this->session['account_id'] = PHARMACIST_ID;

		$req = Request::factory('ajax/videocall/accept');
		$req->requested_with('xmlhttprequest');
		$req->method('POST');
		$req->post('call_id', $this->lastResult['result'][0]['id']);
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertTrue($result['result']['accepted']);
		$this->assertEquals(18, count($result['result']['call']));
	}

	/**
	 * @group call-ajax
	 */
	public function testAjaxDismissCalling()
	{
		$this->testAjaxCheckCalling();

		$this->session->regenerate();
		$this->session['account_id'] = PHARMACIST_ID;

		$req = Request::factory('ajax/videocall/dismiss');
		$req->requested_with('xmlhttprequest');
		$req->method('POST');
		$req->post('call_id', $this->lastResult['result'][0]['id']);
		$req->post('action', 'dismiss');
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertTrue($result['result']['dismissed']);
	}

	/**
	 * @group call-ajax
	 */
	public function testAjaxCheckAcceptOnCallOrDrop()
	{
		$this->testAjaxAcceptCalling();

		$this->session->regenerate();
		$this->session['account_id'] = TECHNICIAN_ID;

		$req = Request::factory('ajax/videocall/check_call');
		$req->requested_with('xmlhttprequest');
		$req->query('call_id', 1);
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertTrue($result['result']['answered']);
		$this->assertEquals(1, $result['result']['call']['id']);
	}

	/**
	 * @group call-ajax
	 */
	public function testAjaxHangUpCall()
	{
		$this->testAjaxAcceptCalling();

		$this->session->regenerate();
		$this->session['account_id'] = PHARMACIST_ID;

		$req = Request::factory('ajax/videocall/hangup');
		$req->requested_with('xmlhttprequest');
		$req->method('POST');
		$req->post('call_id', 1);
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertTrue($result['result']['hanged']);
	}


	/**
	 * @group patient-call-ajax
	 */
	public function testAjaxPatientPlaceCall()
	{
		$this->makeUserOnlineAndAvailable(TECHNICIAN_ID);
		$this->makeUserOnlineAndAvailable(PHARMACIST_ID);

		$this->session->regenerate();
		$this->session['account_id'] = TECHNICIAN_ID;

		$req = Request::factory('ajax/videocall/patient_place');
		$req->requested_with('xmlhttprequest');
		$req->method('POST');
		$req->post('prescription_id', 1);
		$res = $req->execute();

		$result = json_decode($res->body(), true);

		$this->assertFalse($result['error']);
		$this->assertTrue($result['result']['placed']);
		$this->assertEquals(1, $result['result']['call_id']);
	}


	/**
	 * @group tel-152
	 */
	public function testQuitUnansweredCall()
	{
		$this->makeCallFromPatient();
		$call = Model::factory('CallQueue')->find();
		$call->created_dt = date_create('6 minutes ago UTC');
		$call->save();
		$this->assertNotNull($call->session_id);
		$this->assertNotNull($call->token);
		Telepharm_Call::purgeUnAnsweredCalls();
		$call = Model::factory('CallQueue')->find();
		$this->assertEquals(0, $call->is_pending);
		$this->assertEquals('canceled', $call->call_status);
		$this->assertEquals('no answer', $call->cancel_reason);
		$this->assertEquals('error', $call->complete_status);
	}

}
