<?php

/**
* Test the prescription creation process
* taking in  account that a prescription
* can be get from different PMS sources
*/
class TelepharmCreatePrescriptionTest extends DatabaseTestCase
{
	protected $manager = null;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$account = new Model_Account(3); # Technician
		$currentUser = new CurrentUser($account);
		$store = new Model_Store(1);
		$store->url = 'faked';
		$store->pms_query_class = 'Telepharm_PMS_Testing';
		$store->save();
		$currentUser->addStore(1); # Chicago 1 Store
		$this->manager = new Telepharm_Manager_CreatePrescription(
			$currentUser,
			'123456');
	}

	public function testGetCorrectStore()
	{
		$store = $this->manager->getStore();
		$this->assertEquals('1', $store['id']);
		$this->assertEquals('faked', $store['url']);
		$this->assertEquals('Telepharm_PMS_Testing', $store['pms_query_class']);
	}

	public function testGetPrescription()
	{
		$prescription = $this->manager->getPrescription();
		$this->assertInstanceOf('Telepharm_PMS_Result_Prescription', $prescription);
		$this->assertEquals('123456', $prescription->number);
	}

	public function testCreatePrescription()
	{
		$prescription = $this->manager->createPrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription);
		$this->assertEquals('123456', $prescription->number);
		$this->assertEquals('1', $prescription->patient_id);
		$this->assertTrue($prescription->loaded());
		$this->assertTrue($prescription->prescription_store->loaded());
		$this->assertEquals(1, $prescription->prescription_logs->count_all() );
		$this->assertEquals(1, $prescription->prescription_drugs->count_all() );
		$this->assertEquals(1, $prescription->prescription_dispenses->count_all() );
		$this->assertEquals(1, $prescription->prescription_prescribers->count_all() );
	}

	public function testFindOrCreatePrescriber()
	{
		$db = Database::instance();
		$db->query(Database::UPDATE, 'SET foreign_key_checks = 0;');
		$db->query(Database::DELETE, 'TRUNCATE TABLE `prescription_prescriber`');
		$db->query(Database::DELETE, 'TRUNCATE TABLE `prescriber`');
		$db->query(Database::UPDATE, 'SET foreign_key_checks = 1;');
		$this->assertEquals(0, Model::factory('Prescriber')->count_all() );

		$prescription = $this->manager->createPrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription);
		$this->assertEquals('123456', $prescription->number);
		$this->assertEquals(1, $prescription->prescription_prescribers->count_all() );
		$this->assertEquals(1, Model::factory('Prescriber')->count_all() );

		$this->manager->setPrescriptionNumber('123457');
		$prescription = $this->manager->createPrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription);
		$this->assertEquals('123457', $prescription->number);
		$this->assertEquals(1, $prescription->prescription_prescribers->count_all() );
		// There should be only 1 prescriber (the same as created before)
		$this->assertEquals(1, Model::factory('Prescriber')->count_all() );
	}

	public function testCreatePrescriptionAlreadyCreated()
	{
		$prescription = $this->manager->createPrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription);
		$this->assertEquals('123456', $prescription->number);
		$this->assertEquals('1', $prescription->patient_id);
		$this->assertTrue($prescription->loaded());
		$this->assertTrue($prescription->prescription_store->loaded());
		$this->assertEquals(1, $prescription->prescription_logs->count_all() );
		$this->assertEquals(1, $prescription->prescription_drugs->count_all() );
		$this->assertEquals(1, $prescription->prescription_dispenses->count_all() );
		$this->assertEquals(1, $prescription->prescription_prescribers->count_all() );

		$this->setExpectedException('Exception', 'Prescription already exists', $prescription->id);
		$prescription2 = $this->manager->createPrescription();
	}

	public function testCreatePrescriptionAlreadyCreatedAndCanceled()
	{
		$prescription = $this->manager->createPrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription);
		$this->assertEquals('123456', $prescription->number);
		$this->assertEquals('1', $prescription->patient_id);
		$this->assertTrue($prescription->loaded());
		$this->assertTrue($prescription->prescription_store->loaded());
		$this->assertEquals(1, $prescription->prescription_logs->count_all() );
		$this->assertEquals(1, $prescription->prescription_drugs->count_all() );
		$this->assertEquals(1, $prescription->prescription_dispenses->count_all() );
		$this->assertEquals(1, $prescription->prescription_prescribers->count_all() );

		$prescription->canceled = true;
		$prescription->save();

		$prescription2 = $this->manager->createPrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription2);
		$this->assertEquals('123456', $prescription2->number);
	}


}
