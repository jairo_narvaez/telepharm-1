<?php

/**
* Test the class Telepharm_PMS_CRX
*/
class TelepharmPMSCRXTest extends DatabaseTestCase
{
	protected $pmsQuery = null;
	/**
	 * set up for all testing
	 */
	public function setUp()
	{
		parent::setUp();
		$this->db = Database::instance();
		$this->db->query(Database::DELETE, 'TRUNCATE TABLE `prescription_raw`');
	}

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	/**
	 * Check the pre-draft creation
	 */
	public function testTelepharmPMSCRXCreatePreDraftPrescription()
	{
		$store = new Model_Store(1);
		$store->url = NULL;
		$store->pms_query_class = 'Telepharm_PMS_CRX';
		$store->save();

	    $pmsQuery = Telepharm_PMS::factory($store);

		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$data1 = @include $fixturesDir.'crx-1.php';

		$account = new Model_Account(3); # Technician
		$currentUser = new CurrentUser($account);
		$currentUser->addStore(1); # Chicago 1 Store

	    $prescription = $pmsQuery->createPreDraftPrescription($data1, $currentUser);

	    $this->assertEquals('600008', $prescription->number);
	    $this->assertEquals(1, $prescription->pre_draft);
	    $this->assertEquals(0, $prescription->is_draft);
	}

	/**
	 * Try to create 2 rx on telepharm
	 * 1 is the first fill
	 * 2 is the second refill
	 */
	public function testTelepharmPMSCRXCreatePreDraftPrescriptionRefill()
	{
		$store = new Model_Store(1);
		$store->url = NULL;
		$store->pms_query_class = 'Telepharm_PMS_CRX';
		$store->save();
		$account = new Model_Account(3); # Technician
		$currentUser = new CurrentUser($account);
		$currentUser->addStore(1); # Chicago 1 Store

	    $pmsQuery = Telepharm_PMS::factory($store);

		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$data1 = @include $fixturesDir.'crx-1.php';

	    $prescription = $pmsQuery->createPreDraftPrescription($data1, $currentUser);

	    $this->assertEquals('600008', $prescription->number);
	    $this->assertEquals(1, $prescription->refill_number);
	    $this->assertEquals(1, $prescription->pre_draft);
	    $this->assertEquals(0, $prescription->is_draft);

		// Try to create the new refill
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$data1 = @include $fixturesDir.'crx-1-refill.php';

	    $prescription = $pmsQuery->createPreDraftPrescription($data1, $currentUser);

	    $this->assertEquals('600008', $prescription->number);
	    $this->assertEquals(2, $prescription->refill_number);
	    $this->assertEquals(1, $prescription->pre_draft);
	    $this->assertEquals(0, $prescription->is_draft);
	
	}

	/**
	 * This should be uncommented just to check when
	 */
	public function testTelepharmPMSCRXGetPatientHistory()
	{
		$store = new Model_Store(1);
		$store->url = NULL;
		$store->pms_query_class = 'Telepharm_PMS_CRX';
		$store->save();
		$account = new Model_Account(3); # Technician
		$currentUser = new CurrentUser($account);
		$currentUser->addStore(1); # Chicago 1 Store

	    $pmsQuery = Telepharm_PMS::factory($store);

		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$data1 = @include $fixturesDir.'crx-1.php';

	    $prescription = $pmsQuery->createPreDraftPrescription($data1, $currentUser);

		$history = $pmsQuery->getPatientHistory('2');

		$this->assertEquals(1, count($history));

	}
	/**
	 * Check that we are decoding fine the results
	 *
	 * @dataProvider getDifferentDataResults
	 * @covers TelepharmPMSCRXMakeStandard
	 * @return void
	 */
	public function testTelepharmPMSCRXMakeStandard($data, $result)
	{
		$store = new Model_Store();
		$store->pms_query_class = 'Telepharm_PMS_CRX';

	    $pmsQuery = Telepharm_PMS::factory($store);

	    $standard = $pmsQuery->makeResultStandard($data);

	    $this->assertInstanceOf('Telepharm_PMS_Result_Prescription', $standard);

	    $keys = [
	    	'number',
			'patientFirstName',
			'patientLastName',
			'patientDob',
		];

		foreach ($keys as $key) {
		    $this->assertEquals($result->{$key}, $standard->{$key});
		}

	    $this->assertInstanceOf('Telepharm_PMS_Result_Prescriber', $standard->prescriber);
	    $this->assertInstanceOf('Telepharm_PMS_Result_Dispense', $standard->dispense);
	    $this->assertInstanceOf('Telepharm_PMS_Result_Drug', $standard->drug);

	    if (!$standard->isValid())
	    {
	    	error_log(var_export($standard->errors,1));
	    	error_log(var_export($standard->dispense->errors,1));
	    	error_log(var_export($standard->drug->errors,1));
	    	error_log(var_export($standard->prescriber->errors,1));
	    }
	    $this->assertTrue($standard->isValid());
	}

	/**
	 * Data Provider for testTelepharmPMSCRXMakeStandard
	 *
	 * @return array
	 */
	public function getDifferentDataResults()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$data1 = @include $fixturesDir.'crx-1.php';
		$result1 = new Telepharm_PMS_Result_Prescription;
		$result1->number = '600008';
		$result1->refillNumber = '1';
		$result1->patientFirstName = 'ALICE';
		$result1->patientLastName = 'GREENFIELD';
		$result1->patientDob = '1964-01-15';

		$data2 = @include $fixturesDir.'crx-2.php';
		$result2 = new Telepharm_PMS_Result_Prescription;
		$result2->number = '600006';
		$result2->refillNumber = '1';
		$result2->patientFirstName = 'BOB';
		$result2->patientLastName = 'SMITH';
		$result2->patientDob = '1955-06-01';

	    return array(
	        array($data1, $result1),
	        array($data2, $result2),
	    );
	}


}

