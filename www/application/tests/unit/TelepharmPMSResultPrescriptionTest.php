<?php

/**
* Test the class Telepharm_PMS_Result_Prescription
*/
class TelepharmPMSResultPrescrptionTest extends Unittest_TestCase
{
	/**
	 * check if precription info is valid to
	 * create a prescription on the db
	 *
	 * @dataProvider differerntPrescriptions
	 * @covers TelepharmPMSResultPrescrption
	 * @return void
	 */
	public function testTelepharmPMSResultPrescription($prescription, $result)
	{
		$valid = $prescription->isValid();
	    $this->assertEquals($result, $valid);
	    if ($valid)
		{
		   	Kohana::$log->add(Kohana_Log::DEBUG, var_export($prescription,1));
		}
		else
		{
			Kohana::$log->add(Kohana_Log::DEBUG, var_export($prescription->errors,1));
		}
	}

	/**
	 * Data Provider for testTelepharmPMSResultPrescription
	 *
	 * @return array
	 */
	public function differerntPrescriptions()
	{
		$p1 = new Telepharm_PMS_Result_Prescription;
		$p1->patientID = '1';
		$p1->patientFirstName = 'Marcelo';
		$p1->patientLastName = 'Andrade';
		$p1->patientDob = '1980-08-05';
		$p1->patientPhone = '1234567890';
		$p1->number = '1';
		$p1->isRefill = false;
		$prescriber = new Telepharm_PMS_Result_Prescriber;
		$prescriber->firstName = 'Cristina';
		$prescriber->lastName = 'Medina';
		$prescriber->phone = '8007892501';
		$prescriber->npi = '123456';
		$prescriber->identifier = '123456';
		$p1->prescriber = $prescriber;
		$dispense = new Telepharm_PMS_Result_Dispense;
		$dispense->qtyWritten = '2';
		$dispense->qtyDispensed = '1';
		$dispense->sig = ' Take 1 tablet by mouth daily.';
		$dispense->sigCoded = ' Take 1 tablet by mouth daily.';
		$dispense->sigExpanded = ' Take 1 tablet by mouth daily.';
		$dispense->daysSupply = '60';
		$dispense->fillsOwed = '1';
		$dispense->refillAuth = '1';
		$p1->dispense = $dispense;
		$drug = new Telepharm_PMS_Result_Drug;
		$drug->newNdc = '00143126301';
		$drug->manf = 'WEST-WARD';
		$drug->description = 'LISINOPRIL-HCTZ 20-12.5 MG';
		$drug->packageSize = '100.000';
		$drug->strength = '20-12.5 MG';
		$drug->packageSizeUnitCode = 'EA';
		$p1->drug = $drug;


		$p2 = new Telepharm_PMS_Result_Prescription;
		$p2->patientID = '1';
		$p2->patientFirstName = 'Marcelo';
		$p2->patientLastName = 'Andrade';
		$p2->patientDob = '1980-08-'; # Error on date
		$p2->number = '1';
		$p2->isRefill = false;

		$p3 = new Telepharm_PMS_Result_Prescription;
		$p3->patientID = '1';
		$p3->patientFirstName = 'Marcelo';
		$p3->patientLastName = ''; # No last name
		$p3->patientDob = '1980-08-05';
		$p3->number = '1';
		$p3->isRefill = false;

		$p4 = new Telepharm_PMS_Result_Prescription;
		$p4->patientID = '1';
		$p4->patientFirstName = ''; # No first name
		$p4->patientLastName = 'Andrade';
		$p4->patientDob = '1980-08-05';
		$p4->number = '1';
		$p4->isRefill = false;

		$p5 = new Telepharm_PMS_Result_Prescription;
		$p5->patientID = '1';
		$p5->patientFirstName = 'Marcelo';
		$p5->patientLastName = 'Andrade';
		$p5->patientDob = '1980-08-05';
		$p5->number = ''; # No number
		$p5->isRefill = false;

		$p6 = new Telepharm_PMS_Result_Prescription;
		$p6->patientID = '1';
		$p6->patientFirstName = 'Marcelo';
		$p6->patientLastName = 'Andrade';
		$p6->patientDob = '1980-08-05';
		$p6->number = '123';
		$p6->isRefill = false;
		$p6->patientPhone = '1234567'; # No valid phone number
		$p6->prescriber = $prescriber;
		$p6->dispense = $dispense;
		$p6->drug = $drug;

		$p7 = new Telepharm_PMS_Result_Prescription;
		$p7->patientID = ''; # no patient id
		$p7->patientFirstName = 'Marcelo';
		$p7->patientLastName = 'Andrade';
		$p7->patientDob = '1980-08-05';
		$p7->number = '123';
		$p7->isRefill = false;

	    return array(
	        array($p1, true ),
	        array($p2, false),
	        array($p3, false),
	        array($p4, false),
	        array($p5, false),
	        array($p6, true),
	        array($p7, false),
	    );
	}

}
