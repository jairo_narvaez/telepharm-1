<?php

/**
* Test the class Telepharm_PMS_RxKey
*/
class TelepharmPMSRxKeyTest extends Unittest_TestCase
{
	protected $pmsQuery = null;

	/**
	 * This should be uncommented just to check when
	 */
	public function testTelepharmPMSRxKeyGetPrescription()
	{
		$store = new Model_Store();
		$store->url = 'http://208.126.153.1:90/rxkey-api/';
		$store->pms_query_class = 'Telepharm_PMS_RxKey';

	    $pmsQuery = Telepharm_PMS::factory($store);

	    $prescription = $pmsQuery->getPrescription('1');

	    $this->assertEquals('1', $prescription->number);
	}

	/**
	 * This should be uncommented just to check when
	 */
	public function testTelepharmPMSRxKeyGetPatientHistory()
	{
		$store = new Model_Store();
		$store->url = 'http://208.126.153.1:90/rxkey-api/';
		$store->pms_query_class = 'Telepharm_PMS_RxKey';

	    $pmsQuery = Telepharm_PMS::factory($store);

	    $history = $pmsQuery->getPatientHistory('7');

	    $this->assertEquals(2, count($history));
	}
	/**
	 * Check that we are decoding fine the results
	 *
	 * @dataProvider getDifferentJSONResults
	 * @covers TelepharmPMSRxKeyMakeStandard
	 * @return void
	 */
	public function testTelepharmPMSRxKeyMakeStandard($json, $result)
	{
		$store = new Model_Store();
		$store->pms_query_class = 'Telepharm_PMS_RxKey';

	    $pmsQuery = Telepharm_PMS::factory($store);

		$json_decoded = json_decode($json, true);
	    $standard = $pmsQuery->makeResultStandard($json_decoded['result']);

	    $this->assertInstanceOf('Telepharm_PMS_Result_Prescription', $standard);

	    $keys = [
	    	'number',
			'patientFirstName',
			'patientLastName',
			'patientDob',
		];

		foreach ($keys as $key) {
		    $this->assertEquals($result->{$key}, $standard->{$key});
		}

	    $this->assertInstanceOf('Telepharm_PMS_Result_Prescriber', $standard->prescriber);
	    $this->assertInstanceOf('Telepharm_PMS_Result_Dispense', $standard->dispense);
	    $this->assertInstanceOf('Telepharm_PMS_Result_Drug', $standard->drug);

	    if (!$standard->isValid())
	    {
	    	error_log(var_export($standard->errors,1));
	    	error_log(var_export($standard->dispense->errors,1));
	    	error_log(var_export($standard->drug->errors,1));
	    	error_log(var_export($standard->prescriber->errors,1));
	    }
	    $this->assertTrue($standard->isValid());
	}

	/**
	 * Data Provider for testTelepharmPMSRxKeyMakeStandard
	 *
	 * @return array
	 */
	public function getDifferentJSONResults()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		$json1 = file_get_contents($fixturesDir.'/rxkey_json1.json');
		$result1 = new Telepharm_PMS_Result_Prescription;
		$result1->number = '1';
		$result1->patientFirstName = 'TODD';
		$result1->patientLastName = 'THOMPSON';
		$result1->patientDob = '1971-03-12';

		$json2 = file_get_contents($fixturesDir.'/rxkey_json2.json');
		$result2 = new Telepharm_PMS_Result_Prescription;
		$result2->number = '100881';
		$result2->patientFirstName = 'RUSSELL';
		$result2->patientLastName = 'BRUNER';
		$result2->patientDob = '1959-06-25';

	    return array(
	        array($json1, $result1),
	        array($json2, $result2),
	    );
	}


}
