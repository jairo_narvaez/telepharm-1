<?php
/**
 * Test the tablet sync workflow for:
 *
 * technician initiate the tablet
 * 
 */
class TelepharmTabletSyncTest extends DatabaseTestCase
{
	protected $syncManager = null;
	protected $db = null;

	public function setUp()
	{
		parent::setUp();
		$this->syncManager = new Telepharm_Manager_TabletSync();
		$account = new Model_Account(3); # Technician
		$currentUser = new CurrentUser($account);
		$store = new Model_Store(1);
		$store->url = 'faked';
		$store->pms_query_class = 'Telepharm_PMS_Testing';
		$store->save();
		$currentUser->addStore(1); # Chicago 1 Store
		$this->currentUser = $currentUser;

		$this->db = Database::instance();
		$this->db->query(Database::DELETE, "TRUNCATE TABLE `patient_api_account`");
		$this->db->query(Database::DELETE, "TRUNCATE TABLE `patient_api_session`");
		$this->db->query(Database::INSERT, "
INSERT INTO `patient_api_account` (`id`, `uuid`, `name`, `store_id`, `is_active`, `created_dt`)
VALUES
	(1, '123', 'iPad at Victor', 1, 1, '0000-00-00 00:00:00');");
	}

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}


	public function testInitiatePatientTablet()
	{
		$prescriptionId = 1;
		$initiated = $this->syncManager->initiate($this->currentUser, $prescriptionId);
		$this->assertTrue($initiated);
		$session = Model::factory('PatientApiSession')->find();
		$this->assertTrue($session->loaded());
		$this->assertEquals($prescriptionId, $session->prescription_id);
		$this->assertEquals(1, $session->patient_api_account_id);
	}

	public function testInitiatePatientTabletButBusy()
	{
		$this->db->query(Database::INSERT, "
INSERT INTO `patient_api_session` (`id`, `token`, `patient_api_account_id`, `prescription_id`, `created_dt`, `end_dt`)
VALUES
	(1, 'beyxpbgexkerwwearuetqxjwebcpkpucfyunthzavprlfiaqsk', 1, 1, '2013-02-21 18:35:44', NULL);
");			
		$prescriptionId = 1;
		$this->setExpectedException('HTTP_Exception_501', 'The tablet is currently reserved (Rx # 102030)');
		$initiated = $this->syncManager->initiate($this->currentUser, $prescriptionId);
	}

	public function testInitiatePatientTabletButNoOneProvisioned()
	{
		$this->db->query(Database::DELETE, "TRUNCATE TABLE `patient_api_account`");
		$this->db->query(Database::DELETE, "TRUNCATE TABLE `patient_api_session`");
		$prescriptionId = 1;
		$this->setExpectedException('HTTP_Exception_401', 'Tablet is not registered in the store');
		$initiated = $this->syncManager->initiate($this->currentUser, $prescriptionId);
	}

}
