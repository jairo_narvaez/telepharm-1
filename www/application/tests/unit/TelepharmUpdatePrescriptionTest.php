<?php

/**
* Test the prescription update process
* taking in  account that a prescription
* can be get from different PMS sources
*/
class TelepharmUpdatePrescriptionTest extends DatabaseTestCase
{
	protected $manager = null;

	/**
	 * Populates the database using a xml file
	 * created using mysqldump -X telepharm > telepharm.xml
	 */
	public function getDataSet()
	{
		$fixturesDir = DOCROOT . 'application/tests/fixtures/';
		return $this->createMySQLXMLDataSet($fixturesDir .
			'prescription_create_manager.xml');
	}

	public function setUp()
	{
		parent::setUp();
		$db = Database::instance();
		$db->query(Database::DELETE, 'TRUNCATE TABLE `prescription_log`');
		$account = new Model_Account(3); # Technician
		$currentUser = new CurrentUser($account);
		$store = new Model_Store(1);
		$store->url = 'faked';
		$store->pms_query_class = 'Telepharm_PMS_Testing';
		$store->save();
		$currentUser->addStore(1); # Chicago 1 Store
		$this->manager = new Telepharm_Manager_UpdatePrescription(
			$currentUser,
			1);
	}

	public function testGetCorrectStore()
	{
		$store = $this->manager->getStore();
		$this->assertEquals('1', $store['id']);
		$this->assertEquals('faked', $store['url']);
		$this->assertEquals('Telepharm_PMS_Testing', $store['pms_query_class']);
	}

	public function testGetPrescription()
	{
		$prescription = $this->manager->getPrescription();
		$this->assertInstanceOf('Telepharm_PMS_Result_Prescription', $prescription);
		$this->assertEquals('102030', $prescription->number);
	}

	public function testUpdatePrescription()
	{
		$prescription = $this->manager->updatePrescription();
		$this->assertInstanceOf('Model_Prescription', $prescription);
		$this->assertEquals('102030', $prescription->number);
		$this->assertTrue($prescription->loaded());
		$this->assertTrue($prescription->prescription_store->loaded());
		$this->assertEquals(1, $prescription->prescription_logs->count_all() );
		$this->assertEquals(1, $prescription->prescription_drugs->count_all() );
		$this->assertEquals(1, $prescription->prescription_dispenses->count_all() );
		$this->assertEquals(1, $prescription->prescription_prescribers->count_all() );
	}

}
