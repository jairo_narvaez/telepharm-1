<?php
namespace traits;

use TelePharm_Event;

trait TelePharm_EventSource
{
	protected $_eventListeners = [];

	public function addEventListener($type, callable $listener)
	{
		$this->_eventListeners[$type][] = $listener;
	}

	public function removeEventListner($type, callable $listener)
	{
		foreach ($this->_eventListeners[$type] as $key => $val)
		{
			if ($listener === $val)
				unset($this->_eventListeners[$type][$key]);
		}
	}

	public function dispatchEvent(TelePharm_Event $event)
	{
		if (isset($this->_eventListeners[$event->type]))
		{
			foreach ($this->_eventListeners[$event->type] as $listener)
			{
				$listener($event);
				if ($event->canceled) break;
			}	
		}
	}
}