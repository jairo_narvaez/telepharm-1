<?php
namespace traits;

Trait TelePharm_Scaffold
{
	public static function getModelName() { throw new Exception('Please implement this on your controller'); }
	public static function getViewsDir() { return 'admin/scaffold'; }
	public static function getHumanName() { throw new Exception('Please implement this on your controller'); }
	public static function getUrl() { throw new Exception('Please implement this on your controller'); }
	public static function getDataProvider() { return new \TelePharm_DataProvider(self::getModelName()); }
	public static function getAdditionalActions() { return 'admin/scaffold/additional_actions'; }

	public function action_index()
	{
		$view = \View::factory(self::getViewsDir().'/index');
		$provider = $this->getDataProvider();
		$view->columns = $provider->getColumns();
		$view->rows = $provider->getRows();
		$view->model = $this->getModelName();
		$humanName = self::getHumanName();
		$view->humanName = $humanName;
		$view->url = self::getUrl();
		$additionalActions = self::getAdditionalActions();
		$view->untaint('additionalActions');
		$view->additionalActions = \View::factory($additionalActions);
		$this->template->title = "{$humanName}";
		$this->template->content = $view;
	}

	/* GET for form, POST for create */
	public function action_create()
	{
		$view = \View::factory(self::getViewsDir().'/form');
		$provider = $this->getDataProvider();
		$form = $provider->createForm(self::getUrl().'create');
		$humanName = self::getHumanName();
		
		if (count($_POST))
		{
			$form->load($_POST);
			if ($form->validate())
			{
				if ($provider->create($form))
				{
					\TelePharm_Flash::instance()->message('success', "{$humanName} created successfully");
					$this->request->redirect(self::getUrl());
				}
			}
		}

		$view->form = $form;
		$view->humanName = $humanName;
		$view->model = $this->getModelName();
		$view->action = 'Create';
		$view->url = self::getUrl();
		$this->template->title = "New {$humanName}";
		$this->template->content = $view;
	}

	/* GET for form, POST for update */
	public function action_edit()
	{
		$view = \View::factory(self::getViewsDir().'/form');
		$provider = $this->getDataProvider();
		$id = $this->request->param('id');
		$form = $provider->createForm(self::getUrl().'edit/'.$id, $id);
		$humanName = self::getHumanName();
		
		if (count($_POST))
		{
			$form->load($_POST);
			if ($form->validate())
			{
				if ($provider->update($form))
				{
					\TelePharm_Flash::instance()->message('success', "{$humanName} updated successfully");
					$this->request->redirect(self::getUrl());
				}
			}
		}

		$view->form = $form;
		$view->humanName = $humanName;
		$view->url = self::getUrl();
		$view->action = 'Edit';
		$view->model = $this->getModelName();
		$this->template->title = "Edit {$humanName}";
		$this->template->content = $view;
	}

	public function action_delete()
	{
		$view = \View::factory(self::getViewsDir().'/delete');
		$provider = $this->getDataProvider();
		$id = $this->request->param('id');
		$form = $provider->createForm(self::getUrl().'delete/'.$id, $id);
		$humanName = self::getHumanName();
		
		if (count($_POST))
		{
			if ($provider->delete($id))
			{
				\TelePharm_Flash::instance()->message('success', "{$humanName} deleted successfully");
				$this->request->redirect(self::getUrl());
			}
		}

		$view->form = $form;
		$view->humanName = $humanName;
		$view->url = self::getUrl();
		$view->action = 'Delete';
		$view->model = $this->getModelName();
		$this->template->title = "Delete {$humanName}";
		$this->template->content = $view;
	}
}
