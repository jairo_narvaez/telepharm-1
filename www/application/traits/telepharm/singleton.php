<?php
namespace traits;

/**
 * The singleton pattern.
 *
 * Any class that uses this trait turns into a singleton.
 *
 * class Foo
 * {
 *    use traits\TelePharm_Singleton;
 *    protected function initialize()
 *    {
 *       # 'constructor' goes here
 *    }
 * }
 *
 * $foo = Foo::getInstance();
 * 
 */
trait TelePharm_Singleton
{
	static protected $__instance;

	/**
	 * Constructor calls the initialize() function.
	 *
	 * The classes actual initialization happens in the initialize() function
	 */
	final private function __construct()
	{
		$this->initialize();
	}

	/**
	 * Creates the instance if it doesn't yet exit and returns it.
	 *
	 * @return object    The single instance of the class
	 */
	static public function getInstance()
	{
		if (!static::$__instance)
			static::$__instance = new static();			

		return static::$__instance;
	}

	/**
	 * The class must implement the initialze() function
	 *
	 * @return void    The return value is ignored
	 */
	abstract protected function initialize();

	/**
	 * Prevents cloning.
	 *
	 * @throws Exception
	 */
	final public function __clone()
	{
		throw new Exception("Singletons cannot be cloned.");
	}

	/**
	 * Prevents unserializing.
	 *
	 * @throws Exception
	 */
	final public function __wakeup()
	{
		throw new Exeption("Singletons cannot be unserialized");
	}
}