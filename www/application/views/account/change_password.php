<div class="row profile-page">
	<div class="span6 offset3 box">
		<div class="row">
			<div class="span6 well">
				<h5>Change Password</h5>
				<hr/>
				<form method="POST" enctype="multipart/form-data">
				<div class="row">
					<div class="span4 data">
						<?php if ($form->errors): ?>
						<ul class="error">
						<?php foreach ($form->errors as $error): ?>
							<li><?= htmlspecialchars($error) ?>.</li>
						<?php endforeach ?>
						</ul>
						<?php endif ?>
						<?= $form['old_password']->render() ?>
						<?= $form['new_password']->render() ?>
						<?= $form['confirmation']->render() ?>
						<?= $form['submit']->render() ?>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

