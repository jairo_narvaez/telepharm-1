<script>
    require(['account_index']);
</script>
<div class="row profile-page">
    <div class="span6 offset3 box">
        <div class="row">
            <div class="span6 well">
                <h5>Account Profile</h5>
                <hr/>
                <div class="row">
                    <div class="span4 data">
                        <dl>
                            <dt>Name
                            <dd><?=$account->first_name . ' ' . $account->last_name?></dd>
                            </dt>
                            <dt>Email
                            <dd><?=$account->email?></dd>
                            </dt>
                            <dt>
                            <dd><a href="<?= url::www('account/change_password') ?>">Change Password</a></dd>
                            </dt>
                        </dl>
                    </div>
                    <div class="span2 image">
                        <div class="photo">
                            <img src="<?= URL::uploaded_image($current_user->account->image, 'no-photo.png'); ?>" alt=""
                                 class="img-polaroid"/>
                        </div>
                        <br/>
                        <a href="<?= url::www('account/update') ?>">Upload New Photo</a>
                    </div>
                </div>
                <p>
                    <a class="btn btn-primary" href="<?= URL::www('account/update') ?>">Edit Profile</a>
                </p>
            </div>
        </div>
    </div>
</div>

