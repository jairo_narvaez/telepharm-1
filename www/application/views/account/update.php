<script>
    require(['account_update']);
</script>
<div class="row profile-page">
	<div class="span6 offset3 box">
		<div class="row">
			<div class="span6 well">
				<h5>Edit Profile</h5>
				<hr/>
				<div class="row">
					<form method="POST" enctype="multipart/form-data">
					<div class="span4 data">
						<?= $form['first_name']->render();?>
						<?= $form['last_name']->render();?>
						<?= $form['email']->render();?>
                        <?= view::factory('partials/pharmacist_store_assignment')->bind('company_stores', $company_stores)->bind('user_stores', $user_stores)->bind('currently_associated', $currently_associated); ?>
                        <?= view::factory('partials/technician_store_assignment')->bind('company_stores', $company_stores)->bind('user_stores', $user_stores)->bind('currently_associated', $currently_associated); ?>
						<dl>
							<dt>
								<dd><a href="<?=url::www('account/change_password')?>">Change Password</a></dd>
							</dt>
						</dl>
						<?= $form['submit']->render();?>
					</div>
					<div class="span2 image">
						<div class="photo">
							<img src="<?= URL::uploaded_image($current_user->account->image, 'no-photo.png');?>" alt="" class="img-polaroid" />
						</div>
						<?= $form['image']->render(['style' => 'width: 170px; font-size: 11px']);?>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
