<?php if ($permissions->isAllowed('account', 'view-roles')): ?>
<a class="btn btn-mini" 
	href="<?= url::www('admin/account/'.$row['id'].'/roles') ?>">
	<i class="icon-briefcase"></i> Roles</a>
<?php endif; ?>

