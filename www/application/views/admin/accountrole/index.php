<script type="text/javascript">
	require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/account')?>">Accounts</a> <span class="divider">/</span></li>
			<li class="active">Roles of user <strong><?=$account['full_name']?></strong></li>
		</ul>
	</div>
</div>
<hr />
<div class="row">
	<div class="span13">
		<table id="records" style="margin-top: 10px;" class="table table-striped table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>ID <span class="sorting-icon"></span></th>
					<th>Role <span class="sorting-icon"></span></th>
					<th>Company/Store <span class="sorting-icon"></span></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row): ?>
				<tr>
					<td>
					<?= $row['id'] ?>
					</td>
					<td>
					<?= $row['name'] ?>
					</td>
					<td>
					<?= $row['type'] ?>
					</td>
					<td>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>


