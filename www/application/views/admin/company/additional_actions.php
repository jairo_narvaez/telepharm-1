<a class="btn btn-mini" 
	href="<?= url::www('admin/company/'.$row['id'].'/store') ?>">
	<i class="icon-map-marker"></i> Stores</a>
<?php if ($permissions->isAllowed('companyroles', 'assign')): ?>
<a class="btn btn-mini" 
	href="<?= url::www('admin/company/'.$row['id'].'/accounts') ?>">
	<i class="icon-user"></i> Accounts</a>
<?php endif; ?>
