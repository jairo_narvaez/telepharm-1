<script type="text/javascript">
	require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/company/')?>">Companies</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/company/'.$company['id'].'/accounts')?>">Accounts Assigned to Company: <?= $company['name']?></a> <span class="divider">/</span></li>
			<li class="active">Search for Accounts to assign to Company: <?= $company['name']?></li>
		</ul>
	</div>
</div>
<hr />
<div class="row">
	<div class="span13">
		<table id="records" style="margin-top: 10px;" class="table table-striped table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>ID <span class="sorting-icon"></span></th>
					<th>Email <span class="sorting-icon"></span></th>
					<th>First Name <span class="sorting-icon"></span></th>
					<th>Last Name <span class="sorting-icon"></span></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row): ?>
				<tr>
					<td>
					<?= $row['id'] ?>
					</td>
					<td>
					<?= $row['email'] ?>
					</td>
					<td>
					<?= $row['first_name'] ?>
					</td>
					<td>
					<?= $row['last_name'] ?>
					</td>
					<td>
					<a href="<?=url::www('admin/company/'.$company['id'].'/accounts/modify/'.$row['id'])?>" class="btn btn-mini" ><i class="icon-edit"></i> Add</a>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
