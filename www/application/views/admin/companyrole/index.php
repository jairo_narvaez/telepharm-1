<script type="text/javascript">
	require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/company/')?>">Companies</a> <span class="divider">/</span></li>
			<li class="active">Accounts assigned to company <strong><?= $company['name']?></strong></li>
		</ul>
	</div>
</div>
<div id="left-button">
	<a href="<?= url::www('admin/company/'.$company['id'].'/accounts/assign')?>" class="btn btn-small btn-primary">Add Account</a>
</div>
<hr />
<div class="row">
	<div class="span13">
		<table id="records" style="margin-top: 10px;" class="table table-striped table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>ID <span class="sorting-icon"></span></th>
					<th>Email <span class="sorting-icon"></span></th>
					<th>First Name <span class="sorting-icon"></span></th>
					<th>Last Name <span class="sorting-icon"></span></th>
					<th>Role <span class="sorting-icon"></span></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row): ?>
				<tr>
					<td>
					<?= $row['account']['id'] ?>
					</td>
					<td>
					<?= $row['account']['email'] ?>
					</td>
					<td>
					<?= $row['account']['first_name'] ?>
					</td>
					<td>
					<?= $row['account']['last_name'] ?>
					</td>
					<td>
					<?= $row['role']['name'] ?>
					</td>
					<td>
					<a href="<?=url::www('admin/company/'.$company['id'].'/accounts/modify/'.$row['account']['id'])?>" class="btn btn-mini" ><i class="icon-edit"></i> Modify</a>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
