<script type="text/javascript">
	require(['scaffold_index']);
</script>
<script>
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    var graph = <?= json_encode($completedScripts);?>;
    function drawChart() {
        var data = google.visualization.arrayToDataTable(graph);
        var options = {
            title: 'Completed Scripts',
            curveType: "function",
            width: 620,
            height: 300,
            hAxis: {
                textStyle:{
                    fontSize: 8
                }
            },
            vAxis: {
                textStyle:{
                    fontSize: 10
                }
            }
        };
        var chart = new google.visualization.LineChart(document.getElementById('dailyScriptsChart'));
        chart.draw(data, options);
    }
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li class="active">Admin Dashboard</li>
		</ul>
	</div>
</div>
<hr />
<div class="row">
    <div class="dashboard-actions">
        <strong>Switch To: </strong>
        <?php if($interval=='week'): ?>
        <a href="<?=URL::www('admin/dashboard/statistic/month');?>"  class="btn btn-primary" id="month-stat">Month</a>
        <?php else: ?>
        <a href="<?=URL::www('admin/dashboard/statistic/week');?>" class="btn btn-primary" id="week-stat">Week</a>
        <?php endif; ?>
    </div>
    <div id="dailyScriptsChart" class="span8"></div>
</div>
