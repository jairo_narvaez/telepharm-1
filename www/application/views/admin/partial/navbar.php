<?php if ($permissions->isAllowed('admin', 'view')): ?>
<div class="navbar">
	<div class="navbar-inner">
		<ul class="nav">
		<a class="brand" href="<?= url::www('admin/dashboard') ?>">Admin Pages:</a>
		<?php if ($permissions->isAllowed('account', 'admin')): ?>
			<li><a href="<?= url::www('admin/account') ?>">Accounts</a></li>
		<?php endif; ?>
		<?php if ($permissions->isAllowed('company', 'admin')): ?>
			<li><a href="<?= url::www('admin/company') ?>">Companies</a></li>
		<?php endif; ?>
		<?php if ($permissions->isAllowed('roles', 'assign')): ?>
			<li><a href="<?= url::www('admin/roles') ?>">Roles</a></li>
		<?php endif; ?>
		<?php if ($permissions->isAllowed('prescription', 'admin')): ?>
			<li><a href="<?= url::www('admin/prescription') ?>">Prescriptions</a></li>
		<?php endif; ?>
		</ul>
	</div>
</div>
<?php endif; ?>
