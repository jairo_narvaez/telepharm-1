<script type="text/javascript">
	require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/roles')?>">Roles</a> <span class="divider">/</span></li>
			<li class="active">Accounts assigned to role <strong><?=$role['name']?></strong></li>
		</ul>
	</div>
</div>
<hr />
<div class="row">
	<div class="span13">
		<table id="records" style="margin-top: 10px;" class="table table-striped table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>ID <span class="sorting-icon"></span></th>
					<th>Email <span class="sorting-icon"></span></th>
					<th>First Name <span class="sorting-icon"></span></th>
					<th>Last Name <span class="sorting-icon"></span></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row): ?>
				<tr>
					<td>
					<?= $row['id'] ?>
					</td>
					<td>
					<?= $row['email'] ?>
					</td>
					<td>
					<?= $row['first_name'] ?>
					</td>
					<td>
					<?= $row['last_name'] ?>
					</td>
					<td>
					<form method="POST" action="<?=url::www('admin/roles/'.$role['id'].'/remove/'.$row['id'])?>">
						<button class="btn btn-mini" ><i class="icon-remove"></i> Remove</button>
					</form>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
