<script type="text/javascript">
    require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=$url?>"><?= Inflector::plural($humanName,2) ?></a> <span class="divider">/</span></li>
			<li class="active"><?=$action?> <?= $humanName ?></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="span13">
		<?php if ($action == 'Delete'): ?>
			<p class="text-error"><strong>Plesae press Save to confirm 
			that you want to delete this record.<br/>
			This action can not be undone.</strong></p>
		<?php endif; ?>
		<?= $form->render() ?>	
	</div>
</div>
