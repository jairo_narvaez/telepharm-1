<script type="text/javascript">
    require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=$url?>"><?= Inflector::plural($humanName,2) ?></a> <span class="divider">/</span></li>
			<li class="active"><?=$action?> <?= $humanName ?></li>
		</ul>
	</div>
</div>
<hr/>
<div class="row">
	<?php if ($form['id']->saneValue != '' && $permissions->isAllowed($model, 'delete')): ?>
	<a class="btn btn-mini btn-danger" style="float: right;" href="<?= $url.'delete/'.$form['id']->saneValue ?>"><i class="icon-remove icon-white"></i> Delete This Record</a>
	<?php endif; ?>
	<div class="span13 admin-form">
		<?= $form->render() ?>	
	</div>
</div>

