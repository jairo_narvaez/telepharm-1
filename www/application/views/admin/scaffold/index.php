<script type="text/javascript">
	require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li class="active"><?=$humanName?></li>
		</ul>
	</div>
</div>
<hr />
<div id="left-button">
	<?php if ($permissions->isAllowed($model, 'create')): ?>
	<a class="btn btn-primary btn-small" href="<?= $url.'create' ?>">New <?= $humanName ?></a>
	<?php endif; ?>
</div>
<div class="row">
	<div class="span13">
		<table id="records" style="margin-top: 10px;" class="table table-striped table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<?php foreach ($columns as $key=>$details): ?>
					<th><?= Text::ucfirst(Inflector::humanize($key)) ?> <span class="sorting-icon"></span></th>
					<?php endforeach; ?>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row): ?>
				<tr>
					<?php foreach ($columns as $key=>$details): ?>
					<td><?= $row[$key] ?></td>
					<?php endforeach ?>
					<td>
						<?php if ($permissions->isAllowed($model, 'edit')): ?>
						<a class="btn btn-mini" href="<?= $url.'edit/'.$row['id'] ?>"><i class="icon-edit"></i></a>
						<?php endif; ?>
						<?= $additionalActions->set('row', $row)->render() ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="<?=count($columns)+1?>"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
