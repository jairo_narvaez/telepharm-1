<?php if ($permissions->isAllowed('storeroles', 'assign')): ?>
<a class="btn btn-mini" 
	href="<?= url::www('admin/store/'.$row['id'].'/accounts') ?>">
	<i class="icon-user"></i> Accounts</a>
<?php endif; ?>

