<script type="text/javascript">
	require(['scaffold_index']);
</script>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/company/')?>">Companies</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/company/'.$store['company_id'].'/store/')?>">Stores</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/store/'.$store['id'].'/accounts/')?>">Assigned Accounts to Stores</a> <span class="divider">/</span></li>
			<li class="active">Modify Roles Assigned to <?=$account['full_name']?> in Store: <?= $store['name']?></li>
		</ul>
	</div>
</div>
<form method="POST">
<div id="left-button">
	<input type="submit" class="btn btn-small btn-primary" value="Save Roles Assigned" />
</div>
<hr />
<div class="row">
	<div class="span13">
		<table id="records" style="margin-top: 10px;" class="table table-striped table-hover table-bordered table-condensed">
			<thead>
				<tr>
					<th>ID <span class="sorting-icon"></span></th>
					<th>Role <span class="sorting-icon"></span></th>
					<th>Add?</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows as $row): ?>
				<tr>
					<td>
					<?= $row['id'] ?>
					</td>
					<td>
					<?= $row['name'] ?>
					</td>
					<td>
					<?= form::checkbox('roles[]',
						$row['id'],
						in_array($row['id'], $current_roles)
					) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
</form>
