<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=$title?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=1020, minimum-scale=0.5, maximum-scale=3.0, user-scalable=yes"/>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--	<link href="--><?//= URL::asset('css/bootstrap.css');?><!--" rel="stylesheet" type="text/css">-->
    <!--	<link href="--><?//= URL::asset('css/bootstrap-lightbox.css');?><!--" rel="stylesheet" type="text/css">-->
    <!--	<link href="--><?//= URL::asset('css/typography.css');?><!--" rel="stylesheet" type="text/css">-->
    <!--	<link href="--><?//= URL::asset('css/reset.css');?><!--" rel="stylesheet" type="text/css">-->
    <!--	<link href="--><?//= URL::asset('css/master.css');?><!--" rel="stylesheet" type="text/css">-->
    <!--    <link href="--><?//= URL::asset('css/global.css');?><!--" rel="stylesheet" type="text/css">-->
    <!--	<link href="--><?//= URL::asset('css/font-awesome.min.css');?><!--" rel="stylesheet" type="text/css">-->
    <link href="<?= URL::css_asset('main-built.css');?>" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        var TP = {};
        TP.urls = {
            www : '<?=url::www()?>',
            assets : '<?=url::asset()?>',
            requireJsBaserUrl: '<?=url::js_asset('./', true)?>'
        };
        TP.TB = {
            api_key: '<?=Kohana::$config->load('opentok.api_key')?>'
        };
        TP.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        TP.serverDate = "<?= date_create('now UTC')->format('Y-m-d H:i:s O') ?>";
        TP.clientDate = new Date();
        TP.urlArgs = "v=" + "<?= $release_time; ?>";
        window.TP = TP;
        var require = {
            baseUrl: TP.urls.requireJsBaserUrl,
            urlArgs: TP.urlArgs,
            paths: {
                app: '../app',
                patient_prescription : '../patient_prescription'
                <?php if (strcmp($env, 'development') == 0): ?>
                ,
                pharmacist_index : '../pharmacist_index',
                prescription_view : '../prescription_view',
                prescription_create : '../prescription_create',
                login_index : '../login_index',
                account_index : '../account_index',
                account_update : '../account_update',
                scaffold_index : '../scaffold_index',
                common : '../common'
                <?php endif; ?>
            }
        };
    </script>
    <?php $openTokServer = 'http://static.opentok.com'; ?>
    <?php if (isset($_SERVER['HTTPS']) && !empty($_SERVER["HTTPS"])): ?>
        <?php $openTokServer = 'https://swww.tokbox.com'; ?>
    <?php endif; ?>
    <?php if (Kohana::$config->load('opentok.use_webrtc')===true): ?>
        <script src="<?=$openTokServer?>/webrtc/v2.0/js/TB.min.js" ></script>
    <?php else: ?>
        <script src="<?=$openTokServer?>/v1.1/js/TB.min.js"></script>
    <?php endif; ?>
    <?php if ($permissions->isAllowed('admin', 'view')): ?>
        <script src="https://www.google.com/jsapi"></script>
    <?php endif;?>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34145166-1']);
        _gaq.push(['_setDomainName', 'telepharm.com']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <script data-main="" src="<?= url::js_asset('require-jquery.js', false) ?>"></script>
</head>

<body>
<?php
$isAdminClass = '';
if ($permissions->isAllowed('admin', 'view')):
    $isAdminClass = 'admin';
endif;
?>
<header class="<?= $isAdminClass ?>"> <!-- Header -->
    <div class="container">
        <?php if ($current_user->account): ?>
            <div class="span4 sidebar">
                <h1><a href="<?=URL::www();?>">Telepharm</a></h1>
                <?php if ($permissions->isAllowed('prescription', 'create') &&
                    strcmp($controller, 'technician') == 0 &&
                    strcmp($action, 'index') == 0 &&
                    strcmp($env, 'production') != 0): ?>
                    <div id="create-prescription-container">
                        <input type="hidden" value="<?= $current_store_rx_num_length ?>" id="current_store_rx_num_length" />
                        <a class="button btn-fill-rx" href="#" id="create-prescription-link">Fill Rx</a>
                        <div class="bottom popover hide"
                             id="create-prescription-popover">
                            <div class="arrow"></div>
                            <div class="popover-inner">
                                <h3 class="popover-title">Create a Prescription</h3>
                                <div class="popover-content">
                                    <div>
                                        <div class="inputs">
                                            <input id="new-prescription-text" maxlength="<?= $current_store_rx_num_length + 3 ?>" />
                                        </div>
                                        <div class="controls">
                                            <button id="cancel-rx-button" class="btn btn-default">Cancel</button>
                                            <button id="create-refill-button" class="btn btn-primary">Create</button>
                                        </div>
                                    </div>
                                    <div id="create-prescription-found">
                                        <div class="hide wrapper">
                                            <table class="results hide">
                                                <tbody data-bind="foreach: createPrescription.found">
                                                <tr>
                                                    <td>
                                                        Rx # <span data-bind="text: fullNumber"></span><br/>
                                                        <span class="status" data-bind="text: rxState"></span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="" data-bind="attr: { class : getClass() }, click: $root.createPrescription.useThis, text : preDraft() ? 'Create refill' : 'Use this' "></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
            <div class="span9 main-content">
	    <?php if ($controller == 'pharmacist' ||
                    $controller == 'technician' ||
                    ($controller == 'prescription' && ($action == 'view' || $action == 'create'))) :?>
                    <?php if (strcmp($controller, 'prescription') == 0 && strcmp($action, 'create') == 0): ?>
                        <input type="hidden" value="<?= $current_store_rx_required_image_number ?>" id="current_store_rx_required_image_number" />
                    <?php endif; ?>
                    <a class="comments" href="#"
                       data-bind="tpPopOver: {container:'comments-popup-box', leftOffset: -172}">
                        <span data-bind="text: $root.notifications.countNotificationsNotProcessed, css: { 'zero': $root.notifications.countNotificationsNotProcessed() == 0 }" class="number zero"></span>
                    </a>
                    <?php if ($permissions->isAllowed('technician', 'dashboard')): ?>
                        <a class="tablets-active in-<?= $controller ?>"
                           style="visibility: hidden;"
                           data-bind="tpPopOver: {container:'synced-tablets-popover',
                                                        leftOffset: -74, topOffset: 10},
                                                        style: { visibility: tabletSync.currentSync().length != 0 ? 'visible' : 'hidden' }">
                            Active
                        </a>
                        <?= View::factory('partials/synced_tablets');?>
                    <?php endif; ?>
                    <?= View::factory('partials/notifications_feed');?>
                <?php endif; ?>
                <?php if ($permissions->isAllowed('prescription', 'search') &&
                    ( ($controller == 'pharmacist' && $action == 'index') ||
                        ($controller == 'technician' && $action == 'index') )
                ): ?>
                    <div class="search-box">
                        <input id="search-text-box" name="search" type="text" class="search" placeholder="Search" data-bind="value: search.query, event: { keyup: search.listenEnterForSearch }" />
                        <input name="btn-search" type="submit" class="btn-search" data-bind="click: search.showAdvancedSearch"/>
                    </div>
                <?php endif ?>
                <ul class="technician-name">
                    <li class="dropdown">
                        <a class="dropdown-toggle" id="dLabel"
                           role="button"
                           data-toggle="dropdown"
                           href="<?= url::www('account/index') ?>">
                            <span class="name"><?= $current_user ?></span>
                            <span class="dropdown-arrow">▽</span>
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <?php foreach($perspectives as $p => $v): ?>
                                <li>
                                    <a href="<?=url::www('account/switch_perspective')?>?new_perspective=<?=$p?>">
                                        <?php if ($p == $current_perspective): ?>
                                            <i class="icon-arrow-right"></i>
                                        <?php endif ?>
                                        <?=$p?></a>
                                </li>
                            <?php endforeach; ?>
                            <li><a href="<?=url::www('account/index')?>">Profile</a></li>
			    <li><a href="<?= URL::asset('update.html');?>">Updates</a></li>
                            <li><a href="<?=url::www('account/logout')?>">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($permissions->isAllowed('admin', 'view')): ?>
        <?= View::factory('admin/partial/navbar'); ?>
    <?php endif; ?>
</header> <!-- End Header -->
<section class="container" id="loading-container"  <?= isset($is_ko_page) ? 'style="display: block;"' : 'style="display: none;"' ?>>
</section>
<section class="container <?= $isAdminClass ?>" id="main-container"  <?= isset($is_ko_page) ? 'style="display: none;"' : '' ?>> <!-- Container -->
    <div class="row">
        <div class="span13">
            <?= Telepharm_Flash::instance()->html() ?>
        </div>
    </div>
    <?= $content; ?>
</section> <!-- End Container -->
<?= view::factory('partials/account_data'); ?>
<?= View::factory('partials/notifications_modal');?>
<div class="modal hide fade" id="modal-template" data-bind="with: modal">
    <div class="modal-header">
        <button type="button" class="close" data-bind="click: close" aria-hidden="true">&times;</button>
        <h3 data-bind="text: title"></h3>
    </div>
    <div class="modal-body" data-bind="html: content">
    </div>
    <div class="modal-footer">
        <a class="btn" data-bind="click: close, text: closeTitle, visible: closeTitle() != ''">Close</a>
        <a class="btn btn-primary" data-bind="click: ok, text: okTitle">Save changes</a>
    </div>
</div>
<?php if ($permissions->isAllowed('prescription', 'search') &&
    ( ($controller == 'pharmacist' && $action == 'index') ||
        ($controller == 'technician' && $action == 'index') )
): ?>
    <?= view::factory('partials/advanced_search') ?>
<?php endif; ?>
<?= view::factory('partials/intervals')->set('intervals', $intervals); ?>

<script type="text/javascript">
    var __lc = {};
    __lc.license = 2246901;

    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>

</body>
</html>
