<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?=$title?></title>
<meta name="description" content="">
<meta name="viewport" content="width=1020, minimum-scale=0.5, maximum-scale=3.0, user-scalable=yes"/>
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--	<link href="--><?//= URL::asset('css/bootstrap.css');?><!--" rel="stylesheet" type="text/css">-->
<!--	<link href="--><?//= URL::asset('css/bootstrap-lightbox.css');?><!--" rel="stylesheet" type="text/css">-->
<!--	<link href="--><?//= URL::asset('css/typography.css');?><!--" rel="stylesheet" type="text/css">-->
<!--	<link href="--><?//= URL::asset('css/reset.css');?><!--" rel="stylesheet" type="text/css">-->
<!--	<link href="--><?//= URL::asset('css/master.css');?><!--" rel="stylesheet" type="text/css">-->
<!--    <link href="--><?//= URL::asset('css/global.css');?><!--" rel="stylesheet" type="text/css">-->
<!--	<link href="--><?//= URL::asset('css/font-awesome.min.css');?><!--" rel="stylesheet" type="text/css">-->
    <link href="<?= URL::css_asset('main-built.css');?>" rel="stylesheet" type="text/css">
	<script type="text/javascript">
	var TP = {};
	TP.urls = {
		www : '<?=url::www()?>',
		assets : '<?=url::asset()?>',
		requireJsBaserUrl: '<?=url::js_asset('./', true)?>'
	};
	TP.TB = {
		api_key: '<?=Kohana::$config->load('opentok.api_key')?>'
	};
	TP.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	TP.serverDate = "<?= date_create('now UTC')->format('Y-m-d H:i:s O') ?>";
	TP.clientDate = new Date();
	TP.urlArgs = "v=" + "<?= Kohana::$config->load('application.serve_js_with_release_version') ? strtotime(Kohana::$config->load('release.build')) : time(); ?>";
	window.TP = TP;
        var require = {
            baseUrl: TP.urls.requireJsBaserUrl,
            urlArgs: TP.urlArgs,
            paths: {
                app: '../app',
                <?php if (strcmp(Kohana::$config->load('release.env'), 'development') == 0): ?>
                pharmacist_index : '../pharmacist_index',
                prescription_view : '../prescription_view',
                prescription_create : '../prescription_create',
                login_index : '../login_index',
                account_index : '../account_index',
                account_update : '../account_update',
                common : '../common'
                <?php endif; ?>
            }
        };
	</script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-34145166-1']);
        _gaq.push(['_setDomainName', 'telepharm.com']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <script data-main="" src="<?= url::js_asset('require-jquery.js', false) ?>"></script>
</head>

<body>
	<header> <!-- Header -->
		<div class="container">
			<div class="span4 sidebar">

			</div>
			<div class="span9 main-content">
		</div>
	</header> <!-- End Header -->
	<section class="container" id="loading-container"  <?= isset($is_ko_page) ? 'style="display: block;"' : 'style="display: none;"' ?>>
	</section>
	<section class="container" id="main-container"  <?= isset($is_ko_page) ? 'style="display: none;"' : '' ?>> <!-- Container -->
		<div class="row">
			<div class="span13">
			<?= Telepharm_Flash::instance()->html() ?>
			</div>
		</div>
		<?= $content; ?>
	</section> <!-- End Container -->
	<footer class="extensive">
		<div class="container">
			<p class="small muted">&copy; Telepharm <?= date('Y') ?></p>
			<p class="footer-text">Built in Iowa City</p>
		</div>
	</footer>
    <div class="modal hide fade" id="modal-template" data-bind="with: modal">
    	<div class="modal-header">
    		<button type="button" class="close" data-bind="click: close" aria-hidden="true">&times;</button>
    		<h3 data-bind="text: title"></h3>
    	</div>
    	<div class="modal-body" data-bind="html: content">
    	</div>
    	<div class="modal-footer">
    		<a class="btn" data-bind="click: close, text: closeTitle, visible: closeTitle() != ''">Close</a>
    		<a class="btn btn-primary" data-bind="click: ok, text: okTitle">Save changes</a>
    	</div>
    </div>

	<script type="text/javascript">
	var __lc = {};
	__lc.license = 2246901;

	(function() {
		var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
		lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
	})();
	</script>

</body>
</html>
