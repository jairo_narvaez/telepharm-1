<?php if (count($flash_messages)): ?>
<?php foreach($flash_messages as $k => $v): ?>
<div class="alert alert-<?=$k?>" style="margin-top: 10px;">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<ul>
		<?php foreach($v as $message): ?>
		<li><?=$message?></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endforeach; ?>
<?php endif;?>
