<form action="<?= $form->action ?>" 
	method="post" 
	enctype="multipart/form-data" 
	class="form-horizontal">
	<fieldset>
		<legend>Account Information</legend>
		<div style="display: none;">
			<?= $form['id']->render() ?>
		</div>
		<?= $form['username']->render() ?>
		<?= $form['first_name']->render() ?>
		<?= $form['last_name']->render() ?>
		<?php if ($attributes['show_company_select']) echo $form['company_id']->render() ?>
		<?= $form['timezone']->render() ?>
		<?= $form['email']->render() ?>
		<?= $form['password']->render(['autocomplete' => 'off']) ?>
		<?= $form['confirm_password']->render(['autocomplete' => 'off']) ?>
	</fieldset>

	<div class="form-actions">
		<input type="submit" class="btn btn-primary" value="Save" />
	</div>
</form>

