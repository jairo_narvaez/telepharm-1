<form action="<?= $form->action ?>" 
	method="post" 
	enctype="multipart/form-data" 
	class="form-horizontal">
	<fieldset>
		<legend>Company Information</legend>
		<div style="display: none;">
			<?= $form['id']->render() ?>
		</div>
		<?= $form['name']->render() ?>
		<?= $form['description']->render() ?>
		<?= $form['phone']->render(['placeholder'=>'Area code first']) ?>
		<?= $form['size']->render(['placeholder'=>'15']) ?>
	</fieldset>

	<?= $form->location->render() ?>

	<div class="form-actions">
		<input type="submit" class="btn btn-primary" value="Save" />
	</div>
</form>
