<fieldset>
	<legend>Location Information</legend>
	<?= $form['location_name']->render() ?>
	<?= $form['address1']->render() ?>
	<?= $form['address2']->render() ?>
	<?= $form['zip']->render() ?>
	<?= $form['city']->render() ?>
	<?= $form['state']->render() ?>
</fieldset>
