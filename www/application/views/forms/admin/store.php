<form action="<?= $form->action ?>" 
	method="post" 
	enctype="multipart/form-data" 
	class="form-horizontal">
	<fieldset>
		<legend>Store Information</legend>
		<?= $form['company']->render(['readonly' => 'true']) ?>
		<div style="display: none;">
			<?= $form['id']->render() ?>
			<?= $form['company_id']->render() ?>
		</div>
		<?= $form['name']->render() ?>
		<?= $form['description']->render() ?>
		<?= $form['contact']->render() ?>
		<?= $form['phone']->render(['placeholder'=>'Area code first']) ?>
		<?= $form['pms_query_class']->render() ?>
		<?= $form['url']->render(['placeholder'=>'The PMS url to query the Prescriptions']) ?>
		<?= $form['server_ip']->render(['placeholder'=>'The PMS IP']) ?>
        <?= $form['rx_num_length']->render(['placeholder'=>'Rx Number Length']) ?>
        <?= $form['rx_required_image_number']->render(['placeholder'=>'Rx Required Image Number']) ?>
	</fieldset>

	<?= $form->location->render() ?>

	<div class="form-actions">
		<input type="submit" class="btn btn-primary" value="Save" />
	</div>
</form>

