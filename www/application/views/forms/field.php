<?php $hasErrors = count($field->errors) > 0; ?>
<div class="control-group <?= $hasErrors ? 'error' : ''?>">
	<label class="control-label" for="<?=$field->id?>"><?=$field->label?></label>
	<div class="controls">
		<?= $html ?>
		<?php if ($field->errors): ?>
			<span class="help-inline">
			<?php foreach ($field->errors as $error): ?>
				<?php if (isset($attributes[':errors'][$error->code])): ?>
				<?= $attributes[':errors'][$error->code] ?>
				<?php else: ?>
				<?= $error ?>
				<?php endif ?>
			<?php endforeach ?>
			</span>
		<?php endif ?>
	</div>
</div>
