<?php $caption = isset($attributes[':caption']) ? $attributes[':caption'] : $field->caption ?>
<input
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="hidden"
	value="0"
/>
<label class="checkbox">
<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="checkbox"
	value="1"
<?php if ($field->value != 0): ?>
	checked="checked"
<?php endif ?>
/>
<?= TelePharm_TaintedData::clean($caption) ?>
</label>
