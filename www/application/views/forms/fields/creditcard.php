<?php if ($field->hideValue): ?>
<?php $tmp = mt_rand() ?>
<span id="cc-<?=$tmp?>-l4">
**** <?= substr($field->value, -4) ?>
<a href="#" onclick="$('#cc-<?=$tmp?>-l4').hide(); $('#cc-<?=$tmp?>-full').fadeIn(); $('#<?= htmlspecialchars($field->fullyQualifiedId)?>').val(''); return false;" style="margin-left: 1em;">Change</a>
</span>
<span id="cc-<?=$tmp?>-full" style="display: none">
<?php endif ?>

<input
	id="<?= htmlspecialchars($field->fullyQualifiedId)?>"
	name="<?= htmlspecialchars($field->transformedName)?>"
	type="text"
	autocomplete="off"
	value="<?php if (!$field->hideValue): ?><?= htmlspecialchars($field->value) ?><?php else: ?>x<?php endif ?>"
	<?php $field->renderHTMLAttributes(); ?>
/>

<?php if ($field->value): ?>
</span>
<?php endif ?>