<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="file"
<?php $field->renderHTMLAttributes() ?>
/>