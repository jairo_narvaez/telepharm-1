<table id="<?= htmlspecialchars($field->fullyQualifiedId) ?>">
<?php foreach($field->fields as $f): ?>
	<tr>
		<td><?= isset($attributes[':labels'][$f->id]) ? $attributes[':labels'][$f->id] : ucwords(str_replace('_', ' ', $f->id)) ?></td>
		<td><?= $f->html ?></td>
	</tr>
<?php endforeach ?>
</table>