<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="password"
<?php $field->renderHTMLAttributes() ?>
/>