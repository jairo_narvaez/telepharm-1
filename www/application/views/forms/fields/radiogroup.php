<?php $fid = $field->fullyQualifiedId; $i = 0; $lb = isset($attributes[':lineBreaks']) ? $attributes[':lineBreaks'] : 0; ?>
<?php foreach ($field->options as $value => $caption): ?>
<?php $id = htmlspecialchars($fid.'_'.mt_rand()) ?>

<input
	id="<?= $id ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
	type="radio"
	value="<?= htmlspecialchars($value) ?>"	
	<?php if ($field->value == $value): ?> checked="checked"<?php endif ?>
/>

<?php if ($caption): ?>
<label for="<?= $id ?>"><?= htmlspecialchars($caption) ?></label>
<?php endif ?>

<?php if ($lb && !(++$i % $lb)): ?><br /><?php endif ?>

<?php endforeach ?>