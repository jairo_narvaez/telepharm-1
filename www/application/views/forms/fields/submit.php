
<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
	class="btn btn-primary"
	type="submit"
<?php if ($field->caption !== null): ?>
	value="<?= htmlspecialchars($field->caption) ?>"
<?php endif ?>
<?php $field->renderHTMLAttributes() ?>

/>
