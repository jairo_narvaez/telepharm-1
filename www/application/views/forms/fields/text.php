<input
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName)?>"
	type="text"
	value="<?= htmlspecialchars($field->value) ?>"
	<?php $field->renderHTMLAttributes(); ?>
/>