<textarea
	id="<?= htmlspecialchars($field->fullyQualifiedId) ?>"
	name="<?= htmlspecialchars($field->transformedName) ?>"
<?php $field->renderHTMLAttributes() ?>
><?= htmlspecialchars($field->value) ?></textarea>