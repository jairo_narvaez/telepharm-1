<form action="<?= $form->action ?>" class="form-horizontal" method="post" enctype="multipart/form-data">

	<?php $form->renderRules() ?>

	<?php if ($form->errors): ?>
	<ul class="error">
	<?php foreach ($form->errors as $error): ?>
		<li><?= htmlspecialchars($error) ?>.</li>
	<?php endforeach ?>
	</ul>
	<?php endif ?>

	<?php foreach($form->fields as $field): ?>
		<?= $field->render(['placeholder' => $field->label]) ?>
	<?php endforeach ?>

</form>

