
<form action="<?= $form->action ?>" method="post" enctype="multipart/form-data">

<?php $form->renderRules() ?>

<?php if ($form->errors): ?>
<ul class="error">
<?php foreach ($form->errors as $error): ?>
	<li><?= htmlspecialchars($error) ?>.</li>
<?php endforeach ?>
</ul>
<?php endif ?>

<table>
	<tr>
		<td>Username</td>
		<td><?php $form['username']->render(['placeholder' => 'Username']) ?>
	</tr>
	<tr>
		<td>Password</td>
		<td><?php $form['password']->render(['placeholder' => 'Password']) ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php $form['remember_me']->render() ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php $form['action']->render() ?></td>
	</tr>
</table>

</form>
