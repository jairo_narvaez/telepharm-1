<p>

<p>A password reset has been requested. To reset your password go to <a href="<?= URL::www($reset_url);?>"><?= URL::www($reset_url);?></a> . This url is only valid for 24 hours.</p>

<p>If you did not request this reset, please ignore this email, or contact support.</p>

</p>
