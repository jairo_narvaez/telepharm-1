<div class="row">
	<div class="span6 offset3">
		<div class="row logo">
			<img src="<?= URL::asset("img/telepharm-logo-login.png")?>" alt="Telepharm" />
		</div>
		<div class="row login">
			<div class="span6 well">
				<h5>Reset Your Password</h5>
				<p>
					Please enter the email address associated with your <?=$site_name?> account.
				</p>
				<?= $form->render([':template' => 'form']) ?>
				<p><a href="<?= url::www('login') ?>">Cancel</a></p>
			</div>
		</div>
	</div>
</div>

