<div class="row">
	<div class="span6 offset3">
		<div class="row logo">
			<img src="<?= URL::asset("img/telepharm-logo-login.png")?>" alt="Telepharm" />
		</div>
		<div class="row login">
			<div class="span6 well">
				<h5>Thank You</h5>
				<p>
					We have received your password reset request. An email has been sent to your email address to complete the reset process.
				</p>
				<p><a class="back-to-account" href="<?= url::www('login') ?>">Back to account log in</a></p>
			</div>
		</div>
	</div>
</div>



