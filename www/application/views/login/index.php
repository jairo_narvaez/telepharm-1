<div class="row box-login">
	<div class="span6 offset3">
		<div class="row logo">
			<img src="<?= URL::asset("img/telepharm-logo-login.png")?>" alt="Telepharm" />
		</div>
		<div class="row login">
			<div class="span6 well">
				<h5>Log Back In</h5>
				<?= $form->render([':template' => 'form']) ?>
				<p><a class="forgot_password" href="<?= url::www('login/forgot_password') ?>">Forgot your password?</a></p>
			</div>
		</div>
	</div>
</div>
