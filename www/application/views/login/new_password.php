<div class="row">
    <div class="span6 offset3">
        <div class="row logo">
            <img src="<?= URL::asset("img/telepharm-logo-login.png") ?>" alt="Telepharm"/>
        </div>
        <div class="row login">
            <div class="span6 well">
                <h5>Password Reset Request</h5>
                <p>
                    Please enter your new password.
                </p>

                <?= $form->render() ?>

                <a href="/login">Back to Login</a>
            </div>
        </div>
    </div>
</div>
