<div class="account-form">
	<h1>Password Reset Request</h1>

	We're sorry but the url you used to request a password reset is not valid.

	<a href="/login">Back to Login</a>
</div>