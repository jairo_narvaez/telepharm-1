<?php if ($current_user->account) :?>
<?php
	if ($permissions->isAllowed('pharmacist', 'dashboard'))
		$role = 'Pharmacist';
	elseif ($permissions->isAllowed('technician', 'dashboard'))
		$role = 'Technician';
	elseif ($permissions->isAllowed('admin', 'view'))
		$role = 'Admin';
	else
		$role = 'Guest';
?>
<textarea id="account-data" style="display: none;"><?=
json_encode(
	array_merge(
		$current_user->account->publicArray(),
		[
			'role' => $role,
		]
	)
)?></textarea>
<?php endif; ?>
