<div class="modal hide fade" id="advanced-search">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2>Advanced Search</h2>
	</div>
	<div class="modal-body">
		<form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="store">Store:</label>
				<div class="controls">
					<select id="store" data-bind="value: search.advancedSearch.store">
						<option value="all">---All Stores---</option>
						<?php foreach($current_user->stores as $store): ?>
						<option value="<?=$store->id?>"><?= $store->name ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="status">Status:</label>
				<div class="controls">
					<select id="status" data-bind="value: search.advancedSearch.status">
						<option value="all">---All Statuses---</option>
						<option value="approval">Awaiting Approval</option>
						<option value="on-hold">On Hold</option>
						<option value="counsel">Awaiting Counsel</option>
						<option value="completed">Completed</option>
						<option value="cancelled">Cancelled</option>
						<option value="archived">Archived</option>
						<option value="error">Reported With Error</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="patientName">Patient Name:</label>
				<div class="controls">
					<input type="text" data-bind="value: search.advancedSearch.name" id="patientName" placeholder="Patient Name" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="rxNumber">Rx Number:</label>
				<div class="controls">
					<input type="text" data-bind="value: search.advancedSearch.rxNumber" id="rxNumber" placeholder="Rx Number" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="fromDate">From Date:</label>
				<div class="controls">
					<input type="text" data-bind="value: search.advancedSearch.fromDate" id="fromDate" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="toDate">To Date:</label>
				<div class="controls">
					<input type="text" data-bind="value: search.advancedSearch.toDate" id="toDate" />
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-bind="click: search.performAdvancedSearch">Search</a>
	</div>
</div>
