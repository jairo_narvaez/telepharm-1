<div class="modal hide fade" id="approve-modal" style="width: 700px">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Confirm</h2>
    </div>
    <div class="modal-body">
        <p>Are you sure you want to approve this prescription?</p>
    </div>
    <div class="modal-footer clearfix">
        <a class="btn btn-danger left-button" data-dismiss="modal">No</a>
        <a class="btn btn-primary right-button" data-bind="click: prescription.approve">Yes to Next Prescription</a>
        <a class="btn right-button dashboard" data-bind="click: prescription.approve">Yes to Dashboard</a>
    </div>
</div>
