<?php if ($type == 'Technician'): ?>
<h3 class="prelative">Telepharmacies</h3>
<!-- ko if: videoConf.currentCall() != null -->
<p class="calling">Waiting answer from <span data-bind="text: videoConf.callee"></span></p>
<!-- /ko -->
<div class="available-persons">
	<ul data-bind="foreach: availableUsers.stores">
		<li class="technician-pharmacists">
			<p data-bind="text: 'Store: ' + name()">Loading...</p>
			<a href="#" data-bind="click: function() { $root.videoConf.requestVideoConf($data) }"><span class="icon-video"></span></a>
		</li>
	</ul>
</div>
<?php else: ?>
<h3 class="prelative">Tele<?=inflector::plural(strtolower($type), 2)?><div style="float: right; width: 90px; text-align: right; margin-right: 2px;"><a style="color: #2882ce; font-family: Helvetica; font-size: 11px; text-decoration: bold; text-shadow: none;" id="video-call-next-available" data-bind="visible: $root.availableUsers.users().length > 0, click: function() { $root.videoConf.showVideoConfOptions($(event.srcElement).parents('.sidebar').find('h3:first'), null, 1) }" href="#">Call next RPh</a></div></h3>
<!-- ko if: videoConf.currentCall() != null -->
<p class="calling">Waiting answer from <span data-bind="text: videoConf.callee"></span></p>
<!-- /ko -->
<div class="available-persons">
	<ul data-bind="foreach: availableUsers.users">
		<li class="technician-pharmacists">
			<p data-bind="text: name">Loading...</p>
			<?php /* <a href="#" data-bind="click: function() { $root.videoConf.requestVideoConf($data) }"><span class="icon-video"></span></a> */ ?>
			<a href="#" data-bind="click: function() { var el = $root.videoConf.showVideoConfOptions($(event.srcElement).parents('.sidebar').find('h3:first'), $data, 0) }"><span class="icon-video"></span></a>
		</li>
	</ul>
</div>
<?php endif; ?>
