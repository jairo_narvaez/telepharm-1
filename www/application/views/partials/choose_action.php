<div class="modal hide fade" id="choose-modal" style="width: 700px">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2>What would you like to do next?</h2>
	</div>
	<div class="modal-body">
		<p>Press the back arrow <i class="icon-arrow-left"></i> key to return to dashboard, or the forward arrow <i class="icon-arrow-right"></i> key to view next prescription.</p>
	</div>
	<div class="modal-footer" style="text-align: center">
		<a href="#" data-bind="click: $root.chooser.processReturn" class="btn btn-default"><i class="icon-arrow-left"></i> Return to Dashboard</a>
		<a href="#" data-bind="click: $root.chooser.processNext" class="btn btn-primary">View Next Prescription <i class="icon-arrow-right"></i></a>
	</div>
</div>
