<a href="#"
		data-bind="visible: prescription.reportedError(), click: prescription.viewError"
		style="margin-top: 30px; display:block;">
		View Error</a>
<a href="#" data-bind="visible: prescription.canReportError(), click: prescription.reportError"
		style="margin-top: 30px; display:block; color: red;">
		Report Error</a>
<a href="#" data-bind="click: print, visible : !prescription.isTransfer()" class="print"><img src="<?= URL::asset('img/icon-print-rx.png');?>" alt="Print Prescription" /> Print</a>
