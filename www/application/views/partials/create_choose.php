<div class="modal hide fade" id="create-choose-modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2>What would you like to do next?</h2>
	</div>
	<div class="modal-body">
		<p>
			Press the back arrow <i class="icon-arrow-left"></i>
			key to return to dashboard,
			or the forward arrow <i class="icon-arrow-right"></i>
			key to view the submitted prescription.
		</p>
		<hr/>
		<p>
			Or input the Rx Number in the following form
			to fill a new prescription.
		</p>
		<br/>
            <div class="hide" id="create-prescription-container-errors">
                <div class="header">
                    <p>There was a problem with the Rx number submitted:</p>
                </div>
                <div class="content">

                </div>
            </div>
        <div id="create-prescription-container">
            <input type="hidden" value="<?= $current_store_rx_num_length ?>" id="current_store_rx_num_length" />
            <div style="text-align: center">
                <div>
                    <div class="inputs" style="float: left; margin: 3px 0 0 120px">
                        <input id="new-prescription-text" maxlength="<?= $current_store_rx_num_length + 3 ?>" />
                    </div>
                    <div class="controls" style="margin: 0 115px 5px 0">
                        <button id="create-refill-button" class="btn btn-success">Fill Rx</button>
                    </div>
                </div>
                <div id="create-prescription-found">
                    <div class="hide wrapper">
                        <table class="results hide" style="text-align: center; width: 50%;">
                            <tbody data-bind="foreach: $root.createChooser.found" style="text-align: center">
                            <tr>
                                <td>
                                    Rx # <span data-bind="text: fullNumber"></span><br/>
                                    <span class="status" data-bind="text: rxState"></span>
                                </td>
                                <td>
                                    <a href="#" class="" data-bind="attr: { class : getClass() }, click: $root.createChooser.useThis, text : preDraft() ? 'Create refill' : 'Use this' "></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="modal-footer" style="text-align: center">
		<a href="#" data-bind="click: $root.createChooser.processReturn"
			class="btn btn-default"><i class="icon-arrow-left"></i> Return to Dashboard</a>
		<a href="#" data-bind="click: $root.createChooser.processNext"
			class="btn btn-primary">View Submitted Prescription <i class="icon-arrow-right"></i></a>
	</div>
</div>

