<p><strong>Quantity Written:</strong> <span data-bind="text: prescriptionDispense.qtyWritten"></span></p>
<p><strong>Quantity Dispensed:</strong> <span data-bind="text: prescriptionDispense.qtyDispensed"></span</p>
<p><strong>Written:</strong> <span data-bind="text: prescription.written"></span></p>
<p><strong>Expires:</strong> <span data-bind="text: prescription.expires"></span></p>
<p><strong>Filled:</strong> <span data-bind="text: prescription.filled"></span></p>
