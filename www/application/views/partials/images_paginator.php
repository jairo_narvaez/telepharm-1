<div class="images-paginator" data-bind="visible: prescription.uploadedImages().length >= 3">
	<a class="previous" data-bind="style: {'visibility' : prescription.canGoPrevious() ? '' : 'hidden'}, 
		click: prescription.goPrevious">
		<i class="icon-chevron-left icon-white"></i></a>
	<div class="upload">
		<?php if ($upload_link): ?>
		<a data-bind="click: wizard.show">
			<i class="icon-arrow-up"></i>
			Upload Additional Photos</a>
		<?php endif; ?>
	</div>
	<a class="next" data-bind="style: {'visibility' : prescription.canGoNext() ? '' : 'hidden'},
		click: prescription.goNext">
		<i class="icon-chevron-right icon-white"></i></a>
</div>
