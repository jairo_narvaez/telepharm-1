<?php if(count($medispan_images)): ?>
<div class="modal hide fade" id="medispan-full">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2>Medi-Span Drug Image</h2>
	</div>
	<div class="modal-body">
		<div id="medispan-carousel" class="carousel slide">
			<!-- Carousel items -->
			<div class="carousel-inner">
				<?php foreach($medispan_images as $img): ?>
				<div class="active item">
					<img src="<?=$img?>" data-medispan="true"/>
				</div>
				<?php endforeach; ?>
			</div>
			<!-- Carousel nav -->
			<?php if (count($medispan_images) > 1) :?>
			<a class="carousel-control left" href="#medispan-carousel" data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#medispan-carousel" data-slide="next">&rsaquo;</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
	</div>
</div>
<?php endif; ?>
