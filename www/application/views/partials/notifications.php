<!-- ko if: notifications.growlNotifications().length > 0 -->
<!-- ko foreach: notifications.growlNotifications -->
<div style="cursor: pointer" class="notification-display alert fade" data-bind="css: {'in': show}">
	<a class="close" href="#" data-bind="click: function() { $data.dismiss();  $root.notifications.dismissNotification($data) }">x</a>
	<p data-bind="text: message"></p>
	<a href="#" data-bind="text: actionMessage, click: execute"></a>
</div>
<!-- /ko -->
<!-- /ko -->
