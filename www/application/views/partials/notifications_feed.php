<div id="comments-popup-box" class="popover fade bottom in">
	<div class="arrow"></div>
	<div class="popover-inner">
		<ul> 
			<!-- ko foreach: notifications.recentNotProcessed -->
				<li data-bind="css: className, click: execute">
					<!-- ko if: typeName() == 'Comment' -->
					<p><strong data-bind="text: typeName"></strong>:</p>
					<p><span data-bind="text: message"></span></p>
					<!-- /ko -->
					<!-- ko if: typeName() == 'Notification' -->
					<p><strong data-bind="text: typeName">:</strong>
					   <span class="bold" data-bind="text: prescriptionName"></span>
					   <span data-bind="text: customMessage"></span>
					</p>
					<!-- /ko -->
				</li>
			<!-- /ko -->
		</ul>
		<div class="see-all-notifications">
			<a href="#" data-bind="click: notifications.showNotificationsModal">See all notifications</a>
		</div> 
	</div>
</div>
