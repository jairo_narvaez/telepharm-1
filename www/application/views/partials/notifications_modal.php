<div class="modal hide fade all-notifications"> <!-- Modal All Notifications and Comments -->
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>
		<h5>Notifications</h5>    		
	</div>
	<div class="modal-body">
		<div class="tabbable tabs-left">
	    	<ul class="nav nav-tabs">
		    	<li class="active"><a href="#tab1" data-toggle="tab">All</a></li>
		    	<li><a href="#tab2" data-toggle="tab">Comments</a></li>
		    	<li><a href="#tab3" data-toggle="tab">Notifications</a></li>
		    </ul>
		    <div class="tab-content">
			    <div class="tab-pane active cursor-pointer" id="tab1" data-bind="foreach: notifications.allNotifications">
					<div data-bind="css: className, click: execute">
						<!-- ko if: typeName() == 'Comment' -->
						<p><strong data-bind="text: typeName"></strong>:</p>
						<p><span data-bind="text: message"></span></p>
						<!-- /ko -->
						<!-- ko if: typeName() == 'Notification' -->
						<p><strong data-bind="text: typeName"></strong>:</p>
						<p>
							<span data-bind="text: message"></span> 
						</p>
						<!-- /ko -->
					</div>
				</div>
				<div class="tab-pane cursor-pointer" id="tab2" data-bind="foreach: notifications.allNotificationsOfTypeComments">
					<div data-bind="css: className, click: execute">
						<p><strong data-bind="text: typeName"></strong></p>
						<p><span data-bind="text: message"></span></p>
					</div>
				</div>
				<div class="tab-pane cursor-pointer" id="tab3" data-bind="foreach: notifications.allNotificationsOfTypeNotifications">
					<div data-bind="css: className, click: execute">
						<p><strong data-bind="text: typeName"></strong>
						   <span data-bind="text: message"></span> 
						</p>
					</div>
				</div>
			</div>
		</div>
    </div>
</div> <!-- End Modal All Notifications and Comments  -->
