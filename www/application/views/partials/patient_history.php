<div id="patient-history-modal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2>Patient History</h2>
	</div>
	<div class="modal-body">
		<div class="patient-wrapper">
            <div class="center">
                <div class="left">
                    <div class="patient-info">
                        <label>Patient Name:</label> <span data-bind="text: patientHistory.patientName">William Johnson</span>
                        <label>DOB:</label> <span data-bind="text: patientHistory.dob">5/16/59</span>
                    </div>
                </div>
                <div class="right">
                    <div class="patient-info">
                        <label>Prescriber Name:</label> <span data-bind="text: patientHistory.prescriber">Dr. Jay Williams</span>
                        <label>Number:</label> <span data-bind="text: patientHistory.prescriberPhone">(319) 458 3696</span>
                    </div>
                </div>
            </div>
		</div>
		<div class="patient-history-results" data-bind="visible: patientHistory.prescriptions().length > 0">
			<table>
				<thead>
					<tr>
						<th>Date</th>
						<th>Rx Number</th>
						<th>Drug</th>
						<th>Qty Written</th>
						<th>Qty Disp.</th>
						<th>Last Fill</th>
						<th>Expires</th>
                        <th colspan="2" style="width:100px"></th>
					</tr>
				</thead>
				<tbody class="body-scroller" data-bind="">
                <!-- ko foreach: patientHistory.prescriptions -->
                <!-- ko if : firstLoadItem() -->
                <tr id="patient-history-load-first-prescription" class="extra">
                    <td colspan="8"></td>
                </tr>
                <!-- /ko -->
					<tr>
						<td data-bind="text: dateWritten"></td>
						<td data-bind="text: number"></td>
						<td data-bind="text: drug.description"></td>
						<td data-bind="text: dispense.qtyWritten()"></td>
						<td data-bind="text: dispense.qtyDispensed()"></td>
						<td data-bind="text: dateFilled"></td>
						<td data-bind="text: dateExpires"></td>
                        <td data-bind="click : showPrescription">
                            <a href="#" class="btn btn-mini btn-default">View Rx</a>
                        </td>
					</tr>
                <!-- /ko -->
                <!-- ko if : patientHistory.lastRx() -->
                <tr class="patient-history-last-set">
                    <td data-bind="text: patientHistory.lastRx().dateWritten()"></td>
                    <td data-bind="text: patientHistory.lastRx().number"></td>
                    <td data-bind="text: patientHistory.lastRx().drug.description"></td>
                    <td data-bind="text: patientHistory.lastRx().dispense.qtyWritten()"></td>
                    <td data-bind="text: patientHistory.lastRx().dispense.qtyDispensed()"></td>
                    <td data-bind="text: patientHistory.lastRx().dateFilled"></td>
                    <td data-bind="text: patientHistory.lastRx().dateExpires"></td>
                    <td data-bind="click : patientHistory.lastRx().showPrescription">
                        <a href="#" class="btn btn-mini btn-default">View Rx</a>
                    </td>
                </tr>
                <!-- /ko -->
				</tbody>
			</table>
            <div id="patient-history-actions" class="patient-history-actions final">
                <div class="extra">
                    <span data-bind="visible : patientHistory.showNoMoreResults">No more results</span>
                </div>
            </div>
        </div>
	</div>
	<div class="modal-footer">
        <div class="right">
            <a href="#" class="btn btn-default" data-bind="click : patientHistory.hide">Close</a>
        </div>
	</div>
</div>
