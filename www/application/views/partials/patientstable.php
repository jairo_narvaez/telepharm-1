<article class="content">
	<ul class="approval-status">
		<?php if ($permissions->isAllowed('technician', 'dashboard')): ?>
		<li class="rejected" data-bind="click: function() { queue.filterPrescriptionBy('rejected') }">
			<span data-bind="text: queue.rejectedCount">0</span>Rejected Rx</li>
		<li class="approval" data-bind="click: function() { queue.filterPrescriptionBy('approved') }">
			<span data-bind="text: queue.awaitingApprovalCount">0</span>Awaiting Approval</li>
		<li class="counsel" data-bind="click: function() { queue.filterPrescriptionBy('counsel')  }">
                        <span data-bind="text: queue.awaitingCounselCount" >0</span>Awaiting Counsel</li>
		<li class="draft" data-bind="click: function() { queue.filterPrescriptionBy('draft')  }">
			<span data-bind="text: queue.draftCount" >0</span>Draft</li>
		<?php endif;?>
		<?php if ($permissions->isAllowed('pharmacist', 'dashboard')): ?>
		<li class="approval" data-bind="click: function() { queue.filterPrescriptionBy('approved') }">
			<span data-bind="text: queue.awaitingApprovalCount">0</span>Awaiting Approval</li>
		<li class="rejected" data-bind="click: function() { queue.filterPrescriptionBy('rejected') }">
			<span data-bind="text: queue.rejectedCount">0</span>Rejected Rx</li>
        <li class="transfer" data-bind="click: function() { queue.filterPrescriptionBy('transfer') }">
            <span data-bind="text: queue.transferCount">0</span>Transfer RX</li>
		<?php endif;?>
	</ul>
	<div id="status-details-div">
		<table class="status-details">
			<thead>
				<tr>
					<th class="first-sort"><p data-bind="click: function() { queue.changeSort('default'); }">&nbsp;<span data-bind="attr:{class: queue.getSort('default')}"></span></p></th>
					<th class="second-sort"><p data-bind="click: function() { queue.changeSort('patient-name'); }">Patient Name &amp; DOB <span data-bind="attr:{class: queue.getSort('patient-name')}"></span></p></th>
					<th class="third-sort"><p data-bind="click: function() { queue.changeSort('number'); }">Rx Number <span data-bind="attr:{class: queue.getSort('number')}"></span></p></th>
					<th class="fourth-sort"><p data-bind="click: function() { queue.changeSort('date'); }">Date &amp; Time <span data-bind="attr:{class: queue.getSort('date')}"></span></p></th>
			<?php if ($permissions->isAllowed('technician', 'dashboard')): ?>
					<th class="fifth-sort"><p data-bind="click: function() { queue.changeSort('priority'); }">Initiate <span data-bind="attr:{class: queue.getSort('priority')}"></span></p></th>
			<?php else: ?>
					<th class="sixth-sort"><p data-bind="click: function() { queue.changeSort('store'); }">Store<span data-bind="attr:{class: queue.getSort('store')}"></span></p></th>
			<?php endif; ?>
				</tr>
			</thead>
			<tbody>
				<!-- ko foreach: queue.prescriptionsToShow -->
				<tr data-bind="css: {'even' : $index() % 2 == 0, 'odd' : $index() % 2 != 0}, tpHighlight: highlight()">
					<td data-bind="css: {'details-color-red': isRejected(),
						'details-color-blue' : isAwaitingCounsel(),
						'details-color-green': isAwaitingApproval(),
						'details-color-orange': isTransfer() }"></td>
					<td class="cursor-pointer" data-bind="click: showPrescription">
						<p><strong class="link" data-bind="text: patientName"></strong>
						<em data-bind="text: isDraft() ? 'draft' + (onHold() ? (', on hold' + (isPartial() ? ', partial' : '')) : isPartial() ? ', partial' : '' ) : onHold() ? ('on hold' + (isPartial() ? ', partial' : '')) : isPartial() ? 'partial' : ''"  class="text-info"></em>
						</p>
						<p data-bind="text: patientDOB().format('M-D-YY')"></p>
                        <?php if ($permissions->isAllowed('pharmacist','dashboard')): ?>
                            <a data-bind="visible: isAwaitingApproval() && isHighPriority()" class="high-priority-pharmacist"></a>
                        <?php endif;
                        if ($permissions->isAllowed('technician', 'dashboard')):?>

                        <?php endif;?>
					</td>
					<td data-bind="click: showPrescription" class="cursor-pointer"><p class="link" data-bind="text: fullNumber"></p></td>
					<td><span class="date" data-bind="text: createdDt().format('M/D/YY')"></span>
							<time data-bind="text: createdDt().format('h:mm a')"></time></td>
			<?php if ($permissions->isAllowed('technician', 'dashboard')): ?>
					<td>
						<a href="#" data-bind="tpTip: 'Initiate patient tablet', visible: canInitiateTablet(), click: function(a, b) { initiatePatientTabletDialog($('#status-details-div'), 'left', b); }"><i class="counsel-icon" ></i></a>
                        <a data-bind="visible: isAwaitingApproval() && isHighPriority(), click: toggleHighPriority" class="high-priority cursor-pointer" data-original-title=""><img src="<?= URL::asset('img/high-priority.png'); ?>" alt="High Priority Button" /></a>
                        <a data-bind="visible: isAwaitingApproval() && !isHighPriority(), click: toggleHighPriority" class="low-priority cursor-pointer" data-original-title=""><img src="<?= URL::asset('img/low-priority.png'); ?>" alt="Low Priority Button" /></a>
					</td>
			<?php else: ?>
					<td>
					<span class="store-name" data-bind="text: storeName"></span>
					</td>
			<?php endif; ?>
				</tr>
				<!-- /ko -->
				<!-- ko if: queue.prescriptionsToShow().length == 0 -->
				<tr class="even">
					<td class="no-records" colspan="5">
						<p><strong>No patients found</strong></p>
					</td>
				</tr>
				<!-- /ko -->
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">&nbsp;</td>
				</tr>
			</tfoot>
		</table>
	</div>
<footer class="extensive">
        <p class="small">&copy; Telepharm <?= date('Y') ?></p>
        <p class="footer-text">Built in Iowa City</p>
</footer>
</article>
