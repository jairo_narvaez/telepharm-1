<?php if ($permissions->isAllowed('pharmacist','dashboard')): ?>
<button class="btn-approve-rx"
		data-bind="visible: prescription.canApprove() && !prescription.isTransfer(),
		click: prescription.approveMessage">
		<span data-bind="text: prescription.onHold() ? 'Approve On Hold' : 'Approve'">Approve</span></button>
<div class="checkbox-request-counsel clearfix" data-bind="visible: prescription.canRequestCounsel() && !prescription.onHold()">
    <input id="require_counsel" type="checkbox" style="display: none;" data-bind="tpCheckbox: {className:'styledcheckbox', label:'Require Counsel', checked: $data.checked}">
</div>
<button id="pharmacist-complete-transfer-button" class="btn btn-primary"
        data-bind="visible: prescription.isTransfer(),
		click: completeTransfer">
        <span>Complete & Print</span></button>
<button class="btn-reject"
		data-bind="visible: prescription.canReject() && !prescription.isTransfer(),
		click: prescription.rejectMessage">Reject</button>
<button class="btn btn-success"
                style="display:block;"
	 	onclick="location.href='/pharmacist';">
                Return Home</button>
<a href="#"
		data-bind="visible: prescription.canCancel() && prescription.isAwaitingCounsel(),
		click: function() { prescription.cancel(function() { if (videoConf.acceptedVideoCall() != null) { videoConf.tokbox.noModalHangUp(); } }) }">Cancel Rx</a>
<?php endif; ?>
