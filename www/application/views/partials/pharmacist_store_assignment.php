<?php if ($permissions->isAllowed('pharmacist','dashboard')): ?>
<div class="pharmacist-store-assignment">
    <?php foreach($company_stores as $company_store): ?>
        <div class="store">
            <label>
                <?php foreach($user_stores as $user_store): ?>
                    <?php $currently_associated = false; ?>
                    <?php if($user_store['id'] == $company_store['id']): ?>
                        <?php $currently_associated = true; break; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <input class="pharmacist-store-assignment-checkbox" type="checkbox" name="store-assignment" data-store-id="<?= $company_store['id'] ?>" <?php if($currently_associated): ?>checked="checked"<?php endif; ?> />
            <span><?= $company_store['name'] ?></span>
	    </label>
        </div>
    <?php endforeach; ?>
    <span class="saved hide" id="store-assign-success">
        Saved.
    </span>
    <div class="errors hide" id="store-assign-error">

    </div>
</div>
<?php endif; ?>
