<h4 class="comments-title">
	<ul class="nav nav-tabs">
		<li class="active">
			<div class="arrow"></div>
			<a href="#comments" data-toggle="tab">Comments</a></li>
		<li>
			<div class="arrow"></div>
			<a href="#history" data-toggle="tab">History</a></li>
	</ul>
</h4>
<div class="prescription-comments tab-content">
	<div class="tab-pane active" id="comments">
		<ul data-bind="foreach: $root.commentsVM.allComments" class="comments-list">
			<li data-bind="event: { mouseover: setAsRead}, attr:{class: isRead() ? 'read' : '' }">
				<div>
					<span class="comment-author" data-bind="text: createdBy"></span>
					<span class="comment-datetime" data-bind="text: createdDt().format('M/D/YYYY @ hh:mm a:')"></span><br/>
					<span class="comment-text" data-bind="text: comment"></span>
				</div>
			</li>
		</ul>
		<textarea name="comments" id="comments" rows="2" data-bind="value: newComment.text"></textarea>
		<button data-bind="click: newComment.save">Add Comment</button>
	</div>
	<div class="tab-pane" id="history">
		<ul data-bind="foreach: $root.commentsVM.allHistory" class="comments-list">
			<!-- ko if: title() != null -->
			<li class="read">
				<div>
					<span class="comment-author" data-bind="text: createdBy"></span>
					<span class="comment-datetime" data-bind="text: createdDt().format('M/D/YYYY @ hh:mm a:')"></span><br/>
					<strong class="log-type" data-bind="text: title() + ' Rx: '"></strong>
					<span class="log-text" data-bind="text: prescription().number"></span>
				</div>
                <!-- ko if: message -->
                <div>
                    <span class="log-text" data-bind="text: message"></span>
                </div>
                <!-- /ko -->
			</li>
			<!-- /ko -->
		</ul>
	</div>
</div>
