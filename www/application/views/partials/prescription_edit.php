<div class="modal hide fade" id="prescription-edit-modal">
    <div class="modal-header">
        <button type="button" class="close" data-bind="click: prescriptionEdit.close" aria-hidden="true">×</button>
        <h2>Confirm</h2>
    </div>
    <div class="modal-body">
        <div class="errors">
            <p data-bind="text : prescriptionEdit.error"></p>
        </div>
        <b>Are you sure you want to re-open this Prescription?</b>
        <span style="font-weight: normal;">Make sure that the information has been re-entered and sent from your PMS.</span>
    </div>
    <div class="modal-footer">
        <select style="float: left;" data-bind="value : prescriptionEdit.editReasonTypeId">
            <option value="1">Partial</option>
            <option value="2">Rejected</option>
            <option value="3">Technician Error</option>
            <option value="4">Other</option>
        </select>

        <div data-bind="visible: prescriptionEdit.requiresInput">
            <span>Reason:</span>
            <input type="text" data-bind="value : prescriptionEdit.message"/>
        </div>
        <a class="btn" data-bind="click: prescriptionEdit.close, text: 'Cancel'">No</a>
        <a class="btn btn-primary" data-bind="click: prescriptionEdit.complete, text: 'Ok'">Yes</a>
    </div>
</div>