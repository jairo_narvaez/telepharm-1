<!-- ko foreach: prescriptionImageLightBoxModalCollection() -->
<div class="lightbox hide not-printable" tabindex="-1" role="dialog" aria-hidden="true" data-bind="attr: {id: 'lightbox-image-' + prescriptionImage.image_id()}">
	<div class="lightbox-content">
        <img class="full-image" data-bind="attr: {src: prescriptionImage.imageUrl() + '?original=true&ts=' + timeStamp.getTime(), alt: prescriptionImage.imageType()}">
        <?php if ($permissions->isAllowed('pharmacist', 'dashboard')): ?>
           <span class="left">
               <img class="rotate-clockwise" src="<?= URL::asset('img/icon-rotate-clockwise.png') ?>" alt="Rotate Image Clockwise" data-bind="click : rotateClockwise" />
           </span>
            <span class="right">
                <img class="rotate-counter-clockwise"  src="<?= URL::asset('img/icon-rotate-counter-clockwise.png') ?>" alt="Rotate Image Counter Clockwise" data-bind="click : rotateCounterClockwise" />
            </span>
        <?php endif;?>
    </div>
</div>
<!-- /ko -->