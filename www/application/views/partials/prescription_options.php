<h4>
	<a href="#">Prescriptions Options</a>
	<b class="caret"></b>
</h4>
<div class="prescription-options">
    <div class="set-priority">
        <a href="#" data-bind="visible: prescription.isHighPriority(), click: prescription.toggleHighPriority, tpTip: 'Move prescription to low priority'" id="high-priority" class="high-priority">
            <img src="<?= URL::asset('img/high-priority.png');?>" alt="Move prescription to low priority" />
        </a>
        <a href="#" data-bind="visible: !prescription.isHighPriority(), click: prescription.toggleHighPriority, tpTip: 'Move prescription to high priority'" id="low-priority" class="low-priority">
            <img src="<?= URL::asset('img/low-priority.png');?>" alt="Move prescription to high priority" />
        </a>
        <label data-bind="css: prescription.isHighPriority() ? 'active' : 'inactive', attr: {for : prescription.isHighPriority() ? 'high-priority' : 'low-priority'}">High Priority</label>
    </div>
	<ul>
		<!-- ko foreach: prescriptionOptions.allOptions -->
			<li>
				<input type="checkbox" style="display: none;" data-bind="tpCheckbox: {className:'styledcheckbox', label:$data.label, checked: $data.checked, disabled: $root.prescription.rejected()}" />
			</li>
		<!-- /ko -->
	</ul>
</div>