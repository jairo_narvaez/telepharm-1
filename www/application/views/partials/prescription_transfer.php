<?php if ($permissions->isAllowed('technician','dashboard')): ?>
    <!-- ko if: prescriptionTransfer.isLoaded() -->
<div class="modal hide fade" id="prescription-transfer-modal">
    <div class="modal-header">
        <button type="button" class="close" data-bind="click : prescriptionTransfer.close" aria-hidden="true">&times;</button>
        <h2 data-bind="text : 'Transfer Prescription #' + prescription.number() + ' ' + prescription.refillNumber()"></h2>
    </div>
    <div id="transfer-errors" class="hide">
        <div class="fader">
        <!-- ko if : (prescription.transfer.errorMessages().length > 0) -->
            <div class="border-bottom">
                <div class="wrapper">
                <p>There was an error processing your request.</p>
                <ul>
                    <!-- ko foreach:prescription.transfer.errorMessages() -->
                        <!-- ko if: $data.severity < 2 -->
                            <li data-bind="text : $data.message"></li>
                        <!-- /ko -->
                    <!-- /ko -->
                </ul>
                </div>
            </div>
        <!-- /ko -->
        </div>
    </div>
    <div class="modal-body">
        <div class="controls">
            <div class="called-in-by">
               <h3 class="h3-transfer">Called In By: </h3>
               <div class="create-new">
                   <input maxlength="64" type="text" data-bind='value: prescription.transfer.calledInPerson.firstName'  placeholder="First Name">
                   <input maxlength="64" type="text" data-bind='value: prescription.transfer.calledInPerson.lastName' placeholder="Last Name">
               </div>
           </div>
            <div id="transfer-pharmacist">
                <h3 class="h3-transfer">Pharmacist to transfer to:</h3>
                <div class="fader">
                    <!-- ko if: prescription.transfer.pharmacists().length > 0 -->
                    <div class="select-existing">
                        <select data-bind="value : prescription.transfer.externalPharmacistId, valueUpdate : 'blur'">
                            <option value="">Select a pharmacist</option>
                            <!-- ko foreach: prescription.transfer.pharmacists() -->
                            <option data-bind="value : id, text: person.first_name + ' ' + person.last_name"></option>
                            <!-- /ko -->
                        </select>
                        <a href="#" data-bind="click : prescription.transfer.setNewPharmacist">Use a new pharmacist</a>
                    </div>
                    <!-- /ko -->
                    <div class="create-new">
                        <input type="text" placeholder="Pharmacist First Name" data-bind="value : prescription.transfer.externalPharmacist.person.firstName" />
                        <input type="text" placeholder="Pharmacist Last Name" data-bind="value : prescription.transfer.externalPharmacist.person.lastName" />
                        <a href="#" data-bind="click : prescription.transfer.setExistingPharmacist, visible: (prescription.transfer.pharmacists().length > 0)">Use an existing pharmacist</a>
                    </div>
                </div>
            </div>
            <div id="transfer-pharmacy">
               <h3 class="h3-transfer">Pharmacy to transfer to:</h3>
                <div class="fader" data-bind="visible : prescription.transfer.useNewPharmacist()">
                    <div class="create-new">
                        <input type="text" placeholder="Name" data-bind="value: prescription.transfer.externalPharmacist.externalStore.name" />
			<input type="text" placeholder="Street Line 1" data-bind="value: prescription.transfer.externalPharmacist.externalStore.address.line1" />
                        <input type="text" placeholder="Street Line 2 (optional)" data-bind="value: prescription.transfer.externalPharmacist.externalStore.address.line2" />
                        <input type="text" placeholder="City" data-bind="value: prescription.transfer.externalPharmacist.externalStore.address.city" />
                        <select id="transfer-state" data-bind="value: prescription.transfer.externalPharmacist.externalStore.address.state.id, valueUpdate : 'blur'">
                            <option value="">Select a state</option>
                            <!-- ko foreach: prescription.transfer.stateCollection.stateCollection() -->
                            <option data-bind="value : id, text: name + ' (' + abbreviation + ')', attr : { 'data-state-abbr' : abbreviation }"></option>
                            <!-- /ko -->
                        </select>
                        <input type="text" placeholder="Zip Code" data-bind="value: prescription.transfer.externalPharmacist.externalStore.address.postalCode" />
                        <a href="#" data-bind="click : prescription.transfer.setExistingPharmacy, visible : prescription.transfer.stores().length > 0">Use an existing pharmacy</a>
                   </div>
                    <!-- ko if : prescription.transfer.stores().length > 0 -->
                    <div class="select-existing">
                        <div class="wrapper">
                            <select data-bind="value: prescription.transfer.externalStoreId, valueUpdate : 'blur'">
                                <option value="">Select a pharmacy</option>
                                <!-- ko foreach : prescription.transfer.stores() -->
                                <option data-bind="value : id, text: name"></option>
                                <!-- /ko -->
                            </select>
                            <a href="#" data-bind="click : prescription.transfer.setNewPharmacy">Use a new pharmacy</a>
                        </div>
                    </div>
                    <!-- /ko -->
                </div>
                <!-- ko if : prescription.transfer.externalPharmacistId() == 0 -->
                <div class="pharmacy-information" data-bind="visible : !prescription.transfer.useNewPharmacist()">
                    <p><strong>Please select a pharmacist</strong></p>
                </div>
                <!-- /ko -->
                <div class="pharmacy-information" style="text-align: left; width: 100%;" data-bind="visible : prescription.transfer.externalPharmacist.externalStore.id() && !prescription.transfer.useNewPharmacist()">
                    <p><strong>Name:</strong> <span data-bind="text : prescription.transfer.externalPharmacist.externalStore.name()"></span></p>
                    <p><strong>Address:</strong>
                    <div class="transfer-address">
                    <p style="float: left" data-bind="text : prescription.transfer.externalPharmacist.externalStore.address.line1() + ', ' + ' '"></p>    
		    <p data-bind="text : prescription.transfer.externalPharmacist.externalStore.address.line2()"></p>
                        <p data-bind="text : prescription.transfer.externalPharmacist.externalStore.address.city() + ', ' + prescription.transfer.externalPharmacist.externalStore.address.state.abbreviation() + ' ' + prescription.transfer.externalPharmacist.externalStore.address.postalCode()"></p>
                    </div>
                    </p>
                </div>
            </div>
        </div>
        <div class="transfer-rx-display">
            <ul class="prescription-view" id="transfer-rx-info">
            </ul>
        </div>
    </div>
    <div class="modal-footer transfer-modal-footer">
        <div class="actions">
            <a href="#" data-bind="click: prescriptionTransfer.ok" class="btn btn-warning">Submit Transfer</a>
            <a href="#" data-bind="click: prescriptionTransfer.close" class="btn">Close</a>
        </div>
    </div>
</div>
    <!-- /ko -->

<?php endif; ?>
