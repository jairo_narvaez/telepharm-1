<div class="profile">
	<img src="<?=URL::uploaded_image($current_user->account->image, 'no-photo.png')?>" alt="Photo" />
	<div id="btn_available_state" data-bind="click: videoConf.changeAvailabilityForVideoConf, css: { unavailable: !videoConf.isAvailable() } ">
		<p data-bind="text: videoConf.isAvailable() ? 'Available' : 'Unavailable' ">Available</p>
		<a href="#">Click To Toggle</a>
	</div>
</div>
