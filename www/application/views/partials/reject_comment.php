<div class="modal hide fade" id="reject-comment-modal">
	<div class="modal-header">
		<h2>Please add a Comment about the Rejection</h2>
	</div>
	<div class="modal-body">
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="comment">Comment</label>
				<div class="controls">
					<textarea id="comment" rows="3" data-bind="value: rejectComment.text"></textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-bind="click: rejectComment.save">Save Comment</a>
	</div>
</div>
