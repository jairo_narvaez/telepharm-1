<div class="modal hide fade" id="report-error-modal">
	<div class="modal-header">
		<div class="row">
			<div class="span3">
				<h2>Error Reporting</h2>
				<p>Reported by: <span data-bind="text: currentUser.account.name"></span></p>
			</div>
			<div class="span3">
				<ul>
					<li><strong>Date:</strong> <span data-bind="text: moment().format('MM/DD/YYYY hh:mm A')"></span></li>
					<li><strong>Pharmacist:</strong> <span data-bind="text: reportError.pharmacist"></span></li>
					<li><strong>Technician:</strong> <span data-bind="text: reportError.technician"></span></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="modal-body">
		<div class="prescription-info">
			<ul class="patient-data">
				<li><strong>Patient Name:</strong> <span data-bind="text: prescription.patientName"></span></li>
				<li><strong>Phone:</strong> <span data-bind="text: prescription.patientPhone"></span></li>
			</ul>
			<ul class="drug-info">
				<li><strong>SIG:</strong> <span data-bind="text: prescriptionDispense.sigExpanded"></span></li>
				<li><strong>Prescribed:</strong> <span data-bind="text: prescriptionDrug.description"></span></li>
				<li><strong>NDC:</strong> <span data-bind="text: prescriptionDrug.newNDC"></span></li>
				<li><strong>Package:</strong> <span data-bind="text: prescriptionDrug.packageSize"></span></li>
				<li><strong>Strength:</strong> <span data-bind="text: prescriptionDrug.strength"></span></li>
			</ul>
			<ul class="dispense-info">
			<li><strong>Quantity Written:</strong> <span data-bind="text: prescriptionDispense.qtyWritten"></span></li>
				<li><strong>Quantity Dispensed:</strong> <span data-bind="text: prescriptionDispense.qtyDispensed"></span</li>
			</ul>
		</div>
		<hr/>
		<label for="error-text">Please explain the error:</label>
		<textarea id="error-text" rows="3" class="span7" data-bind="value: reportError.errorText, disable: reportError.disabled()"></textarea>
	</div>
	<div class="modal-footer">
		<!-- ko if: reportError.disabled() -->
		<a href="#" class="btn btn-default" data-bind="click: reportError.dismiss">Close</a>
		<!-- /ko -->
		<!-- ko ifnot: reportError.disabled() -->
		<a href="#" class="btn btn-default" data-bind="click: reportError.dismiss">Cancel</a>
		<a href="#" class="btn btn-primary" data-bind="click: reportError.save">Send Error</a>
		<!-- /ko -->
	</div>
</div>
