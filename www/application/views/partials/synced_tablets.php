<div id="synced-tablets-popover" class="popover fade bottom in">
	<div class="arrow"></div>
	<div class="popover-inner">
		<!-- ko if: tabletSync.currentSync().length == 1 -->
		<h4>Patient Tablet is currently active:</h4>
		<!-- /ko -->
		<!-- ko if: tabletSync.currentSync().length > 1 -->
		<h4>Patient Tablets are currently active:</h4>
		<!-- /ko -->
		<ul data-bind="foreach: tabletSync.currentSync">
			<li>
				<button class="btn btn-warning"
					data-bind="click: $root.tabletSync.unSync">
					Deactivate
					<span data-bind="text: humanName"></span>
				</button>
			</li>
		</ul>
	</div>
</div>

