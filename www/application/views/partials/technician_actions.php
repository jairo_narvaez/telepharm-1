<?php if ($permissions->isAllowed('technician','dashboard')): ?>
<button class="btn-approve-rx"
		data-bind="visible: prescription.canTechCompleteCounsel(),
		click: technicianComplete.open"> Complete</button>
<button class="btn btn-edit"
		data-bind="visible: prescription.canEdit(),
		click: prescriptionEdit.open">Edit</button>
<button class="btn btn-darkblue"
		data-bind="visible: prescription.canInitiateTablet(),
		click: function(a, b) { prescription.initiatePatientTabletDialog($(event.srcElement), 'right', b); }">Initiate Tablet</button>
    <!-- ko if: ((prescription.approvedOnHold() || prescription.completed()) && (!prescription.isTransfer() && !prescription.completedTransfer())) -->
<button class="btn btn-warning btn-transfer-rx" data-bind="click: prescriptionTransfer.show">Transfer RX</button>
    <!-- /ko -->
<?php endif; ?>
