<div class="modal hide fade" id="technician-complete-modal">
    <div class="modal-header">
        <button type="button" class="close" data-bind="click: technicianComplete.close" aria-hidden="true">×</button>
        <h2>Confirm</h2>
    </div>
    <div class="modal-body">
        <div class="errors">
            <p data-bind="text : technicianComplete.error"></p>
        </div>
        <b>Are you sure you want to complete the counseling session for this prescription?</b>
    </div>
    <div class="modal-footer">
        <select style="float: left;" data-bind="value : technicianComplete.technicianCompleteTypeId">
            <option value="1">Didn't Pick-up</option>
            <option value="2">Mail Order</option>
            <option value="3">Delivery</option>
            <option value="4">Other</option>
        </select>

        <div data-bind="visible: technicianComplete.requiresInput">
            <span>Reason:</span>
            <input type="text" data-bind="value : technicianComplete.message"/>
        </div>
        <a class="btn" data-bind="click: technicianComplete.close, text: 'Cancel'">No</a>
        <a class="btn btn-primary" data-bind="click: technicianComplete.complete, text: 'Ok'">Yes</a>
    </div>
</div>