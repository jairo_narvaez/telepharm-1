<h4 class="prescriptions"><span data-bind="text: 'Rx &#35; ' + prescription.fullNumber()"></span></h4>
<div class="fill-rx-btns">
<button class="btn-submite-rx"
	data-bind="visible: prescription.canSubmit(),
	click: prescription.submit">Submit Rx</button>
<button class="btn-draft"
	data-bind="
	click: prescription.saveDraft">Save Draft</button>
<div class="link-cancel-div"><a class="link-cancel"
	data-bind="visible: prescription.canCancel(),
	click: prescription.cancel">Cancel Rx</a></div>
</div>
