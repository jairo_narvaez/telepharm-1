<?php if ($permissions->isAllowed('technician','dashboard')): ?>
    <div class="technician-store-assignment">
        <select id="technician-store-assignment-select">
            <?php foreach($company_stores as $company_store): ?>
                <?php foreach($user_stores as $user_store): ?>
                    <?php $currently_associated = false; ?>
                    <?php if($user_store['id'] == $company_store['id']): ?>
                        <?php $currently_associated = true; break; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <option value="<?= $company_store['id'] ?>" <?php if($currently_associated): ?>selected="selected"<?php endif; ?>><?= $company_store['name'] ?></option>
            <?php endforeach; ?>
        </select>
        <span class="saved hide" id="store-assign-success">
        Saved.
        </span>
        <div class="errors hide" id="store-assign-error">

        </div>
    </div>
<?php endif; ?>