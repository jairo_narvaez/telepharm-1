<div class="modal hide fade" id="modal-video">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: videoConf.tokbox.hangUp">&times;</button>
		<h2>Video Call</h2>
	</div>
	<div class="modal-body">
		<div id="tokbox-container">
			<div id="publish-container"></div>
			<div id="subscribe-container"></div>
		</div>
	</div>
	<div class="modal-footer">
		<a class="btn" data-bind="click: videoConf.tokbox.hangUp">Hang Up</a>
	</div>
</div>
