<article class="personal-data">
	<img class="main-photo" src="<?= URL::asset("img/main-photo.jpg")?>" alt="Main Photo" />
	<ul class="flipped">
		<li><strong>Roby Miller</strong></li>
		<li>
			<p>332 8th St, Chicago IL</p>
			<p>319-458-0169</p>
			<p>robymiller@gmail.com</p>
		</li>
	</ul>
	<ul class="flipped">
		<li><strong>Prescription Information</strong></li>
		<li>
			<p>Rx Name</p>
			<p class="rx">Rx&#35;123456</p>
		</li>
	</ul>
</article>
<article class="sign-up-information">
	<p>Check each box after reading and sign your name.</p>
	<ul>
		<li><span class="styledcheckbox-signup checked"><input type="checkbox" style="opacity: 0;"></span><label for="not-take">Do not take with...</label></li>
		<li><span class="styledcheckbox-signup"><input type="checkbox" style="opacity: 0;"></span><label for="not-take">Do not take with...</label></li>
		<li><span class="styledcheckbox-signup"><input type="checkbox" style="opacity: 0;"></span><label for="not-take">Do not take with...</label></li>
	</ul>
	<input name="sign-here" type="text" class="sign-here" placeholder="Sign here">
	<button class="btn-talk-pharmacist">Talk To Pharmacist</button>
	<button class="btn-decline-counsel">Decline Counsel</button>
</article>