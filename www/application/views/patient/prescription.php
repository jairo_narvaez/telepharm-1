<script type="text/javascript">
	require(['patient_prescription']);
</script>
<div class="alert alert-block" data-bind="visible: prescription.requiredCounsel()">
	<i class="icon-warning-sign icon-3x" style="color:#A41C14;"></i> <span data-bind="text: prescription.approvedByPharmacist"></span>, has requested to speak to you 
	about your <span data-bind="text: prescriptionDrug.description"></span> prescription. Please tap
	<a class="btn btn-success btn-mini" href="#talk-to-pharmacist">Talk to pharmacist</a> to speak with your
	pharmacist.
</div>
<article class="personal-data">
	<div id="video">
		<div id="pharmacist-video">
			<img class="main-photo" src="<?= URL::asset("img/main-photo.jpg")?>" alt="Main Photo" />
		</div>
		<div id="patient-video" style="display:none"></div>
	</div>
	<ul class="flipped">
		<li>
			<strong>
					<span data-bind="text: prescription.patientName"></span>
			</strong>
		</li>
		<li>
			<p>
			</p>
			<p data-bind="text: prescription.patientPhone"></p>
			<p data-bind="text: prescription.patientEmail"></p>
		</li>
	</ul>
	<ul class="flipped">
		<li><strong>Prescription Information</strong></li>
		<li>
			<p>Rx Name: <span data-bind="text: prescriptionDrug.description"></span></p>
			<p class="rx">Rx&#35; <span data-bind="text: prescription.fullNumber()"></span></p>
		</li>
	</ul>
</article>
<article class="sign-up-information" id="talk-to-pharmacist">
		<!-- ko if: prescription.canCompleteCounsel() -->
			<!-- ko if: prescriptionLabels().length > 0 -->
			<p>Check each box after reading and sign your name.</p>
			<ul data-bind="foreach: prescriptionLabels">
				<li class="clear">
					<input type="checkbox" style="display:none;" data-bind="tpCheckbox: {className: 'styledcheckbox-signup', label: label()} , attr:{name : 'rule_' + $index() , id: 'rule_' + $index()}" class="required"/>
				</li>
			</ul>
			<!-- /ko -->
			<div class="sign-here-box">
				<input name="sign-here" type="text" class="sign-here required" placeholder="Sign here">
			</div>
			<!-- ko if:videoConf.currentCall() == null && videoConf.acceptedVideoCall() == null -->
			<button class="btn-talk-pharmacist" data-bind="click: talkToPharmacist">Talk To Pharmacist</button>
			<button class="btn-decline-counsel" data-bind="click: declineCounsel">Decline Counsel</button>
			<!-- /ko -->
			<!-- ko if:videoConf.currentCall() != null -->
			<p>Calling the tele-pharmacist</p>
			<!-- /ko -->
			<!-- ko if:videoConf.acceptedVideoCall() != null -->
			<button class="btn-decline-counsel" data-bind="click: finishCounsel">Finish Counsel</button>
			<!-- /ko -->
		<!-- /ko -->
		<!-- ko ifnot: prescription.canCompleteCounsel() -->
		<p>This prescription has been already counseled</p>
		<!-- /ko -->
</article>
<textarea style="display:none" id="prescription-data"><?=json_encode($prescription);?></textarea>
<textarea style="display:none" id="prescription-labels-data"><?=json_encode($prescription_labels);?></textarea>
<?= View::factory('partials/videoconf');?>
