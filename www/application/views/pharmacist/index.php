<script type="text/javascript">
	require(['pharmacist_index']);
</script>
<?= View::factory('partials/prescriptions_data')->set('prescriptions', $prescriptions);?>
<div class="span4 sidebar"> <!-- Left Content -->
	<?= View::factory('partials/profile');?>
	<?= View::factory('partials/available_users')->set('type', 'Technician');?>
	<?= View::factory('partials/analytics');?>
</div>
<div class="span9 main-content"> <!-- Right Content -->
	<article class="title">
		<h2>Patient Queue</h2>
		<a href="#" class="view-all" data-bind="click: function() { queue.filterPrescriptionBy('all') }">View All 
			(<span data-bind="text: queue.allCount ">0</span>)</a>
		<?= View::factory('partials/notifications');?>
		<?= View::factory('partials/audio_toggle');?>
	</article>
	<?= View::factory('partials/patientstable');?>
</div> <!-- End Right Content -->
<?= View::factory('partials/videoconf');?>
<?= view::factory('partials/start_call')->bind('call_id', $call_id) ?>
