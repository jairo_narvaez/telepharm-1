<script type="text/javascript">
    require(['prescription_create']);
</script>
<div class="span4 sidebar"> <!-- Left Content -->
    <?= View::factory('partials/profile');?>
    <?= View::factory('partials/technician_rx');?>
    <?= View::factory('partials/prescription_options');?>
    <?= View::factory('partials/prescription_comments');?>
</div> <!-- End Left Content -->
<div class="span9 main-content"> <!-- Right Content -->
    <div id="image-upload-wizard">
        <?= View::factory('partials/notifications');?>
        <div class="modal-header">
            <h2 data-bind="text: wizard.title"></h2>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="side">
                    <div class="scroller">
                        <ul class="images">
                            <!-- ko foreach: prescription.sortedImages() -->
                            <li data-bind="css: {selected: displayOrder() == $parent.wizard.selectedImage()}">
                                <i data-bind="css: {'icon-ok': id() != null, 'icon-white': id() == null}"></i>
                                <img data-bind="attr: {src: imageUrl(), alt: imageType(), id : 'uploaded-image-' + id() }, tpFileUploader: $data, click: $parent.wizard.selectImage"/>
                                <ul class="image-actions">
                                    <li data-bind="visible: exists()">
                                        <a href="#" class="enlarge-photo" data-bind="tpTip: 'Enlarge image', click: $parent.wizard.expandImage ">
                                            <img src="<?= URL::asset('img/icon-image-expand.png');?>" alt="Enlarge Image" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="upload-photo" data-bind="tpFilePicker: $data, tpTip:'Upload an image from your computer'" data-action="<?=URL::www('ajax/prescription/store_image');?>">
                                            <img src="<?= URL::asset('img/icon-image-upload-arrow.png');?>" alt="Upload Image" />
                                        </a>
                                    </li>
                                    <li data-bind="visible: exists()">
                                        <a href="#" class="delete-photo" data-bind="tpTip:'Delete image', click: $parent.wizard.deleteImage ">
                                            <img src="<?= URL::asset('img/icon-image-delete.png');?>" alt="Delete Image" />
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- /ko -->
                            <li><a href="#" data-bind="click: wizard.addImage"><i class="icon-plus"></i> Add Image</a></li>
                        </ul>
                    </div>
                </div>
                <div class="main">
                    <div class="stream">
                        <div class="header"
                             data-bind="click: wizard.startCamera, css: {'camera-started': wizard.isCameraOn()} "><p><a
                                    data-bind="text: wizard.startCameraText">Click to Start Live Camera</a></p></div>
                        <div class="body">
                            <video id="wizard-video"></video>
                            <canvas id="wizard-canvas"></canvas>
                            <img id="wizard-photo" class="hide"/>
                        </div>
                        <div class="footer">
                            <button class="btn btn-inverse" id="wizard-startbutton"
                                    data-bind="click: wizard.takeShot, visible: wizard.isReady">
                                <i class="icon-camera-retro icon-large"></i>
                            </button>
                            <span class="uploading" id="wizard-loading" style="display: none"></span>
                        </div>
                    </div>
                </div>
                <div class="right-side">
                    <div class="prescription-info">
                        <ul>
                            <li><strong>Patient Name:</strong> <span data-bind="text: prescription.patientName"></span>
                            </li>
                            <li><strong>SIG:</strong> <span data-bind="text: prescriptionDispense.sigExpanded"></span>
                            </li>
                            <li><strong>Prescribed:</strong> <span
                                    data-bind="text: prescriptionDrug.description"></span></li>
                            <li><strong>NDC:</strong> <span data-bind="text: prescriptionDrug.newNDC"></span></li>
                            <li><strong>Quantity Written:</strong> <span
                                    data-bind="text: prescriptionDispense.qtyWritten"></span></li>
                            <li><strong>Quantity Dispensed:</strong> <span
                                    data-bind="text: prescriptionDispense.qtyDispensed"></span</li>
                        </ul>
                    </div>
                    <?php if (count($medispan_images)): ?>
                        <div class="drug-image">
                            <h2>Medispan Image</h2>
                            <?php foreach ($medispan_images as $img): ?>
                                <img src="<?= $img ?>"/>
                                <?php break; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End Right Content -->
<div id="image-expand-modal" class="hide">

</div>
<textarea id="prescription-data" style="display:none;"><?=json_encode($prescription);?></textarea>
<?= view::factory('partials/patient_history') ?>
<?= View::factory('partials/videoconf'); ?>
<?= view::factory('partials/medispan_images')->bind('medispan_images', $medispan_images); ?>

