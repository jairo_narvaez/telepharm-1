<div class="span9 main-content"> <!-- Right Content -->
	<!-- ko if: prescription.isAwaitingCounsel() -->
	<article class="video-counseling"> <!-- Video Counseling -->
		<div class="video-details">
			<!-- ko if: videoConf.acceptedVideoCall() != null -->
			<button class="btn btn-mini btn-default" data-bind="click: function() { $('.counsel-comments').modal('show'); } " style="margin-bottom: 10px">Show Video Conference</button>
			<!-- /ko -->
			<p><strong>Patient Directions:</strong></p>
			<p data-bind="text: prescriptionDispense.sigExpanded"></p>
		</div>
	</article> <!-- End Video Counseling -->
	<!-- /ko -->
	<article class="patient-information"> <!-- Patient Information -->
		<div class="modal hide fade counsel-comments"> <!-- Counsel Comments Modal -->
			<div class="modal-body">
				<div class="video-list">
					<ul>
						<li>
							<div id="patient-video"></div>
						</li>
						<li>
							<div id="local-video"></div>
						</li>
					</ul>
					<button type="submit" class="btn btn-end-counsel" data-bind="click: function() { prescription.completeCounsel(function() { if (videoConf.acceptedVideoCall() != null) { videoConf.tokbox.noModalHangUp();  $('.counsel-comments').modal('hide'); } }) } ">End Counsel</button>
				</div>
			</div>
			<div class="modal-footer">
				<form class="form-inline">
					<input type="text" class="input" data-bind="value: newComment.text" autocomplete="off">
					<button type="submit" data-bind="click: newComment.save" class="btn-add-comment">Add Comment</button>
    			</form>
    			<div class="counsel-comment-box">
					<ul data-bind="foreach: $root.commentsVM.allComments"> 
						<li data-bind="event: {mouseover: setAsRead}, attr: {class: isRead() ? 'read' : ''}">
							<p><strong data-bind="text: createdBy">Tood McPharmacist</strong><span class="comment-datetime" data-bind="text: createdDt().format('M/D/YYYY')">1/18/2013:</span>
							<span data-bind="text: comment"></span></p>
						</li>
					</ul>
				</div>
			</div>
    	</div> <!-- End Counsel Comments Modal -->
    	<div class="modal hide fade  counsel-expand"> <!-- Counsel Modal Move Window -->
    		<div class="modal-header">
	    		<h5>Header</h5>
	    		<a class="move-window move-off" href="#">move</a>
	    		<a class="minimize" href="#">minimize</a>    		
	    	</div>
	    	<div class="modal-body">
				<div class="video-list">
					<ul>
						<li>
							<img src="<?= URL::asset('img/bg-counsel-video.jpg');?>" alt="Counsel Video" />
						</li>
						<li>
							<img src="<?= URL::asset('img/bg-counsel-video.jpg');?>" alt="Counsel Video" />
						</li>
					</ul>
					<button type="submit" class="btn btn-end-counsel">End Counsel</button>
				</div>
			</div>
    	</div> <!-- End Counsel Modal Move Window  -->
    	<div class="modal hide fade counsel-close"> <!-- Counsel Modal Close Window -->
    		<div class="modal-header">
	    		<h5>Header</h5>
	    		<a class="move-window move-off" href="#">move</a>
	    		<a class="open" href="#">open</a>    		
	    	</div>
    	</div> <!-- End Counsel Modal Close Window  -->
    	
		<h2>Patient Information</h2>
		<ul class="flipped">
			<li><strong>Name:</strong> <span data-bind="text: prescription.patientName"></span></li>
			<li><strong>Phone:</strong> <span data-bind="text: prescription.patientPhone"></span></li>
		</ul>
		<ul class="flipped">
			<li><strong>DOB:</strong> <span data-bind="text: prescription.patientDOB().format('MM-DD-YYYY')"></span></li>
			<li><a class="patient-history" href="#" data-bind="click: patientHistory.show">Patient History</a></li>
		</ul>
		<div class="gallery-information-up">
			<div class="gallery-information-down">
				<!-- ko foreach: prescription.paginatedImages() -->
				<div class="item">
					<p data-bind="text: imageType()"></p>
					<img data-bind="attr: {src: imageUrl(), alt: imageType(), id: 'prescription-image-' + id()}, tpMedispanImage: $data" width="224" height="219"/>
				</div>
				<!-- /ko -->
			</div>
			<?= view::factory('partials/images_paginator')->set('upload_link', 0); ?>
		</div>
		<div class="drug-prescription verify-rx">
			<a href="#" data-bind="click: function() {drugInformationDisplay(!drugInformationDisplay())} ">Drug Information<b class="caret" data-bind="css: {'open-caret': drugInformationDisplay()}"></b></a>
			<div data-bind="visible: drugInformationDisplay()">
			<?= view::factory('partials/drug_information'); ?>
			</div>
		</div>
		<div class="drug-prescription verify-rx">
			<a href="#" data-bind="click: function() {dispenseInformationDisplay(!dispenseInformationDisplay())} ">Prescription Information<b class="caret" data-bind="css: {'open-caret': dispenseInformationDisplay()}"></b></a>
			<div data-bind="visible: dispenseInformationDisplay()">
			<?= view::factory('partials/dispense_information'); ?>
			</div>
		</div>
	</article> <!-- End Patient Information -->
</div> <!-- End Right Content -->
