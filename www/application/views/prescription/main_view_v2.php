<div class="hide" id="image-placeholder">
    <img class="fake hide" src="<?= URL::asset('img/prescription_placeholder.png');?>" />
</div>
<div class="span9 main-content"> <!-- Right Content -->
	<!-- ko if: prescription.isAwaitingCounsel() -->
	<article class="video-counseling not-printable"> <!-- Video Counseling -->
		<div class="video-details">
			<!-- ko if: videoConf.acceptedVideoCall() != null -->
			<button class="btn btn-mini btn-default" data-bind="click: function() { $('.counsel-comments').modal('show'); } " style="margin-bottom: 10px">Show Video Conference</button>
			<!-- /ko -->
		</div>
	</article> <!-- End Video Counseling -->
	<!-- /ko -->
	<div class="not-printable"> <!-- Patient Information -->
		<div class="modal hide fade counsel-comments"> <!-- Counsel Comments Modal -->
			<div class="modal-body" style="padding-bottom:2px;">
				<div class="video-list">
					<ul style="height:220px">
						<li>
							<div id="patient-video"></div>
						</li>
						<li>
							<div id="local-video"></div>
						</li>
					</ul>
					<button type="submit" class="btn btn-end-counsel" data-bind="click: function() { prescription.completeCounselPharm(function() { window.location = '/pharmacist'; }) } ">End Counsel</button>
				</div>
			</div>
            <!-- ko if: $root.videoConf.acceptedVideoCall() -->
            <div class="prescription-list">
                <p class="title">Additional Prescriptions</p>
                <ul data-bind="foreach: $root.videoConf.acceptedVideoCall().additional_rx" class="list">
                    <li class="list-item">
                        <strong>Rx Number</strong><p class="additional-rx-number" data-bind="text: number + '-' + refill_number"></p>
                        <p class="description" data-bind="text : description"></p>
                    </li>
                </ul>
            </div>
            <!-- /ko -->
			<div class="modal-footer">
				<form class="form-inline">
					<input type="text" class="input" data-bind="value: newComment.text" autocomplete="off" />
					<button type="submit" data-bind="click: newComment.confSave" class="btn-add-comment">Add Comment</button>
				</form>
				<div class="counsel-comment-box">
					<ul data-bind="foreach: $root.commentsVM.allComments" style="overflow:auto; height:50px;">
						<li data-bind="event: {mouseover: setAsRead}, attr: {class: isRead() ? 'read' : ''}">
							<p><strong data-bind="text: createdBy">Tood McPharmacist</strong><span class="comment-datetime" data-bind="text: createdDt().format('M/D/YYYY')">1/18/2013:</span>
								<span data-bind="text: comment"></span></p>
						</li>
					</ul>
				</div>
			</div>
		</div> <!-- End Counsel Comments Modal -->
		<div class="modal hide fade counsel-expand"> <!-- Counsel Modal Move Window -->
			<div class="modal-header">
				<h5>Header</h5>
				<a class="move-window move-off" href="#">move</a>
				<a class="minimize" href="#">minimize</a>    		
			</div>
			<div class="modal-body">
				<div class="video-list">
					<ul>
						<li>
							<img src="<?= URL::asset('img/bg-counsel-video.jpg');?>" alt="Counsel Video" />
						</li>
						<li>
							<img src="<?= URL::asset('img/bg-counsel-video.jpg');?>" alt="Counsel Video" />
						</li>
					</ul>
					<button type="submit" class="btn btn-end-counsel">End Counsel</button>
				</div>
			</div>
		</div> <!-- End Counsel Modal Move Window  -->
		<div class="modal hide fade counsel-close"> <!-- Counsel Modal Close Window -->
			<div class="modal-header">
				<h5>Header</h5>
				<a class="move-window move-off" href="#">move</a>
				<a class="open" href="#">open</a>    		
			</div>
		</div> <!-- End Counsel Modal Close Window  -->
	</div>
<?= View::factory('partials/notifications');?>
	<ul class="prescription-view" id="patient-info-print">
		<li class="patient-info">
			<div class="row">
				<div class="span2">
					<p class="pharmacy-name">
						<span data-bind="text: prescription.storeName"></span> Pharmacy
					</p>
					<p class="pharmacy-phone">
						<span data-bind="text: prescription.storePhone"></span>
					</p>
					<?php /*
					<a href="#" class="btn-call">Call store</a>
					*/ ?>
				</div>
				<div class="span2 technician-info">
					<p>Technician: <span data-bind="text: prescription.technicianInitials"></span></p>					
					<p>
						<span data-bind="text: prescription.createdDt().format('h:mm A MM/DD/YY')"></span>
					</p>
				</div>
				<div class="clear"/>
				<hr/>
				<div class="span4 prescription-data">
					<div class="row">
						<div class="span2">
							<p class="rx-number">
								RX #: <span data-bind="text: prescription.fullNumber"></span>
							</p>
							<p class="patient-name">
								<span data-bind="text: prescription.patientName"></span>
							</p>
							<p>
								<a href="#" class="patient-history" data-bind="click: patientHistory.show">Patient History</a>
							</p>
						</div>
						<div class="span2 dates">
							<p>
								Filled: &nbsp;&nbsp;&nbsp;<span data-bind="text: prescription.filled"></span>
							</p>
							<p>
								Written: <span data-bind="text: prescription.written"></span>
							</p>
							<p>
								Expires: <span data-bind="text: prescription.expires"></span>
							</p>
							<p>
								Tech: &nbsp;&nbsp;&nbsp;&nbsp;<span data-bind="text: prescription.technicianInitials"></span>
							</p>
							<p data-bind="visible: prescription.approvedForCounsel">
								Pharmacist: <span data-bind="text: prescription.pharmacistInitials"></span>
							</p>
						</div>
					</div>
					<p class="sig-info" data-bind="text: prescriptionDispense.sigExpanded"></p>
					<p class="drug-name" data-bind="text: prescriptionDrug.description"></p>
					<div class="span2 prescriber">
						<p>NDC: <span data-bind="text: prescriptionDrug.formattedNDC"></span></p>
						<p>DR. <span data-bind="text: prescriptionPrescriber.name"></span></p>
                        <p><span data-bind="text: (prescriptionDispense.fillsOwed() || (prescriptionDispense.numRefills() + ' Refill(s)'))"></span></p>
					</div>
					<div class="span2 quantity">
						<p>QTY: <span data-bind="text: prescriptionDispense.qtyDispensed"></span></p>
					</div>
				</div>
			</div>
		</li>
		<!-- ko foreach: prescription.sortedImages() -->
		<li class="item not-printable prescription-image-li" data-bind="attr : { id : 'prescription-image-li-' + id() }">
            <img class="real hide" data-bind="tpMedispanImage: $data" />
		</li>
		<!-- /ko -->
	</ul>
	<div class="clear"></div>
	<!--
	<div class="interactions">
		<span class="count">0</span>
		<h2>INTERACTIONS</h2>
		Patient is prescribed <span data-bind="text: prescriptionDrug.description"></span> and may cause... (coming soon)
		<a class="open-interactions">Click to open</a>
	</div>
	-->
	<div class="row bottom-patient-info not-printable">
		<div class="span9">
			<div class="row">
				<div class="span5">
					<h3>Patient Information</h3>
					<ul>
						<li>
							<strong>Name:</strong>
							<span data-bind="text: prescription.patientName"></span>
						</li>
						<li>
							<strong>Phone:</strong>
							<span data-bind="text: prescription.patientPhone"></span>
						</li>
						<li>
							<strong>DOB: </strong>
							<span data-bind="text: prescription.patientDOB().format('MM/DD/YY')"></span>
						</li>
						<?php /* we are not capturing this yet
						<li>
							<strong>Address</strong>
							<span data-bind="text: prescription.patientAddress"></span>
						</li>
						*/ ?>
					</ul>
				</div>
				<div class="span4">
					<h3>Prescriber</h3>
					<ul>
						<li>
							<strong>Name:</strong>
							DR. <span data-bind="text: prescriptionPrescriber.name"></span>
						</li>
						<li>
							<strong>Phone:</strong>
							<span data-bind="text: prescriptionPrescriber.phone"></span>
						</li>
						<li>
							<strong>DEA:</strong>
							<span data-bind="text: prescriptionPrescriber.identifier"></span>
						</li>
						<li>
							<strong>NPI:</strong>
							<span data-bind="text: prescriptionPrescriber.npi"></span>
						</li>
						<?php /* we are not capturing this yet
						<li>
							<strong>Address</strong>
							<span>...</span>
						</li>
						*/ ?>
					</ul>
				</div>
			</div>
		</div>
    </div>
<!-- ko if: prescription.completedTransfer() || prescription.isTransfer() -->
        <div id="transfer-information-print" class="transfer-information">
            <div class="wrapper">
                <div class="completed-info">
                    <h3>Transfer Pharmacy</h3>
                    <div class="called-in-by">
                        <h4>Called In By: </h4>
                       <p><strong>First Name:</strong> <span data-bind='text: prescription.transfer.calledInPerson.firstName()'></span></p>
                       <p><strong>Last Name:</strong> <span data-bind='text: prescription.transfer.calledInPerson.lastName()'></span></p>
                    </div>
                    <div class="transfer-pharmacy">
                        <h4>Pharmacy to transfer to:</h4>
                        <p><strong>Name:</strong> <span data-bind="text : prescription.transfer.externalStore.name()"></span></p>
                        <p><strong>Address:</strong>
                        <div class="transfer-address">
                            <p data-bind="text : prescription.transfer.externalStore.address.line1()"></p>
                            <p data-bind="text : prescription.transfer.externalStore.address.line2()"></p>
                            <p data-bind="text : prescription.transfer.externalStore.address.city() + ', ' + prescription.transfer.externalStore.address.state.abbreviation() + ' ' + prescription.transfer.externalStore.address.postalCode()"></p>
                        </div>
                    </div>
                    <div class="transfer-pharmacist">
                        <h4>Pharmacist to transfer to:</h4>
                        <p><strong>First Name:</strong>  <span data-bind="text : prescription.transfer.externalPharmacist.person.firstName()"></span></p>
                        <p><strong>Last Name:</strong> <span data-bind="text : prescription.transfer.externalPharmacist.person.lastName()"></span></p>
                    </div>
                </div>
                <div class="completed-info top">
                    <h3 class="from" data-bind="text : prescription.store.name()"></h3>
                    <div>
                        <h4>Address:</h4>
                        <p data-bind="text : prescription.store.location.address1()"></p>
                        <p data-bind="text : prescription.store.location.address2()"></p>
                        <p data-bind="text : prescription.store.location.city() + ', ' + prescription.store.location.state() + ' ' + prescription.store.location.zip()"></p>
                    </div>
                     <div class="initiated-by">
                        <h4>Technician:</h4>
                        <p><span data-bind="text : prescription.transfer.initiatedBy()"></span></p>
                        <p><span data-bind="text : prescription.transfer.initiatedDate"></span></p>
                    </div>
                    <div class="completed-by" data-bind="visible:prescription.completedTransfer()">
                        <h4>Pharmacist:</h4>
                        <p><span data-bind="text : prescription.transfer.completedBy()"></span></p>
                        <p><span data-bind="text : prescription.transfer.completedDate"></span></p>
                    </div>
                </div>
                <div class="transfer-complete not-printable" data-bind="visible:prescription.completedTransfer()">
                    <div class="right">
                    <p>This transfer has been completed</p>
                        <a href="#" data-bind="click: print" class="print"><img src="<?= URL::asset('img/icon-print-rx.png');?>" alt="Print Prescription" title="Print" /></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /ko -->
        <!--
        <div class="span2">
            <ul>
                <li>
                    Total: $100
                </li>
                <li class="acq">
                    ACQ: $42
                </li>
                <li class="gp">
                    GP: $58
                </li>
            </ul>

        </div>
        -->
<!-- ko if: prescription.hasSignature() -->
<div class="signature-information not-printable">
    <div class="span9">
        <p class="signature-date" data-bind="text: prescription.counselDate()"></p>
        <p class="counseling-pharmacist">Counseling Pharmacist</p>
        <p class="counseling-pharmacist-name" data-bind="text:prescription.counselingPharmacistFullName() || 'Declined Counsel'"></p>
        <p class="signature-timestamp" data-bind="text : prescription.signatureDt() ? 'Signed On: ' + prescription.signatureDt() : ''"></p>
        <img class="signature-image" data-bind="attr: {src : prescription.signatureImageUrl() }" />
    </div>
</div>
<!-- /ko -->
<?= view::factory('partials/prescription_image_modal_light_box') ?>
<!-- End Right Content -->


