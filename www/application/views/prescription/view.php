<script type="text/javascript">
	require(['prescription_view']);
</script>
<?php if ($permissions->isAllowed('prescription', 'admin')): ?>
<div class="row">
	<div class="span13">
		<ul class="breadcrumb">
			<li><a href="<?=url::www('admin')?>">Home</a> <span class="divider">/</span></li>
			<li><a href="<?=url::www('admin/prescription')?>">Prescriptions</a> <span class="divider">/</span></li>
			<li class="active">Prescription <strong><?=$prescription['number']?></strong></li>
		</ul>
	</div>
</div>
<hr />
<?php endif; ?>
<?= View::factory('partials/prescription_data')->set('prescription', $prescription);?>

<div class="span4 sidebar"> <!-- Left Content -->
	<?= View::factory('partials/profile');?>
	<h4 class="prescriptions" data-bind="css: {
		'details-color-red': prescription.isRejected(),
		'details-color-blue' : prescription.isAwaitingCounsel(),
        'details-color-orange': prescription.isTransfer(),
		'details-color-green': prescription.isAwaitingApproval(),
	}">
	<span data-bind="visible: prescription.isDraft()">Draft</span>
	<span data-bind="visible: prescription.onHold() && !prescription.isTransfer()">On Hold</span>
	<span data-bind="visible: prescription.approvedOnHold() && !prescription.isTransfer()">Approved On Hold</span>
	<span data-bind="visible: prescription.requiredCounsel() &&
			prescription.refusedCounsel()">Patient has refused counsel</span>
	<span data-bind="visible: prescription.rejected()">Rejected</span>
	<span data-bind="visible: prescription.canceled()">Canceled</span>
	<span data-bind="visible: prescription.completed()">Completed</span>
	<span data-bind="visible: prescription.archived()">Archived</span>
	<span data-bind="visible: prescription.isTransfer()">Transfer RX</span>
	<span data-bind="visible: prescription.completedTransfer()">
        Completed Transfer
    </span>
	<span data-bind="visible: prescription.isAwaitingApproval() && !prescription.approvedOnHold() && !prescription.isTransfer()">Awaiting Approval</span>
	<span data-bind="visible: prescription.isAwaitingCounsel() && !prescription.completedTransfer(),
		   text: prescription.refusedCounsel() ?
		   'Refused Counsel' :
		   'Awaiting Counsel'">
		   </span>
	</h4>
	

	<div class="prescriptions-btns">
		<?= view::factory('partials/pharmacist_actions') ?>
		<?= view::factory('partials/technician_actions') ?>
		<?= view::factory('partials/common_labels') ?>
	</div>
	<div class="accordion">
		<h4>
			<a href="#">Additional Patient Prescriptions</a>
			<b class="caret"></b>
		</h4>
        <input type="hidden" id="total_load_count" value="<?php echo $total_load_count ?>" />
        <input type="hidden" id="current_load_count" value="<?php echo count($additional_prescriptions) ?>" />
        <div id="additional-rx" class="add-prescriptions">
			<ul>
                <li class="append-results-break">
                    <p>Results 1 - <?php echo count($additional_prescriptions) ?> of <?php echo $total_load_count ?></p>
                </li>
				<?php foreach($additional_prescriptions as $key=>$ap): ?>
				<li>
                    <div class="<?php echo $key == (count($additional_prescriptions) - 1) ? 'last-container' : 'container'; ?>" />
                        <div class="left">
                            <p><strong>Rx &#35; <?= $ap['number'] ?> - <? $ap['refill_number'] ?></strong></p>
                            <p class="description"><?= $ap['drug']['description']; ?></p>
                        </div>
                        <div class="right">
                            <button class="btn btn-mini btn-default" data-bind="click: function() { document.location = '<?=url::www('prescription/view/'.$ap['id'])?>'; } ">View Rx</button>
                        </div>
                    </div>
				</li>
				<?php endforeach; ?>

                <!-- ko foreach : additionalPrescriptions() -->
                <!-- ko if : firstLoadItem() -->
                <li id="load-first-prescription">
                    <p></p>
                </li>
                <!-- /ko -->
                <li>
                    <div data-bind="css : lastLoadItem() ? 'last-container' : 'container'">
                        <div class="left">
                            <p><strong data-bind="text : 'Rx' + fullNumber()"></strong></p>
                            <p class="description" data-bind="text : drug.description()"></p>
                        </div>
                        <div class="right">
                            <button class="btn btn-mini btn-default" data-bind="click: showPrescription">View Rx</button>
                        </div>
                    </div>
                </li>
                <!-- /ko -->
                <!-- ko if : !moreToLoad() -->
                <li class="final">
                    <p>No more results</p>
                </li>
                <!-- /ko -->
                <!-- ko if : moreToLoad() -->
                <li class="load-more-add-prescriptions" data-bind="visible : moreToLoad()">
                    <a href="#" data-bind="click : loadAdditionalPrescriptions">Load More</a>
                </li>
                <!-- /ko -->
			</ul>
		</div>
		<h4>
			<a class="auxillary-labels" href="#">Auxiliary Labels</a>
			<b class="caret auxillary"></b>
			<a class="add-label" id="add-label-link"
				href="#" data-bind="click: function() { auxiliaryLabels.showNewLabel(!auxiliaryLabels.showNewLabel()) }, visible: currentUser.role() == 'Pharmacist' " >
				Add Label</a>
		</h4>
		<div class="bottom popover" data-bind="style : { display: auxiliaryLabels.showNewLabel() ? 'block' : 'none'}"
			style="top: 0; left: 0; display: block;"
			id="create-label-popover">
			<div class="arrow"></div>
			<div class="popover-inner">
				<h3 class="popover-title">Create a Label</h3>
				<div class="popover-content">
					<p>
						<input data-bind="value: auxiliaryLabels.newLabelText, event: { keyup: auxiliaryLabels.listenEnterForAddLabel }" id="new-label-text" />
						<br/><br/>
						<button class="btn btn-primary" data-bind="click: auxiliaryLabels.addLabel">Create</button>
					</p>
				</div>
			</div>
		</div>
		<div class="auxillary-information">
			<textarea id="default-labels-data" style="display:none"><?= json_encode($default_labels)?></textarea>
			<textarea id="current-labels-data" style="display:none"><?= json_encode($current_labels)?></textarea>
			<ul data-bind="foreach: auxiliaryLabels.allLabels">
				<li data-bind="style: {background: $data.color}"><span data-bind=" text: $data.label()"></span></li>
			</ul>
		</div>

		<?= View::factory('partials/prescription_comments');?>
		<?php /*
		<time class="patient" data-bind="text: prescription.waitingTime">00:00</time>
		 */ ?>
	</div>
</div> <!-- End Left Content -->
<!-- Right Content -->
<?= view::factory('prescription/main_view_v2') ?>
<!-- End Right Content -->
<textarea id="prescription-comments-data" style="display:none"><?= json_encode($prescription_comments)?></textarea>
<textarea id="prescription-log-data" style="display:none"><?= json_encode($prescription_logs)?></textarea>
<?= view::factory('partials/start_call')->bind('call_id', $call_id) ?>
<?= view::factory('partials/patient_history') ?>
<?= View::factory('partials/videoconf');?>
<?= view::factory('partials/medispan_images')->bind('medispan_images', $medispan_images); ?>
<?= view::factory('partials/choose_action') ?>
<?= view::factory('partials/approve_action') ?>
<?= view::factory('partials/reject_action') ?>
<?= view::factory('partials/reject_comment') ?>
<?= view::factory('partials/report_error') ?>
<?= view::factory('partials/prescription_transfer') ?>
<?= view::factory('partials/technician_complete'); ?>
<?= view::factory('partials/prescription_edit'); ?>


