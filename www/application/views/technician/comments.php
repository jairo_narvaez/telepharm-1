<div class="span4 sidebar">
	<?= View::factory('partials/profile');?>
	<?= View::factory('partials/pharmacists');?>
	<?= View::factory('partials/analytics');?>
</div>

<div class="span9 main-content"> <!-- Right Content -->
	<article class="title">
		<h2>Comments</h2>
		<a href="#" class="view-all">View All</a>
	</article>
	<article class="content">
		<div class="status-results">
			<p>Patient Name &amp; DOB</p>
			<p>Rx Number</p>
			<p>Date &amp; Time</p>
		</div>
		<table class="status-details">
			<tr class="even">
				<td class="details-color-red"></td>
				<td><p><strong>William Johnson</strong></p><p>04-30-1959</p></td>
				<td><p>R34111947</p></td>
				<td><span class="date">09/25/12</span><time>10:15</time></td>
				<td><a class="options" href="#">options</a></td>
			</tr>
			<tr class="odd">
				<td class="details-color-blue"></td>
				<td><p><strong>Cindy Peterson</strong></p><p>04-30-1959</p></td>
				<td><p>R34111947</p></td>
				<td><span class="date">09/25/12</span><time>10:17</time></td>
				<td><a class="options" href="#">options</a></td>
			</tr>
			<tr class="even">
				<td class="details-color-green"></td>
				<td><p><strong>Felicia Hardy</strong></p><p>04-30-1959</p></td>
				<td><p>R34111947</p></td>
				<td><span class="date">09/25/12</span><time>10:30</time></td>
				<td><a class="options" href="#">options</a></td>
			</tr>
			<tr class="odd">
				<td class="details-color-blue"></td>
				<td><p><strong>Stephen Miller</strong></p><p>04-30-1959</p></td>
				<td><p>R34111947</p></td>
				<td><span class="date">09/25/12</span><time>10:31</time></td>
				<td><a class="options" href="#">options</a></td>
			</tr>
			<tr class="even">
				<td class="details-color-green"></td>
				<td><p><strong>Roger Hayman</strong></p><p>04-30-1959</p></td>
				<td><p>R34111947</p></td>
				<td><span class="date">09/25/12</span><time>10:40</time></td>
				<td><a class="options" href="#">options</a></td>
			</tr>
			<tr class="odd">
				<td class="details-color-green"></td>
				<td><p><strong>Johnathan Walker</strong></p><p>04-30-1959</p></td>
				<td><p>R34111947</p></td>
				<td><span class="date">09/25/12</span><time>10:42</time></td>
				<td><a class="options" href="#">options</a></td>
			</tr>
		</table>
		<div class="end-status-results"></div>
	</article>
</div>