<div class="span4"> <!-- Left Content -->
	<div class="profile">
		<img src="<?=URl::asset("img/photo-technician.jpg")?>" alt="Photo" />
		<div id="btn_available_state">
			<p>Available</p>
			<a href="#">Click To Toggle</a>
		</div>
	</div>
	<h4 class="prescriptions">Rx &#35; 100156</h4>
	<div class="fill-rx-btns">
		<button class="btn-submite-rx">Submit Rx</button>
		<button class="btn-cancel">Cancel</button>
	</div>
	<h4>
		<a href="#">Prescriptions Options</a>
		<b class="caret"></b>
	</h4>
	<div class="prescription-options">
		<ul>
			<li><span class="styledcheckbox checked"><input type="checkbox" style="opacity: 0;"></span>Partial</li>
			<li><span class="styledcheckbox"><input type="checkbox" style="opacity: 0;"></span>On Hold</li>
			<li><span class="styledcheckbox"><input type="checkbox" style="opacity: 0;"></span>Refill</li>
			<li><span class="styledcheckbox"><input type="checkbox" style="opacity: 0;"></span>New</li>
		</ul>
	</div>
</div> <!-- End Left Content -->
<div class="span9"> <!-- Right Content -->
	<article class="patient-information"> <!-- Patient Information -->
		<h2>Patient Information</h2>
		<ul class="flipped">
			<li><strong>Name:</strong> William Johnson</li>
			<li><strong>Phone:</strong> (813) 494-2300</li>
		</ul>
		<ul class="flipped">
			<li><strong>DOB:</strong> 04-30-1959</li>
			<li><a class="patient-history" href="#">Patient History</a></li>
		</ul>
		<div class="gallery-information-up verify-rx fill">
			<div class="gallery-information-down verify-rx fill">
				<div class="item">
					<p>Prescription</p>
					<img src="<?=URl::asset("img/photo-prescription-verify-rx.jpg")?>" alt="Prescription" />
					<a class="re-take-photo" href="#">Re-take Photo</a>
					<a class="upload-photo" href="#">Upload Photo</a>
				</div>
				<div class="item">
					<p>Drug Label</p>
					<img src="<?=URl::asset("img/photo-drug-verify-rx.jpg")?>" alt="Drug Label" />
					<a class="re-take-photo" href="#">Re-take Photo</a>
					<a class="upload-photo" href="#">Upload Photo</a>
				</div>
				<div class="item">
					<p>Drug Dispensed</p>
					<img src="<?=URl::asset("img/photo-drug-dispensed-verify-rx.jpg")?>" alt="Drug Dispensed" />
					<a class="re-take-photo" href="#">Re-take Photo</a>
					<a class="upload-photo" href="#">Upload Photo</a>
				</div>
			</div>
		</div>
		<div class="drug-prescription verify-rx ">
			<h5>Drug Information</h5>
			<p><strong>SIG:</strong> Take 1 tablet by mouth daily.</p>
			<p><strong>Prescribed:</strong> LIPITOR ORAL TABLET 3-0.02 MG</p>
			<p><strong>NDC:</strong> 000093-5423-28</p>
			<p><strong>Package:</strong> 28.0000 Each</p>
			<p><strong>Strength:</strong> 3-0.02 MG</p>
			<p><strong>Drug Interactions:</strong> The use of SIMVASTATIN 10% may result in an allergic reaction based on a reported history of allery.</p>
			<a class="read-more" href="#">Read More...</a>
		</div>
		<div class="drug-prescription verify-rx">
			<h5>Prescription Information</h5>
			<p><strong>Quantity Written:</strong> 84.0000</p>
			<p><strong>Quantity Dispensed:</strong> 84.0000</p>
			<p><strong>Written:</strong> 02/15/2012</p>
			<p><strong>Expires:</strong> 02/15/2013</p>
			<p><strong>Expires:</strong> 10/02/2012, 4:23pm</p>
			<p><strong>Last Fill:</strong> 10/02/2012, 12:01pm</p>
		</div>
	</article> <!-- End Patient Information -->
		</div> <!-- End Right Content -->