<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?=$title?></title>
<meta name="description" content="">
<meta name="viewport" content="width=1020, minimum-scale=0.5, maximum-scale=3.0, user-scalable=yes, target-densitydpi=device-dpi"/>
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link href="<?= URL::asset('css/bootstrap.css');?>" rel="stylesheet" type="text/css">
<link href="<?= URL::asset('css/typography.css');?>" rel="stylesheet" type="text/css">
<link href="<?= URL::asset('css/reset.css');?>" rel="stylesheet" type="text/css">
<style>
* {
color:#5F5F5F;
}
.header {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	height: 102px;
	background: #fcfff4;
	background: -moz-linear-gradient(top,  #fcfff4 0%, #eeeff1 40%, #d8dde1 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfff4), color-stop(40%,#eeeff1), color-stop(100%,#d8dde1));
	background: -webkit-linear-gradient(top,  #fcfff4 0%,#eeeff1 40%,#d8dde1 100%);
	background: -o-linear-gradient(top,  #fcfff4 0%,#eeeff1 40%,#d8dde1 100%);
	background: -ms-linear-gradient(top,  #fcfff4 0%,#eeeff1 40%,#d8dde1 100%);
	background: linear-gradient(to bottom,  #fcfff4 0%,#eeeff1 40%,#d8dde1 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#d8dde1',GradientType=0 );
	border-bottom: 1px solid #C1C6CA;
	-webkit-box-shadow:  0px 1px 1px 1px rgba(44, 44, 44, 0.2);
        box-shadow:  0px 1px 1px 1px rgba(44, 44, 44, 0.2);
	min-width: 820px;
}
.header img {
	float: right;
	margin: 22px;
	margin-right: 100px;
}
.header h1 {
	font-size: 22px;
	font-weight: bold;
	margin: 30px;
	color: #626463;
	display: inline-block;
}
.main {
	margin-top: 140px;
	margin-left: 30px;
	font-size: 16px;
}
</style>
</head>
<body>
<div class="header">
<h1>Sorry, your browser is not supported.</h1> <img src="<?=url::asset('img/telepharm-logo-login.png')?>" width="159" height="52"/>
</div>
<div class="main">
<p>To successfully view this page, please use <a href="http://www.google.com/chrome" target="_blank">Google<img src="<?=url::asset('img/chrome.png')?>" /></a>
</div>
</body>
</html>
