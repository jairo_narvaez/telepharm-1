	<div class="span4 sidebar"> <!-- Left Content -->
			<div class="profile">
				<img src="<?= url::asset("img/photo-pharmacist.jpg")?>" alt="Photo" />
				<div id="btn_available_state">
					<p>Available</p>
					<a href="#">Click To Toggle</a>
				</div>
			</div>
			<h4 class="prescriptions">Rx &#35; 100156</h4>
			<div class="prescriptions-btns">
				<button class="btn-approve-rx">Approve Rx</button>
				<button class="btn-request-counsel">Request Counsel</button>
				<button class="btn-reject">Reject</button>
			</div>
			<div class="accordion">
				<h4>
					<a href="#">Additional Patient Prescriptions</a>
					<b class="caret"></b>
				</h4>
				<div class="add-prescriptions">
					<ul>
						<li>
							<p><strong>Rx &#35; 598114</strong></p>
							<p>Crestor</p>
							<button class="btn-view-rx">View Rx</button>
						</li>
						<li>
							<p><strong>Rx &#35; 598114</strong></p>
							<p>Generic Simvastatin</p>
							<button class="btn-view-rx">View Rx</button>
						</li>
					</ul>
				</div>
				<h4>
					<a class="auxillary-labels" href="#">Auxillary Labels</a>
					<b class="caret"></b>
					<a class="add-label" href="#">Add Label</a>
				</h4>
				<div class="auxillary-information">
					<ul>
						<li><span class="styledcheckbox checked"><input type="checkbox" style="opacity: 0;"></span>Medicaid Will Not Reimbuse</li>
						<li><span class="styledcheckbox checked"><input type="checkbox" style="opacity: 0;"></span>For External Use Only</li>
						<li><span class="styledcheckbox checked"><input type="checkbox" style="opacity: 0;"></span>DO NOT CRUSH</li>
					</ul>
				</div>
				<time class="patient">1:40</time>
			</div>
		</div> <!-- End Left Content -->
		<div class="span9 main-content"> <!-- Right Content -->
			<article class="patient-information"> <!-- Patient Information -->
				<h2>Patient Information</h2>
				<ul class="flipped">
					<li><strong>Name:</strong> William Johnson</li>
					<li><strong>Phone:</strong> (813) 494-2300</li>
				</ul>
				<ul class="flipped">
					<li><strong>DOB:</strong> 04-30-1959</li>
					<li><a class="patient-history" href="#">Patient History</a></li>
				</ul>
				<div class="gallery-information-up verify-rx">
					<div class="gallery-information-down verify-rx">
						<div class="item">
							<p>Prescription</p>
							<img src="<?= url::asset("img/photo-prescription-verify-rx.jpg")?>" alt="Prescription" />
						</div>
						<div class="item">
							<p>Drug Label</p>
							<img src="<?= url::asset("img/photo-drug-verify-rx.jpg")?>" alt="Drug Label" />
						</div>
						<div class="item">
							<p>Drug Dispensed</p>
							<img src="<?= url::asset("img/photo-drug-dispensed-verify-rx.jpg")?>" alt="Drug Dispensed" />
						</div>
					</div>
				</div>
				<div class="drug-prescription verify-rx ">
					<h5>Drug Information</h5>
					<p><strong>SIG:</strong> Take 1 tablet by mouth daily.</p>
					<p><strong>Prescribed:</strong> LIPITOR ORAL TABLET 3-0.02 MG</p>
					<p><strong>NDC:</strong> 000093-5423-28</p>
					<p><strong>Package:</strong> 28.0000 Each</p>
					<p><strong>Strength:</strong> 3-0.02 MG</p>
					<p><strong>Drug Interactions:</strong> The use of SIMVASTATIN 10% may result in an allergic reaction based on a reported history of allery.</p>
					<a class="read-more" href="#">Read More...</a>
				</div>
				<div class="drug-prescription verify-rx">
					<h5>Prescription Information</h5>
					<p><strong>Quantity Written:</strong> 84.0000</p>
					<p><strong>Quantity Dispensed:</strong> 84.0000</p>
					<p><strong>Written:</strong> 02/15/2012</p>
					<p><strong>Expires:</strong> 02/15/2013</p>
					<p><strong>Expires:</strong> 10/02/2012, 4:23pm</p>
					<p><strong>Last Fill:</strong> 10/02/2012, 12:01pm</p>
				</div>
			</article> <!-- End Patient Information -->
		</div> <!--