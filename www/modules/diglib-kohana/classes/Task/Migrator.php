<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Task for use the DI_Migrator class from the command line
 *
 * It can accept the following options:
 *  - action: the action you want to perform
 *
 * could be one of this commands
 *    index (default)
 *    create
 *    pending
 *    diff
 *    apply
 *    applydiff
 *    latest
 * 
 * examples:
 * 
 *   show this help
 *     php index.php --task=migrator --action=index
 *   create a migration file
 *     php index.php --task=migrator --action=create
 *   show pending migrations by latest timestamp:
 *     php index.php --task=migrator --action=pending
 *   show pending migrations by diff with applied migrations:
 *     php index.php --task=migrator --action=diff
 *   apply pending migrations:
 *     php index.php --task=migrator --action=apply
 *   apply pending migrations by diff with applied migrations:
 *     php index.php --task=migrator --action=applydiff
 *   get latest version migrations:
 *     php index.php --task=migrator --action=latest
 *  
 *
 * @package    DI
 * @category   Task
 * @author     Digintent
 * @copyright  (c) 2013 - Digintent
 */
class Task_Migrator extends Minion_Task
{
	protected $migrator = null;

	protected $_options = [
		'action' => ['pending', 'diff', 'apply', 'applydiff', 'latest'],
	];

	protected function _execute(array $params)
	{
		$this->migrator = new DI_Migrator(DI_KohanaCompat::config('migrator.migrations_path'));
		switch ($params['action']) {
		case 'create': 
		{
			$migration_name = Minion_CLI::read('File name for the migration');
			$date = gmdate('YmdHis');
			$new_file = realpath(DI_KohanaCompat::config('migrator.migrations_path')) . DIRECTORY_SEPARATOR . $date . '_' . $migration_name . ".sql";
			$fh = fopen($new_file, 'w') or die("can't open file for write ");
			fwrite ( $fh, '-- Please add the migration SQL --');
			fclose($fh);
			echo 'File created ' . $new_file . "\n";
			break;
		}
		case 'pending':
			var_export($this->migrator->get_pending());
			break;
		case 'diff':
			var_export($this->migrator->get_pending_by_diff());
			break;
		case 'apply':
			var_export($this->migrator->apply_pending());
			break;
		case 'applydiff':
			var_export($this->migrator->apply_pending("pending_by_diff"));
			break;
		case 'latest':
			var_export($this->migrator->get_latest_version());
			break;
		default:
			$this->_help($params);
		}
	}

	public function build_validation(Validation $validation)
	{
		return parent::build_validation($validation)
			->rule('action', 'not_empty'); // Require this param
	}
}
