<?php defined('SYSPATH') or die('No direct script access.');

class Controller_DI_Migrator extends Controller_DI_CLI
{
	private $migrator = null;

	public function before()
	{
		parent::before();

		$this->migrator = new DI_Migrator(DI_KohanaCompat::config('migrator.migrations_path'));
	}

	public function action_create()
	{
		$migration_name = $this->getString('Name the migration: ');
		$date = gmdate('YmdHis');
		$new_file = realpath(DI_KohanaCompat::config('migrator.migrations_path')) . DIRECTORY_SEPARATOR . $date . '_' . $migration_name . ".sql";
		$fh = fopen($new_file, 'w') or die("can't open file for write ");
		fwrite ( $fh, '-- Please add the migration SQL --');
		fclose($fh);
		echo 'File created ' . $new_file . "\n";
	}

	public function action_pending()
	{
		var_export($this->migrator->get_pending());
	}

	public function action_diff()
	{
		var_export($this->migrator->get_pending_by_diff());
	}

	public function action_apply()
	{
		var_export($this->migrator->apply_pending());
	}

	public function action_applydiff()
	{
		var_export($this->migrator->apply_pending("pending_by_diff"));
	}

	public function action_latest()
	{
		var_export($this->migrator->get_latest_version());
	}

	public function action_index()
	{
		echo <<<USAGE
usage:

php index.php --uri=dbmigrate/{option}

option:
	could be one of this commands	
		index
		create
		pending
		diff
		apply
		applydiff
		latest

examples:

	show this help
		php index.php --uri=dbmigrate/index
	create a migration file
		php index.php --uri=dbmigrate/create
	show pending migrations by latest timestamp:
		php index.php --uri=dbmigrate/pending
	show pending migrations by diff with applied migrations:
		php index.php --uri=dbmigrate/diff
	apply pending migrations:
		php index.php --uri=dbmigrate/apply
	apply pending migrations by diff with applied migrations:
		php index.php --uri=dbmigrate/applydiff
	get latest version migrations:
		php index.php --uri=dbmigrate/latest

USAGE;

	}

}

