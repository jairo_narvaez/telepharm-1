<?php
class Database_MySQL_Result extends Kohana_Database_MySQL_Result implements DI_Untaintable
{
	public function isTainted()
	{
		return true;
	}
	
	public function untaint()
	{
		$results = array();
		foreach ($this as $result)
		{
			$results[] = $result->as_array();
		}
		
		return $results;
	}
}