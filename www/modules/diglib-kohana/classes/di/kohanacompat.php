<?php
/* This class exists only to help the DI module support multiple versions
 * of Kohana. Indvidual applications should not use this class.
 */
class DI_KohanaCompat
{
	/* Version 3.2 introduced a new way to load config settings. 
	 * This function should be used to load single keys.
	 */
	static public function config($key)
	{
		return version_compare(Kohana::VERSION, '3.2.0') >= 0 ? 
			Kohana::$config->load($key) : Kohana::config($key);
	}

	static public function logLevel($level)
	{
		static $levels = [
			LOG_EMERG => 'EMERGENCY',
			LOG_ALERT => 'ALERT',
			LOG_CRIT => 'CRITICAL',
			LOG_ERR => 'ERROR',
			LOG_WARNING => 'WARNING',
			LOG_NOTICE => 'NOTICE',
			LOG_INFO => 'INFO',
			LOG_DEBUG => 'DEBUG',
			8 => 'STRACE'
		];

		return version_compare(Kohana::VERSION, '3.2.0') >= 0 ?
			$level : $levels[$level];
	}
}