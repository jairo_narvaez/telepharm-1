<?php
	# DBRecordSet.class.php
	# Copyright 2006-07 by Matthew Leverton
	#
	# Represents a special type of PDOStatement that holds a record set.
	#	
	class DBRecordSet implements Iterator
	{
		private $sth;
		public $fetchType = PDO::FETCH_ASSOC;
		private $row = null, $rownum = 0;
		
		# __construct(PDOStatement $sth)
		#
		# This should never explicitly be called.
		#
		public function __construct(PDOStatement $sth)
		{
			$this->sth = $sth;
			$this->colCount = $sth ? $sth->columnCount() : 0;
		}
		
		# __destruct()
		#
		# Closes the recordset if it's still open.
		#
		public function __destruct()
		{
			if ($this->sth) $this->close();
		}
		
		# void close(void)
		#
		# Closes the recordset.
		#
		public function close()
		{
			$this->sth->closeCursor();
			$this->sth = NULL;
		}
		
		# int numRows(void)
		#
		# Returns the number of rows in the record set. Note that this 
		# might not be supported by the given database or recordset type.
		#
		public function numRows()
		{
			return $this->sth ? $this->sth->rowCount() : 0;
		}
		
		# bool fetchInto(mixed &$row)
		#
		# Fetches the next row of the recordset into the $row variable. 
		# Returns TRUE if a row was fetched or FALSE if one was not.
		#
		public function fetchInto(&$row)
		{
			return ($row = $this->sth->fetch($this->fetchType)) ? TRUE : FALSE;
		}
		
		# mixed[] fetchRow(void)
		#
		# Returns the next row or FALSE if no more are available.
		#
		public function fetchRow()
		{
			return $this->sth->fetch($this->fetchType);
		}

		public function current()
		{
			return $this->row;
		}

		public function key()
		{
			return $this->rowNum;
		}

		public function next()
		{
			++$this->rowNum;
		}

		public function rewind()
		{
			if ($this->row !== null)
				throw new Exception("You can only iterator through a RecordSet once.");
		}

		public function valid()
		{
			return ($this->row = $this->fetchRow());
		}
	}
?>
