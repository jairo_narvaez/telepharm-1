<?php defined('SYSPATH') or die('No direct script access.');

interface DI_Untaintable
{
	public function untaint();
	public function isTainted();
}