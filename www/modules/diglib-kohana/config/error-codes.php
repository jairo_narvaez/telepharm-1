<?php
return [
	'missing' => 'Required field',
	'out-of-range' => 'Out of range',
	'weak-password' => 'The password is too weak',
	'luhn-checksum' => 'Invalid number'
];