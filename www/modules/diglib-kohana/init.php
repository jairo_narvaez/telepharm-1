<?php defined('SYSPATH') or die('No direct script access.');

if (PHP_SAPI == 'cli')
{
	Route::set('dbmigrate', 'dbmigrate/(<action>(/<id>))')->defaults(
		array(
			'directory' => 'di',
			'controller' => 'migrator',
			'action'     => 'index',
		)
	);

	Route::set('config', 'config/<id>')->defaults([
		'directory' => 'di',
		'controller' => 'cli',
		'action' => 'config'
	]);
}