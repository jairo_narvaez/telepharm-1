<?php
require_once __DIR__.'/db/DB.class.php';
require_once __DIR__.'/db/DBStatement.class.php';
require_once __DIR__.'/db/DBRecordSet.class.php';

function db_open($config)
{
	/* $config is from Kohana */

	return new DB('mysql:host='.$config['connection']['hostname'].';dbname='.$config['connection']['database'].';charset='.$config['charset'],
		$config['connection']['username'],
		$config['connection']['password']
	);

}