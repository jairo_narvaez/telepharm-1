<?php
	# DBStatement.class.php
	# Copyright 2006-07 by Matthew Leverton
	#
	# Unlike the PDOStatement, a DBStatement is always a prepared
	# statement and never a recordset. There is never a reason to
	# explicitly create a DBStatement. Instead, it is returned by a call
	# to DB::prepare().
	#
	class DBStatement
	{
		private $sth;

		# __construct(PDOStatment $sth, mixed args[])
		#
		# This should never be called explicitly.
		#
		public function __construct(PDOStatement $sth, $args = NULL)
		{
			$this->sth = $sth;
			
			if ($args) foreach ($args as $i => $val) $this->sth->bindValue($i, $val);
		}

		# bool bindParam(string $key, mixed $val, [enum $type])
		# 
		# See PDOStatment::bindParam()
		# 
		public function bindParam($key, &$val, $type = NULL)
		{
			$this->sth->bindParam($key, $val, $type);
			return $this;
		}

		# bool bindValue(string $key, mixed $val, [enum $type])
		# 
		# see PDOStatment::bindValue()
		# 		
		public function bindValue($i, $val, $type = NULL)
		{
			$this->sth->bindValue($i, $val, $type);
			return $this;
		}
		
		# void bindValues([mixed $val1, [mixed $val2, ...]])
		#
		# Takes N arguments and binds them as parameters 1 to N.
		#
		public function bindValues()
		{
			$i = 0;
			foreach (func_get_args() as $val)
			{
				$this->sth->bindValue(++$i, $val);
			}

			return $this;
		}
		
		# mixed getOne(void)
		#
		# Returns the first field from the first record. 
		#
		public function getOne()
		{
			$this->sth->execute();
			$val = $this->sth->fetchColumn();
			$this->sth->closeCursor();
			return $val;
		}
		
		# mixed[] getCol(int $col = 0)
		#
		# Returns the $col'th field from each record.
		#
		public function getCol($col=0)
		{
			$this->sth->execute();
			$col = $this->sth->fetchAll(PDO::FETCH_COLUMN, $col);
			$this->sth->closeCursor();
			return $col;
		}
		
		# mixed[] getRow(void)
		#
		# Returns the first row as an array.
		#
		public function getRow()
		{
			$this->sth->execute();
			$row = $this->sth->fetch(PDO::FETCH_ASSOC);
			$this->sth->closeCursor();
			return $row;
		}
		
		# mixed[mixed key][] getAssoc(void)
		#
		# Returns each row, using the first field as the key. If the record
		# set only has two fields, then a key => val pair is returned;
		# otherwise a key => row pair is returned.
		#		
		public function getAssoc()
		{
			$sth = $this->sth;
			$sth->execute();
			$rs = array();
			if ($sth->columnCount() == 2)
			{
				while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$rs[array_shift($row)] = array_shift($row);
			}
			else
			{
				while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$rs[array_shift($row)] = $row;
			}
			
			return $rs;
		}
		
		# mixed[][] getAll(void)
		#
		# Returns the entire result set as an array of rows.
		#
		public function getAll()
		{
			$this->sth->execute();
			return $this->sth->fetchAll(PDO::FETCH_ASSOC);			
		}

		# bool exec(void)
		#
		# Executes the statement, which must not be a SELECT query.
		# Returns TRUE on success or FALSE on failure.
		#
		public function exec()
		{
			return $this->sth->execute();
		}
		
		# DBRecordSet query(void)
		#
		# Returns a record set for the given SELECT query.
		#
		public function query()
		{
			$this->sth->execute();
			return new DBRecordSet($this->sth);
		}
	}
?>
