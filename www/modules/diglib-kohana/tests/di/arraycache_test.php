<?php

require_once 'cache_trait.php';

/**
 * Test cases for DI_Cache_Array implementation
 * 
 */
class DI_ArraycacheTest extends PHPUnit_Framework_TestCase
{

	/**
	 * creates the cache object with an memory array backend
	 */
	public function setUp()
	{
		$this->cache = DI_Cache::factory('Array');
	}

	use Cache_Tests;

}
