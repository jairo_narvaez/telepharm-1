<?php

Trait Cache_Tests 
{
	/**
	 * cache var for making the tests
	 * 
	 * @var mixed  Defaults to null. 
	 */
	protected $cache;

	/**
	 * Different kind of objects / values to test the cache
	 * 
	 * @return array <mixed>
	 */
	public static function provider()
	{
		$obj = new stdClass;
		$obj->value = 'value';
		return array(
			array('value'), // string
			array(100), // int
			array($obj), // object
			array(array('a','b','c')), // array
		);
	}


	/**
	 * Test a memcache implementation creation
	 */
	public function testCacheCreation()
	{
		$this->assertNotNull($this->cache);
	}

	/**
	 * Test a memcache implementation use
	 *
	 * @dataProvider provider
	 */
	public function testCacheGetSet($value)
	{
		$this->cache->set('key', $value);
		$this->assertEquals($value, $this->cache->get('key'));
	}


	/**
	 * Test a memcache implementation use
	 */
	public function testCacheGetSetWithExpire()
	{
		$this->cache->set('key', 'value', (new DateTime)->add(new DateInterval("PT1S")));
		sleep(2); // wait until expire
		$this->assertEquals(false, $this->cache->get('key'));
	}


	/**
	 * Test a memcache implementation using a callback when getting
	 */
	public function testCacheGetSetWithCallbackTrue()
	{
		$this->cache->set('key', 'value');
		$result = $this->cache->get('key', function(&$data) {
			return true;
		});
		$this->assertEquals('value', $result);
		$this->assertEquals('value', $this->cache->get('key'));
	}

	/**
	 * Test a memcache implementation using a callback when getting
	 */
	public function testCacheGetSetWithCallbackFalse()
	{
		$this->cache->set('key', 'value');
		$result = $this->cache->get('key', function(&$data) {
			$data = 'value1'; // modifying inside the callback
			return false;
		});
		// It should return a new value but not cached it
		$this->assertEquals('value1', $result);
		// It should return the old value
		$this->assertEquals('value', $this->cache->get('key'));
	}

	/**
	 * Test a memcache implementation using a callback when getting
	 */
	public function testCacheGetSetWithCallbackDateTime()
	{
		$this->cache->set('key', 'value');
		$result = $this->cache->get('key', function(&$data) {
			$data = 'value1'; // modifying inside the callback
			return (new DateTime)->add(new DateInterval("PT1S"));
		});
		sleep(2); // wait to expire
		$this->assertEquals('value1', $result);
		$this->assertEquals(false, $this->cache->get('key'));
	}


	/**
	 * Test that the cache delete a key
	 * 
	 * @dataProvider provider
	 */
	public function testCacheDelete($value)
	{
		$this->cache->set('key', $value);
		$this->assertEquals($value, $this->cache->get('key'));
		$this->cache->delete('key');
		$this->assertEquals(false, $this->cache->get('key'));
	}
}	
