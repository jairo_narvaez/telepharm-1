<?php

# require apc.enable_cli=On on php.ini to work

if (! extension_loaded('apc')) return;

require_once 'cache_trait.php';

/**
 * Test cases for DI_Cache_Kohana implementation
 * 
 */
class DI_KohanaAPCcacheTest extends PHPUnit_Framework_TestCase
{

	/**
	 * creates the cache object with an memory array backend
	 */
	public function setUp()
	{
		$this->cache = DI_Cache::factory('Kohana', 'apc');
	}

	use Cache_Tests;

}




