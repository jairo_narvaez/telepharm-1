<?php

require_once 'cache_trait.php';

/**
 * Test cases for DI_Cache_Kohana implementation
 * 
 */
class DI_KohanacacheTest extends PHPUnit_Framework_TestCase
{

	/**
	 * creates the cache object with an memory array backend
	 */
	public function setUp()
	{
		$this->cache = DI_Cache::factory('Kohana');
	}

	use Cache_Tests;

}

