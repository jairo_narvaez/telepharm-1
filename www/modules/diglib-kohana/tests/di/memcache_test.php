<?php

if (! extension_loaded('memcached')) return;

require_once 'cache_trait.php';

/**
 * Test cases for DI_Cache_Memcache implementation
 * 
 */
class DI_MemcacheTest extends PHPUnit_Framework_TestCase
{

	/**
	 * creates the cache object with a memcached backend
	 */
	public function setUp()
	{
		$memcached = new Memcached;
		$memcached->addServer ( 'localhost' , 11211 );
		$this->cache = DI_Cache::factory('Memcache', $memcached);
	}

	use Cache_Tests;

}
