<?php 

class DI_MigratorTest extends PHPUnit_Framework_TestCase
{
	private $migrator = null;
	
	public function setUp()
	{
		$this->migrator = new DI_Migrator('migration_tests', 'testing'); 
	}
	
	/**
	 * check that the migrator class output the information required
	 *
	 **/
	public function test_db_connection()
	{
		// should report pending migrations to be applied to the db
		
		$this->assertNotNull($this->migrator);
		$this->assertTrue($this->migrator->is_connected());
		$this->assertEquals('migration_tests', $this->migrator->get_migrations_path());
	}
	
	/**
	 * check that the migrator class output the information required
	 *
	 **/
	public function test_pending_migrations()
	{
		// should report pending migrations to be applied to the db
		try {$this->migrator->execute_sql('TRUNCATE TABLE `schema_migrations`');} catch(Exception $e){}

		$pending = $this->migrator->get_pending();

		$this->assertEquals(2, count($pending));
		$this->assertSame(array('20111123015304_test1.sql', '20111123020304_test2.sql'), $pending);
	}
	
	/**
	 * check that the migrator class output the information required
	 *
	 **/
	public function test_pending_migrations_by_diff()
	{
		$this->test_apply_pending();

		rename($this->migrator->get_migrations_path() . "/". "test3.sql", 
			$this->migrator->get_migrations_path() . "/". "20111124014504_test3.sql");
		
		// should report pending migrations to be applied to the db
		$pending = $this->migrator->get_pending_by_diff();

		rename($this->migrator->get_migrations_path() . "/". "20111124014504_test3.sql", 
			$this->migrator->get_migrations_path() . "/". "test3.sql");

		$this->assertEquals(1, count($pending));
		$this->assertSame(array('20111124014504_test3.sql'), $pending);
	}
	
	/*
	 * @depends test_pending_migrations
	*/
	public function test_apply_pending()
	{
		try {$this->migrator->execute_sql('TRUNCATE TABLE `schema_migrations`');} catch(Exception $e){}
		try {$this->migrator->execute_sql('DROP TABLE dummy_test1');} catch(Exception $e){}
		try {$this->migrator->execute_sql('DROP TABLE dummy_test2');} catch(Exception $e){}
		try {$this->migrator->execute_sql('DROP TABLE dummy_test3');} catch(Exception $e){}

		$applied = $this->migrator->apply_pending();
		
		$this->assertEquals(2, count($applied));
		$this->assertSame(array('20111123015304_test1.sql', '20111123020304_test2.sql'), $applied);
		
		$this->assertSame('20111123020304', $this->migrator->get_latest_version());
	}

	/*
	 * @depends test_pending_migrations_by_diff
	*/
	public function test_apply_pending_by_diff()
	{
		try {$this->migrator->execute_sql('TRUNCATE TABLE `schema_migrations`');} catch(Exception $e){}
		try {$this->migrator->execute_sql('DROP TABLE dummy_test1');} catch(Exception $e){}
		try {$this->migrator->execute_sql('DROP TABLE dummy_test2');} catch(Exception $e){}
		try {$this->migrator->execute_sql('DROP TABLE dummy_test3');} catch(Exception $e){}

		$applied = $this->migrator->apply_pending("pending_by_diff");
		
		$this->assertEquals(2, count($applied));
		$this->assertSame(array('20111123015304_test1.sql', '20111123020304_test2.sql'), $applied);
		
		$this->assertSame('20111123020304', $this->migrator->get_latest_version());
	}
	
	public function test_apply_new_pending_files()
	{
		// already migrate 2 files
		try {$this->migrator->execute_sql('DROP TABLE dummy_test4');} catch(Exception $e){}

		$this->test_apply_pending();
			
		$this->assertSame('20111123020304', $this->migrator->get_latest_version());
		
		// create a new migration		
		rename($this->migrator->get_migrations_path() . "/". "test3.sql", 
			$this->migrator->get_migrations_path() . "/". "20111124014504_test3.sql");
		
		$pending = $this->migrator->get_pending();

		$this->assertEquals(1, count($pending));
		$this->assertSame(array('20111124014504_test3.sql'), $pending);
		
		$applied = $this->migrator->apply_pending();

		$this->assertEquals(1, count($applied));
		$this->assertSame(array('20111124014504_test3.sql'), $applied);

		// check that the latest version is the one with the new file

		$this->assertSame('20111124014504', $this->migrator->get_latest_version());
		
		rename($this->migrator->get_migrations_path() . "/". "20111124014504_test3.sql", 
			$this->migrator->get_migrations_path() . "/". "test3.sql");
	}
	
	
} // END class Tests_Migrator_Test extends PHPUnit_Framework_TestCase
