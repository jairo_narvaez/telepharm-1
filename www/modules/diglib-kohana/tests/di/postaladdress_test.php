<?php

/**
 * Test Postal Address Verifiers
 */
class DI_PostalAddressTest extends PHPUnit_Framework_TestCase
{
	/**
	 * fake postal code verifier implementation
	 */
	public function testVeriftyAddressUsingTestClass()
	{
		for ($i=-1; $i<10; $i++) {
			$address = new DI_PostalAddress;
			$address->street = "Main AV$i";
			$address->city = "Chicago";
			$address->state = "Illinois";
			$address->level2 = "level2";
			$address->level3 = "level3";

			$verifier = DI_PostalAddress_Verifier::factory('Test');

			switch ($i) {
			case -1:
				$verified = $verifier->verify($address);
				$this->assertEquals(1, count($verified));
				$this->assertEquals($address->asArray() , $verified[0]->address->asArray());
				# var_export($verified[0]->jsonSerialize());
				break;
			case 0:
				$verified = $verifier->verify($address);
				$this->assertEquals(0, count($verified));
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				$verified = $verifier->verify($address);
				$this->assertEquals($i, count($verified));
				foreach($verified as $v)
				{
					var_export($v->address->street);
					# var_export($verified[0]->jsonSerialize());
				}
				break;
			default:
				try {
					$verified = $verifier->verify($address);
				} catch (Exception $e) {
					$this->assertEquals("test exception", $e->getMessage());
				}
				break;
			}
			echo "\n";
		}
	}

	/**
	 * test smarty streets postal code verifier
	 */
	public function testVeriftyAddressUsingSmartyStreets()
	{
		$address = new DI_PostalAddress;
		$address->street = "118 N Peoria Street, 3S";
		$address->city = "Chicago";
		$address->state = "IL";

		$verifier = DI_PostalAddress_Verifier::factory('SmartyStreets');

		# commented to not waste api calls from the free tier
		/*$verified = $verifier->verify($address);
		$this->assertEquals(1, count($verified));
		var_export($verified[0]->address);*/
		$this->assertNotNull($verifier);
	}
}

