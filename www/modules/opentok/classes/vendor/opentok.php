<?php

/**
 * The OpenTok SDK requires
 */
require_once dirname(__FILE__) . '/OpenTokSDK.php';
require_once dirname(__FILE__) . '/OpenTokArchive.php';
require_once dirname(__FILE__) . '/OpenTokSession.php';
